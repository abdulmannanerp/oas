$(document).ready(function() {
	$.uploadPreview({
		input_field: "#image-upload",
		preview_box: "#image-preview",
		label_field: "#image-label",
	});
	$('#image-upload').on("change", function(){
		$('#image-label').val('Photo');
	});

	$('.degree-recieved').on('click', function(){
		$this = $(this);
		serialNumberContainer = $('#serial-number-container');
		// matricCertificateContainer = $('#matric_certificate_container');
		if ( $this.val() == 'Yes' ) {
			// if ( ! matricCertificateContainer.hasClass('hidden') ) {
			// 	$('#matric_certificate').removeAttr('required');
			// 	matricCertificateContainer.addClass('hidden');
			// }
			serialNumberContainer.removeClass('hidden');
			$('#degree-serial-number').attr('required', 'true');
		} else if ( $this.val() == 'No' ) {
			if ( ! serialNumberContainer.hasClass('hidden') ) {
				$('#degree-serial-number').removeAttr('required');
				serialNumberContainer.addClass('hidden');
			}
			// matricCertificateContainer.removeClass('hidden');
			// $('#matric_certificate').attr('required', 'true');
		}
	});

	$('.is_transcript_recieved').on('click', function(){
		var $this = $(this);
		if ( $this.val() == 'No' ) {
			$('.alert-message').removeClass('hidden');
			$('.upload-transcript-container').addClass('hidden');
			$('#upload_transcript').removeAttr('required');
		} else {
			$('.upload-transcript-container').removeClass('hidden');
			$('#upload_transcript').attr('required', 'true');
			$('.alert-message').addClass('hidden');
		}
	});

    $('.select-degree').select2();

   	$('.guest_1_name').on('blur', function() {
   		$this = $(this);
   		$cnic = $('.guest_1_cnic');
   		if ( ! $this.val() == '' ) {
   			$cnic.attr('required', 'true');
   		} else {
   			$cnic.removeAttr('required');
   		}
   	});

   	$('.guest_2_name').on('blur', function() {
   		$this = $(this);
   		$cnic = $('.guest_2_cnic');
   		if ( ! $this.val() == '' ) {
   			$cnic.attr('required', 'true');
   		} else {
   			$cnic.removeAttr('required');
   		}
   	});
});
