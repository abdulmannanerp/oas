$(function(){
    $('#domicile').on('change', function(){
       $.ajax({
           method: 'post',
           url: '/frontend/getDistrict',
           data: {
               id: $(this).val()
           },
           success: function ( response ) {
               $('#district').empty();
               $('#district').append(
                    '<option value="">Select District</option>'
                );
               $.each(response, function (key, value) {
                   $('#district').append(
                       '<option value="'+value.pkDistId+'">'+ value.distName+'</option>'
                   );
               });
           }
       });
    });


    $('.disability_control').on('change', function(){
        var disability_control = $('.disability_control').find(":selected").val();

        if(disability_control == 'None'){
            $('.frm_hide_show').addClass('ext-hide_disable').removeClass('ext-show_disable');
        }else{
            $('.frm_hide_show').addClass('ext-show_disable').removeClass('ext-hide_disable');
        }
        console.log(disability_control);
    });
    



    $('#nationality').on('change', function(){
        console.log('inside');
        var nationality_selected = $('#nationality').find(":selected").val();
        if(nationality_selected == 1){
            $('.ext-hide-2').addClass('ext-show').removeClass('ext-hide-2'); 
            $("#domicile, #district").attr("required", true);  
            $('.contact-label').text('');         
            $('.contact-label').text('Mobile No.(301252xxx)'); 
            $('#carrier, #permanent_carrier, #phone, #permanent_phone').attr("required", true); 
            $('#overseas_phone, #overseas_permanent_carrier, #overseas_mailing_carrier, #nicop').attr("required", false);
            $('#overseas_phone, #overseas_permanent_carrier, #overseas_mailing_carrier, .afghan_refugee, .nicop').addClass('ext-hide-2').removeClass('ext-show'); 
            // Dev Mannan: Code for fields shifting in between different tabs
            $("#place_of_birth, #passport_number, #date_issue, #place_issue, #hec_letter_no, #hec_letter_date, #pakistani_visa, #type_of_visa, #visa_issue_date, #visa_expiry_date").attr("required", false); 
            $('.ext-show_new').addClass('ext-hide-2_new').removeClass('ext-show_new'); 

        }else if(nationality_selected == 2 || nationality_selected == 3){
            $('.ext-show').addClass('ext-hide-2').removeClass('ext-show');
            $("#domicile, #district").attr("required", false); 
            $('.contact-label').text('');         
            $('.contact-label').text('Contact No'); 
            $('#carrier, #permanent_carrier, #phone, #permanent_phone, #nicop').attr("required", false); 
            $('#overseas_phone, #overseas_permanent_carrier, #overseas_mailing_carrier').attr("required", true); 
            $('#domicile, #district, .nicop').addClass('ext-hide-2').removeClass('ext-show');
            $('#overseas_phone, #overseas_permanent_carrier, #overseas_mailing_carrier, .afghan_refugee').addClass('ext-show').removeClass('ext-hide-2');
            // Dev Mannan: Code for fields shifting in between different tabs
            $("#place_of_birth, #passport_number, #date_issue, #place_issue, #hec_letter_no, #hec_letter_date, #pakistani_visa, #type_of_visa, #visa_issue_date, #visa_expiry_date").attr("required", true); 
            $('.ext-hide-2_new').addClass('ext-show_new').removeClass('ext-hide-2_new');
        }
        if(nationality_selected == 2){
            $('.nicop').addClass('ext-show').removeClass('ext-hide-2');
            $("#nicop").attr("required", true);
        }
    });

    // $('#first_prefer').on('change', function (e) {
    //     var optionSelected = $("option:selected", this);
    //     var valueSelected = this.value;
    //     if(valueSelected == 131){
    //         $('#second_prefer').find('option').remove();
    //         $("#second_prefer").append(new Option("BS Mechanical Engineering", "132"));
    //         $("#second_prefer").append(new Option("BS Civil Engineering", "157"));
    //     }
    //     if(valueSelected == 132){
    //         $('#second_prefer').find('option').remove();
    //         $("#second_prefer").append(new Option("BS Electrical Engineering", "131"));
    //         $("#second_prefer").append(new Option("BS Civil Engineering", "157"));
    //     }
    //     if(valueSelected == 157){
    //         $('#second_prefer').find('option').remove();
    //         $("#second_prefer").append(new Option("BS Electrical Engineering", "131"));
    //         $("#second_prefer").append(new Option("BS Mechanical Engineering", "132"));
    //     }
        
    // });
    // $('#second_prefer').on('change', function (e) {
    //     var optionSelected = $("option:selected", this);
    //     var valueSelected = this.value;
    //     if(valueSelected == 131){
    //         $('#first_prefer').find('option').remove();
    //         $("#first_prefer").append(new Option("BS Mechanical Engineering", "132"));
    //         $("#first_prefer").append(new Option("BS Civil Engineering", "157"));
    //     }
    //     if(valueSelected == 132){
    //         $('#first_prefer').find('option').remove();
    //         $("#first_prefer").append(new Option("BS Electrical Engineering", "131"));
    //         $("#first_prefer").append(new Option("BS Civil Engineering", "157"));
    //     }
    //     if(valueSelected == 157){
    //         $('#first_prefer').find('option').remove();
    //         $("#first_prefer").append(new Option("BS Electrical Engineering", "131"));
    //         $("#first_prefer").append(new Option("BS Mechanical Engineering", "132"));
    //     }
    // });
    $('input[name="first_radio"]:radio').on('change', function (e) {
        var valueSelected = this.value;
        if(valueSelected == 'first'){
            $('input[name=second_radio][value=first]').prop('checked', false);
            $('input[name=third_radio][value=first]').prop('checked', false);
        }
        if(valueSelected == 'second'){
            $('input[name=second_radio][value=second]').prop('checked', false);
            $('input[name=third_radio][value=second]').prop('checked', false);
        }
        if(valueSelected == 'third'){
            $('input[name=second_radio][value=third]').prop('checked', false);
            $('input[name=third_radio][value=third]').prop('checked', false);
        }
    });

    $('input[name="second_radio"]:radio').on('change', function (e) {
        var valueSelected = this.value;
        if(valueSelected == 'first'){
            $('input[name=first_radio][value=first]').prop('checked', false);
            $('input[name=third_radio][value=first]').prop('checked', false);
        }
        if(valueSelected == 'second'){
            $('input[name=first_radio][value=second]').prop('checked', false);
            $('input[name=third_radio][value=second]').prop('checked', false);
        }
        if(valueSelected == 'third'){
            $('input[name=first_radio][value=third]').prop('checked', false);
            $('input[name=third_radio][value=third]').prop('checked', false);
        }
    });
    $('input[name="third_radio"]:radio').on('change', function (e) {
        var valueSelected = this.value;
        if(valueSelected == 'first'){
            $('input[name=first_radio][value=first]').prop('checked', false);
            $('input[name=second_radio][value=first]').prop('checked', false);
        }
        if(valueSelected == 'second'){
            $('input[name=first_radio][value=second]').prop('checked', false);
            $('input[name=second_radio][value=second]').prop('checked', false);
        }
        if(valueSelected == 'third'){
            $('input[name=first_radio][value=third]').prop('checked', false);
            $('input[name=second_radio][value=third]').prop('checked', false);
        }
    });

});

$(function () {
    $( "#full_name, #father_name, #SSC_Title_t, #SSC_From_t, #SSC_Subject_t, #HSC_Title_t, #HSC_From_t, #HSC_Subject_t, #BA_Bsc_Title_t, #BA_Bsc_From_t, #MSc_Title_t, #MSc_From_t, #BS_Title_t, #BS_From_t, #MS_Title_t, #MS_From_t, #BA_Bsc_Subject, #BA_Bsc_Specialization, #MSc_Subject, #MSc_Specialization, #BS_Subject, #BS_Specialization, #MS_Subject, #MS_Specialization" ).keypress(function(e) {
        var arr = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz ";
        var code;
        if (window.event)
            code = e.keyCode;
        else
            code = e.which;
        var char = keychar = String.fromCharCode(code);
        if (arr.indexOf(char) == -1)
            return false;   
         });
});
$(function(){
    $('.faculty-title').on('click', function(){
        $this = $(this);
        // console.log ( $this.find('i'));
        if ( $this.find('i.more-less').eq(0).hasClass('fa-minus') ) {
            $this.find('i.more-less').eq(0).removeClass('fa-minus').addClass('fa-plus');
        }else{
            $this.find('i.more-less').eq(0).removeClass('fa-plus').addClass('fa-minus');
        }
    })
});
$(document).ready(function() {
    var date = new Date($('input[name=server_date]').val());
    // console.log(date);
    var currDate = new Date($('input[name=server_date]').val());
    date.setFullYear( date.getFullYear() - 10 );
    $('.fltpckr').flatpickr({
        maxDate: date,
        // clickOpens: false
    });
    $('.fltpckr1').flatpickr({
        maxDate: currDate,
        // clickOpens: false
    });
    $('.datepicker').datepicker({
        autoclose: true,
        endDate: '0'
    });
    // $('.endYear').datepicker({
    //     endDate:'-10y'
    // });
    $(".datepickerz").datepicker({
        // format: 'mm/dd/yyyy'
    });
    // $(".datepickerz").datepicker({
    //     format: 'mm/dd/yyyy'
    // }).end().on('keypress paste', function (e) {
    //     e.preventDefault();
    //     return false;
    // });

    $("#picture").change(function (){
        //The maximum size that the uploaded file can be.
        var maxSizeMb = 4;
        //Get the file that has been selected by
        //using JQuery's selector.
        var file = $('#picture')[0].files[0];
        //Make sure that a file has been selected before
        //attempting to get its size.
        if(file !== undefined){
            //Get the size of the input file.
            var totalSize = file.size;
            //Convert bytes into MB.
            var totalSizeMb = totalSize  / Math.pow(1024,2);
            //Check to see if it is too large.
            if(totalSizeMb > maxSizeMb){
                //Create an error message to show to the user.
                var errorMsg = 'File too large. Maximum file size is ' + maxSizeMb + 'MB. Selected file is ' + totalSizeMb.toFixed(2) + 'MB';
                //Show the error.
                alert(errorMsg);
                //Return FALSE.
                $('#picture').val('');
                return false;
            }
 
        }


        if (typeof (FileReader) != "undefined") {

            var image_holder = $("#ghost");
            image_holder.empty();

            var reader = new FileReader();
            reader.onload = function (e) {
                $("<img />", {
                    "src": e.target.result,
                    "class": "img-thum",
                    'id':'pic1'
                }).appendTo(image_holder);

            }
            image_holder.show();
            reader.readAsDataURL($(this)[0].files[0]);
        } else {
            alert("This browser does not support FileReader.");
        }
      });
    

    $('.expand_all').on('click', function () {
        $('.expand_all').removeClass('fa-plus');
        $('.expand_all').removeClass('fa-minus');
        console.log($(".fa-minus").length);
        if ($(".fa-minus").length > 0) {
            console.log('minus');
            $('.fa-minus').removeClass('fa-minus').addClass('fa-plus');
            $('.cls-faculty').collapse('hide');
        }else{
            console.log('plus');
            $('.fa-plus').removeClass('fa-plus').addClass('fa-minus');
            $('.cls-faculty').collapse('show');
        }
    });
    $("#cnic").keyup(function () {
        $("#cnic").val($(this).val().toLowerCase().replace(/[^a-zA-Z0-9]+/g, ""));
    });
    // $(".ext-hide").keyup(function () {
    //     // console.log($(this));
    //     $(this).val($(this).val().replace(/^\s*\s*$/, ''));
    // });
    $("#email").keyup(function(){
        var email = $("#email").val();
        var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        if (!filter.test(email)) {
          //alert('Please provide a valid email address');
          $("#error_email").text("Email is not valid");
          email.focus;
          //return false;
       } else {
           $("#error_email").text("");
       }
    }); 
    $('.pre_student').on('ifClicked', function(event){
        if(this.value == 1){
            $("#reg_no").show();
        }else{
            $("#reg_no").val('');
            $("#reg_no").hide();
        }
    });
    // previousqualification starts
    var url = $(location).attr('href'),
    parts = url.split("/"),
    last_part = parts[parts.length-1];
    second_last_part = parts[parts.length-2];
    // console.log(last_part+' = '+second_last_part);
    if(last_part == 'previousqualification' || second_last_part == 'previousqualification'){
        forOLevel();
        if ($('.hssc_result_waiting').is(':checked')){
            result_waiting_check();
        }
        if ($('.combined_checkbox').is(':checked')){
            combined_checkbox_check();
        }
        if(!($('.hssc_result_waiting').is(':checked')) && !($('.combined_checkbox').is(':checked'))){
            combined_checkbox_uncheck();
        }
        if($('#SSC_Title_t').val() == ''){
            $("#SSC_Title_t").attr("required", false);
            $("#SSC_Title_d").attr("required", true);
            $("#SSC_Title_d").show();
        }else{
            $("#SSC_Title_t").attr("required", true);
            $("#SSC_Title_t").show();
            $("#SSC_Title_d").attr("required", false);
        }
        if($('#SSC_From_t').val() == ''){
            $("#SSC_From_t").attr("required", false);
            $("#SSC_From_d").attr("required", true);
            $("#SSC_From_d").show();
        }else{
            $("#SSC_From_t").attr("required", true);
            $("#SSC_From_t").show();
            $("#SSC_From_d").attr("required", false);
        }
        if($('#SSC_Subject_t').val() == ''){
            $("#SSC_Subject_t").attr("required", false);
            $("#SSC_Subject_d").attr("required", true);
            $("#SSC_Subject_d").show();
        }else{
            $("#SSC_Subject_t").attr("required", true);
            $("#SSC_Subject_t").show();
            $("#SSC_Subject_d").attr("required", false);
        }
        if($('#HSC_Title_t').val() == ''){
            $("#HSC_Title_t").attr("required", false);
            $("#HSC_Title_d").attr("required", true);
            $("#HSC_Title_d").show();
        }else{
            $("#HSC_Title_t").attr("required", true);
            $("#HSC_Title_t").show();
            $("#HSC_Title_d").attr("required", false);
        } 
        if($('#HSC_From_t').val() == ''){
            $("#HSC_From_t").attr("required", false);
            $("#HSC_From_d").attr("required", true);
            $("#HSC_From_d").show();
        }else{
            $("#HSC_From_t").attr("required", true);
            $("#HSC_From_t").show();
            $("#HSC_From_d").attr("required", false);
        }
        if($('#HSC_Subject_t').val() == ''){
            $("#HSC_Subject_t").attr("required", false);
            $("#HSC_Subject_d").attr("required", true);
            $("#HSC_Subject_d").show();
        }else{
            $("#HSC_Subject_t").attr("required", true);
            $("#HSC_Subject_t").show();
            $("#HSC_Subject_d").attr("required", false);
        }      
           

        if($('#BA_Bsc_Title_t').val() == ''){
            $("#BA_Bsc_Title_d").show();
        }else{
            $("#BA_Bsc_Title_t").show();
        }
        if($('#BA_Bsc_From_t').val() == ''){
            $("#BA_Bsc_From_d").show();
        }else{
            $("#BA_Bsc_From_t").show();
        } 

        if($('#MSc_Title_t').val() == ''){
            $("#MSc_Title_d").show();
        }else{
            $("#MSc_Title_t").show();
        }
        if($('#MSc_From_t').val() == ''){
            $("#MSc_From_d").show();
        }else{
            $("#MSc_From_t").show();
        }

        if($('#BS_Title_t').val() == ''){
            $("#BS_Title_d").show();
        }else{
            $("#BS_Title_t").show();
        }
        if($('#BS_From_t').val() == ''){
            $("#BS_From_d").show();
        }else{
            $("#BS_From_t").show();
        }

        if($('#MS_Title_t').val() == ''){
            $("#MS_Title_d").show();
        }else{
            $("#MS_Title_t").show();
        }
        if($('#MS_From_t').val() == ''){
            $("#MS_From_d").show();
        }else{
            $("#MS_From_t").show();
        }

        $('.14_div').hide();
        $('.ma_div').hide();
        $('.16_div').hide();
        $('.18_div').hide();
        if($('input[name=userLevel]').val() == 2 || $('input[name=userLevel]').val() == 3){
            f_14_div();
        }
        if($('input[name=userLevel]').val() == 4){
            $('.14_div').show();
            $('.ma_div').show();
            $('.16_div').show();
        }
        if($('input[name=userLevel]').val() == 5){
            f_18_div();
        }

    }
    // previousqualification ends
});

$(function () {
    $('.prog_check_box').on('ifChecked', function(event){
        var msg = $('#tool_prog_'+$(this).val()).attr('title');
        var title = $('.prg-name-'+$(this).val()).text();
        $('#myToolModal .modal-body').text('');
        $('#i_agree, #recentProgram').val('');
        $('#i_agree').val($(this).val());
        // Code to get program fee starts 
        $.ajax({
            method: 'post',
            url: '/frontend/getProgramFee',
            data: {
                id: $(this).val()
            },
            success: function ( response ) {
                console.log(response[0].admission_fee+response[0].university_dues+response[0].library_security);
                $('#myToolModal .modal-body').html(msg+'<div><span class="program-fee-select-program">First Semester Fee:</span> '+ (parseInt(response[0].admission_fee)+parseInt(response[0].university_dues)+parseInt(response[0].library_security)) +'</div>');
            }
        });
        // Code to hey program Fee ends 
        // $('#myToolModal .modal-body').text(msg);
        $('#myToolModal .modal-title').text('');
        $('#myToolModal .modal-title').text(title + ' Requirements');
        /* updated code */
        $('#myToolModal .modal-footer .eng-tech-prog').remove();
        if($(this).val() == 55 || $(this).val() == 56 || $(this).val() == 133 ){
            $( "#myToolModal .modal-footer" )
                .prepend( "<p class='eng-tech-prog'>I understand that B.Tech program has been renamed to BSc Engineering Technology.</p>" );
        }
        /* end updated code */
        $('#myToolModal').modal('show');
    });
    $('#myToolModal').on('hidden.bs.modal', function () {
        var checked = $('#recentProgram').val();
        var unchecked = $('#i_agree').val();
        if(checked==''){
            $(":checkbox[value="+unchecked+"]").iCheck('uncheck');
        }
    })
    $('#father_check').on('ifChecked', function(event){
        $('#father_city').val((typeof($('#permanent_city').find(":selected").val()) == 'undefined')? $('#permanent_city').val(): $('#permanent_city').find(":selected").val()).trigger('change');
        $('#father_carrier').val((typeof($('#permanent_carrier').find(":selected").val()) == 'undefined')? $('#permanent_carrier').val(): $('#permanent_carrier').find(":selected").val()).trigger('change');
        $('#father_phone').val($('#permanent_phone').val());
        $('#father_address').val($('#permanent_address').val());
        $('#father_area').val($('#permanent_area').find(":selected").val()).trigger('change');
    });
    $('#father_check').on('ifUnchecked', function(event){
        $('#father_city').val('').trigger('change');
        $('#father_carrier').val('').trigger('change');
        $('#father_phone').val('');
        $('#father_address').val('');
        $('#father_area').val('').trigger('change');
    });
    $('#mailing_check').on('ifChecked', function(event){
        console.log($('#permanent_city').find(":selected").val());
        $('#mailing_city').val((typeof($('#permanent_city').find(":selected").val()) == 'undefined')? $('#permanent_city').val(): $('#permanent_city').find(":selected").val()).trigger('change');
        $('#mailing_carrier').val((typeof($('#permanent_carrier').find(":selected").val()) == 'undefined')? $('#permanent_carrier').val(): $('#permanent_carrier').find(":selected").val()).trigger('change');
        $('#mailing_phone').val($('#permanent_phone').val());
        $('#mailing_address').val($('#permanent_address').val());
        $('#mailing_area').val($('#permanent_area').find(":selected").val()).trigger('change');
    });
    $('#mailing_check').on('ifUnchecked', function(event){
        $('#mailing_city').val('').trigger('change');
        $('#mailing_carrier').val('').trigger('change');
        $('#mailing_phone').val('');
        $('#mailing_address').val('');
        $('#mailing_area').val('').trigger('change');
    });
    // previousqualification starts
    $(".combined_checkbox").change(function() {
        if(this.checked){
            combined_checkbox_check();
        }else{
            combined_checkbox_uncheck();
        }
    });
    $(".hssc_result_waiting").change(function() {
        if(this.checked){
            result_waiting_check();
        }else{
            if(!($('.hssc_result_waiting').is(':checked')) && !($('.combined_checkbox').is(':checked'))){
                combined_checkbox_uncheck();
            }
        }
    });
    // previousqualification ends
    $('.sh-hd-ot').on('change', function(event){
        if(this.value == 'Other' || this.value == 'Other Boards' || this.value == 'Other Universities'){
            var clicked = $(this).attr('id');
            var trim_last = clicked.slice(0,-1);
            console.log(clicked);
            // $('#'+clicked).select2('destroy');
            // $('#'+clicked).hide();
            $('#'+trim_last+'t').show();
            if(clicked == 'SSC_Title_d' || clicked == 'SSC_From_d' || clicked == 'SSC_Subject_d' || clicked == 'HSC_Title_d' || clicked == 'HSC_From_d' || clicked == 'HSC_Subject_d'){
                $('#'+clicked).removeAttr("required");
                $('#'+trim_last+'t').attr("required","required");
            }
            // if($('input[name=userLevel]').val() == 5){
            //     $('#'+clicked).removeAttr("required");
            //     $('#'+trim_last+'t').attr("required","required");
            // }
            
        }else{
            // console.log('here i am');
            var clicked = $(this).attr('id');
            var trim_last = clicked.slice(0,-1);
            $('#'+trim_last+'t').hide();
            $('#'+clicked).show();
            if(clicked == 'SSC_Title_d' || clicked == 'SSC_From_d' || clicked == 'SSC_Subject_d' || clicked == 'HSC_Title_d' || clicked == 'HSC_From_d' || clicked == 'HSC_Subject_d'){
                $('#'+clicked).attr("required","required");
                $('#'+trim_last+'t').removeAttr("required");
            }else{
                $('#'+trim_last+'t').removeAttr("required");
            }
            if(clicked == 'HSC_Title_d'){
                a_level_result_awaiting();
            }
            $('#'+trim_last+'t').val('');
        }
        if(this.value == 'O-Level'){
            console.log("O level selection");
            var clicked = $(this).attr('id');
            if(clicked == 'SSC_Title_d'){
                $('#SSC_Total').val(1);
                $('#SSC_Composite').val(1);

                $('#SSC_Total').hide();
                $('#SSC_Composite').hide();

                $('.hide-grade').hide();
                $('.show-grade').show();
                $('.show-grade').attr("required","required");
                $('.for-o').show();
                $('.for-o').text('Obtained Grade');
            }

        }else{
            console.log("O level de selection");
            var clicked = $(this).attr('id');
            if(clicked == 'SSC_Title_d'){
                $('#SSC_Total, #SSC_Composite').val('');
                $('#SSC_Total, #SSC_Composite, .hide-grade').show();
                $('.show-grade').hide();
                $('.show-grade').removeAttr("required");
                $('.for-o').text('Total Marks');
            }
        }
    });
});
function forOLevel(){
    var select = document.getElementById('SSC_Title_d');
    var value = select.options[select.selectedIndex].value;
    if(value == 'O-Level'){
        $('#SSC_Total').val(1);
        $('#SSC_Composite').val(1);

        $('#SSC_Total').hide();
        $('#SSC_Composite').hide();

        $('.hide-grade').hide();
        $('.show-grade').show();
        $('.show-grade').attr("required","required");
        $('.for-o').show();
        $('.for-o').text('Obtained Grade');

    }else{
        // $('#SSC_Total, #SSC_Composite').val('');
        $('#SSC_Total, #SSC_Composite, .hide-grade').show();
        $('.show-grade').hide();
        $('.show-grade').removeAttr("required");
        $('.for-o').text('Total Marks');
    }
}
// previousqualification starts
function f_14_div(){
    $('.14_div').show();
    $('.14_div').find('label').addClass('req-rd');
    $('#ug_1').removeClass('req-rd');
    $('#resultwaitbsc').parent().parent().find('label').removeClass('req-rd');

    $(".14_div .form-group").find("select").attr("required", true);
    $(".14_div .form-group").find("input").attr("required", true);
    $('#BA_Bsc_Title_t, #BA_Bsc_From_t').attr("required", false);


    if($('#BA_Bsc_Title_t').val() == ''){
        $("#BA_Bsc_Title_d").show();
        $("#BA_Bsc_Title_d").attr('required', true);
        $("#BA_Bsc_Title_t").attr('required', false);
    }else{
        $("#BA_Bsc_Title_t").show();
        $("#BA_Bsc_Title_t").attr('required', true);
        $("#BA_Bsc_Title_d").attr('required', false);
    }
    if($('#BA_Bsc_From_t').val() == ''){
        $("#BA_Bsc_From_d").show();
        $("#BA_Bsc_From_d").attr('required', true);
        $("#BA_Bsc_From_t").attr('required', false);
    }else{
        $("#BA_Bsc_From_t").show();
        $("#BA_Bsc_From_t").attr('required', true);
        $("#BA_Bsc_From_d").attr('required', false);
    } 
}
function f_18_div(){
    $('.14_div').show();
    $('.ma_div').show();
    $('.16_div').show();
    $('.18_div').show();
    $('.18_div').find('label').addClass('req-rd');
    $('#pd_1').removeClass('req-rd');
    $(".18_div .form-group").find("select").attr("required", true);
    $(".18_div .form-group").find("input").attr("required", true);
    $('#MS_Title_t, #MS_From_t').attr("required", false);

    if($('#MS_Title_t').val() == ''){
        $("#MS_Title_d").show();
        $("#MS_Title_d").attr('required', true);
        $("#MS_Title_t").attr('required', false);
    }else{
        $("#MS_Title_t").show();
        $("#MS_Title_t").attr('required', true);
        $("#MS_Title_d").attr('required', false);
    }
    if($('#MS_From_t').val() == ''){
        $("#MS_From_d").show();
        $("#MS_From_d").attr('required', true);
        $("#MS_From_t").attr('required', false);
    }else{
        $("#MS_From_t").show();
        $("#MS_From_t").attr('required', true);
        $("#MS_From_d").attr('required', false);
    } 
}
function combined_checkbox_check(){
    $('#HSSC_Total1').val('');
    $('#HSSC1').val('');
    $('#hssc_2').text('');
    $('#HSSC_Total2').val('');
    $('#HSSC2').val('');
    $('#hssc_3').text('');
    $('.separate_hssc_marks').hide();
    $(".separate_hssc_marks .form-group").find("input").attr("required", false);
    $('.total_hssc_marks').show();
    $(".total_hssc_marks .form-group").find("input").attr("required", true);
    // $('.hssc_result_waiting').iCheck('uncheck');
    $('.hssc_result_waiting').prop('checked',false);
}
function combined_checkbox_uncheck(){
    $('#HSSC_Total').val('');
    $('#HSSC_Composite').val('');
    $('#hssc_1').text('');
    $('.total_hssc_marks').hide();
    $(".total_hssc_marks .form-group").find("input").attr("required", false);
    $('.separate_hssc_marks').show();
    $(".separate_hssc_marks .form-group").find("input").attr("required", true);
    // Dev Mannan: changes for fall-2021 in case of result waiting remove required from part 1
    $('.inresult_waiting_part_1 label').addClass('req-rd');
    // Dev Mannan: changes for fall-2021 ends
}
function result_waiting_check(){
    // $('.combined_checkbox').iCheck('uncheck');
    $('.combined_checkbox').prop('checked',false);
    combined_checkbox_uncheck();
    a_level_result_awaiting();
    $('#HSSC_Total2').val('');
    $('#HSSC2').val('');
    $('#hssc_2').text('');
    $('.inresult_waiting').hide();
    // Dev Mannan: changes for fall-2021 in case of result waiting remove required from part 1
    $(".inresult_waiting_part_1 .form-group").find("input").attr("required", false);
    $('.inresult_waiting_part_1 label').removeClass('req-rd');
    // Dev Mannan: changes for fall-2021 ends
    $(".inresult_waiting .form-group").find("input").attr("required", false);
    $("#resultwaitbsc").iCheck('uncheck');
   
}
function a_level_result_awaiting(){
    if($('#HSC_Title_d').find(":selected").val() == 'A-Level' && $('.hssc_result_waiting').prop("checked") == true){
        console.log('A-level check is coming');
        $('#HSSC_Total1').val('');
        $('#HSSC1').val('');
        $('#hssc_2').text('');
        $('#HSSC_Total2').val('');
        $('#HSSC2').val('');
        $('#hssc_3').text('');
        $('.separate_hssc_marks').hide();
        $('.separate_hssc_marks').hide();
        $(".separate_hssc_marks .form-group").find("input").attr("required", false);
        $('#HSSC_Total').val('');
        $('#HSSC_Composite').val('');
        $('#hssc_1').text('');
        $('.total_hssc_marks').hide();
        $(".total_hssc_marks .form-group").find("input").attr("required", false);
    }else{
        // if($('#HSC_Title_d').find(":selected").val() != 'A-Level' || $('.hssc_result_waiting').prop("checked") == true){
        //     combined_checkbox_uncheck();
        // }
        // $('.combined_checkbox').prop('checked',true);
        // combined_checkbox_check();
    }
}
    $('#resultwaitbsc').on('ifChecked', function(event){
        // special changes start
        // comment Changes for covid-19 fall 2020
        // $("#BA_Bsc_Total").attr('required', false);
        // $("#BA_Bsc").attr('required', false);
        // $('.rm-aster label').removeClass('req-rd');
        // special changes ends
        $('.hssc_result_waiting').prop('checked',false);
    });
    $('#resultwaitbsc').on('ifUnchecked', function(event){
        // special changes start
        // comment Changes for covid-19 fall 2020
        // $("#BA_Bsc_Total").attr('required', true);
        // $("#BA_Bsc").attr('required', true);
        // $('.rm-aster label').addClass('req-rd');
        // special changes ends
    });
// previousqualification ends
function isNumberKey(evt){
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode != 46 && charCode > 31 
    && (charCode < 48 || charCode > 57))
    return false;
    return true;
} 
$( "#i_agree" ).click(function() {
    // alert();
    $('#recentProgram').val($(this).val());
});
$('#aptitudeForm').submit(function(){
    var ob_1 = parseInt($('#obtained_1').val());
    var to_1 = parseInt($('#total_1').val());
    var ob_2 = parseInt($('#obtained_2').val());
    var to_2 = parseInt($('#total_2').val());
    var ob_3 = parseInt($('#obtained_3').val());
    var to_3 = parseInt($('#total_3').val());
    var ob_4 = parseInt($('#obtained_4').val());
    var to_4 = parseInt($('#total_4').val());
    if (ob_1 > 0 || to_1 > 0) {
       if(ob_1 > to_1 || (isNaN(ob_1) || isNaN(to_1))){
           alert('Obtained marks should be less than total marks');
           return false;
       }
    }
    if (ob_2 > 0 || to_2 > 0) {
        if(ob_2 > to_2 || (isNaN(ob_2) || isNaN(to_2))){
            alert('Obtained marks should be less than total marks');
            return false;
        }
     }
     if (ob_3 > 0 || to_3 > 0) {
        if(ob_3 > to_3 || (isNaN(ob_3) || isNaN(to_3))){
            alert('Obtained marks should be less than total marks');
            return false;
        }
     }
     if (ob_4 > 0 || to_4 > 0) {
        if(ob_4 > to_4 || (isNaN(ob_4) || isNaN(to_4))){
            alert('Obtained marks should be less than total marks');
            return false;
        }
        // console.log(ob_1+'='+to_4);
        // return false;
     }
});
function checkMarks(total_id, obtained_id, percent){
    var total_field = total_id;
    var total_id = parseInt($('#'+total_id).val());
    var less = obtained_id;
    var obtained_id = parseInt($('#'+obtained_id).val());
    if (total_id < 1 || obtained_id < 1) {
        alert("Please Enter valid marks");
        $('#'+total_field).val('');
        $('#'+less).val('');
        $('#'+percent).text('');
        return false;
    }
    if (total_id > 0 || obtained_id > 0) {
        if(isNaN(total_id) || isNaN(obtained_id)){

        }else{
            if(obtained_id > total_id ){
                alert('Obtained marks should be less than total marks');
                $('#'+less).val('');
                $('#'+percent).text('');
                return false;
            }else{
                var perc = ((obtained_id/total_id) * 100);
                // perc = Math.floor(perc);
                perc = perc.toFixedNoRounding(2);
                // console.log(obtained_id+ '='+total_id+'='+ perc);
                $('#'+percent).text(perc+' %');
                if(perc > 49){
                    $( '#'+percent ).removeClass('not-ok').addClass( 'ok' );
                }else{
                    $( '#'+percent ).removeClass('ok').addClass( 'not-ok' );
                }
            }
        }
     }
}

Number.prototype.toFixedNoRounding = function(n) {
    const reg = new RegExp("^-?\\d+(?:\\.\\d{0," + n + "})?", "g")
    const a = this.toString().match(reg)[0];
    const dot = a.indexOf(".");
    if (dot === -1) { // integer, insert decimal dot and pad up zeros
        return a + "." + "0".repeat(n);
    }
    const b = n - (a.length - dot) + 1;
    return b > 0 ? (a + "0".repeat(b)) : a;
 }

$(window).on('load',function(){
    var url = $(location).attr('href'),
    parts = url.split("/"),
    last_part = parts[parts.length-1];
    second_last_part = parts[parts.length-2];
    if(last_part == 'personaldetail' || second_last_part == 'personaldetail'){
        $('#admissionReq').modal('show');
    }
});


// code for loading icon
document.onreadystatechange = function () {
    var state = document.readyState
    if (state == 'interactive') {
         document.getElementById('for-loading').style.visibility="hidden";
    } else if (state == 'complete') {
        setTimeout(function(){
           document.getElementById('interactive');
           document.getElementById('load').style.visibility="hidden";
           document.getElementById('for-loading').style.visibility="visible";
        },1000);
    }
  }


$('#previous-qualification-btn').click(function() {
    $("#confirm-submit").modal("show");
     return false;
});

$('#qualification-submit').click(function(){
    $('.previousqualificationForm').submit();
});

function groupingSubmit() {
    $('#save-detail-btn').attr("disabled", "disabled");
}
