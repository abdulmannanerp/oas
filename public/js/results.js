

$( document ).ready(function() {
    $('.expand_all').on('click', function () {
        $('.expand_all').removeClass('fa-plus');
        $('.expand_all').removeClass('fa-minus');
        console.log($(".fa-minus").length);
        if ($(".fa-minus").length > 0) {
            console.log('minus');
            $('.fa-minus').removeClass('fa-minus').addClass('fa-plus');
            $('.cls-faculty').collapse('hide');
        }else{
            console.log('plus');
            $('.fa-plus').removeClass('fa-plus').addClass('fa-minus');
            $('.cls-faculty').collapse('show');
        }
    });
    $("#email, #re_email").keyup(function(){
        var email = $("#email").val();
        var re_email = $("#re_email").val();
        // var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        // var filter = /^\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i
        var filter = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if (!filter.test(email)) {
          $("#error_email").text("Email is not valid");
          email.focus;
          $('#signup-btn').attr('disabled', true);
       } else {
           console.log('valid');
            $("#error_email").text("");
            $('#signup-btn').attr('disabled', false);
            if(email != '' && re_email != ''){
                if (email != re_email){
                    $("#error_re_email").text("Emails do not match!");
                    $('#signup-btn').attr('disabled', true);
                }else{
                    $("#error_re_email").text("");
                    $("#error_email").text("");
                    $('#signup-btn').attr('disabled', false);
                }
            }
       }
    }); 
    $("#cnic").keyup(function () {
        $("#cnic").val($(this).val().toLowerCase().replace(/[^a-zA-Z0-9]+/g, ""));
    }); 
    $("#repassword, #password").keyup(function () {
        var password = $("#password").val();
        var confirmPassword = $("#repassword").val();
        if(password != '' && confirmPassword != ''){
            if (password != confirmPassword){
                $("#error_password").text("Passwords do not match!");
                $('#signup-btn').attr('disabled', true);
            }else{
                $("#error_password").text("");
                $('#signup-btn').attr('disabled', false);
            }
        }
    }); 
});

$(function () {
    $( "#name" ).keypress(function(e) {
        var arr = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz ";
        var code;
        if (window.event)
            code = e.keyCode;
        else
            code = e.which;
        var char = keychar = String.fromCharCode(code);
        if (arr.indexOf(char) == -1)
            return false;   
    });
});

function isNumberKey(evt){
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode != 46 && charCode > 31 
    && (charCode < 48 || charCode > 57))
    return false;
    return true;
} 
function cnicData(resultId = '',  rollNo = '', docType = ''){
    var getUrl = window.location;
    $('.cnicClass').attr('action', '');
    $('.cnicClass').attr('action', window.location.protocol + "//" + window.location.host + "/get-results-docs/" +resultId+ "/" +rollNo+ "/"+ docType);
}
$('.cnicClass').submit(function() {
    var cnic = $('.cnicUnique').val();
    $('.cnicClass').attr("action", $('.cnicClass').attr("action") +"/"+ cnic);
    // alert(cnic); 2870 nazia
    // return false;
    // alert('submit');
}); 

