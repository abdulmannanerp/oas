$(document).ready(function () {
    $('#entries').change(function (e) {
        $("#appSearching").submit();
    });
    $('.btn-verify').click(function (e) {
        $('.verify-it').attr('id', $(this).attr('id'));
        $('.reject-it').attr('id', $(this).attr('id'));
    });
    $('.verify-it, .reject-it').click(function (e) {
        var id = $(this).attr('id');
        (".verify-it, .reject-it").prop('disabled', true);
        var selection, comments = ''
        if ($(this).hasClass('reject-it')) {
            selection = 'reject';
            if ($.trim($('#comment').val()) == '') {
                alert('comments must be entered');
                return false;
            } else {
                comments = $('#comment').val();
            }
        } else {
            selection = 'verify';
        }
        $.ajax({
            type: "POST",
            url: '/applicantverifreject',
            data: {id: id, selection: selection, comments: comments},
            success: function (response) {
                console.log(response);
                $(".verified-" + id).empty();
                $(".status-" + id).empty();
                if (response == 'verify') {
//                    $(".verified-" + id).html('<strong class="verified">Verified!</strong>');
                    $(".status-" + id).html('Approved');
                } else if (response == 'reject') {
//                    $(".verified-" + id).html('<span class="clr-rej">Rejected: </span>' + comments + '');
                    $(".status-" + id).html('Rejected');
                } else if (response == 'verify-1') {
                    $(".status-" + id).html('Approved');
                    $(".verified-" + id).html('<div class="sbox-tools  clearfix cus-div"><button name="save" id="' + id + '" class="tips btn btn-sm btn-save btn-verify" data-toggle="modal" data-target="#verifyModal" title="Back">Verify/Reject </button></div>');
                } else if (response == 'reject-1') {
                    $(".status-" + id).html('Rejected');
                    $(".verified-" + id).html('<div class="sbox-tools  clearfix cus-div"><button name="save" id="' + id + '" class="tips btn btn-sm btn-save btn-verify" data-toggle="modal" data-target="#verifyModal" title="Back">Verify/Reject </button></div>');
                }
                $('#verifyModal').modal('hide');
            }
        });
    });
});