
		//function to parse db count
		function getParsedCount(data,id){
			var parsedData = JSON.parse(data);
			var	count = 0;
			$.each( parsedData , function ( key, value ) {
				if(id == parseInt(value.code))
					count =  parseInt(value.value) 
			});
			return count;
		}

/*
TODO:
- Check data labels after drilling. Label rank? New positions?
*/

var mapdata = Highcharts.geojson(Highcharts.maps['countries/pk/pk-all']),
    separators = Highcharts.geojson(Highcharts.maps['countries/pk/pk-all'], 'mapline'),
   
   // Some responsiveness
    small = $('#container').width() < 400;
	
	/* Set drilldown pointers
	$.each(mapdata, function (i) {
		this.drilldown = this.properties['who_prov_c'];
		this.value = i; // Non-random bogus data
	});*/
	
$.get('index.php?id=all', function (dbdata) {
	
	// Set drilldown pointers and count values
    $.each(mapdata, function (i) {
		this.drilldown = this.properties['who_prov_c'];
		this.value = getParsedCount(dbdata,this.properties['who_prov_c'])
	});

    // Instantiate the map
    Highcharts.mapChart('container', {

		chart: {
			events: {
				drilldown: function (e) {
					if (!e.seriesOptions) {
						var chart = this,
							mapKey = e.point.drilldown,
							// Handle error, the timeout is cleared on success
							fail = setTimeout(function () {
								if (!Highcharts.maps[mapKey]) {
									chart.showLoading('<i class="icon-frown"></i> Failed loading ' + e.point.name);
									fail = setTimeout(function () {
										chart.hideLoading();
									}, 1000);
								}
							}, 3000);

						// Show the spinner
						chart.showLoading('<i class="icon-spinner icon-spin icon-3x"></i>'); // Font Awesome spinner

						// Load the drilldown map
						$.getScript( mapKey + '.js', function () {

							data = Highcharts.geojson(Highcharts.maps[mapKey]);
							
							$.get('index.php?id='+mapKey, function (dbdata) {

							// Set a count value
							$.each(data, function (i) {
								this.value = getParsedCount(dbdata,this.properties['who_dist_c'])			
							});

							// Hide loading and add series
							chart.hideLoading();
							clearTimeout(fail);
							chart.addSeriesAsDrilldown(e.point, {
								name: e.point.properties['who_prov_n'],
								data: data,
								dataLabels: {
									enabled: true,
									format: '{point.properties.map_label}'
								},
								tooltip: {
									pointFormat: '{point.properties.who_dist_n}<br> Students Applied: {point.value}'
								}
							});
							});
						});
					}
					console.log(e.point.properties['who_prov_n']);
					this.setTitle(null, { text: e.point.properties['who_prov_n'] });
				},
				drillup: function () {
					this.setTitle(null, { text: 'Pakistan' });
				}
			}
		},

        title: {
            text: 'Applicant'
        },
		
		subtitle: {
			text: 'Pakistan',
			floating: true,
			align: 'right',
			y: 50,
			style: {
				fontSize: '16px'
			}
		},

		legend: small ? {} : {
			layout: 'vertical',
			align: 'right',
			verticalAlign: 'middle'
		},

		colorAxis: {
			min: 0,
			minColor: '#E6E7E8',
			maxColor: '#005645'
		},

		mapNavigation: {
			enabled: true,
			buttonOptions: {
				verticalAlign: 'bottom'
			}
		},

		plotOptions: {
			map: {
				states: {
					hover: {
						color: '#EEDD66'
					}
				}
			}
		},
		
		series: [{
			data: mapdata,
			name: 'PAK',
			dataLabels: {
				enabled: true,
				format: '{point.properties.map_label}'
			},
			tooltip: {
				pointFormat: '{point.properties.name}<br> Students Applied: {point.value}'
			}	
			
		}, {
			type: 'mapline',
			data: separators,
			color: 'silver',
			enableMouseTracking: false,
			animation: {
				duration: 500
			}
		}],
		
		drilldown: {
			activeDataLabelStyle: {
				color: '#FFFFFF',
				textDecoration: 'none',
				textOutline: '1px #000000'
			},
			drillUpButton: {
				relativeTo: 'spacingBox',
				position: {
					x: 0,
					y: 60
				}
			}
		}
		
    });
});
