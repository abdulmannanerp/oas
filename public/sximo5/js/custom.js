$(document).ready(function () {

    // $('#printbTN').click(function () {
    //     html2canvas(document.querySelector(".printable")).then(canvas => {  
    //         var dataURL = canvas.toDataURL();
    //         var width = canvas.width;
    //         var printWindow = window.open("");
    //         $(printWindow.document.body)
    //           .html("<img id='Image' src=" + dataURL + " style='" + width + "'></img>")
    //           .ready(function() {
    //           printWindow.focus();
    //           printWindow.print();
    //         });
    //       });
    // });

    $('#submit').click(function () {
        /* when the submit button in the modal is clicked, submit the form */
        //    alert('submitting');
        $('#formfield').submit();
    });
    $('#entries').change(function (e) {
        $("#appSearching").submit();
    });
    $(document).on("click", ".btn-verify", function () {
        $('.verify-it').attr('id', $(this).attr('id'));
        $('.reject-it').attr('id', $(this).attr('id'));
        $(".verify-it, .reject-it").prop('disabled', false);
    });

    $('.super-click').click(function (e) {
        var id = $(this).attr('id');
        $.ajax({
            type: "POST",
            url: '/admin/superadmin',
            data: {
                id: id
            },
            success: function (response) {
                console.log(response);

                $(".admin-details").html('');
                $(".admin-details").html('<div class="squeeze"><b>Creation Date: </b>' + response['application']['created_at'] + '<br /><b>Fee Submitted Date: </b>' + response['application']['feeSubmitted'] + '<br /><b>Fee Verified Date: </b>' + response['application']['feeVerified'] + '<br /><b>Fee Verified By: </b>' + response['fee_verified_by'] + '<br /><b>Documents Verified Date: </b>' + response['application']['doumentsVerified'] + '<br /><b>Documents Verified By: </b>' + response['documents_verified_by'] + '<br /> <b>Application Modified By: </b>' + response['application']['fkModifiedBy'] + '</div>');
                $.each(response['logs'], function (key, value) {
                    $(".admin-details").append('<div class="squeeze"><b>Creation Date & Time: </b>' + value.createdDtm + '<br><b>fkApplicationID: </b>' + value.fkApplicationID + '<br><b>id: </b>' + value.id + '<br>');
                    parsedTest = JSON.parse(value.sessionData);
                    if (("AdminUserId" in parsedTest)) {
                        // console.log(parsedTest.AdminUserId);
                        $.ajax({
                            type: "POST",
                            url: '/admin/superadminid',
                            async: false,
                            data: {
                                id: parsedTest.AdminUserId
                            },
                            success: function (response) {
                                console.log(response.username);
                                if(response.username){
                                    $(".squeeze:last-child").append('<b>Admin Username: </b>' + response.username + '<br>');
                                }
                            }
                        });
                    }
                    $(".squeeze:last-child").append('<b>Page: </b>' + value.userAgent + '<br><b>userId: </b>' + value.userId + '<br></div>');

                });
                $('#superadminModal').modal('show');
            }
        });
    });

    // Admin Login to frontend
    $('.md-fy').click(function (e) {
        var id = $(this).attr('id');
        id = APP_URL + '/frontend/admin-login/' + id;
        var windowObjectReference;
        var strWindowFeatures = 'menubar=yes,location=yes,resizable=yes,scrollbars=yes,status=yes';
        windowObjectReference = window.open(id, 'form_edit', strWindowFeatures);
    });

    $('.verify-it, .reject-it').click(function (e) {
        var id = $(this).attr('id');
        var selection, comments = ''
        $(".verify-it, .reject-it").prop('disabled', true);
        if ($(this).hasClass('reject-it')) {
            selection = 'reject';
            if ($.trim($('#comment').val()) == '') {
                alert('comments must be entered');
                $(".verify-it, .reject-it").prop('disabled', false);
                return false;
            } else {
                comments = $('#comment').val();
            }
        } else {
            selection = 'verify';
        }
        $.ajax({
            type: "POST",
            url: '/admin/applicantverifreject',
            data: {
                id: id,
                selection: selection,
                comments: comments
            },
            success: function (response) {
                console.log(response);
                $(".verified-" + id).empty();
                $(".status-" + id).empty();
                if (response == 'verify') {
                    //                    $(".verified-" + id).html('<strong class="verified">Verified!</strong>');
                    $(".status-" + id).html('<span class="Approved">Approved</span>');
                } else if (response == 'reject') {
                    //                    $(".verified-" + id).html('<span class="clr-rej">Rejected: </span>' + comments + '');
                    $(".status-" + id).html('<span class="Rejected">Rejected</span>');
                } else if (response == 'verify-1') {
                    $(".status-" + id).html('<span class="Approved">Approved</span>');
                    $(".verified-" + id).html('<div class="sbox-tools  clearfix cus-div flt-none"><button name="save" id="' + id + '" class="tips btn btn-sm btn-save btn-verify" data-toggle="modal" data-target="#verifyModal" title="Back">Re-Verify </button></div>');
                } else if (response == 'reject-1') {
                    $(".status-" + id).html('<span class="Rejected">Rejected</span>');
                    $(".verified-" + id).html('<div class="sbox-tools  clearfix cus-div flt-none"><button name="save" id="' + id + '" class="tips btn btn-sm btn-save btn-verify" data-toggle="modal" data-target="#verifyModal" title="Back">Re-Verify </button></div>');
                }
                alert(id + ' has been ' + selection + ' successfully');
                $('#verifyModal').modal('hide');
            }
        });
    });

    /* Document Verification Rewerite */
    $('.docs-verification-btn').on('click', function(){
        let $this = $(this),
            formId = $this.data('form');
        $('#applicationIdModalField').val(formId);
        $('#verificaiton-modal').modal('show');
    });

    $('.modalVerifyRejctBtn').on('click', function(){
        event.preventDefault();
        let $this = $(this);
        if ( $this.text() == 'Reject' && $('#docs-verify-reject-comment').val() == '' ) {
            alert('Comments must be entered in case of rejection' );
            return;
        }
        $.ajax({
            url: $(this).data('url'),
            method: 'post',
            data: {
                id: $('#applicationIdModalField').val(),
                comments: $('#docs-verify-reject-comment').val(),
                type: $this.text()
            },
            success: function( response ) {
                $('#docs-verify-reject-comment').val('');
                response = JSON.parse( response );
                $('button[data-form='+response.id+']').parent().prev().text(response.message);;
            }
        });
        $('#verificaiton-modal').modal('hide');
    });
    /* End Document Verification Rewerite */

    $('#semesterSelector').on('change', function(){
        $this = $(this);
        if ( $this.val() !== '' )
            $('#semesterSelectorForm').submit();
    });
});

jQuery(function ($) {
    'use strict';
    $('.print-link').click(function (e) {
        var d = new Date($.now());
        $(".page-content-wrapper").print({
            globalStyles: true,
            mediaPrint : false,
            noPrintSelector : ".print-hiding",
            stylesheet : "../sximo5/css/dashboard-print.css",
            // append: "<h6 style='text-align: left; color: #1ab394 !important;'> Print Date: "+d.getDate()+"-"+(d.getMonth()+1)+"-"+d.getFullYear()+" "+d.getHours()+":"+d.getMinutes()+":"+d.getSeconds()+"</h6>",
            prepend: "<h6 style='text-align: left; color: #1ab394 !important;'> Print Date: "+d.getDate()+"-"+(d.getMonth()+1)+"-"+d.getFullYear()+" "+d.getHours()+":"+d.getMinutes()+":"+d.getSeconds()+"</h6>",

        });
    });

});
jQuery(function ($) {
    'use strict';
    $('.print-dashboard').click(function (e) {
        var d = new Date($.now());
        $(".page-content-wrapper").print({
            globalStyles: true,
            mediaPrint : false,
            noPrintSelector : ".print-hiding",
            stylesheet : "../sximo5/css/dashboard-print.css",
            // append: "<h6 style='text-align: left; color: #1ab394 !important;'> Print Date: "+d.getDate()+"-"+(d.getMonth()+1)+"-"+d.getFullYear()+" "+d.getHours()+":"+d.getMinutes()+":"+d.getSeconds()+"</h6>",
            prepend: "<h6 style='text-align: left; color: #1ab394 !important;'> Print Date: "+d.getDate()+"-"+(d.getMonth()+1)+"-"+d.getFullYear()+" "+d.getHours()+":"+d.getMinutes()+":"+d.getSeconds()+"</h6>",

        });
    });

});
jQuery(function ($) {
    'use strict';
    $('.program-print').click(function (e) {
        var d = new Date($.now());
        $(".page-content-wrapper").print({
            globalStyles: true,
            mediaPrint : false,
            noPrintSelector : ".print-hiding",
            stylesheet : "../sximo5/css/dashboard-print.css",
            // append: "<h6 style='text-align: left; color: #1ab394 !important;'> Print Date: "+d.getDate()+"-"+(d.getMonth()+1)+"-"+d.getFullYear()+" "+d.getHours()+":"+d.getMinutes()+":"+d.getSeconds()+"</h6>",
            prepend: "<h6 style='text-align: left; color: #1ab394 !important;'> Print Date: "+d.getDate()+"-"+(d.getMonth()+1)+"-"+d.getFullYear()+" "+d.getHours()+":"+d.getMinutes()+":"+d.getSeconds()+"</h6>",

        });
    });

});
jQuery(function ($) {
    'use strict';
    $('.department-print').click(function (e) {
        var d = new Date($.now());
        $(".page-content-wrapper").print({
            globalStyles: true,
            mediaPrint : false,
            noPrintSelector : ".print-hiding",
            stylesheet : "../sximo5/css/dashboard-print.css",
            // append: "<h6 style='text-align: left; color: #1ab394 !important;'> Print Date: "+d.getDate()+"-"+(d.getMonth()+1)+"-"+d.getFullYear()+" "+d.getHours()+":"+d.getMinutes()+":"+d.getSeconds()+"</h6>",
            prepend: "<h6 style='text-align: left; color: #1ab394 !important;'> Print Date: "+d.getDate()+"-"+(d.getMonth()+1)+"-"+d.getFullYear()+" "+d.getHours()+":"+d.getMinutes()+":"+d.getSeconds()+"</h6>",

        });
    });

});

jQuery(function ($) {
    'use strict';
    $('.print-dashboard-color').click(function (e) {
        var d = new Date($.now());
        $(".page-content-wrapper").print({
            globalStyles: true,
            mediaPrint : false,
            noPrintSelector : ".print-hiding",
            stylesheet : "../sximo5/css/dashboard-print-color.css",
            // append: "<h6 style='text-align: left; color: #1ab394 !important;'> Print Date: "+d.getDate()+"-"+(d.getMonth()+1)+"-"+d.getFullYear()+" "+d.getHours()+":"+d.getMinutes()+":"+d.getSeconds()+"</h6>",
            prepend: "<h6 style='text-align: left; color: #1ab394 !important;'> Print Date: "+d.getDate()+"-"+(d.getMonth()+1)+"-"+d.getFullYear()+" "+d.getHours()+":"+d.getMinutes()+":"+d.getSeconds()+"</h6>",

        });
    });

});
jQuery(function ($) {
    'use strict';
    $('.program-print-color').click(function (e) {
        var d = new Date($.now());
        $(".page-content-wrapper").print({
            globalStyles: true,
            mediaPrint : false,
            noPrintSelector : ".print-hiding",
            stylesheet : "../sximo5/css/dashboard-print-color.css",
            // append: "<h6 style='text-align: left; color: #1ab394 !important;'> Print Date: "+d.getDate()+"-"+(d.getMonth()+1)+"-"+d.getFullYear()+" "+d.getHours()+":"+d.getMinutes()+":"+d.getSeconds()+"</h6>",
            prepend: "<h6 style='text-align: left; color: #1ab394 !important;'> Print Date: "+d.getDate()+"-"+(d.getMonth()+1)+"-"+d.getFullYear()+" "+d.getHours()+":"+d.getMinutes()+":"+d.getSeconds()+"</h6>",

        });
    });

});

function printIt(){
    html2canvas(document.querySelector(".printable")).then(canvas => {  
        var dataURL = canvas.toDataURL();
        var width = canvas.width;
        var printWindow = window.open("");
        $(printWindow.document.body)
          .html("<img id='Image' src=" + dataURL + " style='" + width + "'></img>")
          .ready(function() {
          printWindow.focus();
          printWindow.print();
        });
      });
}


