<?php
/* Frontend Routes */
//Route::match(['get','post'], '/', 'Frontend\SignUpController@index')->name('signup');
Route::get('/', function(){
    return Redirect::to('https://cms.iiu.edu.pk');
});

// Route::match(['get','post'], '/', 'ResultController@show')->name('displayResults');


Route::match(['get','post'], '/AdminRoute55@66', 'Frontend\SignUpController@index')->name('signupAdminRoute84@3');

Route::prefix('frontend')->group(function(){
    Route::middleware(['auth:applicant'])->group(function(){
        
         /* ScholarshipPortal Route by Haider Ali */
        Route::match(['get','post'], '/scholarship','Frontend\ScholarshipDetailController@index')->name('scholarship');

        Route::match(['get','post'], '/progressDetail','Frontend\ProgressDetailController@index')->name('progressDetail');
        
        Route::match(['get','post'], '/qard-e-hasnah','Frontend\QardehasnahController@index')->name('qard-e-hasnah');
        /* End Route by Haider Ali */
        
        
        Route::match(['get','post'], '/personaldetail/{admin?}', 'Frontend\PersonalDetailController@index')->name('personaldetail');
        // Route::match(['get','post'], '/addressdetail/{admin?}', 'Frontend\AddressDetailController@index')->name('addressdetail');
        Route::match(['get','post'], '/languageproficency/{admin?}', 'Frontend\LanguageProficencyController@index')->name('languageproficency');
        Route::match(['get','post'], '/aptitudetest/{admin?}', 'Frontend\AptitudeTestController@index')->name('aptitudetest');
        Route::match(['get','post'], '/familydetail/{admin?}', 'Frontend\FamilyDetailController@index')->name('familydetail');
        Route::match(['get','post'], '/otherdetail/{admin?}', 'Frontend\OtherDetailController@index')->name('otherdetail');
        Route::match(['get','post'], '/savedetail/{admin?}', 'Frontend\SaveDetailController@index')->name('savedetail');
        Route::match(['get','post'], '/previousqualification/{admin?}', 'Frontend\PreviousQualificationController@index')->name('previousqualification');
        Route::match(['get','post'], '/selectprogram/{admin?}', 'Frontend\SelectProgramController@index')->name('selectprogram'); 

        Route::match(['get','post'],'/programGrouping/{admin?}', 'Frontend\SelectProgramController@programGrouping')->name('programGrouping');;
        // Preference Route
        Route::match(['get','post'], '/selectpreference/{admin?}', 'Frontend\SelectPreferenceController@index')->name('selectpreference'); 
        Route::get('deleteApplication/{id?}', 'Frontend\SelectProgramController@deleteApplication')->name('deleteApplication');

        Route::get('reApply/{id?}', 'Frontend\SelectProgramController@reApply')->name('reApply');

        Route::match(['get','post'], '/updatepassword/{admin?}', 'Frontend\UpdatePasswordController@index')->name('updatepassword');                
        Route::match(['get','post'], '/application/{admin?}', 'Frontend\ApplicationController@index')->name('application');

        Route::match(['get','post'], '/applicationStatus/{admin?}', 'Frontend\ApplicationController@statusUpdate')->name('applicationStatus');

        Route::get('/fee-challan/{applicationid}/{semesterid}/{programid}', 'Frontend\ApplicationController@getFeeChallan')->name('feeChallan');

        Route::get('/fee-challan-pkr/{applicationid}/{semesterid}/{programid}', 'Frontend\ApplicationController@getFeeChallanPkr')->name('feeChallanPkr');
        // print application form route
        Route::match(['get', 'post'], 'applicationForm/{oas_app_id}', 'Application\ApplicationController@printApplication')->name('applicationForm');
        // print roll number slip route
        Route::match(['get', 'post'], 'rollNumberSlip/{oas_app_id}', 'Application\ApplicationController@printRollNumber')->name('rollNumberSlip');
        /** Ajax Requests */
        Route::post('/getDistrict/{id?}', 'Frontend\PersonalDetailController@getDistrict')->name('getDistrict');
        Route::post('/getProgramFee/{id?}', 'Frontend\SelectProgramController@getProgramFee')->name('getProgramFee');

        // Print Joining Performa
        Route::match(['get', 'post'], 'joiningPerforma/{oas_app_id}/{rollno}', 'Frontend\ApplicationController@printJoiningPerforma')->name('joiningPerforma');

    });
    Route::match(['get','post'], '/contactus', 'FrontController@contactus')->name('contactus');

    
    Route::match(['get','post'], '/faq', 'FrontController@faq')->name('faq');
    Route::get('/admin-login/{hash}','ApplicantAuth\LoginController@adminAuth')->name('adminFrontendAuth');
    /* Authentication */
    Route::post('/login', 'ApplicantAuth\LoginController@applicantLogin')->name('applicantLogin');
    Route::post('/resetPassword', 'Frontend\ApplicationController@resetPassword')->name('resetPassword');
    Route::post('/logout', 'ApplicantAuth\LoginController@applicantLogout')->name('applicantLogout');
    /*Scholarship Login Authentication by Haider Ali*/
    Route::post('/logins', 'ApplicantAuth\SchLoginController@schapplicantLogin')->name('schapplicantLogin');
    /*End route*/
});
Route::get('/faq', function(){
    return redirect()->route('faq');
});
