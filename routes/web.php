<?php
use Illuminate\Support\Facades\Auth;

/* Load frontend routes */
require_once('frontend/frontend.php');
Route::get('/admin', 'HomeController@index');
Route::get('/admin/public/', function () {
    return redirect('/admin');
});


 /*scholarshipSearch route by Haider Ali*/
Route::match(['get', 'post'], '/admin/scholarships/{semester?}', 'SearchScholarshipsController@index')->name('scholarships');

//Route::match(['get', 'post'], '/admin/scholarships/{semester?}', 'SearchSchController@index')->name('searchsch');

 /*scholarshipSearch_new route by Haider Ali*/
Route::get('/admin/searchsch','SearchSchController@index')->name('scholarship-fetch');

Route::get('/admin/searchsch/fetch','SearchSchController@fetch')->name('scholarship.fetch');

// CSV Export Route
Route::get('/admin/searchsch/export-csv/{sid}','SearchSchController@exportcsv')->name('exportcsv');

/* List Scholarship Route by Haider Ali */
Route::match(['get', 'post'], '/admin/addscholarship', 'AddScholarshipController@index')->name('addscholarship');

// List Scholarship Route by Haider Ali
Route::match(['get', 'post'], '/admin/addscholarship', 'AddScholarshipController@index')->name('addscholarship');

// Edit Apply Scholarship Route by Haider Ali
Route::get('/frontend/editscholarshipDetail/{id}', 'ScholarshipDetailController@edit');

// Edit Scholarship Route by Haider Ali
Route::get('/admin/editscholarship/{id}', 'AddScholarshipController@edit');

// Update Scholarship Route by Haider Ali
Route::post('/admin/updatescholarship/{id}', 'AddScholarshipController@update')->name('updatescholarship');

 /* Fee Verification Route by Haider Ali */
Route::match(['get', 'post'], '/admin/feeverification', 'SchFeeVerification@index')->name('feeverification');

// Edit Fee Verification Route by Haider Ali
Route::get('/admin/editfeeverification/{id}', 'SchFeeVerification@edit');

// Delete Fee Verification Route by Haider Ali
Route::get('/admin/deletefeeverification/{id}', 'SchFeeVerification@delete');

// Update Fee Verification Route
Route::match(['get', 'post'], '/admin/updatefeeverification/{id}', 'SchFeeVerification@update')->name('updatefeestatus');

// Document Verification Route
Route::match(['get', 'post'], '/admin/docverification', 'SchDocumentController@index')->name('docverification');

// Edit Document Verification Route
Route::get('/admin/editdocverification/{id}', 'SchDocumentController@edit');

// Update Document Verification Route
Route::match(['get', 'post'], '/admin/updatedocverification/{id}', 'SchDocumentController@update')->name('updatedocVerification');


// Conduct Verification Route
Route::match(['get', 'post'], '/admin/conductverification', 'SchConductVerificationController@index')->name('updatedocstatus');

// Edit Conduct Verification Route
Route::get('/admin/editconductverification/{id}', 'SchConductVerificationController@edit');

// Update Conduct Verification Route
Route::match(['get', 'post'], '/admin/updateconductverification/{id}', 'SchConductVerificationController@update')->name('updateverificationstatus');



Route::post('/home/submit', 'HomeController@submit');
Route::get('/home/skin/{any?}', 'HomeController@getSkin');
/* Auth & Profile */
Route::get('user/profile', 'UserController@getProfile');
Route::get('/admin', 'UserController@getLogin');
Route::get('user/register', 'UserController@getRegister');
Route::get('user/logout', 'UserController@getLogout');
Route::get('user/reminder', 'UserController@getReminder');
Route::get('user/reset/{any?}', 'UserController@getReset');
Route::get('user/reminder', 'UserController@getReminder');
Route::get('user/activation', 'UserController@getActivation');
// Social Login
Route::get('user/socialize/{any?}', 'UserController@socialize');
Route::get('user/autosocialize/{any?}', 'UserController@autosocialize');
Route::post('user/signin', 'UserController@postSignin');
Route::post('user/create', 'UserController@postCreate');
Route::post('user/saveprofile', 'UserController@postSaveprofile');
Route::post('user/savepassword', 'UserController@postSavepassword');
Route::post('user/doreset/{any?}', 'UserController@postDoreset');
Route::post('user/request', 'UserController@postRequest');

/* Posts & Blogs */
Route::get('posts', 'HomeController@posts');
Route::get('posts/{any}', 'HomeController@posts');
Route::post('posts/comment', 'HomeController@comment');
Route::get('posts/remove/{id?}/{id2?}/{id3?}', 'HomeController@remove');
// Start Routes for Notification
Route::resource('notification', 'NotificationController');
Route::get('home/load', 'HomeController@getLoad');
Route::get('home/lang/{any}', 'HomeController@getLang');
Route::get('/set_theme/{any}', 'HomeController@set_theme');
include('pages.php');
Route::resource('sximoapi', 'SximoapiController');
// Routes for  all generated Module
include('module.php');
$path = base_path().'/routes/custom/';
$lang = scandir($path);
foreach ($lang as $value) {
    if ($value === '.' || $value === '..') {
        continue;
    }
    include('custom/'. $value);
}
Route::group(['namespace' => 'Sximo','middleware' => 'auth'], function () {
    // This is root for superadmin
    include('sximo.php');
});
Route::group(['namespace' => 'Core','middleware' => 'auth'], function () {
    include('core.php');
});
Route::get('abl/{id?}', 'ResultController@checkFeeEmail')->name('abl');

 // Route for print Scholarship Form
    Route::match(['get', 'post'], 'scholarshipprintform/{applicant_id}/{scholarship_id?}', 'Frontend\ScholarshipDetailController@printScholarship')->name('scholarshipprintform');

 // Route for print Scholarship Form - Fee Verification
    


// Custom routes
Route::prefix('admin')->middleware(['auth:web'])->group(function () {
    /* Dashboard Routes */
    // Route::get('/dashboard', 'DashboardController@index')->name('dashboard');
    
    Route::match(['get', 'post'], '/dashboard', 'DashboardController@index')->name('dashboard');

    Route::get('dashboard-stats/{semester?}', 'DashboardController@dashboardStats')->name('showDashboardStats');
    Route::post('specific-semester-stats', 'DashboardController@getSpecificSemesterStats')->name('getSpecificSemesterStats');
    Route::get('cache-stats/{semester?}', 'DashboardController@refreshStatsAndAddToCache')->name('cacheStats');
    Route::get('force-refresh-stats', 'DashboardController@forceRefreshStats')->name('forceRefreshStats');
    Route::get('refreshPreviousStats', 'StatsController@refreshPreviousStats')->name('refreshPreviousStats');
    
    Route::match(['get', 'post'], 'applicants/{semester?}', 'Application\ApplicationController@index')->name('applicants');

    Route::match(['get', 'post'], 'user-import', 'ImportUserController@index')->name('user-import');

    // Dev Mannan: Google Classroom Routes Starts
    Route::get('gc-course-create/{name?}/{section?}/{courseState?}/{alias?}/{courseId?}/{teacherEmail?}', 'googleClassroom@addCourse')
        ->name('googleCourseCreate');
    Route::get('gc-course-status-update/{id}/{status}/{courseId}', 'googleClassroom@updateCourseStatus')
        ->name('googleUpdateCourseStatus');
    Route::get('gc-course-delete/{id?}/{courseId?}', 'googleClassroom@deleteCourse')
        ->name('googleDeleteCourse');
    Route::get('g-queue', 'googleClassroom@startQueue');
    Route::get('g-send-email', 'googleClassroom@sendEamilToTeacher');
    Route::get('g-index', 'googleClassroom@index');
    Route::get('g-test', 'googleClassroom@testQuery');
    // Dev Mannan: Google Classroom Routes Ends
    Route::post('get-specific-semester-applications', 'Application\ApplicationController@getSpecificSemesterApplications')->name('getSpecificSemesterApplications');
    Route::match(['get', 'post'], 'edit-application/{id?}/{fkApplicantId?}', 'Application\ApplicationController@editApplication')->name('edit-application');

    Route::match(['get', 'post'], 'view-log/{id?}', 'Application\ApplicationController@viewLog')->name('view-log');

    Route::post('applicantverifreject', 'Application\ApplicationController@verify');
    Route::post('superadmin', 'Application\ApplicationController@superadmin');
    Route::post('superadminid', 'Application\ApplicationController@superadminid');
    Route::match(['get', 'post'], 'applicantprintform/{oas_app_id}/{semesterid?}', 'Application\ApplicationController@printApplication')->name('applicantprintform');
    Route::match(['get', 'post'], 'nocprintform/{oas_app_id}/{semesterid?}', 'Application\ApplicationController@printNoc')->name('nocprintform');
    Route::match(['get', 'post'], 'applicantprintchallan/{oas_app_id}', 'Application\ApplicationController@printChallan')->name('applicantprintchallan');
    // new applicaion challan route
    Route::get('/get-fee-challan/{applicationid}/{semesterid}/{programid}/{userId?}', 'Frontend\ApplicationController@getFeeChallan')->name('getFeeChallan');
    Route::match(['get', 'post'], 'rollnumberprint/{oas_app_id}', 'Application\ApplicationController@printRollNumber')->name('rollnumberprint');
    Route::get('email', 'Application\ApplicationController@basic_email')->name('email');
    Route::match(['get', 'post'], 'btreports', 'StatisticsController@index')->name('btreports');
    Route::match(['get', 'post'], 'program-detail-report', 'StatisticsController@index')->name('program-detail-report');
    Route::match(['get', 'post'], 'top-bottom-stats', 'StatisticsController@topBottomStats')->name('top-bottom-stats');
    Route::match(['get', 'post'], 'dpstats', 'StatisticsController@department')->name('dpstats');
    Route::match(['get', 'post'], 'dpstats-acad', 'StatisticsController@department')->name('dpstats-acad');
    Route::match(['get', 'post'], 'student-verify', 'StudentController@index')->name('student-verify');
    Route::match(['get', 'post'], 'edit-account/{id?}', 'AccountController@editAccount')->name('edit-account');
    Route::match(['get', 'post'], 'faculty-accounts', 'AccountController@index')->name('faculty-accounts');
    Route::match(['get', 'post'], 'program-fee', 'ProgramFeeController@index')->name('program-fee');
    Route::match(['get', 'post'], 'add-fee/{id?}', 'ProgramFeeController@addFee')->name('add-fee'); 

    /* Admission Fee Verification */
    Route::match(['get','post'], 'verify', 'Application\VerificationController@index')->name('verify');
    Route::match(['get', 'post'], '/verify/verified', 'Application\VerificationController@verified')->name('verified');
    Route::match(['get', 'post'], '/verify/unverified', 'Application\VerificationController@unverified')->name('unverified');
    Route::match(['get', 'post'], '/verify/failed', 'Application\VerificationController@failed')->name('failed');
    /* Ending admission fee verification */
    
    /* First semester fee verification */
    Route::match(['get','post'], 'challanverify', 'ChallanVerificationController@index')->name('challanverify');
    Route::match(['get', 'post'], '/chverify/chverified', 'ChallanVerificationController@verified')->name('chverified');
    Route::match(['get', 'post'], '/chverify/chunverified', 'ChallanVerificationController@unverified')->name('chunverified');
    /* Ending first semester fee verificaiton */
    

    Route::get('report', 'Report\ReportController@index')->name('report');
    Route::match(['get', 'post'], 'report/faculty/{semester?}', 'Report\ReportController@faculty')->name('faculty');
    Route::get('sms', 'SMSController@index');
    Route::post('sms', 'SMSController@store');
    Route::get('create-csv-for-overall-report/{helper?}/{operator?}/{status?}/{reportname?}', 'Report\ReportController@createCsvForOverallReports')->name('createCsvForOverallReports');
    Route::get('create-csv-for-overall-reports-gender-wise/{helper?}/{operator?}/{status?}/{gender?}/{reportname?}', 'Report\ReportController@createCsvForOverallReportsGenderWise')->name('createCsvForOverallReportsGenderWise');
    Route::get('create-csv-report/{facultyId}/{nationalityId?}/{helperWord?}/{fileName?}', 'Report\ReportController@createCsvReport')->name('createCsvReport');
    Route::post('create-csv-report', 'Report\ReportController@createCsvReport')->name('postCreateCsvReport');
    
    Route::get('create-csv-for-departments/{applicationsIds}/{operator}/{nationality}/{helper?}/{fileName?}', 'Report\ReportController@createCsvReportForDepartments')->name('createCsvForDepartments');
    Route::post('create-csv-for-departments', 'Report\ReportController@createCsvReportForDepartments')->name('postCreateCsvForDepartments');

    Route::get('create-csv-for-departments-g/{applicationIds}/{operator}/{status}/{gender}/{nationalityOperator}/{nationality}/{fileName?}', 'Report\ReportController@createCsvReportForDepartmentsGeneral')->name('createCsvForDepartmentsGeneral');
    Route::post('create-csv-for-departments-g', 'Report\ReportController@createCsvReportForDepartmentsGeneral')->name('postCreateCsvForDepartmentsGeneral');

    //programs
    Route::get('create-csv-for-programs/{programId}/{operator}/{nationality}/{helper?}/{fileName?}', 'Report\ReportController@createCsvReportForPrograms')->name('createCsvForPrograms');
    Route::post('create-csv-for-programs', 'Report\ReportController@createCsvReportForPrograms')->name('postCreateCsvForPrograms');
    Route::get('create-csv-for-programs-g/{programId}/{operator}/{status}/{gender}/{nationalityOperator}/{nationality}/{fileName?}', 'Report\ReportController@createCsvReportForProgramsGeneral')->name('createCsvForProgramsGeneral');
    Route::post('create-csv-for-programs-g', 'Report\ReportController@createCsvReportForProgramsGeneral')->name('postCreateCsvForProgramsGeneral');

    Route::get('/overall-report/{semester?}', 'Report\ReportController@topLevelReport')->name('topLevelReport');
    Route::post('get-report-from-specific-semester/{reportName}', 'Report\ReportController@getReportFromSpecificSemester')->name('getReportFromSpecificSemester');

    Route::get('create-csv-for-quota', 'Report\ReportController@QuotaCsv')->name('create-csv-for-quota');

    /* Documents Verificaiton */
    Route::get('docs-new', 'DocumentVerification@docs')->name('documentVerification');
    Route::post('ajax-verify-reject', 'DocumentVerification@ajaxDocsVerify')->name('ajaxVerifyReject');
    Route::get('export-docs', 'DocumentVerification@exportResults')->name('exportDocs');
    Route::get('generate-stats', 'DocumentVerification@generateStats')->name('generateStats');
    // Route::get('/docs', 'DocumentVerification@index');
    // Route::get('/docs1', 'DocumentVerification@index');
    Route::get('/docs', function () {
        return redirect()->route('documentVerification');
    });
    Route::get('/docs1', function () {
        return redirect()->route('documentVerification');
    });
    Route::post('getDepartments/{id?}', 'DocumentVerification@getDepartments')->name('getDepartments');
    Route::post('getProgramme/{id?}', 'DocumentVerification@getProgramme')->name('getProgramme');
    // Route::post('docs', 'DocumentVerification@index')->name('getApplications');
    Route::post('docs1', 'DocumentVerification@index')->name('getApplications');
    Route::post('update-document-status', 'DocumentVerification@updateDocumentStatus')->name('update-docs-status');
    Route::post('export-docs-verification-data', 'DocumentVerification@index')->name('export-docs-verification-data');
    /* Ending Documents Verification */

    Route::post('getDepartmentsFee/{id?}', 'ProgramFeeController@getDepartments')->name('getDepartmentsFee');
    Route::post('getProgrammeFee/{id?}', 'ProgramFeeController@getProgramme')->name('getProgrammeFee');

    /** Result Routes */
    Route::get('upload-results/{id?}', 'ResultController@index')->name('getUploadResults');
    Route::post('upload-results', 'ResultController@store')->name('postResult');

    // Merit Routes
    Route::get('merit-results-generate/{id?}', 'ResultController@indexMerit')->name('getUploadResultsMerit');
    Route::post('merit-results-generate', 'ResultController@storeMerit')->name('postResultMerit');

    // Arriers challan
    Route::get('arriersCalculation', 'ResultController@arriersCalculation')->name('arriersCalculation');
    // Route::get('abl', 'ResultController@checkFeeEmail')->name('abl');
    // Dev Mannan: Picture move
    Route::get('picmove', 'ResultController@picMove')->name('picmove');
    Route::get('ablArriers', 'ResultController@arrearChallanData')->name('ablArriers');
    Route::patch('patch-upload-results', 'ResultController@update')->name('patchResult');
    Route::match(['get', 'post'], 'results/manage', 'ResultController@manage')->name('manageResults');
    Route::post('results/change-status', 'ResultController@changeActiveStatus')->name('changeStatus');
    Route::post('results/change-state-public', 'ResultController@changeStatePublic')->name('changePublicState');
    Route::get('fckrecords', 'ResultController@fckrecords')->name('fckrecords');
    /* Ending Result Routes */

    Route::get('test/{id}', 'TestController@index')->name('test');

    Route::get('updatePqm', 'DocumentVerification@updatePqmForSpring2021')->name('updatePqm');
    Route::get('update-pqm-for-fet-bs-program', 'DocumentVerification@bulkUpdatePqmForFetBsProgram')
        ->name('updatePqmForFetBsProgram');
    Route::get('bulk-update-pqm/{id}', 'DocumentVerification@bulkUpdatePqmForProgram')
        ->name('bulkUpdatePqmForProgram');


    Route::get('get-incorrect-programs', 'TestController@getIncorrectPrograms')->name('getIncorrectPrograms');
    Route::get('email-campaign', 'Campaign\EmailCampaign@sendEmail')->name('emailCampaign');
    Route::get('ec', 'Campaign\EmailCampaign@sendEmailReminderToProgram');

    Route::get('bulk/sms', 'BulkController@index');
    Route::get('bulk/email', 'BulkController@index');
    Route::post('bulk/store', 'BulkController@store')->name('bulkStore');

    Route::get('stats', 'StatsController@index');
    Route::get('late-candidates', 'StatsController@lateCandidates');
    Route::get('sttesting', 'StatsController@test')->name('sttesting');
    Route::match(['get', 'post'], 'program-stats', 'StatsController@programStats')->name('program-stats');
    Route::post('get-meta', 'StatsController@getSingelSemesterMeta')->name('getStatsOfASemester');

    Route::get('/options', 'OptionsController@index')->name('options');
    Route::get('/options/create/{id?}', 'OptionsController@create')->name('createOptions');
    Route::post('/options/store', 'OptionsController@store')->name('storeOptions');
    Route::get('/options/delete/{id}', 'OptionsController@delete')->name('deleteOptions');

    //merit routes
    Route::get('/merit/{id?}', 'MeritController@index')->name('merit');
    Route::get('/merit/{id}/update', 'MeritController@updateMerit')->name('updateMerit');
    Route::post('/merit', 'MeritController@store')->name('postMerit');
    Route::get('/merit/{id}/file-stats', 'MeritController@fileStats')->name('filestats');
    Route::get('/merit/{id}/view-file', 'MeritController@viewFile')->name('viewFile');
    Route::get('/merit/{id}/view-file-with-pqm', 'MeritController@viewFileWithPqm')->name('viewFileWithPqm');
    Route::get('/merit/view-list/{id}/{list}', 'MeritController@viewList')->name('viewCutpointList');
    Route::get('/merit/publish-or-suspend-list/{id}', 'MeritController@publishOrSuspendList')->name('publishOrSuspendList');
    Route::post('/store-merit', 'MeritController@storeMerit')->name('storeMerit');
    Route::get('/manage-merit/{id}', 'MeritController@manage')->name('manage');
    Route::get('/merit-listing', 'MeritController@getGeneratedMeritLists')->name('getMeritLists');
    Route::post('/merit/manage/store-cutpont', 'MeritController@storeMeritCutpoint')->name('storeCutPoint');
    Route::post('/search-listing', 'MeritController@searchListing')->name('searchListing');

    Route::get('/quota', 'QuotaController@index')->name('quota');
    Route::post('/quota', 'QuotaController@store')->name('postQuota');
    Route::post('/quota-download', 'QuotaController@download')->name('downloadQuota');
    /* Convocation Routes */
    Route::match(['get','post'], '/convocation-fee-verify', 'Convocation\ConvocationVerificationController@index')->name('convocation-verify');
    Route::match(['get', 'post'], '/convocation-verify/verified', 'Convocation\ConvocationVerificationController@verified')->name('convocation-verified');
    Route::match(['get', 'post'], '/convocation-verify/unverified', 'Convocation\ConvocationVerificationController@unverified')->name('convocation-unverified');
    Route::get('generate-convocation-challan', 'Convocation\ConvocationVerificationController@generateChallanOfExistingRecords');
    Route::get('generate-convocation-fee', 'Convocation\ConvocationVerificationController@generateFeeOfExistingRecords');
    Route::get('convocation-report/{type}', 'Convocation\ConvocationVerificationController@generateReport');

    /* Small Utilities */
    Route::get('/add-department-and-faculty', 'UtilityController@addDepartmentAndFacultyWithProgramInApplicationsTable');
    // Fall2020, Send Email
    Route::get('/send-email-to-fet', 'UtilityController@sendEmailToFet');
    Route::get('/fll-notification', 'UtilityController@emailNotificationToFll');
    // End Fall2020, Send Email
    /**
     * Joining Routes
     */
    Route::match(['get', 'post'], '/joining', 'JoiningController@index')->name('joining');
    Route::post('/joining/confirm', 'JoiningController@confirmJoining')->name('confirmJoining');
    Route::get('/migrate-to-aljamia', 'MigrateToAljamia@index');
    Route::get('/split-migrate-to-aljamia', 'MigrateToAljamia@splitMigrateToAljamia');

    Route::get('/split-save-to-tables/{faculty}/{semester}', 'MigrateToTables@index');

    Route::get('/update-gender', 'MigrateToAljamia@updateGender');
    
    Route::prefix('aljamia')->namespace('AljamiaApi')->group(function () {
        Route::get('get-faculties', 'AljamiaController@getFaculties');
        Route::get('get-departments', 'AljamiaController@getDepartments');
        Route::get('get-programs', 'AljamiaController@getPrograms');
        Route::get('get-batches', 'AljamiaController@getBatches');
        Route::get('get-courses', 'AlJamiaController@getCourses');
        Route::prefix('report')->group(function () {
            Route::match(['get', 'post'], 'course-listing', 'AljamiaReportController@courseListingReport')
                ->name('courseListingReport');
            Route::match(['get', 'post'], 'course-listing-overview', 'AljamiaReportController@courseListingOverview')
                ->name('courseListingOverview');
            Route::match(['get', 'post'], 'gc-course-audit/{courseId?}/{teacherId?}', 'AljamiaReportController@courseAudit')
                ->name('googleCourseAudit');
        });
        Route::get('get-aljamia-departments', 'AljamiaHelperController@getDepartments')->name('getAljamiaDepartments');
        Route::get('get-aljamia-programs', 'AljamiaHelperController@getPrograms')->name('getAljamiaPrograms');
        Route::get('get-aljamia-batches', 'AljamiaHelperController@getBatches')->name('getAljamiaBatches');
        Route::get('teacher-detail/{id}', 'AljamiaHelperController@getTeacherInfo')
            ->name('alJamiaCompleteTeacherInfo');// second-param {aljamiaId}
        Route::get('course-detail/{id}/{courseCode}', 'AljamiaHelperController@getCourseInfo')
            ->name('alJamiaCompleteCourseInfo');
    });
    // Dev Mannan: Route for result list comparison
    Route::get('resultsListComparison/{id}/{gender}/{semester}', 'ResultController@compareList');
}); //end admin routes

/** Result Public Routes */
Route::get('results/{id?}', 'ResultController@show')->name('getResultsMainPage');
Route::get('recent-results/{id?}', 'ResultController@recentResults')->name('recent-results');;
Route::post('results', 'ResultController@show')->name('displayResults');
Route::post('getDepartmentsWhereResultIsAnnounced/{id?}', 'ResultController@getDepartments')->name('getDepartmentsResults');
Route::post('getProgrammeWhereResultIsAnnounced/{id?}', 'ResultController@getProgramme')->name('getProgrammeResults');
Route::get('results/{id}/list', 'ResultController@getList')->name('displayLists');
Route::post('results/get-results-by-rollnumber', 'ResultController@getResultByRollNumber')->name('getResultByRollNumber');
Route::get('/get-results-docs/{result}/{rollno}/{doc}/{cnic?}', 'ResultController@getResultDocs')->name('getResultDocs');
Route::get('result/mean/{program}', 'ResultController@calculateMean');
Route::get('instructions', 'ResultController@instructions')->name('instructions');
Route::get('/affidavit-specimen', 'ResultController@affidavitSpecimen')->name('affidavit-specimen');

/* Ending result public routes */


/* Public Scholarship Route by Haider Ali */
Route::get('scholarshipdetail', 'ScholarshipViewController@index')->name('scholarshipdetail');

Route::get('fee-structure', 'FeeViewController@index')->name('fee-structure');
// Route::get('fee-structure-old', 'FeeViewOldController@index')->name('fee-structure-old');
Route::get('programes', 'FeeViewController@programes')->name('programes');
// Route::get('programes', function(){
// 	return redirect()->away('https://admission.iiu.edu.pk');
// })->name('programes');



/** Results SP19 */
Route::get('results-sp19', 'MeritController@resultsIndex');
Route::post('list-results-sp19', 'MeritController@postMeritResultListing')->name('postMeritListResults');
Route::get('list-results-sp19/{programId}', 'MeritController@getMeritResultListing')->name('getMeritResultListing');
Route::get('list-results-sp19/view/{id}', 'MeritController@getSingleList')->name('getSingleList');

/* Merit Result */
Route::get('merit/results', 'MeritResultController@index');
Route::match(['get', 'post'], 'add-previous/', 'PreviousStatsController@index')->name('add-previous');

Route::get('mapp/{id?}', 'DashboardController@mapQuery')->name('mapp');

/** EP & BP Report */
Route::get('/ep', 'TestController@epReport');
Route::get('/bp', 'TestController@buildReport');

/* Dev Talha: Custom Routes */
// Route::resource('backup', 'ApplicantsBackupController');
Route::get('pqmm', 'PreviousQualiController@index')->name('pqmm');


// Route::match(['get', 'post'], 'publicRollNumberSlip', 'Application\ApplicationController@publicPrintRollNumber')->name('publicRollNumberSlip');
// Route::match(['get', 'post'], 'rollnumberprint/{oas_app_id}', 'Application\ApplicationController@printRollNumber')->name('rollnumberprint');

/**
 * Utility Routes
 */

Route::get('get-phone-of-status-2', 'UtilityController@getPhoneOfStatus2');
Route::get('arriers-calculation', 'ResultController@arriersCalculation')->name('arriersCalculation');

Route::get('course-email-viewed/{id}/{aljamiaId}', 'googleClassroom@emailViewed')->name('emailViewed');
Route::get('redirecting-to-google-class/{courseId?}', 'googleClassroom@courseViewed')->name('courseViewed');

Route::get('requeue-jobs', 'UtilityController@reQueueFailedJobs');

Route::get('/send-results-sms', 'GetsmsController@index');

// Route::get('/fee-verify-cron', 'Application\VerificationController@feeVerifyCron');

// Route::get('/fee-verify-cron-free', 'Application\VerificationController@feeVerifyFree');

Route::get('/fee-verify-group', 'Application\VerificationController@groupFeeVerify');


Route::get('/send-abl-chln', 'ResultController@challanAblEmail')->name('send-abl-chln');

