<?php
        
// Start Routes for appreport 
Route::resource('appreport','AppreportController');
// -- Post Method --

// End Routes for appreport 

                    
// Start Routes for faculty 
Route::resource('faculty','FacultyController');
// End Routes for faculty 

                    
// Start Routes for department 
Route::resource('department','DepartmentController');
// End Routes for department 

                    
// Start Routes for semester 
Route::resource('semester','SemesterController');
// End Routes for semester 

                    
// Start Routes for program 
Route::resource('program','ProgramController');
// End Routes for program 

                    
// Start Routes for programschedule 
Route::resource('programschedule','ProgramscheduleController');
// End Routes for programschedule 

                    
// Start Routes for permissions 
Route::resource('permissions','PermissionsController');
// End Routes for permissions 

                    ?>