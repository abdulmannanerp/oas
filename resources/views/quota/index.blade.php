@extends('layouts.app')
@section('content')
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
	<div class="container">
		@if ( $status )
			<div class="alert alert-success">
				{{ $status }}
			</div>
		@endif
		<section class="page-header row">
			<h2> Quota </h2>
		</section>
		<div class="page-content row">
			<div class="page-content-wrapper no-margin">
				@if($error)
					<div class="alert alert-danger">
				        <p>{{$error}}</p>
				    </div>
				@endif
				<form method="POST" action="{{ route('postQuota') }}" accept-charset="UTF-8" class="form-horizontal validated" enctype="multipart/form-data">
					{{ csrf_field() }}
					<input type="hidden" value="{{auth()->id()}}" name="userId">
					<div class="sbox">
						<div class="sbox-content clearfix">
							<div class="form-group  ">
								<div class="row">
									<div class="col-md-4">
										<label for="Title" class=" control-label col-md-4 text-left"> Programs </label>
									</div>
									<div class="col-md-6">
									  <select name="programme" id="programme" class="form-control input-sm select2" required>
									  	<option value="">--Select Program--</option>
								  		@foreach ( $programs as $program )
								  			<option value="{{$program->pkProgId}}"> {{ $program->title}} </option>
								  		@endforeach
									  </select>
									</div> 
								</div> <!-- Ending row -->
							</div> <!-- Ending form-group -->

							<div class="form-group  ">
								<div class="row">
									<div class="col-md-4">
										<label for="Title" class=" control-label col-md-4 text-left"> Select File </label>
									</div>
									<div class="col-md-6">
										<input name="fileToUplaod" type="file" class="form-control input-sm" accept=".csv" required>
									</div> 
								</div> <!-- Ending row -->
							</div> <!-- Ending form-group -->

							<div class="form-group  ">
								<div class="row">
									<div class="col-md-4">
										
									</div>
									<div class="col-md-6">
										<button name="apply" class="tips btn btn-sm btn-primary btn-apply" type="submit" title="Back"><i class="fa  fa-check"></i> Next </button>
									</div> 
								</div> <!-- Ending row -->
							</div> <!-- Ending form-group -->

						</div> <!-- Ending sbox-content -->
					</div> <!-- Ending sbox -->
				</form>

{{-- Quota generate --}}
				<form method="POST" action="{{ route('downloadQuota') }}" accept-charset="UTF-8" class="form-horizontal validated" enctype="multipart/form-data">
					{{ csrf_field() }}
					<input type="hidden" value="{{auth()->id()}}" name="userId">
					<div class="sbox">
						<div class="sbox-content clearfix">
							<div class="form-group  ">
								<div class="row">
									<div class="col-md-4">
										<label for="Title" class=" control-label col-md-4 text-left"> Programs </label>
									</div>
									<div class="col-md-6">
									  <select name="programme" id="programme" class="form-control input-sm select2" required>
									  	<option value="">--Select Program--</option>
								  		@foreach ( $programs as $program )
								  			<option value="{{$program->pkProgId}}"> {{ $program->title}} </option>
								  		@endforeach
									  </select>
									</div> 
								</div> <!-- Ending row -->
							</div> <!-- Ending form-group -->
							<div class="form-group  ">
								<div class="row">
									<div class="col-md-4">
										<label for="Title" class=" control-label col-md-4 text-left"> Gender </label>
									</div>
									<div class="col-md-6">
										<div class="iradio_square-green" style="position: relative;">
										  	<input type="radio" name="selectGender" value="3" class="minimal-red" style="position: absolute; opacity: 0;" data-parsley-multiple="selectGender">
										</div> Male 
					
										<div class="iradio_square-green" style="position: relative;">
											<input type="radio" name="selectGender" value="2" class="minimal-red" data-parsley-multiple="selectGender" style="position: absolute; opacity: 0;">
										</div> Female  
									</div> 
								</div> <!-- Ending row -->
							</div> <!-- Ending form-group -->
							<div class="form-group  ">
								<div class="row">
									<div class="col-md-4">
										
									</div>
									<div class="col-md-6">
										<button name="apply" class="tips btn btn-sm btn-primary btn-apply" type="submit" title="Back"><i class="fa  fa-check"></i> Download </button>
									</div> 
								</div> <!-- Ending row -->
							</div> <!-- Ending form-group -->

						</div> <!-- Ending sbox-content -->
					</div> <!-- Ending sbox -->
				</form>
			</div> <!-- Ending page-content-wrapper -->
		</div> <!-- Ending page-content row -->
	</div> <!-- Ending Container -->

	<script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
	<script>
        $(function(){
            $('#faculty').on('change', function(){

               $.ajax({
                   method: 'post',
                   url: '{{ route('getDepartments') }}',
                   data: {
                       id: $(this).val()
                   },
                   success: function ( response ) {
                       $('#department').empty();
                       $('#department').append(
                            '<option value="">--Select Department--</option>'
                        );
                       $.each(response, function (key, value) {
                           $('#department').append(
                               '<option value="'+value.pkDeptId+'">'+ value.title+'</option>'
                           );
                       });
                   }
               });
            });

            $('#department').on('change', function(){
                $.ajax({
                    method: 'post',
                    url: '{{ route('getProgramme') }}',
                    data: {
                        id: $(this).val()
                    },
                    success: function (response) {
                        $('#programme').empty();
                        if ( response == '' ) {
                            $('#programme').append(
                                '<option value="">--Select Porgramme--</option>'
                            );
                            return;
                        }
                        $('#programme').append(
                            '<option value="">--Select Porgramme--</option>'
                        );
                        $.each(response, function (key, value) {
                            $('#programme').append(
                                '<option value="'+value.pkProgId+'">'+ value.title+'</option>'
                            );
                        });
                    }
                });
            });

            $('#start-date').flatpickr();
	        $('#end-date').flatpickr();
        });
    </script>

@endsection