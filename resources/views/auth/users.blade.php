@extends('layouts.app')

@section('content')
 
<div class="page-content row"> 
    <div class="page-content-wrapper m-t">
        <div class="sbox" style="border-top: none">
            <div class="sbox-title"> 
                <h3> All Users </h3>
            </div>
            <table class="table table-bordered table-hover">
                <thead class="table-head">
                    <tr>
                        <th>S.No</th>
                        <th>Name</th>
                        <th>Username</th>
                        <th>Email</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @php $i = 1; @endphp
                    @foreach ( $users as $user )
                    <tr>
                        <td> {{ $i }} </td>
                        <td> {{ $user->first_name . ' ' . $user->last_name }} </td>
                        <td> {{ $user->username }} </td>
                        <td> {{ $user->email }} </td>
                        <td> {{ ($user->active ) ? 'Active' : 'Inactive' }}</td>
                        <td>
                            <form action="{{route('auth')}}" method="POST">
                                {{ csrf_field() }}                                
                                <input type="hidden" value="{{ $user->email }}" name="email">
                                <button class="btn btn-primary btn-sm">Login</button>                                
                            </form>
                        </td>
                    </tr>
                    @php $i++; @endphp
                    @endforeach
                </tbody>
            </table>
        </div>  
    </div>
</div> 

                     
@stop