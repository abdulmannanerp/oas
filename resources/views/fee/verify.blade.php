@extends('layouts.app')


@section('content')
    <div class="page-content row">
        <div class="page-content-wrapper m-t">
            @if ($status)
                <div class="alert alert-success mt5 mb5">
                    {{ $status }}
                </div>
            @endif

            <div class="alert alert-danger">
                <strong>Fee challans verified here must be re-consile with bank on daily basis. In case of any query kindly call/email at 2799/abdul.mannan@iiu.edu.pk</strong>
            </div>
            <form action="{{ route('verify') }}" method="POST" id="application-numbers-form">
                {{ csrf_field() }}
                <div class="form-group">
                    <label for="">Enter Application Numbers</label>
                    <textarea name="application_ids" id="application-ids-textbox" class="form-control" placeholder="171005,758000,875520"
                        required></textarea>
                </div> <!-- Ending Form Group -->
                <div class="form-group">
                    <input type="submit" class="btn btn-primary" value="List Records" name="list_records">
                </div>
            </form>

            {{-- <form action="{{route('verify')}}" method="POST" enctype="multipart/form-data" id="upload-csv-form">
                <div class="form-group">
                    {{ csrf_field() }}
                    <label for="">Upload CSV</label>
                    <input type="file" name="csv" class="btn btn-default" accept=".csv" required>
                </div>
                <div class="form-group">
                    <input type="submit" class="btn btn-primary" value="Uplaod & List" name="upload_and_list_csv">
                </div>
            </form> --}}

            @if (session('verifiedApplications'))
                <h4 class="applicatoins-verified-relist">Following applications has been verified</h4>
                <ul>
                    @foreach (session('verifiedApplications') as $app)
                        <li> {{ $app->id }}</li>
                    @endforeach
                </ul>
            @endif


            @if ($applications)
                <div class="table-responsive">
                    <table class="table table-hover table-bordered app-search-grid" id="ManualVerifyTable">
                        <thead class="table-head">
                            <tr>
                                <th>Challan#</th>
                                <th>Name</th>
                                <th>Programme</th>
                                <th>Status</th>
                                <th>Verified By</th>
                                <th>Date</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php $allApplications = []; @endphp
                            @foreach ($applications as $application)
                                @if ($application->application->applicant != '')
                                    <tr>
                                        <td>{{ $application->id }}</td>
                                        <td>{{ $application->application->applicant->name }}</td>
                                        <td>{{ $application->application->program->title ?? '' }}</td>
                                        <td>
                                            @if ($application->application->fkCurrentStatus >= 4)
                                                <span class="fee-already-verified">Fee Already Verified</span>
                                            @else
                                                <span class="fee-not-verified">Unverified</span>
                                            @endif
                                        </td>
                                        <td> {{ @$application->user->first_name . ' ' . @$application->user->last_name }}
                                            </th>
                                        <td>
                                            @if ($application->application->feeVerified)
                                                {{ date('d-M-Y h:i A', strtotime($application->application->feeVerified)) ?? '' }}
                                            @else
                                                <span></span>
                                            @endif
                                        </td>
                                    </tr>
                                @endif
                                @if ($application->application->fkCurrentStatus > 1 && $application->application->fkCurrentStatus < 4)
                                    {{-- @if ($application->application->applicant->fkLevelId == 1)
                                @php 
                                    $value = \App\Model\Application::where(['fkApplicantId' => $application->application->applicant->userId])
                                    ->where('fkCurrentStatus','>=', 2)
                                    ->get()->pluck('id')->toArray();
                                    // dd($value);
                                    $challanDetailNew = \App\Model\ChallanDetail::whereIn('fkApplicationtId', $value)
                                        ->pluck('id');
                                    foreach($challanDetailNew as $v){
                                        // dump($v);
                                        $allApplications[] = $v;
                                    }
                                @endphp
                                @else --}}
                                    @php $allApplications[] = $application->id; @endphp
                                    {{-- @endif --}}
                                @endif
                            @endforeach
                        </tbody>
                    </table>
                    <form action="{{ route('verify') }}" method="POST">
                        {{ csrf_field() }}
                        <p class="incomplete-application-verification-message">(Incomplete application will not be verified)
                        </p>
                        <input type="hidden" name="verify" value={{ serialize($allApplications) }}>
                        <input type="submit" value="Verify All" class="btn btn-primary">
                    </form>
                </div>
            @endif
            {{-- @if ($applications_not_exists)
                <h3>Application(s) not found</h3>
                <ol>
                @foreach ($applications_not_exists as $application)
                     <li> {{ $application }}</li>
                @endforeach
                </ol>
            @endif --}}
        </div>
    </div>
@stop
