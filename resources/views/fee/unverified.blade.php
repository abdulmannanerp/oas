@extends('layouts.app')


@section('content')
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">

    <div class="page-content row">
        <div class="page-content-wrapper m-t">

            <div class="alert alert-danger">
                <strong>Fee challans verified here must be re-consile with bank on daily basis. In case of any query kindly call/email at 2799/abdul.mannan@iiu.edu.pk</strong>
            </div>
            
            <h3>Fee Pending</h3>
            <div class="filter-and-export">
                <form action="{{ route('unverified') }}" method="GET" class="varified-page-filter-form">
                    <table>
                        <tr><label for="filter-records">Filter Records:</label></tr>
                        <tr>
                            <td><input type="text" id="date-from" placeholder="Date From" name="start_date" required></td>
                            <td><input type="text" id="date-to" placeholder="Date To" name="end_date" required></td>
                            <td><input type="submit" class="btn btn-primary" value="Search"></td>
                        </tr>
                    </table>
                </form>
                <div class="verified-export-to-csv-btn pull-right">
                    <form action="{{route('unverified')}}" method="POST">
                        <input type="hidden" name="start_date" value={{$startDate ?? ''}}>
                        <input type="hidden" name="end_date" value={{$endDate ?? ''}}>
                        <input type="hidden" name="export" value='export'/>
                        <button type="submit" class="btn btn-primary btn-sm">Export to CSV</button>
                    </form>
                </div>
            </div>
            <div class="table-responsive">
                {{-- <h3>Verified Fee {{ '('.$applications_count_with_fee_verified.')' ?? '(0)' }}</h3> --}}
                {{-- <h5>Verified By Me: {{ '('.$applications_count_with_fee_verified_by_me.')' ?? '(0)' }}</h5> --}}
                {{-- <h5>Verified By Others: {{ '('.$applications_count_with_fee_verified_by_others.')' ?? '(0)' }}</h5> --}}
                @if ( $applications->total() ) 
                    <h3>Verification Pending {{ '('.$applications->total().')' ?? '(0)' }}</h3>
                @endif
                @if ( count($applications) != 0 )
                
                <table class="table table-hover table-bordered app-search-grid" id="ManualVerifyTable">
                    <thead class="table-head">
                    <tr>
                        <th>Serial No</th>
                        <th>Challan Number</th>
                        <th>Name</th>
                        <th class="table-program-column">Programme</th>
                        <th>Application Status</th>
                        <th>Bank Details</th>
                        <td>Branch Code</td>
                        <th>Date</th>
                        <td>Challan</td>
                    </tr>
                    </thead>

                    <tbody>
                    @php $i = $applications->perPage() * ($applications->currentPage() - 1); @endphp
                    @foreach ( $applications as $application)
                        @if ( $application->applicant != '' )
                        <tr>
                            <td> {{ ++$i }} </td>
                            {{-- <td>{{ $application->id }}</td> --}}
                            <td> {{ $application->challan->id ?? '' }} </td>
                            <td class="left-align">{{ $application->applicant->name }}</td>
                            <td>{{ $application->program->title ?? '' }}</td>
                            <td><span class="unverified-fee">{{ 'Unverified' }}</span></td>                                
                            <td>{{ $application->bankDetails ?? '' }}</td>
                            <td> {{ $application->branchCode ?? '' }} </td>
                            <td>
                                @php $date = date_create($application->feeSubmitted); echo date_format($date, 'd-M-Y') @endphp
                            </td>
                            <td>
                                <a target="_blank" href={{'https://admission.iiu.edu.pk/storage/'.$application->challan_pic}}>View</a>
                            </td>
                        </tr>
                        @endif
                    @endforeach
                    </tbody>
                </table>
            </div>
                {{ $applications->appends(['start_date' => $startDate, 'end_date' =>  $endDate])->links() }}
            @else
                <h5>There are no applicants with fee pending</h5>
            @endif
        </div>
        </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
    <script>
        $('#date-from').flatpickr();
        $('#date-to').flatpickr();
        document.querySelector('#date-from').value = '{{request('start_date')}}';
        document.querySelector('#date-to').value = '{{request('end_date')}}';
    </script>

@stop
