@extends('layouts.app')
@section('content')
@php 
use App\Http\Controllers\SchFeeVerification;
@endphp

<div class="page-content row">
    <div class="page-content-wrapper m-t">
        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div> <!-- Ending ending alert-danger -->
        @endif


    <div class="sbox">

        <form action="{{ route('updatefeestatus',['id' => $SchView]) }}" class="scholarshipDetailForm" class="scholarshipDetailForm" method="POST">
            

        <div class="sbox-title clearfix">
            <h1>Fee Verification - Update</h1>
            <div class="sbox-tools pull-right" >
                <button class="tips btn btn-sm btn-save" id="personal-detail-btn" type="submit"><i class="fa fa-save"></i> Update Status</button>
            </div>
        </div>

        <div class="sbox-content clearfix">
        <div class="row">
           <div class="col-md-12">
            <div class="form-group">
                <label for="scholarship_name" class="control-label col-md-4 text-left"> Student Name </label>
                <div class="col-md-6">
                    <div class="form-group">
                <input  type='text' name='sch_name' id='sch_name' value="{{$Applicant->name}}" class='form-control input-sm' readonly />
            </div>
                 </div>
              </div>


              <div class="form-group">
                <label for="scholarship_name" class="control-label col-md-4 text-left"> Name </label>
                <div class="col-md-6">
                    <div class="form-group">
                <input  type='text' name='sch_name' id='sch_name' value="{{$scholarshipname->name}}" class='form-control input-sm' readonly/>
            </div>
                 </div>
              </div>

              <div class="form-group" >
                <label for="Start_date" class="control-label col-md-4 text-left"> Start Date </label>
                <div class="col-md-6">
                  <div class="form-group">
                    <input  type='date' name='start_date' id='start_date' value="{{$scholarshipname->start_date}}"  class='form-control input-sm' readonly/>
                  </div>
                </div>
              </div> 

              <div class="form-group" >
                <label for="end_date" class="control-label col-md-4 text-left"> End Date</label>
                <div class="col-md-6">
                    <div class="form-group">
                    <input  type='date' name='end_date' id='end_date' value="{{$scholarshipname->end_date}}"  class="form-control input-sm" readonly />
                    </div>
                </div> 
              </div>

              <div class="form-group" >
                    <label for="status" class=" control-label col-md-4 text-left">Status </label>
                    <div class="col-md-6">
                        <div class="form-group">
                        <select name='fee_ver_status' rows='5' id='fee_ver_status' class='select2'>
                            <option valu="">--- Select Option ---</option>
                            <option value="0" <?php echo ($SchView->feeverification == '0') ?'selected':''; ?>>Pending</option>
                            <option value="1" <?php echo ($SchView->feeverification == '1') ?'selected':''; ?>>Verified</option>
                            <!-- <option value="2" <?php echo ($SchView->feeverification == '2') ?'selected':''; ?>>Reject</option> -->
                        </select> 
                    </div>
                    </div> 
                </div>
                
                <div class="form-group" >
                <label for="end_date" class="control-label col-md-4 text-left"> Remarks</label>
                <div class="col-md-6">
                    <div class="form-group">
                    <textarea class="form-control input-sm" rows="4" id="fee_remarks" name="fee_remarks">{{$SchView->feeremarks ?? old('feeremarks')}}</textarea>
                    </div>
                </div> 
              </div>

            </div>
        </div>
        </div>
    </form>
    </div>
    </div> <!-- Ending page-content-wrapper --> 
</div> <!-- Ending page-content --> 

<script type="text/javascript">
    $(document).ready(function() {
    $('#example').DataTable();
} );
</script>
@stop