@extends('layouts.app')
@section('content')
@php 
use App\Http\Controllers\SchDocumentController;
@endphp

<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.5/css/jquery.dataTables.min.css">
<style type="text/css">
    #example_wrapper .dataTables_filter {
        display: block !important;
    }
</style>
<div class="page-content row">
    <div class="page-content-wrapper m-t">
        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div> <!-- Ending ending alert-danger --> 
        @endif

        <div class="sbox">
            <div class="sbox-title">
                <h1>Scholarship - Document Verification</h1>
            </div> <!-- Ending sbox-title -->

            <div class="sbox-content ">
                <div class="table-responsive" style="min-height:300px;">
                    <table id="example" class="table table-striped table-bordered">
                    <thead class="table-head">
                        <tr>
                            <th>Applicant Name</th>
                            <th>Scholarship Name</th>
                            <th>Apply at</th>
                            <th>Fee Verification</th>
                            <th>Conduct Certificate Verification</th>
                            <th>Document Verification</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        
                            @foreach ($SchView as $s)
                            @if($s->feeverification == '1' && $s->conductverfication == '1')
                            <tr>
                                <td> {{ $s->applicant->name }} </td>
                                <td>{{ $s->listscholarship->name }}</td>
                                <td>{{ date('d-m-Y', strtotime($s->created_at))}}</td>

                                <td>
                                    <?php if($s->feeverification == '0'){
                                    ?>  
                                    <label class="label label-warning">
                                        <?php echo "In Progress"; ?>
                                    </label>
                                    <?php
                                    }

                                    else if($s->feeverification == '1'){
                                        ?>
                                        <label class="label label-info">
                                        <?php echo "Verified"; ?>
                                    </label>
                                    <?php
                                    }
                                    else if($s->feeverification == '2') {
                                        ?>
                                        <label class="label label-danger">
                                        <?php echo "Rejected"; ?>
                                    </label>
                                    <?php
                                    }
                                    ?>
                                </td>
                                <td>
                                    <?php if($s->conductverfication == '0'){
                                    ?>  
                                    <label class="label label-warning">
                                        <?php echo "In Progress"; ?>
                                    </label>
                                    <?php
                                    }

                                    else if($s->conductverfication == '1'){
                                        ?>
                                        <label class="label label-info">
                                        <?php echo "Verified"; ?>
                                    </label>
                                    <?php
                                    }
                                    else if($s->conductverfication == '2') {
                                        ?>
                                        <label class="label label-danger">
                                        <?php echo "Rejected"; ?>
                                    </label>
                                    <?php
                                    }
                                    ?>
                                </td>
                                <td>
                                    <?php if($s->status == '0'){
                                    ?>  
                                    <label class="label label-warning">
                                        <?php echo "In Progress"; ?>
                                    </label>
                                    <?php
                                    }

                                    else if($s->status == '1'){
                                        ?>
                                        <label class="label label-info">
                                        <?php echo "Verified"; ?>
                                    </label>
                                    <?php
                                    }
                                    else if($s->status == '2') {
                                        ?>
                                        <label class="label label-danger">
                                        <?php echo "Rejected"; ?>
                                    </label>
                                    <?php
                                    }
                                    ?>
                                </td>


                                 <td>

                                    <div class="btn-group">
                                      <button class="btn btn-primary btn-xs dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Action
                                      </button>
                                      <ul class="dropdown-menu">
                                        <li>
                                            <a href="{{url('/admin/editdocverification/'.$s->id)}}">Edit</a>
                                        </li>
                                        
                                        <li>
                                            <a target="_blank" href="{{route('scholarshipprintform',['applicant_id' => $s->applicant->userId, 'scholarship_id'=> $s->listscholarship->id])}}">Print</a>
                                        </li>
                                      </ul>
                                    </div>
                                </td>
                            </tr>
                            @endif
                            @endforeach                            

                        </tbody>
                    </table>
                </div> <!-- Ending table-responsive --> 
            </div> <!-- Ending sbox-content - 18 --> 
        </div> <!-- Ending sbox - 14 --> 
    </div> <!-- Ending page-content-wrapper --> 
</div> <!-- Ending page-content --> 



<script type="text/javascript">
    
    // $('#example').DataTable({
    //     searching: true});
    // }
    //$(document).ready(function() {

       // $('#example').dataTable({searching: true, paging: true, info: true});

        $(document).ready(function() {
          $('#example').DataTable({
           
            
          });
        });
   // });

</script>
<script type="text/javascript" src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js"></script>

@stop