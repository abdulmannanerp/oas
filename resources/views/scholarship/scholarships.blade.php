@extends('layouts.app')
@section('content')
@php 
use App\Http\Controllers\SearchScholarshipsController;
@endphp

<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.12.1/css/jquery.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/2.2.3/css/buttons.dataTables.min.css">
<style type="text/css">
    #example_wrapper .dataTables_filter {
        display: block !important;
    }
</style>


<div class="page-content row">
    <div class="page-content-wrapper m-t">
        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div> <!-- Ending ending alert-danger --> 
        @endif


        <div class="sbox">
            <div class="sbox-title">
                <h1>List of Student Applied in Scholarship</h1>
            </div> <!-- Ending sbox-title -->
        <form method="get" action="/admin/scholarships" onchange="this.submit()">
            <div class="sbox-content ">
                <div class="row">
                  <div class="col-lg-4">
                    <div class="form-group">
                       <div class="form-group">
                          <label class="req-rd">Select Scholarship</label>
                            <select class="form-control select2" id="listscholarship" name="sid" required style="width: 100%;">
                                <option value="">Select Scholarship</option>
                                @foreach ( $listscholarship as $ls )
                                <option value="{{$ls->id}}"> {{$ls->name}} </option>
                                @endforeach
                            </select> 
                        </div>
                    </div>
                  </div>
              </div>
          </div>
      </form>
<div class="table-responsive" style="min-height:300px;">
<table id="example" class="table table-striped table-bordered">
<thead class="table-head">
    <tr>
        <th>Sr#</th>
        <th>Scholarship Name</th>
        <th>Gender</th>
        <th>Name</th>
        <th>CNIC</th>
        <th>Father Name</th>
        <th>Father CNIC</th>
        <th>Reg #</th>
        <th style="display:none;">Father Death Date</th>
        <th style="display:none;">Province</th>
        <th style="display:none;">District</th>
        <th>Faculty</th>
        <th style="display:none;">Deptt</th>
        <th style="display:none;">Program</th>
        <th style="display:none;">CGPA</th>
        <th style="display:none;">Email id of Student</th>
        <th style="display:none;">Contact No (Student)</th>
        <th style="display:none;">Contact No (Guardian)</th>
        <th style="display:none;">Last Degree / Certificate Passed</th>
        <th style="display:none;">Name of  Last Institution Studying</th>
        <th style="display:none;">Marks obatained in FA/Fsc</th>
        <th style="display:none;">Passed In  Year</th>
        <th style="display:none;">Per Month Fee of Last Institution\ college</th>
        <!-- <th style="display:none;">Father Status (Deceased OR Alive, Seperated)</th> -->
        <th style="display:none;">Other Supporting Person</th>
        <th style="display:none;">Father/Guardian's Profession Status (Retired /Jobless/seperated)</th>
        <th style="display:none;">Total No of Family Members</th>
        <th style="display:none;">Total No. of Dependent Family Members</th>
        <th style="display:none;">Family Member Studying</th>
        <th style="display:none;">Earning Hands </th>
        <th style="display:none;">Father / Mother Guardian Income</th>
        <th style="display:none;">Mother Income </th>
        <th style="display:none;">Income from land </th>
        <th style="display:none;">Income from other sources </th>
        <th style="display:none;">Total Monthly Income </th>
        <th style="display:none;">Eelctricity </th>
        <th style="display:none;">Gas </th>
        <th style="display:none;">Telephone</th>
        <th style="display:none;">Water </th>
        <th style="display:none;">Family Expense on education/Semester</th>
        <th style="display:none;">Expense on Food</th>
        <th style="display:none;">Expense on Medical</th>
        <th style="display:none;">Total Monthly Expense</th>
        <th style="display:none;">Accomodation Ownership</th>
        <th style="display:none;">Accommodation/Rent Expense</th>
        <th style="display:none;">Accommodation & Location</th>
        <th style="display:none;">Accomodation Structure</th>
        <th style="display:none;">No of Vehicles</th>
        <th style="display:none;">Vehicle Type (Car OR Motorcycle OR NA)   Model of the Vehicle</th>
        <th style="display:none;">Vehicle Engine Capacity / CC</th>
        <th style="display:none;">Size of agricultural Land (in acres)</th>
        <th style="display:none;">Size of house Land (in Marlas)</th>
        <th style="display:none;">Live Stock/ No. of Cattles</th>
        <th style="display:none;">Monthly Pocket Money</th>
        <th style="display:none;">Reasons of applying </th>
        <th style="display:none;">How were admission charges</th>
        <th style="display:none;">Total outstanding/Unpaid dues (fee section Verfication)</th>
        <th style="display:none;">Financial Assistance earlier received (if any)/will be filled by Fee section also</th>
        <th style="display:none;">Recommended by the Committee (R/NR/W)</th>
        <th style="display:none;">Total Marks </th>
        <th style="display:none;">Recommended Financial Impact (Rs.)</th>
    </tr>
</thead>
<tbody>

<?php $i = 0 ?>
@foreach ($Applicant as $s)
<?php $i++ ?>
   <tr>
   <td>{{$i}}</td>
   <td>{{ App\Http\Controllers\SearchScholarshipsController::getScholorshipTitle($_GET['sid']) }}</td>
   <td>{{($s->gender->gender)??'Nill'}}</td>
   <td>{{($s->name)??'Nill'}}</td>
   <td>{{($s->cnic)??'Nill'}} </td>
   <td>{{($s->ApplicantDetail->fatherName)??'Nill'}}</td>
   <td>{{($s->ApplicantDetail->father_cnic)??'Nill'}}</td>
   <td>{{($s->ScholarshipInformation->reg)??'Nill'}}</td>
   <td style="display:none;">{{($s->ScholarshipFamilyDetail->death_date)??'Nill'}}</td>
   <td style="display:none;">{{($s->applicantDetail->province->provName)??'Nill'}}</td>
   <td style="display:none;">{{($s->address->cityPo)??'Nill'}}</td>
   <td>{{ App\Http\Controllers\SearchScholarshipsController::getFacultyTitle($s->Application[0]->fkFacId) }}</td>
   <td style="display:none;">(Deptartment)</td>
   <td style="display:none;">(Programm)</td>
   <td style="display:none;">{{($s->ScholarshipInformation->cgpa)??'Nill'}}</td>
   <td style="display:none;">{{($s->email)??'Nill'}}</td>
   <td style="display:none;">{{($s->ApplicantDetail->mobile)??'Nill'}}</td>
   <td style="display:none;">{{($s->ApplicantDetail->fatherMobile)??'Nill'}}</td>
   <td style="display:none;">{{($s->ScholarshipInformation->year_of_passing)??'Nill'}}</td>
   <td style="display:none;">{{($s->ScholarshipInformation->last_inst_attend)??'Nill'}}</td>
   <td style="display:none;">{{($s->ScholarshipInformation->marks_obtain)??'Nill'}}</td>
   <td style="display:none;">{{($s->ScholarshipInformation->year_of_passing)??'Nill'}}</td>
   <td style="display:none;">{{($s->ScholarshipInformation->inst_fee)??'Nill'}}</td>
   
   <td style="display:none;">{{($s->ScholarshipFamilyDetail->supporting_person)??'Nill'}}</td>
   <td style="display:none;">{{($s->ApplicantDetail->fatherOccupation)??'Nill'}} </td>
   <td style="display:none;"></td>
   <td style="display:none;">{{($s->ApplicantDetail->dependants)??'Nill'}}</td>
   <td style="display:none;">{{($s->ScholarshipFamilyDetail->family_mem_studing)??'Nill'}}</td>
   <td style="display:none;">{{($s->ScholarshipFamilyDetail->family_mem_earning)??'Nill'}}</td>
   <td style="display:none;">{{($s->ApplicantDetail->monthlyIncome)??'Nill'}}</td>
   <td style="display:none;">{{($s->ScholarshipFamilyDetail->mother_income)??'Nill'}}</td>
   <td style="display:none;">{{($s->ScholarshipFamilyDetail->land_income)??'Nill'}}</td>
   <td style="display:none;">{{($s->ScholarshipFamilyDetail->misc_income)??'Nill'}}</td>
   <td style="display:none;">{{($s->ScholarshipFamilyDetail->monthly_income)??'Nill'}}</td>
   <td style="display:none;">{{($s->ScholarshipFamilyDetail->elect_bill)??'Nill'}}</td>
   <td style="display:none;">{{($s->ScholarshipFamilyDetail->gas_bill)??'Nill'}} </td>
   <td style="display:none;">{{($s->ScholarshipFamilyDetail->tel_bill)??'Nill'}}</td>
   <td style="display:none;">{{($s->ScholarshipFamilyDetail->water_bill)??'Nill'}}</td>
   <td style="display:none;">{{($s->ScholarshipFamilyDetail->edu_expense)??'Nill'}}</td>
   <td style="display:none;">{{($s->ScholarshipFamilyDetail->food_expense)??'Nill'}}</td>
   <td style="display:none;">{{($s->ScholarshipFamilyDetail->medical_expense)??'Nill'}}</td>
   <td style="display:none;">{{($s->ScholarshipFamilyDetail->monthly_expenditure)??'Nill'}}</td>
   <td style="display:none;">{{($s->ScholarshipFamilyAssets->home_status)??'Nill'}}</td>
   <td style="display:none;">{{($s->ScholarshipFamilyAssets->home_value)??'Nill'}}</td>
   <td style="display:none;">{{($s->ScholarshipFamilyAssets->accommodation_land)??'Nill'}}</td>
   <td style="display:none;">{{($s->ScholarshipFamilyAssets->accommodation_type)??'Nill'}}</td>
   <td style="display:none;">{{($s->ScholarshipFamilyAssets->vehicals)??'Nill'}}</td>
   <td style="display:none;"> - </td>
   <td style="display:none;">{{($s->ScholarshipFamilyAssets->engine_cap)??'Nill'}}</td>
   <td style="display:none;">{{($s->ScholarshipFamilyAssets->land_size)??'Nill'}}</td>
   <td style="display:none;">{{($s->ScholarshipFamilyAssets->acco_size)??'Nill'}}</td>
   <td style="display:none;">{{($s->ScholarshipFamilyAssets->no_of_cattles)??'Nill'}}</td>
   <td style="display:none;">{{($s->ScholarshipFamilyDetail->pocket_money)??'Nill'}}</td>
   <td style="display:none;">{{($s->ScholarshipFamilyAssets->reason_for_apply)??'Nill'}} </td>
   <td style="display:none;">{{($s->ScholarshipFamilyAssets->source_of_financing)??'Nill'}} </td>
   <td style="display:none;">{{($s->ScholarshipDetail->feeremarks)??'Nill'}}</td>
   <td style="display:none;">{{($s->ScholarshipDetail->condverremarks)??'Nill'}}</td>
   <td style="display:none;"></td>
   <td style="display:none;">{{($s->ScholarshipInformation->total_marks)??'Nill'}}</td>
   <td style="display:none;"></td>
   </tr>

   @endforeach
</tbody>
                    </table>
                </div> <!-- Ending table-responsive --> 
            </div> <!-- Ending sbox-content - 18 --> 
        </div> <!-- Ending sbox - 14 --> 
    </div> <!-- Ending page-content-wrapper --> 
</div> <!-- Ending page-content --> 


<script type="text/javascript">

        $(document).ready(function() {
          $('#example').DataTable({
           
             dom: 'Bfrtip',
        buttons: [
             'csv'
        ]
          });
        });

</script>


<!-- <script type="text/javascript" src="https://code.jquery.com/jquery-3.5.1.js"></script> -->
<script type="text/javascript" src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/2.2.3/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/2.2.3/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/2.2.3/js/buttons.print.min.js"></script>
@stop