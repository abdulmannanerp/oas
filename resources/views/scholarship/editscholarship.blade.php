@extends('layouts.app')
@section('content')
@php 
use App\Http\Controllers\AddScholarshipController;
@endphp

<div class="page-content row">
    <div class="page-content-wrapper m-t">
        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div> <!-- Ending ending alert-danger -->
        @endif


    <div class="sbox">

        <form action="{{ url('/admin/updatescholarship/'.$Listscholarship->id)}}" class="scholarshipDetailForm" method="POST">
            

        <div class="sbox-title clearfix">
            <h1>Scholarship - Update</h1>
            <div class="sbox-tools pull-right" >
                <button class="tips btn btn-sm btn-save" id="personal-detail-btn" type="submit"><i class="fa fa-save"></i> Update Scholarship</button>
            </div>
        </div>

        <div class="sbox-content clearfix">
        <div class="row">
           <div class="col-md-12">

              <div class="form-group">
                <label for="scholarship_name" class="control-label col-md-4 text-left"> Name </label>
                <div class="col-md-6">
                    <div class="form-group">
                <input  type='text' name='sch_name' id='sch_name' value="{{$Listscholarship->name}}" class='form-control input-sm'/>
            </div>
                 </div>
              </div>

              <div class="form-group" >
                <label for="Start_date" class="control-label col-md-4 text-left"> Start Date </label>
                <div class="col-md-6">
                  <div class="form-group">
                    <input  type='date' name='start_date' id='start_date' min="{{date("Y-m-d")}}" value="{{$Listscholarship->start_date}}" class='form-control input-sm'/>
                  </div>
                </div>
              </div> 

              <div class="form-group" >
                <label for="end_date" class="control-label col-md-4 text-left"> End Date</label>
                <div class="col-md-6">
                    <div class="form-group">
                    <input  type='date' name='end_date' id='end_date' min="{{date("Y-m-d")}}" value="{{$Listscholarship->end_date}}" class="form-control input-sm" />
                    </div>
                </div> 
              </div>

              <div class="form-group" >
                    <label for="status" class=" control-label col-md-4 text-left"> Status </label>
                    <div class="col-md-6">
                        <div class="form-group">
                        <select name='sch_status' rows='5' id='sch_status' class='select2'>
                            <option valu="">--- Select Option ---</option>
                            <option value="1" <?php echo ($Listscholarship->status == '1') ?'selected':''; ?>>Active</option>
                            <option value="0" <?php echo ($Listscholarship->status == '0') ?'selected':''; ?>>InActive</option>
                        </select> 
                    </div>
                    </div> 
                </div>

            </div>
        </div>
        </div>
    </form>
    </div>
    </div> <!-- Ending page-content-wrapper --> 
</div> <!-- Ending page-content --> 

<script type="text/javascript">
    $(document).ready(function() {
    $('#example').DataTable();
} );
</script>
@stop