@extends('layouts.app')
@section('content')
@php 
use App\Http\Controllers\SearchSchController;
@endphp

{{-- <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">--}}
        <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap.min.css">



<style type="text/css">
    #example_wrapper .dataTables_filter {
        display: block !important;
    }
</style>


<div class="page-content row">
    <div class="page-content-wrapper m-t">
        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div> <!-- Ending ending alert-danger --> 
        @endif


        <div class="sbox">

            <div class="sbox-title clearfix">
                <h1>List of Student Applied in Scholarship</h1>
            </div>
            
                    <form method="get" action="/admin/searchsch" onchange="this.submit()">
                        <div class="sbox-content ">
                            <div class="row">
                              <div class="col-lg-4">
                                <div class="form-group">
                                   <div class="form-group">
                                      <label class="req-rd">Select Scholarship</label>
                                        <select class="form-control select2" id="listscholarship" name="sid" required style="width: 100%;">
                                            <option value="">Select Scholarship</option>
                                            @foreach ( $listscholarship as $ls )
                                            <option value="{{$ls->id}}"> {{$ls->name}} </option>
                                            @endforeach
                                        </select> 
                                    </div>
                                </div>
                              </div>
                          </div>
                      </div>
                  </form>


                   <div class="text-right">
                        
                        <a href="/admin/searchsch/export-csv/{{ !empty($_GET['sid']) ? $_GET['sid'] : 0}}" id="export" class="btn btn-success btn-sm"> Export CSV
                        </a>
                  </div>



                    <div class="table-responsive" style="min-height:300px;">

                    <table id="example" class="table table-striped table-bordered" style="width:100%">
                        <thead class="table-head">
                            <tr>
                                <th>Scholarship Name</th>
                                <th>Gender</th>
                                <th>Name</th>
                                <th>CNIC</th>
                                <th>Father Name</th>
                                <th>Father CNIC</th>
                                <th>Reg #</th>
                                <th>Faculty</th>
                            </tr>
                        </thead>
                        <tfoot class="table-head">
                            <tr>
                                <th>Scholarship Name</th>
                                <th>Gender</th>
                                <th>Name</th>
                                <th>CNIC</th>
                                <th>Father Name</th>
                                <th>Father CNIC</th>
                                <th>Reg #</th>
                                <th>Faculty</th>
                            </tr>
                        </tfoot>
                    </table>
                </div> <!-- Ending table-responsive --> 
            </div> <!-- Ending sbox-content - 18 --> 
        </div> <!-- Ending sbox - 14 --> 
    </div> <!-- Ending page-content-wrapper --> 
</div> <!-- Ending page-content --> 


<script type="text/javascript">
  $(document).ready(function () {
    $('#example').DataTable({
        processing: true,
        serverSide: true,

        ajax: {
            url : '{{ route('scholarship.fetch') }}?sid={{ !empty($_GET["sid"]) ? $_GET["sid"] : 0}}',
            dataType : 'json',
            type : 'GET'
        }
    });
  });

</script>

<script type="text/javascript">
    function exportTasks(_this) {
      let _url = $(_this).data('href');
      window.location.href = _url;
   }
</script>

<!-- <script type="text/javascript" src="https://code.jquery.com/jquery-3.5.1.js"></script> -->

    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap.min.js"></script>

    
<!-- <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/2.2.3/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/2.2.3/js/buttons.print.min.js"></script> -->
@stop