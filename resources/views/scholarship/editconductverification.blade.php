@extends('layouts.app')
@section('content')
@php 
use App\Http\Controllers\SchConductVerificationController;
@endphp

<div class="page-content row">
    <div class="page-content-wrapper m-t">
        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div> <!-- Ending ending alert-danger -->
        @endif


    <div class="sbox">

        <form action="{{ route('updateverificationstatus',['id' => $SchView]) }}" class="scholarshipDetailForm" class="scholarshipDetailForm" method="POST">
            

        <div class="sbox-title clearfix">
            <h1>Scholarship - Update Conduct Verification Status</h1>
            <div class="sbox-tools pull-right" >
                <button class="tips btn btn-sm btn-save" id="personal-detail-btn" type="submit"><i class="fa fa-save"></i> Update Status</button>
            </div>
        </div>

        <div class="sbox-content clearfix">

        <div class="row">
           <div class="col-md-12">
            <div class="form-group">
                <label for="scholarship_name" class="control-label col-md-4 text-left"> Student Name </label>
                <div class="col-md-6">
                    <div class="form-group">
                <input  type='text' name='sch_name' id='sch_name' value="{{$ApplicantDetail->applicant->name}}" class='form-control input-sm' readonly />
            </div>
                 </div>
              </div>


              <div class="form-group">
                <label for="scholarship_name" class="control-label col-md-4 text-left"> Scholarship Name </label>
                <div class="col-md-6">
                    <div class="form-group">
                <input  type='text' name='sch_name' id='sch_name' value="{{$scholarshipname->name}}" class='form-control input-sm' readonly/>
            </div>
                 </div>
              </div>

              <div class="form-group" >
                    <label for="status" class=" control-label col-md-4 text-left"> Conduct Verification Status </label>
                    <div class="col-md-6">
                        <div class="form-group">
                        <select name='con_ver_status' rows='5' id='con_ver_status' class='select2'>
                            <option valu="">--- Select Option ---</option>
                            <option value="0" <?php echo ($SchView->conductverfication == '0') ?'selected':''; ?>>In Progress</option>
                            <option value="1" <?php echo ($SchView->conductverfication == '1') ?'selected':''; ?>>Verified</option>
                            <!-- <option value="2" <?php echo ($SchView->conductverfication == '2') ?'selected':''; ?>>Rejected</option> -->
                        </select> 
                    </div>
                    </div> 
                </div>

              <div class="form-group" >
                <label for="end_date" class="control-label col-md-4 text-left"> Remarks</label>
                <div class="col-md-6">
                    <div class="form-group">
                        <textarea class="form-control input-sm" rows="4" id="con_ver_remarks" name="con_ver_remarks">{{$SchView->condverremarks ?? old('condverremarks')}}</textarea>
                    </div>
                </div> 
              </div>
              
            </div>
        </div>
        </div>
    </form>
    </div>
    </div> <!-- Ending page-content-wrapper --> 
</div> <!-- Ending page-content --> 

<script type="text/javascript">
    $(document).ready(function() {
    $('#example').DataTable();
} );
</script>
@stop