@extends('layouts.app')
@section('content')
@php 
use App\Http\Controllers\AddScholarshipController;
@endphp


<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.12.1/css/jquery.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/2.2.3/css/buttons.dataTables.min.css">
<style type="text/css">
    #example_wrapper .dataTables_filter {
        display: block !important;
    }
</style>


<div class="page-content row">
    <div class="page-content-wrapper m-t">
        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div> <!-- Ending ending alert-danger --> 
        @endif


    <div class="sbox">

        <form action="{{route('addscholarship')}}" class="scholarshipDetailForm" method="POST" enctype="multipart/form-data">
            
        <div class="sbox-title clearfix">
            <h1>Scholarship - Add new Scholarships</h1>
            <div class="sbox-tools pull-right" >
                <button class="tips btn btn-sm btn-save" id="personal-detail-btn" type="submit"><i class="fa fa-save"></i> Save</button>
            </div>
        </div>

        <div class="sbox-content clearfix">
        <div class="row">
           <div class="col-md-12">

              <div class="form-group">
                <label for="scholarship_name" class="control-label col-md-4 text-left"> Name </label>
                <div class="col-md-6">
                    <div class="form-group">
                <input  type='text' name='sch_name' id='sch_name' class='form-control input-sm'/>
            </div>
                 </div>
              </div>

              <div class="form-group" >
                <label for="Start_date" class="control-label col-md-4 text-left"> Start Date </label>
                <div class="col-md-6">
                  <div class="form-group">
                    <input  type='date' name='start_date' id='start_date' class='form-control input-sm' min="{{date("Y-m-d")}}" />
                  </div>
                </div>
              </div> 

              <div class="form-group" >
                <label for="end_date" class="control-label col-md-4 text-left"> End Date</label>
                <div class="col-md-6">
                    <div class="form-group">
                    <input  type='date' name='end_date' id='end_date' class='form-control input-sm' min="{{date("Y-m-d")}}" />
                    </div>
                </div> 
              </div>

              <div class="form-group" >
                    <label for="status" class=" control-label col-md-4 text-left"> Status </label>
                    <div class="col-md-6">
                        <div class="form-group">
                        <select name='sch_status' rows='5' id='sch_status' class='select2'>
                            <option>--- Select Option ---</option>
                            <option value="1">Active</option>
                            <option value="0">InActive</option>
                        </select> 
                    </div>
                    </div> 
                </div>
            </div>
        </div>
        </div>
    </form>
    </div>

        <div class="sbox">
            <div class="sbox-title">
                <h1>List Scholarship</h1>
            </div> <!-- Ending sbox-title -->

            <div class="sbox-content ">
                <div class="table-responsive" style="min-height:300px;">
                    <table id="example" class="table table-striped table-bordered">
                    <thead class="table-head">
                        <tr>
                            <th>Name</th>
                            <th>Start Date</th>
                            <th>End Date</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                            @foreach ( $SchView as $s )
                            <tr>
                                <td> {{ $s->name }} </td>
                                <td> {{ date('d-m-Y', strtotime($s->start_date))}} </td>
                                <td> {{ date('d-m-Y', strtotime($s->end_date))}} </td>
                                <td> <?php echo ($s->status==1)?'Active':'Inactive'; ?></td>
                                <td>
                                 <a href="{{url('/admin/editscholarship/'.$s->id)}}" type="button" class="btn btn-primary btn-xs">
                                <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Edit
                                </a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div> <!-- Ending table-responsive --> 
            </div> <!-- Ending sbox-content - 18 --> 
        </div> <!-- Ending sbox - 14 --> 
    </div> <!-- Ending page-content-wrapper --> 
</div> <!-- Ending page-content --> 
<script type="text/javascript">

        $(document).ready(function() {
          $('#example').DataTable({
            pageLength: 10,
            filter: true,
            deferRender: true,
            scrollY: 200,
            scrollCollapse: false,
            scroller: false,
            "searching": true,
             dom: 'Bfrtip',
        buttons: [
        ]
          });
        });

</script>
<!-- <script type="text/javascript" src="https://code.jquery.com/jquery-3.5.1.js"></script> -->
<script type="text/javascript" src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/2.2.3/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>


@stop