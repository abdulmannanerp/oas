@extends('layouts.app')


@section('content')
    <div class="page-content row">
        <div class="page-content-wrapper m-t">
            <form action="/sms" method="POST">
                {{ csrf_field() }}
                <div class="form-group">
                    <label for="">Enter recipient phone</label>
                    <input type="number" class="form-control" name="phone" placeholder="923338999963">
                </div>
                <div class="form-group">
                    <label for="">Message Body</label>
                    <textarea name="body" class="form-control" id="" cols="30" rows="10" placeholder="Dear Applicant, Your fee has been verified."></textarea>
                </div>
                <input type="submit" value="Send" class="btn btn-primary">
            </form>
        </div>
    </div>
@stop
