<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>NOC Form</title>	
  <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
  <link href="{{asset('css/hec-performa.css')}}" rel="stylesheet" type="text/css" media="all">
</head>
<body>
  <div class="container wdt-fx">
    <div class="row">
        <div class="col-lg-2 wd-10 fl-lft">
          <img style="width:100px;"src="{{asset('storage/images/hec.png')}}" />
        </div>
        <div class="col-lg-8 wd-68 fl-lft">
          <div class="heading-hec">
            <h4>HIGHER EDUCATION COMMISSION, PAKISTAN</h4>
            <h4>Academic Division</h4>
            <p>Foreign Student's Information Sheet</p>
            <h4>Email: nocforeign@hec.gov.pk</h4>
          </div>
        </div>
        <div class="col-lg-2 wd-10 fl-lft">
          <div class="image-wdth-joining">
            Attach Picture 
          </div>
        </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <table class="table table-bordered" >
          <tr class="bg-primary bg-custom-clr">
            <th colspan="4" class="txt-cntr wd-100">
                Personal Details
            </th>
          </tr>
          {{-- {{dd($oas_applications->applicant->address)}} --}}
          <tr>
            <td class="col-md-2 bg-primary bg-custom-clr wd-10">Name</td>
            <td class="col-md-10 wd-90" colspan="3">{{$oas_applications->applicant->name ?? ''}}</td>
          </tr>
          <tr>
            <td class="col-md-2 bg-primary bg-custom-clr wd-10">Address In Pakistan</td>
            <td class="col-md-10 wd-90" colspan="3">{{$oas_applications->applicant->address->addressPo ?? ''}}</td>
          </tr>
          <tr>
            <td class="col-md-2 bg-primary bg-custom-clr wd-10">Address In Home Country</td>
            <td class="col-md-10 wd-90" colspan="3">{{$oas_applications->applicant->address->addressFather ?? ''}}</td>
          </tr>
          <tr>
            <td class="col-md-2 bg-primary bg-custom-clr wd-10">Date Of Birth</td>
            <td class="col-md-4 wd-40">{{$oas_applications->applicant->applicantDetail->DOB ?? ''}}</td>
            <td class="col-md-2 bg-primary bg-custom-clr wd-10">Place Of BIrth</td>
            <td class="col-md-4 wd-40">{{$oas_applications->applicant->applicantDetail->place_of_birth ?? ''}}</td>
          </tr>
          <tr>
            <td class="col-md-2 bg-primary bg-custom-clr wd-10">Nationality</td>
            <td class="col-md-4 wd-40">{{$oas_applications->applicant->nationality->nationality ?? ''}}</td>
            <td class="col-md-2 bg-primary bg-custom-clr wd-10">Email</td>
            <td class="col-md-4 wd-40">{{$oas_applications->applicant->email ?? ''}}</td>
          </tr>
          <tr>
            <td class="col-md-2 bg-primary bg-custom-clr wd-10">Contact Number</td>
            <td class="col-md-4 wd-40">{{$oas_applications->applicant->applicantDetail->mobile ?? ''}}</td>
            <td class="col-md-2 bg-primary bg-custom-clr wd-10">Passport Number</td>
            <td class="col-md-4 wd-40">{{$oas_applications->applicant->applicantDetail->passport_number ?? ''}}</td>
          </tr>
          <tr>
            <td class="col-md-2 bg-primary bg-custom-clr wd-10">Date Of Passport Issue</td>
            <td class="col-md-4 wd-40">{{$oas_applications->applicant->applicantDetail->date_issue ?? ''}}</td>
            <td class="col-md-2 bg-primary bg-custom-clr wd-10">Place Of Passport Issue</td>
            <td class="col-md-4 wd-40">{{$oas_applications->applicant->applicantDetail->place_issue ?? ''}}</td>
          </tr>
        </table>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <table class="table table-bordered" >
          <tr class="bg-primary bg-custom-clr">
            <th colspan="4" class="txt-cntr">
                Previous Education In Pakistan (If Applicable)
            </th>
          </tr>
          <tr>
            <td class="col-md-2 bg-primary bg-custom-clr wd-19" style="width: 132px">University</td>
            <td class="col-md-10" colspan="3"></td>
          </tr>
          <tr>
            <td class="col-md-2 bg-primary bg-custom-clr wd-19">Degree Completed or Not.</td>
            <td class="col-md-4"  style="width: 210px"></td>
            <td class="col-md-2 bg-primary bg-custom-clr wd-19" style="width: 152px">If not completed (please attach clearance certificate)</td>
            <td class="col-md-4"></td>
          </tr>
          <tr>
            <td class="col-md-2 bg-primary bg-custom-clr wd-19">Course/Program</td>
            <td class="col-md-4"></td>
            <td class="col-md-2 bg-primary bg-custom-clr wd-19">Duration</td>
            <td class="col-md-4"></td>
          </tr>
          <tr>
            <td class="col-md-2 bg-primary bg-custom-clr wd-19">HEC NOC Letter Number</td>
            <td class="col-md-4">{{$oas_applications->applicant->applicantDetail->hec_letter_no ?? ''}}</td>
            <td class="col-md-2 bg-primary bg-custom-clr wd-19">HEC NOC Letter Date</td>
            <td class="col-md-4">{{$oas_applications->applicant->applicantDetail->hec_letter_date ?? ''}}</td>
          </tr>

        </table>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <table class="table table-bordered" >
          <tr class="bg-primary bg-custom-clr">
            <th colspan="4" class="txt-cntr">
                Visa Details (If Applicable)
            </th>
          </tr>
          <tr>
            <td class="col-md-2 bg-primary bg-custom-clr wd-19" style="width: 132px">Pakistani Visa No.</td>
            <td class="col-md-4" style="width: 210px">{{$oas_applications->applicant->applicantDetail->pakistani_visa ?? ''}}</td>
            <td class="col-md-2 bg-primary bg-custom-clr wd-19" style="width: 152px">Type Of Visa</td>
            <td class="col-md-4">{{$oas_applications->applicant->applicantDetail->type_of_visa ?? ''}}</td>
          </tr>
          <tr>
            <td class="col-md-2 bg-primary bg-custom-clr wd-19">Visa Issue Date</td>
            <td class="col-md-4">{{$oas_applications->applicant->applicantDetail->visa_issue_date ?? ''}}</td>
            <td class="col-md-2 bg-primary bg-custom-clr wd-19">Visa Expiry Date</td>
            <td class="col-md-4">{{$oas_applications->applicant->applicantDetail->visa_expiry_date ?? ''}}</td>
          </tr>
        </table>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="bg-primary bg-custom-clr txt-cntr head-app no-mar-no-pad">
          University Confirmation
        </div>
      </div>
    </div>
    <div class="border-custom">
      <div class="row">
        <div class="col-md-12">
          <p>This is to certify that Mr./Ms <b class="text-under">{{$oas_applications->applicant->title ?? ''}}</b> Passport Number <b class="text-under">{{$oas_applications->applicant->applicantDetail->passport_number ?? ''}}</b> 
            has been granted provional admission in <b class="text-under">{{$oas_applications->program->name ?? ''}}</b> program of <b class="text-under">{{$oas_applications->program->duration ?? ''}}</b>
            years/months duration starting from _________ in the <b class="text-under">{{$oas_applications->program->department->title ?? ''}}</b>
          at this University/Institution. It is verified that the said program is also included in 
          <b>Pakistan Qualification Register (PQR)</b> of Higher Education Commision, Pakistan.</p>
        </div>
      </div>
      <div class="row">
        <div class="col-md-9 wd-70 fl-lft mr-tp-s">
          <b>Name and Signature of authorized Officer__________________________</b> <br />
          Date: _______________
        </div>
        <div class="col-md-3 wd-20 fl-lft mr-lft-20">
          <div class="dotted-border">
            Official Stamp
          </div>
        </div>
      </div>
      <div class="row padding-20">
        <div class="col-md-4 wd-100">
          University/Institution:  <b class="text-under">International Islamic University Islamabad</b>
        </div>
      </div>
      <div class="row">
          <div class="col-md-6 wd-50 fl-lft">
            Email <b class="text-under"> {{$oas_applications->applicant->email ?? ''}}</b>
          </div>
          <div class="col-md-6 wd-50 fl-lft">
            Phone <b class="text-under"> {{$oas_applications->applicant->applicantDetail->mobile ?? ''}}</b>
          </div>
      </div>
    </div>
  </div> <!-- Ending container --> 
</body>
</html>