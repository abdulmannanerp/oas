<!DOCTYPE html>

<html lang="en-US">	
    <head>
        <meta charset="utf-8">	
    </head>	
    <body>
        Dear {{$name}}, <br /><br />
        Your application for <b>{{$program_name ?? ''}}</b> has been accepted for further processing. <br /><br />
        For Test and Interview schedule please visit <a href="https://admission.iiu.edu.pk/programes">https://admission.iiu.edu.pk/programes</a> . The merit criteria can be found at <a href="http://admission.iiu.edu.pk/programes">http://admission.iiu.edu.pk/programes</a> OR visit at <a href="https://www.iiu.edu.pk/?page_id=1007#merit">https://www.iiu.edu.pk/?page_id=1007#merit.</a> Your Roll No slip can be found in online admission account at <a href="http://admission.iiu.edu.pk">http://admission.iiu.edu.pk </a> <br /><br />Regards, <br />Admission Office <br />International Islamic University, Islamabad (IIUI)<br /><a href="https://www.iiu.edu.pk/?page_id=1007">https://www.iiu.edu.pk/?page_id=1007</a> <br />
        Please note that no admission test will be conducted for Undergraduate programs (BS and MA/MSc) except for BS Engineering, CS, IT and SE Programs.<br />
        {{-- Please report for admission test/interview scheduled along with Roll No slip/Submitted Fee challan as per given Test/Interview schedule at <a href="http://admission.iiu.edu.pk/programes">http://admission.iiu.edu.pk/programes</a> OR visit at <a href="https://www.iiu.edu.pk/?page_id=1007#merit">https://www.iiu.edu.pk/?page_id=1007#merit.</a> Your Roll No slip can be found in online admission account at <a href="http://admission.iiu.edu.pk">http://admission.iiu.edu.pk </a> <br /><br />Regards, <br />Admission Office <br />International Islamic University, Islamabad (IIUI)<br /><a href="http://admission.iiu.edu.pk">http://admission.iiu.edu.pk/</a><br /> --}}
        {{-- Please note that no admission test will be conducted  (except BS Engineering Programs).<br />
        For merit criterion visit at <a href="https://www.iiu.edu.pk/?page_id=1007#merit">https://www.iiu.edu.pk/?page_id=1007#merit</a><br />
        Interview will be conducted for MS/LLM/PhD programs.<br /> --}}
        Roll Number slip can be downoladed from online admission account at <a href="http://admission.iiu.edu.pk">http://admission.iiu.edu.pk </a><br /><br />
        Regards, <br />
        Admission Office <br />
        International Islamic University, Islamabad (IIUI)<br />
        <a href="http://admission.iiu.edu.pk">http://admission.iiu.edu.pk/</a><br /> 
    </body>
</html>