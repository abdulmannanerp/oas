<!DOCTYPE html>

<html lang="en-US">	
    <head>
        <meta charset="utf-8">	
    </head>	
    <body>
        Dear {{$name}}, <br /><br />
        Result has been published. For more details please visit admission.iiu.edu.pk%0aAdmission Office<br /><br />
        Regards, <br />
        Admission Office <br />
        International Islamic University, Islamabad (IIUI)<br />
        <a href="http://admission.iiu.edu.pk">http://admission.iiu.edu.pk/</a><br /> 
    </body>
</html>