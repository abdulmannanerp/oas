
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Application Form</title>
        <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    </head>

    <body>
        <style>
            body{
                font-size:14px;
                line-height:18px;
                color:#000;
                font-family:Arial, Helvetica, sans-serif;
            }
            .text-center{
                text-align:center;
            }
            .text-right{
                text-align:right;
            }
            .tp-ps-ab{
                position: absolute;
                top: 6%;
                right: 2%;
            }
            p{
                margin:0px 0px 2px 0px;
            }
            .admission-form-main{
                width:700px;
                margin:0 auto;
            }
            .admission-form-main p{
                font-weight:bold;
            }		
            table {
                border-collapse: collapse;
                border-spacing: 0;
                width: 100%;
                border: 1px solid #ddd;
            }

            th, td {
                text-align: left;
                padding:5px 5px;
                border: 1px solid #ddd;
            }
            .table-main{
                padding-bottom:10px;
                width:100%;
            [class*="col-"] {
                float: left;

            }
            .col-1 {width: 8.33%;}
            .col-2 {width: 16.66%;}
            .col-3 {width: 25%;}
            .col-4 {width: 33.33%;}
            .col-5 {width: 41.66%;}
            .col-6 {width: 50%;}
            .col-7 {width: 58.33%;}
            .col-8 {width: 66.66%;}
            .col-9 {width: 75%;}
            .col-10 {width: 83.33%;}
            .col-11 {width: 91.66%;}
            .col-12 {width: 100%;}	
            @media (max-width:500px){ 
                .row, .text-right{
                    text-align:center !important;
                }
                .col-1, .col-2, col-3, .col-4, .col-5, .col-6, .col-7, .col-8, .col-9, .col-10, .col-11, .col-12{
                    width:100%;
                }
                .table-main{
                    width:100%;
                    overflow:scroll;

                }
                .admission-form-main {
                    width:100%;

                }
                .table-main table{
                    width:700px;
                }		

            }	
            @media print { 
                .applicant-signature {
                    padding-top:30px;

                }
                #printpagebutton{
                    display:none;	

                }
                .name-mail {
                    padding-bottom:5px;
                }
                .table-main{
                    padding-bottom:5px;
                    width:100%;
                }
                body{
                    font-size:12px;
                }
                table{
                    width:100% !important;
                    overflow:hidden !important;
                }
                .admission-form-main {
                    width:98% !important;
                    overflow:hidden !important;

                }	

            }

            .row p{
                font-size:12px;

            }	
            h4, h3{
                margin:7px 0px;
                line-height:18px;
            }	
            h1{
                line-height:28px;
                color:#777;
            }	
            .name-mail {
                padding-bottom:10px;
            }	
            .name-mail p, .decalartion p{
                font-weight:normal !important;
            }	
            .name-mail p span{
                font-weight:bold !important;
            }
            .decalartion p{
                line-height:20px;
                margin-bottom:15px;
                text-align: justify;
            }				
            .decalartion{
                padding-top:15px;
            }


            .name-mail tr td {
                padding:4px 5px;
                font-size:12px;
                line-height:10px;

            }
            .btn {
                display: inline-block;
                padding: 6px 12px;
                margin-bottom: 0;
                font-size: 14px;
                font-weight: 400;
                line-height: 1.42857143;
                text-align: center;
                white-space: nowrap;
                vertical-align: middle;
                -ms-touch-action: manipulation;
                touch-action: manipulation;
                cursor: pointer;
                -webkit-user-select: none;
                -moz-user-select: none;
                -ms-user-select: none;
                user-select: none;
                background-image: none;
                border: 1px solid transparent;
                border-radius: 4px;
            }

            .under-tr td{
                height: 25px;
            }
            .less-fifty{
                color: #FF0000;
            }
            .education-detail td:last-child{
                text-align: right;
            }
            .mar-tp-10{
                margin-top: 10px;
            }
            .disability-section td:nth-of-type(1) {
                width: 100px;
                min-width: 100px;
                max-width: 100px;
            }
            .disability-section td:nth-of-type(2) {
                width: 100px;
                min-width: 100px;
                max-width: 100px;
            }   
            .pd-tp-hs{
                padding-top: 15px;
            }
            .bld-undr{
                text-align: center;
                text-decoration: underline;
                font-weight: bold;
            }
            .mar-tp{
                padding-top: 40px;
            }         
        </style>
        <button id="printpagebutton" class="btn btn-default" style="float:right; margin-top: 1em;" type="button"  onclick="printpage()"/><i class="fa fa-print"></i> Print</button>
        <div class="admission-form-main">
<?php 
$text_1 = '';
$text_2 = '';
// $dates_check = $appli_semes->fkSemesterId;

if($current_semester == 11){
    $text_1 = 'September 2 to 8, 2019';
    $text_2 = 'September 9 to 15, 2019';
}
if($current_semester == 12){
    $text_1 = 'February 3 to 9, 2020';
    $text_2 = 'February 10 to 16, 2020';
    
}
if($current_semester == 13){
    $text_1 = 'September 14 to 18, 2020';
    $text_2 = 'September 21 to 25, 2020';
    
}
if($current_semester == 14){
    $text_1 = 'February 1 to 5, 2021';
    $text_2 = 'February 8 to 12, 2021';
}
if($current_semester == 15){
    $text_1 = 'September 6 to 10, 2021';
    $text_2 = 'September 13 to 17, 2021';
}
if($current_semester == 16){
    $text_1 = 'February 1 to 7, 2022';
    $text_2 = 'February 8 to 15, 2022';
}
if($current_semester == 17){
    $text_1 = 'September 1 to 7, 2022';
    $text_2 = 'September 8 to 15, 2022';
}
if($current_semester == 18){
    $text_1 = 'February 1 to 7, 2023';
    $text_2 = 'February 8 to 15, 2023';
}
if($current_semester == 19){ 
    $text_1 = 'September 1 to 7, 2023';
    $text_2 = 'September 8 to 15, 2023';
}
$appli_semes = $oas_applications->applicant->previousQualification($current_semester)->first();
    ?> 
            <div class="row" style="padding-bottom:15px;">

                <div class="col-2 text-center"><img width="80" src="/storage/images/iiulogo.jpg" /></div>
                <div class="col-8 text-center">
                    <img width="250" src="/storage/images/jami.jpg" />
                    <h3>INTERNATIONAL ISLAMIC UNIVERSITY, ISLAMABAD</h3>
                    <h4>Admission FORM FOR 
                        <?php
                        if($oas_applications->program->pkProgId == 189 && $oas_applications->applicant->fkGenderId == 2){
                            echo 'BS Electrical';
                       }else{
                           echo $oas_applications->program->title;
                           if($oas_applications->program->pkProgId == 65){
                                echo '<br />Specialization: '.$appli_semes->phd_usuludin_specialization;
                            }
                           if($oas_applications->program->pkProgId == 66){
                                echo '<br />Specialization: '.$appli_semes->ms_usuludin_specialization;
                            }
                       }
                            ?>
                         </h4> <!-- Dev Mannan: Upper Case -->
                </div>
                <div class="col-2 text-right">
                    <p>Form No: {{$oas_applications->id}}</p>
                </div>
<?php
    if($oas_applications->applicant->fkNationality == 1){
        $admission_fee = '<li>Admission Processing Fee Rs.1500/- is non-refundable / non-adjustable.</li>';
        $instruction_1 = '';
        $instruction_2 = '';
        // $admission_fee_1 = ''; 
        $bank_details = '';
      }
      if($oas_applications->applicant->fkNationality == 2 || $oas_applications->applicant->fkNationality == 3){
        $admission_fee = '';
        $instruction_1 = '<li>Admission shall be granted to all foreign/Overseas Pakistani candidates in all degree programs on the basis of paper qualification except F/o Engineering & Technology after fulfillment the admission requirements.</li>
        <li>All Refugees residing in Pakistan are required to appear in Entry Test/Interview for admission in all degree programs as local candidates as per schedule</li>';
        $instruction_2 = '<li>Please follow following instructions at the time of form submission
        <ul>
            <li>Download Admission processing fee challan</li>
            <li>Submit required amount in the bank</li>
            </ul></li>
        <li>Please attach following required documents and send through email on overseas.admissions@iiu.edu.pk for (Male candidates) and  overseas.female.admissions@iiu.edu.pk for (Female candidates)
            <ul>
            <li>Paid Fee Challan</li>
            <li> Picture </li>
            <li> Passport </li>
            <li> All Certificates/degrees (10th Grade, 12th Grade, Bachelor, Master) </li>
            <li> No. Objection Certificate from M/o Foreign Affairs Or your Embassy in Pakistan </li>
            <li> Residential Permit (Aqamah)  </li>
            <li> NICOP (for Overseas Pakistani only).  </li>
            <li> Foreign Students Information Sheet (Required for NOC of HEC and study Visa)  </li>
            </ul>
          </li>';
        //   $admission_fee_1 = ' or equal amount in Pak Rupee by Swift/Electronic Func Transfer through following bank details';
          $bank_details = '<li>Please Deposit Non-refundable Admission Processing Fee USD $75/- or equal amount in Pak Rupees by Swift/Electronic Fund Transfer through following bank details. </li>
          <table class="table table-striped">
            <thead>
            <tr>
              <th>Title of account</th>
              <th>IIUI Foreign Currency / Account</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>Account No.</td>
                <td>5006-79008735-10</td>
              </tr>
              <tr>
                <td>Currency</td>
                <td>US $</td>
              </tr>
              <tr>
                <td>Bank</td>
                <td>Habib Bank Limited</td>
              </tr>
              <tr>
                <td>Branch Code</td>
                <td>5006</td>
              </tr>
              <tr>
                <td>Branch Address</td>
                <td>HBL, IBB-IIU, Islamabad Branch, Sector H-10, Islamabad, Pakistan.</td>
              </tr>
              <tr>
                <td>Swift Code</td>
                <td>HABBPKKA</td>
              </tr>
              <tr>
                <td>IBAN</td>
                <td>PK39-HABB-0050-0679-0087-3510</td>
              </tr>
            </tbody></table>';
      }
?>
            </div>
            <h3>Important Instructions Before Applying</h3>
            <ul>
            <li>
                Please check admission eligibility criteria carefully. Apply only if qualify as per requirement.
            </li>
            {{-- <li>
                Please check test schedule carefully if applying for more than one program as IIUI shall not be responsible for any clash of test dates.
            </li> --}}
            <?= $admission_fee ?>
            <?= $bank_details ?>
            <?= $instruction_1 ?>
            <?= $instruction_2 ?>
            <li>
                University Fee once paid is refunded only as per below mentioned HEC policy for all candidates including those who applied / admitted on result awaiting basis:
                <ul>
                    <li>
                        Full (100%) fee refund (Excluding admission fee) up to 7 th day ({{$text_1}} including Saturday  &amp; Sunday)  after commencement of classes as per academic calendar notified by the University.
                    </li>
                    <li>
                        Half (50%) fee refund (Excluding admission fee) from 8 th – 15 th day ({{$text_2}} including Saturday &amp; Sunday) after commencement of classes as per the academic calendar notified by the University
                    </li>
                    <li>
                        No fee refund thereafter except refundable security (from 16th day onwards).
                    </li>
                    <li>
                        The students on provisional admission (result awaiting status) failing to obtain the required percentage as per advertised eligibility criteria will be refunded 100% of fee within 10 days of the declaration of result from respective board.
                    </li>
                </ul>
            </li>
            </ul>
            <hr />

            <div class="row name-mail">

                <div class="col-6">



                    <table>
                        <tr>
                            <td>Name:</td>
                            <td><b>{{$oas_applications->applicant->name}} </b></td>

                        </tr>
                        <tr>
                            <td>Father's Name:</td>
                            <td><b>{{$oas_applications->applicant->applicantDetail->fatherName}}</b></td>

                        </tr>
                        <tr>
                            <td>Date of Birth:</td>
                            <td><b>{{$oas_applications->applicant->applicantDetail->DOB}}</b></td>

                        </tr>

                        <tr>
                            <td>CNIC/Passport No: </td>
                            <td><b>{{$oas_applications->applicant->cnic}}</b></td>

                        </tr>
                        <?php
                        if($oas_applications->applicant->fkNationality == 2){
                        ?>
                         <tr>
                            <td>NICOP No: </td>
                            <td><b>{{$oas_applications->applicant->applicantDetail->nicop}}</b></td>

                        </tr>
                        <?php
                        }
                        ?>
                        <tr>
                            <td>Gender:</td>
                            <td>
                                <b>
                                    <?php 
                                    if ($oas_applications->applicant->gender_other != NULL){
                                        echo $oas_applications->applicant->gender_other;
                                    }else{
                                        echo $oas_applications->applicant->gender->gender;
                                    }
                                    ?>
                                </b>
                            </td>

                        </tr>


                        <tr>
                            <td>Mobile:</td>
                            <td><b>{{$oas_applications->applicant->applicantDetail->mobile}}</b></td>

                        </tr>


                        <tr>
                            <td>Nationality: </td>
                            <td><b>{{$oas_applications->applicant->nationality->nationality}}  </b></td>

                        </tr>
                        <tr>
                            <td>Religion</td>
                            <td><b>{{$oas_applications->applicant->applicantDetail->religion}}</b></td>
                        </tr>


                        <tr>
                            @if($oas_applications->applicant->applicantDetail->province)
                            <td>Domicile  </td>
                            <td><b>{{$oas_applications->applicant->applicantDetail->province->provName}} 
                                    @else 
                                </b></td>

                        </tr>


                        <tr>
                            <td>Country: </td>

                            <td><b>{{$oas_applications->applicant->applicantAddress->country->countryName}}
                                    @endif
                                </b></td>

                        </tr>


                        <tr>
                            <td>Email:</td>
                            <td><b>{{$oas_applications->applicant->email}} </b></td>

                        </tr>


                    </table>




                <?php 
                // $exploded = explode('.', $oas_applications->applicant->applicantDetail->pic);
                // $pic = str_replace('.'.end($exploded), "_thumb.jpg" , $oas_applications->applicant->applicantDetail->pic);
                ?>
                </div>
                <?php /* ?>
                <div class="col-3 text-center">
                    <h2>
                        <?php
                        if($oas_applications->program->pkProgId == 189 && $oas_applications->applicant->fkGenderId == 2){
                            echo 'BS Electrical';
                       }else{
                           echo $oas_applications->program->title;
                       }
                        ?>
                        </h2>
                </div>
                <?php */ ?>
                <div class="col-3 text-right tp-ps-ab">
                        <img src="/storage/{{($oas_applications->applicant->applicantDetail->pic == '')? '/images/default.png': $oas_applications->applicant->applicantDetail->pic}}" id="pic1" alt="Profile Pic" class="img-thum">
                    {{-- <img width="150" height="130" src="http://admission.iiu.edu.pk/{{$oas_applications->applicant->applicantDetail->pic}}" /> --}}
                </div>

            </div>
            <?php
            if ($oas_applications->preferrence1 == 1) {
                $pref1 = 'EE';
            } elseif ($oas_applications->preferrence1 == 2) {
                $pref1 = 'ME';
            } elseif ($oas_applications->preferrence1 == 3) {
                $pref1 = 'CE';
            } else {
                $pref1 = '';
            }
            if ($oas_applications->preferrence2 == 1) {
                $pref2 = 'EE';
            } elseif ($oas_applications->preferrence2 == 2) {
                $pref2 = 'ME';
            } elseif ($oas_applications->preferrence2 == 3) {
                $pref2 = 'CE';
            } else {
                $pref2 = '';
            }
            if ($oas_applications->preferrence3 == 1) {
                $pref3 = 'EE';
            } elseif ($oas_applications->preferrence3 == 2) {
                $pref3 = 'ME'; 
            } elseif ($oas_applications->preferrence3 == 3) {
                $pref3 = 'CE';
            } else {
                $pref3 = '';
            }
            ?>




            <div class="table-main">
                <table>
                    <tr bgcolor="#f8f8f8">
                        <td>Program:</td>
                       <?php /*  <td><b>{{$oas_applications->program->title}}<?php if ($pref1 != '' && $pref2 != '' && $pref3 != '') echo ' (' . $pref1 . ') ' . '(' . $pref2 . ') ' . '(' . $pref3 . ') ' ?> </b></td>  */ ?>
                       <td><b><?php
                       if($oas_applications->program->pkProgId == 189 && $oas_applications->applicant->fkGenderId == 2){
                            echo 'BS Electrical';
                       }else{
                           echo $oas_applications->program->title;
                           if($oas_applications->program->pkProgId == 65){
                                echo '<br />Specialization: '.$appli_semes->phd_usuludin_specialization;
                            }
                           if($oas_applications->program->pkProgId == 66){
                                echo '<br />Specialization: '.$appli_semes->ms_usuludin_specialization;
                            }
                       }
                        // if($oas_applications->program->title != 'BS Engineering (EE & ME)' && $oas_applications->applicant->gender->gender == 'Male')
                        //     echo $oas_applications->program->title; 
                        // elseif ($oas_applications->program->title == 'BS Engineering (EE & ME)' && $oas_applications->applicant->gender->gender == 'Male'){
                        //    echo 'BS Electrical, BS Mechanical, BS Civil';
                        // } elseif($oas_applications->program->title != 'BS Engineering (EE & ME)' && $oas_applications->applicant->gender->gender == 'Female')
                        //    echo $oas_applications->program->title;
                        // elseif ($oas_applications->program->title == 'BS Engineering (EE & ME)' && $oas_applications->applicant->gender->gender == 'Female'){
                        //     echo 'BS Electrical';
                        // }// if($pref1 != '' && $pref2 != '' && $pref3 != '') echo ' ('.$pref1.') '.'('.$pref2.') '.'('.$pref3.') '?></b></td>
                        <td>Faculty:</td>
                        <td><b>	{{$oas_applications->program->faculty->title}}</b></td>
                    </tr>


                </table>
            </div>


            <p>Education Detail</p>
            <div class="table-main education-detail">
                <table>
                    <tr>
                        <td><b> Degree</b></td>
                        <td><b>Board / Univ</b></td>
                        <td><b>Degree Title</b></td>
                        <td><b>Subjects</b></td>
                        <td style="text-align:center"><b>Total Marks</b></td>
                        <td width="5%" style="text-align:center"><b>Obtained Marks</b></td>
                        <td><b>Percentage</b></td>
                        
                    </tr>
                    <tr>


                        <td> {{$appli_semes->SSC_Title}} </td>
                        <td>{{$appli_semes->SSC_From}}</td>
                        <td>{{$appli_semes->SSC_Subject}}</td>
                        <td></td>
                        <td style="text-align:center">{{$appli_semes->SSC_Total}}</td>
                        <td style="text-align:center">{{$appli_semes->SSC_Composite}}</td>
                        <td class="{{((bcdiv(($appli_semes->SSC_Composite/$appli_semes->SSC_Total*100), 1, 2)) < 50)? 'less-fifty': ''}}">{{bcdiv(($appli_semes->SSC_Composite/$appli_semes->SSC_Total*100), 1, 2)}} %</td>
                    </tr>
                    <tr>

                        <td>{{$appli_semes->HSC_Title}} </td>
                        <td>{{$appli_semes->HSC_From}}</td>
                        <td>{{$appli_semes->HSC_Subject}}</td>
                        <td></td>
                        <td style="text-align:center">
                            <?php $totalmark = (integer)($appli_semes->HSSC_Total1) + (integer)($appli_semes->HSSC_Total2); ?>
                            @if($totalmark)
                            {{$totalmark}}
                            @else
                            {{$appli_semes->HSSC_Total}}
                            @endif

                        </td>
                        <td style="text-align:center"><?php
                            $mark = (integer)($appli_semes->HSSC1) + (integer)($appli_semes->HSSC2);
                            ?>
                            @if($mark)
                            {{$mark}}
                            @else
                            {{$appli_semes->HSSC_Composite}}
                            @endif

                        </td>
                        
                                @if(!$totalmark)
                                @php
                                    $totalmark = $appli_semes->HSSC_Total;    
                                @endphp
                                @endif

                                @if(!$mark)
                                @php
                                    $mark = $appli_semes->HSSC_Composite;    
                                @endphp
                                @endif
                                @if($mark == '' && $totalmark == '')
                                    <td></td>
                                @else
                                    <td class="{{((bcdiv(($mark/$totalmark*100), 1, 2)) < 50)? 'less-fifty': ''}}">
                                        {{bcdiv(($mark/$totalmark*100), 1, 2)}} %
                                    </td>           
                                @endif
                    </tr>

                    <?php
                    if (/* $oas_applications->applicant->fkLevelId == 1  && */ $appli_semes->BA_Bsc != '' && $appli_semes->BA_Bsc_Total != '') {
                        ?>
                        <tr>

                            <td> {{$appli_semes->BA_Bsc_Title}} </td>
                            <td>{{$appli_semes->BA_Bsc_From}}</td>
                            <td>{{$appli_semes->BA_Bsc_Subject}}</td>
                            <td>{{$appli_semes->BA_Bsc_Specialization}}</td>
                            <td style="text-align:center">{{$appli_semes->BA_Bsc_Total}}</td>
                            <td style="text-align:center">{{$appli_semes->BA_Bsc}}</td>
                        <td class="{{((bcdiv(($appli_semes->BA_Bsc/$appli_semes->BA_Bsc_Total*100), 1, 2)) < 50)? 'less-fifty': ''}}">{{bcdiv(($appli_semes->BA_Bsc/$appli_semes->BA_Bsc_Total*100), 1, 2)}} %</td>
                        </tr>
                        <?php

                    }
                    if (/* $oas_applications->applicant->fkLevelId == 2 && */ $appli_semes->MSc != '' && $appli_semes->MSc_Total != '') {
                        ?>
                        <tr>

                            <td> {{$appli_semes->MSc_Title}} </td>
                            <td>{{$appli_semes->MSc_From}}</td>
                            <td>{{$appli_semes->MSc_Subject}}</td>
                            <td>{{$appli_semes->MSc_Specialization}}</td>
                            <td style="text-align:center">{{$appli_semes->MSc_Total}}</td>
                            <td style="text-align:center">{{$appli_semes->MSc}}</td>
                            <td class="{{((bcdiv(($appli_semes->MSc/$appli_semes->MSc_Total*100), 1, 2)) < 50)? 'less-fifty': ''}}">
                                @if(round($appli_semes->MSc) > 10)
                                {{bcdiv(($appli_semes->MSc/$appli_semes->MSc_Total*100), 1, 2). ' %'}}
                                @endif
                            </td>
                        </tr>
                        <?php

                    }

                    if (/* $oas_applications->applicant->fkLevelId == 2 && */$appli_semes->BS != '' && $appli_semes->BS_Total != '') {
                        ?>
                        <tr>

                            <td> {{$appli_semes->BS_Title}} 
                                    @if($appli_semes->BS_Exam_System != 'NULL') 
                                   ({{$appli_semes->BS_Exam_System}})
                                   @endif
                            </td>
                            <td>{{$appli_semes->BS_From}}</td>
                            <td>{{$appli_semes->BS_Subject}}</td>
                            <td>{{$appli_semes->BS_Specialization}}</td>
                            <td style="text-align:center">{{$appli_semes->BS_Total}}</td>
                            <td style="text-align:center">{{$appli_semes->BS}}</td>
                            <td class="{{((bcdiv(($appli_semes->BS/$appli_semes->BS_Total*100), 1, 2)) < 50)? 'less-fifty': ''}}">
                                @if(round($appli_semes->BS) > 10)
                                {{bcdiv(($appli_semes->BS/$appli_semes->BS_Total*100), 1, 2). ' %'}}
                                @endif
                            </td>
                        </tr>
                        <?php

                    }
                    if ($oas_applications->applicant->fkLevelId == 5 && $appli_semes->MS != '' && $appli_semes->MS_Total != '') {
                        ?>
                        <tr>
                            
                            <td> {{$appli_semes->MS_Title}} 
                                    @if($appli_semes->MS_Exam_System != 'NULL') 
                                    ({{$appli_semes->MS_Exam_System}})
                                    @endif
                            </td>
                            <td>{{$appli_semes->MS_From}}</td>
                            <td>{{$appli_semes->MS_Subject}}</td>
                            <td>{{$appli_semes->MS_Specialization}}</td>
                            <td style="text-align:center">{{$appli_semes->MS_Total}}</td>
                            <td style="text-align:center">{{$appli_semes->MS}}</td>
                            <td class="{{((bcdiv(($appli_semes->MS/$appli_semes->MS_Total*100), 1, 2)) < 50)? 'less-fifty': ''}}">
                                @if(round($appli_semes->MS) > 10)
                                {{bcdiv(($appli_semes->MS/$appli_semes->MS_Total*100), 1, 2). ' %'}}
                                @endif
                            </td>
                        </tr>
                    <?php 
                } ?>





@if($appli_semes->resultAwaiting != 0)
      <tr>
      <td>
      @if($appli_semes->resultAwaiting != 0)
      {{'HSSC Result awaiting'}}
      </td>
      <td>
      {{'Yes'}} 
    @endif
    </td>
      </tr>
  @endif
  @if($appli_semes->resultwaitbsc != 0)
      <tr>
      <td>
      @if($appli_semes->resultwaitbsc != 0)
      {{'BSC Result awaiting'}}
      </td>
      <td>
      {{'Yes'}} 
    @endif
    </td>
      </tr>
  @endif
    @if($appli_semes->mba_experience != NULL)
      <tr>
      <td>
      {{'MBA-EXECUTIVE (Weekend Program) Experience'}}
      </td>
      <td>
      {{$appli_semes->mba_experience. ' Years'}} 
    </td>
      </tr>
  @endif

@if($appli_semes->llb_total_marks != NULL || $appli_semes->llb_obtained_marks != NULL || $appli_semes->law_completion_date != NULL || $appli_semes->lat_upcoming != NULL)
<tr>
    <th>Test</th>
    <th>Total Marks</th>
    <th>Obtained Marks</th>
    <th>Completion Date</th>
    <th>LAT Upcoming Test Applied ?</th>
</tr>
<tr>
    <td>
        {{'Law Admission Test Score'}}
    </td>
    <td>
        {{$appli_semes->llb_total_marks}} 
    </td>
    <td>
        {{$appli_semes->llb_obtained_marks}} 
    </td>

    <td>
        {{$appli_semes->law_completion_date}} 
    </td>
    <td>
        <?php
        if($appli_semes->lat_upcoming == '1'){
            echo 'Yes';
        }
        if($appli_semes->lat_upcoming == '0'){
            echo 'No';
        }
        ?> 
    </td>
        
</tr>
@endif
@if($appli_semes->entry_test_center != NULL || $appli_semes->entry_test_total_marks != NULL || $appli_semes->entry_test_obtained_marks != NULL || $appli_semes->iiu_test_appear_willingness != NULL)
<tr>
    <th>Test</th>
    <th>Total Marks</th>
    <th>Obtained Marks</th>
    <th>Are you willing to appear for IIU entrance test ?</th>
    <th>Attachment</th>
</tr>
<tr>
    <td>
        {{$appli_semes->entry_test_center}} 
    </td>
    <td>
        {{$appli_semes->entry_test_total_marks}} 
    </td>

    <td>
        {{$appli_semes->entry_test_obtained_marks}} 
    </td>
    <td>
        <?php
        if($appli_semes->iiu_test_appear_willingness == '1'){
            echo 'Yes';
        }
        if($appli_semes->iiu_test_appear_willingness == '0'){
            echo 'No';
        }
        ?> 
    </td>
    <td>@if($appli_semes->entry_test_sheet)<a target="_blank" href="{{asset('storage/'.$appli_semes->entry_test_sheet)}}">Attachment</a>@endif</td>
        
</tr>
@endif
@if($appli_semes->gat_gre_test_type != NULL || $appli_semes->gat_gre_total_marks != NULL || $appli_semes->gat_gre_obtained_marks != NULL || $appli_semes->gat_gre_completion_date != NULL)
<tr>
    <th>Test</th>
    <th>Total Marks</th>
    <th>Obtained Marks</th>
    <th>Completion Date</th>
</tr>

<tr>
    <td>
        {{$appli_semes->gat_gre_test_type}} 
    </td>
    <td>
        {{$appli_semes->gat_gre_total_marks}} 
    </td>
    <td>
        {{$appli_semes->gat_gre_obtained_marks}} 
    </td>

    <td>
        {{$appli_semes->gat_gre_completion_date}} 
    </td>
</tr>
@endif

</table>
@if($appli_semes->matric_last_result || $appli_semes->hssc_last_result || $appli_semes->bs_last_result || $appli_semes->ms_last_result || $appli_semes->phd_research_proposal)
<p class="mar-tp-10">Uploaded Degree(s) Link(s)</p>
<table class="table-main mar-tp-10">
<tr>
    @if($appli_semes->matric_last_result)<th><a target="_blank" href="{{asset('storage/'.$appli_semes->matric_last_result)}}">SSC Degree</a></th>@endif
    @if($appli_semes->hssc_last_result)<th><a target="_blank" href="{{asset('storage/'.$appli_semes->hssc_last_result)}}">HSSC Degree</a></th>@endif
    @if($appli_semes->bs_last_result)<th><a target="_blank" href="{{asset('storage/'.$appli_semes->bs_last_result)}}">BS Degree</a></th>@endif
    @if($appli_semes->ms_last_result)<th><a target="_blank" href="{{asset('storage/'.$appli_semes->ms_last_result)}}">MS Degree</a></th>@endif
    @if($appli_semes->phd_research_proposal)<th><a target="_blank" href="{{asset('storage/'.$appli_semes->phd_research_proposal)}}">Phd Research Proposal</a></th>@endif
</tr>
</table>
@endif
            </div>
            <p>Address Details:</p>
            <div class="table-main">
                <table>
                    <tr>
                        <td><b>Address Type</b></td>
                        <td><b>Address</b></td>
                        <td><b>City</b></td>
                        <td><b>Phone</b></td>
                    </tr>
                    <tr>
                        <td><b>Mailing Address</b></td>
                        <td> {{$oas_applications->applicant->applicantAddress->addressPo}}</td>
                        <td>{{$oas_applications->applicant->applicantAddress->cityPo}}</td>
                        <td>{{$oas_applications->applicant->applicantAddress->phonePo}}</td>
                    </tr>
                    <tr>
                        <td><b>Permanent Address</b></td>
                        <td> {{$oas_applications->applicant->applicantAddress->addressPmt}}</td>
                        <td>{{$oas_applications->applicant->applicantAddress->cityPmt}}</td>
                        <td>{{$oas_applications->applicant->applicantAddress->phonePmt}}</td>
                    </tr>

                    {{-- <tr>
                        <td><b>Father Address</b></td>
                        <td> {{$oas_applications->applicant->applicantAddress->addressFather}}</td>
                        <td>{{$oas_applications->applicant->applicantAddress->cityFather}}</td>
                        <td>{{$oas_applications->applicant->applicantAddress->phoneFather}}</td>
                    </tr> --}}
                </table>
            </div>
            <p>Family Details:</p>
            <div class="table-main">
                <table>
                    <tr>
                        <td>Father Status:</td>
                        <td><?php
                            if ($oas_applications->applicant->applicantDetail->fatherStatus == 1)
                                echo 'Alive';
                            elseif ($oas_applications->applicant->applicantDetail->fatherStatus == 2)
                                echo 'Deceased';
                            ?></td>
                        <td>Guardian Name:</td>
                        <td colspan="3">{{$oas_applications->applicant->applicantDetail->Guradian_Spouse}}</td>
                    </tr>
                    <tr>
                        <td>Father / Guardian <br />
                            Occupation:</td>
                        <td>{{$oas_applications->applicant->applicantDetail->fatherOccupation}}</td>
                        <td>Monthly Income:</td>
                        <td colspan="3">{{$oas_applications->applicant->applicantDetail->monthlyIncome}}</td>
                    </tr>
                    <tr>
                        <td>No. of dependants</td>
                        <td>{{$oas_applications->applicant->applicantDetail->dependants}}</td>
                        <td>Father / Guardian <br />
                            Phone:</td>
                        <td>{{$oas_applications->applicant->applicantDetail->fatherPhone}}</td>
                        <td>Father / Guardian<br />
                            Cell Phone</td>
                        <td>{{$oas_applications->applicant->applicantDetail->fatherMobile}}</td>
                    </tr>
                    <tr>
                        <td colspan="6"><b>Bank Account detail for credit of refundable security / fee if any:</b></td>
                    </tr>
                    <tr>
                        <td>Title of Account:</td>
                        <td>{{$oas_applications->applicant->applicantDetail->titleBankAccount}}</td>
                        <td>Account No:</td>
                        <td colspan="3">{{$oas_applications->applicant->applicantDetail->accountNo}}</td>
                    </tr>
                    <tr>
                        <td>Bank Name:</td>
                        <td>{{$oas_applications->applicant->applicantDetail->bankName}}</td>
                        <td>Bank Branch:</td>
                        <td colspan="3">{{$oas_applications->applicant->applicantDetail->bankBranch}}</td>
                    </tr>
                </table>
            </div>
<?php /* ?>
            <p>Language Details:</p>
            <div class="table-main">
                <table>
                    <tr>
                        <td><b>Language</b></td>
                        <td><b>Written</b></td>
                        <td><b>Spoken</b></td>
                    </tr>
                    <tr>

                        <td>Arabic</td>
                        <td><?php
                            $written = $oas_applications->applicant->applicantDetail->arabicWritten;
                            if (empty($written)) {
                                echo 'None';
                            } else if (!empty($written) && $written == 1) {
                                echo 'Fair';
                            } else if (!empty($written) && $written == 2) {
                                echo 'Good';
                            } else if (!empty($written) && $written == 3) {
                                echo 'Excellent';
                            }
                            ?></td>

                        <td><?php
                            $speak = $oas_applications->applicant->applicantDetail->arabicSpoken;
                            if (empty($speak)) {
                                echo 'None';
                            } elseif (!empty($speak) && $speak == 1) {
                                echo 'Fair';
                            } elseif (!empty($speak) && $speak == 2) {
                                echo 'Good';
                            } elseif (!empty($speak) && $speak == 3) {
                                echo 'Excellent';
                            }
                            ?></td>
                    </tr>
                    <tr>
                        <td>English</td>
                        <td><?php
                            $ewritten = $oas_applications->applicant->applicantDetail->englishWritten;
                            if (empty($ewritten)) {
                                echo 'None';
                            } elseif (!empty($ewritten) && $ewritten == 1) {
                                echo 'Fair';
                            } elseif (!empty($ewritten) && $ewritten == 2) {
                                echo 'Good';
                            } elseif (!empty($ewritten) && $ewritten == 3) {
                                echo 'Excellent';
                            }
                            ?></td>
                        <td><?php
                            $speak = $oas_applications->applicant->applicantDetail->englishSpoken;
                            if (empty($speak)) {
                                echo 'None';
                            } elseif (!empty($speak) && $speak == 1) {
                                echo 'Fair';
                            } elseif (!empty($speak) && $speak == 2) {
                                echo 'Good';
                            } elseif (!empty($speak) && $speak == 3) {
                                echo 'Excellent';
                            }
                            ?></td>
                    </tr>
                    <?php /* ?>
                    <tr>
                        <?php
                        if (!empty($oas_applications->applicant->applicantDetail->language3)) {
                            ?>
                            <td><?php echo $oas_applications->applicant->applicantDetail->language3 ?></td>
                            <td><?php
                                $ewritten = $oas_applications->applicant->applicantDetail->language3Written;
                                if (empty($ewritten)) {
                                    echo 'None';
                                } elseif (!empty($ewritten) && $ewritten == 1) {
                                    echo 'Fair';
                                } elseif (!empty($ewritten) && $ewritten == 2) {
                                    echo 'Good';
                                } elseif (!empty($ewritten) && $ewritten == 3) {
                                    echo 'Excellent';
                                }
                                ?></td>
                            <td><?php
                                $speak = $oas_applications->applicant->applicantDetail->language3Spoken;
                                if (empty($speak)) {
                                    echo 'None';
                                } elseif (!empty($speak) && $speak == 1) {
                                    echo 'Fair';
                                } elseif (!empty($speak) && $speak == 2) {
                                    echo 'Good';
                                } elseif (!empty($speak) && $speak == 3) {
                                    echo 'Excellent';
                                }
                                ?></td>
                        </tr>
                        <?php

                    }
                    ?>
                    <tr>
                        <?php
                        if (!empty($oas_applications->applicant->applicantDetail->language4)) {
                            ?>
                            <td><?php echo $oas_applications->applicant->applicantDetail->language4 ?></td>
                            <td><?php
                                $ewritten = $oas_applications->applicant->applicantDetail->language4Written;
                                if (empty($ewritten)) {
                                    echo 'None';
                                } elseif (!empty($ewritten) && $ewritten == 1) {
                                    echo 'Fair';
                                } elseif (!empty($ewritten) && $ewritten == 2) {
                                    echo 'Good';
                                } elseif (!empty($ewritten) && $ewritten == 3) {
                                    echo 'Excellent';
                                }
                                ?></td>
                            <td><?php
                                $speak = $oas_applications->applicant->applicantDetail->language4Spoken;
                                if (empty($speak)) {
                                    echo 'None';
                                } elseif (!empty($speak) && $speak == 1) {
                                    echo 'Fair';
                                } elseif (!empty($speak) && $speak == 2) {
                                    echo 'Good';
                                } elseif (!empty($speak) && $speak == 3) {
                                    echo 'Excellent';
                                }
                                ?></td>
                        </tr>
                        <?php

                    }
                    ?> 
                    <?php */ /*?>   
                    </tr>
                </table>
            </div>
            <?php
            */
            if (!empty($appli_semes->APT_Test_1)) {
                ?>
                <p>Aptitude Tests:</p>
                <div class="table-main">
                    <table>
                        <tr>
                            <td><b>Test</b></td>
                            <td><b> Yeark Taken</b></td>
                            <td><b> Total Marks</b></td>
                            <td><b>Marks Obtained</b></td>
                        </tr>
                        <tr>
                            <td><?php
                                $test = $appli_semes->APT_Test_1;
                                if ($test == 1)
                                    echo 'NAT';
                                elseif ($test == 2)
                                    echo 'GAT';
                                if ($test == 3)
                                    echo 'GAT-Subject';
                                if ($test == 4)
                                    echo 'SAT';
                                if ($test == 5)
                                    echo 'GRE';
                                if ($test == 6)
                                    echo 'Other';
                                ?> </td>
                            <td> <?php
                                $year = $appli_semes->APT_Test_1_Year;
                                if ($year == 1)
                                    echo '2015';
                                elseif ($year == 2)
                                    echo '2016';
                                if ($year == 3)
                                    echo '2017';
                                if ($year == 4)
                                    echo '2018';
                                ?> </td>
                            <td><?php echo $appli_semes->APT_Test_1_Total_Marks ?></td>
                            <td><?php echo $appli_semes->APT_Test_1_Marks ?></td>
                        </tr>
                        <?php
                        if (!empty($appli_semes->APT_Test_2)) {
                            ?>
                            <tr>
                                <td><?php
                                    $test = $appli_semes->APT_Test_2;
                                    if ($test == 1)
                                        echo 'NAT';
                                    elseif ($test == 2)
                                        echo 'GAT';
                                    if ($test == 3)
                                        echo 'GAT-Subject';
                                    if ($test == 4)
                                        echo 'SAT';
                                    if ($test == 5)
                                        echo 'GRE';
                                    if ($test == 6)
                                        echo 'Other';
                                    ?> </td>
                                <td> <?php
                                    $year = $appli_semes->APT_Test_2_Year;
                                    if ($year == 1)
                                        echo '2015';
                                    elseif ($year == 2)
                                        echo '2016';
                                    if ($year == 3)
                                        echo '2017';
                                    if ($year == 4)
                                        echo '2018';
                                    ?> </td>
                                <td><?php echo $appli_semes->APT_Test_2_Total_Marks ?></td>
                                <td><?php echo $appli_semes->APT_Test_2_Marks ?></td>
                            </tr>
                            <?php

                        }
                        ?>
                        <?php
                        if (!empty($appli_semes->APT_Test_3)) {
                            ?>
                            <tr>
                                <td><?php
                                    $test = $appli_semes->APT_Test_3;
                                    if ($test == 1)
                                        echo 'NAT';
                                    elseif ($test == 2)
                                        echo 'GAT';
                                    if ($test == 3)
                                        echo 'GAT-Subject';
                                    if ($test == 4)
                                        echo 'SAT';
                                    if ($test == 5)
                                        echo 'GRE';
                                    if ($test == 6)
                                        echo 'Other';
                                    ?> </td>
                                <td> <?php
                                    $year = $appli_semes->APT_Test_3_Year;
                                    if ($year == 1)
                                        echo '2015';
                                    elseif ($year == 2)
                                        echo '2016';
                                    if ($year == 3)
                                        echo '2017';
                                    if ($year == 4)
                                        echo '2018';
                                    ?> </td>
                                <td><?php echo $appli_semes->APT_Test_3_Total_Marks ?></td>
                                <td><?php echo $appli_semes->APT_Test_3_Marks ?></td>
                            </tr>
                            <?php

                        }
                        ?>
                        <?php
                        if (!empty($appli_semes->APT_Test_4)) {
                            ?>
                            <tr>
                                <td><?php
                                    $test = $appli_semes->APT_Test_4;
                                    if ($test == 1)
                                        echo 'NAT';
                                    elseif ($test == 2)
                                        echo 'GAT';
                                    if ($test == 3)
                                        echo 'GAT-Subject';
                                    if ($test == 4)
                                        echo 'SAT';
                                    if ($test == 5)
                                        echo 'GRE';
                                    if ($test == 6)
                                        echo 'Other';
                                    ?> </td>
                                <td> <?php
                                    $year = $appli_semes->APT_Test_4_Year;
                                    if ($year == 1)
                                        echo '2015';
                                    elseif ($year == 2)
                                        echo '2016';
                                    if ($year == 3)
                                        echo '2017';
                                    if ($year == 4)
                                        echo '2018';
                                    ?> </td>
                                <td><?php echo $appli_semes->APT_Test_4_Total_Marks ?></td>
                                <td><?php echo $appli_semes->APT_Test_4_Marks ?></td>
                            </tr>
                            <?php

                        }
                        ?>
                    </table>
                </div>
                <?php

            }
            ?>
            <p>Have you been student of IIU before:</p>
            <div class="table-main">
                <table>
                    <tr>
                        <td colspan="3">Have you been student of IIU before?</td>
                        <td colspan="4"><b><?php
                                            $xtud = $oas_applications->applicant->applicantDetail->xStudent;
                                            if (!empty($xtud))
                                                echo 'Yes';
                                            elseif (empty($xtud))
                                                echo 'No'
                                            ?></b></td>
                    </tr>
                    <tr>
                      <!--<td>Degree Name</td>
                      <td> <b>CS</b> </td>-->
                        <td colspan="3">Registration No</td>
                        <td colspan="4"><b><?php
                                $xtud = $oas_applications->applicant->applicantDetail->xStudent;
                                if (!empty($xtud))
                                    echo $xtud;
                                elseif (empty($xtud))
                                    echo 'No'
                                ?></b></td>
                        <!--<td>Degree Completed</td>
                        <td><b>Yes</b></td>-->
                    </tr>

                </table>
            </div>

            <p>Hobbies:</p>
            <div class="table-main">
                <table>
                    <?php
                    if (!empty($oas_applications->applicant->applicantDetail->hobbie1)) {
                        ?>
                        <tr>
                            <td><b>1</b></td>
                            <td><b>2</b></td>
                            <td><b>3</b></td>
                        </tr>
                        <tr>
                            <td><?php echo $oas_applications->applicant->applicantDetail->hobbie1 ?></td>
                            <td><?php
                                $hobbie2 = $oas_applications->applicant->applicantDetail->hobbie2;
                                if (empty($hobbie2))
                                    echo 'None';
                                elseif (!empty($hobbie2))
                                    echo $hobbie2;
                                ?></td>
                            <td><?php
                                $hobbie3 = $oas_applications->applicant->applicantDetail->hobbie3;
                                if (empty($hobbie3))
                                    echo 'None';
                                elseif (!empty($hobbie3))
                                    echo $hobbie3;
                                ?></td>
                        </tr>
                        <?php

                    } else {
                        ?>
                        <tr>
                            <td>No Hobies entered!</td>
                        </tr>
                        <?php

                    }
                    ?>
                </table>
            </div>

            <p>Extra Curricular Activities:</p>
            <div class="table-main">
                <table>
                    <?php
                    if (!empty($oas_applications->applicant->applicantDetail->activity1)) {
                        ?>
                        <tr>
                            <td><b>Activity</b></td>
                            <td><b>Prize</b></td>
                            <td><b>Awarded</b></td>
                        </tr>
                        <tr>
                            <td><?php echo $oas_applications->applicant->applicantDetail->activity1 ?></td>
                            <td><?php
                                $prize1 = $oas_applications->applicant->applicantDetail->prize1;
                                if (empty($prize1))
                                    echo 'None';
                                elseif (!empty($prize1))
                                    echo $prize1;
                                ?></td>
                            <td><?php
                                $awardBy1 = $oas_applications->applicant->applicantDetail->awardBy1;
                                if (empty($awardBy1))
                                    echo 'None';
                                elseif (!empty($awardBy1))
                                    echo $awardBy1;
                                ?></td>
                        </tr>
                        <?php
                        if (!empty($oas_applications->applicant->applicantDetail->activity2)) {
                            ?>
                            <tr>
                                <td><?php echo $oas_applications->applicant->applicantDetail->activity2 ?></td>
                                <td><?php
                                    $prize2 = $oas_applications->applicant->applicantDetail->prize1;
                                    if (empty($prize2))
                                        echo 'None';
                                    elseif (!empty($prize2))
                                        echo $prize2;
                                    ?></td>
                                <td><?php
                                    $awardBy2 = $oas_applications->applicant->applicantDetail->awardBy1;
                                    if (empty($awardBy2))
                                        echo 'None';
                                    elseif (!empty($awardBy2))
                                        echo $awardBy2;
                                    ?></td>
                            </tr>
                            <?php

                        }
                        ?>
                        <?php
                        if (!empty($oas_applications->applicant->applicantDetail->activity3)) {
                            ?>
                            <tr>
                                <td><?php echo $oas_applications->applicant->applicantDetail->activity3 ?></td>
                                <td><?php
                                    $prize3 = $oas_applications->applicant->applicantDetail->prize3;
                                    if (empty($prize3))
                                        echo 'None';
                                    elseif (!empty($prize3))
                                        echo $prize3;
                                    ?></td>
                                <td><?php
                                    $awardBy3 = $oas_applications->applicant->applicantDetail->awardBy3;
                                    if (empty($awardBy3))
                                        echo 'None';
                                    elseif (!empty($awardBy3))
                                        echo $awardBy3;
                                    ?></td>
                            </tr>
                            <?php

                        }
                        ?>
                        <?php

                    } else {
                        ?>
                        <tr>
                            <td>No Extra Curricular Activities entered!</td>
                        </tr>
                        <?php

                    }
                    ?>
                </table>

            </div>

            <p> Other Info:</p>
            <div class="table-main">
                <table>
                    <tr>
                        <td>Blood Group: {{$oas_applications->applicant->applicantDetail->blood}}</td>
                        <td>Disability: {{$oas_applications->applicant->applicantDetail->disable}}</td>
                        <td>Marital Status: {{$oas_applications->applicant->applicantDetail->married}}</td>
                        <td>Religion: {{$oas_applications->applicant->applicantDetail->religion}}</td>
                    </tr>
                </table>
            </div>
            <?php 
            if($oas_applications->program->level->pkLevelId == 1 || $oas_applications->program->level->pkLevelId == 2 || $oas_applications->program->level->pkLevelId == 3){
            ?>
            <div class="decalartion">
                <h4>Undertaking: (For result awaiting candidate)</h4>
                <?php 
                if($oas_applications->applicant->fkNationality == 1){
                    $statement_1 = ' within one (01) ';
                    $statement_2 = '';
                    $statement_3 = '';
                }
                if($oas_applications->applicant->fkNationality == 2 || $oas_applications->applicant->fkNationality == 3){
                    $statement_1 = ' and its equivalence to be obtained by IBCC/HEC within 06 months ';
                    $statement_2 = ' or fail to obtain equivalence of my certificates/degrees from IBCC/HEC Islamabad ';
                    $statement_3 = ' without any further notice ';
                }
                if($oas_applications->fkProgramId == 189){
                    $percentage_show = '60%';
                }else{
                    $percentage_show = '50%';
                }
                ?>
                <p>I, <b>{{$oas_applications->applicant->name}} </b> S/o | D/o <b>{{$oas_applications->applicant->applicantDetail->fatherName}}</b> hereby solemnly undertake / declare that I will submit the requisite result (FA/FSc/BA/BSc) <?= $statement_1 ?> month time after my admission in IIUI.  In case of failure in any subject or if I fail to secure required percentage of marks (<?= $percentage_show ?>) or CGPA <?= $statement_2 ?> within stipulated time, IIUI has the right to cancel my admission <?= $statement_3 ?>, I shall not claim refund of any portion of fee whatsoever as per University policy. </p>
                <table>
                    <tr class="under-tr">
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>Parent/Guardian's Signature</td>
                        <td>Date</td>
                        <td>Applicant's Signature</td>
                        <td>Date</td>
                    </tr>
                </table>                        

            
            </div>
            {{-- Dev Mannan: Disability section starts --}}
            <?php if($oas_applications->applicant->applicantDetail->disable != 'None'){ ?>
            <h4 class="pd-tp-hs">Student with disability information</h4>
            <div class="table-main disability-section">
                <table>
                    <tbody>
                        <tr>
                            <td>Disability / Type of disability</td>
                            <td><b>{{$oas_applications->applicant->applicantDetail->disable}}</b></td>
                        </tr>
                        <tr>
                            <td>Name of disability certificate issuing authority</td>
                            <td><b>{{$oas_applications->applicant->applicantDetail->issuing_authority}}</b></td>
                        </tr>
                        <tr>
                            <td>Nature of disability</td>
                            <td><b>{{$oas_applications->applicant->applicantDetail->nature_of_disability}}</b></td>
                        </tr>
                        <tr>
                            <td>Cause of disability</td>
                            <td><b>{{$oas_applications->applicant->applicantDetail->cause_of_disability}}</b></td>
                        </tr>
                        <tr>
                            <td>Do you use wheelchair ?</b></td>
                            <td><b>
                                <?php 
                                $wheel = $oas_applications->applicant->applicantDetail->wheel_chair;
                                echo ($wheel == '1') ? 'Yes' : 'No'; 
                                ?>

                            </b></td>
                        </tr>
                        <tr>
                            <td>Accommodation/Hostel at university required ?</b></td>
                            <td><b>
                                <?php 
                                $accomodation = $oas_applications->applicant->applicantDetail->accomodation_required;
                                echo ($accomodation == '1') ? 'Yes' : 'No'; 
                                ?>

                            </b></td>
                        </tr>
                        <tr>
                            <td>Accessibility requirement of undertaking the study and Exams at this university ?</td>
                            <td><b>{{$oas_applications->applicant->applicantDetail->disabled_accessibility}}</b></td>
                        </tr>
                        <tr>
                            <td>Do you need special arrangements for entrance test or interview, if so, please specify ?</td>
                            <td><b>{{$oas_applications->applicant->applicantDetail->special_arrangements}}</b></td>
                        </tr>
                        <tr>
                            <td>Describe about the special arrangements, made by the institute during your HSSC study ?</td>
                            <td><b>{{$oas_applications->applicant->applicantDetail->hssc_arrangements}}</b></td>
                        </tr>
                    </tbody>

                </table>
            </div>

            <div class="decalartion">

                <h4 class="bld-undr">Physical Verification</h4>
                <p>
                    <b>REMARKS:</b>
                    It is hereby verified that under reference student had visited this office/university along with  original disability certificate and OR CNIC (issued to disable persons only) and he/she was  found;
                </p>
                <p>
                    Disable: Yes: ___________________________________________ No: __________________________________________
                </p>
                <p>
                    Name of Chief Medical Officer / Principle Medical Officer: _____________________________________________________
                </p>
                <p>
                    Signature: ____________________________________________ Stamp: ________________________________________
                </p>
            </div>

            <div class="decalartion">

                <h4 class="bld-undr ">For Director (Academics & Exams) Only</h4>
                <p class="mar-tp">
                    Remarks Approved/Not-Approved: ______________  Name/Signatutre with stamp:_________________________________
                </p>
            </div>

           <?php } ?>
            {{-- Dev Mannan: Disability section ends --}}
            <?php
            }
            ?>
            <div class="row decalartion">

                <div class="col-12">
                    <h4>Declaration:</h4>
                    <p>I hereby solemnly undertake / declare that I have personally filled in this Application Form and the information contained herein is complete and correct to the best of my knowledge and belief. I understand that withholding or giving false information will make me ineligible for admission and future enrolment and that if withholding or giving false information is discovered after successful admission, the University has the right to cancel my admission. I further understand that I may be required to appear for an interview or to undergo such test as required by the University Admission Committee as a condition for admission to the programme of study for which I have applied. I will abide by all the current rules & regulations, all other rules to be framed time to time by University administration and will observe Islamic code of conduct during my whole period of study at IIUI.</p>
                    <p>I authorise IIUI to credit my refundable security or any other which amount becomes refundable to me at any stage in the Bank account mentioned above</p>
                </div>
                <div class="col-12 text-right applicant-signature">
                    Applicant's Signature
                </div>
            </div>

            <?php 
             if($oas_applications->applicant->fkNationality == 2 || $oas_applications->applicant->fkNationality == 3){
                ?>
                <br /><br /><br /><br />
                <div class="row overseas-div">
                    <h4>For querries (Please contact) Admission Office:</h4>
                    <?php
                    if($oas_applications->applicant->gender->gender == 'Male'){
                        echo '<p>Email: overseas.admissions@iiu.edu.pk</p>';
                        echo '<p>Room No. 129, First Floor, Admin Block IIUI</p>';
                        echo '<p>Tel. 0092-51-9019565</p>';
                    }
                    if($oas_applications->applicant->gender->gender == 'Female'){
                        echo '<p>Email: overseas.female.admissions@iiu.edu.pk</p>';
                        echo '<p>Room No. 03, Ground Floor, Admin Block IIUI</p>';
                        echo '<p>Tel. 0092-51-9019288</p>';
                    }
                    ?>
                </div>
                <?php 
             }
            ?>
        </div>
        <script>
            function printpage() {
                //Get the print button and put it into a variable
                var printButton = document.getElementById("printpagebutton");
                //Set the print button visibility to 'hidden' 
                printButton.style.visibility = 'hidden';
                //Print the page content
                window.print()
                //Set the print button to 'visible' again 
                //[Delete this line if you want it to stay hidden after printing]
                printButton.style.visibility = 'visible';
            }
        </script>
    </body>
</html>
