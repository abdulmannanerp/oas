<!DOCTYPE html>

<html lang="en-US">	
    <head>
        <meta charset="utf-8">	
    </head>	
    <body>
        Dear Candidate, <br /><br />
        Thank you for selecting IIUI for completion/advancement of your educational endeavours. Kindly complete the joining formalities by filling-in the requisite information in the given form, if you have paid the prsecribed fee for the program. <br />
        Please note that in order to confirm your joining/admission, you will be required to submit all the credentials, 5 passport sized photograhps, domicile, CNIC/B-form and original educational documents (for verification only) within 15 days,  but not later than 30th September. <br />
        Wish you all the best. <br /><br />
        Regards, <br />
        Admission Office <br />
        International Islamic University, Islamabad (IIUI)<br />
        <a href="http://admission.iiu.edu.pk">http://admission.iiu.edu.pk/</a><br /> 
    </body>
</html>