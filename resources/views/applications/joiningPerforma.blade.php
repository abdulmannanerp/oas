<!DOCTYPE html>
<html lang="en">
<head>
  <title>joining-performa</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <link href="{{public_path('css/joining-performa.css')}}" rel="stylesheet" type="text/css" media="all">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container-fluid">
  <div class="row text-center">
    <div class="col-md-10 wd-80">
      <h6><b>INTERNATIONAL ISLAMIC UNIVERSITY ISLAMABAD</b></h6>
      <h6 class="mr-tp-mn"><b>PROVISIONAL ENROLMENT SLIP</b></h6> 
      <h6 class="mr-tp-mn">
        <b>
          @if($duesDetail->fkSemesterId == 14 && $duesDetail->applicant->program->pkProgId == 157)
						{{ 'Fall-2020' }}
					@else
						{{ $semester->title ?? ''}}
					@endif
          {{-- {{$semester->title}} --}}
        </b>
        </h6> 
    </div>
    <div class="col-md-2 wd-20 fl-rgt">
        {{-- <img id="pic1" src="{{asset('storage/images/joining.png')}}" id="pic1" alt="Profile Pic" class="img-thum"> --}}
        <div class="image-wdth-joining">
          Attach Picture 
        </div>
    </div>
  </div>
  <div class="row mr-tp-pl">
    <p>Dated: <span class="print-php">{{date("d-M-Y")}}</span></p>
  </div>
  <div class="row">
    <p>
    Certified that <span class="print-php">{{strtoupper($applicantDetail->applicant->name)}}</span> S/o | D/o <span class="print-php">{{strtoupper($applicantDetail->fatherName)}}</span> has been enrolled in the University for the <span class="print-php">{{$program->title}}</span> (Degree Program)
    </p>
  </div>
  <div class="row">
      <p>Detail of Amount paid by the student:</p>
   </div>
   <div class="row">
      <table class="table table-bordered tbl-wdt">
          <thead>
            <tr>
              <th scope="col">Particulars</th>
              <th scope="col" class="text-center">Amount Paid</th>
              <th scope="col" class="text-center">Bank Challan #</th>
              <th scope="col" class="text-center">Dated</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <th scope="row">University Fee</th>
              <td class="text-center"><b class="bld-cus">{{$duesDetail->total}}</b></td>
              <td class="text-center"><b class="bld-cus">{{$duesDetail->id}}</b></td>
              <td></td>
            </tr>
            <tr>
              <th scope="row">Security Deposit</th>
              <td class="text-center"><b class="bld-cus">{{$securityDetail->total}}</b></td>
              <td class="text-center"><b class="bld-cus">{{$securityDetail->id}}</b></td>
              <td></td>
            </tr>
            <tr>
              <th scope="row">Total:</th>
              <td class="text-center"><b class="bld-cus">{{$duesDetail->total+$securityDetail->total}}</b></td>
              <td colspan="2"></td>
            </tr>
          </tbody>
      </table>
   </div>
   <div class="row">
    <table class="table table-bordered tbl-wdt">
      <tbody>
        <tr>
          <th scope="row" class="verified">Verified:</th>
          <td>a) Result / eligibility and % of marks. <span class="box-cus"></span></td>
          <td>b) Result not submitted by the student.<span class="box-cus"></span></td>
        </tr>
      </tbody>
    </table>
   </div>
   <div class="row text-center">
    <div class="pt-5 wd-50 brdr-cus">
      <h6 class="cus-line"><b>Student's Signature</b></h6>
    </div>
    <div class="pt-5 wd-50 fl-rgt brdr-cus">
      <h6 class="cus-line"><b>Concerned Official (Admissions)</b></h6>
    </div>    
   </div>
<hr />
    <div class="row text-center">
      <div class="col-md-10">
        <h6><b>INTERNATIONAL ISLAMIC UNIVERSITY ISLAMABAD</b></h6>
        <h6 class="mr-tp-mn"><b>PROVISIONAL ENROLMENT SLIP</b></h6> 
        <h6 class="mr-tp-mn">
          <b>
            @if($duesDetail->fkSemesterId == 14 && $duesDetail->applicant->program->pkProgId == 157)
              {{ 'Fall-2020' }}
            @else
              {{ $semester->title ?? ''}}
            @endif
          </b>
        </h6> 
      </div>
      <div class="col-md-2">
          {{-- <img id="pic1" src="{{asset('storage/images/joining.png')}}" id="pic1" alt="Profile Pic" class="img-thum"> --}}
          <div class="image-wdth-joining">
            Attach Picture 
          </div>
      </div>
    </div>
    <div class="row mr-tp-pl">
      <p>Dated: <span class="print-php">{{date("d-M-Y")}}</span> </p>
    </div>
    <div class="row">
      <p>
          Certified that <span class="print-php">{{strtoupper($applicantDetail->applicant->name)}}</span> S/o | D/o <span class="print-php">{{strtoupper($applicantDetail->fatherName)}}</span> has been enrolled in the University for the <span class="print-php">{{$program->title}}</span> (Degree Program)
      </p>
    </div>
    <div class="row">
        <p>Detail of Amount paid by the student:</p>
     </div>
     <div class="row">
        <table class="table table-bordered tbl-wdt">
            <thead>
              <tr>
                <th scope="col">Particulars</th>
                <th scope="col" class="text-center">Amount Paid</th>
                <th scope="col" class="text-center">Bank Challan #</th>
                <th scope="col" class="text-center">Dated</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <th scope="row">University Fee</th>
                <td class="text-center"><b class="bld-cus">{{$duesDetail->total}}</b></td>
                <td class="text-center"><b class="bld-cus">{{$duesDetail->id}}</b></td>
                <td></td>
              </tr>
              <tr>
                <th scope="row">Security Deposit</th>
                <td class="text-center"><b class="bld-cus">{{$securityDetail->total}}</b></td>
                <td class="text-center"><b class="bld-cus">{{$securityDetail->id}}</b></td>
                <td></td>
              </tr>
              <tr>
                <th scope="row">Total:</th>
                <td class="text-center"><b class="bld-cus">{{$duesDetail->total+$securityDetail->total}}</b></td>
                <td colspan="2"></td>
              </tr>
            </tbody>
        </table>
     </div>
     <div class="row">
      <table class="table table-bordered tbl-wdt">
        <tbody>
          <tr>
            <th scope="row" class="verified">Verified:</th>
            <td>a) Result / eligibility and % of marks. <span class="box-cus"></span></td>
            <td>b) Result not submitted by the student.<span class="box-cus"></span></td>
          </tr>
        </tbody>
      </table>
     </div>
     <div class="row">
        <p><b>Note:</b></p>
     </div>
     <div class="row">
        <p>a) The student has been admitted provisionally and if at any stage it is found that he / she 	does not 	qualify on merit or concealed the facts admission shall be cancelled.</p>
     </div>
     <div class="row">
        <p>b) Students are advised to keep the original slip in their safe custody, as Duplicate Slip will not be provided in any case.</p>
     </div>
     <div class="row text-center">
        <div class="pt-5 wd-33 brdr-cus max-hgt">
          <b class="cus-line bld-cus">Student's Signature</b>
        </div>
        <div class="pt-5 wd-33 fl-rgt brdr-cus max-hgt">
          <b class="cus-line bld-cus">Assistant Director (Admissions)</b>
        </div>    
        <div class="pt-5 wd-33 fl-rgt brdr-cus max-hgt">
          <b class="cus-line bld-cus">Concerned Official (Admissions)</b>
        </div>    
      </div>
     <div class="row">
        <b class="bld-cus">The student is advice to submit a photocopy of Enrolment Slip to:</b>
     </div>
  <div class="row">
    <div class="wd-50">
        1.	Faculty/Department concerned.
    </div>
    <div class="wd-50 fl-rgt">
        2.	Provost Office (In case of boarders only)
    </div>    
   </div>


  </div>

<div class="break-page"></div>

<div class="container-fluid">
    <div class="row text-center">
      <div class="col-md-10">
        <h6><b>INTERNATIONAL ISLAMIC UNIVERSITY ISLAMABAD</b></h6>
        <h6 class="mr-tp-mn"><b>ADMISSION OFFICE</b></h6> 
        <h6 class="mr-tp-mn"><b>JOINING REPORT</b></h6> 
      </div>
      <div class="col-md-2">
        {{-- <img id="pic1" src="{{asset('storage/images/joining.png')}}" id="pic1" alt="Profile Pic" class="img-thum"> --}}
        <div class="image-wdth-joining">
          Attach Picture 
        </div>
      </div>
    </div>
    <ol class="decimal mr-tp-pl">
      <li>Name of Student:(In English) <span class="print-php">{{ucwords($applicantDetail->applicant->name)}}</span></li>
      <li class="wd-100">Name of Student:(In Urdu)</li> ______________________________________
      <li>Father's Name: <span class="print-php">{{ucwords($applicantDetail->fatherName)}}</span></li>
      <li>Date Of Birth: <span class="print-php">{{date('d-M-Y',strtotime($applicantDetail->DOB))}}</span></li>
      <li>CNIC/Passport #:  <span class="print-php">{{$applicantDetail->applicant->cnic}}</span></li>
      <li>Nationality: <span class="print-php">{{$applicantDetail->applicant->nationality->nationality}}</span></li>
      <li>Contact Number: <span class="print-php">{{$applicantDetail->mobile}}</span></li>
      <li>Email: <span class="print-php">{{$applicantDetail->applicant->email}}</span></li>
      <li class="wd-100">Present/Mailing Address: <span class="print-php">{{$applicantDetail->applicant->applicantAddress->addressPo}}</span> District: ____________ Province: ____________</li>
      <li class="wd-100">Permanent Address: <span class="print-php">{{$applicantDetail->applicant->applicantAddress->addressPmt}}</span> District: ____________ Province:____________</li>
      <li>Faculty/Institute: <span class="print-php">{{$faculty->title}}</span></li>
      <li>Degree Program: <span class="print-php">{{$program->title}}</span></li>
    </ol>

    <div class="row">
      <b class="bld-cus">
          Qualification:
      </b>
   </div>
   <div class="row">
          <table class="table table-bordered tbl-wdt cus-fnn">
              <tr>
                  <th class="text-center">Degree</th>
                  <th class="text-center">Board / Univ</th>
                  <th class="text-center">Degree Title</th>
                  <th class="text-center">Subjects</th>
                  <th class="text-center">Total Marks</th>
                  <th width="5%" class="text-center">Obtained Marks</th>
                  <th class="text-center">Percentage</th>
                  
              </tr>
              <tr> 
                  <td> {{$applicantDetail->applicant->previousQualification($semester->pkSemesterId)->first()->SSC_Title}} </td>
                  <td>{{$applicantDetail->applicant->previousQualification($semester->pkSemesterId)->first()->SSC_From}}</td>
                  <td>{{$applicantDetail->applicant->previousQualification($semester->pkSemesterId)->first()->SSC_Subject}}</td>
                  <td></td>
                  <td class="text-center">{{$applicantDetail->applicant->previousQualification($semester->pkSemesterId)->first()->SSC_Total}}</td>
                  <td class="text-center">{{$applicantDetail->applicant->previousQualification($semester->pkSemesterId)->first()->SSC_Composite}}</td>
                  <td>{{round($applicantDetail->applicant->previousQualification($semester->pkSemesterId)->first()->SSC_Composite/$applicantDetail->applicant->previousQualification($semester->pkSemesterId)->first()->SSC_Total*100)}} %</td>
              </tr>
              <tr>

                  <td>{{$applicantDetail->applicant->previousQualification($semester->pkSemesterId)->first()->HSC_Title}} </td>
                  <td>{{$applicantDetail->applicant->previousQualification($semester->pkSemesterId)->first()->HSC_From}}</td>
                  <td>{{$applicantDetail->applicant->previousQualification($semester->pkSemesterId)->first()->HSC_Subject}}</td>
                  <td></td>
                  <td class="text-center">
                      <?php $totalmark = (integer)($applicantDetail->applicant->previousQualification($semester->pkSemesterId)->first()->HSSC_Total1) + (integer)($applicantDetail->applicant->previousQualification($semester->pkSemesterId)->first()->HSSC_Total2); ?>
                      @if($totalmark)
                      {{$totalmark}}
                      @else
                      {{$applicantDetail->applicant->previousQualification($semester->pkSemesterId)->first()->HSSC_Total}}
                      @endif

                  </td>
                  <td class="text-center"><?php
                      $mark = (integer)($applicantDetail->applicant->previousQualification($semester->pkSemesterId)->first()->HSSC1) + (integer)($applicantDetail->applicant->previousQualification($semester->pkSemesterId)->first()->HSSC2);
                      ?>
                      @if($mark)
                      {{$mark}}
                      @else
                      {{$applicantDetail->applicant->previousQualification($semester->pkSemesterId)->first()->HSSC_Composite}}
                      @endif

                  </td>
                  <td>
                          @if(!$totalmark)
                          @php
                              $totalmark = $applicantDetail->applicant->previousQualification($semester->pkSemesterId)->first()->HSSC_Total;    
                          @endphp
                          @endif

                          @if(!$mark)
                          @php
                              $mark = $applicantDetail->applicant->previousQualification($semester->pkSemesterId)->first()->HSSC_Composite;    
                          @endphp
                          @endif
                          @if($totalmark && $mark)
                            {{round($mark/$totalmark*100)}} %
                          @endif
                  </td>
              </tr>

              <?php
              if (/* $applicantDetail->applicant->fkLevelId == 1  && */ $applicantDetail->applicant->previousQualification($semester->pkSemesterId)->first()->BA_Bsc != '' && $applicantDetail->applicant->previousQualification($semester->pkSemesterId)->first()->BA_Bsc_Total != '') {
                  ?>
                  <tr>

                      <td> {{$applicantDetail->applicant->previousQualification($semester->pkSemesterId)->first()->BA_Bsc_Title}} </td>
                      <td>{{$applicantDetail->applicant->previousQualification($semester->pkSemesterId)->first()->BA_Bsc_From}}</td>
                      <td>{{$applicantDetail->applicant->previousQualification($semester->pkSemesterId)->first()->BA_Bsc_Subject}}</td>
                      <td>{{$applicantDetail->applicant->previousQualification($semester->pkSemesterId)->first()->BA_Bsc_Specialization}}</td>
                      <td class="text-center">{{$applicantDetail->applicant->previousQualification($semester->pkSemesterId)->first()->BA_Bsc_Total}}</td>
                      <td class="text-center">{{$applicantDetail->applicant->previousQualification($semester->pkSemesterId)->first()->BA_Bsc}}</td>
                  <td>{{round($applicantDetail->applicant->previousQualification($semester->pkSemesterId)->first()->BA_Bsc/$applicantDetail->applicant->previousQualification($semester->pkSemesterId)->first()->BA_Bsc_Total*100)}} %</td>
                  </tr>
                  <?php

              }
              if (/* $applicantDetail->applicant->fkLevelId == 2 && */ $applicantDetail->applicant->previousQualification($semester->pkSemesterId)->first()->MSc != '' && $applicantDetail->applicant->previousQualification($semester->pkSemesterId)->first()->MSc_Total != '') {
                  ?>
                  <tr>

                      <td> {{$applicantDetail->applicant->previousQualification($semester->pkSemesterId)->first()->MSc_Title}} </td>
                      <td>{{$applicantDetail->applicant->previousQualification($semester->pkSemesterId)->first()->MSc_From}}</td>
                      <td>{{$applicantDetail->applicant->previousQualification($semester->pkSemesterId)->first()->MSc_Subject}}</td>
                      <td>{{$applicantDetail->applicant->previousQualification($semester->pkSemesterId)->first()->MSc_Specialization}}</td>
                      <td class="text-center">{{$applicantDetail->applicant->previousQualification($semester->pkSemesterId)->first()->MSc_Total}}</td>
                      <td class="text-center">{{$applicantDetail->applicant->previousQualification($semester->pkSemesterId)->first()->MSc}}</td>
                      <td>
                          @if(round($applicantDetail->applicant->previousQualification($semester->pkSemesterId)->first()->MSc) > 10)
                          {{round($applicantDetail->applicant->previousQualification($semester->pkSemesterId)->first()->MSc/$applicantDetail->applicant->previousQualification($semester->pkSemesterId)->first()->MSc_Total*100). ' %'}}
                          @endif
                      </td>
                  </tr>
                  <?php

              }

              if (/* $applicantDetail->applicant->fkLevelId == 2 && */$applicantDetail->applicant->previousQualification($semester->pkSemesterId)->first()->BS != '' && $applicantDetail->applicant->previousQualification($semester->pkSemesterId)->first()->BS_Total != '') {
                  ?>
                  <tr>

                      <td> {{$applicantDetail->applicant->previousQualification($semester->pkSemesterId)->first()->BS_Title}} 
                              @if($applicantDetail->applicant->previousQualification($semester->pkSemesterId)->first()->BS_Exam_System != 'NULL') 
                             ({{$applicantDetail->applicant->previousQualification($semester->pkSemesterId)->first()->BS_Exam_System}})
                             @endif
                      </td>
                      <td>{{$applicantDetail->applicant->previousQualification($semester->pkSemesterId)->first()->BS_From}}</td>
                      <td>{{$applicantDetail->applicant->previousQualification($semester->pkSemesterId)->first()->BS_Subject}}</td>
                      <td>{{$applicantDetail->applicant->previousQualification($semester->pkSemesterId)->first()->BS_Specialization}}</td>
                      <td class="text-center">{{$applicantDetail->applicant->previousQualification($semester->pkSemesterId)->first()->BS_Total}}</td>
                      <td class="text-center">{{$applicantDetail->applicant->previousQualification($semester->pkSemesterId)->first()->BS}}</td>
                      <td>
                          @if(round($applicantDetail->applicant->previousQualification($semester->pkSemesterId)->first()->BS) > 10)
                          {{round($applicantDetail->applicant->previousQualification($semester->pkSemesterId)->first()->BS/$applicantDetail->applicant->previousQualification($semester->pkSemesterId)->first()->BS_Total*100). ' %'}}
                          @endif
                      </td>
                  </tr>
                  <?php

              }
              if ($applicantDetail->applicant->fkLevelId == 5 && $applicantDetail->applicant->previousQualification($semester->pkSemesterId)->first()->MS != '' && $applicantDetail->applicant->previousQualification($semester->pkSemesterId)->first()->MS_Total != '') {
                  ?>
                  <tr>
                      
                      <td> {{$applicantDetail->applicant->previousQualification($semester->pkSemesterId)->first()->MS_Title}} 
                              @if($applicantDetail->applicant->previousQualification($semester->pkSemesterId)->first()->MS_Exam_System != 'NULL') 
                              ({{$applicantDetail->applicant->previousQualification($semester->pkSemesterId)->first()->MS_Exam_System}})
                              @endif
                      </td>
                      <td>{{$applicantDetail->applicant->previousQualification($semester->pkSemesterId)->first()->MS_From}}</td>
                      <td>{{$applicantDetail->applicant->previousQualification($semester->pkSemesterId)->first()->MS_Subject}}</td>
                      <td>{{$applicantDetail->applicant->previousQualification($semester->pkSemesterId)->first()->MS_Specialization}}</td>
                      <td class="text-center">{{$applicantDetail->applicant->previousQualification($semester->pkSemesterId)->first()->MS_Total}}</td>
                      <td class="text-center">{{$applicantDetail->applicant->previousQualification($semester->pkSemesterId)->first()->MS}}</td>
                      <td>
                          @if(round($applicantDetail->applicant->previousQualification($semester->pkSemesterId)->first()->MS) > 10)
                          {{round($applicantDetail->applicant->previousQualification($semester->pkSemesterId)->first()->MS/$applicantDetail->applicant->previousQualification($semester->pkSemesterId)->first()->MS_Total*100). ' %'}}
                          @endif
                      </td>
                  </tr>
              <?php 
          } ?>

          @if($applicantDetail->applicant->previousQualification($semester->pkSemesterId)->first()->resultAwaiting != 0)
          <tr>
          <td>
          @if($applicantDetail->applicant->previousQualification($semester->pkSemesterId)->first()->resultAwaiting != 0)
          {{'HSSC Result awaiting'}}
          </td>
          <td>
          {{'Yes'}} 
          @endif
          </td>
          </tr>
          @endif
          @if($applicantDetail->applicant->previousQualification($semester->pkSemesterId)->first()->resultwaitbsc != 0)
          <tr>
          <td>
          @if($applicantDetail->applicant->previousQualification($semester->pkSemesterId)->first()->resultwaitbsc != 0)
          {{'BSC Result awaiting'}}
          </td>
          <td>
          {{'Yes'}} 
          @endif
          </td>
          </tr>
          @endif
          @if($applicantDetail->applicant->previousQualification($semester->pkSemesterId)->first()->mba_experience != NULL)
          <tr>
          <td>
          {{'MBA-EXECUTIVE (Weekend Program) Experience'}}
          </td>
          <td>
          {{$applicantDetail->applicant->previousQualification($semester->pkSemesterId)->first()->mba_experience. ' Years'}} 
          </td>
          </tr>
          @endif

          @if($applicantDetail->applicant->previousQualification($semester->pkSemesterId)->first()->llb_total_marks != NULL || $applicantDetail->applicant->previousQualification($semester->pkSemesterId)->first()->llb_obtained_marks != NULL || $applicantDetail->applicant->previousQualification($semester->pkSemesterId)->first()->law_completion_date != NULL)
          <tr>
          <th class="text-center">Test</th>
          <th class="text-center">Total Marks</th>
          <th class="text-center">Obtained Marks</th>
          <th class="text-center">Completion Date</th>
          </tr>
          <tr>
          <td>
            {{'Law Admission Test Score'}}
          </td>
          <td>
            {{$applicantDetail->applicant->previousQualification($semester->pkSemesterId)->first()->llb_total_marks}} 
          </td>
          <td>
            {{$applicantDetail->applicant->previousQualification($semester->pkSemesterId)->first()->llb_obtained_marks}} 
          </td>

          <td>
            {{date("d-M-Y", strtotime($applicantDetail->applicant->previousQualification($semester->pkSemesterId)->first()->law_completion_date))}} 
          </td>
            
          </tr>
          @endif
  </table>


   </div>
   <div class="row">
        <div class="wd-50 cus-line pt-5">
         <b> Date Of Joining </b>
        </div>
        <div class="wd-50 fl-rgt cus-line text-rgt pt-5">
            <b>Student's Signature</b>
        </div>
   </div>
</div>
  
</body>
</html>
