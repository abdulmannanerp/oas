@extends('layouts.app')
@section('content')  
@php 
use App\Http\Controllers\Application\ApplicationController;
@endphp
<div class="page-content row">
    <div class="page-content-wrapper m-t">
        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div> <!-- Ending ending alert-danger --> 
        @endif
        <div class="sbox">
            <div class="sbox-title">
                <h1> Applicants Search</h1>
                <div class="pull-right">
                    <div class="form-group">
                        <form name="semesterSelectorForm" id="semesterSelectorForm" action="{{route('getSpecificSemesterApplications')}}"  method="post">
                            <select class="form-control" name="semesterSelector" id="semesterSelector">
                                <option> --Select Semester-- </option>
                                @foreach ( $semesters as $semester )
                                    <option value="{{ $semester->pkSemesterId }}" {{ ($semester->pkSemesterId == $selectedSemester ) ? 'selected' : '' }}> 
                                        {{ $semester->title }} 
                                    </option>
                                @endforeach
                            </select>
                        </form>
                    </div>
                </div>
            </div> <!-- Ending sbox-title --> 
            <div class="sbox-content "> 
                <div class="row">
                    <form id="appSearching" method="POST" action="{{route('applicants', ['semester' => $selectedSemester])}}" >
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="col-md-3">
                                <input class="form-control input-sm" name="search" id="search_input" type="text" value="{{ $searchable }}">
                            </div>  
                            <div class="col-md-7 mar-top">
                                <input type="radio" value="id" name="search_rdo" {{($filter == 'id')? 'checked="checked"' : 'checked="checked"'}}  value="blank" class="minimal-red"> <ins class="iCheck-helper"></ins> 
                                <label> Form Number</label>
                                <input type="radio" value="name" name="search_rdo" {{($filter == 'name')? 'checked="checked"' : ''}}  value="blank" class="minimal-red"> <ins class="iCheck-helper"></ins> 
                                <label> Name</label>
                                <input type="radio" value="cnic" name="search_rdo" {{($filter == 'cnic')? 'checked="checked"' : ''}} value="blank" class="minimal-red"> <ins class="iCheck-helper"></ins> 
                                <label> CNIC</label>
                                <input type="radio" value="email" name="search_rdo" {{($filter == 'email')? 'checked="checked"' : ''}} value="blank" class="minimal-red"> <ins class="iCheck-helper"></ins> 
                                <label> Email</label>
                                <input type="radio" value="rollno" name="search_rdo" {{($filter == 'rollno')? 'checked="checked"' : ''}} value="blank" class="minimal-red"> <ins class="iCheck-helper"></ins> 
                                <label> Roll Number</label>
                                <input type="radio" value="challan_no" name="search_rdo" {{($filter == 'challan_no')? 'checked="checked"' : ''}} value="blank" class="minimal-red"> <ins class="iCheck-helper"></ins> 
                                <label> Challan Number</label>
                            </div>
                            <div class="col-md-2">
                                Show <select name="entries" id="entries">
                                    <option value="10" {{($paginate == '10') ? 'selected' : ''}}>10</option>
                                    <option value="25" {{($paginate == '25') ? 'selected' : ''}}>25</option>
                                    <option value="50" {{($paginate == '50') ? 'selected' : ''}}>50</option>
                                    <option value="80" {{($paginate == '80') ? 'selected' : ''}}>80</option>
                                </select> Entries 
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="sbox-tools  clearfix cus-div">
                                    <button name="save" class="tips btn btn-sm btn-save" title="Back">
                                        <i class="fa  fa-paste"></i>Search 
                                    </button>
                                </div> <!-- Ending ending sbox-tools --> 
                            </div> <!-- Ending col-md-12 --> 
                        </div> <!-- Ending row --> 
                    </form>
                </div> <!-- Ending row --> 
                <div class="total-records"> {{ (($applications) ? $applications->total() .' Records' : '')}}</div>
                
                <div class="table-responsive" style="min-height:300px;">
                    <table class="table table-hover table-bordered app-search-grid appli-search">
                    <thead class="table-head">
                        <tr>
                            <th>Sr#</th>
                            <th> Form No </th>
                            <th>Name</th>
                            <th>CNIC</th>
                            <th>Program</th>
                            <th>Status</th>
                            @if(session('gid') != '11')
                                <th>Documents</th>
                            @endif
                            <th>Print</th>
                            @if(session('gid') != '11')
                                <th>Modify</th>
                            @endif
                        </tr>
                    </thead>
                    <tbody>
                        @if ( $applications )
                            @php
                                $i = ($applications->perPage()) * ($applications->currentPage() - 1);
                                ++$i;
                            @endphp
                            @foreach ( $applications as $application )
                            <tr>
                                <td> {{$i}} </td>
                                <td> {{ $application->id }} </td>
                                <td>
                                    {{ $application->applicant->name }}
                                    @if(auth()->id() == 1 || auth()->id() == 23)
                                        <a id="{{ $application->id }}" class="btn btn-xs btn-log super-click"><i class="fa fa-info"></i></a>
                                        <a href={{ route('view-log', ['id' => $application->id,  'fkApplicantId' => $application->fkApplicantId ]) }} 
                                            target="_blank" class="tips btn btn-xs btn-save flt-rgt clr-two" 
                                            title="Previous Qualification and Verify/Reject Log"><i class="fa fa-info"></i></a>
                                    @endif
                                </td>
                                <td> {{ $application->applicant->cnic }} </td>
                                <td>
                                    {{ $application->program->title ?? 'N/A' }}
                                    <?= (isset($application->semester->title))? '<br /><b>('.$application->semester->title.')</b>' :  '' ?>
                                </td>
                                    @php 
                                    $challan_data = $application->challanMany()->where('challan_type', 2)->where('fkApplicationtId', $application->id)->get()->toArray();
                                    // dd($challan_data);
                                    @endphp
                                    @if($challan_data)
                                    @endif
                                    {{-- Dev Mannan: Code ends --}}
                                {{-- </td> --}}
                                <td class="status-{{ $application->id }}">
                                    <span class="{{ $application->status->status }}">
                                        {{ $application->status->status }}
                                        @php
                                            if($application->status->pkAppStatusId == 6){
                                                echo $application->comments;
                                            }                                            
                                        @endphp
                                    </span>
                                </td>
                                @if(session('gid') != '11')
                                    <td>
                                        @if ( $application->fkCurrentStatus < 4)
                                            <span> Fee Not Verified </span>
                                        @else
                                            @if($application->fkSemesterId == $curr_sem->pkSemesterId)
                                                <button class="btn btn-primary docs-verification-btn btn-sm" data-form="{{ $application->id}}">
                                                    {{ ($application->fkCurrentStatus == 4 ) ? 'Verify' : 'Reverify'  }}
                                                </button>
                                            @endif
                                        @endif
                                    </td>
                                @endif
                                <td>
                                    <div class="sbox-tools clearfix cus-div flt-none print-renew">
                                        @if( $application->fkCurrentStatus > 1 )
                                            <p>
                                                <span class="fl-lft">Application Form:</span> 
                                                <a target="_blank"
                                                    href="{{route('applicantprintform',['oas_app_id' => $application->id, 'semester_id' => $selectedSemester])}}"
                                                    name="save" class="tips btn btn-sm btn-save flt-rgt" title="Application Form">
                                                    <i class="fa fa-print"></i>
                                                </a>
                                            </p>
                                            @if(session('gid') != '11')
                                                <!-- <p>
                                                    <span class="fl-lft">Challan:</span>
                                                    <a target="_blank" title="Challan" 
                                                        href="{{route('getFeeChallan', ['applicationid' => $application->id, 'semesterid' => $application->fkSemesterId, 'programid' => $application->fkProgramId, 'userId' => $application->applicant->userId])}}" 
                                                        name="save" class="tips btn btn-sm btn-save flt-rgt clr-two"><i class="fa fa-print"></i></a>
                                                </p> -->
                                            @endif
                                        @endif
                                        @if($application->fkCurrentStatus == 5 && session('gid') != '11')
                                            <p>
                                                <span class="fl-lft">Roll Number Slip:</span>
                                                <a target="_blank"
                                                    href="{{route('rollnumberprint',['oas_app_id'=>$application->id])}}"
                                                    class="tips btn btn-sm btn-save flt-rgt clr-three" title="Roll Number Slip">
                                                    <i class="fa fa-print"></i>
                                                </a>
                                            </p>
                                            
                                        @endif
                                        @if(session('gid') != '11' && $application->fkCurrentStatus >= 4 && ($application->applicant->fkNationality == 2 || $application->applicant->fkNationality == 3))
                                            <p>
                                                <span class="fl-lft">NOC Form:</span> 
                                                <a target="_blank"
                                                    href="{{route('nocprintform',['oas_app_id' => $application->id, 'semester_id' => $selectedSemester])}}"
                                                    name="save" class="tips btn btn-sm btn-save flt-rgt" title="Application Form">
                                                    <i class="fa fa-print"></i>
                                                </a>
                                            </p>
                                        @endif
                                    </div>
                                </td>  
                                @if(session('gid') != '11')
                                    <td>
                                        @php
                                            $param = base64_encode(auth()->id() . '/' . $application->applicant->userId);
                                        @endphp
                                        <div class="sbox-tools clearfix cus-div flt-none print-renew">
                                            @if(session('gid') != '9')
                                            <p>
                                                <span class="fl-lft">Application:</span>
                                                @if($application->fkSemesterId == $curr_sem->pkSemesterId)
                                                    <a name="save" id="{{$param}}" 
                                                        class="tips btn btn-sm btn-save md-fy flt-rgt" 
                                                        title="Application"><i class="fa fa-edit"></i></a>
                                                @endif
                                            </p>
                                            @endif
                                            @if ( session('gid') == 1 || session('gid') == 2 || session('gid') == 6 || session('gid') == 7)
                                            <p>
                                                <span class="fl-lft">Profile:</span>
                                                {{-- @if($application->fkSemesterId == $curr_sem->pkSemesterId)                                             --}}
                                                    <a href={{ route('edit-application', ['id' => $application->id ]) }} 
                                                        target="_blank" class="tips btn btn-sm btn-save flt-rgt clr-two" 
                                                        title="Profile"><i class="fa fa-edit"></i></a>
                                                {{-- @endif --}}
                                            </p>
                                            @endif 
                                        </div>
                                    </td>
                                @endif 
                            </tr>
                            @php $i++ @endphp
                            @endforeach
                        {{ $applications->appends(request()->all())->links() }}
                        @endif
                        <div id="verificaiton-modal" class="modal fade" role="dialog">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title">Document Verification</h4>
                                    </div>
                                    <div class="modal-body">
                                        <form action="#" method="POST" id="docsVerificationForm">
                                            <input type="hidden" id="applicationIdModalField" name="applicationId" value="">
                                            <div class="form-group">
                                                <label for="">Enter Comments</label>
                                                <br>
                                                <small class="color-red">*Mandatory in case of rejection</small>
                                                <textarea name="comments" id="docs-verify-reject-comment" cols="30" rows="10" class="form-control"></textarea>
                                            </div>
                                            <div class="form-group">
                                                <button type="submit" class="modalVerifyRejctBtn btn btn-success btn-sm" data-url="{{route('ajaxVerifyReject')}}">Approve</button>
                                                <button type="submit" class="modalVerifyRejctBtn btn btn-danger btn-sm" data-url="{{route('ajaxVerifyReject')}}">Reject</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div> <!-- Ending Modal -->

                        <div class="modal fade" id="superadminModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document" style="width: 100%;">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Applicant Log</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="admin-details"></div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    </div>
                                </div>
                            </div>
                            </div> <!-- Ending modal -->
                        </tbody>
                    </table>
                </div> <!-- Ending table-responsive --> 
            </div> <!-- Ending sbox-content - 18 --> 
        </div> <!-- Ending sbox - 14 --> 
    </div> <!-- Ending page-content-wrapper --> 
</div> <!-- Ending page-content --> 
@stop