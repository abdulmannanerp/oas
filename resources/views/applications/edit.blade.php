@extends('layouts.app')
@section('content')
    <div class="page-content row">
        <div class="page-content-wrapper m-t">
            <div class="sbox">
                <div class="sbox-title">
                    <h1> Edit Application </h1>
                </div> <!-- Ending sbox-title -->

                <div class="sbox-content ">
                    <div class="row">
                        <div class="col-sm-8">
                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            @if ($status)
                                <div class="alert alert-success" role="alert"> {{ $status }}</div>
                            @endif
                            <form action="{{ route('edit-application') }}" method="POST" class="quick-edit-form">
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <input type="hidden" name="id" value="{{ $application->id }}"
                                        class="form-control" />
                                    <input type="hidden" name="applicant" value="{{ $application->fkApplicantId }}"
                                        class="form-control" />
                                </div>
                                <div class="form-group">
                                    <label for="name">Name</label>
                                    <input type="text" name="name" class="form-control"
                                        value="{{ $application->applicant->name ?? '' }}" />
                                </div>
                                <div class="form-group">
                                    <label for="name">Email</label>
                                    <input type="email" name="email" class="form-control"
                                        value="{{ $application->applicant->email ?? '' }}" />
                                </div>
                                <div class="form-group">
                                    <label for="name">CNIC</label>
                                    <input type="text" name="cnic" class="form-control"
                                        value="{{ $application->applicant->cnic ?? '' }}" />
                                </div>
                                <div class="form-group">
                                    <label for="name">New Password</label>
                                    <input type="text" name="password" class="form-control" />
                                </div>
                                <div class="form-group">
                                    <label for="name">Status</label>
                                    <select name="status" class="form-control select2">
                                        <option value="1" {{ $application->applicant->status == 1 ? 'selected' : '' }}>
                                            Active</option>
                                        <option value="0"
                                            {{ $application->applicant->status == 0 ? 'selected' : '' }}>Suspended
                                        </option>
                                    </select>
                                </div>
                               <!-- Program Change Access to: 20 = Shafaqat | 192 = Noman | 180 = tahir | 209 =rubab -->
								
                                @if ($application->fkSemesterId == $current_semes && (auth()->id() == 20 || auth()->id() == 1 || auth()->id() == 192 || auth()->id() == 180 || auth()->id() == 209))
                                    <div class="form-group">
                                        <label for="name">Program</label>
                                        <select name="program" class="form-control select2">
                                            <option value="">--Select Program--</option>
                                            @foreach ($programs as $program)
                                                <option value="{{ $program->pkProgId }}"
                                                    {{ $application->fkProgramId == $program->pkProgId ? 'selected' : '' }}>
                                                    {{ $program->title }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                @endif
                                <div class="form-group">
                                    <input type="submit" name="submit" class="btn btn-primary" value="Save Changes" />
                                </div>
                            </form>
                        </div>
                        <div class="col-sm-4">
                            <h3>Application Log</h3>
                            @if ($logs->count())
                                <ul>
                                    @foreach ($logs as $log)
                                        </p>
                                        <li>
                                            <strong>
                                                {{ $log->user->first_name ?? ('' . ' ' . $log->user->last_name ?? '') }}
                                            </strong> Changed From <span class="green">
                                                {{ $log->existingProgram->title ?? 'N/A' }} </span> to <span
                                                class="red"> {{ $log->updatedProgram->title ?? 'N/A' }} </span> at
                                            <strong>{{ $log->created_at->format('D d-M-Y H:i A') }}</strong>
                                        </li>
                                    @endforeach
                                </ul>
                            @else
                                <h5><span class="green">No logs found</span></h5>
                            @endif
                        </div>
                    </div> <!-- Ending row -->
                </div> <!-- Ending sbox-content - 18 -->
            </div> <!-- Ending sbox - 14 -->
        </div> <!-- Ending page-content-wrapper -->
    </div> <!-- Ending page-content -->
@stop
