<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <!-- Set the viewport so this responsive site displays correctly on mobile devices -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Bank Challan Form</title>
        <!-- Include bootstrap CSS -->
        <link href="http://admission.iiu.edu.pk/assets/challanassets/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="http://admission.iiu.edu.pk/assets/challanassets/style.css" rel="stylesheet">
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:/ -->
        <!--[if lt IE 9]>
                <script src="https:/oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
                <script src="https:/oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
            <![endif]-->
        <style>
            @media print {

                body{
                    -webkit-transform: scale(.65%,.65%); 
                    -moz-transform: scale(.65%,.65%) }    }

            .copy, .padding-top-10, .padding-top-20, .challan-in {
                padding-top: 0px;
            }
            .challan-in td, th{
                padding: 0px !important;
                font-size: 10px;
            }
            p{
                font-size: 10px;
                line-height: normal;
            }
            .challan-in{
                margin: 0px !important;
            }
            body{
                line-height: normal;
            }
            .top-no{
                margin-top: 20px;
            }
            .top-mm{
                margin-top: 10px;
            }
            ul{
                padding-left: 15px;
            }
            ul li{
                 font-size: 10px;
            }
            .tpl{
                font-size: 10px;
            }
            @media (min-width: 992px){
                .col-md-8 {
                    width: 74.666667%;
                }
            }
        </style>
    </head>
    <body>

        <!-- Site header and navigation -->


        <!-- Site banner -->


        <!-- Middle content section -->


        <!-- Site footer -->
        <div class="challan-form-main">
            <div class="container">
            <!--<a href="#" class="print-button">Print <i class="fa fa-print"></i></a>-->
                <a id="printpagebutton" class="btn btn-default" style="float:right; margin-top: 1em;" type="button"  onclick="printpage()"/><i class="fa fa-print"></i> Print</a>
                <div class="row">


                    <div class="col-md-8 col-print-12 col-md-offset-2 col-sm-10 col-sd-offset-1">
                        <div class="row">
                            <div class="col-md-6 col-sm-6 col-print-6 col-xs-12">
                                <div class="challan-in">
                                    <div class="row">
                                        <div class="col-md-12 col-print-12 text-center logo-main">
                                            <img src="http://admission.iiu.edu.pk/assets/challanassets/images/logo.jpg" class="img-responsive" />
                                            <p>ADMISSION PROCESSING FEE</p>
                                            <p class="copy">BANK COPY</p>
                                        </div>
                                    </div>

                                    <div class="row padding-top-10">
                                        <div class="col-md-4 col-sm-4 col-print-4"> 
                                            <p><b>{{$oas_applications->semester->title }}</b></p>
                                        </div>
                                        <div class="col-md-8 col-sm-8 col-print-8 text-right">
                                            <p><b>{{$oas_applications->applicant->programLevel->programLevel}} Programe</b></p>
                                        </div>
                                    </div>
                                    <div class="row padding-top-10">
                                        <div class="col-md-6 col-sm-6 col-print-6"> 
                                            <p class='due-date'>Issue Date: 
<?php                                                
$issuedt = new DateTime($oas_applications->updated_at);

echo $issuedt->format('d-m-Y');
?></p>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-print-6 text-right"> 
                                            <p class="due-date">Due Date: <u>
 <?php 
$dt = new DateTime($oas_applications->program->programscheduler->lastDate);

// echo '26-07-2018';
echo $dt->format('d-m-Y');
?></u></p>
                                        </div>
                                    </div>
                                 
<?php
if ($oas_applications->applicant->fkGenderId == 3) {
    ?>
   
                                        <div class="row padding-top-10">
                                            <div class="col-md-4 col-sm-4 col-print-4">
                                                <img src="http://admission.iiu.edu.pk/assets/challanassets/images/HBL-logo-copy-640x480.jpg" class="img-responsive" />
                                            </div>
                                            <div class="col-md-8 col-sm-8 col-print-8 text-right padding-top-10">
                                                <p>A/C No: {{$bankDetails[0]->accountNo}} </p>
                                            </div>
                                        </div>
                                        <?php
                                    } elseif ($oas_applications->applicant->fkGenderId == 2) {
                                        ?>
                                        <div class="row padding-top-10">
                                            <div class="col-md-4 col-sm-4 col-print-4">
                                                <img src="http://admission.iiu.edu.pk/assets/challanassets/images/AlliedBank.png" class="img-responsive" />
                                            </div>
                                            <div class="col-md-8 col-sm-8 col-print-8 text-right padding-top-10">
                                                <p>A/C No: {{$bankDetails[0]->accountNo}} </p>
                                            </div>
                                        </div>
                                        
                                        <?php
                                    }
                                    ?>
                                    <div class="table-main padding-top-10">  
                                        <table>


                                            <tr>
                                                <td><b>Challan #</b></td> 
                                                <td><b>{{$oas_applications->id}} </b></td>
                                            </tr>
                                            <tr>
                                                <td>Admission Form #</td> 
                                                <td>{{$oas_applications->id}} </td>
                                            </tr>
                                            <tr>
                                                <td>Name:</td>
                                            <td>{{$oas_applications->applicant->name}} </td>
                                            </tr>
                                            <tr>
                                                <td>Program:</td>
                                            <td>{{$oas_applications->program->title}}</td>
                                            </tr>
                                            <tr>
                                                <td>Faculty</td>
                                                <td>{{$oas_applications->program->faculty->title}}</td>
                                            </tr>
                                        </table>
                                    </div>


                                    <div class="row padding-top-10">
                                        <div class="col-md-4 col-sm-4 col-print-4">
                                            <p>Cash (Rs):</p>
                                        </div>
                                        <div class="col-md-8 col-sm-8 col-print-8 text-right">
                                            <?php
                                            if ($oas_applications->applicant->fkGenderId == 3 && $oas_applications->applicant->fkNationality != 1) {
                                                ?>
                                                <p><?php echo '6000' ?>/-</p>
                                                <?php
                                            } elseif ($oas_applications->applicant->fkGenderId == 3) {
                                                ?>
                                                <p>{{$oas_applications->program->fee}}/-</p>
                                                <?php
                                            } elseif ($oas_applications->applicant->fkGenderId == 2 && $oas_applications->fkProgramId == 189 && $oas_applications->applicant->fkNationality == 1) {
                                                ?>
                                                <p>1500/-</p>
                                               
                                                <?php
                                            } elseif ($oas_applications->applicant->fkGenderId == 2 && $oas_applications->fkProgramId == 189 && $oas_applications->applicant->fkNationality != 1) {
                                                ?>
                                                <p>6000/-</p>
                                                <?php
                                            } elseif ($oas_applications->applicant->fkGenderId == 2 && $oas_applications->applicant->fkNationality != 1) {
                                                ?>
                                                <p><?php echo '6000' ?>/-</p>
                                                <?php
                                            } elseif ($oas_applications->applicant->fkGenderId == 2) {
                                                ?>
                                                <p>{{$oas_applications->program->fee}}/-</p>
                                                <?php
                                            }
                                            ?>
                                        </div>
                                    </div>

                                    <div class="row padding-top-10">
                                        <div class="col-md-12 col-print-12">
                                            <b class='tpl'>Total Payment (Cash) Rs:</b> 
                                            <?php 
                                            if ($oas_applications->program->fee == 2000 && $oas_applications->fkProgramId == 189 && $oas_applications->applicant->fkGenderId == 3 && $oas_applications->applicant->fkNationality == 1) {
                                                ?>
                                                <p><b>(Two Thousand) Rupees Only.</b></p>
    <?php
} elseif ($oas_applications->program->fee == 2000 && $oas_applications->fkProgramId == 189 && $oas_applications->applicant->fkGenderId == 3 && $oas_applications->applicant->fkNationality != 1) {
    ?>
                                                <p><b>(Six Thousand) Rupees Only.&nbsp;Or&nbsp;   US $50</b></p>
                                                <?php
                                            } elseif ($oas_applications->applicant->fkGenderId == 2 && $oas_applications->applicant->fkNationality != 1) {
                                                ?>
                                                <p><b>(Six Thousand) Rupees Only.&nbsp;Or&nbsp;   US $50</b></p>
                                                <?php
                                            } elseif ($oas_applications->applicant->fkGenderId == 2 && $oas_applications->applicant->fkNationality == 1) {
                                                ?>
                                                <p><b>(One Thousand Five Hundred) Rupees Only.</b></p>
                                                <?php
                                            } elseif ($oas_applications->applicant->fkGenderId == 3 && $oas_applications->applicant->fkNationality != 1) {
                                                ?>
                                                <p><b>(Six Thousand) Rupees Only.&nbsp;Or&nbsp;   US $50</b></p>
                                                <?php
                                            } elseif ($oas_applications->applicant->fkGenderId == 3 && $oas_applications->applicant->fkNationality == 1) {
                                                ?>
                                                <p><b>(One Thousand Five Hundred) Rupees Only.</b></p>
                                                <?php
                                            }
                                            ?>

                                        </div>

                                    </div>

                                    <div class="row padding-top-10">
                                        <div class="col-md-12 col-print-12">
                                            <ul>
                                                <li><b>Overwriting will Not be accepted.</b></li>
                                                <li><b><u>Application Form will not be entertained without Original Deposit Slip (Admission office Copy) with original Bank stamp.</u></b></li>
                                                <li><b>This Fee is Non-Refundable / Non transferable in anycase.</b></li>
                                            </ul>

                                            <p><b style="text-decoration:underline">Bank Instructions:</b><br>	
                                                1) Please enter the fee Challan No at the time of punching.

                                                <br>2) All branches are requested to receive the challan.
                                            </p>
                                        </div>

                                    </div>

                                    <div class="row padding-top-10">
                                        <div class="col-md-12 col-print-12 text-center">
                                            <p class='top-mm'><b>Bank stamp</b> </p>
                                        </div>

                                    </div>

                                    <div class="row padding-top-20">
                                        <div class="col-md-12 col-print-12 text-center">
                                            <p><b><?php if ($oas_applications->applicant->fkGenderId == 2) echo 'Allied Bank';
                                            elseif ($oas_applications->applicant->fkGenderId == 3) echo 'Habib Bank Ltd.'; ?></b></p>

                                        </div>

                                    </div>        

                                </div>
                            </div>



                            <div class="col-md-6 col-sm-6 col-print-6 col-xs-12">
                                <div class="challan-in">
                                    <div class="row">
                                        <div class="col-md-12 col-print-12 text-center logo-main">
                                            <img src="http://admission.iiu.edu.pk/assets/challanassets/images/logo.jpg" class="img-responsive" />
                                            <p>ADMISSION PROCESSING FEE</p>
                                            <p class="copy">ACCOUNTS COPY</p>
                                        </div>
                                    </div>

                                    <div class="row padding-top-10">
                                        <div class="col-md-4 col-sm-4 col-print-4">
                                            <p><b>{{$oas_applications->semester->title }}</b></p>
                                        </div>
                                        <div class="col-md-8 col-sm-8 col-print-8 text-right">
                                            <p><b>{{$oas_applications->applicant->programLevel->programLevel}} Programe</b></p>
                                        </div>
                                    </div>
                                    <div class="row padding-top-10">
                                        <div class="col-md-6 col-sm-6 col-print-6">
                                            <p class='due-date'>Issue Date: <?php 
                                            $issuedt = new DateTime($oas_applications->updated_at);

                                            echo $issuedt->format('d-m-Y');
                                            ?></p>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-print-6 text-right">
                                            <p class="due-date">Due Date: <u><?php
                                            $dt = new DateTime($oas_applications->program->programscheduler->lastDate);

                                            // echo '26-07-2018';
                                            echo $dt->format('d-m-Y');
                                            ?></u></p>
                                        </div>
                                    </div>
<?php
if ($oas_applications->applicant->fkGenderId == 3) {
    ?>
                                        <div class="row padding-top-10">
                                            <div class="col-md-4 col-sm-4 col-print-4">
                                                <img src="http://admission.iiu.edu.pk/assets/challanassets/images/HBL-logo-copy-640x480.jpg" class="img-responsive" />
                                            </div>
                                            <div class="col-md-8 col-sm-8 col-print-8 text-right padding-top-10">
                                                <p>A/C No: {{$bankDetails[0]->accountNo}}</p>
                                            </div>
                                        </div>
                                                    <?php
                                                } elseif ($oas_applications->applicant->fkGenderId == 2) {
                                                    ?>
                                        <div class="row padding-top-10">
                                            <div class="col-md-4 col-sm-4 col-print-4">
                                                <img src="http://admission.iiu.edu.pk/assets/challanassets/images/AlliedBank.png" class="img-responsive" />
                                            </div>
                                            <div class="col-md-8 col-sm-8 col-print-8 text-right padding-top-10">
                                                <p>A/C No: {{$bankDetails[0]->accountNo}}</p>
                                            </div>
                                        </div>
    <?php
}
?>

                                    <div class="table-main padding-top-10">  
                                        <table>


                                            <tr>
                                                <td><b>Challan #</b></td>
                                                <td><b>{{$oas_applications->id}}</b> </td>
                                            </tr>
                                            <tr>
                                                <td>Admission Form #</td>
                                                <td>{{$oas_applications->id}} </td>
                                            </tr>
                                            <tr>
                                                <td>Name:</td>
                                                <td>{{$oas_applications->applicant->name}}</td>
                                            </tr>
                                            <tr>
                                                <td>Program:</td>
                                                <td>{{$oas_applications->program->title}}</td>
                                            </tr>
                                            <tr>
                                                <td>Faculty</td>
                                                <td>{{$oas_applications->program->faculty->title}}</td>
                                            </tr>
                                        </table>
                                    </div>


                                    <div class="row padding-top-10">
                                        <div class="col-md-4 col-sm-4 col-print-4">
                                            <p>Cash (Rs):</p>
                                        </div>
                                        <div class="col-md-8 col-sm-8 col-print-8 text-right">
<?php
if ($oas_applications->applicant->fkGenderId == 3 && $oas_applications->applicant->fkNationality != 1) {
    ?>
                                                <p><?php echo '6000' ?>/-</p>
    <?php
} elseif ($oas_applications->applicant->fkGenderId == 3) {
    ?>
                                                <p>{{$oas_applications->program->fee}}/-</p>
    <?php
} elseif ($oas_applications->applicant->fkGenderId == 2 && $oas_applications->fkProgramId == 189 && $oas_applications->applicant->fkNationality == 1) {
    ?>
                                                <p>1500/-</p>
    <?php
} elseif ($oas_applications->applicant->fkGenderId == 2 && $oas_applications->fkProgramId == 189 && $oas_applications->applicant->fkNationality != 1) {
    ?>
                                                <p>6000/-</p>
    <?php
} elseif ($oas_applications->applicant->fkGenderId == 2 && $oas_applications->applicant->fkNationality != 1) {
    ?>
                                                <p><?php echo '6000' ?>/-</p>
                                                <?php
                                            } elseif ($oas_applications->applicant->fkGenderId == 2) {
                                                ?>
                                                <p>{{$oas_applications->program->fee}}/-</p>
                                                <?php
                                            }
                                            ?>
                                        </div>
                                    </div>

                                    <div class="row padding-top-10">
                                        <div class="col-md-12 col-print-12">
                                            <b class='tpl'>Total Payment (Cash) Rs:</b>
                                            <?php
                                            if ($oas_applications->program->fee == 2000 && $oas_applications->fkProgramId == 189 && $oas_applications->applicant->fkGenderId == 3 && $oas_applications->applicant->fkNationality == 1) {
                                                ?>
                                                <p><b>(Two Thousand) Rupees Only.</b></p>
                                                <?php
                                            } elseif ($oas_applications->program->fee == 2000 && $oas_applications->fkProgramId == 189 && $oas_applications->applicant->fkGenderId == 3 && $oas_applications->applicant->fkNationality != 1) {
                                                ?>
                                                <p><b>(Six Thousand) Rupees Only.&nbsp;Or&nbsp;   US $50</b></p>
                                                <?php
                                            } elseif ($oas_applications->applicant->fkGenderId == 2 && $oas_applications->applicant->fkNationality != 1) {
                                                ?>
                                                <p><b>(Six Thousand) Rupees Only.&nbsp;Or&nbsp;   US $50</b></p>
                                                <?php
                                            } elseif ($oas_applications->applicant->fkGenderId == 2 && $oas_applications->applicant->fkNationality == 1) {
                                                ?>
                                                <p><b>(One Thousand Five Hundred) Rupees Only.</b></p>
                                                <?php
                                            } elseif ($oas_applications->applicant->fkGenderId == 3 && $oas_applications->applicant->fkNationality != 1) {
                                                ?>
                                                <p><b>(Six Thousand) Rupees Only.&nbsp;Or&nbsp;   US $50</b></p>
                                                <?php
                                            } elseif ($oas_applications->applicant->fkGenderId == 3 && $oas_applications->applicant->fkNationality == 1) {
                                                ?>
                                                <p><b>(One Thousand Five Hundred) Rupees Only.</b></p>
    <?php
}
?>
                                        </div>

                                    </div>

                                    <div class="row padding-top-10">
                                        <div class="col-md-12 col-print-12">
                                            <ul>
                                                <li><b>Overwriting will Not be accepted.</b></li>
                                                <li><b><u>Application Form will not be entertained without Original Deposit Slip (Admission office Copy) with original Bank stamp.</u></b></li>
                                                <li><b>This Fee is Non-Refundable / Non transferable in anycase.</b></li>
                                            </ul>
                                            <p><b style="text-decoration:underline">Bank Instructions:</b><br>	
                                                1) Please enter the fee Challan No at the time of punching.

                                                <br>2) All branches are requested to receive the challan.
                                            </p>
                                        </div>

                                    </div>

                                    <div class="row padding-top-10">
                                        <div class="col-md-12 col-print-12 text-center">
                                            <p class='top-mm'><b>Bank stamp</b></p>

                                        </div>

                                    </div>

                                    <div class="row padding-top-20">
                                        <div class="col-md-12 col-print-12 text-center">
                                            <p><b><?php if ($oas_applications->applicant->fkGenderId == 2) echo 'Allied Bank';
elseif ($oas_applications->applicant->fkGenderId == 3) echo 'Habib Bank Ltd.'; ?></b></p>

                                        </div>

                                    </div>        

                                </div>
                            </div>




                            <div class="col-md-6 col-sm-6 col-print-6 col-xs-12 top-no">
                                <div class="challan-in">
                                    <div class="row">
                                        <div class="col-md-12 col-print-12 text-center logo-main">
                                            <img src="http://admission.iiu.edu.pk/assets/challanassets/images/logo.jpg" class="img-responsive" />
                                            <p>ADMISSION PROCESSING FEE</p>
                                            <p class="copy">ADMISSION OFFICE COPY</p>
                                        </div>
                                    </div>

                                    <div class="row padding-top-10">
                                        <div class="col-md-4 col-sm-4 col-print-4">
                                            <p><b>{{$oas_applications->semester->title }}</b></p>
                                        </div>
                                        <div class="col-md-8 col-sm-8 col-print-8 text-right">
                                            <p><b>{{$oas_applications->applicant->programLevel->programLevel}} Programe</b></p>
                                        </div>
                                    </div>
                                    <div class="row padding-top-10">
                                        <div class="col-md-6 col-sm-6 col-print-6">
                                            <p class='due-date'>Issue Date: <?php
                                            $issuedt = new DateTime($oas_applications->updated_at);

                                            echo $issuedt->format('d-m-Y');
?></p>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-print-6 text-right">
                                            <p class="due-date">Due Date: <u>
                                                <?php
                                            $dt = new DateTime($oas_applications->program->programscheduler->lastDate);

                                            // echo '26-07-2018';
                                            echo $dt->format('d-m-Y');
?></u></p>
                                        </div>
                                    </div>

<?php
if ($oas_applications->applicant->fkGenderId == 3) {
    if ($oas_applications->program->fkDepId != 29) {
        ?>
                                            <div class="row padding-top-10">
                                                <div class="col-md-4 col-sm-4 col-print-4">
                                                    <img src="http://admission.iiu.edu.pk/assets/challanassets/images/HBL-logo-copy-640x480.jpg" class="img-responsive" />
                                                </div>
                                                <div class="col-md-8 col-sm-8 col-print-8 text-right padding-top-10">
                                                    <p>A/C No: {{$bankDetails[0]->accountNo}}</p>
                                                </div>
                                            </div>
        <?php
    } elseif ($oas_applications->program->fkDepId == 29) {
        ?>
                                            <div class="row padding-top-10">
                                                <div class="col-md-4 col-sm-4 col-print-4">
                                                    <img src="http://admission.iiu.edu.pk/assets/challanassets/images/HBL-logo-copy-640x480.jpg" class="img-responsive" />
                                                </div>
                                                <div class="col-md-8 col-sm-8 col-print-8 text-right padding-top-10">
                                                    <p>A/C No: {{$bankDetails[0]->accountNo}}</p>
                                                </div>
                                            </div>
                                                        <?php
                                                    }
                                                    ?>
    <?php
} elseif ($oas_applications->applicant->fkGenderId == 2) {
    if ($oas_applications->program->fkDepId != 29) {
        ?>
                                            <div class="row padding-top-10">
                                                <div class="col-md-4 col-sm-4 col-print-4">
                                                    <img src="http://admission.iiu.edu.pk/assets/challanassets/images/AlliedBank.png" class="img-responsive" />
                                                </div>
                                                <div class="col-md-8 col-sm-8 col-print-8 text-right padding-top-10">
                                                    <p>A/C No: {{$bankDetails[0]->accountNo}}</p>
                                                </div>
                                            </div>
                                            <?php
                                        } elseif ($oas_applications->program->fkDepId == 29) {
                                            ?>

                                            <div class="row padding-top-10">
                                                <div class="col-md-4 col-sm-4 col-print-4">
                                                    <img src="http://admission.iiu.edu.pk/assets/challanassets/images/AlliedBank.png" class="img-responsive" />
                                                </div>
                                                <div class="col-md-8 col-sm-8 col-print-8 text-right padding-top-10">
                                                    <p>A/C No: {{$bankDetails[0]->accountNo}}</p>
                                                </div>
                                            </div>
                                            <?php
                                        }
                                    }
                                    ?>
                                    <div class="table-main padding-top-10">  
                                        <table>


                                            <tr>
                                                <td><b>Challan #</b></td>
                                                <td><b>{{$oas_applications->id}}</b> </td>
                                            </tr>
                                            <tr>
                                                <td>Admission Form #</td>
                                                <td>{{$oas_applications->id}} </td>
                                            </tr>
                                            <tr>
                                                <td>Name:</td>
                                                <td>{{$oas_applications->applicant->name}}</td>
                                            </tr>
                                            <tr>
                                                <td>Program:</td>
                                                <td>{{$oas_applications->program->title}}</td>
                                            </tr>
                                            <tr>
                                                <td>Faculty</td>
                                                <td>{{$oas_applications->program->faculty->title}}</td>
                                            </tr>
                                        </table>
                                    </div>


                                    <div class="row padding-top-10">
                                        <div class="col-md-4 col-sm-4 col-print-4">
                                            <p>Cash (Rs):</p>
                                        </div>
                                        <div class="col-md-8 col-sm-8 col-print-8 text-right">
<?php
if ($oas_applications->applicant->fkGenderId == 3 && $oas_applications->applicant->fkNationality != 1) {
    ?>
                                                <p><?php echo '6000' ?>/-</p>
    <?php
} elseif ($oas_applications->applicant->fkGenderId == 3) {
    ?>
                                                <p>{{$oas_applications->program->fee}}/-</p>
                                        <?php
                                    } elseif ($oas_applications->applicant->fkGenderId == 2 && $oas_applications->fkProgramId == 189 && $oas_applications->applicant->fkNationality == 1) {
                                        ?>
                                                <p>1500/-</p>
    <?php
} elseif ($oas_applications->applicant->fkGenderId == 2 && $oas_applications->fkProgramId == 189 && $oas_applications->applicant->fkNationality != 1) {
    ?>
                                                <p>6000/-</p>
    <?php
} elseif ($oas_applications->applicant->fkGenderId == 2 && $oas_applications->applicant->fkNationality != 1) {
    ?>
                                                <p><?php echo '6000' ?>/-</p>
    <?php
} elseif ($oas_applications->applicant->fkGenderId == 2) {
    ?>
                                                <p>{{$oas_applications->program->fee}}/-</p>
    <?php
}
?>
                                        </div>
                                    </div>

                                    <div class="row padding-top-10">
                                        <div class="col-md-12 col-print-12">
                                            <b class='tpl'>Total Payment (Cash) Rs:</b>
<?php
if ($oas_applications->program->fee == 2000 && $oas_applications->fkProgramId == 189 && $oas_applications->applicant->fkGenderId == 3 && $oas_applications->applicant->fkNationality == 1) {
    ?>
                                                <p><b>(Two Thousand) Rupees Only.</b></p>
    <?php
} elseif ($oas_applications->program->fee == 2000 && $oas_applications->fkProgramId == 189 && $oas_applications->applicant->fkGenderId == 3 && $oas_applications->applicant->fkNationality != 1) {
    ?>
                                                <p><b>(Six Thousand) Rupees Only.&nbsp;Or&nbsp;   US $50</b></p>
    <?php
} elseif ($oas_applications->applicant->fkGenderId == 2 && $oas_applications->applicant->fkNationality != 1) {
    ?>
                                                <p><b>(Six Thousand) Rupees Only.&nbsp;Or&nbsp;   US $50</b></p>
                                                <?php
                                            } elseif ($oas_applications->applicant->fkGenderId == 2 && $oas_applications->applicant->fkNationality == 1) {
                                                ?>
                                                <p><b>(One Thousand Five Hundred) Rupees Only.</b></p>
                                                <?php
                                            } elseif ($oas_applications->applicant->fkGenderId == 3 && $oas_applications->applicant->fkNationality != 1) {
                                                ?>
                                                <p><b>(Six Thousand) Rupees Only.&nbsp;Or&nbsp;   US $50</b></p>
                                                <?php
                                            } elseif ($oas_applications->applicant->fkGenderId == 3 && $oas_applications->applicant->fkNationality == 1) {
                                                ?>
                                                <p><b>(One Thousand Five Hundred) Rupees Only.</b></p>
                                                <?php
                                            }
                                            ?>
                                        </div>

                                    </div>

                                    <div class="row padding-top-10">
                                        <div class="col-md-12 col-print-12">
                                            <ul>
                                                <li><b>Overwriting will Not be accepted.</b></li>
                                                <li><b><u>Application Form will not be entertained without Original Deposit Slip (Admission office Copy) with original Bank stamp.</u></b></li>
                                                <li><b>This Fee is Non-Refundable / Non transferable in anycase.</b></li>
                                            </ul>
                                            <p><b style="text-decoration:underline">Bank Instructions:</b><br>	
                                                1) Please enter the fee Challan No at the time of punching.

                                                <br>2) All branches are requested to receive the challan.
                                            </p>
                                        </div>

                                    </div>

                                    <div class="row padding-top-10">
                                        <div class="col-md-12 col-print-12 text-center">
                                            <p class='top-mm'><b>Bank stamp</b></p>

                                        </div>

                                    </div>

                                    <div class="row padding-top-20">
                                        <div class="col-md-12 col-print-12 text-center">
                                            <p><b><?php if ($oas_applications->applicant->fkGenderId == 2) echo 'Allied Bank';
                                            elseif ($oas_applications->applicant->fkGenderId == 3) echo 'Habib Bank Ltd.'; ?></b></p>

                                        </div>

                                    </div>        

                                </div>
                            </div>


                            <div class="col-md-6 col-sm-6 col-print-6 col-xs-12 top-no">
                                <div class="challan-in">
                                    <div class="row">
                                        <div class="col-md-12 col-print-12 text-center logo-main">
                                            <img src="http://admission.iiu.edu.pk/assets/challanassets/images/logo.jpg" class="img-responsive" />
                                            <p>ADMISSION PROCESSING FEE</p>
                                            <p class="copy">STUDENT COPY</p>
                                        </div>
                                    </div>

                                    <div class="row padding-top-10">
                                        <div class="col-md-4 col-sm-4 col-print-4">
                                            <p><b>{{$oas_applications->semester->title }}</b></p>
                                        </div>
                                        <div class="col-md-8 col-sm-8 col-print-8 text-right">
                                            <p><b>{{$oas_applications->applicant->programLevel->programLevel}} Programe</b></p>
                                        </div>
                                    </div>
                                    <div class="row padding-top-10">
                                        <div class="col-md-6 col-sm-6 col-print-6">
                                            <p class='due-date'>Issue Date: <?php 
                                            $issuedt = new DateTime($oas_applications->updated_at);

                                            echo $issuedt->format('d-m-Y');
                                            ?></p>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-print-6 text-right">
                                            <p class="due-date">Due Date: <u><?php 
                                            $dt = new DateTime($oas_applications->program->programscheduler->lastDate);

                                            // echo '26-07-2018';
                                            echo $dt->format('d-m-Y');
                                            ?></u></p>
                                        </div>
                                    </div>

<?php
if ($oas_applications->applicant->fkGenderId == 3) {
    ?>
                                        <div class="row padding-top-10">
                                            <div class="col-md-4 col-sm-4 col-print-4">
                                                <img src="http://admission.iiu.edu.pk/assets/challanassets/images/HBL-logo-copy-640x480.jpg" class="img-responsive" />
                                            </div>
                                            <div class="col-md-8 col-sm-8 col-print-8 text-right padding-top-10">
                                                <p>A/C No: {{$bankDetails[0]->accountNo}}</p>
                                            </div>
                                        </div>
    <?php
} elseif ($oas_applications->applicant->fkGenderId == 2) {
    ?>
                                        <div class="row padding-top-10">
                                            <div class="col-md-4 col-sm-4 col-print-4">
                                                <img src="http://admission.iiu.edu.pk/assets/challanassets/images/AlliedBank.png" class="img-responsive" />
                                            </div>
                                            <div class="col-md-8 col-sm-8 col-print-8 text-right padding-top-10">
                                                <p>A/C No: {{$bankDetails[0]->accountNo}}</p>
                                            </div>
                                        </div>
    <?php
}
?>

                                    <div class="table-main padding-top-10">  
                                        <table>


                                            <tr>
                                                <td><b>Challan #</b></td>
                                                <td><b>{{$oas_applications->id}} </b></td>
                                            </tr>
                                            <tr>
                                                <td>Admission Form #</td>
                                                <td>{{$oas_applications->id}} </td>
                                            </tr>
                                            <tr>
                                                <td>Name:</td>
                                                <td>{{$oas_applications->applicant->name}}</td>
                                            </tr>
                                            <tr>
                                                <td>Program:</td>
                                                <td>{{$oas_applications->program->title}}</td>
                                            </tr>
                                            <tr>
                                                <td>Faculty</td>
                                                <td>{{$oas_applications->program->faculty->title}}</td>
                                            </tr>
                                        </table>
                                    </div>


                                    <div class="row padding-top-10">
                                        <div class="col-md-4 col-sm-4 col-print-4">
                                            <p>Cash (Rs):</p>
                                        </div>
                                        <div class="col-md-8 col-sm-8 col-print-8 text-right">
                                    <?php
                                    if ($oas_applications->applicant->fkGenderId == 3 && $oas_applications->applicant->fkNationality != 1) {
                                        ?>
                                                <p><?php echo '6000' ?>/-</p>
    <?php
} elseif ($oas_applications->applicant->fkGenderId == 3) {
    ?>
                                                <p>{{$oas_applications->program->fee}}/-</p>
    <?php
} elseif ($oas_applications->applicant->fkGenderId == 2 && $oas_applications->fkProgramId == 189 && $oas_applications->applicant->fkNationality == 1) {
    ?>
                                                <p>1500/-</p>
                                        <?php
                                    } elseif ($oas_applications->applicant->fkGenderId == 2 && $oas_applications->fkProgramId == 189 && $oas_applications->applicant->fkNationality != 1) {
                                        ?>
                                                <p>6000/-</p>
                                        <?php
                                    } elseif ($oas_applications->applicant->fkGenderId == 2 && $oas_applications->applicant->fkNationality != 1) {
                                        ?>
                                                <p><?php echo '6000' ?>/-</p>
    <?php
} elseif ($oas_applications->applicant->fkGenderId == 2) {
    ?>
                                                <p>{{$oas_applications->program->fee}}/-</p>
                                        <?php
                                    }
                                    ?>
                                        </div>
                                    </div>

                                    <div class="row padding-top-10">
                                        <div class="col-md-12 col-print-12">
                                            <b class='tpl'>Total Payment (Cash) Rs:</b>
<?php
if ($oas_applications->program->fee == 2000 && $oas_applications->fkProgramId == 189 && $oas_applications->applicant->fkGenderId == 3 && $oas_applications->applicant->fkNationality == 1) {
    ?>
                                                <p><b>(Two Thousand) Rupees Only.</b></p>
    <?php
} elseif ($oas_applications->program->fee == 2000 && $oas_applications->fkProgramId == 189 && $oas_applications->applicant->fkGenderId == 3 && $oas_applications->applicant->fkNationality != 1) {
    ?>
                                                <p><b>(Six Thousand) Rupees Only.&nbsp;Or&nbsp;   US $50</b></p>
    <?php
} elseif ($oas_applications->applicant->fkGenderId == 2 && $oas_applications->applicant->fkNationality != 1) {
    ?>
                                                <p><b>(Six Thousand) Rupees Only.&nbsp;Or&nbsp;   US $50</b></p>
    <?php
} elseif ($oas_applications->applicant->fkGenderId == 2 && $oas_applications->applicant->fkNationality == 1) {
    ?>
                                                <p><b>(One Thousand Five Hundred) Rupees Only.</b></p>
    <?php
} elseif ($oas_applications->applicant->fkGenderId == 3 && $oas_applications->applicant->fkNationality != 1) {
    ?>
                                                <p><b>(Six Thousand) Rupees Only.&nbsp;Or&nbsp;   US $50</b></p>
    <?php
} elseif ($oas_applications->applicant->fkGenderId == 3 && $oas_applications->applicant->fkNationality == 1) {
    ?>
                                                <p><b>(One Thousand Five Hundred) Rupees Only.</b></p>
    <?php
}
?>
                                        </div>

                                    </div>

                                    <div class="row padding-top-10">
                                        <div class="col-md-12 col-print-12">
                                            <ul>
                                                <li><b>Overwriting will Not be accepted.</b></li>
                                                <li><b><u>Application Form will not be entertained without Original Deposit Slip (Admission office Copy) with original Bank stamp.</u></b></li>
                                                <li><b>This Fee is Non-Refundable / Non transferable in anycase.</b></li>
                                            </ul>
                                            <p><b style="text-decoration:underline">Bank Instructions:</b><br>	
                                                1) Please enter the fee Challan No at the time of punching.

                                                <br>2) All branches are requested to receive the challan.
                                            </p>
                                        </div>

                                    </div>

                                    <div class="row padding-top-10">
                                        <div class="col-md-12 col-print-12 text-center">
                                            <p class='top-mm'><b>Bank stamp</b></p>

                                        </div>

                                    </div>

                                    <div class="row padding-top-20">
                                        <div class="col-md-12 col-print-12 text-center">
                                            <p><b><?php if ($oas_applications->applicant->fkGenderId == 2) echo 'Allied Bank';
elseif ($oas_applications->applicant->fkGenderId == 3) echo 'Habib Bank Ltd.'; ?></b></p>

                                        </div>

                                    </div>        

                                </div>
                            </div>





                        </div>  

                    </div>


                </div>


            </div>
        </div>


        <script src="http://admission.iiu.edu.pk/assets/challanassets/bootstrap/js/bootstrap.min.js"></script>
        <script src="http://admission.iiu.edu.pk/assets/challanassets/print.js"></script>
    </body>
</html>