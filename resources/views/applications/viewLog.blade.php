@extends('layouts.app')
@section('content')
    <div class="page-content row">
        <div class="page-content-wrapper m-t">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div> <!-- Ending ending alert-danger -->
            @endif

            <div class="sbox">
                <div class="sbox-title">
                    <h1> Previous Qualification Log</h1>
                </div> <!-- Ending sbox-title -->
                <div class="sbox-content ">
                    {{-- {{dd($previousQualificationLog)}} --}}
                    @if ($previousQualificationLog)
                        @foreach ($previousQualificationLog as $pq)
                            <?php
                            $userInfo = \DB::select("SELECT * from `tb_users`  WHERE id = $pq->modified_by");
                            if ($userInfo) {
                                $userData = $userInfo[0]->first_name . ' ' . $userInfo[0]->last_name;
                            } else {
                                $userData = '';
                            }
                            ?>
                            <div class="row">
                                <div class="col-md-12">
                                    @php
                                        echo '<div class="algn-cntr"><p><b>created_at=</b> ' . $pq->created_at . '</p>';
                                        echo '<p><b>modified_by=</b> ' . $userData . '</p></div>';
                                        $ed = unserialize($pq->existing_value);
                                        echo '<div class="log-div-quali">
                                    <p class="log-rec"> <h4><b>Existing Data</b></h4> </p>
                                    <p class="log-rec"><b> SSC_Title=</b> ' .
                                            $ed['SSC_Title'] .
                                            '</p>' .
                                            '<p class="log-rec"><b> SSC_From=</b> ' .
                                            $ed['SSC_From'] .
                                            '</p>' .
                                            '<p class="log-rec"><b> SSC_Subject=</b> ' .
                                            $ed['SSC_Subject'] .
                                            '</p>' .
                                            '<p class="log-rec"><b> SSC_Year=</b> ' .
                                            $ed['SSC_Year'] .
                                            '</p>' .
                                            '<p class="log-rec"><b> SSC_Total=</b> ' .
                                            $ed['SSC_Total'] .
                                            '</p>' .
                                            '<p class="log-rec"><b> SSC_Composite=</b> ' .
                                            $ed['SSC_Composite'] .
                                            '</p>' .
                                            '<p class="log-rec"><b> SSC_degree_type=</b> ' .
                                            $ed['SSC_degree_type'] .
                                            '</p>' .
                                            '<p class="log-rec"><b> HSC_Title=</b> ' .
                                            $ed['HSC_Title'] .
                                            '</p>' .
                                            '<p class="log-rec"><b> HSC_From=</b> ' .
                                            $ed['HSC_From'] .
                                            '</p>' .
                                            '<p class="log-rec"><b> HSC_Subject=</b> ' .
                                            $ed['HSC_Subject'] .
                                            '</p>' .
                                            '<p class="log-rec"><b> HSC_Year=</b> ' .
                                            $ed['HSC_Year'] .
                                            '</p>' .
                                            '<p class="log-rec"><b> HSSC_Total=</b> ' .
                                            $ed['HSSC_Total'] .
                                            '</p>' .
                                            '<p class="log-rec"><b> HSSC_Composite=</b> ' .
                                            $ed['HSSC_Composite'] .
                                            '</p>' .
                                            '<p class="log-rec"><b> HSSC_Total1=</b> ' .
                                            $ed['HSSC_Total1'] .
                                            '</p>' .
                                            '<p class="log-rec"><b> HSSC1=</b> ' .
                                            $ed['HSSC1'] .
                                            '</p>' .
                                            '<p class="log-rec"><b> HSSC_Total2=</b> ' .
                                            $ed['HSSC_Total2'] .
                                            '</p>' .
                                            '<p class="log-rec"><b> HSSC2=</b> ' .
                                            $ed['HSSC2'] .
                                            '</p>' .
                                            '<p class="log-rec"><b> resultAwaiting=</b> ' .
                                            $ed['resultAwaiting'] .
                                            '</p>' .
                                            '<p class="log-rec"><b> HSC_degree_type=</b> ' .
                                            $ed['HSC_degree_type'] .
                                            '</p>' .
                                            '<p class="log-rec"><b> llb_total_marks=</b> ' .
                                            $ed['llb_total_marks'] .
                                            '</p>' .
                                            '<p class="log-rec"><b> llb_obtained_marks=</b> ' .
                                            $ed['llb_obtained_marks'] .
                                            '</p>' .
                                            '<p class="log-rec"><b> law_completion_date=</b> ' .
                                            $ed['law_completion_date'] .
                                            '</p>' .
                                            '<p class="log-rec"><b> lat_upcoming=</b> ' .
                                            $ed['lat_upcoming'] .
                                            '</p>' .
                                            '<p class="log-rec"><b> BA_Bsc_Title=</b> ' .
                                            $ed['BA_Bsc_Title'] .
                                            '</p>' .
                                            '<p class="log-rec"><b> BA_Bsc_From=</b> ' .
                                            $ed['BA_Bsc_From'] .
                                            '</p>' .
                                            '<p class="log-rec"><b> BA_Bsc_Subject=</b> ' .
                                            $ed['BA_Bsc_Subject'] .
                                            '</p>' .
                                            '<p class="log-rec"><b> BA_Bsc_Year=</b> ' .
                                            $ed['BA_Bsc_Year'] .
                                            '</p>' .
                                            '<p class="log-rec"><b> BA_Bsc_Specialization=</b> ' .
                                            $ed['BA_Bsc_Specialization'] .
                                            '</p>' .
                                            '<p class="log-rec"><b> BA_Bsc_Total=</b> ' .
                                            $ed['BA_Bsc_Total'] .
                                            '</p>' .
                                            '<p class="log-rec"><b> BA_Bsc=</b> ' .
                                            $ed['BA_Bsc'] .
                                            '</p>' .
                                            '<p class="log-rec"><b> resultwaitbsc=</b> ' .
                                            $ed['resultwaitbsc'] .
                                            '</p>' .
                                            '<p class="log-rec"><b> BA_Bsc_degree_type=</b> ' .
                                            $ed['BA_Bsc_degree_type'] .
                                            '</p>' .
                                            '<p class="log-rec"><b> MSc_From=</b> ' .
                                            $ed['MSc_From'] .
                                            '</p>' .
                                            '<p class="log-rec"><b> MSc_Subject=</b> ' .
                                            $ed['MSc_Subject'] .
                                            '</p>' .
                                            '<p class="log-rec"><b> MSc_Year=</b> ' .
                                            $ed['MSc_Year'] .
                                            '</p>' .
                                            '<p class="log-rec"><b> MSc_Specialization=</b> ' .
                                            $ed['MSc_Specialization'] .
                                            '</p>' .
                                            '<p class="log-rec"><b> MSc_Total=</b> ' .
                                            $ed['MSc_Total'] .
                                            '</p>' .
                                            '<p class="log-rec"><b> MSc=</b> ' .
                                            $ed['MSc'] .
                                            '</p>' .
                                            '<p class="log-rec"><b> MSc_degree_type=</b> ' .
                                            $ed['MSc_degree_type'] .
                                            '</p>' .
                                            '<p class="log-rec"><b> BS_Title=</b> ' .
                                            $ed['BS_Title'] .
                                            '</p>' .
                                            '<p class="log-rec"><b> BS_From=</b> ' .
                                            $ed['BS_From'] .
                                            '</p>' .
                                            '<p class="log-rec"><b> BS_Subject=</b> ' .
                                            $ed['BS_Subject'] .
                                            '</p>' .
                                            '<p class="log-rec"><b> BS_Year=</b> ' .
                                            $ed['BS_Year'] .
                                            '</p>' .
                                            '<p class="log-rec"><b> BS_Specialization=</b> ' .
                                            $ed['BS_Specialization'] .
                                            '</p>' .
                                            '<p class="log-rec"><b> BS_Total=</b> ' .
                                            $ed['BS_Total'] .
                                            '</p>' .
                                            '<p class="log-rec"><b> BS=</b> ' .
                                            $ed['BS'] .
                                            '</p>' .
                                            '<p class="log-rec"><b> BS_Exam_System=</b> ' .
                                            $ed['BS_Exam_System'] .
                                            '</p>' .
                                            '<p class="log-rec"><b> BS_degree_type=</b> ' .
                                            $ed['BS_degree_type'] .
                                            '</p>' .
                                            '<p class="log-rec"><b> MS_Title=</b> ' .
                                            $ed['MS_Title'] .
                                            '</p>' .
                                            '<p class="log-rec"><b> MS_From=</b> ' .
                                            $ed['MS_From'] .
                                            '</p>' .
                                            '<p class="log-rec"><b> MS_Subject=</b> ' .
                                            $ed['MS_Subject'] .
                                            '</p>' .
                                            '<p class="log-rec"><b> MS_Year=</b> ' .
                                            $ed['MS_Year'] .
                                            '</p>' .
                                            '<p class="log-rec"><b> MS_Specialization=</b> ' .
                                            $ed['MS_Specialization'] .
                                            '</p>' .
                                            '<p class="log-rec"><b> MS_Total=</b> ' .
                                            $ed['MS_Total'] .
                                            '</p>' .
                                            '<p class="log-rec"><b> MS=</b> ' .
                                            $ed['MS'] .
                                            '</p>' .
                                            '<p class="log-rec"><b> MS_Exam_System=</b> ' .
                                            $ed['MS_Exam_System'] .
                                            '</p>' .
                                            '<p class="log-rec"><b> MS_degree_type=</b> ' .
                                            $ed['MS_degree_type'] .
                                            '</p>' .
                                            '<p class="log-rec"><b> bs_last_result=</b> ' .
                                            $ed['bs_last_result'] .
                                            '</p>' .
                                            '<p class="log-rec"><b> ms_last_result=</b> ' .
                                            $ed['ms_last_result'] .
                                            '</p>' .
                                            '<p class="log-rec"><b> phd_research_proposal=</b> ' .
                                            $ed['phd_research_proposal'] .
                                            '</p>
                                    </div>';
                                        
                                        $ed = unserialize($pq->updated_value);
                                        echo '<div class="log-div-quali">
                                    <p class="log-rec"> <h4><b>Updated Data</b></h4> </p>
                                    <p class="log-rec"><b> SSC_Title=</b> ' .
                                            $ed['SSC_Title'] .
                                            '</p>' .
                                            '<p class="log-rec"><b> SSC_From=</b> ' .
                                            $ed['SSC_From'] .
                                            '</p>' .
                                            '<p class="log-rec"><b> SSC_Subject=</b> ' .
                                            $ed['SSC_Subject'] .
                                            '</p>' .
                                            '<p class="log-rec"><b> SSC_Year=</b> ' .
                                            $ed['SSC_Year'] .
                                            '</p>' .
                                            '<p class="log-rec"><b> SSC_Total=</b> ' .
                                            $ed['SSC_Total'] .
                                            '</p>' .
                                            '<p class="log-rec"><b> SSC_Composite=</b> ' .
                                            $ed['SSC_Composite'] .
                                            '</p>' .
                                            '<p class="log-rec"><b> SSC_degree_type=</b> ' .
                                            $ed['SSC_degree_type'] .
                                            '</p>' .
                                            '<p class="log-rec"><b> HSC_Title=</b> ' .
                                            $ed['HSC_Title'] .
                                            '</p>' .
                                            '<p class="log-rec"><b> HSC_From=</b> ' .
                                            $ed['HSC_From'] .
                                            '</p>' .
                                            '<p class="log-rec"><b> HSC_Subject=</b> ' .
                                            $ed['HSC_Subject'] .
                                            '</p>' .
                                            '<p class="log-rec"><b> HSC_Year=</b> ' .
                                            $ed['HSC_Year'] .
                                            '</p>' .
                                            '<p class="log-rec"><b> HSSC_Total=</b> ' .
                                            $ed['HSSC_Total'] .
                                            '</p>' .
                                            '<p class="log-rec"><b> HSSC_Composite=</b> ' .
                                            $ed['HSSC_Composite'] .
                                            '</p>' .
                                            '<p class="log-rec"><b> HSSC_Total1=</b> ' .
                                            $ed['HSSC_Total1'] .
                                            '</p>' .
                                            '<p class="log-rec"><b> HSSC1=</b> ' .
                                            $ed['HSSC1'] .
                                            '</p>' .
                                            '<p class="log-rec"><b> HSSC_Total2=</b> ' .
                                            $ed['HSSC_Total2'] .
                                            '</p>' .
                                            '<p class="log-rec"><b> HSSC2=</b> ' .
                                            $ed['HSSC2'] .
                                            '</p>' .
                                            '<p class="log-rec"><b> resultAwaiting=</b> ' .
                                            $ed['resultAwaiting'] .
                                            '</p>' .
                                            '<p class="log-rec"><b> HSC_degree_type=</b> ' .
                                            $ed['HSC_degree_type'] .
                                            '</p>' .
                                            '<p class="log-rec"><b> llb_total_marks=</b> ' .
                                            $ed['llb_total_marks'] .
                                            '</p>' .
                                            '<p class="log-rec"><b> llb_obtained_marks=</b> ' .
                                            $ed['llb_obtained_marks'] .
                                            '</p>' .
                                            '<p class="log-rec"><b> law_completion_date=</b> ' .
                                            $ed['law_completion_date'] .
                                            '</p>' .
                                            '<p class="log-rec"><b> lat_upcoming=</b> ' .
                                            $ed['lat_upcoming'] .
                                            '</p>' .
                                            '<p class="log-rec"><b> BA_Bsc_Title=</b> ' .
                                            $ed['BA_Bsc_Title'] .
                                            '</p>' .
                                            '<p class="log-rec"><b> BA_Bsc_From=</b> ' .
                                            $ed['BA_Bsc_From'] .
                                            '</p>' .
                                            '<p class="log-rec"><b> BA_Bsc_Subject=</b> ' .
                                            $ed['BA_Bsc_Subject'] .
                                            '</p>' .
                                            '<p class="log-rec"><b> BA_Bsc_Year=</b> ' .
                                            $ed['BA_Bsc_Year'] .
                                            '</p>' .
                                            '<p class="log-rec"><b> BA_Bsc_Specialization=</b> ' .
                                            $ed['BA_Bsc_Specialization'] .
                                            '</p>' .
                                            '<p class="log-rec"><b> BA_Bsc_Total=</b> ' .
                                            $ed['BA_Bsc_Total'] .
                                            '</p>' .
                                            '<p class="log-rec"><b> BA_Bsc=</b> ' .
                                            $ed['BA_Bsc'] .
                                            '</p>' .
                                            '<p class="log-rec"><b> resultwaitbsc=</b> ' .
                                            $ed['resultwaitbsc'] .
                                            '</p>' .
                                            '<p class="log-rec"><b> BA_Bsc_degree_type=</b> ' .
                                            $ed['BA_Bsc_degree_type'] .
                                            '</p>' .
                                            '<p class="log-rec"><b> MSc_From=</b> ' .
                                            $ed['MSc_From'] .
                                            '</p>' .
                                            '<p class="log-rec"><b> MSc_Subject=</b> ' .
                                            $ed['MSc_Subject'] .
                                            '</p>' .
                                            '<p class="log-rec"><b> MSc_Year=</b> ' .
                                            $ed['MSc_Year'] .
                                            '</p>' .
                                            '<p class="log-rec"><b> MSc_Specialization=</b> ' .
                                            $ed['MSc_Specialization'] .
                                            '</p>' .
                                            '<p class="log-rec"><b> MSc_Total=</b> ' .
                                            $ed['MSc_Total'] .
                                            '</p>' .
                                            '<p class="log-rec"><b> MSc=</b> ' .
                                            $ed['MSc'] .
                                            '</p>' .
                                            '<p class="log-rec"><b> MSc_degree_type=</b> ' .
                                            $ed['MSc_degree_type'] .
                                            '</p>' .
                                            '<p class="log-rec"><b> BS_Title=</b> ' .
                                            $ed['BS_Title'] .
                                            '</p>' .
                                            '<p class="log-rec"><b> BS_From=</b> ' .
                                            $ed['BS_From'] .
                                            '</p>' .
                                            '<p class="log-rec"><b> BS_Subject=</b> ' .
                                            $ed['BS_Subject'] .
                                            '</p>' .
                                            '<p class="log-rec"><b> BS_Year=</b> ' .
                                            $ed['BS_Year'] .
                                            '</p>' .
                                            '<p class="log-rec"><b> BS_Specialization=</b> ' .
                                            $ed['BS_Specialization'] .
                                            '</p>' .
                                            '<p class="log-rec"><b> BS_Total=</b> ' .
                                            $ed['BS_Total'] .
                                            '</p>' .
                                            '<p class="log-rec"><b> BS=</b> ' .
                                            $ed['BS'] .
                                            '</p>' .
                                            '<p class="log-rec"><b> BS_Exam_System=</b> ' .
                                            $ed['BS_Exam_System'] .
                                            '</p>' .
                                            '<p class="log-rec"><b> BS_degree_type=</b> ' .
                                            $ed['BS_degree_type'] .
                                            '</p>' .
                                            '<p class="log-rec"><b> MS_Title=</b> ' .
                                            $ed['MS_Title'] .
                                            '</p>' .
                                            '<p class="log-rec"><b> MS_From=</b> ' .
                                            $ed['MS_From'] .
                                            '</p>' .
                                            '<p class="log-rec"><b> MS_Subject=</b> ' .
                                            $ed['MS_Subject'] .
                                            '</p>' .
                                            '<p class="log-rec"><b> MS_Year=</b> ' .
                                            $ed['MS_Year'] .
                                            '</p>' .
                                            '<p class="log-rec"><b> MS_Specialization=</b> ' .
                                            $ed['MS_Specialization'] .
                                            '</p>' .
                                            '<p class="log-rec"><b> MS_Total=</b> ' .
                                            $ed['MS_Total'] .
                                            '</p>' .
                                            '<p class="log-rec"><b> MS=</b> ' .
                                            $ed['MS'] .
                                            '</p>' .
                                            '<p class="log-rec"><b> MS_Exam_System=</b> ' .
                                            $ed['MS_Exam_System'] .
                                            '</p>' .
                                            '<p class="log-rec"><b> MS_degree_type=</b> ' .
                                            $ed['MS_degree_type'] .
                                            '</p>' .
                                            '<p class="log-rec"><b> bs_last_result=</b> ' .
                                            $ed['bs_last_result'] .
                                            '</p>' .
                                            '<p class="log-rec"><b> ms_last_result=</b> ' .
                                            $ed['ms_last_result'] .
                                            '</p>' .
                                            '<p class="log-rec"><b> phd_research_proposal=</b> ' .
                                            $ed['phd_research_proposal'] .
                                            '</p>
                                    </div>';
                                    @endphp
                                </div>
                            </div>
                        @endforeach
                    @endif
                </div>
            </div>

            <div class="sbox">
                <div class="sbox-title">
                    <h1> Document Verify/Reject Log</h1>
                </div> <!-- Ending sbox-title -->
                <div class="sbox-content ">
                    @if ($documentsLog)
                        @foreach ($documentsLog as $dl)
                            <?php
                            $userInfo1 = \DB::select("SELECT * from `tb_users`  WHERE id = $dl->modified_by");
                            
                            if ($userInfo1) {
                                $userData1 = $userInfo1[0]->first_name . ' ' . $userInfo1[0]->last_name;
                            } else {
                                $userData1 = '';
                            }
                            
                            ?>
                            <div class="row">
                                <div class="col-md-12">
                                    @php
                                        echo '<div class="algn-cntr"><p><b>created_at=</b> ' . $dl->created_at . '</p>';
                                        echo '<p><b>modified_by=</b> ' . $userData1 . '</p></div>';
                                        $ed = unserialize($dl->existing_value);
                                        echo '<div class="log-div-quali">
                                    <p class="log-rec"> <h4><b>Existing Data</b></h4> </p>
                                    <p class="log-rec"><b> fkCurrentStatus=</b> ' .
                                            $ed['fkCurrentStatus'] .
                                            '</p>' .
                                            '<p class="log-rec"><b> doumentsVerified=</b> ' .
                                            $ed['doumentsVerified'] .
                                            '</p>' .
                                            '<p class="log-rec"><b> comments=</b> ' .
                                            $ed['comments'] .
                                            '</p>' .
                                            '<p class="log-rec"><b> fkReviewedBy=</b> ' .
                                            $ed['fkReviewedBy'] .
                                            '</p>
                                    </div>';
                                        
                                        $ed = unserialize($dl->updated_value);
                                        echo '<div class="log-div-quali">
                                    <p class="log-rec"> <h4><b>Updated Data</b></h4> </p>
                                    <p class="log-rec"><b> fkCurrentStatus=</b> ' .
                                            $ed['fkCurrentStatus'] .
                                            '</p>' .
                                            '<p class="log-rec"><b> doumentsVerified=</b> ' .
                                            $ed['doumentsVerified'] .
                                            '</p>' .
                                            '<p class="log-rec"><b> comments=</b> ' .
                                            $ed['comments'] .
                                            '</p>' .
                                            '<p class="log-rec"><b> fkReviewedBy=</b> ' .
                                            $ed['fkReviewedBy'] .
                                            '</p>
                                    </div>';
                                    @endphp
                                </div>
                            </div>
                        @endforeach
                    @endif
                </div>
            </div>
        </div>
    </div>
@stop
