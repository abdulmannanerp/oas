<!DOCTYPE html>

<html lang="en-US">	
    <head>
        <meta charset="utf-8">	
    </head>	
    <body>
        Dear {{$name}}, <br /><br />

       You application for <b>{{$program_name ?? ''}}</b> has been rejected due to following reason. <br />
       <b>{{$comment ?? ''}}</b> <br />In case of any query, Please contact admission section. <br /><br />Regards, <br />Admission Office <br />International Islamic University, Islamabad (IIUI)<br /><a href="http://admission.iiu.edu.pk">http://admission.iiu.edu.pk/</a><br />
    
    </body>
</html>