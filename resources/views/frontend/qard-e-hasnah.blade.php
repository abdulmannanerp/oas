@section('title')
Apply for Qardh-e-Hasnah
@endsection
@include('frontend.schheader')

@php
if($applicationStatus->step_save == 1 && $hash ==''){
  $readonlyUser = "readonly='readonly'";
  $disabledUser = "disabled";
  $disabledUser1 = 'disabled="disabled"';
}else{
  $readonlyUser='';   
  $disabledUser = '';
  $disabledUser1 = '';
}
@endphp

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          <i class="fa fa-user text-yellow"></i> IT Alumni Qardh-e-Hasnah Program / Study Loan scheme
Application Form
          <small>Field mark with (*) are mandatory</small>
        </h1>
      </section>

      @if(session()->has('message'))
            <div class="alert alert-danger">
                {{ session()->get('message') }}
            </div>
        @endif

      @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    @if ( $status )
        <div class="alert alert-success" role="alert"> {{ $status }}</div>
    @endif  
    
      <!-- Main content -->
      <section class="content">

      <!-- <div class="alert alert-success" role="alert">
           <h4 style="text-align:center">SECTION A: PERSONAL INFORMATION</h4>
      </div> -->




      <form  class="scholarshipDetailForm" method="POST" enctype="multipart/form-data">
          <input type="hidden" name="userId" value="{{$userdetail->userId}}">
          <input type="hidden" name="server_date" value="{{date('Y-m-d h:i:s')}}">
          {{csrf_field()}}

        <!-- SELECT2 EXAMPLE -->
        <div class="box box-info">

          <!--<div class="box-header with-border">
            <h3 class="box-title">Enter Personal Information</h3>
          </div>-->


          <div class="box-header with-border box box-info">
            <div class="alert alert-success" role="alert">
               <h4 style="text-align:center">SECTION A: PERSONAL INFORMATION</h4>
            </div>

            <h3 class="box-title">A.1 Information about the applicant: Student</h3>
          </div>

          <!-- /.box-header -->
          <div class="box-body">
            <div class="row">



              <div class="col-md-4">
                <!-- /.form-group -->
                <div class="form-group">
                  <label>Applicant Name</label>
                  <input type="text" name="full_name" id="full_name" class="form-control" value="{{$userdetail->name}}" required {{ $readonlyUser }} readonly>
                </div>
              </div>

              <div class="col-md-4">
                 <div class="form-group">
                  <label>CNIC/B-Form/Passport No.</label><br />
                  <input type="text" class="form-control" id="cnic" name="cnic" value="{{$userdetail->cnic}}" required readonly>
                </div>
              </div>


              <div class="col-md-4">
                <div class="form-group">
                  <label>Registration Number</label>
                  <input type="text" name="reg_number" id="reg_number" 
                    value="{{$scholarshipinformation->reg ?? old('reg_number')}}" class="form-control">
                </div>
              </div>

              <div class="col-md-4">
                <div class="form-group">
                  <label>Degree</label>
                  <input type="text" name="std_degree" id="std_degree" class="form-control" required>
                </div>
              </div>

              <div class="col-md-4">

                <div class="form-group">
                    <label class="contact-label"> Student Mobile No.(301252xxx)</label>
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-phone"></i>
                      </div>
                      @php
                      $carrier = '';
                      $phone = '';
                      if($applicantDetail->mobile){
                        $carrier = substr($applicantDetail->mobile, 2, 3);
                        $phone = substr($applicantDetail->mobile, 5, 7);
                      }
                      @endphp
                      <select name="carrier" id="carrier" class="form-control {{ ($userdetail->fkNationality == 1) ? 'ext-show' : 'ext-hide-2'}}" {{ ($userdetail->fkNationality == 1) ? 'required' : ''}} style="width: 30%;" {{$disabledUser1}}>
                        <option value="">Code</option>
                        <option value="300" {{ ($carrier == '300' || old('carrier') == '300') ? 'selected' : ''}}>0300</option>
                        <option value="301" {{ ($carrier == '301' || old('carrier') == '301') ? 'selected' : ''}}>0301</option>
                        <option value="302" {{ ($carrier == '302' || old('carrier') == '302') ? 'selected' : ''}}>0302</option>
                        <option value="303" {{ ($carrier == '303' || old('carrier') == '303') ? 'selected' : ''}}>0303</option>
                        <option value="304" {{ ($carrier == '304' || old('carrier') == '304') ? 'selected' : ''}}>0304</option>
                        <option value="305" {{ ($carrier == '305' || old('carrier') == '305') ? 'selected' : ''}}>0305</option>
                        <option value="306" {{ ($carrier == '306' || old('carrier') == '306') ? 'selected' : ''}}>0306</option>
                        <option value="307" {{ ($carrier == '307' || old('carrier') == '307') ? 'selected' : ''}}>0307</option>
                        <option value="308" {{ ($carrier == '308' || old('carrier') == '308') ? 'selected' : ''}}>0308</option>
                        <option value="309" {{ ($carrier == '309' || old('carrier') == '309') ? 'selected' : ''}}>0309</option>
                        <option value="310" {{ ($carrier == '310' || old('carrier') == '310') ? 'selected' : ''}}>0310</option>
                        <option value="311" {{ ($carrier == '311' || old('carrier') == '311') ? 'selected' : ''}}>0311</option>
                        <option value="312" {{ ($carrier == '312' || old('carrier') == '312') ? 'selected' : ''}}>0312</option>
                        <option value="313" {{ ($carrier == '313' || old('carrier') == '313') ? 'selected' : ''}}>0313</option>
                        <option value="314" {{ ($carrier == '314' || old('carrier') == '314') ? 'selected' : ''}}>0314</option>
                        <option value="315" {{ ($carrier == '315' || old('carrier') == '315') ? 'selected' : ''}}>0315</option>
                        <option value="316" {{ ($carrier == '316' || old('carrier') == '316') ? 'selected' : ''}}>0316</option>
                        <option value="317" {{ ($carrier == '317' || old('carrier') == '317') ? 'selected' : ''}}>0317</option>
                        <option value="318" {{ ($carrier == '318' || old('carrier') == '318') ? 'selected' : ''}}>0318</option>
                        <option value="320" {{ ($carrier == '320' || old('carrier') == '320') ? 'selected' : ''}}>0320</option>
                        <option value="321" {{ ($carrier == '321' || old('carrier') == '321') ? 'selected' : ''}}>0321</option>
                        <option value="322" {{ ($carrier == '322' || old('carrier') == '322') ? 'selected' : ''}}>0322</option>
                        <option value="323" {{ ($carrier == '323' || old('carrier') == '323') ? 'selected' : ''}}>0323</option>
                        <option value="324" {{ ($carrier == '324' || old('carrier') == '324') ? 'selected' : ''}}>0324</option>
                        <option value="330" {{ ($carrier == '330' || old('carrier') == '330') ? 'selected' : ''}}>0330</option>
                        <option value="331" {{ ($carrier == '331' || old('carrier') == '331') ? 'selected' : ''}}>0331</option>
                        <option value="332" {{ ($carrier == '332' || old('carrier') == '332') ? 'selected' : ''}}>0332</option>
                        <option value="333" {{ ($carrier == '333' || old('carrier') == '333') ? 'selected' : ''}}>0333</option>
                        <option value="334" {{ ($carrier == '334' || old('carrier') == '334') ? 'selected' : ''}}>0334</option>
                        <option value="335" {{ ($carrier == '335' || old('carrier') == '335') ? 'selected' : ''}}>0335</option>
                        <option value="336" {{ ($carrier == '336' || old('carrier') == '336') ? 'selected' : ''}}>0336</option>
                        <option value="337" {{ ($carrier == '337' || old('carrier') == '337') ? 'selected' : ''}}>0337</option>
                        <option value="340" {{ ($carrier == '340' || old('carrier') == '340') ? 'selected' : ''}}>0340</option>
                        <option value="341" {{ ($carrier == '341' || old('carrier') == '341') ? 'selected' : ''}}>0341</option>                        
                        <option value="342" {{ ($carrier == '342' || old('carrier') == '342') ? 'selected' : ''}}>0342</option>
                        <option value="343" {{ ($carrier == '343' || old('carrier') == '343') ? 'selected' : ''}}>0343</option>
                        <option value="344" {{ ($carrier == '344' || old('carrier') == '344') ? 'selected' : ''}}>0344</option>
                        <option value="345" {{ ($carrier == '345' || old('carrier') == '345') ? 'selected' : ''}}>0345</option>
                        <option value="346" {{ ($carrier == '346' || old('carrier') == '346') ? 'selected' : ''}}>0346</option>
                        <option value="347" {{ ($carrier == '347' || old('carrier') == '347') ? 'selected' : ''}}>0347</option>
                        <option value="348" {{ ($carrier == '348' || old('carrier') == '348') ? 'selected' : ''}}>0348</option>
                        <option value="349" {{ ($carrier == '349' || old('carrier') == '349') ? 'selected' : ''}}>0349</option>
                      </select>

                      <input type="text" class="form-control {{ ($userdetail->fkNationality == 1) ? 'ext-show' : 'ext-hide-2'}}" style="width: 70%;" onkeypress="return isNumberKey(event)" id="phone" name="phone" minlength="7" maxlength="7" value="{{$phone ?: old('phone')}}" {{ ($userdetail->fkNationality == 1) ? 'required' : ''}} {{ $readonlyUser }}>
                      <input type="number" class="form-control {{ ($userdetail->fkNationality == 2 || $userdetail->fkNationality == 3) ? 'ext-show' : 'ext-hide-2'}}" {{ ($userdetail->fkNationality == 2 || $userdetail->fkNationality == 3) ? 'required' : ''}} id="overseas_phone"  value="{{$applicantDetail->mobile ?: old('overseas_phone')}}" name="overseas_phone" {{ $readonlyUser }}>
                    </div>
                  </div> 
              </div>

              <div class="col-md-4">
                <div class="form-group">
                  <label>Email</label>
                  <input type="email" name="std_email" id="std_email" class="form-control" required>
                </div>
              </div>


               <div class="col-md-12">
                <div class="form-group">
                  <label >Postal Address</label>
                  <textarea class="form-control" value="{{$applicantAddress->addressPmt ?? old('permanent_address')}}" name="present_address" id="present_address" rows="1" required {{ $disabledUser }} readonly>{{$applicantAddress->addressPmt ?? old('permanent_address')}}</textarea>
                </div> 
              </div>


              <!-- /.col -->
            </div>
          </div>



            <div class="box-header with-border box box-info">
              <h3 class="box-title">A.2 Information about Father / Guardian</h3>
            </div>

          <!-- /.box-header -->
          <div class="box-body">
            <div class="row">

              <div class="col-md-4">
                <!-- /.form-group -->
                <div class="form-group">
                 <label>Father Name / Guardian Name</label>
                  <input type="text" name="father_name" id="father_name" class="form-control" value="{{$applicantDetail->fatherName ?? old('father_name')}}" required {{ $readonlyUser }} readonly>
                </div>              
                <!-- /.form-group -->
              </div>

              <div class="col-md-4">
                 <div class="form-group">
                  <label>Father CNIC</label>
                    <input type="text" name="father_cnic" id="father_cnic" class="form-control" value="{{$applicantDetail->father_cnic ?? old('father_cnic')}}" required {{ $readonlyUser }} readonly>
                  </div>
              </div>


              <div class="col-md-4">
                 <div class="form-group">
                  <label>Father Mobile No. </label>
                    <input type="text" name="father_mobile" id="father_mobile" class="form-control" value="{{$applicantDetail->fatherMobile ?? old('fatherMobile')}}" required {{ $readonlyUser }} readonly>
                </div>
              </div>

              <div class="col-md-4">
                 <div class="form-group">
                  <label>Father Telephone No. </label>
                    <input type="text" name="father_tel" id="father_tel" class="form-control" placeholder="Enter Telephone Number">
                </div>
              </div>

               <div class="col-md-4">
                <div class="form-group">
                  <label>Email</label>
                  <input type="email" name="father_email" id="father_email" class="form-control" required>
                </div>
              </div>

              <div class="col-md-4">
                <div class="form-group">
                  <label>Profession</label>
                  <select class='form-control select2' id="father_profession" name="father_profession" required>
                      <option>Select Profession</option>
                      <option value="Electrician">Electrician</option>
                      <option value="Architect">Architect</option>
                      <option value="Dentist">Dentist</option>
                      <option value="Accountant">Accountant</option>
                      <option value="Lawyer">Lawyer</option>
                      <option value="Physician">Physician</option>
                      <option value="Chef">Chef</option>
                      <option value="Engineer">Engineer</option>
                      <option value="Designer">Designer</option>
                      <option value="Butcher">Butcher</option>
                      <option value="Software Developer">Software Developer</option>
                      <option value="Businessperson">Businessperson</option>
                      <option value="Teacher">Teacher</option>
                      <option value="Artist">Artist</option>
                      <option value="Scientist">Scientist</option>
                      <option value="Actor">Actor</option>
                      <option value="Bartender">Bartender</option>
                      <option value="Dental hygienist">Dental hygienist</option>
                      <option value="Firefighter">Firefighter</option>
                      <option value="Economist">Economist</option>
                      <option value="Dietitian">Dietitian</option>
                      <option value="Barber">Barber</option>
                      <option value="Journalist">Journalist</option>
                      <option value="Librarian">Librarian</option>
                      <option value="Secretary">Secretary</option>
                      <option value="Mail carrier">Mail carrier</option>
                      <option value="Photographer">Photographer</option>
                      <option value="Bookkeeper">Bookkeeper</option>
                      <option value="Cashier">Cashier</option>
                      <option value="Air traffic controller">Air traffic controller</option>
                      <option value="Soldier">Soldier</option>
                      <option value="Baker">Baker</option>
                      <option value="Waiting staff">Waiting staff</option>
                      <option value="Bus driver">Bus driver</option>
                      <option value="Veterinarian">Veterinarian</option>
                      <option value="Police officer">Police officer</option>
                      <option value="Plumber">Plumber</option>
                      <option value="Pharmacist">Pharmacist</option>
                      <option value="Hairdresser">Hairdresser</option>
                      <option value="Tailor">Tailor</option>
                      <option value="Farmer">Farmer</option>
                      <option value="Painter and decorator">Painter and decorator</option>
                      <option value="Florist">Florist</option>
                      <option value="Bricklayer">Bricklayer</option>
                      <option value="driver">driver</option>
                      <option value="Gardener">Gardener</option>
                      <option value="cleaner">cleaner</option>
                      <option value="Fisherman">Fisherman</option>
                      <option value="Lifeguard">Lifeguard</option>
                      <option value="Estate agent">Estate agent</option>
                      <option value="Judge">Judge</option>
                  </select>
                </div>
              </div>

              <div class="col-md-4">
                <div class="form-group">
                  <label>Employement Status</label>
                  <select class='form-control select2' id="father_emp_status" name="father_emp_status">
                      <option>Select Status</option>
                      <option value="Working">Working</option>
                      <option value="Retired">Retired</option>
                  </select>
                </div>
              </div>

              <div class="col-md-8">
                <div class="form-group">
                  <label>Postal Address</label>
                  <input type="text" name="father_postal_address" id="father_postal_address" class="form-control" required>
                </div>
              </div>
            </div>
          </div>


          <div class="box-header with-border box box-info">
            <h3 class="box-title">FATHER EMPLOYMENT HISTORY: Latest employment first. List the last 4. If currently retired, please include pension</h3>
          </div>

          <!-- /.box-header -->
          <div class="box-body">
            <div class="row">
              <div class="col-lg-12" style="overflow-x:auto;">
                <button class="btn btn-md btn-primary" id="addBtn" type="button">
                    Add New Row
                </button>
              <div class="table-responsive">
                  <!--Table-->
                  <table class="table table-striped">

                    <!--Table head-->
                    <thead>
                      <tr>
                        <th>From</th>
                        <th>To</th>
                        <th>Designation</th>
                        <th>Organization</th>
                        <th>Monthly Salary</th>
                        <th>Row Data</th>
                      </tr>
                    </thead>
                    <!--Table head-->

                    <!--Table body-->
                    <tbody id="tbody">
                      
                    </tbody>
                    <!--Table body-->
                  </table>
                  <!--Table-->
                </div>
              </div>

              
            </div>
          </div>


          <!--Information About Mother-->

          <div class="box-header with-border box box-info">
            <h3 class="box-title">A.3 Information about Mother (Complete all that applies)</h3>
          </div>

          <!-- /.box-header -->
          <div class="box-body">
            <div class="row">

              <div class="col-md-4">
                <!-- /.form-group -->
                <div class="form-group">
                 <label>Mother Name</label>
                  <input type="text" name="mother_name" id="mother_name" class="form-control" placeholder="Enter Mother Name" required >
                </div>              
                <!-- /.form-group -->
              </div>

              <div class="col-md-4">
                 <div class="form-group">
                  <label>Mother CNIC</label>
                    <input type="text" name="mother_cnic" id="mother_cnic" class="form-control" placeholder="Enter CNIC Number" required >
                  </div>
              </div>


              <div class="col-md-4">
                 <div class="form-group">
                  <label>Mother Mobile No. </label>
                    <input type="text" name="mother_mobile" id="mother_mobile" class="form-control" placeholder="Enter Mobile Number" required >
                </div>
              </div>

              <div class="col-md-4">
                 <div class="form-group">
                  <label>Mother Telephone No. </label>
                    <input type="text" name="mother_tel" id="mother_tel" class="form-control" placeholder="Enter Telephone Number" >
                </div>
              </div>

               <div class="col-md-4">
                <div class="form-group">
                  <label>Email</label>
                  <input type="email" name="mother_email" id="mother_email" class="form-control" placeholder="Enter Mother Email" required >
                </div>
              </div>

              <div class="col-md-4">
                <div class="form-group">
                  <label>Profession</label>
                  <select class='form-control select2' id="mother_profession" name="mother_profession">
                      <option value="Service">Service</option>
                      <option value="Business">Business</option>
                      <option value="Hous Wife">Hous Wife</option>
                  </select>
                </div>
              </div>

              <div class="col-md-4">
                <div class="form-group">
                  <label>Employement Status</label>
                  <select class='form-control select2' id="mother_emp_status" name="mother_emp_status">
                      <option value="Working">Working</option>
                      <option value="Retired">Retired</option>
                      <option value="House Wife">House Wife</option>
                  </select>
                </div>
              </div>

              <div class="col-md-8">
                <div class="form-group">
                  <label>Postal Address</label>
                  <input type="text" name="father_postal_address" id="father_postal_address" class="form-control">
                </div>
              </div>
            </div>
          </div>


          <div class="box-header with-border box box-info">
            <h3 class="box-title">MOTHER EMPLOYMENT HISTORY: Latest employment first. List the last 4. If currently retired, please include pension</h3>
          </div>

          <!-- /.box-header -->
          <div class="box-body">
            <div class="row">
              <div class="col-lg-12" style="overflow-x:auto;">
                <button class="btn btn-md btn-primary" id="addBtn_mother" type="button">
                    Add New Row
                </button>
              <div class="table-responsive">
                  <!--Table-->
                  <table class="table table-striped">

                    <!--Table head-->
                    <thead>
                      <tr>
                        <th>From</th>
                        <th>To</th>
                        <th>Designation</th>
                        <th>Organization</th>
                        <th>Monthly Salary</th>
                        <th>Row Data</th>
                      </tr>
                    </thead>
                    <!--Table head-->

                    <!--Table body-->
                    <tbody id="tbody_mother">
                      
                    </tbody>
                    <!--Table body-->
                  </table>
                  <!--Table-->
                </div>
              </div>
            </div>
          </div>



          <div class="box-header with-border box box-info">
            <h3 class="box-title">A.4 Information about brothers/Sisters in school/not working</h3>
          </div>

          <!-- /.box-header -->
          <div class="box-body">
             <div class="row">

              <div class="col-lg-12" style="overflow-x:auto;">
                <button class="btn btn-md btn-primary" id="addBtn_siblings" type="button">
                    Add New Row
                </button>
              <div class="table-responsive">
                  <!--Table-->
                  <table class="table table-striped">

                    <!--Table head-->
                    <thead>
                      <tr>
                        <th>Name</th>
                        <th>Date of Birth</th>
                        <th>Relation</th>
                        <th>Educational Institute</th>
                        <th>Anual Tuition fee</th>
                        <th>Granting Agency</th>
                        <th>Row Data</th>
                      </tr>
                    </thead>
                    <!--Table head-->

                    <!--Table body-->
                    <tbody id="tbody_siblings">
                      
                    </tbody>
                    <!--Table body-->
                  </table>
                  <!--Table-->
                </div>
              </div>
            </div>
          </div>



          <div class="box-header with-border box box-info">
            <h3 class="box-title">A.5 Information about brothers/Sisters employed</h3>
          </div>

          <!-- /.box-header -->
          <div class="box-body">
             <div class="row">

              <div class="col-lg-12" style="overflow-x:auto;">
                <button class="btn btn-md btn-primary" id="addBtn_siblings_emp_history" type="button">
                    Add New Row
                </button>
              <div class="table-responsive">
                  <!--Table-->
                  <table class="table table-striped">

                    <!--Table head-->
                    <thead>
                      <tr>
                        <th>Name</th> 
                        <th>Profession</th>
                        <th>Designation</th>
                        <th>Company</th>
                        <th>Monthly Income</th>
                        <th>Row Data</th>
                      </tr>
                    </thead>
                    <!--Table head-->

                    <!--Table body-->
                    <tbody id="tbody_siblings_emp_history">
                      
                    </tbody>
                    <!--Table body-->
                  </table>
                  <!--Table-->
                </div>
              </div>
            </div>
          </div>


          <div class="box-header with-border box box-info">
            <div class="alert alert-success" role="alert">
               <h4 style="text-align:center">SECTION B: CURRENTLY MONTHLY FAMILY INCOME</h4>
            </div>

            <h3 class="box-title">B.1 Family Income from Salary/Pension Income</h3>
          </div>

          <!-- /.box-header -->
          <div class="box-body">
             <div class="row">

              <div class="col-lg-12" style="overflow-x:auto;">
              <div class="table-responsive">
                  <!--Table-->
                  <table class="table table-striped">

                    <thead class="thead-dark">
                      <tr>
                        <th scope="col">Relation</th>
                        <th scope="col">Average Monthly Income</th>
                        <th scope="col">Employers</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <th scope="row">Father/Guardian</th>
                        <td>
                          <div class="form-group">
                            <input type="text" name="father_avg_mnthly_income" id="father_avg_mnthly_income" class="form-control" required >
                          </div>
                        </td>
                        <td>
                          <div class="form-group">
                            <input type="text" name="father_employers" id="father_employers" class="form-control" required >
                          </div>
                        </td>
                      </tr>
                      <tr>
                        <th scope="row">Mother</th>
                        <td>
                          <div class="form-group">
                            <input type="text" name="mother_avg_mnthly_income" id="mother_avg_mnthly_income" class="form-control" required >
                          </div>
                        </td>
                        <td>
                          <div class="form-group">
                            <input type="text" name="mother_employers" id="mother_employers" class="form-control" required >
                          </div>
                        </td>
                      </tr>
                      <tr>
                        <th scope="row">Brother/Sisters</th>
                        <td>
                          <div class="form-group">
                            <input type="text" name="siblings_avg_mnthly_income" id="siblings_avg_mnthly_income" class="form-control" required >
                          </div>
                        </td>
                        <td>
                          <div class="form-group">
                            <input type="text" name="siblings_employers" id="siblings_employers" class="form-control" required >
                          </div>
                        </td>
                      </tr>
                    </tbody>
                    <!--Table body-->
                  </table>
                  <!--Table-->
                </div>
              </div>
            </div>
          </div>



          <div class="box-header with-border box box-info">
            <h3 class="box-title">B.2 Family Income from Business including agriculture income</h3>
          </div>

          <!-- /.box-header -->
          <div class="box-body">
             <div class="row">
                <div class="col-lg-12" style="overflow-x:auto;">
                  <div class="table-responsive">
                    <!-- /.form-group -->
                    <table class="table table-striped">
                        <thead class="thead-dark">
                          <tr>
                            <th scope="col">Relation</th>
                            <th scope="col">Average Monthly Income</th>
                            <th scope="col">Name and type of Business</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <th scope="row">Father/Guardian</th>
                            <td>
                              <div class="form-group">
                                <input type="text" name="father_avg_mnthly_income" id="father_avg_mnthly_income" class="form-control" required >
                              </div>
                            </td>
                            <td>
                              <div class="form-group">
                                <input type="text" name="father_employers" id="father_employers" class="form-control" required >
                              </div>
                            </td>
                          </tr>
                          <tr>
                            <th scope="row">Mother</th>
                            <td>
                              <div class="form-group">
                                <input type="text" name="mother_avg_mnthly_income" id="mother_avg_mnthly_income" class="form-control" required >
                              </div>
                            </td>
                            <td>
                              <div class="form-group">
                                <input type="text" name="mother_employers" id="mother_employers" class="form-control" required >
                              </div>
                            </td>
                          </tr>
                          <tr>
                            <th scope="row">Brother/Sisters</th>
                            <td>
                              <div class="form-group">
                                <input type="text" name="siblings_avg_mnthly_income" id="siblings_avg_mnthly_income" class="form-control" required >
                              </div>
                            </td>
                            <td>
                              <div class="form-group">
                                <input type="text" name="siblings_employers" id="siblings_employers" class="form-control" required >
                              </div>
                            </td>
                          </tr>
                        </tbody>
                      </table>             
                    <!-- /.form-group -->
                 </div>
              </div>
            </div>
          </div>


          <div class="box-header with-border box box-info">
            <h3 class="box-title">B.3 Family Income from investment (Dividends, Intrest on shares,bond, fixed deposits etc)</h3>
          </div>

          <!-- /.box-header -->
          <div class="box-body">
             <div class="row">
              <div class="col-lg-12" style="overflow-x:auto;">
                 <div class="table-responsive">
                <!-- /.form-group -->
                      <table class="table table-striped">
                        <thead class="thead-dark">
                          <tr>
                            <th scope="col">Relation</th>
                            <th scope="col">Average Monthly Income</th>
                            <th scope="col">Name of Investment </th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <th scope="row">Father/Guardian</th>
                            <td>
                              <div class="form-group">
                                <input type="text" name="father_avg_mnthly_income" id="father_avg_mnthly_income" class="form-control" required >
                              </div>
                            </td>
                            <td>
                              <div class="form-group">
                                <input type="text" name="father_employers" id="father_employers" class="form-control" required >
                              </div>
                            </td>
                          </tr>
                          <tr>
                            <th scope="row">Mother</th>
                            <td>
                              <div class="form-group">
                                <input type="text" name="mother_avg_mnthly_income" id="mother_avg_mnthly_income" class="form-control" required >
                              </div>
                            </td>
                            <td>
                              <div class="form-group">
                                <input type="text" name="mother_employers" id="mother_employers" class="form-control" required >
                              </div>
                            </td>
                          </tr>
                          <tr>
                            <th scope="row">Brother/Sisters</th>
                            <td>
                              <div class="form-group">
                                <input type="text" name="siblings_avg_mnthly_income" id="siblings_avg_mnthly_income" class="form-control" required >
                              </div>
                            </td>
                            <td>
                              <div class="form-group">
                                <input type="text" name="siblings_employers" id="siblings_employers" class="form-control" required >
                              </div>
                            </td>
                          </tr>
                        </tbody>
                      </table>             
                    <!-- /.form-group -->
                </div>
              </div>
            </div>
          </div>



          <div class="box-header with-border box box-info">
            <h3 class="box-title">B.4 Family Income from rental/other income (Include income from all sources not listed above)</h3>
          </div>

          <!-- /.box-header -->
          <div class="box-body">
             <div class="row">



              <div class="col-md-12">
                <div class="table-responsive">
                <!-- /.form-group -->
                <table class="table table-striped">
                    <thead class="thead-dark">
                      <tr>
                        <th scope="col">Relation</th>
                        <th scope="col">Average Monthly Income</th>
                        <th scope="col">Source e.g. Rental Income/Other</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <th scope="row">Father/Guardian</th>
                        <td>
                          <div class="form-group">
                            <input type="text" name="father_avg_mnthly_income" id="father_avg_mnthly_income" class="form-control" required >
                          </div>
                        </td>
                        <td>
                          <div class="form-group">
                            <input type="text" name="father_source_income" id="father_source_income" class="form-control" required >
                          </div>
                        </td>
                      </tr>
                      <tr>
                        <th scope="row">Mother</th>
                        <td>
                          <div class="form-group">
                            <input type="text" name="mother_avg_mnthly_income" id="mother_avg_mnthly_income" class="form-control" required >
                          </div>
                        </td>
                        <td>
                          <div class="form-group">
                            <input type="text" name="mother_source_income" id="mother_source_income" class="form-control" required >
                          </div>
                        </td>
                      </tr>
                      <tr>
                        <th scope="row">Brother/Sisters</th>
                        <td>
                          <div class="form-group">
                            <input type="text" name="siblings_avg_mnthly_income" id="siblings_avg_mnthly_income" class="form-control" required >
                          </div>
                        </td>
                        <td>
                          <div class="form-group">
                            <input type="text" name="siblings_source_income" id="siblings_source_income" class="form-control" required >
                          </div>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                  </div>             
                <!-- /.form-group -->
              </div>
              
            </div>
          </div>


          <div class="box-header with-border box box-info">
            <h3 class="box-title">Total Monthly Family Income (Pak Rs.) -For Section B</h3>
          </div>

          <!-- /.box-header -->
          <div class="box-body">
             <div class="row">
              <div class="col-md-12">
                <div class="table-responsive">
                <!-- /.form-group -->
                <table class="table table-striped">
                    <tbody>
                      <tr>
                        <th scope="row">Total Monthly Family Income</th>
                        <td>
                          <div class="form-group">
                            <input type="number" name="secB_total_income" id="secB_total_income" class="form-control" readonly required >
                          </div>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                  </div>             
                <!-- /.form-group -->
              </div>
              
            </div>
          </div>


          <div class="box-header with-border box box-info">
            <h3 class="box-title">B.5 Any other Supporting Person (Family Relative)</h3>
          </div>

          <!-- /.box-header -->
          <div class="box-body">
             <div class="row">



              <div class="col-md-12">
                <!-- /.form-group -->
               <div class="table-responsive">
                <!-- /.form-group -->
                <table class="table table-striped">
                    <thead class="thead-dark">
                      <tr>
                        <th scope="col">#</th>
                        <th scope="col">#</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <th scope="row" style="width:30%">Name</th>
                        <td>
                          <div class="form-group">
                            <input type="text" name="father_avg_mnthly_income" id="father_avg_mnthly_income" class="form-control" required >
                          </div>
                        </td>
                      </tr>
                      <tr>
                        <th scope="row">Relation</th>
                        <td>
                          <div class="form-group">
                            <input type="text" name="mother_avg_mnthly_income" id="mother_avg_mnthly_income" class="form-control" required >
                          </div>
                        </td>
                      </tr>
                      <tr>
                        <th scope="row">Occupation and Designation</th>
                        <td>
                          <div class="form-group">
                            <input type="text" name="siblings_avg_mnthly_income" id="siblings_avg_mnthly_income" class="form-control" required >
                          </div>
                        </td>
                      </tr>
                      <tr>
                        <th scope="row">Monthly Financial Support Available By Family relative in Pak Rs.</th>
                        <td>
                          <div class="form-group">
                            <input type="text" name="siblings_avg_mnthly_income" id="siblings_avg_mnthly_income" class="form-control" required >
                          </div>
                        </td>
                      </tr>
                    </tbody>
                  </table>             
                <!-- /.form-group -->
                </div>
              </div>
            </div>
          </div>



          <div class="box-header with-border box box-info">
            <div class="alert alert-success" role="alert">
               <h4 style="text-align:center">SECTION C: WEALTH ASSESMENT</h4>
               <p style="text-align:center">List all assets and property owned by the family, classified by source, Include all owned by Father/Guardian, Mother and Brother and Sister</p>
            </div>

            <h3 class="box-title">C.1 List residential, commercial and agricultural properties owned by the family </h3>
          </div>

          <!-- /.box-header -->
          <div class="box-body">
             <div class="row">

              <div class="col-lg-12" style="overflow-x:auto;">
                <button class="btn btn-md btn-primary" id="addBtn_wealth_history" type="button">
                    Add New Row
                </button>
              <div class="table-responsive">
                  <!--Table-->
                  <table class="table table-striped">

                    <!--Table head-->
                    <thead>
                      <tr>
                        <th>Address</th> 
                        <th>Property Type</th>
                        <th>Area (Square Feet)</th>
                        <th>Approximate market Vaue</th>
                        <th>Status</th>
                      </tr>
                    </thead>
                    <!--Table head-->

                    <!--Table body-->
                    <tbody id="tbody_wealth_history">
                      
                    </tbody>
                    <!--Table body-->
                  </table>
                  <!--Table-->
                </div>
              </div>
            </div>
          </div>


          <div class="box-header with-border box box-info">
            <h3 class="box-title">C.2 List motor vehicles owned by the family </h3>
          </div>

          <!-- /.box-header -->
          <div class="box-body">
             <div class="row">

              <div class="col-lg-12" style="overflow-x:auto;">
                <button class="btn btn-md btn-primary" id="addBtn_motor_history" type="button">
                    Add New Row
                </button>
              <div class="table-responsive">
                  <!--Table-->
                  <table class="table table-striped">

                    <!--Table head-->
                    <thead>
                      <tr>
                        <th>Type</th>
                        <th>Make & Model Year</th> 
                        <th>Registration</th>
                        <th>Approximate Market Value</th>
                        
                      </tr>
                    </thead>
                    <!--Table head-->

                    <!--Table body-->
                    <tbody id="tbody_motor_history">
                      
                    </tbody>
                    <!--Table body-->
                  </table>
                  <!--Table-->
                </div>
              </div>
            </div>
          </div>



          <div class="box-header with-border box box-info">
            <h3 class="box-title">C.3 List investments and valuable (shares, bond, fixed deposits, gold etc) </h3>
          </div>

          <!-- /.box-header -->
          <div class="box-body">
             <div class="row">

              <div class="col-lg-12" style="overflow-x:auto;">
                <button class="btn btn-md btn-primary" id="addBtn_investment_history" type="button">
                    Add New Row
                </button>
              <div class="table-responsive">
                  <!--Table-->
                  <table class="table table-striped">

                    <!--Table head-->
                    <thead>
                      <tr>
                        <th>Type</th>
                        <th>Face Value</th> 
                        <th>Approximate Market Value</th>
                        
                      </tr>
                    </thead>
                    <!--Table head-->

                    <!--Table body-->
                    <tbody id="tbody_investment_history">
                      
                    </tbody>
                    <!--Table body-->
                  </table>
                  <!--Table-->
                </div>
              </div>
            </div>
          </div>



          <div class="box-header with-border box box-info">
            <h3 class="box-title">C.4 List all other assests (include all family assets not listed above) </h3>
          </div>

          <!-- /.box-header -->
          <div class="box-body">
             <div class="row">

              <div class="col-lg-12" style="overflow-x:auto;">
                <button class="btn btn-md btn-primary" id="addBtn_assets_history" type="button">
                    Add New Row
                </button>
              <div class="table-responsive">
                  <!--Table-->
                  <table class="table table-striped">

                    <!--Table head-->
                    <thead>
                      <tr>
                        <th>Type</th>
                        <th>Face Value</th> 
                        <th>Approximate Market Value</th>
                        
                      </tr>
                    </thead>
                    <!--Table head-->

                    <!--Table body-->
                    <tbody id="tbody_assets_history">
                      
                    </tbody>
                    <!--Table body-->
                  </table>
                  <!--Table-->
                </div>
              </div>
            </div>
          </div>


           <div class="box-header with-border box box-info">
            <h3 class="box-title">Total Family assets - Approximate market value (Pak Rs) - For Section C</h3>
          </div>

          <!-- /.box-header -->
          <div class="box-body">
             <div class="row">
              <div class="col-md-12">
                <div class="table-responsive">
                <!-- /.form-group -->
                <table class="table table-striped">
                    <tbody>
                      <tr>
                        <th scope="row">Total Assests Value</th>
                        <td>
                          <div class="form-group">
                            <input type="number" name="secB_total_assets" id="secB_total_assets" class="form-control" readonly required >
                          </div>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                  </div>             
                <!-- /.form-group -->
              </div>
              
            </div>
          </div>




          <div class="box-header with-border box box-info">
            <div class="alert alert-success" role="alert">
               <h4 style="text-align:center">SECTION D: UTILITY EXPENSES</h4>
            </div>
            <h3 class="box-title">D.1 Telephone expenses (Both LandLine and Mobile) - (Monthly average bill) </h3>
          </div>

          <!-- /.box-header -->
          <div class="box-body">
             <div class="row">

              <div class="col-lg-12" style="overflow-x:auto;">
                <button class="btn btn-md btn-primary" id="addBtn_tel_expense_history" type="button">
                    Add New Row
                </button>
              <div class="table-responsive">
                  <!--Table-->
                  <table class="table table-striped">

                    <!--Table head-->
                    <thead>
                      <tr>
                        <th>Type</th>
                        <th>Telephone #</th> 
                        <th>Average Monthly Bill</th>
                        
                      </tr>
                    </thead>
                    <!--Table head-->

                    <!--Table body-->
                    <tbody id="tbody_tel_expense_history">
                      
                    </tbody>
                    <!--Table body-->
                  </table>
                  <!--Table-->
                </div>
              </div>
            </div>
          </div>



          <div class="box-header with-border box box-info">
            <h3 class="box-title">D.2 Electricity expenses (Monthly average bill) </h3>
          </div>

          <!-- /.box-header -->
          <div class="box-body">
             <div class="row">

              <div class="col-lg-12" style="overflow-x:auto;">
                <button class="btn btn-md btn-primary" id="addBtn_elect_expense_history" type="button">
                    Add New Row
                </button>
              <div class="table-responsive">
                  <!--Table-->
                  <table class="table table-striped">

                    <!--Table head-->
                    <thead>
                      <tr>
                        <th>Consumer Number</th>
                        <th>Address</th> 
                        <th>Average Monthly Bill</th>
                        
                      </tr>
                    </thead>
                    <!--Table head-->

                    <!--Table body-->
                    <tbody id="tbody_elect_expense_history">
                      
                    </tbody>
                    <!--Table body-->
                  </table>
                  <!--Table-->
                </div>
              </div>
            </div>
          </div>



          <div class="box-header with-border box box-info">
            <h3 class="box-title">D.3 Gas expenses (Monthly average bill) </h3>
          </div>

          <!-- /.box-header -->
          <div class="box-body">
             <div class="row">

              <div class="col-lg-12" style="overflow-x:auto;">
                <button class="btn btn-md btn-primary" id="addBtn_gas_expense_history" type="button">
                    Add New Row
                </button>
              <div class="table-responsive">
                  <!--Table-->
                  <table class="table table-striped">

                    <!--Table head-->
                    <thead>
                      <tr>
                        <th>Consumer Number</th>
                        <th>Address</th> 
                        <th>Average Monthly Bill</th>
                        
                      </tr>
                    </thead>
                    <!--Table head-->

                    <!--Table body-->
                    <tbody id="tbody_gas_expense_history">
                      
                    </tbody>
                    <!--Table body-->
                  </table>
                  <!--Table-->
                </div>
              </div>
            </div>
          </div>


          <div class="box-header with-border box box-info">
            <h3 class="box-title">Monthly family utility expenditure (Pak Rs) - For Section D</h3>
          </div>

          <!-- /.box-header -->
          <div class="box-body">
             <div class="row">
              <div class="col-md-12">
                <div class="table-responsive">
                <!-- /.form-group -->
                <table class="table table-striped">
                    <tbody>
                      <tr>
                        <th scope="row">Total Utility Expenses</th>
                        <td>
                          <div class="form-group">
                            <input type="number" name="secD_total_utility_exp" id="secD_total_utility_exp" class="form-control" readonly required >
                          </div>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                  </div>             
                <!-- /.form-group -->
              </div>
              
            </div>
          </div>


          <div class="box-header with-border box box-info">
            <div class="alert alert-success" role="alert">
               <h4 style="text-align:center">SECTION E: HARDSHP OR NEED JUSTIFICATION</h4>
            </div>
            <h3 class="box-title">E.1 Background Information </h3>
          </div>

          <!-- /.box-header -->
          <div class="box-body">
             <div class="row">
                 <div class="col-md-3">
                  <!-- /.form-group -->
                  <div class="form-group">
                    <label>Father passed away?</label>
                      <select class='form-control select2' name="father_living_status" id="father_living_status" required>
                          <option >Select Option</option>
                          <option value="Yes">Yes</option>
                          <option value="No">No</option>
                      </select>
                  </div>
                </div>

                <div class="col-md-3">
                   <div class="form-group">
                    <label>Father is disabled?</label><br />
                      <select class='form-control select2' name="father_disabled_status" id="father_disabled_status" required>
                          <option >Select Option</option>
                          <option value="Yes">Yes</option>
                          <option value="No">No</option>
                      </select>
                  </div>
                </div>


                <div class="col-md-3">
                  <div class="form-group">
                    <label>Both father and mother passed away?</label>
                      <select class='form-control select2' name="father_mother_living_status" id="father_mother_living_status" required>
                          <option >Select Option</option>
                          <option value="Yes">Yes</option>
                          <option value="No">No</option>
                      </select>
                  </div>
                </div>

                <div class="col-md-3">
                  <div class="form-group">
                    <label>Domicile from the backward area?</label>
                     <input type="text" class="form-control" name="std_domicile" id="std_domicile" placeholder="Balochistan, Interior Sindh, Thal" required />
                  </div>
                </div>


                <div class="col-md-12">
                  <div class="form-group">
                    <label>How much loan/scholarship do you need? (Justification for the right amount will increase your chance, the Grants committee will have a final decision regarding the total granted amount as per your need and wealth/household income)</label>
                     <textarea class="form-control" rows="6", cols="50" name="loan_need_justification" id="loan_need_justification" required ></textarea>
                  </div>
                </div>


                <div class="col-md-12">
                  <div class="form-group">
                    <label>Statement of purpose/ describe your hardship and need for this load. (Atleast 5 lines)</label>
                    <textarea class="form-control" rows="6", cols="50" name="loan_purpose" id="loan_purpose" required ></textarea>
                  </div>
                </div>
            </div>
          </div>



          <div class="box-header with-border box box-info">
            <div class="alert alert-success" role="alert">
               <h4 style="text-align:center">SECTION F: ACHIEVEMENTS</h4>
            </div>
            <h3 class="box-title">F.1 Achievements Information </h3>
          </div>

          <!-- /.box-header -->
          <div class="box-body">
             <div class="row">
                 <div class="col-md-3">
                  <!-- /.form-group -->
                  <div class="form-group">
                    <label>Marks in Matriculation</label>
                    <input type="number" class="form-control" name="matriculation_marks" id="matriculation_marks" required />
                  </div>
                </div>

                <div class="col-md-3">
                  <!-- /.form-group -->
                  <div class="form-group">
                    <label>Marks in F.Sc.</label>
                    <input type="number" class="form-control" name="fsc_marks" id="fsc_marks" required />
                  </div>
                </div>


                <div class="col-md-12">
                  <div class="form-group">
                    <label>Summarize your extracurrucular Achievements (Sports, public speaking, Volunteer work, small business, society or clubs, awards etc.)</label>
                     <textarea class="form-control" rows="6", cols="50" name="extracurrucular_activity" id="extracurrucular_activity" required ></textarea>
                  </div>
                </div>


                <div class="col-md-12">
                  <div class="form-group">
                    <label>Summarize your educational Achievements. (Certifications, skills, Distictions)</label>
                    <textarea class="form-control" rows="6", cols="50" name="loan_purpose" id="loan_purpose" required ></textarea>
                  </div>
                </div>
            </div>
          </div>

          <div class="box-header with-border box box-info">
            <h3 class="box-title">Other Details</h3>
          </div>

          <!-- /.box-header -->
          <div class="box-body">
            <div class="row">

          </div>

          <div class="box-footer">
              <button class="btn btn-social btn-success" id="personal-detail-btn" type="submit"><i class="fa fa-save"></i> Apply</button>
          </div>
        </div>
      </form>
        <!-- /.box --> 
      </section>
      <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->



<script type='text/javascript' src='https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js'></script>


<!--Table for Father Employement History-->
<script>
    $(document).ready(() => {
 
        // Adding row on click to Add New Row button
        $('#addBtn').click(function () {
            let dynamicRowHTML = `
            <tr class="rowClass"">
            <td class="row-index text-center">
              <input type="date" name="emp_his_dateF" id="emp_his_dateF" class="form-control" required>
            </td>

            <td class="text-center">
              <input type="date" name="emp_his_dateT" id="emp_his_dateT" class="form-control" required>
            </td>
            <td class="text-center">
              <input type="text" name="emp_his_des" id="emp_his_des" class="form-control" required>
            </td>
            <td class="text-center">
              <input type="text" name="emp_his_org" id="emp_his_org" class="form-control" required>
            </td>
            <td class="text-center">
              <input type="text" name="emp_his_salry" id="emp_his_salry" class="form-control" required>
            </td>
            <td class="text-center">
              <button class="btn btn-danger remove" type="button">
                  Remove
              </button>
            </td>

            </tr>`;
            $('#tbody').append(dynamicRowHTML);
        });
 
        // Removing Row on click to Remove button
        $('#tbody').on('click', '.remove', function () {
            $(this).parent('td.text-center').parent('tr.rowClass').remove(); 
        });
    })
</script>

<!--End Table for Father Employement History--> 



<!--Table for Mother Employement History-->
<script>
    $(document).ready(() => {
 
        // Adding row on click to Add New Row button
        $('#addBtn_mother').click(function () {
            let dynamicRowHTML = `
            <tr class="rowClass"">
            <td class="row-index text-center">
              <input type="date" name="emp_his_dateF_mother" id="emp_his_dateF_mother" class="form-control" required>
            </td>

            <td class="text-center">
              <input type="date" name="emp_his_dateT_mother" id="emp_his_dateT_mother" class="form-control" required>
            </td>
            <td class="text-center">
              <input type="text" name="emp_his_des_mother" id="emp_his_des_mother" class="form-control" required>
            </td>
            <td class="text-center">
              <input type="text" name="emp_his_org_mother" id="emp_his_org_mother" class="form-control" required>
            </td>
            <td class="text-center">
              <input type="text" name="emp_his_salry_mother" id="emp_his_salry_mother" class="form-control" required>
            </td>
            <td class="text-center">
              <button class="btn btn-danger remove" type="button">
                  Remove
              </button>
            </td>

            </tr>`;
            $('#tbody_mother').append(dynamicRowHTML);
        });
 
        // Removing Row on click to Remove button
        $('#tbody_mother').on('click', '.remove', function () {
            $(this).parent('td.text-center').parent('tr.rowClass').remove(); 
        });
    })
</script>

<!--End Table for Mother Employement History--> 

<!--Table for Siblings Information-->
<script>
    $(document).ready(() => {
 
        // Adding row on click to Add New Row button
        $('#addBtn_siblings').click(function () {
            let dynamicRowHTML = `
            <tr class="rowClass"">
            <td class="row-index text-center">
              <input type="text" name="sibling_name" id="sibling_name" class="form-control" required >
            </td>

            <td class="text-center">
              <input type="date" name="sibling_dob" id="sibling_dob" class="form-control" required >
            </td>

            <td class="text-center">
              <select class='form-control select2' name="sibling_relation" id="sibling_relation" required>
                  <option value="Brother">Brother</option>
                  <option value="Sister">Sister</option>
              </select>
            </td>

            <td class="text-center">
               <input type="text" name="sibling_class" id="sibling_class" class="form-control" placeholder="Institute Name" required >
            </td>

            <td class="text-center">
              <input type="text" name="sibling_anual_fee" id="sibling_anual_fee" class="form-control" required >
            </td>

            <td class="text-center">
            <input type="text" name="sibling_granting_agency" id="sibling_granting_agency" class="form-control" required >
            </td>

            <td class="text-center">
              <button class="btn btn-danger remove" type="button">
                  Remove
              </button>
            </td>

            </tr>`;
            $('#tbody_siblings').append(dynamicRowHTML);
        });
 
        // Removing Row on click to Remove button
        $('#tbody_siblings').on('click', '.remove', function () {
            $(this).parent('td.text-center').parent('tr.rowClass').remove(); 
        });
    })
</script>

<!--End Table for Siblings Information--> 



<!--Table for Siblings History-->
<script>
    $(document).ready(() => {
 
        // Adding row on click to Add New Row button
        $('#addBtn_siblings_emp_history').click(function () {
            let dynamicRowHTML = `
            <tr class="rowClass"">
            <td class="row-index text-center">
              <input type="text" name="sibling_name_employeed" id="sibling_name_employeed" class="form-control" required >
            </td>

            <td class="text-center">
              <select class='form-control select2' name="sibling_profession" id="sibling_profession" required>
                  <option value="Service">Service</option>
                  <option value="Businessman">Businessman</option>
              </select>
            </td>

            <td class="text-center">
              <input type="text" name="sibling_designation" id="sibling_designation" class="form-control" required >
            </td>

            <td class="text-center">
              <input type="text" name="sibling__company_emp" id="sibling__company_emp" class="form-control" required >
            </td>

            <td class="text-center">
              <input type="text" name="sibling__income_emp" id="sibling__income_emp" class="form-control" required >
            </td>

            <td class="text-center">
              <button class="btn btn-danger remove" type="button">
                  Remove
              </button>
            </td>

            </tr>`;
            $('#tbody_siblings_emp_history').append(dynamicRowHTML);
        });
 
        // Removing Row on click to Remove button
        $('#tbody_siblings_emp_history').on('click', '.remove', function () {
            $(this).parent('td.text-center').parent('tr.rowClass').remove(); 
        });
    })
</script>

<!--End Table for Siblings Employment History--> 


<!--Table for Wealth History-->
<script>
    $(document).ready(() => {
 
        // Adding row on click to Add New Row button
        $('#addBtn_wealth_history').click(function () {
            let dynamicRowHTML = `
            <tr class="rowClass"">
            <td class="row-index text-center">
              <input type="text" name="wealth_adress" id="wealth_adress" class="form-control" required >
            </td>

            <td class="text-center">
              <select class='form-control select2' name="wealth_property_type" id="wealth_property_type" required>
                  <option value="commercial">commercial</option>
                  <option value="residental plot">residental plot</option>
                  <option value="constructed">constructed</option>
              </select>
            </td>

            <td class="text-center">
              <input type="text" name="wealth_area" id="wealth_area" class="form-control" required >
            </td>

            <td class="text-center">
              <input type="text" name="wealth_market_value" id="wealth_market_value" class="form-control" required >
            </td>

            <td class="text-center">
              <select class='form-control select2' name="wealth_property_status" id="wealth_property_status" required>
                  <option value="self occupied">self occupied</option>
                  <option value="rented">rented</option>
                  <option value="vacant">vacant</option>
              </select>
            </td>

            <td class="text-center">
              <button class="btn btn-danger remove" type="button">
                  Remove
              </button>
            </td>

            </tr>`;
            $('#tbody_wealth_history').append(dynamicRowHTML);
        });
 
        // Removing Row on click to Remove button
        $('#tbody_wealth_history').on('click', '.remove', function () {
            $(this).parent('td.text-center').parent('tr.rowClass').remove(); 
        });
    })
</script>

<!--End Table for Wealth History--> 




<!--Table for motor History-->
<script>
    $(document).ready(() => {
 
        // Adding row on click to Add New Row button
        $('#addBtn_motor_history').click(function () {
            let dynamicRowHTML = `
            <tr class="rowClass"">
            <td class="text-center">
              <select class='form-control select2' name="motor_type" id="motor_type" required>
                  <option value="car">car</option>
                  <option value="motorcycle">motorcycle</option>
                  <option value="other">other</option>
              </select>
            </td>

            <td class="text-center">
              <input type="text" name="motor_make_year" id="motor_make_year" class="form-control" required >
            </td>

            <td class="text-center">
              <input type="text" name="motor_registration" id="motor_registration" class="form-control" required >
            </td>

            <td class="text-center">
              <input type="text" name="motor_market_value" id="motor_market_value" class="form-control" required >
            </td>

            <td class="text-center">
              <button class="btn btn-danger remove" type="button">
                  Remove
              </button>
            </td>

            </tr>`;
            $('#tbody_motor_history').append(dynamicRowHTML);
        });
 
        // Removing Row on click to Remove button
        $('#tbody_motor_history').on('click', '.remove', function () {
            $(this).parent('td.text-center').parent('tr.rowClass').remove(); 
        });
    })
</script>

<!--End Table for Motor History--> 



<!--Table for investment History-->
<script>
    $(document).ready(() => {
 
        // Adding row on click to Add New Row button
        $('#addBtn_investment_history').click(function () {
            let dynamicRowHTML = `
            <tr class="rowClass"">
            <td class="text-center">
              <select class='form-control select2' name="investment_type" id="investment_type" required>
                  <option value="shares">shares</option>
                  <option value="bonds">bonds</option>
                  <option value="fixed deposits">fixed deposits</option>
                  <option value="gold">gold</option>
                  <option value="other">other</option>
              </select>
            </td>

            <td class="text-center">
              <input type="number" name="investment_face_value" id="investment_face_value" class="form-control" required >
            </td>

            <td class="text-center">
              <input type="text" name="approx_market_value" id="approx_market_value" class="form-control" required >
            </td>

            <td class="text-center">
              <button class="btn btn-danger remove" type="button">
                  Remove
              </button>
            </td>

            </tr>`;
            $('#tbody_investment_history').append(dynamicRowHTML);
        });
 
        // Removing Row on click to Remove button
        $('#tbody_investment_history').on('click', '.remove', function () {
            $(this).parent('td.text-center').parent('tr.rowClass').remove(); 
        });
    })
</script>

<!--End Table for Investment History--> 


<!--Table for assets History-->
<script>
    $(document).ready(() => {
 
        // Adding row on click to Add New Row button
        $('#addBtn_assets_history').click(function () {
            let dynamicRowHTML = `
            <tr class="rowClass"">
            <td class="text-center">
              <select class='form-control select2' name="assets_type" id="assets_type" required>
                  <option value="shares">shares</option>
                  <option value="bonds">bonds</option>
                  <option value="fixed deposits">fixed deposits</option>
                  <option value="gold">gold</option>
                  <option value="property">property</option>
                  <option value="other">other</option>
              </select>
            </td>

            <td class="text-center">
              <input type="number" name="assets_face_value" id="assets_face_value" class="form-control" required >
            </td>

            <td class="text-center">
              <input type="text" name="assets_approx_market_value" id="assets_approx_market_value" class="form-control" required >
            </td>

            <td class="text-center">
              <button class="btn btn-danger remove" type="button">
                  Remove
              </button>
            </td>

            </tr>`;
            $('#tbody_assets_history').append(dynamicRowHTML);
        });
 
        // Removing Row on click to Remove button
        $('#tbody_assets_history').on('click', '.remove', function () {
            $(this).parent('td.text-center').parent('tr.rowClass').remove(); 
        });
    })
</script>

<!--End Table for assets History--> 



<!--Table for telephone History-->
<script>
    $(document).ready(() => {
 
        // Adding row on click to Add New Row button
        $('#addBtn_tel_expense_history').click(function () {
            let dynamicRowHTML = `
            <tr class="rowClass"">
            <td class="text-center">
              <select class='form-control select2' name="telephone_type" id="telephone_type" required>
                  <option value="landline">landline</option>
                  <option value="mobile">mobile</option>
              </select>
            </td>

            <td class="text-center">
              <input type="number" name="tel_expense_telephone" id="tel_expense_telephone" class="form-control" required >
            </td>

            <td class="text-center">
              <input type="text" name="tel_expense_bill" id="tel_expense_bill" class="form-control" required >
            </td>

            <td class="text-center">
              <button class="btn btn-danger remove" type="button">
                  Remove
              </button>
            </td>

            </tr>`;
            $('#tbody_tel_expense_history').append(dynamicRowHTML);
        });
 
        // Removing Row on click to Remove button
        $('#tbody_tel_expense_history').on('click', '.remove', function () {
            $(this).parent('td.text-center').parent('tr.rowClass').remove(); 
        });
    })
</script>

<!--End Table for telephone History--> 


<!--Table for electricity History-->
<script>
    $(document).ready(() => {
 
        // Adding row on click to Add New Row button
        $('#addBtn_elect_expense_history').click(function () {
            let dynamicRowHTML = `
            <tr class="rowClass"">

            <td class="text-center">
              <input type="number" name="elect_expense_consumer" id="elect_expense_consumer" class="form-control" required >
            </td>

            <td class="text-center">
              <input type="text" name="elect_expense_address" id="elect_expense_address" class="form-control" required >
            </td>

            <td class="text-center">
              <input type="text" name="elect_expense_monthly_bill" id="elect_expense_monthly_bill" class="form-control" required >
            </td>

            <td class="text-center">
              <button class="btn btn-danger remove" type="button">
                  Remove
              </button>
            </td>

            </tr>`;
            $('#tbody_elect_expense_history').append(dynamicRowHTML);
        });
 
        // Removing Row on click to Remove button
        $('#tbody_elect_expense_history').on('click', '.remove', function () {
            $(this).parent('td.text-center').parent('tr.rowClass').remove(); 
        });
    })
</script>

<!--End Table for electricity History--> 


<!--Table for gas History-->
<script>
    $(document).ready(() => {
 
        // Adding row on click to Add New Row button
        $('#addBtn_gas_expense_history').click(function () {
            let dynamicRowHTML = `
            <tr class="rowClass"">

            <td class="text-center">
              <input type="number" name="gas_expense_consumer" id="gas_expense_consumer" class="form-control" required >
            </td>

            <td class="text-center">
              <input type="text" name="gas_expense_address" id="gas_expense_address" class="form-control" required >
            </td>

            <td class="text-center">
              <input type="text" name="gas_expense_monthly_bill" id="gas_expense_monthly_bill" class="form-control" required >
            </td>

            <td class="text-center">
              <button class="btn btn-danger remove" type="button">
                  Remove
              </button>
            </td>

            </tr>`;
            $('#tbody_gas_expense_history').append(dynamicRowHTML);
        });
 
        // Removing Row on click to Remove button
        $('#tbody_gas_expense_history').on('click', '.remove', function () {
            $(this).parent('td.text-center').parent('tr.rowClass').remove(); 
        });
    })
</script>

<!--End Table for electricity History--> 



<script>

 function ShowHideDiv() {
        var cattle_status = document.getElementById("cattle_status");
        var noofcattles_div = document.getElementById("noofcattles_div");
        noofcattles_div.style.display = cattle_status.value == "1" ? "block" : "none";
    }




$(function() {
$("#bank_balance, #stock, #misc").on("keydown keyup", total_assets);

  function total_assets() {
  $("#total_assets").val(Number($("#bank_balance").val())+ Number($("#stock").val())+ Number($("#misc").val()));
  }
});



$(function() {
$("#guardian_income, #mother_income, #land_income, #misc_income").on("keydown keyup", monthly_income);

  function monthly_income() {
  $("#monthly_income").val(Number($("#guardian_income").val())+ Number($("#mother_income").val())+ Number($("#land_income").val())+ Number($("#misc_income").val()));
  }
});


$(function() {
$("#guardian_income, #mother_income, #land_income, #misc_income, #gas_bill, #electricity_bill,#telephone_bill,#water_bill,#edu_expenditure,#family_accommodation,#food_expense,#medical_expense,#pocketmoney").on("keydown keyup", disposable_income);

  function disposable_income() {
  $("#disposable_income").val(Number($("#guardian_income").val())+ Number($("#mother_income").val())+ Number($("#land_income").val())+ Number($("#misc_income").val()) - Number($("#gas_bill").val()) - Number($("#electricity_bill").val()) - Number($("#telephone_bill").val()) - Number($("#water_bill").val()) - Number($("#edu_expenditure").val()) - Number($("#family_accommodation").val()) - Number($("#food_expense").val()) - Number($("#medical_expense").val()) - Number($("#pocketmoney").val())   );
  }
});




$(function() {
    $("#gas_bill, #electricity_bill,#telephone_bill,#water_bill,#edu_expenditure,#family_accommodation,#food_expense,#medical_expense,#pocketmoney").on("keydown keyup", monthly_expensiture);

  function monthly_expensiture() {
  $("#monthly_expensiture").val(Number($("#gas_bill").val())+ Number($("#pocketmoney").val())+ Number($("#medical_expense").val())+ Number($("#food_expense").val())+ Number($("#family_accommodation").val())+ Number($("#edu_expenditure").val())+ Number($("#water_bill").val())+ Number($("#electricity_bill").val()) + Number($("#telephone_bill").val()));
  }
});


</script>
@include('frontend.footer')