
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Scholarship Form</title>
        <link href="https:/stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    </head>

    <body>
        <style>
            body{
                font-size:14px;
                line-height:18px;
                color:#000;
                font-family:Arial, Helvetica, sans-serif;
            }
            p{
                margin:0px 0px 2px 0px;
            }
            .admission-form-main{
                width:700px;
                margin:0 auto;
            }
            .admission-form-main p{
                font-weight:bold;
            }		
            table {
                border-collapse: collapse;
                border-spacing: 0;
                width: 100%;
                border: 1px solid #ddd;
            }

            th, td {
                text-align: left;
                padding:5px 5px;
                border: 1px solid #ddd;
            }
            .table-main{
                padding-bottom:10px;
                width:100%;
            }
            .row::after {
                content: "";
                clear: both;
                display: table;
            }
            [class*="col-"] {
                float: left;

            }
            .col-1 {width: 8.33%;}
            .col-2 {width: 16.66%;}
            .col-3 {width: 25%;}
            .col-4 {width: 33.33%;}
            .col-5 {width: 41.66%;}
            .col-6 {width: 50%;}
            .col-7 {width: 58.33%;}
            .col-8 {width: 66.66%;}
            .col-9 {width: 75%;}
            .col-10 {width: 83.33%;}
            .col-11 {width: 91.66%;}
            .col-12 {width: 100%;}	
            @media (max-width:500px){ 
                .row, .text-right{
                    text-align:center !important;
                }
                .col-1, .col-2, col-3, .col-4, .col-5, .col-6, .col-7, .col-8, .col-9, .col-10, .col-11, .col-12{
                    width:100%;
                }
                .table-main{
                    width:100%;
                    overflow:scroll;

                }
                .admission-form-main {
                    width:100%;

                }
                .table-main table{
                    width:700px;
                }		

            }	
            @media print { 
                .applicant-signature {
                    padding-top:30px;

                }
                #printpagebutton{
                    display:none;	

                }
                .name-mail {
                    padding-bottom:5px;
                }
                .table-main{
                    padding-bottom:5px;
                    width:100%;
                }
                body{
                    font-size:12px;
                }
                table{
                    width:100% !important;
                    overflow:hidden !important;
                }
                .admission-form-main {
                    width:98% !important;
                    overflow:hidden !important;

                }	

            }
            .text-center{
                text-align:center;
            }
            .text-right{
                text-align:right;
            }
            .row p{
                font-size:12px;

            }	
            h4, h3{
                margin:7px 0px;
                line-height:18px;
            }	
            h1{
                line-height:28px;
                color:#777;
            }	
            .name-mail {
                padding-bottom:10px;
            }	
            .name-mail p, .decalartion p{
                font-weight:normal !important;
            }	
            .name-mail p span{
                font-weight:bold !important;
            }
            .decalartion p{
                line-height:20px;
                margin-bottom:15px;
                text-align: justify;
            }				
            .decalartion{
                padding-top:15px;
            }


            .name-mail tr td {
                padding:4px 5px;
                font-size:12px;
                line-height:10px;

            }
            .btn {
                display: inline-block;
                padding: 6px 12px;
                margin-bottom: 0;
                font-size: 14px;
                font-weight: 400;
                line-height: 1.42857143;
                text-align: center;
                white-space: nowrap;
                vertical-align: middle;
                -ms-touch-action: manipulation;
                touch-action: manipulation;
                cursor: pointer;
                -webkit-user-select: none;
                -moz-user-select: none;
                -ms-user-select: none;
                user-select: none;
                background-image: none;
                border: 1px solid transparent;
                border-radius: 4px;
            }

            .under-tr td{
                height: 25px;
            }
            .less-fifty{
                color: #FF0000;
            }
            .education-detail td:last-child{
                text-align: right;
            }
            .mar-tp-10{
                margin-top: 10px;
            }
        </style>
        <button id="printpagebutton" class="btn btn-default" style="float:right; margin-top: 1em;" type="button"  onclick="printpage()"/><i class="fa fa-print"></i> Print</button>
        <div class="admission-form-main">
<?php 

//$appli_semes = $oas_applications->applicant->previousQualification($current_semester)->first();
    ?> 

            <div class="row" style="padding-bottom:15px;">

                <div class="col-2"><img width="80" src="/storage/images/iiulogo.jpg" /></div>
                <div class="col-8 text-center">
                    <img width="250" src="/storage/images/jami.jpg" />
                    <h3>INTERNATIONAL ISLAMIC UNIVERSITY, ISLAMABAD</h3>
                    <h4>General Application Form for Scholarship</h4> 
                    <h4>{{$scholarshipname->name}}</h4> 
                      <!-- Dev Mannan: Upper Case -->
                </div>
               <!--  <div class="col-2 text-right">
                    <p>Form No: </p>
                </div> -->

            </div>
            <h3>Important Instructions Before Applying for Scholarship relevent Scholarship</h3>

            <ul>
                <li>
                    Please check eligibility criteria carefully. Apply only if qualify as per requirement.
                </li>
            </ul>
            <hr />
            

            <div class="row name-mail">

                <div class="col-6">
                    <table>
                        <tr>
                            <td>Name:</td>
                            <td><b>{{$applicant->name}} </b></td>

                        </tr>

                        <tr>
                            <td>Marital Status</td>
                            <td><b>{{($applicantDetail->married) ?? 'Nill' }}</b></td>
                        </tr>

                        <tr>
                            <td>Date of Birth:</td>
                            <td><b>{{($applicantDetail->DOB) ?? 'Nill'}}</b></td>

                        </tr>
                        <tr>
                            <td>CNIC/Passport No: </td>
                            <td><b>{{$applicant->cnic}}</b></td>

                        </tr>

                        <tr>
                            <td>Gender: </td>
                            <td><b><?php if($applicant->fkGenderId == '1'){
                                echo "Male & Female (Both)";
                            }else if($applicant->fkGenderId == '2'){
                                echo "Female";
                            }else {
                                echo "Male";
                            }
                        ?>
                            
                        </b></td>

                        </tr>

                        <tr>
                            <td>Mobile:</td>
                            <td><b>{{($applicantDetail->mobile) ?? 'Nill'}}</b></td>

                        </tr>

                        <tr>
                            <td>Nationality: </td>
                            <td><b>{{$applicant->nationality->nationality}}  </b></td>

                        </tr>
                        <tr>
                            <td>Religion</td>
                            <td><b>{{($applicantDetail->religion) ?? 'Nill'}}</b></td>
                        </tr>

                        <tr>
                            <td>Domicile  </td>
                            <td><b>{{($applicantDetail->province->provName) ?? 'Nill'}} </b></td>
                        </tr>

                        <tr>
                            <td>Email:</td>
                            <td><b>{{($applicant->email) ?? 'Nill'}} </b></td>
                        </tr>
                        <tr>
                            <td>Reg:</td>
                            <td><b>{{$scholarshipinformation->reg}} </b></td>
                        </tr>
                         <tr>
                            <td>CGPA:</td>
                            <td><b>{{$scholarshipinformation->cgpa}} </b></td>
                        </tr>
                    </table>
                </div>

                <div class="col-6">
                    <table>
                         <tr>
                            <td>Father's Name:</td>
                            <td><b>{{($applicantDetail->fatherName) ?? 'Nill'}}</b></td>
                        </tr>
                        <tr>
                            <td>Father's Status:</td>
                            <td><b><?php echo ($applicantDetail->fatherStatus==1)?'Alive':'Deceased'; ?></b></td>
                        </tr>

                        <tr>
                            <td>Father CNIC</td>
                            <td><b>{{($applicantDetail->father_cnic) ?? 'Nill'}}</b></td>
                        </tr>

                        <tr>
                            <td>Father Mobile</td>
                            <td><b>{{($applicantDetail->fatherMobile) ?? 'Nill'}}</b></td>
                        </tr>

                        <tr>
                            <td>Father Occupation</td>
                            <td><b>{{($applicantDetail->fatherOccupation) ?? 'Nill'}}</b></td>
                        </tr>


                        <tr>
                            <td>Guardian Name</td>
                            <td><b>{{($applicantDetail->Guradian_Spouse) ?? 'Nill'}}</b></td>
                        </tr>
                        <tr>
                            <td>Father Death Date (if any)</td>
                            <td><b>{{$scholarshipFamilyDetail->death_date}}</b></td>
                        </tr>
                        <tr>
                            <td>Any Other Supporting Person</td>
                            <td><b>{{($scholarshipFamilyDetail->supporting_person) ?? 'Nill'}}</b></td>
                        </tr>

                        <tr>
                            <td></td>
                            <td><b></b></td>
                        </tr>

                        <tr>
                            <td>Year of Passing</td>
                            <td><b>{{$scholarshipinformation->year_of_passing}}</b></td>
                        </tr>


                        <tr>
                            <td>Total Marks In FSC</td>
                            <td><b>{{$scholarshipinformation->total_marks}}</b></td>
                        </tr>

                        <tr>
                            <td>Total Obtain Marks</td>
                            <td><b>{{$scholarshipinformation->marks_obtain}}</b></td>
                        </tr>
                    </table>
                </div>
            </div>

            <div class="row name-mail">
                <table>
                    <tr>
                        <td style="text-align:left"><b>Name of Last instition attended:</b></td>
                        <td style="text-align:left">{{$scholarshipinformation->last_inst_attend}}</td>
                        <td style="text-align:left"><b>Per Month fee of last institution:</b></td>
                        <td style="text-align:left">{{$scholarshipinformation->inst_fee}}</td>
                    </tr>
                    <tr>
                        <td style="text-align:left"><b>Semester(s) Completed</b></td>
                        <td style="text-align:left">{{$scholarshipinformation->sem_complete}}</td>
                        <td style="text-align:left"><b>Semester(s) Left</b></td>
                        <td style="text-align:left">{{$scholarshipinformation->sem_left}}</td>
                    </tr>
                </table>
            </div>


            <p>Family Income</p>

            <div class="table-main education-detail">
                <table>
                    <tr>
                        <td><b>Dep.Family Mem</b></td>
                        <td><b>Family Mem Studying</b></td>
                        <td><b>Earning Family Members</b></td>
                        <td style="text-align:left"><b>Guardian Income</b></td>
                        
                    </tr>
                    <tr>
                        <td>{{$applicantDetail->dependants}} </td>
                        <td>{{$scholarshipFamilyDetail->family_mem_studing}} </td>
                        <td>{{$scholarshipFamilyDetail->family_mem_earning}} </td>
                        <td style="text-align:left">{{$applicantDetail->monthlyIncome}} </td>
                        
                    </tr>

                    <tr>
                        <td><b>Mother Income</b></td>
                        <td><b>Land Income</b></td>
                        <td><b>Misc income</b></td>
                        <td style="text-align:left"><b>Total Monthly Income</b></td>
                    </tr>
                    <tr>
                        <td>{{$scholarshipFamilyDetail->mother_income}} </td>
                        <td>{{$scholarshipFamilyDetail->land_income}} </td>
                        <td>{{$scholarshipFamilyDetail->misc_income}} </td>
                        <td style="text-align:left">{{$scholarshipFamilyDetail->monthly_income}} </td>
                    </tr>
                </table>
            </div>

            <br>

            <p>Family  Expenditures (Monthly)</p>

            <div class="table-main education-detail">
                <table>
                    <tr>
                        <td><b>Gas Bill</b></td>
                        <td><b>Electricity Bill</b></td>
                        <td><b>Telephone Bill</b></td>
                        <td style="text-align:left"><b>Water Bill</b></td>
                    </tr>
                    <tr>
                        <td>{{$scholarshipFamilyDetail->gas_bill}} </td>
                        <td>{{$scholarshipFamilyDetail->elect_bill}} </td>
                        <td>{{$scholarshipFamilyDetail->tel_bill}} </td>
                        <td style="text-align:left">{{$scholarshipFamilyDetail->water_bill}} </td>
                        
                    </tr>
                    <tr>
                        <td><b>Family's Expenditure on Education</b></td>
                        <td><b>Family's Accommodation Expenses</b></td>
                        <td><b>Food Expense</b></td>
                        <td style="text-align:left"><b>Medical Expense</b></td>
                        
                    </tr>
                    <tr>
                        <td>{{$scholarshipFamilyDetail->edu_expense}} </td>
                        <td>{{$scholarshipFamilyDetail->acc_expense}} </td>
                        <td>{{$scholarshipFamilyDetail->food_expense}} </td>
                        <td style="text-align:left">{{$scholarshipFamilyDetail->medical_expense}} </td>
                        
                    </tr>
                    <tr>
                        <td style="text-align:left"><b>Pocket Mony Recieved</b></td>
                        <td style="text-align:left"><b>Total Monthly Expenditure</b></td>
                        
                    </tr>
                    <tr>
                        <td >{{$scholarshipFamilyDetail->pocket_money}} </td>
                        <td style="text-align:left">{{$scholarshipFamilyDetail->monthly_expenditure}} </td>
                        
                    </tr>
                </table>
            </div>

            <br>

            <p>Family  Fixed Assets</p>

            <div class="table-main education-detail">
                <table>
                    <tr>
                        <td><b>No. of Vehicles</b></td>
                        <td><b>Vehicle Type and Model</b></td>
                        <td><b>Vehicle Engine Capacity</b></td>
                        <td style="text-align:left"><b>Size of Land</b></td>
                    </tr>
                    <tr>
                        <td>{{$scholarshipFamilyAssets->vehicals}} </td>
                        <td>{{$scholarshipFamilyAssets->vehicle_type}} </td>
                        <td>{{$scholarshipFamilyAssets->engine_cap}} </td>
                        <td style="text-align:left">{{$scholarshipFamilyAssets->land_size}} </td>
                        
                    </tr>
                    <tr>
                        <td><b>Value of Land</b></td>
                        <td><b>Accommodation and Location</b></td>
                        <td><b>Type of Accommodation and Size</b></td>
                        <td style="text-align:left"><b>Value of Home</b></td>
                    </tr>
                    <tr>
                        <td>{{$scholarshipFamilyAssets->land_value}} </td>
                        <td>{{$scholarshipFamilyAssets->acco_loc}} </td>
                        <td>{{$scholarshipFamilyAssets->acco_size}} </td>
                        <td style="text-align:left">{{$scholarshipFamilyAssets->home_value}} </td>
                    </tr>
                    <tr>
                        <td><b>Any Other Property</b></td>
                        <td style="text-align:left"><b>Value of Property</b></td>
                        <td style="text-align:left"><b>Status of Home</b></td>
                    </tr>
                    <tr>
                        <td>{{$scholarshipFamilyAssets->other_property}} </td>
                        <td style="text-align:left">{{$scholarshipFamilyAssets->property_value}} </td>
                        <td style="text-align:left">{{$scholarshipFamilyAssets->home_status}} </td>
                    </tr>
                </table>
            </div>

            <br>

            <p>Family Current Assets</p>

            <div class="table-main education-detail">
                <table>
                    <tr>
                        <td><b>Bank Balance</b></td>
                        <td><b>Stock/Prize Bond</b></td>
                        <td><b>Misc</b></td>
                        <td style="text-align:left"><b>Total Assets</b></td>
                    </tr>
                    <tr>
                        <td>{{$scholarshipFamilyAssets->bank_bal}} </td>
                        <td>{{$scholarshipFamilyAssets->stock}} </td>
                        <td>{{$scholarshipFamilyAssets->misc}} </td>
                        <td style="text-align:left">{{$scholarshipFamilyAssets->total_asset}} </td>
                    </tr>

                    <tr>
                        <td><b>Numbers Cattle?</b></td>
                        <td style="text-align:left">{{$scholarshipFamilyAssets->no_of_cattles}}</td>
                    </tr>
                </table>
            </div>

            <br>
             <p>Address Details</p>

            <div class="table-main education-detail">
                <table>
                    <tr>
                        <th style="text-align:left"><b>Address Type</b></th>
                        <th style="text-align:left">Address</th>
                        <th style="text-align:left"><b>City</b></th>
                    </tr>
                    <tr>
                        <td style="text-align:left"><b>Present Address</b></td>
                        <td style="text-align:left">{{$applicantAddress->addressPo}}</td>
                        <td style="text-align:left">{{$applicantAddress->cityPo}}</td>
                    </tr>
                    <tr>
                        <td style="text-align:left"><b>Permanent Address</b></td>
                        <td style="text-align:left">{{$applicantAddress->addressPmt}} </td>
                        <td style="text-align:left">{{$applicantAddress->cityPmt}} </td>
                    </tr>
                </table>
            </div>

            <br>
             <p>Remarks</p>

            <div class="table-main education-detail">
                <table>
                    <tr>
                        <td style="text-align:left">{{$scholarshipFamilyAssets->remarks}}</td>
                    </tr>
                </table>
            </div>

            <br>
             <p>How were the admission / first Semester charges paid / any other source of financing?</p>

            <div class="table-main education-detail">
                <table>
                    <tr>
                        <td style="text-align:left">{{$scholarshipFamilyAssets->source_of_financing}}</td>
                    </tr>
                </table>
            </div>

            <br>
             <p>REQUEST OF THE STUDENT/REASON FOR APPLYING</p>

            <div class="table-main education-detail">
                <table>
                    <tr>
                        <td style="text-align:left">{{$scholarshipFamilyAssets->reason_for_apply}}</td>
                    </tr>
                </table>
            </div>
            <br>
            <br>
            <table>
                    <tr>
                        <td><u>Parent/Guardian's Signature</u></td>
                        <td></td>
                        <td></td>
                        <td style="text-align:right"><u>Applicant's Signature</u></td>
                    </tr>
                </table>

            <br>

            <p>Are you already availing any Scholarship through IIUI (if yes) please specify</p>
            <div class="table-main education-detail">
                <table>
                    <tr>
                        <td style="text-align:left" height="20px"></td>
                    </tr>
                </table>
            </div>

            <br>

            <div class="row decalartion">
                <div class="col-12">
                    <h3>To be filled by relevant fee section </h3>
                    <ol>
                        <li>
                            <b>Detail of financial assistance already recieved by the student (from Rector Fund and any other source)</b>
                        </li>
                        <li>
                            <b>Curent academic status of the student</b>
                        </li>
                    </ol> 
                    <table>
                        <tr>
                            <td style="text-align:left">{{$Scholarshipdetail->feeremarks}}</td>
                        </tr>
                    </table>
                </div>
                <div class="col-12 text-right applicant-signature">
                    DD/Assistant Director(Fee)
                </div>
                <div class="col-12 text-right applicant-signature">
                    Sign & Stamp
                </div>
            </div>

            

            <div class="row decalartion">

                <div class="col-12">
                    <h4>Declaration:</h4>
                    <p>I hereby solemnly undertake / declare that I have personally filled in this Application Form and the information contained herein is complete and correct to the best of my knowledge and belief. I understand that withholding or giving false information will make me ineligible for any kind of Scholarship in future and future enrolment and that if withholding or giving false information is discovered after successful awaed of Scholarship, the University has the right to cancel my Scholarship. I further understand that I may be required to appear for an interview or to undergo such test as required by the University Admission Committee as a condition for admission to the programme of study for which I have applied. I will abide by all the current rules & regulations, all other rules to be framed time to time by University administration and will observe Islamic code of conduct during my whole period of study at IIUI.</p>
                    <p>I authorise IIUI to credit my refundable security or any other which amount becomes refundable to me at any stage in the Bank account mentioned above</p>
                </div>
                <div class="col-12 text-right applicant-signature">
                    Applicant's Signature
                </div>
            </div>
            <br>
            <br>
            <br>

            <div class="row decalartion">
                <div class="col-12">
                    <h3>Documents Check List to be attached </h3>
                    <table>
                        <tr>
                            <td style="width:5px">1.</td>
                            <td>Natiional Id Card</td>
                            <td><input type="checkbox"/></td>
                        </tr>
                        <tr>
                            <td style="width:5px">2.</td>
                            <td>Attach receipt of Institution fee</td>
                            <td><input type="checkbox"/></td>
                        </tr>
                        <tr>
                            <td style="width:5px">3.</td>
                            <td>Dependent Family Members (attach B-Form/ID)</td>
                            <td><input type="checkbox"/></td>
                        </tr>
                        <tr>
                            <td style="width:5px">4.</td>
                            <td>Family Members Studying (attach siblings fee receipts / self)</td>
                            <td><input type="checkbox"/></td>
                        </tr>
                        <tr>
                            <td style="width:5px">5.</td>
                            <td>Father/ Husband/ Guardian Income (attach Proof )</td>
                            <td><input type="checkbox"/></td>
                        </tr>
                        <tr>
                            <td style="width:5px">6.</td>
                            <td>Mother's Income (if any) (attach Proof )</td>
                            <td><input type="checkbox"/></td>
                        </tr>
                        <tr>
                            <td style="width:5px">7.</td>
                            <td>Utility Bills last Month (Gas/ Electricity/ Telephone/ Water)</td>
                            <td><input type="checkbox"/></td>
                        </tr>
                        <tr>
                            <td style="width:5px">8.</td>
                            <td>Rented agreement (if applicable)</td>
                            <td><input type="checkbox"/></td>
                        </tr>

                        <tr>
                            <td style="width:5px">9.</td>
                            <td>Medical (if applicable)</td>
                            <td><input type="checkbox"/></td>
                        </tr>
                        <tr>
                            <td style="width:5px">10.</td>
                            <td>Education Documents (Matric / Intermediate)</td>
                            <td><input type="checkbox"/></td>
                        </tr>
                        <tr>
                            <td style="width:5px">11.</td>
                            <td>Father Death Certificate (if applicable)</td>
                            <td><input type="checkbox"/></td>
                        </tr>
                    </table>
                </div>
            </div>

            <br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
            <div>
                <table>
                    <tr>
                        <th colspan="6" style="text-align: center; padding: 25px;">
                            <u>CONDUCT CERTIFICATE</u>
                        </th>
                    </tr>
                    <tr>
                        <td colspan="6" style="text-align: left; padding: 25px;">I de hereby solemnly declare that I have not been involved in any unlawful activity/protest/strike againt university and its policies at any stage during my studies in IIUI.</td>
                    </tr>
                    <tr>
                        <td style="text-align: left; padding: 25px;">Signature:</td>
                        <td style="text-align: left; padding: 25px; " >________________________</td>
                        
                    </tr>
                    <tr>
                        <td style="text-align: left; padding: 25px;">Name:</td>
                        <td style="text-align: left; padding: 25px;"><b>{{$applicant->name}} </b></td>
                    </tr>
                    <tr>
                        <td style="text-align: left; padding: 25px;">Registration No:</td>
                        <td style="text-align: left; padding: 25px;"><b>{{$scholarshipinformation->reg}} </b></td>
                    </tr>
                    <tr>
                        <td colspan="6" style="text-align: center; padding: 25px;">
                            <b><u>VERIFICATION OF THE ABOVE DECLARATION FROM CONCERNED DIRECTOR ACADEMICS</u></b>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align:left">{{$Scholarshipdetail->condverremarks}}
                        </td>
                    </tr>

                    <!-- <tr>
                        <td colspan="6" style="text-align: left; padding: 25px;">
                            The above mentioned statement of the student is:    
                        </td>
                    </tr>

                    <tr>
                        <td>True</td>
                        <td>False</td>
                        <td>True</td>
                        <td>False</td>
                        <td>True</td>
                        <td>False</td>
                    </tr>
                    <tr>
                        <td><input type="checkbox"/></td>
                        <td><input type="checkbox"/></td>
                        <td><input type="checkbox"/></td>
                        <td><input type="checkbox"/></td>
                        <td><input type="checkbox"/></td>
                        <td><input type="checkbox"/></td>
                    </tr>

                    <tr>
                        <td colspan="2" style="text-align: center; padding: 25px;">
                            ___________________<br>Chairperson/ Chairman
                        </td>
                        <td colspan="2" style="text-align: center; padding: 25px;">
                            ___________________<br>Dean
                        </td>
                        <td colspan="2" style="text-align: center; padding: 25px;">
                            ___________________<br>Director Academics
                        </td>
                    </tr> -->
                </table>
                <div class="col-12 text-right applicant-signature">
                    <b>Verified by Director Academics</b>
                </div>
                <!-- <div class="col-12 text-right applicant-signature">
                    Sign & Stamp
                </div> -->
            </div>

        <script>
            function printpage() {
                //Get the print button and put it into a variable
                var printButton = document.getElementById("printpagebutton");
                //Set the print button visibility to 'hidden' 
                printButton.style.visibility = 'hidden';
                //Print the page content
                window.print()
                //Set the print button to 'visible' again 
                //[Delete this line if you want it to stay hidden after printing]
                printButton.style.visibility = 'visible';
            }
        </script>            

    </body>
</html>
