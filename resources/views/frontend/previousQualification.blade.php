@section('title')
Previous Qualification
@endsection
@include('frontend.header')
@php
if(($applicationStatus->step_save == 1 || $applicationStatus->step_programs == 1) && $hash ==''){
  $readonlyUser = "readonly='readonly'";
  $disabledUser = "disabled";
  $disabledUser1 = 'disabled="disabled"';
}else{ 
  $readonlyUser='';
  $disabledUser = '';
  $disabledUser1 = '';
}
@endphp

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          <i class="fa fa-university text-red"></i> Previous Qualification
          <small>Add / Edit Previous Qualification</small>
        </h1>
      </section>
      @if(Session::has('message'))
          <div class="alert alert-success marg-btm">
            {{ Session::get('message') }}
          </div>
      @endif
       @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    @if ( $custom_error )
        <div class="alert alert-danger alert-quali" role="alert"> {{ $custom_error }}</div>
    @endif       

      <!-- Main content -->
      <section class="content">
          <form action="{{route('previousqualification', $hash)}}" class="previousqualificationForm" enctype="multipart/form-data" method="POST">
            <input type="hidden" name="userId" value="{{$userdetail->userId}}">
            <input type="hidden" name="userLevel" value="{{$userdetail->fkLevelId}}">
            <input type="hidden" name="server_date" value="{{date('Y-m-d h:i:s')}}">
            {{csrf_field()}} 
  
        <!-- SELECT2 EXAMPLE -->
        <div class="box box-info">
          <div class="box-header with-border">
            <h3 class="box-title">SSC/O-level</h3>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <div class="row">
              <div class="col-md-3">
                <div class="form-group">
                  <label class="req-rd">Certificate/Degree</label>
                  @php
                  $collection = collect($sscDegree);
                  $filteredItems = $collection->where('title', $previousQualification->SSC_Title);
                  if($filteredItems->count() > 0){
                    $val = '';
                  }else{
                    $val = $previousQualification->SSC_Title;
                  }
                  if($previousQualification->SSC_Title == ''){
                    $val = '';
                  }
                  @endphp
                  <select class="form-control sh-hd-ot" id="SSC_Title_d" name="SSC_Title_d" style="width: 100%;" {{$disabledUser}}>
                    <option value="">Select</option>
                      @foreach ( $sscDegree as $n )
                      <option value="{{$n->title}}" {{ ($previousQualification->SSC_Title == $n->title || old('SSC_Title_d') == $n->title) ? 'selected' : ''}}>{{$n->title}}</option>
                    @endforeach
                  </select>
                  <input type="text" class="form-control ext-hide" value="{{$val ?: old('SSC_Title_t')}}" id="SSC_Title_t" name="SSC_Title_t"  {{(old('SSC_Title_d') == 'Other')? 'style=display:block': ''}}>
                </div>
                <!-- /.form-group -->
              </div>
              <!-- /.col -->
              <div class="col-md-3">
                <div class="form-group">
                  <label class="req-rd">Board/University</label>
                  @php
                  $collection = collect($sscInstitute);
                  $filteredItems = $collection->where('instName', $previousQualification->SSC_From);
                  if($filteredItems->count() > 0){
                    $val = '';
                  }else{
                    $val = $previousQualification->SSC_From;
                  }
                  if($previousQualification->SSC_From == ''){
                    $val = '';
                  }
                  @endphp
                  <select class="form-control sh-hd-ot" id="SSC_From_d" name="SSC_From_d" style="width: 100%; " {{$disabledUser}}>
                    <option value="">Select</option> 
                    @foreach ( $sscInstitute as $n )
                      <option value="{{$n->instName}}" {{ ($previousQualification->SSC_From == $n->instName || old('SSC_From_d') == $n->instName) ? 'selected' : ''}}>{{$n->instName}}</option>
                    @endforeach
                  </select>
                  <input type="text" class="form-control ext-hide" value="{{$val ?: old('SSC_From_t')}}" id="SSC_From_t" name="SSC_From_t"  {{(old('SSC_From_d') == 'Other Boards')? 'style=display:block': ''}}>
                </div>
                <!-- /.form-group -->
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label class="req-rd">Group</label>
                  @php
                  $collection = collect($sscGroup);
                  $filteredItems = $collection->where('subjects', $previousQualification->SSC_Subject);
                  if($filteredItems->count() > 0){
                    $val = '';
                  }else{
                    $val = $previousQualification->SSC_Subject;
                  }
                  if($previousQualification->SSC_Subject == ''){
                    $val = '';
                  }
                  @endphp
                  <select class="form-control sh-hd-ot" id="SSC_Subject_d" name="SSC_Subject_d" style="width: 100%; " {{$disabledUser}}>
                    <option value="">Select</option>
                    @foreach ( $sscGroup as $n )
                      <option value="{{$n->subjects}}" {{ ($previousQualification->SSC_Subject == $n->subjects || old('SSC_Subject_d') == $n->subjects) ? 'selected' : ''}}>{{$n->subjects}}</option>
                    @endforeach
                  </select>
                  <input type="text" class="form-control ext-hide" value="{{$val ?: old('SSC_Subject_t')}}" id="SSC_Subject_t" name="SSC_Subject_t"  {{(old('SSC_Subject_d') == 'Other')? 'style=display:block': ''}}>
                </div>
                <!-- /.form-group -->
              </div>              
              <!-- /.col -->
              <div class="col-md-3">
                <div class="form-group">
                  <label class="req-rd">Completion Year</label>
                  <select class="form-control sh-hd-ot" id="SSC_Year" name="SSC_Year" required style="width: 100%; " {{$disabledUser}}>
                    <option value="">Select</option>
                    @php
                    $i= date('Y')-70;    
                    for($i; $i<= date('Y'); $i++){
                      if($previousQualification->SSC_Year == $i || old('SSC_Year') == $i){
                        $sel = "selected";
                      }else{
                        $sel = '';
                      }
                      echo '<option value="'.$i.'" '.$sel.'>'.$i.'</option>';
                    }
                    @endphp
                  </select>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-3">
                <div class="form-group">
                  <label class="req-rd hide-grade for-o">Total Marks</label>
                  <input type="text" id="o_level_obtained" name="o_level_obtained" value="{{$previousQualification->o_level_obtained ?? old('o_level_obtained') }}" class="form-control show-grade" {{$disabledUser}}>
                  <input type="number" min="1" id="SSC_Total" value="{{$previousQualification->SSC_Total ?? old('SSC_Total') }}" onBlur="checkMarks('SSC_Total','SSC_Composite','ssc_per')" name="SSC_Total" class="form-control hide-grade" required {{$disabledUser}}>
                </div>
                <!-- /.form-group -->
              </div>
              <!-- /.col -->
              <div class="col-md-3">
                <div class="form-group">
                  <label class="req-rd hide-grade">Obtained Marks</label>
                  <input type="number" min="1" id="SSC_Composite" value="{{$previousQualification->SSC_Composite ?? old('SSC_Composite')}}" onBlur="checkMarks('SSC_Total','SSC_Composite','ssc_per')" name="SSC_Composite" class="form-control hide-grade" required {{$disabledUser}}>
                </div>
                <!-- /.form-group -->
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label class="hide-grade">SSC Percentage</label> <br />
                  <label id="ssc_per" class="hide-grade"> 
                    @php
                      if($previousQualification->SSC_Total != '' && $previousQualification->SSC_Composite != ''){
                        $marks = bcdiv(($previousQualification->SSC_Composite/$previousQualification->SSC_Total*100), 1, 2); 
                        if($marks > 49){
                          echo '<span class="ok">'.$marks.' %</span>';
                        }else{
                          echo '<span class="not-ok">'.$marks.' %</span>';
                        }
                    } 
                   @endphp  
                  </label>
                </div>
                <!-- /.form-group -->
              </div>   
              <div class="col-md-3">
                <div class="form-group">
                  <label class="req-rd">Degree Type</label>
                  <select class="form-control sh-hd-ot" id="SSC_degree_type" required name="SSC_degree_type" style="width: 100%; " {{$disabledUser}}>
                    <option value="">Select</option>
                    <option value="Private" {{ ($previousQualification->SSC_degree_type == 'Private' || old('SSC_degree_type') == 'Private') ? 'selected' : ''}}>Private</option>
                    <option value="Distance Learning" {{ ($previousQualification->SSC_degree_type == 'Distance Learning' || old('SSC_degree_type') == 'Distance Learning') ? 'selected' : ''}}>Distance Learning</option>
                    <option value="Online/Virtual" {{ ($previousQualification->SSC_degree_type == 'Online/Virtual' || old('SSC_degree_type') == 'Online/Virtual') ? 'selected' : ''}}>Online/Virtual</option>
                    <option value="Regular" {{ ($previousQualification->SSC_degree_type == 'Regular' || old('SSC_degree_type') == 'Regular') ? 'selected' : ''}}>Regular</option>
                  </select>
                </div>
                <!-- /.form-group -->
              </div>                         
              <!-- /.col -->
            </div>
           <!-- /.row -->
           <div class="row">
            <div class="col-md-3">
              <div class="form-group">
                <label>Final Transcript <span class="img-upload-cls">File-Format[jpeg/png/jpg/gif/bmp/tiff] only with Max Size (4MB) [Required]*</span></label><br />
                <span class="ok">
                  <b>
                  <?php 
                  if($previousQualification->matric_last_result){
                    echo '<a target="_blank" href="'.asset("storage/".$previousQualification->matric_last_result).'">Uploaded</a>';
                  }
                  ?>
                  </b>
                </span>
                <input type="file" class="form-control" id="matric_last_result" name="matric_last_result">
              </div>
            </div>
           </div>
          </div>
          <!-- /.box-body -->
          <div class="box-header box box-success">
            <h3 class="box-title">HSSC /A-Level</h3>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <div class="row">
              <div class="col-md-3">
                <div class="form-group">
                  <label class="req-rd">Certificate/Degree</label>
                  @php
                  $collection = collect($hsscDegree);
                  $filteredItems = $collection->where('title', $previousQualification->HSC_Title);
                  if($filteredItems->count() > 0){
                    $val = '';
                  }else{
                    $val = $previousQualification->HSC_Title;
                  }
                  if($previousQualification->HSC_Title == ''){
                    $val = '';
                  }
                  @endphp
                  <select class="form-control sh-hd-ot" id="HSC_Title_d" name="HSC_Title_d" style="width: 100%; " {{$disabledUser}}>
                    <option value="">Select</option>
                    @foreach ( $hsscDegree as $n )
                      <option value="{{$n->title}}" {{ ($previousQualification->HSC_Title == $n->title || old('HSC_Title_d') == $n->title) ? 'selected' : ''}}>{{$n->title}}</option>
                    @endforeach
                  </select>
                  <input type="text" class="form-control ext-hide" value="{{$val ?: old('HSC_Title_t')}}" id="HSC_Title_t" name="HSC_Title_t"   {{(old('HSC_Title_d') == 'Other')? 'style=display:block': ''}}>
                </div>
                <!-- /.form-group -->
              </div>
              <!-- /.col -->
              <div class="col-md-3">
                <div class="form-group">
                  <label class="req-rd">Board/University</label>
                  @php
                  $collection = collect($sscInstitute);
                  $filteredItems = $collection->where('instName', $previousQualification->HSC_From);
                  if($filteredItems->count() > 0){
                    $val = '';
                  }else{
                    $val = $previousQualification->HSC_From;
                  }
                  if($previousQualification->HSC_From == ''){
                    $val = '';
                  }
                  @endphp
                  <select class="form-control sh-hd-ot" id="HSC_From_d" name="HSC_From_d" style="width: 100%; " {{$disabledUser}}>
                    <option value="">Select</option>
                    @foreach ( $sscInstitute as $n )
                      <option value="{{$n->instName}}" {{ ($previousQualification->HSC_From == $n->instName || old('HSC_From_d') == $n->instName) ? 'selected' : ''}}>{{$n->instName}}</option>
                    @endforeach
                  </select>
                  <input type="text" class="form-control ext-hide" value="{{$val ?: old('HSC_From_t')}}" id="HSC_From_t" name="HSC_From_t"  {{(old('HSC_From_d') == 'Other Boards')? 'style=display:block': ''}}>
                </div>
                <!-- /.form-group -->
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label class="req-rd">Group</label>
                  @php
                  $collection = collect($hsscGroup);
                  $filteredItems = $collection->where('subjects', $previousQualification->HSC_Subject);
                  if($filteredItems->count() > 0){
                    $val = '';
                  }else{
                    $val = $previousQualification->HSC_Subject;
                  }
                  if($previousQualification->HSC_Subject == ''){
                    $val = '';
                  }
                  @endphp
                  <select class="form-control sh-hd-ot" id="HSC_Subject_d" name="HSC_Subject_d" style="width: 100%; " {{$disabledUser}}>
                    <option value="">Select</option>
                    @foreach ( $hsscGroup as $n )
                      <option value="{{$n->subjects}}" {{ ($previousQualification->HSC_Subject == $n->subjects || old('HSC_Subject_d') == $n->subjects) ? 'selected' : ''}}>{{$n->subjects}}</option>
                    @endforeach
                  </select>
                  <input type="text" class="form-control ext-hide" value="{{$val  ?: old('HSC_Subject_t')}}" id="HSC_Subject_t" name="HSC_Subject_t"  {{(old('HSC_Subject_d') == 'Other')? 'style=display:block': ''}}>
                </div>
                <!-- /.form-group -->
              </div>   
              <div class="col-md-3">
                <div class="form-group">
                  <label class="req-rd">Completion Year</label>
                  <select class="form-control sh-hd-ot" id="HSC_Year" name="HSC_Year" required style="width: 100%; " {{$disabledUser}}>
                    <option value="">Select</option>
                    @php
                    $i=date('Y')-70;    
                    for($i; $i<= date('Y'); $i++){
                      if($previousQualification->HSC_Year == $i || old('HSC_Year') == $i){
                        $sel = "selected";
                      }else{
                        $sel = '';
                      }
                      echo '<option value="'.$i.'" '.$sel.'>'.$i.'</option>';
                    }
                    @endphp
                  </select>
                </div>
              </div>              
              <!-- /.col -->
            </div>
           <!-- /.row -->
           <div class="row">
              <div class="col-md-3">
                  <div class="form-group">
                    <label class="req-rd">Degree Type</label>
                    <select class="form-control sh-hd-ot" id="HSC_degree_type" required name="HSC_degree_type" style="width: 100%; " {{$disabledUser}}>
                      <option value="">Select</option>
                      <option value="Private" {{ ($previousQualification->HSC_degree_type == 'Private' || old('HSC_degree_type') == 'Private') ? 'selected' : ''}}>Private</option>
                      <option value="Distance Learning" {{ ($previousQualification->HSC_degree_type == 'Distance Learning' || old('HSC_degree_type') == 'Distance Learning') ? 'selected' : ''}}>Distance Learning</option>
                      <option value="Online/Virtual" {{ ($previousQualification->HSC_degree_type == 'Online/Virtual' || old('HSC_degree_type') == 'Online/Virtual') ? 'selected' : ''}}>Online/Virtual</option>
                      <option value="Regular" {{ ($previousQualification->HSC_degree_type == 'Regular' || old('HSC_degree_type') == 'Regular') ? 'selected' : ''}}>Regular</option>
                    </select>
                  </div>
                  <!-- /.form-group -->
                </div> 
           </div>
           @php
           $combined_check='';//$result_await='';
           if($previousQualification->HSSC_Total != '' && $previousQualification->HSSC_Composite != ''){
             $combined_check = 'checked';
           }
           @endphp
           <?php 
           /*
           <div class="row">
              <div class="col-md-12">
                  <h5>
                      <label>
                        <input type="checkbox" name="combined_hssc" value="1" class="combined_checkbox" {{$combined_check}} {{$disabledUser}}>
                        Combined HSSC
                      </label>
                    </h5>
              </div> 
           </div>
           */
           ?>
           <div class="row total_hssc_marks">
            <div class="col-md-4">
              <div class="form-group">
                <label class="req-rd">Total Marks</label>
                <input type="number" min="1" id="HSSC_Total" value="{{$previousQualification->HSSC_Total ?? old('HSSC_Total')}}" onBlur="checkMarks('HSSC_Total','HSSC_Composite','hssc_1')" name="HSSC_Total" class="form-control" {{$disabledUser}}>
              </div>
              <!-- /.form-group -->
            </div>
            <!-- /.col -->
            <div class="col-md-4">
              <div class="form-group">
                <label class="req-rd">Obatined Marks</label>
                <input type="number" min="1" id="HSSC_Composite" value="{{$previousQualification->HSSC_Composite ?? old('HSSC_Composite')}}" onBlur="checkMarks('HSSC_Total','HSSC_Composite','hssc_1')" name="HSSC_Composite" class="form-control" {{$disabledUser}}>
              </div>
              <!-- /.form-group -->
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label>HSSC Percentage</label><br />
                <label id="hssc_1" class="">   
                  @php
                    if($previousQualification->HSSC_Total != '' && $previousQualification->HSSC_Composite != ''){
                      $marks = bcdiv(($previousQualification->HSSC_Composite/$previousQualification->HSSC_Total*100), 1, 2); 
                      if($marks > 49){
                        echo '<span class="ok">'.$marks.' %</span>';
                      }else{
                        echo '<span class="not-ok">'.$marks.' %</span>';
                      }
                  } 
                 @endphp  
                </label>
              </div>
              <!-- /.form-group -->
            </div>              
            <!-- /.col -->
          </div>
         <!-- /.row -->
           <div class="row separate_hssc_marks inresult_waiting_part_1">
            <div class="col-md-4">
              <div class="form-group">
                <label class="req-rd">Total Marks (Part-I)</label>
                <input type="number" min="1" id="HSSC_Total1" value="{{$previousQualification->HSSC_Total1 ?? old('HSSC_Total1')}}" onBlur="checkMarks('HSSC_Total1','HSSC1','hssc_2')" name="HSSC_Total1" class="form-control" {{$disabledUser}}>
              </div>
              <!-- /.form-group -->
            </div>
            <!-- /.col -->
            <div class="col-md-4">
              <div class="form-group">
                <label class="req-rd">Obatined Marks (Part-I)</label>
                <input type="number" min="1" id="HSSC1" value="{{$previousQualification->HSSC1 ?? old('HSSC1')}}" onBlur="checkMarks('HSSC_Total1','HSSC1','hssc_2')" name="HSSC1" class="form-control" {{$disabledUser}}>
              </div>
              <!-- /.form-group -->
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label>HSSC-I Percentage</label><br />
                <label id="hssc_2" class="">
                  @php
                    if($previousQualification->HSSC_Total1 != '' && $previousQualification->HSSC1 != ''){
                      $marks = bcdiv(($previousQualification->HSSC1/$previousQualification->HSSC_Total1*100), 1, 2); 
                      if($marks > 49){
                        echo '<span class="ok">'.$marks.' %</span>';
                      }else{
                        echo '<span class="not-ok">'.$marks.' %</span>';
                      }
                  } 
                 @endphp  
                </label>
              </div>
              <!-- /.form-group -->
            </div>              
            <!-- /.col -->
            </div>
         <!-- /.row -->
        <div class="row separate_hssc_marks inresult_waiting">
          <div class="col-md-4">
            <div class="form-group">
              <label class="req-rd">Total Marks (Part-II)[Only Part-II not combined]</label>
              <input type="number" min="1" id="HSSC_Total2" value="{{$previousQualification->HSSC_Total2 ?? old('HSSC_Total2')}}" onBlur="checkMarks('HSSC_Total2','HSSC2','hssc_3')" name="HSSC_Total2" class="form-control" {{$disabledUser}}>
            </div>
            <!-- /.form-group -->
          </div>
          <!-- /.col -->
          <div class="col-md-4">
            <div class="form-group">
              <label class="req-rd">Obatined Marks (Part-II)[Only Part-II not combined]</label>
              <input type="number" min="1" id="HSSC2" value="{{$previousQualification->HSSC2 ?? old('HSSC2')}}" onBlur="checkMarks('HSSC_Total2','HSSC2','hssc_3')"  name="HSSC2" class="form-control" {{$disabledUser}}>
            </div>
            <!-- /.form-group -->
          </div>
          <div class="col-md-4">
            <div class="form-group">
              <label>HSSC-II Percentage</label><br />
              <label id="hssc_3" class="">
                @php
                  if($previousQualification->HSSC_Total2 != '' && $previousQualification->HSSC2 != ''){
                    $marks = bcdiv(($previousQualification->HSSC2/$previousQualification->HSSC_Total2*100), 1, 2); 
                    if($marks > 49){
                      echo '<span class="ok">'.$marks.' %</span>';
                    }else{
                      echo '<span class="not-ok">'.$marks.' %</span>';
                    }
                } 
               @endphp 
              </label>
            </div>
            <!-- /.form-group -->
          </div>              
          <!-- /.col -->
        </div>
       <!-- /.row -->
       <div class="row">
        <div class="col-md-4">
          <div class="form-group">
            <label>Final Transcript <span class="img-upload-cls">File-Format[jpeg/png/jpg/gif/bmp/tiff] only with Max Size (4MB) [Optional]</span></label><br />
            <span class="ok">
              <b>
              {{-- {{ ($previousQualification->bs_last_result) ? 'Uploaded' : ''}} --}}
              <?php 
              if($previousQualification->hssc_last_result){
                echo '<a target="_blank" href="'.asset("storage/".$previousQualification->hssc_last_result).'">Uploaded</a>';
              }
              ?>
              </b>
            </span>
            <input type="file" class="form-control" id="hssc_last_result" name="hssc_last_result">
          </div>
        </div>
        <div class="col-md-4">
            <h5>
                <label>
                  <input type="checkbox" name="resultAwaiting" {{ ($previousQualification->resultAwaiting == 1) ? 'checked' : ''}} id="resultAwaiting" value="1" class="hssc_result_waiting" {{$disabledUser}}>
                  Result Waiting
                </label>
              </h5>
        </div> 
      </div>
        </div>


        <div class="box-header box box-danger">
          <h3 class="box-title">BS Engineering Programs Only</h3>
        </div>

        <div class="box-body">
          <div class="row">

            <div class="col-md-3">
              <div class="form-group">
                <label>Entry Test Centers</label>
                <select class="form-control sh-hd-ot" id="entry_test_center" name="entry_test_center" style="width: 100%; " {{$disabledUser}}>
                  <option value="">Select</option>
                  <option value="UET Lahore" {{ ($previousQualification->entry_test_center == 'UET Lahore' || old('entry_test_center') == 'UET Lahore') ? 'selected' : ''}}>UET Lahore</option>
                  <option value="UET Peshawar" {{ ($previousQualification->entry_test_center == 'UET Peshawar' || old('entry_test_center') == 'UET Peshawar') ? 'selected' : ''}}>UET Peshawar</option>
                  <option value="NED-UET & MUET Jamshoro" {{ ($previousQualification->entry_test_center == 'NED-UET & MUET Jamshoro' || old('entry_test_center') == 'NED-UET & MUET Jamshoro') ? 'selected' : ''}}>NED-UET & MUET Jamshoro</option>
                  <option value="NUST Islamabad" {{ ($previousQualification->entry_test_center == 'NUST Islamabad' || old('entry_test_center') == 'NUST Islamabad') ? 'selected' : ''}}>NUST Islamabad</option>
                  <option value="ETEA/NTS/PEC/HEC/USAT-E or any other" {{ ($previousQualification->entry_test_center == 'ETEA/NTS/PEC/HEC/USAT-E or any other' || old('entry_test_center') == 'ETEA/NTS/PEC/HEC/USAT-E or any other') ? 'selected' : ''}}>ETEA/NTS/PEC/HEC/USAT-E or any other</option>
                </select>
              </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label>Total Marks</label> 
                    <input type="number"  class="form-control" id="entry_test_total_marks" name="entry_test_total_marks" onblur="checkMarks('entry_test_total_marks','entry_test_obtained_marks','ff')" value="{{$previousQualification->entry_test_total_marks ?? old('entry_test_total_marks')}}" {{$disabledUser}}>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label>Obtained Marks</label> 
                    <input type="number"  class="form-control" id="entry_test_obtained_marks" name="entry_test_obtained_marks" onblur="checkMarks('entry_test_total_marks','entry_test_obtained_marks','ff')" value="{{$previousQualification->entry_test_obtained_marks ?? old('entry_test_obtained_marks')}}" {{$disabledUser}}>
                </div>
            </div>
            <div class="col-md-3">
              <div class="form-group">
                  <label>Are you willing to appear for IIU entrance test ?</label><br />
                  <input type="radio" name="iiu_test_appear_willingness" {{ ($previousQualification->iiu_test_appear_willingness == '1') ? 'checked' : ''}} value="1" class="flat-red form-control" {{$disabledUser}}> Yes
                  <input type="radio" name="iiu_test_appear_willingness" {{ ($previousQualification->iiu_test_appear_willingness == '0') ? 'checked' : ''}} value="0" class="flat-red form-control" {{$disabledUser}}> No
              </div>
            </div>

          </div>
          <div class="row">
            <div class="col-md-3">
              <div class="form-group">
                <label>Result card/sheet Transcript <span class="img-upload-cls">File-Format[jpeg/png/jpg/gif/bmp/tiff] only with Max Size (4MB) [Required]*</span></label><br />
                <span class="ok">
                  <b>
                  <?php 
                  if($previousQualification->entry_test_sheet){
                    echo '<a target="_blank" href="'.asset("storage/".$previousQualification->entry_test_sheet).'">Uploaded</a>';
                  }
                  ?>
                  </b>
                </span>
                <input type="file" class="form-control" id="entry_test_sheet" name="entry_test_sheet">
              </div>
            </div>
          </div>
        </div>

        <div class="box-header box box-info">
          <h3 class="box-title">Law Admission Test Score</h3>
        </div>
        {{-- Code for Law admission score starts --}}
        {{-- <div class="box-body">
          <div class="row">
            <div class="col-md-3">
                <div class="form-group">
                    <label>Total Marks</label> 
                    <input type="number"  class="form-control" id="llb_total_marks" name="llb_total_marks" onblur="checkMarks('llb_total_marks','llb_obtained_marks','ff')" value="{{$previousQualification->llb_total_marks ?? old('llb_total_marks')}}" >
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label>Obtained Marks</label> 
                    <input type="number"  class="form-control" id="llb_obtained_marks" name="llb_obtained_marks" onblur="checkMarks('llb_total_marks','llb_obtained_marks','ff')" value="{{$previousQualification->llb_obtained_marks ?? old('llb_obtained_marks')}}">
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label>Completion Date</label>
                    <input type="text" class="form-control fltpckr1" value="@if($previousQualification->law_completion_date){{(\Carbon\Carbon::parse($previousQualification->law_completion_date)->format('Y-m-d'))}}@endif @if(old('law_completion_date')){{(\Carbon\Carbon::parse(old('law_completion_date'))->format('Y-m-d'))}}@endif" name="law_completion_date">
                </div>
            </div>
            <div class="col-md-3">
              <div class="form-group">
                  <label>LAT Upcoming Test Applied ?</label><br />
                  <input type="radio" name="lat_upcoming" {{ ($previousQualification->lat_upcoming == '1') ? 'checked' : ''}} value="1" class="flat-red form-control"> Yes
                  <input type="radio" name="lat_upcoming" {{ ($previousQualification->lat_upcoming == '0') ? 'checked' : ''}} value="0" class="flat-red form-control"> No
              </div>
          </div>
          </div>
        </div> --}}
        {{-- Code for Law admission score ends --}}
        <div class="box-body">
          <div class="row">
            <div class="col-md-3">
                <div class="form-group">
                    <label>Total Marks</label> 
                    <input type="number"  class="form-control" id="llb_total_marks" name="llb_total_marks" onblur="checkMarks('llb_total_marks','llb_obtained_marks','ff')" value="{{$previousQualification->llb_total_marks ?? old('llb_total_marks')}}" {{$disabledUser}}>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label>Obtained Marks</label> 
                    <input type="number"  class="form-control" id="llb_obtained_marks" name="llb_obtained_marks" onblur="checkMarks('llb_total_marks','llb_obtained_marks','ff')" value="{{$previousQualification->llb_obtained_marks ?? old('llb_obtained_marks')}}" {{$disabledUser}}>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label>Completion Date</label>
                    <input type="text" class="form-control fltpckr1" value="@if($previousQualification->law_completion_date){{(\Carbon\Carbon::parse($previousQualification->law_completion_date)->format('Y-m-d'))}}@endif @if(old('law_completion_date')){{(\Carbon\Carbon::parse(old('law_completion_date'))->format('Y-m-d'))}}@endif" name="law_completion_date" {{ $disabledUser1 }}>
                </div>
            </div>
            <div class="col-md-3">
              <div class="form-group">
                  <label>LAT Upcoming Test Applied ?</label><br />
                  <input type="radio" name="lat_upcoming" {{ ($previousQualification->lat_upcoming == '1') ? 'checked' : ''}} value="1" class="flat-red form-control" {{$disabledUser}}> Yes
                  <input type="radio" name="lat_upcoming" {{ ($previousQualification->lat_upcoming == '0') ? 'checked' : ''}} value="0" class="flat-red form-control" {{$disabledUser}}> No
              </div>
            </div>
          </div>
        </div>

        <div class="box-header box box-warning 14_div">
            <h3 class="box-title">Undergraduate(14-Year) Optional</h3>
        </div>
        <div class="box-body 14_div">
            <div class="row">
                <div class="col-md-3">
                  <div class="form-group">
                    <label>Certificate/Degree</label> 
                    @php
                    $collection = collect($bscDegree);
                    $filteredItems = $collection->where('title', $previousQualification->BA_Bsc_Title);
                    if($filteredItems->count() > 0){
                      $val = '';
                    }else{
                      $val = $previousQualification->BA_Bsc_Title;
                    }
                    if($previousQualification->BA_Bsc_Title == ''){
                      $val = '';
                    }
                    @endphp
                    <select class="form-control sh-hd-ot" id="BA_Bsc_Title_d" name="BA_Bsc_Title_d" style="width: 100%;   " {{$disabledUser}}>
                      <option value="">Select</option>
                      @foreach ( $bscDegree as $n )
                        <option value="{{$n->title}}" {{($previousQualification->BA_Bsc_Title == $n->title || old('BA_Bsc_Title_d') == $n->title) ? 'selected' : ''}}>{{$n->title}}</option>
                      @endforeach
                    </select>
                    <input type="text" class="form-control ext-hide" value="{{$val ?: old('BA_Bsc_Title_t')}}" id="BA_Bsc_Title_t" name="BA_Bsc_Title_t"  {{(old('BA_Bsc_Title_d') == 'Other')? 'style=display:block': ''}}>
                  </div>
                  <!-- /.form-group -->
                </div>
                <!-- /.col -->
                <div class="col-md-3">
                  <div class="form-group">
                    <label>Board/University</label>
                    @php
                    $collection = collect($bscInstitute);
                    $filteredItems = $collection->where('instName', $previousQualification->BA_Bsc_From);
                    if($filteredItems->count() > 0){
                      $val = '';
                    }else{
                      $val = $previousQualification->BA_Bsc_From;
                    }
                    if($previousQualification->BA_Bsc_From == ''){
                      $val = '';
                    }
                    @endphp
                    <select class="form-control sh-hd-ot" id="BA_Bsc_From_d" name="BA_Bsc_From_d" style="width: 100%;   " {{$disabledUser}}>
                      <option value="">Select</option>
                      @foreach ( $bscInstitute as $n )
                        <option value="{{$n->instName}}" {{($previousQualification->BA_Bsc_From == $n->instName || old('BA_Bsc_From_d') == $n->instName) ? 'selected' : ''}}>{{$n->instName}}</option>
                      @endforeach
                    </select>
                    <input type="text" class="form-control ext-hide" value="{{$val ?: old('BA_Bsc_From_t')}}" id="BA_Bsc_From_t" name="BA_Bsc_From_t"    {{(old('BA_Bsc_From_d') == 'Other Universities')? 'style=display:block': ''}}>
                  </div>
                  <!-- /.form-group -->
                </div>
                <div class="col-md-3">
                  <div class="form-group">
                    <label>Degree Title</label>
                   <input type="text" id="BA_Bsc_Subject" value="{{$previousQualification->BA_Bsc_Subject ?? old('BA_Bsc_Subject')}}" name="BA_Bsc_Subject" class="form-control" {{$disabledUser}}>
                  </div>
                  <!-- /.form-group -->
                </div>              
                <!-- /.col -->
                <div class="col-md-3">
                    <div class="form-group">
                      <label>Subjects</label>
                     <input type="text" id="BA_Bsc_Specialization" value="{{$previousQualification->BA_Bsc_Specialization ?? old('BA_Bsc_Specialization')}}" name="BA_Bsc_Specialization" class="form-control" {{$disabledUser}}>
                    </div>
                    <!-- /.form-group -->
                  </div>              
                  <!-- /.col -->
              
              
                </div>
             <!-- /.row -->
             <div class="row">
                <div class="col-md-3">
                  <div class="form-group rm-aster">
                    <label>Total Marks</label>
                    <input type="number" min="1" id="BA_Bsc_Total" value="{{$previousQualification->BA_Bsc_Total ?? old('BA_Bsc_Total')}}" onBlur="checkMarks('BA_Bsc_Total','BA_Bsc','ug_1')" name="BA_Bsc_Total" class="form-control" {{$disabledUser}}>
                  </div>
                  <!-- /.form-group -->
                </div>
                <!-- /.col -->
                <div class="col-md-3">
                  <div class="form-group rm-aster">
                    <label>Obatined Marks &nbsp;
                      <span id="ug_1" class="">
                          @php
                          if($previousQualification->BA_Bsc_Total != '' && $previousQualification->BA_Bsc != ''){
                            $marks = bcdiv(($previousQualification->BA_Bsc/$previousQualification->BA_Bsc_Total*100), 1, 2); 
                            if($marks > 49){
                              echo '<span class="ok">'.$marks.' %</span>';
                            }else{
                              echo '<span class="not-ok">'.$marks.' %</span>';
                            }
                        } 
                       @endphp 
                      </span>
                    </label>
                    <input type="number" min="1" step=".01" id="BA_Bsc" value="{{$previousQualification->BA_Bsc ?? old('BA_Bsc')}}" onBlur="checkMarks('BA_Bsc_Total','BA_Bsc','ug_1')" name="BA_Bsc" class="form-control" {{$disabledUser}}>
                  </div>
                  <!-- /.form-group -->
                </div>
                {{-- <div class="col-md-3">
                  <div class="form-group">
                    <label>Percentage</label><br />
                    <label id="ug_1" class="">
                      @php
                        if($previousQualification->BA_Bsc_Total != '' && $previousQualification->BA_Bsc != ''){
                          $marks = bcdiv($previousQualification->BA_Bsc/$previousQualification->BA_Bsc_Total*100); 
                          if($marks > 49){
                            echo '<span class="ok">'.$marks.' %</span>';
                          }else{
                            echo '<span class="not-ok">'.$marks.' %</span>';
                          }
                      } 
                     @endphp 
                    </label>
                  </div>
                  <!-- /.form-group -->
                </div>  --}}
                <div class="col-md-3">
                  <div class="form-group">
                    <label>Completion Year</label>
                    <select class="form-control sh-hd-ot" id="BA_Bsc_Year" name="BA_Bsc_Year" style="width: 100%; " {{$disabledUser}}>
                      <option value="">Select</option>
                      @php
                      $i=date('Y')-70;    
                      for($i; $i<= date('Y'); $i++){
                        if($previousQualification->BA_Bsc_Year == $i || old('BA_Bsc_Year') == $i){
                          $sel = "selected";
                        }else{
                          $sel = '';
                        }
                        echo '<option value="'.$i.'" '.$sel.'>'.$i.'</option>';
                      }
                      @endphp
                    </select>
                  </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                      <label>Degree Type</label>
                      <select class="form-control sh-hd-ot" id="BA_Bsc_degree_type" name="BA_Bsc_degree_type" style="width: 100%; " {{$disabledUser}}>
                        <option value="">Select</option>
                        <option value="Private" {{ ($previousQualification->BA_Bsc_degree_type == 'Private' || old('BA_Bsc_degree_type') == 'Private') ? 'selected' : ''}}>Private</option>
                        <option value="Distance Learning" {{ ($previousQualification->BA_Bsc_degree_type == 'Distance Learning' || old('BA_Bsc_degree_type') == 'Distance Learning') ? 'selected' : ''}}>Distance Learning</option>
                        <option value="Online/Virtual" {{ ($previousQualification->BA_Bsc_degree_type == 'Online/Virtual' || old('BA_Bsc_degree_type') == 'Online/Virtual') ? 'selected' : ''}}>Online/Virtual</option>
                        <option value="Regular" {{ ($previousQualification->BA_Bsc_degree_type == 'Regular' || old('BA_Bsc_degree_type') == 'Regular') ? 'selected' : ''}}>Regular</option>
                      </select>
                    </div>
                    <!-- /.form-group -->
                  </div> 
              </div>
              <div class="row">
                <div class="col-md-12">
                    <h5>
                        <label>
                          <input type="checkbox" name="resultwaitbsc" {{ ($previousQualification->resultwaitbsc == 1 || old('resultwaitbsc') == 1) ? 'checked' : ''}}  id="resultwaitbsc" value="1" class="minimal" {{$disabledUser}}>
                          Result Waiting
                        </label>
                      </h5>
                </div> 
              </div>
        
        </div>


        <div class="box-header box box-danger ma_div">
            <h3 class="box-title">Undergraduate(MA/MSC) Optional</h3>
        </div>
        <div class="box-body ma_div">
            <div class="row">
                <div class="col-md-3">
                  <div class="form-group">
                    <label>Certificate/Degree</label>
                    @php
                    $collection = collect($mscDegree);
                    $filteredItems = $collection->where('title', $previousQualification->MSc_Title);
                    if($filteredItems->count() > 0){
                      $val = '';
                    }else{
                      $val = $previousQualification->MSc_Title;
                    }
                    if($previousQualification->MSc_Title == ''){
                      $val = '';
                    }
                    @endphp
                    <select class="form-control sh-hd-ot" id="MSc_Title_d" name="MSc_Title_d" style="width: 100%;   " {{$disabledUser}}>
                      <option value="">Select</option>
                      @foreach ( $mscDegree as $n )
                        <option value="{{$n->title}}" {{($previousQualification->MSc_Title == $n->title || old('MSc_Title_d') == $n->title) ? 'selected' : ''}}>{{$n->title}}</option>
                      @endforeach
                    </select>
                    <input type="text" class="form-control ext-hide" value="{{$val ?: old('MSc_Title_t')}}" id="MSc_Title_t" name="MSc_Title_t"    {{(old('MSc_Title_d') == 'Other')? 'style=display:block': ''}}>
                  </div>
                  <!-- /.form-group -->
                </div>
                <!-- /.col -->
                <div class="col-md-3">
                  <div class="form-group">
                    <label>Board/University</label>
                    @php
                    $collection = collect($bscInstitute);
                    $filteredItems = $collection->where('instName', $previousQualification->MSc_From);
                    if($filteredItems->count() > 0){
                      $val = '';
                    }else{
                      $val = $previousQualification->MSc_From;
                    }
                    if($previousQualification->MSc_From == ''){
                      $val = '';
                    }
                    @endphp
                    <select class="form-control sh-hd-ot" id="MSc_From_d" name="MSc_From_d" style="width: 100%;   " {{$disabledUser}}>
                      <option value="">Select</option>
                      @foreach ( $bscInstitute as $n )
                        <option value="{{$n->instName}}" {{($previousQualification->MSc_From == $n->instName || old('MSc_From_d') == $n->instName) ? 'selected' : ''}}>{{$n->instName}}</option>
                      @endforeach
                    </select>
                    <input type="text" class="form-control ext-hide"  value="{{$val ?: old('MSc_From_t')}}" id="MSc_From_t" name="MSc_From_t"   {{(old('MSc_From_d') == 'Other Universities')? 'style=display:block': ''}} >
                  </div>
                  <!-- /.form-group -->
                </div>
                <div class="col-md-3">
                  <div class="form-group">
                    <label>Degree Title</label>
                   <input type="text" id="MSc_Subject" value="{{$previousQualification->MSc_Subject ?? old('MSc_Subject')}}" name="MSc_Subject" class="form-control" {{$disabledUser}}>
                  </div>
                  <!-- /.form-group -->
                </div>              
                <!-- /.col -->
                <div class="col-md-3">
                    <div class="form-group">
                      <label>Subjects</label>
                     <input type="text" id="MSc_Specialization" value="{{$previousQualification->MSc_Specialization ?? old('MSc_Specialization')}}" name="MSc_Specialization" class="form-control" {{$disabledUser}}>
                    </div>
                    <!-- /.form-group -->
                  </div>              
                  <!-- /.col -->
              
              
                </div>
             <!-- /.row -->
             <div class="row">
                <div class="col-md-3">
                  <div class="form-group">
                    <label>Total Marks</label>
                    <input type="number" min="1" id="MSc_Total" value="{{$previousQualification->MSc_Total ?? old('MSc_Total')}}" onBlur="checkMarks('MSc_Total','MSc','ma_1')" name="MSc_Total" class="form-control" {{$disabledUser}}>
                  </div>
                  <!-- /.form-group -->
                </div>
                <!-- /.col -->
                <div class="col-md-3">
                  <div class="form-group">
                    <label>Obatined Marks
                      <span id="ma_1" class="">
                        @php
                          if($previousQualification->MSc_Total != '' && $previousQualification->MSc != ''){
                            $marks = bcdiv(($previousQualification->MSc/$previousQualification->MSc_Total*100), 1, 2); 
                            if($marks > 49){
                              echo '<span class="ok">'.$marks.' %</span>';
                            }else{
                              echo '<span class="not-ok">'.$marks.' %</span>';
                            }
                          } 
                        @endphp 
                      </span>
                    </label>
                    <input type="number" min="1" step=".01" id="MSc" value="{{$previousQualification->MSc ?? old('MSc')}}" onBlur="checkMarks('MSc_Total','MSc','ma_1')" name="MSc" class="form-control" {{$disabledUser}}>
                  </div>
                  <!-- /.form-group -->
                </div>
                {{-- <div class="col-md-3">
                  <div class="form-group">
                    <label>Percentage</label><br />
                    <label id="ma_1" class="">
                      @php
                        if($previousQualification->MSc_Total != '' && $previousQualification->MSc != ''){
                          $marks = bcdiv($previousQualification->MSc/$previousQualification->MSc_Total*100); 
                          if($marks > 49){
                            echo '<span class="ok">'.$marks.' %</span>';
                          }else{
                            echo '<span class="not-ok">'.$marks.' %</span>';
                          }
                      } 
                     @endphp 
                    </label>
                  </div>
                  <!-- /.form-group -->
                </div>  --}}
                <div class="col-md-3">
                  <div class="form-group">
                    <label>Completion Year</label>
                    <select class="form-control sh-hd-ot" id="MSc_Year" name="MSc_Year" style="width: 100%; " {{$disabledUser}}>
                      <option value="">Select</option>
                      @php
                      $i=date('Y')-70;    
                      for($i; $i<= date('Y'); $i++){
                        if($previousQualification->MSc_Year == $i || old('MSc_Year') == $i){
                          $sel = "selected";
                        }else{
                          $sel = '';
                        }
                        echo '<option value="'.$i.'" '.$sel.'>'.$i.'</option>';
                      }
                      @endphp
                    </select>
                  </div>
                </div>
                    <div class="col-md-3">
                      <div class="form-group">
                        <label>Degree Type</label>
                        <select class="form-control sh-hd-ot" id="MSc_degree_type" name="MSc_degree_type" style="width: 100%; " {{$disabledUser}}>
                          <option value="">Select</option>
                          <option value="Private" {{ ($previousQualification->MSc_degree_type == 'Private' || old('MSc_degree_type') == 'Private') ? 'selected' : ''}}>Private</option>
                          <option value="Distance Learning" {{ ($previousQualification->MSc_degree_type == 'Distance Learning' || old('MSc_degree_type') == 'Distance Learning') ? 'selected' : ''}}>Distance Learning</option>
                          <option value="Online/Virtual" {{ ($previousQualification->MSc_degree_type == 'Online/Virtual' || old('MSc_degree_type') == 'Online/Virtual') ? 'selected' : ''}}>Online/Virtual</option>
                          <option value="Regular" {{ ($previousQualification->MSc_degree_type == 'Regular' || old('MSc_degree_type') == 'Regular') ? 'selected' : ''}}>Regular</option>
                        </select>
                      </div>
                      <!-- /.form-group -->
                    </div> 
              </div>
        </div>

              {{-- Dev Mannan: GAt/GRE started --}}
              <div class="box-header box box-danger 16_div">
                <h3 class="box-title">GAT/GRE Test Marks</h3>
              </div>
              <div class="box-body 16_div">
                <div class="row">
      
                  <div class="col-md-3">
                    <div class="form-group">
                      <label>Type of Test</label>
                      <select class="form-control" id="gat_gre_test_type" name="gat_gre_test_type" style="width: 100%; " {{$disabledUser}}>
                        <option value="">Select</option>
                        <option value="GAT" {{ ($previousQualification->gat_gre_test_type == 'GAT' || old('gat_gre_test_type') == 'GAT') ? 'selected' : ''}}>GAT</option>
                        <option value="GRE" {{ ($previousQualification->gat_gre_test_type == 'GRE' || old('gat_gre_test_type') == 'GRE') ? 'selected' : ''}}>GRE</option>
                      </select>
                    </div>
                  </div>
      
                  <div class="col-md-3">
                      <div class="form-group">
                          <label>Total Marks</label> 
                          <input type="number"  class="form-control" id="gat_gre_total_marks" name="gat_gre_total_marks" onblur="checkMarks('gat_gre_total_marks','gat_gre_obtained_marks','ff')" value="{{$previousQualification->gat_gre_total_marks ?? old('gat_gre_total_marks')}}" {{$disabledUser}}>
                      </div>
                  </div>
                  <div class="col-md-3">
                      <div class="form-group">
                          <label>Obtained Marks</label> 
                          <input type="number"  class="form-control" id="gat_gre_obtained_marks" name="gat_gre_obtained_marks" onblur="checkMarks('gat_gre_total_marks','gat_gre_obtained_marks','ff')" value="{{$previousQualification->gat_gre_obtained_marks ?? old('gat_gre_obtained_marks')}}" {{$disabledUser}}>
                      </div>
                  </div>
                  <div class="col-md-3">
                      <div class="form-group">
                          <label>Completion Date</label>
                          <input type="text" class="form-control fltpckr1" value="@if($previousQualification->gat_gre_completion_date){{(\Carbon\Carbon::parse($previousQualification->gat_gre_completion_date)->format('Y-m-d'))}}@endif @if(old('gat_gre_completion_date')){{(\Carbon\Carbon::parse(old('gat_gre_completion_date'))->format('Y-m-d'))}}@endif" name="gat_gre_completion_date" {{ $disabledUser1 }}>
                      </div>
                  </div>
                </div>
              </div>
              {{-- Dev Mannan: GAt/GRE ended --}}

        <div class="box-header box box-primary 16_div">
            <h3 class="box-title">Undergraduate(16-Year)</h3>
        </div>
        <div class="box-body 16_div">
            <div class="row">
                <div class="col-md-3">
                  <div class="form-group">
                    <label>Certificate/Degree</label>
                    @php
                    $collection = collect($bsDegree);
                    $filteredItems = $collection->where('title', $previousQualification->BS_Title);
                    if($filteredItems->count() > 0){
                      $val = '';
                    }else{
                      $val = $previousQualification->BS_Title;
                    }
                    if($previousQualification->BS_Title == ''){
                      $val = '';
                    }
                    @endphp
                    <select class="form-control sh-hd-ot" id="BS_Title_d" name="BS_Title_d" style="width: 100%;   " {{$disabledUser}}>
                      <option value="">Select</option>
                      @foreach ( $bsDegree as $n )
                        <option value="{{$n->title}}" {{($previousQualification->BS_Title == $n->title || old('BS_Title_d') == $n->title) ? 'selected' : ''}}>{{$n->title}}</option>
                      @endforeach
                    </select>
                    <input type="text" class="form-control ext-hide" value="{{$val ?: old('BS_Title_t')}}" id="BS_Title_t" name="BS_Title_t"   {{(old('BS_Title_d') == 'Other')? 'style=display:block': ''}} >
                  </div>
                  <!-- /.form-group -->
                </div>
                <!-- /.col -->
                <div class="col-md-3">
                  <div class="form-group">
                    <label>Board/University</label>
                    @php
                    $collection = collect($bscInstitute);
                    $filteredItems = $collection->where('instName', $previousQualification->BS_From);
                    if($filteredItems->count() > 0){
                      $val = '';
                    }else{
                      $val = $previousQualification->BS_From;
                    }
                    if($previousQualification->BS_From == ''){
                      $val = '';
                    }
                    @endphp
                    <select class="form-control sh-hd-ot" id="BS_From_d" name="BS_From_d" style="width: 100%;   " {{$disabledUser}}>
                      <option value="">Select</option>
                      @foreach ( $bscInstitute as $n )
                        <option value="{{$n->instName}}" {{($previousQualification->BS_From == $n->instName || old('BS_From_d') == $n->instName) ? 'selected' : ''}}>{{$n->instName}}</option>
                      @endforeach
                    </select>
                    <input type="text" class="form-control ext-hide" value="{{$val ?: old('BS_From_t')}}" id="BS_From_t" name="BS_From_t"    {{(old('BS_From_d') == 'Other Universities')? 'style=display:block': ''}}>
                  </div>
                  <!-- /.form-group -->
                </div>
                <div class="col-md-3">
                  <div class="form-group">
                    <label>Degree Title</label>
                   <input type="text" id="BS_Subject" value="{{$previousQualification->BS_Subject ?? old('BS_Subject')}}" name="BS_Subject" class="form-control" {{$disabledUser}}>
                  </div>
                  <!-- /.form-group -->
                </div>              
                <!-- /.col -->
                <div class="col-md-3">
                    <div class="form-group">
                      <label>Subjects</label>
                     <input type="text" id="BS_Specialization" value="{{$previousQualification->BS_Specialization  ?? old('BS_Specialization')}}" name="BS_Specialization" class="form-control" {{$disabledUser}}>
                    </div>
                    <!-- /.form-group -->
                  </div>              
                  <!-- /.col -->
              
              
                </div>
             <!-- /.row -->
             <div class="row">
                <div class="col-md-3">
                  <div class="form-group">
                    <label>Total Marks/Total CGPA</label>
                    <input type="number" min="1" id="BS_Total" value="{{$previousQualification->BS_Total ?? old('BS_Total')}}" onBlur="checkMarks('BS_Total','BS','ms_1')" name="BS_Total" class="form-control" {{$disabledUser}}>
                  </div>
                  <!-- /.form-group -->
                </div>
                <!-- /.col -->
                <div class="col-md-3">
                  <div class="form-group">
                    <label>Obtained Marks/Obtained CGPA</label>
                    <input type="number" min="1" step=".01" id="BS" value="{{$previousQualification->BS ?? old('BS')}}" onBlur="checkMarks('BS_Total','BS','ms_1')" name="BS" class="form-control" {{$disabledUser}}>
                  </div>
                  <!-- /.form-group -->
                </div>
                <div class="col-md-3" style="display:none;">
                  <div class="form-group">
                    <label>Percentage</label><br />
                    <label id="ms_1" class="">
                      @php
                        if($previousQualification->BS_Total != '' && $previousQualification->BS != ''){
                          $marks = bcdiv(($previousQualification->BS/$previousQualification->BS_Total*100),1, 2); 
                          if($marks > 49){
                            echo '<span class="ok">'.$marks.' %</span>';
                          }else{
                            echo '<span class="not-ok">'.$marks.' %</span>';
                          }
                      } 
                     @endphp
                    </label>
                  </div>
                  <!-- /.form-group -->
                </div> 
                <div class="col-md-3">
                  <div class="form-group">
                    <label>Completion Year</label>
                    <select class="form-control sh-hd-ot" id="BS_Year" name="BS_Year" style="width: 100%; " {{$disabledUser}}>
                      <option value="">Select</option>
                      @php
                      $i=date('Y')-70;    
                      for($i; $i<= date('Y'); $i++){
                        if($previousQualification->BS_Year == $i || old('BS_Year') == $i){
                          $sel = "selected";
                        }else{
                          $sel = '';
                        }
                        echo '<option value="'.$i.'" '.$sel.'>'.$i.'</option>';
                      }
                      @endphp
                    </select>
                  </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                      <label>Degree Type</label>
                      <select class="form-control sh-hd-ot" id="BS_degree_type" name="BS_degree_type" style="width: 100%; " {{$disabledUser}}>
                        <option value="">Select</option>
                        <option value="Private" {{ ($previousQualification->BS_degree_type == 'Private' || old('BS_degree_type') == 'Private') ? 'selected' : ''}}>Private</option>
                        <option value="Distance Learning" {{ ($previousQualification->BS_degree_type == 'Distance Learning' || old('BS_degree_type') == 'Distance Learning') ? 'selected' : ''}}>Distance Learning</option>
                        <option value="Online/Virtual" {{ ($previousQualification->BS_degree_type == 'Online/Virtual' || old('BS_degree_type') == 'Online/Virtual') ? 'selected' : ''}}>Online/Virtual</option>
                        <option value="Regular" {{ ($previousQualification->BS_degree_type == 'Regular' || old('BS_degree_type') == 'Regular') ? 'selected' : ''}}>Regular</option>
                      </select>
                    </div>
                    <!-- /.form-group -->
                  </div> 
              </div>
              <div class="row">
                <div class="col-md-3">
                  <div class="form-group">
                    <label>Final Transcript <span class="img-upload-cls">File-Format[jpeg/png/jpg/gif/bmp/tiff] only with Max Size (4MB) [Required]*</span></label><br />
                    <span class="ok">
                      <b>
                      {{-- {{ ($previousQualification->bs_last_result) ? 'Uploaded' : ''}} --}}
                      <?php 
                      if($previousQualification->bs_last_result){
                        echo '<a target="_blank" href="'.asset("storage/".$previousQualification->bs_last_result).'">Uploaded</a>';
                      }
                      ?>
                      </b>
                    </span>
                    <input type="file" class="form-control" id="bs_last_result" name="bs_last_result">
                  </div>
                </div>
                <div class=col-md-4>
                    <div class="form-group">
                        <label>Examination System</label><br />
                          <input type="radio" name="BS_Exam_System" {{ ($previousQualification->BS_Exam_System == 'Annual' || old('BS_Exam_System') == 'Annual') ? 'checked' : ''}} value="Annual" class="flat-red form-control" {{$disabledUser}}> Annual System
                          <input type="radio" name="BS_Exam_System" {{ ($previousQualification->BS_Exam_System == 'Semester' || old('BS_Exam_System') == 'Semester') ? 'checked' : ''}} value="Semester" class="flat-red form-control" {{$disabledUser}}> Semester System
                      </div>
                </div>
              </div>
        </div>

        <div class="box-header box box-default 18_div">
            <h3 class="box-title">Graduate(18-Year)</h3>
        </div>
        <div class="box-body 18_div">
            <div class="row">
                <div class="col-md-3">
                  <div class="form-group">
                    <label>Certificate/Degree</label>
                    @php
                    $collection = collect($msDegree);
                    $filteredItems = $collection->where('title', $previousQualification->MS_Title);
                    if($filteredItems->count() > 0){
                      $val = '';
                    }else{
                      $val = $previousQualification->MS_Title;
                    }
                    if($previousQualification->MS_Title == ''){
                      $val = '';
                    }
                    @endphp
                    <select class="form-control sh-hd-ot" id="MS_Title_d" name="MS_Title_d" style="width: 100%;   " {{$disabledUser}}>
                      <option value="">Select</option>
                      @foreach ( $msDegree as $n )
                        <option value="{{$n->title}}" {{($previousQualification->MS_Title == $n->title || old('MS_Title_d') == $n->title) ? 'selected' : ''}}>{{$n->title}}</option>
                      @endforeach
                    </select>
                    <input type="text" class="form-control ext-hide" value="{{$val ?: old('MS_Title_t')}}" id="MS_Title_t" name="MS_Title_t" {{(old('MS_Title_d') == 'Other')? 'style=display:block': ''}}>
                  </div>
                  <!-- /.form-group -->
                </div>
                <!-- /.col -->
                <div class="col-md-3">
                  <div class="form-group">
                    <label>Board/University</label>
                    @php
                    $collection = collect($bscInstitute);
                    $filteredItems = $collection->where('instName', $previousQualification->MS_From);
                    if($filteredItems->count() > 0){
                      $val = '';
                    }else{
                      $val = $previousQualification->MS_From;
                    }
                    if($previousQualification->MS_From == ''){
                      $val = '';
                    }
                    @endphp
                    <select class="form-control sh-hd-ot" id="MS_From_d" name="MS_From_d" style="width: 100%;   " {{$disabledUser}}>
                     <option value="">Select</option>
                      @foreach ( $bscInstitute as $n )
                        <option value="{{$n->instName}}" {{($previousQualification->MS_From == $n->instName || old('MS_From_d') == $n->instName) ? 'selected' : ''}}>{{$n->instName}}</option>
                      @endforeach
                    </select>
                    <input type="text" class="form-control ext-hide" value="{{$val ?: old('MS_From_t')}}" id="MS_From_t" name="MS_From_t"   {{(old('MS_From_d') == 'Other Universities')? 'style=display:block': ''}} >
                  </div>
                  <!-- /.form-group -->
                </div>
                <div class="col-md-3">
                  <div class="form-group">
                    <label>Degree Title</label>
                   <input type="text" id="MS_Subject" value="{{$previousQualification->MS_Subject ?? old('MS_Subject')}}" name="MS_Subject" class="form-control" {{$disabledUser}}>
                  </div>
                  <!-- /.form-group -->
                </div>              
                <!-- /.col -->
                <div class="col-md-3">
                    <div class="form-group">
                      <label>Subjects</label>
                     <input type="text" id="MS_Specialization" value="{{$previousQualification->MS_Specialization ?? old('MS_Specialization')}}" name="MS_Specialization" class="form-control" {{$disabledUser}}>
                    </div>
                    <!-- /.form-group -->
                  </div>              
                  <!-- /.col -->
              
              
                </div>
             <!-- /.row -->
             <div class="row">
                <div class="col-md-3">
                  <div class="form-group">
                    <label>Total Marks/Total CGPA</label>
                    <input type="number" min="1" id="MS_Total" value="{{$previousQualification->MS_Total ?? old('MS_Total')}}" onBlur="checkMarks('MS_Total','MS','pd_1')" name="MS_Total" class="form-control" {{$disabledUser}}>
                  </div>
                  <!-- /.form-group -->
                </div>
                <!-- /.col -->
                <div class="col-md-3">
                  <div class="form-group">
                    <label>Obtained Marks/Obtained CGPA</label>
                    <input type="number" min="1" step=".01" id="MS" value="{{$previousQualification->MS ?? old('MS')}}" onBlur="checkMarks('MS_Total','MS','pd_1')" name="MS" class="form-control" {{$disabledUser}}>
                  </div>
                  <!-- /.form-group -->
                </div>
                <div class="col-md-3" style="display: none;">
                  <div class="form-group">
                    <label>Percentage</label><br />
                    <label id="pd_1" class="">
                      @php
                        if($previousQualification->MS_Total != '' && $previousQualification->MS != ''){
                          $marks = bcdiv(($previousQualification->MS/$previousQualification->MS_Total*100), 1, 2); 
                          if($marks > 49){
                            echo '<span class="ok">'.$marks.' %</span>';
                          }else{
                            echo '<span class="not-ok">'.$marks.' %</span>';
                          }
                      } 
                     @endphp
                    </label>
                  </div>
                  <!-- /.form-group -->
                </div> 
                <div class="col-md-3">
                  <div class="form-group">
                    <label>Completion Year</label>
                    <select class="form-control sh-hd-ot" id="MS_Year" name="MS_Year" style="width: 100%; " {{$disabledUser}}>
                      <option value="">Select</option>
                      @php
                      $i=date('Y')-70;    
                      for($i; $i<= date('Y'); $i++){
                        if($previousQualification->MS_Year == $i || old('MS_Year') ==  $i){
                          $sel = "selected";
                        }else{
                          $sel = '';
                        }
                        echo '<option value="'.$i.'" '.$sel.'>'.$i.'</option>';
                      }
                      @endphp
                    </select>
                  </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                      <label>Degree Type</label>
                      <select class="form-control sh-hd-ot" id="MS_degree_type" name="MS_degree_type" style="width: 100%; " {{$disabledUser}}>
                        <option value="">Select</option>
                        <option value="Private" {{ ($previousQualification->MS_degree_type == 'Private' || old('MS_degree_type') == 'Private') ? 'selected' : ''}}>Private</option>
                        <option value="Distance Learning" {{ ($previousQualification->MS_degree_type == 'Distance Learning' || old('MS_degree_type') == 'Distance Learning') ? 'selected' : ''}}>Distance Learning</option>
                        <option value="Online/Virtual" {{ ($previousQualification->MS_degree_type == 'Online/Virtual' || old('MS_degree_type') == 'Online/Virtual') ? 'selected' : ''}}>Online/Virtual</option>
                        <option value="Regular" {{ ($previousQualification->MS_degree_type == 'Regular' || old('MS_degree_type') == 'Regular') ? 'selected' : ''}}>Regular</option>
                      </select>
                    </div>
                    <!-- /.form-group -->
                  </div> 
              </div>
              <div class="row">
                <div class="col-md-3">
                  <div class="form-group">
                    <label>Final Transcript <span class="img-upload-cls">File-Format[jpeg/png/jpg/gif/bmp/tiff] only with Max Size (4MB)</span></label><br />
                    <span class="ok">
                      <b>
                        {{-- {{ ($previousQualification->ms_last_result) ? 'Uploaded' : ''}} --}}
                      <?php 
                        if($previousQualification->ms_last_result){
                          echo '<a target="_blank" href="'.asset("storage/".$previousQualification->ms_last_result).'">Uploaded</a>';
                        }
                      ?>
                      </b>
                    </span>
                    <input type="file" class="form-control" id="ms_last_result" name="ms_last_result">
                  </div>
                </div>
                <div class="col-md-3">
                  <div class="form-group">
                    <label>Research Proposal / Personal Statement (2 pages) <span class="img-upload-cls">File-Format[pdf] only with Max Size (4MB)</span> </label><br />
                    <span class="ok">
                      <b>
                        {{-- {{ ($previousQualification->phd_research_proposal) ? 'Uploaded' : ''}} --}}
                      <?php 
                        if($previousQualification->phd_research_proposal){
                          echo '<a target="_blank" href="'.asset("storage/".$previousQualification->phd_research_proposal).'">Uploaded</a>';
                        }
                      ?>
                      </b>
                    </span>
                    <input type="file" class="form-control" id="phd_research_proposal" name="phd_research_proposal">
                  </div>
                </div>
                  <div class=col-md-4>
                      <div class="form-group">
                          <label>Examination System</label><br />
                            <input type="radio" name="MS_Exam_System" {{ ($previousQualification->MS_Exam_System == 'Annual' || old('MS_Exam_System') == 'Annual') ? 'checked' : ''}} value="Annual" class="flat-red form-control" {{$disabledUser}}> Annual System
                            <input type="radio" name="MS_Exam_System" {{ ($previousQualification->MS_Exam_System == 'Semester' || old('MS_Exam_System') == 'Semester') ? 'checked' : ''}} value="Semester" class="flat-red form-control" {{$disabledUser}}> Semester System
                        </div>
                  </div>
                </div>
        
        </div>

          <div class="box-footer">
              {{-- Code for Law admission score starts --}}
              {{-- <button class="btn btn-social btn-success" id="previous-qualification-btn" type="submit"><i class="fa fa-save"></i> Save and Move Next</button> --}}
              {{-- Code for Law admission score Ends --}}
              <button class="btn btn-social btn-success" {{$disabledUser}} id="previous-qualification-btn" type="submit"><i class="fa fa-save"></i> Save and Move Next</button>
          </div>
        </form>
        </div>
        <!-- /.box --> 
      </section>
      <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->



    <!-- Modal -->
<div id="confirm-submit" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Important Information</h4>
      </div>
      <div class="modal-body">
        <p>Please Verify Entered Previous Qualification. Once Page has been saved you cannot edit or change your qualification</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal" id="qualification-submit">Save and continue</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Review</button>
      </div>
    </div>

  </div>
</div>

@include('frontend.footer')