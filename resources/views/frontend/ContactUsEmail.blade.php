<!DOCTYPE html>
<html lang="en-US">	
    <head>
        <meta charset="utf-8">	
    </head>	
    <body>
        {{-- {{dd($mess)}} --}}
        Department: <strong>{{$department}}</strong>, <br /><br />
        Name: <strong>{{$name}}</strong>, <br /><br />
        Email: <strong>{{$email}}</strong>, <br /><br />
        Phone: <strong>{{$phone}}</strong>, <br /><br />
        CNIC: <strong>{{$cnic}}</strong>, <br /><br />
        Message: <strong>{{$mess}}</strong>, <br /><br />
        <br/><br/>
        <div style="font-size:smaller;background-color: #3c763d;color: #fff;">This communication contains information that is for the exclusive use of the intended recipient(s). If you are not the intended recipient, disclosure, copying, distribution or other use of, or taking of any action in reliance upon, this communication or the information in it is prohibited and may be unlawful. If you have received this communication in error please notify the sender by return email, delete it from your system and destroy any copies.</div>
    </body>
</html>