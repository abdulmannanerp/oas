@section('title')
Address Detail
@endsection
@include('frontend.header')
@php
if($applicationStatus->step_save == 1 && $hash ==''){
  $readonlyUser = "readonly='readonly'";
  $disabledUser = "disabled";
  $disabledUser1 = 'disabled="disabled"';
}else{
  $readonlyUser='';
  $disabledUser = '';
  $disabledUser1 = '';
}
@endphp
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          <i class="fa fa-address-book-o text-aqua"></i> Address
          <small>Add / Edit Address</small>
        </h1>
      </section>
      @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    @if ( $status )
        <div class="alert alert-success" role="alert"> {{ $status }}</div>
    @endif       
      <!-- Main content -->
      <section class="content">
          <form action="{{route('addressdetail', $hash)}}" class="addressDetailForm" method="POST">
            <input type="hidden" name="userId" value="{{$userdetail->userId}}">
            {{csrf_field()}}  
        <!-- SELECT2 EXAMPLE -->
        <div class="box box-info">
          <div class="box-header with-border">
            <h3 class="box-title">Permanent Address</h3>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <div class="row">
              <div class="col-md-3">
                <div class="form-group">
                  <label>City</label>
                  @if($userdetail->fkNationality == 2 || $userdetail->fkNationality == 3)
                    <input class="form-control" name="permanent_city" id="permanent_city" value ="{{ ($applicantAddress->cityPmt == '') ? old('permanent_city') : $applicantAddress->cityPmt }}" style="width: 100%;" required {{ $disabledUser }}>
                  @endif
                  @if($userdetail->fkNationality == 1)
                    <select class="form-control select2" name="permanent_city" id="permanent_city" style="width: 100%;" required {{ $disabledUser }}>
                      <option value="">Select City</option>
                      @foreach ( $city as $n )
                        <option value="{{$n->city}}" {{ ($applicantAddress->cityPmt == $n->city || old('permanent_city') == $n->city) ? 'selected' : ''}}>{{$n->city}}</option>
                      @endforeach
                    </select>
                  @endif
                </div>
                <!-- /.form-group -->
              </div>
              <!-- /.col -->
              <div class="col-md-3">
                <div class="form-group">
                  <label>Phone</label>
                  <div class="input-group">
                    <div class="input-group-addon">
                      <i class="fa fa-phone"></i>
                    </div>
                    @php
                      $carrier = '';
                      $phone = '';
                      if($applicantAddress->phonePmt){
                        $carrier = substr($applicantAddress->phonePmt, 2, 3);
                        $phone = substr($applicantAddress->phonePmt, 5, 7);
                      }
                      @endphp
                      @if($userdetail->fkNationality == 1)
                        <select name="permanent_carrier" id="permanent_carrier" class="form-control" required style="width: 40%;" {{ $disabledUser1 }}>
                          <option value="">Code</option>
                          <option value="300" {{ ($carrier == '300' || old('carrier') == '300') ? 'selected' : ''}}>0300</option>
                          <option value="301" {{ ($carrier == '301' || old('carrier') == '301') ? 'selected' : ''}}>0301</option>
                          <option value="302" {{ ($carrier == '302' || old('carrier') == '302') ? 'selected' : ''}}>0302</option>
                          <option value="303" {{ ($carrier == '303' || old('carrier') == '303') ? 'selected' : ''}}>0303</option>
                          <option value="304" {{ ($carrier == '304' || old('carrier') == '304') ? 'selected' : ''}}>0304</option>
                          <option value="305" {{ ($carrier == '305' || old('carrier') == '305') ? 'selected' : ''}}>0305</option>
                          <option value="306" {{ ($carrier == '306' || old('carrier') == '306') ? 'selected' : ''}}>0306</option>
                          <option value="307" {{ ($carrier == '307' || old('carrier') == '307') ? 'selected' : ''}}>0307</option>
                          <option value="308" {{ ($carrier == '308' || old('carrier') == '308') ? 'selected' : ''}}>0308</option>
                          <option value="309" {{ ($carrier == '309' || old('carrier') == '309') ? 'selected' : ''}}>0309</option>
                          <option value="310" {{ ($carrier == '310' || old('carrier') == '310') ? 'selected' : ''}}>0310</option>
                          <option value="311" {{ ($carrier == '311' || old('carrier') == '311') ? 'selected' : ''}}>0311</option>
                          <option value="312" {{ ($carrier == '312' || old('carrier') == '312') ? 'selected' : ''}}>0312</option>
                          <option value="313" {{ ($carrier == '313' || old('carrier') == '313') ? 'selected' : ''}}>0313</option>
                          <option value="314" {{ ($carrier == '314' || old('carrier') == '314') ? 'selected' : ''}}>0314</option>
                          <option value="315" {{ ($carrier == '315' || old('carrier') == '315') ? 'selected' : ''}}>0315</option>
                          <option value="316" {{ ($carrier == '316' || old('carrier') == '316') ? 'selected' : ''}}>0316</option>
                          <option value="317" {{ ($carrier == '317' || old('carrier') == '317') ? 'selected' : ''}}>0317</option>
                          <option value="318" {{ ($carrier == '318' || old('carrier') == '318') ? 'selected' : ''}}>0318</option>
                          <option value="320" {{ ($carrier == '320' || old('carrier') == '320') ? 'selected' : ''}}>0320</option>
                          <option value="321" {{ ($carrier == '321' || old('carrier') == '321') ? 'selected' : ''}}>0321</option>
                          <option value="322" {{ ($carrier == '322' || old('carrier') == '322') ? 'selected' : ''}}>0322</option>
                          <option value="323" {{ ($carrier == '323' || old('carrier') == '323') ? 'selected' : ''}}>0323</option>
                          <option value="324" {{ ($carrier == '324' || old('carrier') == '324') ? 'selected' : ''}}>0324</option>
                          <option value="330" {{ ($carrier == '330' || old('carrier') == '330') ? 'selected' : ''}}>0330</option>
                          <option value="331" {{ ($carrier == '331' || old('carrier') == '331') ? 'selected' : ''}}>0331</option>
                          <option value="332" {{ ($carrier == '332' || old('carrier') == '332') ? 'selected' : ''}}>0332</option>
                          <option value="333" {{ ($carrier == '333' || old('carrier') == '333') ? 'selected' : ''}}>0333</option>
                          <option value="334" {{ ($carrier == '334' || old('carrier') == '334') ? 'selected' : ''}}>0334</option>
                          <option value="335" {{ ($carrier == '335' || old('carrier') == '335') ? 'selected' : ''}}>0335</option>
                          <option value="336" {{ ($carrier == '336' || old('carrier') == '336') ? 'selected' : ''}}>0336</option>
                          <option value="337" {{ ($carrier == '337' || old('carrier') == '337') ? 'selected' : ''}}>0337</option>
                          <option value="340" {{ ($carrier == '340' || old('carrier') == '340') ? 'selected' : ''}}>0340</option>
                          <option value="341" {{ ($carrier == '341' || old('carrier') == '341') ? 'selected' : ''}}>0341</option>                        
                          <option value="342" {{ ($carrier == '342' || old('carrier') == '342') ? 'selected' : ''}}>0342</option>
                          <option value="343" {{ ($carrier == '343' || old('carrier') == '343') ? 'selected' : ''}}>0343</option>
                          <option value="344" {{ ($carrier == '344' || old('carrier') == '344') ? 'selected' : ''}}>0344</option>
                          <option value="345" {{ ($carrier == '345' || old('carrier') == '345') ? 'selected' : ''}}>0345</option>
                          <option value="346" {{ ($carrier == '346' || old('carrier') == '346') ? 'selected' : ''}}>0346</option>
                          <option value="347" {{ ($carrier == '347' || old('carrier') == '347') ? 'selected' : ''}}>0347</option>
                          <option value="348" {{ ($carrier == '348' || old('carrier') == '348') ? 'selected' : ''}}>0348</option>
                          <option value="349" {{ ($carrier == '349' || old('carrier') == '349') ? 'selected' : ''}}>0349</option>
                        </select>
                        <input type="text" class="form-control" style="width: 60%;" onkeypress="return isNumberKey(event)" id="permanent_phone" name="permanent_phone" minlength="7" maxlength="7" value="{{$phone ?: old('permanent_phone')}}" required {{ $disabledUser }}>                      
                      @endif 
                      @if($userdetail->fkNationality == 2 || $userdetail->fkNationality == 3)
                        <input type="number" name="permanent_carrier" id="permanent_carrier" class="form-control" value="{{$applicantAddress->phonePmt ?? old('permanent_carrier')}}" required style="width: 100%;" {{ $disabledUser}}>
                      @endif                      
                  </div>
                </div>
                <!-- /.form-group -->
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label>Address</label>
                  <textarea class="form-control" value="{{$applicantAddress->addressPmt ?? old('permanent_address')}}" name="permanent_address" id="permanent_address" rows="1" required {{ $disabledUser }}>{{$applicantAddress->addressPmt ?? old('permanent_address')}}</textarea>
                </div>
              </div>    
              <div class="col-md-3">
                <div class="form-group">
                  <label>Area</label>
                  <select class="form-control select2" name="permanent_area" id="permanent_area" style="width: 100%;" required {{ $disabledUser }}>
                    <option value="">Select Area</option>
                    <option value="Rural" {{ ($applicantAddress->areaPmt == 'Rural' || old('permanent_area') == 'Rural') ? 'selected' : ''}}>Rural</option>
                    <option value="Urban" {{ ($applicantAddress->areaPmt == 'Urban' || old('permanent_area') == 'Urban') ? 'selected' : ''}}>Urban</option>                    
                  </select>
                </div>
              </div>            
              <!-- /.col -->
            </div>
           <!-- /.row -->
          </div>
          <!-- /.box-body -->
          <div class="box-header box box-success">
            <h3 class="box-title">
              @if($userdetail->fkNationality == 2 || $userdetail->fkNationality == 3)
                  {{'Pakistan Address'}}
              @else
                  {{'Mailing Address'}}
              @endif
            </h3>
            <h5>
              <label class="nohash">
                <input type="checkbox" id="mailing_check" class="minimal" {{ ($applicantAddress->phonePo == $applicantAddress->phonePmt && $applicantAddress->phonePmt != '') ? 'checked' : ''}} {{ $disabledUser }}>
                Same as Permanent Address
              </label>
            </h5>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <div class="row">
              <div class="col-md-3">
                <div class="form-group">
                  <label>City</label>
                  @if($userdetail->fkNationality == 2 || $userdetail->fkNationality == 3)
                    <input class="form-control" name="mailing_city" id="mailing_city" value ="{{ ($applicantAddress->cityPo == '') ? old('mailing_city') : $applicantAddress->cityPo }}" style="width: 100%;" required {{ $disabledUser }}>
                  @endif
                  @if($userdetail->fkNationality == 1)
                    <select class="form-control select2" name="mailing_city" id="mailing_city" style="width: 100%;" required {{ $disabledUser }}>
                      <option value="">Select City</option>
                      @foreach ( $city as $n )
                        <option value="{{$n->city}}" {{ ($applicantAddress->cityPo == $n->city || old('mailing_city') == $n->city) ? 'selected' : ''}}>{{$n->city}}</option>
                      @endforeach
                    </select>
                  @endif                    
                </div>
                <!-- /.form-group -->
              </div>
              <!-- /.col -->
              <div class="col-md-3">
                <div class="form-group">
                  <label>Phone</label>
                  <div class="input-group">
                    <div class="input-group-addon">
                      <i class="fa fa-phone"></i>
                    </div>
                    @php
                      $carrier = '';
                      $phone = '';
                      if($applicantAddress->phonePo){
                        $carrier = substr($applicantAddress->phonePo, 2, 3);
                        $phone = substr($applicantAddress->phonePo, 5, 7);
                      }
                      @endphp
                      @if($userdetail->fkNationality == 1)
                        <select name="mailing_carrier" id="mailing_carrier" class="form-control" required style="width: 40%;" {{ $disabledUser }}>
                          <option value="">Code</option>
                          <option value="300" {{ ($carrier == '300' || old('carrier') == '300') ? 'selected' : ''}}>0300</option>
                          <option value="301" {{ ($carrier == '301' || old('carrier') == '301') ? 'selected' : ''}}>0301</option>
                          <option value="302" {{ ($carrier == '302' || old('carrier') == '302') ? 'selected' : ''}}>0302</option>
                          <option value="303" {{ ($carrier == '303' || old('carrier') == '303') ? 'selected' : ''}}>0303</option>
                          <option value="304" {{ ($carrier == '304' || old('carrier') == '304') ? 'selected' : ''}}>0304</option>
                          <option value="305" {{ ($carrier == '305' || old('carrier') == '305') ? 'selected' : ''}}>0305</option>
                          <option value="306" {{ ($carrier == '306' || old('carrier') == '306') ? 'selected' : ''}}>0306</option>
                          <option value="307" {{ ($carrier == '307' || old('carrier') == '307') ? 'selected' : ''}}>0307</option>
                          <option value="308" {{ ($carrier == '308' || old('carrier') == '308') ? 'selected' : ''}}>0308</option>
                          <option value="309" {{ ($carrier == '309' || old('carrier') == '309') ? 'selected' : ''}}>0309</option>
                          <option value="310" {{ ($carrier == '310' || old('carrier') == '310') ? 'selected' : ''}}>0310</option>
                          <option value="311" {{ ($carrier == '311' || old('carrier') == '311') ? 'selected' : ''}}>0311</option>
                          <option value="312" {{ ($carrier == '312' || old('carrier') == '312') ? 'selected' : ''}}>0312</option>
                          <option value="313" {{ ($carrier == '313' || old('carrier') == '313') ? 'selected' : ''}}>0313</option>
                          <option value="314" {{ ($carrier == '314' || old('carrier') == '314') ? 'selected' : ''}}>0314</option>
                          <option value="315" {{ ($carrier == '315' || old('carrier') == '315') ? 'selected' : ''}}>0315</option>
                          <option value="316" {{ ($carrier == '316' || old('carrier') == '316') ? 'selected' : ''}}>0316</option>
                          <option value="317" {{ ($carrier == '317' || old('carrier') == '317') ? 'selected' : ''}}>0317</option>
                          <option value="318" {{ ($carrier == '318' || old('carrier') == '318') ? 'selected' : ''}}>0318</option>
                          <option value="320" {{ ($carrier == '320' || old('carrier') == '320') ? 'selected' : ''}}>0320</option>
                          <option value="321" {{ ($carrier == '321' || old('carrier') == '321') ? 'selected' : ''}}>0321</option>
                          <option value="322" {{ ($carrier == '322' || old('carrier') == '322') ? 'selected' : ''}}>0322</option>
                          <option value="323" {{ ($carrier == '323' || old('carrier') == '323') ? 'selected' : ''}}>0323</option>
                          <option value="324" {{ ($carrier == '324' || old('carrier') == '324') ? 'selected' : ''}}>0324</option>
                          <option value="330" {{ ($carrier == '330' || old('carrier') == '330') ? 'selected' : ''}}>0330</option>
                          <option value="331" {{ ($carrier == '331' || old('carrier') == '331') ? 'selected' : ''}}>0331</option>
                          <option value="332" {{ ($carrier == '332' || old('carrier') == '332') ? 'selected' : ''}}>0332</option>
                          <option value="333" {{ ($carrier == '333' || old('carrier') == '333') ? 'selected' : ''}}>0333</option>
                          <option value="334" {{ ($carrier == '334' || old('carrier') == '334') ? 'selected' : ''}}>0334</option>
                          <option value="335" {{ ($carrier == '335' || old('carrier') == '335') ? 'selected' : ''}}>0335</option>
                          <option value="336" {{ ($carrier == '336' || old('carrier') == '336') ? 'selected' : ''}}>0336</option>
                          <option value="337" {{ ($carrier == '337' || old('carrier') == '337') ? 'selected' : ''}}>0337</option>
                          <option value="340" {{ ($carrier == '340' || old('carrier') == '340') ? 'selected' : ''}}>0340</option>
                          <option value="341" {{ ($carrier == '341' || old('carrier') == '341') ? 'selected' : ''}}>0341</option>                        
                          <option value="342" {{ ($carrier == '342' || old('carrier') == '342') ? 'selected' : ''}}>0342</option>
                          <option value="343" {{ ($carrier == '343' || old('carrier') == '343') ? 'selected' : ''}}>0343</option>
                          <option value="344" {{ ($carrier == '344' || old('carrier') == '344') ? 'selected' : ''}}>0344</option>
                          <option value="345" {{ ($carrier == '345' || old('carrier') == '345') ? 'selected' : ''}}>0345</option>
                          <option value="346" {{ ($carrier == '346' || old('carrier') == '346') ? 'selected' : ''}}>0346</option>
                          <option value="347" {{ ($carrier == '347' || old('carrier') == '347') ? 'selected' : ''}}>0347</option>
                          <option value="348" {{ ($carrier == '348' || old('carrier') == '348') ? 'selected' : ''}}>0348</option>
                          <option value="349" {{ ($carrier == '349' || old('carrier') == '349') ? 'selected' : ''}}>0349</option>
                        </select>
                      <input type="text" class="form-control" style="width: 60%;" onkeypress="return isNumberKey(event)" id="mailing_phone" name="mailing_phone" minlength="7" maxlength="7" value="{{$phone ?: old('mailing_phone')}}" required {{ $disabledUser }}>
                      @endif 
                      @if($userdetail->fkNationality == 2 || $userdetail->fkNationality == 3)
                        <input type="number" name="mailing_carrier" id="mailing_carrier" class="form-control" value="{{$applicantAddress->phonePo ?? old('mailing_carrier')}}" required style="width: 100%;" {{ $disabledUser}}>
                      @endif  
                  </div>
                </div>
                <!-- /.form-group -->
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label>Address</label>
                  <textarea class="form-control" value="{{$applicantAddress->addressPo ?? old('mailing_address')}}" name="mailing_address" id="mailing_address" rows="1" required {{ $disabledUser }}>{{$applicantAddress->addressPo ?? old('mailing_address')}}</textarea>
                </div>
              </div> 
              <div class="col-md-3">
                <div class="form-group">
                  <label>Area</label>
                  <select class="form-control select2" name="mailing_area" id="mailing_area" style="width: 100%;" required {{ $disabledUser }}>
                    <option value="">Select Area</option>
                    <option value="Rural" {{ ($applicantAddress->areaPo == 'Rural' || old('mailing_area') == 'Rural') ? 'selected' : ''}}>Rural</option>
                    <option value="Urban" {{ ($applicantAddress->areaPo == 'Urban' || old('mailing_area') == 'Urban') ? 'selected' : ''}}>Urban</option>                    
                  </select>
                </div>
              </div>               
              <!-- /.col -->
            </div>
           <!-- /.row -->
          </div>
          {{-- <div class="box-header box box-warning">
            <h3 class="box-title">
              @if($userdetail->fkNationality == 2 || $userdetail->fkNationality == 3)
                  {{'Home Country Address'}}
              @else
                  {{'Father/Gaurdian Address'}}
              @endif
            </h3>
            <h5>
              <label class="nohash">
                <input type="checkbox" id="father_check" class="minimal" {{ ($applicantAddress->phoneFather == $applicantAddress->phonePmt && $applicantAddress->phonePmt != '') ? 'checked' : ''}} {{ $disabledUser }}>
                Same as Permanent Address
              </label>
            </h5>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <div class="row">
              <div class="col-md-3">
                <div class="form-group">
                  <label>City</label>
                  @if($userdetail->fkNationality == 2 || $userdetail->fkNationality == 3)
                    <input class="form-control" name="father_city" id="father_city" value ="{{ ($applicantAddress->cityFather == '') ? old('father_city') : $applicantAddress->cityFather }}" style="width: 100%;" required {{ $disabledUser }}>
                  @endif
                  @if($userdetail->fkNationality == 1)
                    <select class="form-control select2" name="father_city" id="father_city" style="width: 100%;" required {{ $disabledUser }}>
                      <option value="">Select City</option>
                      @foreach ( $city as $n )
                        <option value="{{$n->city}}" {{ ($applicantAddress->cityFather == $n->city || old('father_city') == $n->city) ? 'selected' : ''}}>{{$n->city}}</option>
                      @endforeach
                    </select>
                  @endif
                </div>
                <!-- /.form-group -->
              </div>
              <!-- /.col -->
              <div class="col-md-3">
                <div class="form-group">
                  <label>Phone</label>
                  <div class="input-group">
                    <div class="input-group-addon">
                      <i class="fa fa-phone"></i>
                    </div>
                    @php
                      $carrier = '';
                      $phone = '';
                      if($applicantAddress->phoneFather){
                        $carrier = substr($applicantAddress->phoneFather, 2, 3);
                        $phone = substr($applicantAddress->phoneFather, 5, 7);
                      }
                      @endphp
                      @if($userdetail->fkNationality == 1)
                        <select name="father_carrier" id="father_carrier" class="form-control" required style="width: 40%;" {{ $disabledUser }}>
                          <option value="">Code</option>
                          <option value="300" {{ ($carrier == '300' || old('carrier') == '300') ? 'selected' : ''}}>0300</option>
                          <option value="301" {{ ($carrier == '301' || old('carrier') == '301') ? 'selected' : ''}}>0301</option>
                          <option value="302" {{ ($carrier == '302' || old('carrier') == '302') ? 'selected' : ''}}>0302</option>
                          <option value="303" {{ ($carrier == '303' || old('carrier') == '303') ? 'selected' : ''}}>0303</option>
                          <option value="304" {{ ($carrier == '304' || old('carrier') == '304') ? 'selected' : ''}}>0304</option>
                          <option value="305" {{ ($carrier == '305' || old('carrier') == '305') ? 'selected' : ''}}>0305</option>
                          <option value="306" {{ ($carrier == '306' || old('carrier') == '306') ? 'selected' : ''}}>0306</option>
                          <option value="307" {{ ($carrier == '307' || old('carrier') == '307') ? 'selected' : ''}}>0307</option>
                          <option value="308" {{ ($carrier == '308' || old('carrier') == '308') ? 'selected' : ''}}>0308</option>
                          <option value="309" {{ ($carrier == '309' || old('carrier') == '309') ? 'selected' : ''}}>0309</option>
                          <option value="310" {{ ($carrier == '310' || old('carrier') == '310') ? 'selected' : ''}}>0310</option>
                          <option value="311" {{ ($carrier == '311' || old('carrier') == '311') ? 'selected' : ''}}>0311</option>
                          <option value="312" {{ ($carrier == '312' || old('carrier') == '312') ? 'selected' : ''}}>0312</option>
                          <option value="313" {{ ($carrier == '313' || old('carrier') == '313') ? 'selected' : ''}}>0313</option>
                          <option value="314" {{ ($carrier == '314' || old('carrier') == '314') ? 'selected' : ''}}>0314</option>
                          <option value="315" {{ ($carrier == '315' || old('carrier') == '315') ? 'selected' : ''}}>0315</option>
                          <option value="316" {{ ($carrier == '316' || old('carrier') == '316') ? 'selected' : ''}}>0316</option>
                          <option value="317" {{ ($carrier == '317' || old('carrier') == '317') ? 'selected' : ''}}>0317</option>
                          <option value="318" {{ ($carrier == '318' || old('carrier') == '318') ? 'selected' : ''}}>0318</option>
                          <option value="320" {{ ($carrier == '320' || old('carrier') == '320') ? 'selected' : ''}}>0320</option>
                          <option value="321" {{ ($carrier == '321' || old('carrier') == '321') ? 'selected' : ''}}>0321</option>
                          <option value="322" {{ ($carrier == '322' || old('carrier') == '322') ? 'selected' : ''}}>0322</option>
                          <option value="323" {{ ($carrier == '323' || old('carrier') == '323') ? 'selected' : ''}}>0323</option>
                          <option value="324" {{ ($carrier == '324' || old('carrier') == '324') ? 'selected' : ''}}>0324</option>
                          <option value="330" {{ ($carrier == '330' || old('carrier') == '330') ? 'selected' : ''}}>0330</option>
                          <option value="331" {{ ($carrier == '331' || old('carrier') == '331') ? 'selected' : ''}}>0331</option>
                          <option value="332" {{ ($carrier == '332' || old('carrier') == '332') ? 'selected' : ''}}>0332</option>
                          <option value="333" {{ ($carrier == '333' || old('carrier') == '333') ? 'selected' : ''}}>0333</option>
                          <option value="334" {{ ($carrier == '334' || old('carrier') == '334') ? 'selected' : ''}}>0334</option>
                          <option value="335" {{ ($carrier == '335' || old('carrier') == '335') ? 'selected' : ''}}>0335</option>
                          <option value="336" {{ ($carrier == '336' || old('carrier') == '336') ? 'selected' : ''}}>0336</option>
                          <option value="337" {{ ($carrier == '337' || old('carrier') == '337') ? 'selected' : ''}}>0337</option>
                          <option value="340" {{ ($carrier == '340' || old('carrier') == '340') ? 'selected' : ''}}>0340</option>
                          <option value="341" {{ ($carrier == '341' || old('carrier') == '341') ? 'selected' : ''}}>0341</option>                        
                          <option value="342" {{ ($carrier == '342' || old('carrier') == '342') ? 'selected' : ''}}>0342</option>
                          <option value="343" {{ ($carrier == '343' || old('carrier') == '343') ? 'selected' : ''}}>0343</option>
                          <option value="344" {{ ($carrier == '344' || old('carrier') == '344') ? 'selected' : ''}}>0344</option>
                          <option value="345" {{ ($carrier == '345' || old('carrier') == '345') ? 'selected' : ''}}>0345</option>
                          <option value="346" {{ ($carrier == '346' || old('carrier') == '346') ? 'selected' : ''}}>0346</option>
                          <option value="347" {{ ($carrier == '347' || old('carrier') == '347') ? 'selected' : ''}}>0347</option>
                          <option value="348" {{ ($carrier == '348' || old('carrier') == '348') ? 'selected' : ''}}>0348</option>
                          <option value="349" {{ ($carrier == '349' || old('carrier') == '349') ? 'selected' : ''}}>0349</option>
                        </select>
                        <input type="text" class="form-control" style="width: 60%;" onkeypress="return isNumberKey(event)" id="father_phone" name="father_phone" minlength="7" maxlength="7" value="{{$phone ?: old('father_phone')}}" required {{ $disabledUser }}>
                      @endif 
                      @if($userdetail->fkNationality == 2 || $userdetail->fkNationality == 3)
                        <input type="number" name="father_carrier" id="father_carrier" class="form-control" value="{{$applicantAddress->phoneFather ?? old('father_carrier')}}" required style="width: 100%;" {{ $disabledUser}}>
                      @endif  
                  </div>
                </div>
                <!-- /.form-group -->
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label>Address</label>
                  <textarea class="form-control" value="{{$applicantAddress->addressFather ?? old('father_address')}}" name="father_address" id="father_address" rows="1" required {{ $disabledUser }}>{{$applicantAddress->addressFather ?? old('father_address')}}</textarea>
                </div>
              </div>  
              <div class="col-md-3">
                <div class="form-group">
                  <label>Area</label>
                  <select class="form-control select2" name="father_area" id="father_area" style="width: 100%;" required {{ $disabledUser }}>
                    <option value="">Select Area</option>
                    <option value="Rural" {{ ($applicantAddress->areaFather == 'Rural' || old('father_area') == 'Rural') ? 'selected' : ''}}>Rural</option>
                    <option value="Urban" {{ ($applicantAddress->areaFather == 'Urban' || old('father_area') == 'Urban') ? 'selected' : ''}}>Urban</option>                    
                  </select>
                </div>
              </div>              
              <!-- /.col -->
            </div>
           <!-- /.row -->
          </div> --}}
          <!-- /.box-body -->
          <div class="box-footer">
            <button class="btn btn-social btn-success" id="address-detail-btn" type="submit" {{ $disabledUser }}><i class="fa fa-save"></i> Save and Move Next</button>
          </div>
        </form>
        </div>
        <!-- /.box --> 
      </section>
      <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

@include('frontend.footer')