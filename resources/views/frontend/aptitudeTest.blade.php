@section('title')
Aptitude Test
@endsection
@include('frontend.header')
@php
if($applicationStatus->step_save == 1 && $hash ==''){
  $readonlyUser = "readonly='readonly'";
  $disabledUser = "disabled";
  $disabledUser1 = 'disabled="disabled"';
}else{
  $readonlyUser='';
  $disabledUser = '';
  $disabledUser1 = '';
}
@endphp

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          <i class="fa fa-file-text-o text-red"></i> Aptitude Test
          <small>Add / Edit Aptitude Test</small>
        </h1>
      </section>
      @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    @if ( $status )
        <div class="alert alert-success" role="alert"> {{ $status }}</div>
    @endif  
  
      <!-- Main content -->
      <section class="content">
        <form action="{{route('aptitudetest', $hash)}}" id="aptitudeForm" method="POST">
          <input type="hidden" name="userId" value="{{$userdetail->userId}}">
          {{csrf_field()}}
  
        <!-- SELECT2 EXAMPLE -->
        <div class="box box-info">
          <div class="box-header with-border">
            <h3 class="box-title">Aptitude Test Details</h3>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <div class="row">
              <div class="col-md-3">
                <div class="form-group">
                  <label>Test Type</label>
                  <select class="form-control select2" id="type_1" name="type_1" style="width: 100%;" {{$disabledUser}}>
                    <option value="">Select</option>
                    <option value="1" {{ ($previousQualification->APT_Test_1 == 1) ? 'selected' : ''}}>NAT</option>
                    <option value="2" {{ ($previousQualification->APT_Test_1 == 2) ? 'selected' : ''}}>GAT</option>
                    <option value="3" {{ ($previousQualification->APT_Test_1 == 3) ? 'selected' : ''}}>GAT-Subject</option>
                    <option value="4" {{ ($previousQualification->APT_Test_1 == 4) ? 'selected' : ''}}>SAT</option>
                    <option value="5" {{ ($previousQualification->APT_Test_1 == 5) ? 'selected' : ''}}>GRE</option>
                    <option value="6" {{ ($previousQualification->APT_Test_1 == 6) ? 'selected' : ''}}>Other</option>
                  </select>
                </div>
                <!-- /.form-group -->
              </div>
              <!-- /.col -->
              <div class="col-md-3">
                <div class="form-group">
                  <label>Year Taken</label>
                  <select class="form-control select2" id="year_1" name="year_1" style="width: 100%;" {{$disabledUser}}>
                      <option value="">Select</option>
                      <option value="1" {{ ($previousQualification->APT_Test_1_Year == 1) ? 'selected' : ''}}>2015</option>
                      <option value="2" {{ ($previousQualification->APT_Test_1_Year == 2) ? 'selected' : ''}}>2016</option>
                      <option value="3" {{ ($previousQualification->APT_Test_1_Year == 3) ? 'selected' : ''}}>2017</option>
                      <option value="4" {{ ($previousQualification->APT_Test_1_Year == 4) ? 'selected' : ''}}>2018</option>
                      <option value="5" {{ ($previousQualification->APT_Test_1_Year == 5) ? 'selected' : ''}}>2019</option>
                    </select>
                </div>
                <!-- /.form-group -->
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label>Obtained Marks</label>
                  <input type="number" min="0" step="1" id="obtained_1" name="obtained_1" value="{{$previousQualification->APT_Test_1_Marks}}" class="form-control" {{$disabledUser}}>
                </div>
              </div>              
              <!-- /.col -->
              <div class="col-md-3">
                  <div class="form-group">
                    <label>Total Marks</label>
                    <input type="number" min="0" step="1" id="total_1" name="total_1" value="{{$previousQualification->APT_Test_1_Total_Marks}}" class="form-control" {{$disabledUser}}>
                  </div>
                </div>              
                <!-- /.col -->
            </div>
            <div class="row">
              <div class="col-md-3">
                <div class="form-group">
                  <label>Test Type</label>
                  <select class="form-control select2" id="type_2" name="type_2" style="width: 100%;" {{$disabledUser}}>
                    <option value="">Select</option>
                    <option value="1" {{ ($previousQualification->APT_Test_2 == 1) ? 'selected' : ''}}>NAT</option>
                    <option value="2" {{ ($previousQualification->APT_Test_2 == 2) ? 'selected' : ''}}>GAT</option>
                    <option value="3" {{ ($previousQualification->APT_Test_2 == 3) ? 'selected' : ''}}>GAT-Subject</option>
                    <option value="4" {{ ($previousQualification->APT_Test_2 == 4) ? 'selected' : ''}}>SAT</option>
                    <option value="5" {{ ($previousQualification->APT_Test_2 == 5) ? 'selected' : ''}}>GRE</option>
                    <option value="6" {{ ($previousQualification->APT_Test_2 == 6) ? 'selected' : ''}}>Other</option>
                  </select>
                </div>
                <!-- /.form-group -->
              </div>
              <!-- /.col -->
              <div class="col-md-3">
                <div class="form-group">
                  <label>Year Taken</label>
                  <select class="form-control select2" id="year_2" name="year_2" style="width: 100%;" {{$disabledUser}}>
                      <option value="">Select</option>
                      <option value="1" {{ ($previousQualification->APT_Test_2_Year == 1) ? 'selected' : ''}}>2015</option>
                      <option value="2" {{ ($previousQualification->APT_Test_2_Year == 2) ? 'selected' : ''}}>2016</option>
                      <option value="3" {{ ($previousQualification->APT_Test_2_Year == 3) ? 'selected' : ''}}>2017</option>
                      <option value="4" {{ ($previousQualification->APT_Test_2_Year == 4) ? 'selected' : ''}}>2018</option>
                      <option value="5" {{ ($previousQualification->APT_Test_2_Year == 5) ? 'selected' : ''}}>2019</option>
                    </select>
                </div>
                <!-- /.form-group -->
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label>Obtained Marks</label>
                  <input type="number" min="0" step="1" id="obtained_2" name="obtained_2" value="{{$previousQualification->APT_Test_2_Marks}}" class="form-control" {{$disabledUser}}>
                </div>
              </div>              
              <!-- /.col -->
              <div class="col-md-3">
                <div class="form-group">
                  <label>Total Marks</label>
                  <input type="number" min="0" step="1" id="total_2" name="total_2" value="{{$previousQualification->APT_Test_2_Total_Marks}}" class="form-control" {{$disabledUser}}>
                </div>
              </div>              
              <!-- /.col -->
            </div>
            <div class="row">
              <div class="col-md-3">
                <div class="form-group">
                  <label>Test Type</label>
                  <select class="form-control select2" id="type_3" name="type_3" style="width: 100%;" {{$disabledUser}}>
                    <option value="">Select</option>
                    <option value="1" {{ ($previousQualification->APT_Test_3 == 1) ? 'selected' : ''}}>NAT</option>
                    <option value="2" {{ ($previousQualification->APT_Test_3 == 2) ? 'selected' : ''}}>GAT</option>
                    <option value="3" {{ ($previousQualification->APT_Test_3 == 3) ? 'selected' : ''}}>GAT-Subject</option>
                    <option value="4" {{ ($previousQualification->APT_Test_3 == 4) ? 'selected' : ''}}>SAT</option>
                    <option value="5" {{ ($previousQualification->APT_Test_3 == 5) ? 'selected' : ''}}>GRE</option>
                    <option value="6" {{ ($previousQualification->APT_Test_3 == 6) ? 'selected' : ''}}>Other</option>
                  </select>
                </div>
                <!-- /.form-group -->
              </div>
              <!-- /.col -->
              <div class="col-md-3">
                <div class="form-group">
                  <label>Year Taken</label>
                  <select class="form-control select2" id="year_3" name="year_3" style="width: 100%;" {{$disabledUser}}>
                      <option value="">Select</option>
                      <option value="1" {{ ($previousQualification->APT_Test_3_Year == 1) ? 'selected' : ''}}>2015</option>
                      <option value="2" {{ ($previousQualification->APT_Test_3_Year == 2) ? 'selected' : ''}}>2016</option>
                      <option value="3" {{ ($previousQualification->APT_Test_3_Year == 3) ? 'selected' : ''}}>2017</option>
                      <option value="4" {{ ($previousQualification->APT_Test_3_Year == 4) ? 'selected' : ''}}>2018</option>
                      <option value="5" {{ ($previousQualification->APT_Test_3_Year == 5) ? 'selected' : ''}}>2019</option>
                    </select>
                </div>
                <!-- /.form-group -->
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label>Obtained Marks</label>
                  <input type="number" min="0" step="1" id="obtained_3" name="obtained_3" value="{{$previousQualification->APT_Test_3_Marks}}" class="form-control" {{$disabledUser}}>
                </div>
              </div>              
              <!-- /.col -->
              <div class="col-md-3">
                <div class="form-group">
                  <label>Total Marks</label>
                  <input type="number" min="0" step="1" id="total_3" name="total_3" value="{{$previousQualification->APT_Test_3_Total_Marks}}" class="form-control" {{$disabledUser}}>
                </div>
              </div>              
              <!-- /.col -->
            </div>
            <div class="row">
              <div class="col-md-3">
                <div class="form-group">
                  <label>Test Type</label>
                  <select class="form-control select2" id="type_4" name="type_4" style="width: 100%;" {{$disabledUser}}>
                    <option value="">Select</option>
                    <option value="1" {{ ($previousQualification->APT_Test_4 == 1) ? 'selected' : ''}}>NAT</option>
                    <option value="2" {{ ($previousQualification->APT_Test_4 == 2) ? 'selected' : ''}}>GAT</option>
                    <option value="3" {{ ($previousQualification->APT_Test_4 == 3) ? 'selected' : ''}}>GAT-Subject</option>
                    <option value="4" {{ ($previousQualification->APT_Test_4 == 4) ? 'selected' : ''}}>SAT</option>
                    <option value="5" {{ ($previousQualification->APT_Test_4 == 5) ? 'selected' : ''}}>GRE</option>
                    <option value="6" {{ ($previousQualification->APT_Test_4 == 6) ? 'selected' : ''}}>Other</option>
                  </select>
                </div>
                <!-- /.form-group -->
              </div>
              <!-- /.col -->
              <div class="col-md-3">
                <div class="form-group">
                  <label>Year Taken</label>
                  <select class="form-control select2" id="year_4" name="year_4" style="width: 100%;" {{$disabledUser}}>
                      <option value="">Select</option>
                      <option value="1" {{ ($previousQualification->APT_Test_4_Year == 1) ? 'selected' : ''}}>2015</option>
                      <option value="2" {{ ($previousQualification->APT_Test_4_Year == 2) ? 'selected' : ''}}>2016</option>
                      <option value="3" {{ ($previousQualification->APT_Test_4_Year == 3) ? 'selected' : ''}}>2017</option>
                      <option value="4" {{ ($previousQualification->APT_Test_4_Year == 4) ? 'selected' : ''}}>2018</option>
                      <option value="5" {{ ($previousQualification->APT_Test_4_Year == 5) ? 'selected' : ''}}>2019</option>
                    </select>
                </div>
                <!-- /.form-group -->
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label>Obtained Marks</label>
                  <input type="number" min="0" step="1" id="obtained_4" name="obtained_4" value="{{$previousQualification->APT_Test_4_Marks}}" class="form-control" {{$disabledUser}}>
                </div>
              </div>              
              <!-- /.col -->
              <div class="col-md-3">
                <div class="form-group">
                  <label>Total Marks</label>
                  <input type="number" min="0" step="1" id="total_4" name="total_4" value="{{$previousQualification->APT_Test_4_Total_Marks}}" class="form-control" {{$disabledUser}}>
                </div>
              </div>              
              <!-- /.col -->
            </div>
           <!-- /.row -->
          </div>
          <!-- /.box-body -->

          <div class="box-footer">
            <button class="btn btn-social btn-success" {{$disabledUser}} id="aptitude-test-btn" type="submit"><i class="fa fa-save"></i> Save and Move Next</button>
            <a class="btn btn-social btn-dropbox" href="{{ route('familydetail', $hash)}}">
                <i class="fa fa-fast-forward"></i> Skip this step
            </a>
          </div>
        </form>
        </div>
        <!-- /.box --> 
      </section>
      <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

@include('frontend.footer')