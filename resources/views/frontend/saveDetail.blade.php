@section('title')
Save Application
@endsection
@include('frontend.header')
@php
if($applicationStatus->step_save == 1){
  $readonlyUser = "readonly='readonly'";
  $disabledUser = "disabled";
  $disabledUser1 = 'disabled="disabled"';
}else{
  $readonlyUser='';
  $disabledUser = '';
  $disabledUser1 = '';
}
@endphp
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          <i class="fa fa-hdd-o text-yellow"></i> Save
          <small>Add / Edit Save</small>
        </h1>
      </section>
      @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    @if ( $custom_error )
        <div class="alert alert-danger alert-quali" role="alert"> {{ $custom_error }}</div>
    @endif  
      <!-- Main content -->
      <section class="content">
          <form action="{{route('savedetail', $hash)}}" method="POST" onsubmit="groupingSubmit()">
            <input type="hidden" name="userId" value="{{$userdetail->userId}}">
            {{csrf_field()}}
        <!-- SELECT2 EXAMPLE -->
        <div class="box box-info">
          <div class="box-header with-border">
            <h3 class="box-title">Save Form</h3>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <div class="row">
                <div class="col-md-12">
                  <div class="alert alert-success alert-dismissible">
                    <p class="sv-txt"><i class="icon fa fa-check"></i> You've successfully compeleted all application steps. Please click the "Save Application" button to Save your application Online.</p>
                    <p class="sv-txt"><i class="icon fa fa-info"></i> Note: For online admission application, hard copies of the documents are <b>NOT</b> required.</p>
                    <p class="sv-txt"><i class="icon fa fa-info"></i> Note: If you click Save Application button, your application form will be saved online. After that you will not be able to enter/Update any information.</p>
                </div>
              </div>
            </div>
          <!-- /.row -->
          </div>
          <!-- /.box-body -->
          <div class="box-header box box-success">
              <h3 class="box-title">Undertaking</h3>
          </div>
          <div class="box-body">
              <div class="row">
                  <div class="col-md-12">
                    <div class="alert alert-info alert-dismissible under-lrt">
                      <p class="sv-txt">I hereby solemnly declare and affirm that the information provided by me in the Online Admission System System/Admission application Form, is true and correct to the best of my knowldge. I also undertake that I have read and understood all the instructions provided therein and agree to abide by them.</p>
                      <label class="blk-txt">
                        <input type="checkbox" {{($applicationStatus->step_save == 1)? 'checked': ''}} name="save" value="1" id="save" class="flat-red" required {{$disabledUser}}> I hereby solemnly declare that the information provided is correct
                      </label>
                    </div>
                  </div>
              </div>
          </div>
          <div class="box-footer">
            <button class="btn btn-social btn-success" {{$disabledUser}} id="save-detail-btn" type="submit"><i class="fa fa-save"></i> Submit Application</button>
            @if($applicationStatus->step_save == 1)
            <a href="{{ route('application', $hash)}}" class="btn btn-social btn-dropbox">
              <i class="fa fa-eye"></i> View Application
            </a>
            @endif
          </div>
        </div>
        <!-- /.box --> 
      </section>
      <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

@include('frontend.footer')