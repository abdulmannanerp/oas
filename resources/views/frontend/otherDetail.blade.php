@section('title')
Family/Other Details
@endsection
@include('frontend.header')
@php
    if ($applicationStatus->step_save == 1 && $hash == '') {
        // $readonlyUser = "readonly='readonly'";
        // $disabledUser = 'disabled';
        // $disabledUser1 = 'disabled="disabled"';

        $readonlyUser = '';
        $disabledUser = '';
        $disabledUser1 = '';
    } else {
        $readonlyUser = '';
        $disabledUser = '';
        $disabledUser1 = '';
    }
@endphp
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <i class="fa fa-question-circle text-aqua"></i> Family/Other Details
            <small>Add / Edit Other Info</small>
        </h1>
    </section>
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    @if ($status)
        <div class="alert alert-success" role="alert"> {{ $status }}</div>
    @endif

    <!-- Main content -->
    <section class="content">
        <form action="{{ route('otherdetail', $hash) }}" method="POST">
            <input type="hidden" name="userId" value="{{ $userdetail->userId }}">
            {{ csrf_field() }}


            

            <!-- SELECT2 EXAMPLE -->
            <div class="box box-info">

                <div class="box-header">
                    <h3 class="box-title">Enter Family Details</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label class="req-rd">Father Status</label>
                                <select class="form-control select2" id="father_status" required name="father_status"
                                    style="width: 100%;" {{ $disabledUser }}>
                                    <option value="">Select</option>
                                    <option value="1"
                                        {{ $applicantDetail->fatherStatus == 1 || old('father_status') == 1 ? 'selected' : '' }}>
                                        Alive</option>
                                    <option value="2"
                                        {{ $applicantDetail->fatherStatus == 2 || old('father_status') == 2 ? 'selected' : '' }}>
                                        Deceased</option>
                                </select>
                            </div>
                            <!-- /.form-group -->
                        </div>
                        <!-- /.col -->
                        <div class="col-md-3">
                            <div class="form-group">
                                <label class="req-rd">No. of Dependent</label>
                                <input type="number" id="dependent"
                                    value="{{ $applicantDetail->dependants ?? old('dependent') }}" name="dependent"
                                    required class="form-control" {{ $disabledUser }}>
                            </div>
                            <!-- /.form-group -->
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label class="req-rd">Monthly Income</label>
                                <input type="number" id="monthly_income"
                                    value="{{ $applicantDetail->monthlyIncome ?? old('monthly_income') }}"
                                    name="monthly_income" required class="form-control" {{ $disabledUser }}>
                            </div>
                        </div>
                        <!-- /.col -->
                        <div class="col-md-3">
                            <div class="form-group">
                                <label class="req-rd">Guardian/Spouse Name</label>
                                <input type="text" id="guardian_spouse" name="guardian_spouse"
                                    value="{{ $applicantDetail->Guradian_Spouse ?? old('guardian_spouse') }}"
                                    required class="form-control" {{ $disabledUser }}>
                            </div>
                        </div>
                        <!-- /.col -->
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label class="req-rd">Father/Guardian Occupation</label>
                                <input type="text" id="father_occupation" name="father_occupation"
                                    value="{{ $applicantDetail->fatherOccupation ?? old('father_occupation') }}"
                                    required class="form-control" {{ $disabledUser }}>
                            </div>
                            <!-- /.form-group -->
                        </div>
                        <!-- /.col -->
                        <div class="col-md-3">
                            <div class="form-group">
                                <label class="req-rd">Father/Guardian Phone</label>
                                <input type="number" id="father_phone" name="father_phone"
                                    value="{{ $applicantDetail->fatherPhone ?? old('father_phone') }}" required
                                    class="form-control" {{ $disabledUser }}>
                            </div>
                            <!-- /.form-group -->
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label class="req-rd">Father/Guardian Mobile</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-phone"></i>
                                    </div>
                                    @php
                                        $carrier = '';
                                        $phone = '';
                                        if ($applicantDetail->fatherMobile) {
                                            $carrier = substr($applicantDetail->fatherMobile, 2, 3);
                                            $phone = substr($applicantDetail->fatherMobile, 5, 7);
                                        }
                                    @endphp
                                    @if ($userdetail->fkNationality == 1)
                                        <select name="carrier" class="form-control" required style="width: 40%;"
                                            {{ $disabledUser }}>
                                            <option value="">Code</option>
                                            <option value="300"
                                                {{ $carrier == '300' || old('carrier') == '300' ? 'selected' : '' }}>
                                                0300</option>
                                            <option value="301"
                                                {{ $carrier == '301' || old('carrier') == '301' ? 'selected' : '' }}>
                                                0301</option>
                                            <option value="302"
                                                {{ $carrier == '302' || old('carrier') == '302' ? 'selected' : '' }}>
                                                0302</option>
                                            <option value="303"
                                                {{ $carrier == '303' || old('carrier') == '303' ? 'selected' : '' }}>
                                                0303</option>
                                            <option value="304"
                                                {{ $carrier == '304' || old('carrier') == '304' ? 'selected' : '' }}>
                                                0304</option>
                                            <option value="305"
                                                {{ $carrier == '305' || old('carrier') == '305' ? 'selected' : '' }}>
                                                0305</option>
                                            <option value="306"
                                                {{ $carrier == '306' || old('carrier') == '306' ? 'selected' : '' }}>
                                                0306</option>
                                            <option value="307"
                                                {{ $carrier == '307' || old('carrier') == '307' ? 'selected' : '' }}>
                                                0307</option>
                                            <option value="308"
                                                {{ $carrier == '308' || old('carrier') == '308' ? 'selected' : '' }}>
                                                0308</option>
                                            <option value="309"
                                                {{ $carrier == '309' || old('carrier') == '309' ? 'selected' : '' }}>
                                                0309</option>
                                            <option value="310"
                                                {{ $carrier == '310' || old('carrier') == '310' ? 'selected' : '' }}>
                                                0310</option>
                                            <option value="311"
                                                {{ $carrier == '311' || old('carrier') == '311' ? 'selected' : '' }}>
                                                0311</option>
                                            <option value="312"
                                                {{ $carrier == '312' || old('carrier') == '312' ? 'selected' : '' }}>
                                                0312</option>
                                            <option value="313"
                                                {{ $carrier == '313' || old('carrier') == '313' ? 'selected' : '' }}>
                                                0313</option>
                                            <option value="314"
                                                {{ $carrier == '314' || old('carrier') == '314' ? 'selected' : '' }}>
                                                0314</option>
                                            <option value="315"
                                                {{ $carrier == '315' || old('carrier') == '315' ? 'selected' : '' }}>
                                                0315</option>
                                            <option value="316"
                                                {{ $carrier == '316' || old('carrier') == '316' ? 'selected' : '' }}>
                                                0316</option>
                                            <option value="317"
                                                {{ $carrier == '317' || old('carrier') == '317' ? 'selected' : '' }}>
                                                0317</option>
                                            <option value="318"
                                                {{ $carrier == '318' || old('carrier') == '318' ? 'selected' : '' }}>
                                                0318</option>
                                            <option value="319"
                                                {{ $carrier == '319' || old('carrier') == '319' ? 'selected' : '' }}>0319
                                            </option>
                                            <option value="320"
                                                {{ $carrier == '320' || old('carrier') == '320' ? 'selected' : '' }}>
                                                0320</option>
                                            <option value="321"
                                                {{ $carrier == '321' || old('carrier') == '321' ? 'selected' : '' }}>
                                                0321</option>
                                            <option value="322"
                                                {{ $carrier == '322' || old('carrier') == '322' ? 'selected' : '' }}>
                                                0322</option>
                                            <option value="323"
                                                {{ $carrier == '323' || old('carrier') == '323' ? 'selected' : '' }}>
                                                0323</option>
                                            <option value="324"
                                                {{ $carrier == '324' || old('carrier') == '324' ? 'selected' : '' }}>
                                                0324</option>
                                            <option value="327"
                                                {{ $carrier == '327' || old('carrier') == '327' ? 'selected' : '' }}>0327
                                            </option>
                                            <option value="330"
                                                {{ $carrier == '330' || old('carrier') == '330' ? 'selected' : '' }}>
                                                0330</option>
                                            <option value="331"
                                                {{ $carrier == '331' || old('carrier') == '331' ? 'selected' : '' }}>
                                                0331</option>
                                            <option value="332"
                                                {{ $carrier == '332' || old('carrier') == '332' ? 'selected' : '' }}>
                                                0332</option>
                                            <option value="333"
                                                {{ $carrier == '333' || old('carrier') == '333' ? 'selected' : '' }}>
                                                0333</option>
                                            <option value="334"
                                                {{ $carrier == '334' || old('carrier') == '334' ? 'selected' : '' }}>
                                                0334</option>
                                            <option value="335"
                                                {{ $carrier == '335' || old('carrier') == '335' ? 'selected' : '' }}>
                                                0335</option>
                                            <option value="336"
                                                {{ $carrier == '336' || old('carrier') == '336' ? 'selected' : '' }}>
                                                0336</option>
                                            <option value="337"
                                                {{ $carrier == '337' || old('carrier') == '337' ? 'selected' : '' }}>
                                                0337</option>
                                            <option value="340"
                                                {{ $carrier == '340' || old('carrier') == '340' ? 'selected' : '' }}>
                                                0340</option>
                                            <option value="341"
                                                {{ $carrier == '341' || old('carrier') == '341' ? 'selected' : '' }}>
                                                0341</option>
                                            <option value="342"
                                                {{ $carrier == '342' || old('carrier') == '342' ? 'selected' : '' }}>
                                                0342</option>
                                            <option value="343"
                                                {{ $carrier == '343' || old('carrier') == '343' ? 'selected' : '' }}>
                                                0343</option>
                                            <option value="344"
                                                {{ $carrier == '344' || old('carrier') == '344' ? 'selected' : '' }}>
                                                0344</option>
                                            <option value="345"
                                                {{ $carrier == '345' || old('carrier') == '345' ? 'selected' : '' }}>
                                                0345</option>
                                            <option value="346"
                                                {{ $carrier == '346' || old('carrier') == '346' ? 'selected' : '' }}>
                                                0346</option>
                                            <option value="347"
                                                {{ $carrier == '347' || old('carrier') == '347' ? 'selected' : '' }}>
                                                0347</option>
                                            <option value="348"
                                                {{ $carrier == '348' || old('carrier') == '348' ? 'selected' : '' }}>
                                                0348</option>
                                            <option value="349"
                                                {{ $carrier == '349' || old('carrier') == '349' ? 'selected' : '' }}>
                                                0349</option>
                                        </select>
                                        <input type="text" class="form-control"
                                            value="{{ $phone ?: old('father_mobile') }}" minlength="7"
                                            maxlength="7" id="father_mobile" name="father_mobile" required
                                            style="width: 60%;" onkeypress="return isNumberKey(event)"
                                            {{ $disabledUser }}>
                                    @endif
                                    @if ($userdetail->fkNationality == 2 || $userdetail->fkNationality == 3)
                                        <input type="number" name="carrier" id="carrier" class="form-control"
                                            value="{{ $applicantDetail->fatherMobile ?? old('carrier') }}" required
                                            style="width: 100%;" {{ $disabledUser }}>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <!-- /.col -->
                        <div class="col-md-3">
                            <div class="form-group">
                                <label class="req-rd">Father/Guardian, CNIC/Passport</label>
                                <input type="text" id="father_cnic" maxlength="13" minlength="9"
                                    name="father_cnic"
                                    value="{{ $applicantDetail->father_cnic ?? old('father_cnic') }}" required
                                    class="form-control" {{ $disabledUser }}>
                            </div>
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.box-body -->

                
                <div class="box-header box box-success">
                    <h3 class="box-title">Enter Other Details</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="req-rd">Blood Group</label>
                                <select class="form-control select2" id="blood_group" name="blood_group"
                                    style="width: 100%;" required {{ $disabledUser }}>
                                    <option value="">Select</option>
                                    <option value="A+"
                                        {{ $applicantDetail->blood == 'A+' || old('blood_group') == 'A+' ? 'selected' : '' }}>
                                        A+</option>
                                    <option value="A-"
                                        {{ $applicantDetail->blood == 'A-' || old('blood_group') == 'A-' ? 'selected' : '' }}>
                                        A-</option>
                                    <option value="B+"
                                        {{ $applicantDetail->blood == 'B+' || old('blood_group') == 'B+' ? 'selected' : '' }}>
                                        B+</option>
                                    <option value="B-"
                                        {{ $applicantDetail->blood == 'B-' || old('blood_group') == 'B-' ? 'selected' : '' }}>
                                        B-</option>
                                    <option value="O+"
                                        {{ $applicantDetail->blood == 'O+' || old('blood_group') == 'O+' ? 'selected' : '' }}>
                                        O+</option>
                                    <option value="O-"
                                        {{ $applicantDetail->blood == 'O-' || old('blood_group') == 'O-' ? 'selected' : '' }}>
                                        O-</option>
                                    <option value="AB+"
                                        {{ $applicantDetail->blood == 'AB+' || old('blood_group') == 'AB+' ? 'selected' : '' }}>
                                        AB+</option>
                                    <option value="AB-"
                                        {{ $applicantDetail->blood == 'AB-' || old('blood_group') == 'AB-' ? 'selected' : '' }}>
                                        AB-</option>
                                </select>
                            </div>
                            <!-- /.form-group -->
                        </div>
                        <!-- /.col -->
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="req-rd">Marital Status</label>
                                <select class="form-control select2" id="marital_status" name="marital_status"
                                    style="width: 100%;" required {{ $disabledUser }}>
                                    <option value="">Select</option>
                                    <option value="Single"
                                        {{ $applicantDetail->married == 'Single' || old('marital_status') == 'Single' ? 'selected' : '' }}>
                                        Single</option>
                                    <option value="Married"
                                        {{ $applicantDetail->married == 'Married' || old('marital_status') == 'Married' ? 'selected' : '' }}>
                                        Married</option>
                                    <option value="Separated/Divorced"
                                        {{ $applicantDetail->married == 'Separated/Divorced' || old('marital_status') == 'Separated/Divorced' ? 'selected' : '' }}>
                                        Separated/Divorced</option>
                                    <option value="Widowed"
                                        {{ $applicantDetail->married == 'Widowed' || old('marital_status') == 'Widowed' ? 'selected' : '' }}>
                                        Widowed</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Twitter URL</label>
                                <input type="text" id="twitter_url"
                                    value="{{ $applicantDetail->twitter_url ?? old('twitter_url') }}"
                                    name="twitter_url" class="form-control" {{ $disabledUser }}>
                            </div>
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->
                    <div class="row">
                        {{-- <div class="col-md-4">
                  <div class="form-group">
                    <label class="req-rd">Religion</label>
                    <select class="form-control select2" id="religion" name="religion" style="width: 100%;" required {{$disabledUser}}>
                        <option value="">Select</option>
                        <option value="Ahamdi" {{ ($applicantDetail->religion == 'Ahamdi' || old('religion') == 'Ahamdi') ? 'selected' : ''}}>Ahamdi</option>
                        <option value="Budhist" {{ ($applicantDetail->religion == 'Budhist' || old('religion') == 'Budhist') ? 'selected' : ''}}>Budhist</option>
                        <option value="Christianity" {{ ($applicantDetail->religion == 'Christianity' || old('religion') == 'Christianity') ? 'selected' : ''}}>Christianity</option>
                        <option value="Hinduism" {{ ($applicantDetail->religion == 'Hinduism' || old('religion') == 'Hinduism') ? 'selected' : ''}}>Hinduism</option>
                        <option value="Islam" {{ ($applicantDetail->religion == 'Islam' || $applicantDetail->religion == '') ? 'selected' : ''}}>Islam</option>                     
                        <option value="Parsi" {{ ($applicantDetail->religion == 'Parsi' || old('religion') == 'Parsi') ? 'selected' : ''}}>Parsi</option>
                        <option value="Sikh" {{ ($applicantDetail->religion == 'Sikh' || old('religion') == 'Sikh') ? 'selected' : ''}}>Sikh</option>
                        <option value="Others" {{ ($applicantDetail->religion == 'Others' || old('religion') == 'Others') ? 'selected' : ''}}>Others</option>
                    </select>
                  </div>
                </div>  --}}
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Facebook URL</label>
                                <input type="text" id="facebook_url"
                                    value="{{ $applicantDetail->facebook_url ?? old('facebook_url') }}"
                                    name="facebook_url" class="form-control" {{ $disabledUser }}>
                            </div>
                        </div>
                        <div class="col-md-4">
                          <div class="form-group">
                              <label>Have you been Student of IIU before ?</label>
                              <label>
                                  <input type="radio" name="pre_student" value="1"
                                      {{ $applicantDetail->xStudent != '' ? 'checked' : '' }}
                                      class="flat-red form-control pre_student" {{ $disabledUser }}> Yes
                              </label>
                              <label>
                                  <input type="radio" name="pre_student" value="0"
                                      {{ $applicantDetail->xStudent == '' ? 'checked' : '' }}
                                      class="flat-red form-control pre_student" {{ $disabledUser }}> No
                              </label>
                              <input type="text" class="form-control" id="reg_no"
                                  {{ $applicantDetail->xStudent != '' ? 'style=display:block;' : 'style=display:none;' }}
                                  value="{{ $applicantDetail->xStudent }}" name="reg_no"
                                  placeholder="IIU Old Registration Number" {{ $disabledUser }}>
                          </div>
                      </div>
                    </div>
                    {{-- <div class="row">
              <div class="col-md-12">
                  <div class="form-group">
                    <label class="req-rd">How did you find us?</label>
                    <label>
                      <input type="radio" name="find_us" value="Newspaper" {{($applicantDetail->find_us == 'Newspaper')? 'checked': ''}} class="flat-red form-control" {{$disabledUser}} required> Newspaper
                    </label>
                    <label>
                      <input type="radio" name="find_us" value="Facebook" {{($applicantDetail->find_us == 'Facebook')? 'checked': ''}} class="flat-red form-control" {{$disabledUser}}> Facebook
                    </label>
                    <label>
                        <input type="radio" name="find_us" value="Twitter" {{($applicantDetail->find_us == 'Twitter')? 'checked': ''}} class="flat-red form-control" {{$disabledUser}}> Twitter
                    </label>
                    <label>
                        <input type="radio" name="find_us" value="IIU Website" {{($applicantDetail->find_us == 'IIU Website')? 'checked': ''}} class="flat-red form-control" {{$disabledUser}}> IIU Website
                    </label>
                    <label>
                        <input type="radio" name="find_us" value="Other" {{($applicantDetail->find_us == 'Other')? 'checked': ''}} class="flat-red form-control" {{$disabledUser}}> Other
                    </label>                                        
                  </div>
                </div> 
           </div> --}}









                    {{-- Dev Mannan: Address code starts --}}
                    <div class="box-header with-border box box-info">
                        <h3 class="box-title">Permanent Address</h3>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>City</label>
                                    @if ($userdetail->fkNationality == 2 || $userdetail->fkNationality == 3)
                                        <input class="form-control" name="permanent_city" id="permanent_city"
                                            value="{{ $applicantAddress->cityPmt == '' ? old('permanent_city') : $applicantAddress->cityPmt }}"
                                            style="width: 100%;" required {{ $disabledUser }}>
                                    @endif
                                    @if ($userdetail->fkNationality == 1)
                                        <select class="form-control select2" name="permanent_city" id="permanent_city"
                                            style="width: 100%;" required {{ $disabledUser }}>
                                            <option value="">Select City</option>
                                            @foreach ($city as $n)
                                                <option value="{{ $n->city }}"
                                                    {{ $applicantAddress->cityPmt == $n->city || old('permanent_city') == $n->city ? 'selected' : '' }}>
                                                    {{ $n->city }}</option>
                                            @endforeach
                                        </select>
                                    @endif
                                </div>
                                <!-- /.form-group -->
                            </div>
                            <!-- /.col -->
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Phone</label>
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-phone"></i>
                                        </div>
                                        @php
                                            $carrier = '';
                                            $phone = '';
                                            if ($applicantAddress->phonePmt) {
                                                $carrier = substr($applicantAddress->phonePmt, 2, 3);
                                                $phone = substr($applicantAddress->phonePmt, 5, 7);
                                            }
                                        @endphp
                                        {{-- @if ($userdetail->fkNationality == 1) --}}
                                        <select name="permanent_carrier" id="permanent_carrier"
                                            class="form-control {{ $userdetail->fkNationality == 1 ? 'ext-show' : 'ext-hide-2' }}"
                                            {{ $userdetail->fkNationality == 1 ? 'required' : '' }}
                                            style="width: 40%;" {{ $disabledUser1 }}>
                                            <option value="">Code</option>
                                            <option value="300"
                                                {{ $carrier == '300' || old('carrier') == '300' ? 'selected' : '' }}>
                                                0300</option>
                                            <option value="301"
                                                {{ $carrier == '301' || old('carrier') == '301' ? 'selected' : '' }}>
                                                0301</option>
                                            <option value="302"
                                                {{ $carrier == '302' || old('carrier') == '302' ? 'selected' : '' }}>
                                                0302</option>
                                            <option value="303"
                                                {{ $carrier == '303' || old('carrier') == '303' ? 'selected' : '' }}>
                                                0303</option>
                                            <option value="304"
                                                {{ $carrier == '304' || old('carrier') == '304' ? 'selected' : '' }}>
                                                0304</option>
                                            <option value="305"
                                                {{ $carrier == '305' || old('carrier') == '305' ? 'selected' : '' }}>
                                                0305</option>
                                            <option value="306"
                                                {{ $carrier == '306' || old('carrier') == '306' ? 'selected' : '' }}>
                                                0306</option>
                                            <option value="307"
                                                {{ $carrier == '307' || old('carrier') == '307' ? 'selected' : '' }}>
                                                0307</option>
                                            <option value="308"
                                                {{ $carrier == '308' || old('carrier') == '308' ? 'selected' : '' }}>
                                                0308</option>
                                            <option value="309"
                                                {{ $carrier == '309' || old('carrier') == '309' ? 'selected' : '' }}>
                                                0309</option>
                                            <option value="310"
                                                {{ $carrier == '310' || old('carrier') == '310' ? 'selected' : '' }}>
                                                0310</option>
                                            <option value="311"
                                                {{ $carrier == '311' || old('carrier') == '311' ? 'selected' : '' }}>
                                                0311</option>
                                            <option value="312"
                                                {{ $carrier == '312' || old('carrier') == '312' ? 'selected' : '' }}>
                                                0312</option>
                                            <option value="313"
                                                {{ $carrier == '313' || old('carrier') == '313' ? 'selected' : '' }}>
                                                0313</option>
                                            <option value="314"
                                                {{ $carrier == '314' || old('carrier') == '314' ? 'selected' : '' }}>
                                                0314</option>
                                            <option value="315"
                                                {{ $carrier == '315' || old('carrier') == '315' ? 'selected' : '' }}>
                                                0315</option>
                                            <option value="316"
                                                {{ $carrier == '316' || old('carrier') == '316' ? 'selected' : '' }}>
                                                0316</option>
                                            <option value="317"
                                                {{ $carrier == '317' || old('carrier') == '317' ? 'selected' : '' }}>
                                                0317</option>
                                            <option value="318"
                                                {{ $carrier == '318' || old('carrier') == '318' ? 'selected' : '' }}>
                                                0318</option>
                                            <option value="319"
                                                {{ $carrier == '319' || old('carrier') == '319' ? 'selected' : '' }}>0319
                                            </option>
                                            <option value="320"
                                                {{ $carrier == '320' || old('carrier') == '320' ? 'selected' : '' }}>
                                                0320</option>
                                            <option value="321"
                                                {{ $carrier == '321' || old('carrier') == '321' ? 'selected' : '' }}>
                                                0321</option>
                                            <option value="322"
                                                {{ $carrier == '322' || old('carrier') == '322' ? 'selected' : '' }}>
                                                0322</option>
                                            <option value="323"
                                                {{ $carrier == '323' || old('carrier') == '323' ? 'selected' : '' }}>
                                                0323</option>
                                            <option value="324"
                                                {{ $carrier == '324' || old('carrier') == '324' ? 'selected' : '' }}>
                                                0324</option>
                                            <option value="327"
                                                {{ $carrier == '327' || old('carrier') == '327' ? 'selected' : '' }}>0327
                                            </option>
                                            <option value="330"
                                                {{ $carrier == '330' || old('carrier') == '330' ? 'selected' : '' }}>
                                                0330</option>
                                            <option value="331"
                                                {{ $carrier == '331' || old('carrier') == '331' ? 'selected' : '' }}>
                                                0331</option>
                                            <option value="332"
                                                {{ $carrier == '332' || old('carrier') == '332' ? 'selected' : '' }}>
                                                0332</option>
                                            <option value="333"
                                                {{ $carrier == '333' || old('carrier') == '333' ? 'selected' : '' }}>
                                                0333</option>
                                            <option value="334"
                                                {{ $carrier == '334' || old('carrier') == '334' ? 'selected' : '' }}>
                                                0334</option>
                                            <option value="335"
                                                {{ $carrier == '335' || old('carrier') == '335' ? 'selected' : '' }}>
                                                0335</option>
                                            <option value="336"
                                                {{ $carrier == '336' || old('carrier') == '336' ? 'selected' : '' }}>
                                                0336</option>
                                            <option value="337"
                                                {{ $carrier == '337' || old('carrier') == '337' ? 'selected' : '' }}>
                                                0337</option>
                                            <option value="340"
                                                {{ $carrier == '340' || old('carrier') == '340' ? 'selected' : '' }}>
                                                0340</option>
                                            <option value="341"
                                                {{ $carrier == '341' || old('carrier') == '341' ? 'selected' : '' }}>
                                                0341</option>
                                            <option value="342"
                                                {{ $carrier == '342' || old('carrier') == '342' ? 'selected' : '' }}>
                                                0342</option>
                                            <option value="343"
                                                {{ $carrier == '343' || old('carrier') == '343' ? 'selected' : '' }}>
                                                0343</option>
                                            <option value="344"
                                                {{ $carrier == '344' || old('carrier') == '344' ? 'selected' : '' }}>
                                                0344</option>
                                            <option value="345"
                                                {{ $carrier == '345' || old('carrier') == '345' ? 'selected' : '' }}>
                                                0345</option>
                                            <option value="346"
                                                {{ $carrier == '346' || old('carrier') == '346' ? 'selected' : '' }}>
                                                0346</option>
                                            <option value="347"
                                                {{ $carrier == '347' || old('carrier') == '347' ? 'selected' : '' }}>
                                                0347</option>
                                            <option value="348"
                                                {{ $carrier == '348' || old('carrier') == '348' ? 'selected' : '' }}>
                                                0348</option>
                                            <option value="349"
                                                {{ $carrier == '349' || old('carrier') == '349' ? 'selected' : '' }}>
                                                0349</option>
                                        </select>
                                        <input type="text"
                                            class="form-control {{ $userdetail->fkNationality == 1 ? 'ext-show' : 'ext-hide-2' }}"
                                            style="width: 60%;" onkeypress="return isNumberKey(event)"
                                            id="permanent_phone" name="permanent_phone" minlength="7"
                                            maxlength="7" value="{{ $phone ?: old('permanent_phone') }}"
                                            {{ $userdetail->fkNationality == 1 ? 'required' : '' }}
                                            {{ $disabledUser }}>
                                        {{-- @endif 
                      @if ($userdetail->fkNationality == 2 || $userdetail->fkNationality == 3) --}}
                                        <input type="number" name="overseas_permanent_carrier"
                                            id="overseas_permanent_carrier"
                                            class="form-control {{ $userdetail->fkNationality == 2 || $userdetail->fkNationality == 3 ? 'ext-show' : 'ext-hide-2' }}"
                                            value="{{ $applicantAddress->phonePmt ?? old('overseas_permanent_carrier') }}"
                                            {{ $userdetail->fkNationality == 2 || $userdetail->fkNationality == 3 ? 'required' : '' }}
                                            style="width: 100%;" {{ $disabledUser }}>
                                        {{-- @endif                       --}}
                                    </div>
                                </div>
                                <!-- /.form-group -->
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Address</label>
                                    <textarea class="form-control" value="{{ $applicantAddress->addressPmt ?? old('permanent_address') }}"
                                        name="permanent_address" id="permanent_address" rows="1" required {{ $disabledUser }}>{{ $applicantAddress->addressPmt ?? old('permanent_address') }}</textarea>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="no-comp">Area</label>
                                    <select class="form-control select2" name="permanent_area" id="permanent_area"
                                        style="width: 100%;" {{ $disabledUser }}>
                                        <option value="">Select Area</option>
                                        <option value="Rural"
                                            {{ $applicantAddress->areaPmt == 'Rural' || old('permanent_area') == 'Rural' ? 'selected' : '' }}>
                                            Rural</option>
                                        <option value="Urban"
                                            {{ $applicantAddress->areaPmt == 'Urban' || old('permanent_area') == 'Urban' ? 'selected' : '' }}>
                                            Urban</option>
                                    </select>
                                </div>
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </div>

                    <div class="box-header box box-success">
                        <h3 class="box-title">
                            @if ($userdetail->fkNationality == 2 || $userdetail->fkNationality == 3)
                                {{ 'Pakistan Address' }}
                            @else
                                {{ 'Mailing Address' }}
                            @endif
                        </h3>
                        <h5>
                            <label class="nohash">
                                <input type="checkbox" id="mailing_check" class="minimal"
                                    {{ $applicantAddress->phonePo == $applicantAddress->phonePmt && $applicantAddress->phonePmt != '' ? 'checked' : '' }}
                                    {{ $disabledUser }}>
                                Same as Permanent Address
                            </label>
                        </h5>
                    </div>

                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>City</label>
                                    @if ($userdetail->fkNationality == 2 || $userdetail->fkNationality == 3)
                                        <input class="form-control" name="mailing_city" id="mailing_city"
                                            value="{{ $applicantAddress->cityPo == '' ? old('mailing_city') : $applicantAddress->cityPo }}"
                                            style="width: 100%;" required {{ $disabledUser }}>
                                    @endif
                                    @if ($userdetail->fkNationality == 1)
                                        <select class="form-control select2" name="mailing_city" id="mailing_city"
                                            style="width: 100%;" required {{ $disabledUser }}>
                                            <option value="">Select City</option>
                                            @foreach ($city as $n)
                                                <option value="{{ $n->city }}"
                                                    {{ $applicantAddress->cityPo == $n->city || old('mailing_city') == $n->city ? 'selected' : '' }}>
                                                    {{ $n->city }}</option>
                                            @endforeach
                                        </select>
                                    @endif
                                </div>
                                <!-- /.form-group -->
                            </div>
                            <!-- /.col -->
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Phone</label>
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-phone"></i>
                                        </div>
                                        @php
                                            $carrier = '';
                                            $phone = '';
                                            if ($applicantAddress->phonePo) {
                                                $carrier = substr($applicantAddress->phonePo, 2, 3);
                                                $phone = substr($applicantAddress->phonePo, 5, 7);
                                            }
                                        @endphp
                                        {{-- @if ($userdetail->fkNationality == 1) --}}
                                        <select name="mailing_carrier" id="mailing_carrier"
                                            class="form-control {{ $userdetail->fkNationality == 1 ? 'ext-show' : 'ext-hide-2' }}"
                                            {{ $userdetail->fkNationality == 1 ? 'required' : '' }}
                                            style="width: 40%;" {{ $disabledUser }}>
                                            <option value="">Code</option>
                                            <option value="300"
                                                {{ $carrier == '300' || old('carrier') == '300' ? 'selected' : '' }}>
                                                0300</option>
                                            <option value="301"
                                                {{ $carrier == '301' || old('carrier') == '301' ? 'selected' : '' }}>
                                                0301</option>
                                            <option value="302"
                                                {{ $carrier == '302' || old('carrier') == '302' ? 'selected' : '' }}>
                                                0302</option>
                                            <option value="303"
                                                {{ $carrier == '303' || old('carrier') == '303' ? 'selected' : '' }}>
                                                0303</option>
                                            <option value="304"
                                                {{ $carrier == '304' || old('carrier') == '304' ? 'selected' : '' }}>
                                                0304</option>
                                            <option value="305"
                                                {{ $carrier == '305' || old('carrier') == '305' ? 'selected' : '' }}>
                                                0305</option>
                                            <option value="306"
                                                {{ $carrier == '306' || old('carrier') == '306' ? 'selected' : '' }}>
                                                0306</option>
                                            <option value="307"
                                                {{ $carrier == '307' || old('carrier') == '307' ? 'selected' : '' }}>
                                                0307</option>
                                            <option value="308"
                                                {{ $carrier == '308' || old('carrier') == '308' ? 'selected' : '' }}>
                                                0308</option>
                                            <option value="309"
                                                {{ $carrier == '309' || old('carrier') == '309' ? 'selected' : '' }}>
                                                0309</option>
                                            <option value="310"
                                                {{ $carrier == '310' || old('carrier') == '310' ? 'selected' : '' }}>
                                                0310</option>
                                            <option value="311"
                                                {{ $carrier == '311' || old('carrier') == '311' ? 'selected' : '' }}>
                                                0311</option>
                                            <option value="312"
                                                {{ $carrier == '312' || old('carrier') == '312' ? 'selected' : '' }}>
                                                0312</option>
                                            <option value="313"
                                                {{ $carrier == '313' || old('carrier') == '313' ? 'selected' : '' }}>
                                                0313</option>
                                            <option value="314"
                                                {{ $carrier == '314' || old('carrier') == '314' ? 'selected' : '' }}>
                                                0314</option>
                                            <option value="315"
                                                {{ $carrier == '315' || old('carrier') == '315' ? 'selected' : '' }}>
                                                0315</option>
                                            <option value="316"
                                                {{ $carrier == '316' || old('carrier') == '316' ? 'selected' : '' }}>
                                                0316</option>
                                            <option value="317"
                                                {{ $carrier == '317' || old('carrier') == '317' ? 'selected' : '' }}>
                                                0317</option>
                                            <option value="318"
                                                {{ $carrier == '318' || old('carrier') == '318' ? 'selected' : '' }}>
                                                0318</option>
                                            <option value="319"
                                                {{ $carrier == '319' || old('carrier') == '319' ? 'selected' : '' }}>0319
                                            </option>
                                            <option value="320"
                                                {{ $carrier == '320' || old('carrier') == '320' ? 'selected' : '' }}>
                                                0320</option>
                                            <option value="321"
                                                {{ $carrier == '321' || old('carrier') == '321' ? 'selected' : '' }}>
                                                0321</option>
                                            <option value="322"
                                                {{ $carrier == '322' || old('carrier') == '322' ? 'selected' : '' }}>
                                                0322</option>
                                            <option value="323"
                                                {{ $carrier == '323' || old('carrier') == '323' ? 'selected' : '' }}>
                                                0323</option>
                                            <option value="324"
                                                {{ $carrier == '324' || old('carrier') == '324' ? 'selected' : '' }}>
                                                0324</option>
                                            <option value="327"
                                                {{ $carrier == '327' || old('carrier') == '327' ? 'selected' : '' }}>0327
                                            </option>
                                            <option value="330"
                                                {{ $carrier == '330' || old('carrier') == '330' ? 'selected' : '' }}>
                                                0330</option>
                                            <option value="331"
                                                {{ $carrier == '331' || old('carrier') == '331' ? 'selected' : '' }}>
                                                0331</option>
                                            <option value="332"
                                                {{ $carrier == '332' || old('carrier') == '332' ? 'selected' : '' }}>
                                                0332</option>
                                            <option value="333"
                                                {{ $carrier == '333' || old('carrier') == '333' ? 'selected' : '' }}>
                                                0333</option>
                                            <option value="334"
                                                {{ $carrier == '334' || old('carrier') == '334' ? 'selected' : '' }}>
                                                0334</option>
                                            <option value="335"
                                                {{ $carrier == '335' || old('carrier') == '335' ? 'selected' : '' }}>
                                                0335</option>
                                            <option value="336"
                                                {{ $carrier == '336' || old('carrier') == '336' ? 'selected' : '' }}>
                                                0336</option>
                                            <option value="337"
                                                {{ $carrier == '337' || old('carrier') == '337' ? 'selected' : '' }}>
                                                0337</option>
                                            <option value="340"
                                                {{ $carrier == '340' || old('carrier') == '340' ? 'selected' : '' }}>
                                                0340</option>
                                            <option value="341"
                                                {{ $carrier == '341' || old('carrier') == '341' ? 'selected' : '' }}>
                                                0341</option>
                                            <option value="342"
                                                {{ $carrier == '342' || old('carrier') == '342' ? 'selected' : '' }}>
                                                0342</option>
                                            <option value="343"
                                                {{ $carrier == '343' || old('carrier') == '343' ? 'selected' : '' }}>
                                                0343</option>
                                            <option value="344"
                                                {{ $carrier == '344' || old('carrier') == '344' ? 'selected' : '' }}>
                                                0344</option>
                                            <option value="345"
                                                {{ $carrier == '345' || old('carrier') == '345' ? 'selected' : '' }}>
                                                0345</option>
                                            <option value="346"
                                                {{ $carrier == '346' || old('carrier') == '346' ? 'selected' : '' }}>
                                                0346</option>
                                            <option value="347"
                                                {{ $carrier == '347' || old('carrier') == '347' ? 'selected' : '' }}>
                                                0347</option>
                                            <option value="348"
                                                {{ $carrier == '348' || old('carrier') == '348' ? 'selected' : '' }}>
                                                0348</option>
                                            <option value="349"
                                                {{ $carrier == '349' || old('carrier') == '349' ? 'selected' : '' }}>
                                                0349</option>
                                        </select>
                                        <input type="text"
                                            class="form-control {{ $userdetail->fkNationality == 1 ? 'ext-show' : 'ext-hide-2' }}"
                                            style="width: 60%;" onkeypress="return isNumberKey(event)"
                                            id="mailing_phone" name="mailing_phone" minlength="7" maxlength="7"
                                            value="{{ $phone ?: old('mailing_phone') }}"
                                            {{ $userdetail->fkNationality == 1 ? 'required' : '' }}
                                            {{ $disabledUser }}>
                                        {{-- @endif 
                      @if ($userdetail->fkNationality == 2 || $userdetail->fkNationality == 3) --}}
                                        <input type="number" name="overseas_mailing_carrier"
                                            id="overseas_mailing_carrier"
                                            class="form-control {{ $userdetail->fkNationality == 2 || $userdetail->fkNationality == 3 ? 'ext-show' : 'ext-hide-2' }}"
                                            value="{{ $applicantAddress->phonePo ?? old('overseas_mailing_carrier') }}"
                                            {{ $userdetail->fkNationality == 2 || $userdetail->fkNationality == 3 ? 'required' : '' }}
                                            style="width: 100%;" {{ $disabledUser }}>
                                        {{-- @endif   --}}
                                    </div>
                                </div>
                                <!-- /.form-group -->
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Address</label>
                                    <textarea class="form-control" value="{{ $applicantAddress->addressPo ?? old('mailing_address') }}"
                                        name="mailing_address" id="mailing_address" rows="1" required {{ $disabledUser }}>{{ $applicantAddress->addressPo ?? old('mailing_address') }}</textarea>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="no-comp">Area</label>
                                    <select class="form-control select2" name="mailing_area" id="mailing_area"
                                        style="width: 100%;" {{ $disabledUser }}>
                                        <option value="">Select Area</option>
                                        <option value="Rural"
                                            {{ $applicantAddress->areaPo == 'Rural' || old('mailing_area') == 'Rural' ? 'selected' : '' }}>
                                            Rural</option>
                                        <option value="Urban"
                                            {{ $applicantAddress->areaPo == 'Urban' || old('mailing_area') == 'Urban' ? 'selected' : '' }}>
                                            Urban</option>
                                    </select>
                                </div>
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </div>
                    {{-- Dev Mannan: Address code ends --}}











                </div>
                <!-- /.box-body -->
                {{-- <div class="box-header box box-success">
            <h3 class="box-title"> Hobbies (if any)</h3>
          </div>
          <div class="box-body">
            <div class="row">
              <div class="col-md-4">
                <div class="form-group">
                  <label>Hobbie 1</label>
                  <input type="text" id="hobby_1" value="{{$applicantDetail->hobbie1 ?? old('hobby_1')}}" name="hobby_1" class="form-control" {{$disabledUser}}>
                </div>
                <!-- /.form-group -->
              </div>
              <!-- /.col -->
              <div class="col-md-4">
                <div class="form-group">
                  <label>Hobbie 2</label>
                  <input type="text" id="hobby_2" value="{{$applicantDetail->hobbie2 ?? old('hobby_2')}}" name="hobby_2" class="form-control" {{$disabledUser}}>
                </div>
                <!-- /.form-group -->
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label>Hobbie 3</label>
                  <input type="text" id="hobby_3" value="{{$applicantDetail->hobbie3 ?? old('hobby_3')}}" name="hobby_3" class="form-control" {{$disabledUser}}>
                </div>
              </div>              
              <!-- /.col -->
            </div>
           <!-- /.row -->
          </div> --}}
                {{-- <div class="box-header box box-warning">
            <h3 class="box-title"> Extra Curricular Activites (if any)</h3>
          </div>
          <div class="box-body">
            <div class="row">
              <div class="col-md-4">
                <div class="form-group">
                  <label>Activity</label>
                  <input type="text" id="activity_1" value="{{$applicantDetail->activity1 ?? old('activity_1')}}" name="activity_1" class="form-control" {{$disabledUser}}>
                </div>
                <!-- /.form-group -->
              </div>
              <!-- /.col -->
              <div class="col-md-4">
                <div class="form-group">
                  <label>Prize</label>
                  <input type="text" id="prize_1" value="{{$applicantDetail->prize1 ?? old('prize_1')}}" name="prize_1" class="form-control" {{$disabledUser}}>
                </div>
                <!-- /.form-group -->
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label>Awarded by</label>
                  <input type="text" id="awarded_by_1" value="{{$applicantDetail->awardBy1 ?? old('awarded_by_1')}}" name="awarded_by_1" class="form-control" {{$disabledUser}}>
                </div>
              </div>              
              <!-- /.col -->
            </div>
            <div class="row">
              <div class="col-md-4">
                <div class="form-group">
                  <label>Activity</label>
                  <input type="text" id="activity_2" value="{{$applicantDetail->activity2 ?? old('activity_2')}}" name="activity_2" class="form-control" {{$disabledUser}}>
                </div>
                <!-- /.form-group -->
              </div>
              <!-- /.col -->
              <div class="col-md-4">
                <div class="form-group">
                  <label>Prize</label>
                  <input type="text" id="prize_2" value="{{$applicantDetail->prize2 ?? old('prize_2')}}" name="prize_2" class="form-control" {{$disabledUser}}>
                </div>
                <!-- /.form-group -->
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label>Awarded by</label>
                  <input type="text" id="awarded_by_2" value="{{$applicantDetail->awardBy2 ?? old('awarded_by_2')}}" name="awarded_by_2" class="form-control" {{$disabledUser}}>
                </div>
              </div>              
              <!-- /.col -->
            </div>
            <div class="row">
              <div class="col-md-4">
                <div class="form-group">
                  <label>Activity</label>
                  <input type="text" id="activity_3" value="{{$applicantDetail->activity3 ?? old('activity_3')}}" name="activity_3" class="form-control" {{$disabledUser}}>
                </div>
                <!-- /.form-group -->
              </div>
              <!-- /.col -->
              <div class="col-md-4">
                <div class="form-group">
                  <label>Prize</label>
                  <input type="text" id="prize_3" value="{{$applicantDetail->prize3 ?? old('prize_3')}}" name="prize_3" class="form-control" {{$disabledUser}}>
                </div>
                <!-- /.form-group -->
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label>Awarded by</label>
                  <input type="text" id="awarded_by_3" value="{{$applicantDetail->awardBy3 ?? old('awarded_by_3')}}" name="awarded_by_3" class="form-control" {{$disabledUser}}>
                </div>
              </div>              
              <!-- /.col -->
            </div>
           <!-- /.row -->
          </div> --}}








                <div class="box-header box box-success">
                    <h3 class="box-title">Bank Account Detail(if any)</h3>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Account Title</label>
                                <input type="text" id="account_title"
                                    value="{{ $applicantDetail->titleBankAccount ?? old('account_title') }}"
                                    name="account_title" class="form-control" {{ $disabledUser }}>
                            </div>
                            <!-- /.form-group -->
                        </div>
                        <!-- /.col -->
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Account No.</label>
                                <input type="number" id="account_number" name="account_number"
                                    value="{{ $applicantDetail->accountNo ?? old('account_number') }}"
                                    class="form-control" {{ $disabledUser }}>
                            </div>
                            <!-- /.form-group -->
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Bank Name</label>
                                <input type="text" id="bank_name" name="bank_name"
                                    value="{{ $applicantDetail->bankName ?? old('bank_name') }}"
                                    class="form-control" {{ $disabledUser }}>
                            </div>
                        </div>
                        <!-- /.col -->
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Bank Branch</label>
                                <input type="text" id="branch_name" name="branch_name"
                                    value="{{ $applicantDetail->bankBranch ?? old('branch_name') }}"
                                    class="form-control" {{ $disabledUser }}>
                            </div>
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->
                </div>








                <div class="box-footer">
                    <button class="btn btn-social btn-success" {{ $disabledUser }} id="other-detail-btn"
                        type="submit"><i class="fa fa-save"></i> Save and Move Next</button>
                    {{-- <a class="btn btn-social btn-dropbox" href="{{url('/frontend/savedetail')}}">
                <i class="fa fa-fast-forward"></i> Skip this step
            </a> --}}
                </div>
        </form>
</div>
<!-- /.box -->
</section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->

@include('frontend.footer')
