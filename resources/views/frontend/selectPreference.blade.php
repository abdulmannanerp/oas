@section('title')
Select Preference
@endsection
@include('frontend.header')
@php
if($applicationStatus->step_save == 1 && $hash ==''){
  $readonlyUser = "readonly='readonly'";
  $disabledUser = "disabled";
  $disabledUser1 = 'disabled="disabled"';
}else{
  $readonlyUser='';
  $disabledUser = '';
  $disabledUser1 = '';
}
@endphp

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          <i class="fa fa-file-text-o text-red"></i> Select Preference
          <small>Add / Edit Preference</small>
        </h1>
      </section>
      @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    @if ( $status )
        <div class="alert alert-success" role="alert"> {{ $status }}</div>
    @endif  
  
      <!-- Main content -->
      <section class="content">
        <form action="{{route('selectpreference', $hash)}}" id="preferenceForm" method="POST">
          <input type="hidden" name="userId" value="{{$userdetail->userId}}">
          {{csrf_field()}}
  
        <!-- SELECT2 EXAMPLE -->
        <div class="box box-info">
          <div class="box-header with-border">
            <h3 class="box-title">Preference Details</h3>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            @for ($i = 1; $i <= $applied_applications->count(); $i++)
            @php 
                $pref = ''; 
                $pref = 'preference_'.$i;
             @endphp
                <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                    <label>Preference <?= $i ?></label>
                    <select class="form-control select2" id="preference_<?= $i ?>" name="preference_<?= $i ?>" required style="width: 100%;" {{$disabledUser}}>
                        <option value="">Select</option>
                        @foreach($applied_applications as $ap)
                            <option value="{{$ap->program->title}}" {{ ($applicantDetail->$pref == $ap->program->title) ? 'selected' : ''}}>{{$ap->program->title}}</option>
                        @endforeach
                    </select>
                    </div>
                    <!-- /.form-group -->
                </div>
                <!-- /.col -->
                </div>
            @endfor

          </div>
          <!-- /.box-body -->

          <div class="box-footer">
            <button class="btn btn-social btn-success" {{$disabledUser}} id="aptitude-test-btn" type="submit"><i class="fa fa-save"></i> Save and Move Next</button>
          </div>
        </form>
        </div>
        <!-- /.box --> 
      </section>
      <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

@include('frontend.footer')