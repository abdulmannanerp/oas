@section('title')
IIUI Admission
@endsection
@include('results.header')
<div class="container-fluid p50tb results-main-container announcements-panel small-fluid bg-ff back-img">
    <div class="transbox">
    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
@if ( $status )
    <div class="alert alert-danger text-center" role="alert"> {{ $status }}</div>
@endif
@if ( $status_ok )
    <div class="alert alert-success text-center" role="alert"> {{ $status_ok }}</div>
@endif
@if(strtolower($openClose->value) == 'true' || Request::route()->getName() == 'signupAdminRoute84@3')
    <div class="row">
      <h4 class="signupForm text-center mt-3 bl-clr"><i class="fa fa-edit mr-2 bl-clr"></i>Signup-for Admission Portal {{-- <span class="closing-date">Last Date: {{$Closingdate}}</span> --}}
      </h4>
    </div>
    <div class="row">
    <form class="signupForm" action="{{route('signup')}}" method="POST">
        {{csrf_field()}}
      <div class="form-row">
        <div class="col-md-6 mb-3">
          <label class="control-label">Name</label>
          <input type="text" class="form-control is-valid" id="name" name="name" placeholder="Enter Your Full Name" value="{{old('name')}}" required>
        </div>
        <div class="col-md-6 mb-3">
          <label class="control-label">Gender</label>
            <select class="form-control" id="gender" name="gender" required>
              <option value=""> Select Gender</option>
              <option value="3" {{ (old('gender') == 3) ? 'selected' : ''}}>Male</option>
              <option value="2" {{ (old('gender') == 2) ? 'selected' : ''}}>Female</option>
              <option value="others">Others</option>
            </select>
        </div>
      </div>
      <div class="form-row">
        <div class="col-md-6 mb-3">
          <label class="control-label">Email</label><span class="error" id="error_email"></span>
          <input type="email" class="form-control" id="email" name="email" aria-describedby="emailHelp" value="{{old('email')}}" placeholder="abc@xyz.com" required>
        </div>
        <div class="col-md-6 mb-3">
          <label class="control-label">Re-Type Email</label><span class="error" id="error_re_email"></span>
          <input type="email" class="form-control" onpaste="return false;" id="re_email" name="re_email" aria-describedby="emailHelp" value="{{old('re_email')}}" placeholder="abc@xyz.com" required>
        </div>
      </div>
      <div class="form-row">
        <div class="col-md-6 mb-3">
          <label class="control-label">Nationality</label>
            <select class="form-control" id="nationality" name="nationality" required>
                <option value="">Select Nationality</option>
                @foreach ( $nationalilty as $n )
                  <option value="{{$n->id}}" {{ ($n->id == 1) ? 'selected' : ''}}>{{$n->nationality}}</option>
                @endforeach
            </select>
        </div>
        <div class="col-md-6 mb-3">
          <label class="control-label">CNIC/B-Form/Passport No:</label>
          <input type="text" class="form-control is-valid" id="cnic" name="cnic" value="{{old('cnic')}}" placeholder="CNIC without dashes" required maxlength="13" minlength="13"> {{-- onkeypress="return isNumberKey(event)" --}}
        </div>
      </div>
      <div class="form-row">
        <div class="col-md-6 mb-3">
          <label class="control-label">Password</label>
          <input type="password" class="form-control" id="password" name="password" placeholder="Password" required>
        </div>
        <div class="col-md-6 mb-3">
            <label class="control-label">Re-Type Password</label><span class="error" id="error_password"></span>
            <input type="password" class="form-control" id="repassword" name="password_confirmation" placeholder="Re-Type Password" required>
        </div>
      </div>
      <div class="form-row">
        <div class="col-md-6 mb-3">
          <label class="control-label">Applying For</label>
            <select class="form-control" id="level" name="level" required>
                <option value="">Select Level</option>
                @foreach ( $level as $l )
                  <option value="{{$l->pkLevelId}}" {{ (old('level') == $l->pkLevelId) ? 'selected' : ''}}>{{$l->programLevel}}</option>
                @endforeach
            </select>
        </div>
        <div class="col-md-6 mb-3 cl-wh-sn">
          <label class="control-label"></label>
          <input type="checkbox" id="verify_instructions" name="verify_instructions" value="1" required> I've read all the <a href="#" data-toggle="modal" class="vr-sl" data-target="#admissionReq"> Important Instructions Before Applying</a>
        </div>
      </div>
      <button class="btn btn-primary" id="signup-btn" type="submit">Sign Up</button>
    </form>
</div>
@php $rr = 'something'; @endphp
@if(!$extendedPrograms->isEmpty() && $rr == '')
<div class="row" id="extend">
  <h4 class="mt-3 mb-1 pt-1 mr-auto extended-head bl-clr">Last date extended for following Programs</h4>
</div>
<div class="row">
  <table class="table table-striped extended-prog">
      <thead class="thead-light">
          <tr>
              <th>Program</th>
              <th>Last Date</th>
          </tr>
      </thead>
      <tbody>
        @foreach($extendedPrograms as $ep)
        @php 
        $lastDate = new DateTime($ep->lastDate);
        @endphp
          @if($ep->program->pkProgId == 189)
          <tr>
              <td>BS Electrical Engineering (4 years)</td>
              <td>16-Jul-2019</td>
          </tr>
          <tr>
              <td>BS Mechanical Engineering (4 Years)</td>
              <td>16-Jul-2019</td>
          </tr>
          @else
          <tr>
              <td>{{$ep->program->title .' (' .$ep->program->duration.' Years)'}}</td>
              <td>{{$lastDate->format('d-M-Y')}}</td>
          </tr>

          @endif
        @endforeach
      </tbody>
  </table>
</div>
@endif
@else
{{-- <script>window.location = "/results";</script> --}}
{{-- <div class="mar-cn"><img class="al-img-clo" src="{{asset('storage/images/close.jpg')}}"></div> --}}
<div class="alert alert-success text-center" role="alert">Note: The instructions to apply online will be available soon</div>
<div class="alert alert-danger text-center" role="alert">Admission Closed</div>
@endif
</div>
</div>
@include('results.footer')