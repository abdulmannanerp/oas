@section('title')
    Submitted Application(s)
@endsection
@include('frontend.header')


<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <i class="fa fa-edit text-red"></i> Submited Applications
            <small>Add Challan Details</small>
        </h1>
    </section>
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    @if ($status)
        <div class="alert alert-success" role="alert"> {{ $status }}</div>
    @endif

    <!-- Main content -->
    <section class="content">

        <!-- SELECT2 EXAMPLE -->
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">Details</h3>
            </div>

            <?php /* ?> ?> ?> ?> ?> ?> ?> ?> ?> ?> ?> ?> ?> ?> ?> ?> ?>
            <div class="row">
                <div class="col-md-12">
                    <div class="alert alert-info alert-dismissible bg-bccl">
                        Kindly give your consent for joining in IIUI by selecting one of the options given below Offer
                        Accepted / Offer Rejected
                        <form action="{{ route('applicationStatus', $hash) }}" method="POST">
                            <input type="hidden" name="userId" value="{{ $userdetail->userId }}">
                            <input type="hidden" name="server_date" value="{{ date('Y-m-d h:i:s') }}">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label>
                                    <input type="radio" name="consent_taken" id="consent_taken" value="accepted"
                                        {{ $userdetail->accepted_rejected == 'accepted' ? 'checked="checked"' : '' }}
                                        class="flat-red form-control"> Accepted
                                </label>
                                <label>
                                    <input type="radio" name="consent_taken" id="consent_taken" value="rejected"
                                        {{ $userdetail->accepted_rejected == 'rejected' ? 'checked="checked"' : '' }}
                                        class="flat-red form-control"> Rejected
                                </label>
                            </div>
                            <button class="btn btn-success" id="application-submit-btn" type="submit"><i
                                    class="fa fa-save"></i>Save</button>
                        </form>

                    </div>
                </div>
            </div>
            <?php */ ?>

            <!-- /.box-header -->
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="table-responsive mbl-res">
                            <table class="table table-striped table-fl">
                                <thead>
                                    <tr>
                                        <th>S#</th>
                                        <th>Programe Title</th>
                                        <th> Fee Deposit Date </th>
                                        <th> Bank Branch City </th>
                                        <th> Branch Code </th>
                                        <th> Upload Submitted Challan </th>
                                        <th> Save </th>
                                        <th>Downloads</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    
                                    // dump($challanGrouping);
                                    // dump($application);
                                    ?>
                                    @if ($application)
                                        @foreach ($application as $app)
                                            @php
                                                if ($userdetail->fkLevelId == 1) {
                                                    $challanMapping = \DB::select("SELECT challanId from `tbl_oas_challan_grouping`  WHERE fkApplicantId = $userdetail->userId and (applicationId_1 = $app->id  or applicationId_2 = $app->id or applicationId_3 = $app->id)");
                                                
                                                    // $value = \App\Model\ChallanGrouping::where('fkApplicantId', $userdetail->userId)
                                                    //     ->orWhere('applicationId_1', $app->id)
                                                    //     ->orWhere('applicationId_2', $app->id)
                                                    //     ->orWhere('applicationId_3', $app->id)
                                                    //     ->get()
                                                    //     ->pluck('challanId')
                                                    //     ->toArray();
                                                
                                                    $origionalChallan = \App\Model\ChallanDetail::where('fkApplicationtId', $app->id)
                                                        ->where('fkSemesterId', $app->fkSemesterId)
                                                        ->where('fkProgramId', $app->fkProgramId)
                                                        ->where('challan_type', 1)
                                                        ->first();
                                                
                                                    // dump($challanMapping[0]->challanId);
                                                    // dump($origionalChallan->id);
                                                }
                                                
                                            @endphp
                                            <form action="{{ route('application', $hash) }}" method="POST"
                                                enctype="multipart/form-data">
                                                <input type="hidden" name="userId" value="{{ $userdetail->userId }}">
                                                <input type="hidden" name="applicationId" value="{{ $app->id }}">
                                                <input type="hidden" name="server_date"
                                                    value="{{ date('Y-m-d h:i:s') }}">
                                                {{ csrf_field() }}
                                                <tr>
                                                    <td>{{ $loop->iteration }}</td>
                                                    <td>{{ $app->program->title . ' (' . $app->program->duration . ' years)' }}
                                                    </td>
                                                    <td>
                                                        @if ($userdetail->fkLevelId != 1 || $challanMapping[0]->challanId == $origionalChallan->id)
                                                            <div class="input-group date">
                                                                <div class="input-group-addon">
                                                                    <i class="fa fa-calendar"></i>
                                                                </div>

                                                                <input type="text"
                                                                    class="form-control pull-right fltpckr1"
                                                                    value="{{ date('Y-m-d', strtotime($app->feeSubmitted)) == '1970-01-01' ? '' : date('Y-m-d', strtotime($app->feeSubmitted)) }}"
                                                                    name="feeSubmitted" required>

                                                            </div>
                                                        @endif
                                                    </td>
                                                    <td>
                                                        @if ($userdetail->fkLevelId != 1 || $challanMapping[0]->challanId == $origionalChallan->id)
                                                            <select class="form-control select2" name="bankDetails"
                                                                id="bankDetails" style="width: 100%;" required>
                                                                <option value="">Select City</option>
                                                                @foreach ($city as $n)
                                                                    <option value="{{ $n->city }}"
                                                                        {{ $app->bankDetails == $n->city ? 'selected' : '' }}>
                                                                        {{ $n->city }}</option>
                                                                @endforeach
                                                            </select>
                                                        @endif
                                                    </td>
                                                    <td>
                                                        @if ($userdetail->fkLevelId != 1 || $challanMapping[0]->challanId == $origionalChallan->id)
                                                            <input type="number" class="form-control" id="branchCode"
                                                                name="branchCode" value="{{ $app->branchCode }}"
                                                                required>
                                                        @endif
                                                    </td>
                                                    <td>
                                                        @if ($userdetail->fkLevelId != 1 || $challanMapping[0]->challanId == $origionalChallan->id)
                                                            <input type="file" accept="image/*" class="form-control "
                                                                id="picture" name="challan_pic"
                                                                {{ $app->challan_pic ? '' : 'required' }}>
                                                            <label class="img-upload-cls">Max Size (4MB)</label>
                                                            @if ($app->challan_pic != '')
                                                                {{-- <p><a target="_blank" href="/storage/{{($app->challan_pic == '')? '/images/default.png': $app->challan_pic}}" id="pic1" alt="Challan Pic">Uploaded Challan</a></p> --}}
                                                                <p><a target="_blank"
                                                                        href="{{ asset('storage/' . $app->challan_pic) }}"
                                                                        id="pic1" alt="Challan Pic">Uploaded
                                                                        Challan</a></p>
                                                            @endif
                                                        @endif
                                                    </td>
                                                    <td>
                                                        @if ($userdetail->fkLevelId != 1 || $challanMapping[0]->challanId == $origionalChallan->id)
                                                            <button class="btn btn-success" id="application-submit-btn"
                                                                type="submit"><i class="fa fa-save"></i></button>
                                                        @endif
                                                    </td>
                                                    <td>
                                                        <p>
                                                            <a target="blank"
                                                                href="{{ route('applicationForm', ['oas_app_id' => $app->id]) }}"><i
                                                                    class="fa fa-file-pdf-o ac-icon"
                                                                    aria-hidden="true"></i> Application Form</a>
                                                        </p>
                                                        @if ($userdetail->fkLevelId != 1 || $challanMapping[0]->challanId == $origionalChallan->id)
                                                            <p>
                                                                <a target="blank"
                                                                    href="{{ route('feeChallan', ['applicationid' => $app->id, 'semesterid' => $app->fkSemesterId, 'programid' => $app->fkProgramId]) }}"><i
                                                                        class="fa fa-file-pdf-o ac-icon"
                                                                        aria-hidden="true"></i>Challan Form 
                                                                        @if ($userdetail->fkNationality == 2 || $userdetail->fkNationality == 3) 
                                                                        {{'$'}}
                                                                        @endif
                                                                </a>
                                                                @if ($userdetail->fkNationality == 2 || $userdetail->fkNationality == 3) 
                                                                    <a target="blank"
                                                                        href="{{ route('feeChallanPkr', ['applicationid' => $app->id, 'semesterid' => $app->fkSemesterId, 'programid' => $app->fkProgramId]) }}"><i
                                                                        class="fa fa-file-pdf-o ac-icon"
                                                                        aria-hidden="true"></i>Challan Form 
                                                                        @if ($userdetail->fkNationality == 2 || $userdetail->fkNationality == 3) 
                                                                        {{'PKR'}}
                                                                        @endif
                                                                </a>
                                                                @endif
                                                            </p>
                                                        @endif
                                                        <p>
                                                            @if ($app->fkCurrentStatus >= 5 && $app->fkCurrentStatus != 6 && $app->fkCurrentStatus != 7)
                                                                @if ($app->program->programscheduler->testDateTime != '0000-00-00 00:00:00' ||
                                                                    ($app->program->pkProgId == 189 && !is_null($app->getRollNumber->engineering)))
                                                                    <a target="blank"
                                                                        href="{{ route('rollNumberSlip', ['oas_app_id' => $app->id]) }}"><i
                                                                            class="fa fa-file-pdf-o slip-icon"
                                                                            aria-hidden="true"></i>Application ID
                                                                        <span class="blink_me new">
                                                                            Verified Successfully
                                                                        </span>
                                                                    </a>
                                                                @else
                                                                    @if($app->program->pkProgId == 223 || $app->program->pkProgId == 126 || $app->program->pkProgId == 127)
                                                                        <a target="blank"
                                                                            href="{{ route('rollNumberSlip', ['oas_app_id' => $app->id]) }}"><i
                                                                                class="fa fa-file-pdf-o slip-icon"
                                                                                aria-hidden="true"></i>Application ID
                                                                            <span class="blink_me new">
                                                                                Verified Successfully
                                                                            </span>
                                                                        </a>
                                                                    @else
                                                                        <span class="blink_me new">
                                                                            Your application has been successfully submitted
                                                                        </span>
                                                                    @endif
                                                                @endif
                                                            @elseif($app->fkCurrentStatus == 6)
                                                                <i class="slip-icon" aria-hidden="true"></i>Application
                                                                <span class="blink_me rejected">
                                                                    Rejected
                                                                    @php
                                                                        echo '<b>[' . $app->comments . ']</b>';
                                                                    @endphp
                                                                </span>
                                                            @else
                                                                <i class="fa fa-file-pdf-o slip-icon"
                                                                    aria-hidden="true"></i>Application ID
                                                                <span class="blink_me pending">
                                                                    @if ($app->fkCurrentStatus <= 3)
                                                                        In Progress: Fee Verification
                                                                    @endif
                                                                    @if ($app->fkCurrentStatus == 4)
                                                                        In Progress: Document Verification
                                                                    @endif
                                                                </span>
                                                            @endif
                                                        </p>

                                                        @if (!$resultlist->isEmpty())
                                                            <span>
                                                                @foreach ($resultlist as $rs)
                                                                    @if ($rs->result->fkProgrammeId == $app->program->pkProgId)
                                                                        @if ($rs->fkResultId != 457 && $rs->fkResultId != 458)
                                                                            <hr class="no-mar" />
                                                                            <p><b>Admission Offered (Limited Seats)</b>
                                                                            </p>
                                                                            <hr class="no-mar" />
                                                                            <p><a href="{{ route('getResultDocs', ['result' => $rs->fkResultId, 'rollno' => $rs->rollno, 'doc' => 'offer-letter', 'cnic' => $userdetail->cnic]) }}"
                                                                                    class="download-docs"
                                                                                    target="_blank">
                                                                                    <i class="fa fa-file-pdf-o ad-icon"
                                                                                        aria-hidden="true"></i>Admission
                                                                                    Offer Letter <span
                                                                                        class="blink_me new">New</span>
                                                                                </a></p>
                                                                            <p><a href="{{ route('getResultDocs', ['result' => $rs->fkResultId, 'rollno' => $rs->rollno, 'doc' => 'fee-challan', 'cnic' => $userdetail->cnic]) }}"
                                                                                    class="download-docs"
                                                                                    target="_blank">
                                                                                    <i class="fa fa-file-pdf-o ad-icon"
                                                                                        aria-hidden="true"></i>Admission
                                                                                    Challan Form <span
                                                                                        class="blink_me new">New</span>
                                                                                </a></p>
                                                                            <p><a href="{{ route('joiningPerforma', ['oas_app_id' => $rs->fkResultId, 'rollno' => $rs->rollno, 'cnic' => $userdetail->cnic]) }}"
                                                                                    class="download-docs"
                                                                                    target="_blank">
                                                                                    <i class="fa fa-file-pdf-o ad-icon"
                                                                                        aria-hidden="true"></i>Admission
                                                                                    Joining <span
                                                                                        class="blink_me new">New</span>
                                                                                </a></p>
                                                                            {{-- Dev Mannan: Code Commented Below for CNIC donot match issue --}}
                                                                            {{-- <p><a href="{{route('getResultDocs', ['result' => $rs->fkResultId, 'rollno' => $rs->rollno, 'doc' => 'all'])}}" class="download-docs" target="_blank">
                                                                            <i class="fa fa-file-pdf-o ad-icon" aria-hidden="true"></i>Admission [Offer Letter/Challan Form/Joining] <span class="blink_me new">New</span>
                                                                        </a></p> --}}
                                                                        @endif
                                                                    @endif
                                                                @endforeach
                                                            </span>
                                                        @endif


                                                        {{-- Dev Mannan: Code for engineering Program Starts --}}

                                                        @if ($engResListData != '')
                                                            @if ($app->program->pkProgId == 189)
                                                                <span>
                                                                    <hr class="no-mar" />
                                                                    <p><b>Admission Offered (Limited Seats)</b></p>
                                                                    <hr class="no-mar" />
                                                                    @foreach ($engResListData as $ers)
                                                                        {{ $ers->result->program->title }}
                                                                        <p><a href="{{ route('getResultDocs', ['result' => $ers->fkResultId, 'rollno' => $ers->rollno, 'doc' => 'offer-letter']) }}"
                                                                                class="download-docs" target="_blank">
                                                                                <i class="fa fa-file-pdf-o ad-icon"
                                                                                    aria-hidden="true"></i>Admission
                                                                                Offer Letter <span
                                                                                    class="blink_me new">New</span>
                                                                            </a></p>
                                                                        <p><a href="{{ route('getResultDocs', ['result' => $ers->fkResultId, 'rollno' => $ers->rollno, 'doc' => 'fee-challan', 'cnic' => $userdetail->cnic]) }}"
                                                                                class="download-docs" target="_blank">
                                                                                <i class="fa fa-file-pdf-o ad-icon"
                                                                                    aria-hidden="true"></i>Admission
                                                                                Challan Form <span
                                                                                    class="blink_me new">New</span>
                                                                            </a></p>
                                                                        <p><a href="{{ route('joiningPerforma', ['oas_app_id' => $ers->fkResultId, 'rollno' => $ers->rollno]) }}"
                                                                                class="download-docs" target="_blank">
                                                                                <i class="fa fa-file-pdf-o ad-icon"
                                                                                    aria-hidden="true"></i>Admission
                                                                                Joining <span
                                                                                    class="blink_me new">New</span>
                                                                            </a></p>
                                                                    @endforeach
                                                                </span>
                                                            @endif
                                                        @endif
                                                        {{-- Dev Mannan: Code for engineering Program Ends --}}

                                                    </td>
                                                </tr>

                                            </form>

                                            <?php /* ?> ?> ?> ?> ?> ?> ?> ?> ?> ?> ?> ?> ?> ?> ?> ?> ?> ?> ?> ?> ?>
                                            @if (!$resultlist->isEmpty())
                                                @foreach ($resultlist as $rs)
                                                    @if ($rs->result->fkProgrammeId == $app->program->pkProgId)
                                                        @if ($rs->fkResultId != 457 && $rs->fkResultId != 458)
                                                            <tr>

                                                                <td></td>
                                                                <td>{{ $app->program->title . ' (' . $app->program->duration . ' years)' }}
                                                                </td>
                                                                <td colspan="4">
                                                                    Kindly give your consent for joining in IIUI by
                                                                    selecting one of the given option
                                                                </td>
                                                                <td colspan="2">
                                                                    <form
                                                                        action="{{ route('applicationStatus', $hash) }}"
                                                                        method="POST">
                                                                        <input type="hidden" name="userId"
                                                                            value="{{ $userdetail->userId }}">
                                                                        <input type="hidden" name="applicationId"
                                                                            value="{{ $app->id }}">
                                                                        <input type="hidden" name="server_date"
                                                                            value="{{ date('Y-m-d h:i:s') }}">
                                                                        {{ csrf_field() }}
                                                                        <div class="form-group">
                                                                            <label>
                                                                                <input type="radio"
                                                                                    name="consent_taken"
                                                                                    id="consent_taken"
                                                                                    value="accepted"
                                                                                    {{ $app->accepted_rejected == 'accepted' ? 'checked="checked"' : '' }}
                                                                                    class="flat-red form-control">
                                                                                Accepted
                                                                            </label>
                                                                            <label>
                                                                                <input type="radio"
                                                                                    name="consent_taken"
                                                                                    id="consent_taken"
                                                                                    value="rejected"
                                                                                    {{ $app->accepted_rejected == 'rejected' ? 'checked="checked"' : '' }}
                                                                                    class="flat-red form-control">
                                                                                Rejected
                                                                            </label>
                                                                            <button class="btn btn-success mr-lf-10"
                                                                                id="application-submit-btn"
                                                                                type="submit"><i
                                                                                    class="fa fa-save"></i></button>
                                                                        </div>

                                                                    </form>
                                                                </td>

                                                            </tr>
                                                        @endif
                                                    @endif
                                                @endforeach
                                            @endif
                                            <?php */ ?>
                                        @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
        <div class="box box-info">
            <div class="row">
                <div class="col-md-12">
                    <div class="alert alert-info alert-dismissible bg-ccl">
                        <p class="sv-txt"><b>Note:</b></p>
                        <ul>
                            <li>
                                <b>In case you didn't recieve rollnumber slip, you can still appear in entry test with
                                    Submitted fee challan</b>
                            </li>
                            {{-- <li>
                                <b>Candidates applying for undergraduate degree programs can choose upto five programs
                                    against the payment of a single application processing fee</b>
                            </li> --}}
                            <li>
                                Please enter the details of submitted challan form to complete the application
                                submission process.
                            </li>
                            <li>
                                You will get the Roll No. Slip in your online account once the document and fee has been
                                verified.
                            </li>
                            <li>
                                For online application submission hard copies of the documents are NOT required.
                            </li>
                            <li>
                                If you want to apply in other program(s). Please <a
                                    href="{{ url('frontend/selectprogram') }}"><b>Click here</b> </a>
                            </li>
                        </ul>
                    </div>
                    <p><b>*Pending</b> means that your fee and documents are still in verification process</p>
                </div>
            </div>
            <!-- /.row -->
        </div>

    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

@include('frontend.footer')
