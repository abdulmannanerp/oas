@section('title')
Contact Us
@endsection
@include('results.header')
<div class="container-fluid p50tb announcements-panel small-fluid bg-ff">
    <div class="row">
        <h3 class="signupForm text-center mb-3 mt-3 bl-clr font-weight-bold cntct-us">Contact Us </h3>
        <p class="mr-auto">International Islamic University Islmabad Admission Office</p>
    </div>
    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    @if ( $status )
        <div class="alert alert-success text-center" role="alert"> {{ $status }}</div>
    @endif 
    <div class="row">
        <form class="signupForm cntct-us"  action="{{route('contactus')}}" method="POST">
            <div class="form-row">
              <div class="form-group col-md-6">
                <label for="inputEmail4">Department</label>
                <select id="department" class="form-control" name="department" required>
                    <option value="">--Select Department--</option>
										<option value="Admission (Male)">Admission (Male) </option>
										<option value="Admission (Female)">Admission (Female)</option>
										{{-- <option value="Overseas(Male & Female)">Overseas(Male & Female)</option> --}}
                  </select>
              </div>
              <div class="form-group col-md-6">
                <label for="inputPassword4">Name</label>
                <input type="text" class="form-control" id="name" name="name" placeholder="Full Name" required>
              </div>
            </div>
            <div class="form-row">
              <div class="form-group col-md-6">
                <label for="inputEmail4">Email</label><span class="error" id="error_email"></span>
                <input type="text" class="form-control" id="email" name="email" placeholder="abc@xyz.com" required>
              </div>
              <div class="form-group col-md-6">
                <label for="inputPassword4">Phone#</label>
                <input type="number" class="form-control" id="phone" name="phone" placeholder="Phone Number" required>
              </div>
            </div>
            <div class="form-row">
              <div class="form-group col-md-6">
                <label for="inputEmail4">CNIC</label><span class="error" id="error_email"></span>
                <input type="number" class="form-control" id="cnic" name="cnic" placeholder="CNIC" required>
              </div>
            </div>
            <div class="form-row">
              <div class="form-group col-md-12">
                <label for="inputEmail4">Message</label>
                <textarea class="form-control txt-ara" id="message" name="message" rows="1" required></textarea>
              </div>
            </div>            
            <button type="submit" class="btn btn-primary mb-3">Send Message</button>
          </form>
    </div>
    {{-- <div class="row">
        <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3320.916565658344!2d73.0215643!3d33.6593237!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x38df95906a03cfff%3A0x2b2f1c1c99b676ce!2sInternational+Islamic+University+Islamabad!5e0!3m2!1sen!2suk!4v1524180722921" width="100%" height="300" frameborder="0" target="_blank" style="border:0" allowfullscreen></iframe>
    </div> --}}


</div>
@include('results.footer')