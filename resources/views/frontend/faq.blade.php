@section('title')
	Faq's
@endsection
@include('results.header')
<div class="container-fluid p50tb results-main-container announcements-panel small-fluid bg-ff">
    <div class="row">
        <h3 class="signupForm text-center mb-3 mt-3 bl-clr faq-hed font-weight-bold">FAQs</h3>
        {{-- <p class="mr-auto">International Islamic University Islmabad Admission Office</p> --}}
    </div>

    <div class="row">
      <div class="col-md-4 first">
        <h5 class="faq-hed">General Academic Information</h5>
        <div id="accordion" role="tablist" aria-multiselectable="true">
          <div class="card">
            <div class="card-header" role="tab" id="headingOne">
              <h5 class="mb-0">
                <a data-toggle="collapse" data-parent="#accordion" href="#one1" aria-expanded="true" aria-controls="one1">
                    PAYMENT OF FEE
                </a>
              </h5>
            </div>
        
            <div id="one1" class="collapse" role="tabpanel" aria-labelledby="headingOne">
              <div class="card-block">
                  Prescribed fee is required to be paid on or before the due date. Last date may be seen on admission letters or as announced by the Faculty. Partial payments are not accepted. Candidates failing to deposit full fee by the due date, loses his/her seat which is offered to the next waiting candidate on merit list.
              </div>
            </div>
          </div>
          <div class="card">
            <div class="card-header" role="tab" id="headingTwo">
              <h5 class="mb-0">
                <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#one2" aria-expanded="false" aria-controls="one2">
                    NUMBER OF SEATS
                </a>
              </h5>
            </div>
            <div id="one2" class="collapse" role="tabpanel" aria-labelledby="headingTwo">
              <div class="card-block">
                  Number of seats to be offered for admission for one particular semester for one particular program are determined by the faculties on the basis of availability and minimum acceptable score. List of Selected Candidates is placed IIUI website and notice board of respective faculty. Result of test should not be enquired over telephone.
              </div>
            </div>
          </div>
          <div class="card">
            <div class="card-header" role="tab" id="headingThree">
              <h5 class="mb-0">
                <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#one3" aria-expanded="false" aria-controls="one3">
                    ACADEMIC SYSTEM
                </a>
              </h5>
            </div>
            <div id="one3" class="collapse" role="tabpanel" aria-labelledby="headingThree">
              <div class="card-block">
                  IIUI operates on semester based academic system. Two regular semesters i.e. Fall Semester (September-January) & Spring Semester (February-June) are offered during an academic year. Each semester essentially entails 48 teaching hours for a course of 3 credit hours. A student has to register for courses offered by each Deptt/Faculty in every semester as per scheme of study.
              </div>
            </div>
          </div>
          <div class="card">
              <div class="card-header" role="tab" id="headingThree">
                <h5 class="mb-0">
                  <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#one4" aria-expanded="false" aria-controls="one4">
                      ATTENDANCE REQUIREMENT
                  </a>
                </h5>
              </div>
              <div id="one4" class="collapse" role="tabpanel" aria-labelledby="headingThree">
                <div class="card-block">
                    A student is required to maintain an attendance level of at least 75% for each registered course during a semester. There is no relaxation on any ground (including medical and other emergencies) to the minimum required attendance. A student who fails to maintain minimum level of attendance i.e. 75% in any of the registered course(s) is prevented from appearing in terminal examination and is treated as “fail” in the course(s) and all the grades/marks obtained in that course(s) during the semester stands cancelled.
                </div>
              </div>
            
          </div>
          <div class="card">
              <div class="card-header" role="tab" id="headingThree">
                <h5 class="mb-0">
                  <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#one5" aria-expanded="false" aria-controls="one5">
                      ACADEMIC STRUCTURE FOR BS PROGRAM
                  </a>
                </h5>
              </div>
              <div id="one5" class="collapse" role="tabpanel" aria-labelledby="headingThree">
                <div class="card-block">
                    A BS program consists of 132-136 credit hours out of which 60% weightage is assigned to major subjects of the concerned degree whereas 40% weightage is assigned to the general subjects (for example students of Islamic Studies study 60% of the credit hours from Major subjects of their respective area and they take on 40% of the credit hours from Social Sciences, Humanities and Natural Sciences) and likewise in other subjects. Minimum duration of BS program is 4 years i.e. 8 regular semesters qualifying with minimum CGPA range of 2.00/4.00 and passing of Hifz Test.
                </div>
              </div>
          </div>
          <div class="card">
              <div class="card-header" role="tab" id="headingThree">
                <h5 class="mb-0">
                  <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#one6" aria-expanded="false" aria-controls="one6">
                      MEDIUM OF INSTRUCTIONS & EXAMINATIONS
                  </a>
                </h5>
              </div>
              <div id="one6" class="collapse" role="tabpanel" aria-labelledby="headingThree">
                <div class="card-block">
                    Arabic and English are the medium of instructions in the University. Arabic is the primary medium of instruction for the Faculties of Islamic Studies (Usuluddin), Arabic and Shariah & Law. Ability to read, write and comprehend English is also required. Whereas in all other faculties (Except Urdu and Persian Departments), English is the medium of instruction. Applicants seeking admission to IIUI are required to pass Language Proficiency Test to qualify for admissions. The University runs a program for improvement in Arabic and English Proficiency in ALT and ELT Units for deficient students. 2. Students applying for admission to Faculty of Islamic Studies (Usuluddin) and Faculty of Shariah &Law appear for Level determination test for Arabic and English, after which they are offered provisional admission to Language program first. Regular Program will only be allowed to start after qualifying language level test.
                </div>
              </div>
            </div>
          <div class="card">
              <div class="card-header" role="tab" id="headingThree">
                <h5 class="mb-0">
                  <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#one7" aria-expanded="false" aria-controls="one7">
                      EXAMINATIONS & EVALUATION SYSTEM
                  </a>
                </h5>
              </div>
              <div id="one7" class="collapse" role="tabpanel" aria-labelledby="headingThree">
                <div class="card-block">
                    All final terminal examinations are held during last two weeks of the semester i.e 1st two weeks of January for Fall Semester and 1st 02 weeks of June for Spring Semester every year.
                </div>
              </div>
          </div>
          <div class="card">
              <div class="card-header" role="tab" id="headingThree">
                <h5 class="mb-0">
                  <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#one8" aria-expanded="false" aria-controls="one8">
                      GRADING SCHEME
                  </a>
                </h5>
              </div>
              <div id="one8" class="collapse" role="tabpanel" aria-labelledby="headingThree">
                <div class="card-block">
                    <p>The Letter Grades and Grade Points for a course are determined on the basis of the marks obtained by the student in the final examination and the semester work in the following manner (w.e.f. Fall Semester, 2010):-</p>											
											<table class="table table-striped table-responsive">
												<tbody><tr>
													<th>Marks</th>
													<th>Grade</th>
													<th>Point</th>
				
												</tr>
												
												<tr>
													<td>80 &amp; above</td>
													<td>A</td>
													<td>4.00</td>												
												</tr>
												
												<tr>
													<td>75-79.99%</td>
													<td>B+</td>
													<td>3.50</td>													
												</tr>
												
												<tr>
													<td>70-74.99%</td>
													<td>B</td>
													<td>3.00</td>													
												</tr>
												
												<tr>
													<td>65-69.99%</td>
													<td>C+</td>
													<td>2.50</td>												
												</tr>
												
												<tr>
													<td>60-64.99%</td>
													<td>C</td>
													<td>2.00</td>												
												</tr>
												
												<tr>
													<td>55-59.99%</td>
													<td>D+</td>
													<td>1.50</td>												
												</tr>
												
												<tr>
													<td>50-54.99%</td>
													<td>D</td>
													<td>1.00</td>												
												</tr>
												
												<tr>
													<td>Below 50%</td>
													<td>F</td>
													<td>00</td>												
												</tr>
							
											</tbody></table>		
                </div>
              </div>
          </div>
          <div class="card">
              <div class="card-header" role="tab" id="headingThree">
                <h5 class="mb-0">
                  <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#one9" aria-expanded="false" aria-controls="one9">
                      MINIMUM PASS MARKS
                  </a>
                </h5>
              </div>
              <div id="one9" class="collapse" role="tabpanel" aria-labelledby="headingThree">
                <div class="card-block">
                    <tbody><tr>
                        <td>BS</td>
                        <td>60%</td>																								
                      </tr>
                      
                      <tr>
                        <td>MA/MSc/MBA</td>
                        <td>60%</td>																									
                      </tr>
                      
                      <tr>
                        <td>MS/MPhil/LLM</td>
                        <td>65%</td>																									
                      </tr>
                      <tr>
                        <td>PhD</td>
                        <td>70%</td>																								
                      </tr>
          
                    </tbody>
                </div>
              </div>
          </div>
          <div class="card">
              <div class="card-header" role="tab" id="headingThree">
                <h5 class="mb-0">
                  <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#one10" aria-expanded="false" aria-controls="one10">
                    REGULATIONS FOR PROBATION
                  </a>
                </h5>
              </div>
              <div id="one10" class="collapse" role="tabpanel" aria-labelledby="headingThree">
                <div class="card-block">
                  <p>Student not securing required GPA/CGPA in the terminal examinations is put on probation as per following regulations:-</p>
                  <table class="table table-responsive">
                    <tbody><tr>
                      <th>Degree Programs /<br> Category of Students</th>
                      <th colspan="4" class="text-center">Provisions of Probation</th>
                    </tr>
                    
                    <tr>
                      <th></th>
                      <th>Passing Marks</th>
                      <th>Probation Range</th>
                      <th>No. of Semesters<br> allowed to clear Probation</th>
                      <th>Minimum CGPA <br>for Award of Degree</th>
                    </tr>
                    
                    <tr>
                      <th>1</th>
                      <th>2</th>
                      <th>3</th>
                      <th>4</th>
                      <th>5</th>
                    </tr>
                    
                    <tr>
                      <td>Under Graduate Program<br>(BS, BBA &amp; LLB etc.)</td>
                      <td>60%</td>
                      <td>1.00-2.00</td>
                      <td>3 Semesters<br>(POP, PC-1,<br> PC-2, PC-3 Ceased)</td>
                      <td>2.00</td>													
                    </tr>
                    
                    <tr>
                      <td>Post Graduate Program (Masters)<br>(MBA, MCS, MSC &amp; MA)</td>
                      <td>60%</td>
                      <td>1.00-2.50</td>
                      <td>2 Semesters<br>(POP, PC-1,<br> PC-2, PC-3 Ceased)</td>
                      <td>2.50</td>													
                    </tr>
                    
                    <tr>
                      <td>MS / M.Phil / LLM Programs</td>
                      <td>65%</td>
                      <td>2.00-2.50</td>
                      <td>2 Semesters<br>(POP, PC-1,<br> PC-2, Ceased)</td>
                      <td>2.70</td>													
                    </tr>
                    
                    <tr>
                      <td>Ph.D Programs</td>
                      <td>70%</td>
                      <td>2.00-3.00</td>
                      <td>2 Semesters<br>(POP, PC-1,<br> PC-2, Ceased)</td>
                      <td>3.00</td>													
                    </tr>
                    
                    
                  </tbody></table>
                </div>
              </div>
          </div>
          <div class="card">
              <div class="card-header" role="tab" id="headingThree">
                <h5 class="mb-0">
                  <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#one11" aria-expanded="false" aria-controls="one11">
                    DURATION OF DEGREE PROGRAM
                  </a>
                </h5>
              </div>
              <div id="one11" class="collapse" role="tabpanel" aria-labelledby="headingThree">
                <div class="card-block">
                    <p>The following is the minimum and maximum degree duration:</p>
                    <table class="table table-striped table-responsive">
                      <tbody><tr>
                        <th>DEGREE PROGRAM</th>
                        <th class="text-center">DURATION</th>
                      </tr>
                      
                      <tr>
                        <th></th>
                        <th>Minimum</th>
                        <th>Maximum</th>
                      </tr>
                      
                      
                      <tr>
                        <td>Under Graduate Program<br>(BS, BBA &amp; LLB etc.)</td>
                        <td>4 Years</td>
                        <td>6 Years</td>
                      </tr>
                      
                      <tr>
                        <td>Post Graduate Program (Masters)<br>(MCS, MSC &amp; MA)</td>
                        <td>2 Years</td>
                        <td>4 Years</td>
                      </tr>
                      
                      <tr>
                        <td>MBA</td>
                        <td>3.5 Years</td>
                        <td>4 Years</td>
                      </tr>
                      
                      
                      <tr>
                        <td>MS / M.Phil / LLM Programs</td>
                        <td>2 Years</td>
                        <td>3 Years</td>
                      </tr>
                      
                      
                      <tr>
                        <td>Ph.d Program</td>
                        <td>3 Years</td>
                        <td>5 Years</td>
                      </tr>
                      
                    </tbody></table>
                </div>
              </div>
          </div>
          <div class="card">
              <div class="card-header" role="tab" id="headingThree">
                <h5 class="mb-0">
                  <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#one12" aria-expanded="false" aria-controls="one12">
                    RE-ADMISSION
                  </a>
                </h5>
              </div>
              <div id="one12" class="collapse" role="tabpanel" aria-labelledby="headingThree">
                <div class="card-block">
                    <p>A Ceased or Absent Ceased student may seek re-admission only once for a degree program subject to following conditions:</p>
                  <ul>
                    <li>Re-admission is granted in subsequent semester only.</li>
                    <li>Re-admission is granted with minimum CGPA:</li>
                    <li>All Bachelors / Master degree programs (16 years): 1.00/4.00 CGPA</li>
                    <li>All MS / Ph.D degree programs: 2.00/4.00 CGPA</li>
                    <li>The student has to pay prescribed re-admission fee in addition to regular semester fee.</li>
                    <li>The student upon his readmission gets exemption for 60% of his previously passed courses. He repeats 40% of his previously passed courses.</li>
                  </ul>
                </div>
              </div>
          </div>
          <div class="card">
              <div class="card-header" role="tab" id="headingThree">
                <h5 class="mb-0">
                  <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#one13" aria-expanded="false" aria-controls="one13">
                      CLASS TIMINGS
                  </a>
                </h5>
              </div>
              <div id="one13" class="collapse" role="tabpanel" aria-labelledby="headingThree">
                <div class="card-block">
                    Classes are held from 8.30 a.m. to 8.30 p.m. according to the time table of respective Faculty/Department.
                </div>
              </div>
          </div>
          <div class="card">
              <div class="card-header" role="tab" id="headingThree">
                <h5 class="mb-0">
                  <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#one14" aria-expanded="false" aria-controls="one14">
                      TRANSFER OF DEGREE PROGRAM
                  </a>
                </h5>
              </div>
              <div id="one14" class="collapse" role="tabpanel" aria-labelledby="headingThree">
                <div class="card-block">
                    Transfer of degree program from one to another is not permissible.
                </div>
              </div>
          </div>
          <div class="card">
              <div class="card-header" role="tab" id="headingThree">
                <h5 class="mb-0">
                  <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#one15" aria-expanded="false" aria-controls="one15">
                      DEFERMENT OF ADMISSION
                  </a>
                </h5>
              </div>
              <div id="one15" class="collapse" role="tabpanel" aria-labelledby="headingThree">
                <div class="card-block">
                    <p>Admission of a newly admitted candidate may be deferred to a subsequent semester or session.  Following are the conditions for deferment of Admission:</p>
											<ul>
												<li>Admission of newly admitted candidate may be deferred to a subsequent semester or session (1 year) in case the candidate applies for deferment before joining and paying the university fee. </li>
												<li>After submission of fee and joining but before the commencement of semester the applicant may be allowed the deferment with 100% adjustment of fee in the subsequent semester / session.</li>
												<li>After submission of joining and payment of fee and within 02 weeks time from commencement of semester, deferment may be allowed with 50% of the fee is charged and 50% is adjusted in the subsequent semester / session. </li>
												<li>The above policy / rules are not applicable to the student granted admission on “result awaiting basis”.</li>
												
											</ul>
                </div>
              </div>
          </div>
          <div class="card">
              <div class="card-header" role="tab" id="headingThree">
                <h5 class="mb-0">
                  <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#one16" aria-expanded="false" aria-controls="one16">
                      DEFERMENT OF STUDIES
                  </a>
                </h5>
              </div>
              <div id="one16" class="collapse" role="tabpanel" aria-labelledby="headingThree">
                <div class="card-block">
                    <p>On successful completion of 1st semester, the student may seek for deferment of studies on prescribed Performa for a maximum period of two semesters (1 year) on payment of prescribed deferment fee and fulfillment of following conditions:-</p>
											<ul>
												<li>After two weeks time and till mid of the semester (i.e. 30th March for Spring semester &amp; 30th October  for Fall semester), 50% of tuition fee is deducted  and remaining 50% is adjusted in the subsequent semester.  However, if tuition fee has not been paid till then, 50% tuition fee shall be charged before deferment is allowed/ notified.</li>
												<li>A student who neither has registered any subject nor has attended any class, applies for deferment of fee is allowed on payment of deferment fee only (till that time).</li>
												<li>Ex-Post-Facto deferment may be allowed up to a maximum of two semesters only with double deferment fee.</li>
												<li>Except deferment no gap in the degree is condoned.</li>
												<li>After availing of two deferments, if a gap of a semester is pointed out,  the student is  declared “Absent Ceased” and he is required to apply for re-admission.</li>
											</ul>
                </div>
              </div>
          </div>
          <div class="card">
              <div class="card-header" role="tab" id="headingThree">
                <h5 class="mb-0">
                  <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#one17" aria-expanded="false" aria-controls="one17">
                      CODE OF CONDUCT
                  </a>
                </h5>
              </div>
              <div id="one17" class="collapse" role="tabpanel" aria-labelledby="headingThree">
                <div class="card-block">
                    Students shall be required to abide by the prescribed Rules and Regulations of IIUI and observe the dress code and basic Islamic tenets failing which disciplinary action may be initiated against him/her as per disciplinary regulations enforced from time to time by IIUI.
                </div>
              </div>
          </div>
          <div class="card">
              <div class="card-header" role="tab" id="headingThree">
                <h5 class="mb-0">
                  <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#one18" aria-expanded="false" aria-controls="one18">
                      POLICY FOR REFUND OF FEE
                  </a>
                </h5>
              </div>
              <div id="one18" class="collapse" role="tabpanel" aria-labelledby="headingThree">
                <div class="card-block">
                    <table class="table table-striped table-responsive">
												<tbody><tr>
													<th>S.#</th>
													<th>Particulars</th>
													<th>Policy</th>
												</tr>
												<tr>
													<td>1.</td>
													<td>Candidates, who get admission in any
													degree program, deposit  fee and 
													afterwards request for refund of fee due
													to any reason.
													</td>
													
													<td>
														<ul>
															<li>Full (100%) fee refund (Excluding admission fee) Up to 7th day after
															commencement of semester.</li>
															<li>Half (50%) fee refund (Excluding admission fee) From 8th – 15th day after
															commencement of semester</li>
															<li>No fee refund From 16th day after commencement of semester except 
															refundable security</li>
														</ul>
													</td>
												</tr>
												<tr>
													<td>2.</td>
													<td>Continuing students who join the 
													semester and apply for deferment of 
													studies.
													</td>
													
													<td>If student applies for deferment of studies till half of the semester (midterm)<u> as per 
													announced academic schedule,</u> student is entitled to 50% Tuition fee adjustment in 
													subsequent semester(s).</td>
												</tr>
												<tr>
													<td>3.</td>
													<td>Existing students who register in next 
													semester, before declaration of previous 
													result and deposit their fee and join the
													University and afterwards receive result 
													of being ceased.
													</td>
													<td>In case the result of his previous semester declares a student to be ceased from the 
													roll of the University, the semester fee paid by student, is refundable except 
													semester registration fee wherever applicable.		
													</td>
												</tr>
											</tbody></table>
                </div>
              </div>
          </div>

        </div>
      </div>
      <div class="col-md-4 second">
        <h5 class="faq-hed">Questions & Answers</h5>
        <div id="accordion" role="tablist" aria-multiselectable="true">
          <div class="card">
            <div class="card-header" role="tab" id="headingOne">
              <h5 class="mb-0">
                <a data-toggle="collapse" data-parent="#accordion" href="#two1" aria-expanded="true" aria-controls="two1">
                    Q.	How I can apply to academic programs?
                </a>
              </h5>
            </div>
        
            <div id="two1" class="collapse" role="tabpanel" aria-labelledby="headingOne">
              <div class="card-block">
                  <a href="http://admissions.iiu.edu.pk/" target="_blank">Click Here</a>To apply online
              </div>
            </div>
          </div>
          <div class="card">
            <div class="card-header" role="tab" id="headingOne">
              <h5 class="mb-0">
                <a data-toggle="collapse" data-parent="#accordion" href="#two2" aria-expanded="true" aria-controls="two2">
                    Q.	I applied for admission in the last semester but was not successful. Do I need to apply again?
                </a>
              </h5>
            </div>
        
            <div id="two2" class="collapse" role="tabpanel" aria-labelledby="headingOne">
              <div class="card-block">
                  Yes, you must apply again if you want to be admitted to the program of your choice.
              </div>
            </div>
          </div>
          <div class="card">
            <div class="card-header" role="tab" id="headingOne">
              <h5 class="mb-0">
                <a data-toggle="collapse" data-parent="#accordion" href="#two3" aria-expanded="true" aria-controls="two3">
                    Q.	I am a foreign student and do not live in Pakistan. Am I eligible to apply for the program of my choice. If yes, how I can apply to the program of my choice?
                </a>
              </h5>
            </div>
        
            <div id="two3" class="collapse" role="tabpanel" aria-labelledby="headingOne">
              <div class="card-block">
                  You need to apply through your embassy or send your application directly to Director (Acad) IIUI. You can download the application by clicking<a href="https://www.iiu.edu.pk/events/admission/application-forms/2020/Admission-Form-Overseas-Candidates-23062020.pdf" target="_blank"> here</a>. You will be required to submit GMAT and TOEFL score.
              </div>
            </div>
          </div>
          <div class="card">
            <div class="card-header" role="tab" id="headingOne">
              <h5 class="mb-0">
                <a data-toggle="collapse" data-parent="#accordion" href="#two4" aria-expanded="true" aria-controls="two4">
                    Q.	Does my foreign degree qualify me for graduate study at International Islamic University?
                </a>
              </h5>
            </div>
        
            <div id="two4" class="collapse" role="tabpanel" aria-labelledby="headingOne">
              <div class="card-block">
                  International Islamic University’s assessment of a foreign degree is based on the characteristics of your national educational system, the type of institution you attended and the level of studies completed.
              </div>
            </div>
          </div>
          <div class="card">
            <div class="card-header" role="tab" id="headingOne">
              <h5 class="mb-0">
                <a data-toggle="collapse" data-parent="#accordion" href="#two5" aria-expanded="true" aria-controls="two5">
                    Q.	My transcripts are not in English. Will you accept them?
                </a>
              </h5>
            </div>
        
            <div id="two5" class="collapse" role="tabpanel" aria-labelledby="headingOne">
              <div class="card-block">
                  Yes, but the transcripts must be submitted with translations from your school or a professional translating service. The translations must be literal, complete versions of the original documents.
              </div>
            </div>
          </div>
          <div class="card">
            <div class="card-header" role="tab" id="headingOne">
              <h5 class="mb-0">
                <a data-toggle="collapse" data-parent="#accordion" href="#two6" aria-expanded="true" aria-controls="two6">
                    Q.	I am a female student and do not have residence in Islamabad. Do you have separate hostel facilities for girls?
                </a>
              </h5>
            </div>
        
            <div id="two6" class="collapse" role="tabpanel" aria-labelledby="headingOne">
              <div class="card-block">
                Yes, we have separate female campus and separate hostels for women. However, a hostel seat is subject to availability as a great number of students apply for it. So, even if you are guaranteed admission in the university, we do not guarantee you will get accommodation in the hostel.                </div>
            </div>
          </div>
          <div class="card">
            <div class="card-header" role="tab" id="headingOne">
              <h5 class="mb-0">
                <a data-toggle="collapse" data-parent="#accordion" href="#two7" aria-expanded="true" aria-controls="two7">
                    Q.	I do not have my own transport. Does the university provide transport facility to students?
                </a>
              </h5>
            </div>
        
            <div id="two7" class="collapse" role="tabpanel" aria-labelledby="headingOne">
              <div class="card-block">
                Yes, the university has excellent transport facility with separate buses for male and female students. These buses run round the clock to various points in Islamabad and Rawalpindi and their suburbs.
              </div>
            </div>
          </div>
          <div class="card">
            <div class="card-header" role="tab" id="headingOne">
              <h5 class="mb-0">
                <a data-toggle="collapse" data-parent="#accordion" href="#two8" aria-expanded="true" aria-controls="two8">
                    Q.	How do I apply for undergraduate admission? 
                </a>
              </h5>
            </div>
        
            <div id="two8" class="collapse" role="tabpanel" aria-labelledby="headingOne">
              <div class="card-block">
                Follow these steps: Read admission requirements for your program as given in our admission ads and on our website; complete and submit online application form; pay application fee and upload the receipt; Have your transcripts/test scores mailed to our office at the following address: admissions@iiu.edu.pk, admissions.fc@iiu.edu.pk, overseas.admissions@iiu.edu.pk
              </div>
            </div>
          </div>
          <div class="card">
            <div class="card-header" role="tab" id="headingOne">
              <h5 class="mb-0">
                <a data-toggle="collapse" data-parent="#accordion" href="#two9" aria-expanded="true" aria-controls="two9">
                    Q.	Do male and female students study together?
                </a>
              </h5>
            </div>
        
            <div id="two9" class="collapse" role="tabpanel" aria-labelledby="headingOne">
              <div class="card-block">
                Male and female students study at two separate campuses.
              </div>
            </div>
          </div>
          <div class="card">
            <div class="card-header" role="tab" id="headingOne">
              <h5 class="mb-0">
                <a data-toggle="collapse" data-parent="#accordion" href="#two10" aria-expanded="true" aria-controls="two10">
                    Q.	Do you have female teachers for female students?
                </a>
              </h5>
            </div>
        
            <div id="two10" class="collapse" role="tabpanel" aria-labelledby="headingOne">
              <div class="card-block">
                Most of the classes on the women campus are taught by women teachers with the exception of some where it is difficult to find an expert. 
              </div>
            </div>
          </div>
          <div class="card">
            <div class="card-header" role="tab" id="headingOne">
              <h5 class="mb-0">
                <a data-toggle="collapse" data-parent="#accordion" href="#two11" aria-expanded="true" aria-controls="two11">
                    Q.	What facilities do you have in your hostels?
                </a>
              </h5>
            </div>
        
            <div id="two11" class="collapse" role="tabpanel" aria-labelledby="headingOne">
              <div class="card-block">
                At IIUI hostels, we have shared rooms of two, three and more occupants, an almirah for each student, a table/chair, free wifi, and food plans.
              </div>
            </div>
          </div>                                                                                          
        </div>
      </div>
      <div class="col-md-4 third">
        <h5 class="faq-hed">Facilities</h5>
        <div id="accordion" role="tablist" aria-multiselectable="true">
          <div class="card">
            <div class="card-header" role="tab" id="headingOne">
              <h5 class="mb-0">
                <a data-toggle="collapse" data-parent="#accordion" href="#three1" aria-expanded="true" aria-controls="three1">
                    Computer Labs
                </a>
              </h5>
            </div>
        
            <div id="three1" class="collapse" role="tabpanel" aria-labelledby="headingOne">
              <div class="card-block">
                  Students at the IIU campuses enjoy a computer friendly environment, apart from access to computer in the labs, Research centers and libraries also have proper computer facilities for students.
              </div>
            </div>
          </div>
          <div class="card">
            <div class="card-header" role="tab" id="headingOne">
              <h5 class="mb-0">
                <a data-toggle="collapse" data-parent="#accordion" href="#three2" aria-expanded="true" aria-controls="three2">
                    Language Laboratories
                </a>
              </h5>
            </div>
        
            <div id="three2" class="collapse" role="tabpanel" aria-labelledby="headingOne">
              <div class="card-block">
                  IIUI maintains two separate state of the art language laboratories for Arabic and English languages. These labs are well equipped with modern techniques of teaching languages to the students of the University. There are well trained teachers who skillfully use the laboratory techniques and supplement classroom teaching in listening and training skills.
              </div>
            </div>
          </div>
          <div class="card">
            <div class="card-header" role="tab" id="headingOne">
              <h5 class="mb-0">
                <a data-toggle="collapse" data-parent="#accordion" href="#three3" aria-expanded="true" aria-controls="three3">
                    Libraries
                </a>
              </h5>
            </div>
        
            <div id="three3" class="collapse" role="tabpanel" aria-labelledby="headingOne">
              <div class="card-block">
                  IIUI has four (04) well equipped Libraries with Internet facilities and access to international data-base and digital libraries. Central Library maintains all important texts and recommended books. In addition, IIU has a Book Bank lending books to the students for semester studies. The library is continuously updated with the latest editions of relevant books. The library facility is available for students till 10:00 p.m.
              </div>
            </div>
          </div>
          <div class="card">
            <div class="card-header" role="tab" id="headingOne">
              <h5 class="mb-0">
                <a data-toggle="collapse" data-parent="#accordion" href="#three4" aria-expanded="true" aria-controls="three4">
                    Hostels
                </a>
              </h5>
            </div>
        
            <div id="three4" class="collapse" role="tabpanel" aria-labelledby="headingOne">
              <div class="card-block">
                  IIU maintains separate hostels for male and female students. However, availability of seat to newly admitted student is not guaranteed owing to availability of limited seats.
              </div>
            </div>
          </div>
          <div class="card">
            <div class="card-header" role="tab" id="headingOne">
              <h5 class="mb-0">
                <a data-toggle="collapse" data-parent="#accordion" href="#three5" aria-expanded="true" aria-controls="three5">
                    Medical Care Facility
                </a>
              </h5>
            </div>
        
            <div id="three5" class="collapse" role="tabpanel" aria-labelledby="headingOne">
              <div class="card-block">
                  IIUI has well equipped Medical Centers for male and female students with a team of qualified doctors for free outdoor treatment for minor ailments. Cases of serious nature are referred to Government Hospitals in Islamabad for hospitalization and treatment. IIU has set up a pathological laboratory to conduct various tests. The Medical Centers have three ambulances at their disposal to meet any emergency.
              </div>
            </div>
          </div>
          <div class="card">
            <div class="card-header" role="tab" id="headingOne">
              <h5 class="mb-0">
                <a data-toggle="collapse" data-parent="#accordion" href="#three6" aria-expanded="true" aria-controls="three6">
                    Banking Facility
                </a>
              </h5>
            </div>
        
            <div id="three6" class="collapse" role="tabpanel" aria-labelledby="headingOne">
              <div class="card-block">
                  Branches of HBL, ABL and FWBL have been established at the campus to provide banking faculty to the students and staff.
              </div>
            </div>
          </div>
          <div class="card">
            <div class="card-header" role="tab" id="headingOne">
              <h5 class="mb-0">
                <a data-toggle="collapse" data-parent="#accordion" href="#three7" aria-expanded="true" aria-controls="three7">
                    Post Office
                </a>
              </h5>
            </div>
        
            <div id="three7" class="collapse" role="tabpanel" aria-labelledby="headingOne">
              <div class="card-block">
                  A post Office has been set up by Pakistan Postal Services at old Campus to facilitate the students and staff of the University.
              </div>
            </div>
          </div>
          <div class="card">
            <div class="card-header" role="tab" id="headingOne">
              <h5 class="mb-0">
                <a data-toggle="collapse" data-parent="#accordion" href="#three8" aria-expanded="true" aria-controls="three8">
                    Sports / Recreation & Academic Competition
                </a>
              </h5>
            </div>
        
            <div id="three8" class="collapse" role="tabpanel" aria-labelledby="headingOne">
              <div class="card-block">
                  IIU encourages participation of students in co curricula activities. It provides ample of facilities for indoor and outdoor games for boys as well girls. IIU students actively participate in Inter-University competitions and bring good name to IIU by winning medals, trophies & leading positions in these competitions. A modern Gymnasium has been built for female students at their campus. Coaching facilities have also been made available on both Campuses. Recreation trips are arranged by the faculties for male/female students especially on weekends or during vacations. Cultural week is celebrated every year. The IIU also encourages participation of students in academic competitions seminars workshops and curriculum activities
              </div>
            </div>
          </div>
          <div class="card">
            <div class="card-header" role="tab" id="headingOne">
              <h5 class="mb-0">
                <a data-toggle="collapse" data-parent="#accordion" href="#three9" aria-expanded="true" aria-controls="three9">
                    Transport
                </a>
              </h5>
            </div>
        
            <div id="three9" class="collapse" role="tabpanel" aria-labelledby="headingOne">
              <div class="card-block">
                  IIUI maintains a transport pool to facilitate the students from designated points and routes from Islamabad/Rawalpindi.
              </div>
            </div>
          </div>
          <div class="card">
            <div class="card-header" role="tab" id="headingOne">
              <h5 class="mb-0">
                <a data-toggle="collapse" data-parent="#accordion" href="#three10" aria-expanded="true" aria-controls="three10">
                    Financial Assistance from IIU
                </a>
              </h5>
            </div>
        
            <div id="three10" class="collapse" role="tabpanel" aria-labelledby="headingOne">
              <div class="card-block">
                  No financial assistance is given by the University to any student during first semester. However partial financial assistance on need cum merit basis to deserving students is considered upon completion of first semester with minimum CGPA of 3.00/4.00 and as determined by the faculty committee. Students are advised not to rely on Financial Assistance. The University may reduce or discontinue the provision of financial assistance at any stage depending upon its available resources.
              </div>
            </div>
          </div>
          <div class="card">
            <div class="card-header" role="tab" id="headingOne">
              <h5 class="mb-0">
                <a data-toggle="collapse" data-parent="#accordion" href="#three11" aria-expanded="true" aria-controls="three11">
                    Financial Assistance form Worker Welfare Fund
                </a>
              </h5>
            </div>
        
            <div id="three11" class="collapse" role="tabpanel" aria-labelledby="headingOne">
              <div class="card-block">
                  Worker Welfare Fund (WWF), Ministry of Labor, Government of Pakistan provides full financing for wards(sons and daughters) of registered industrial workers of public and private industrial units, if they pass test for admission for the academic programs at IIUI. University only facilitates applicants for assistance from Worker Welfare Fund (WWF) and does not take any responsibility what so ever on this account. 2. Pakistan Bait-ul-Mal also facilitates deserving students from IIU to some extent as per their policy.
              </div>
            </div>
          </div>                                                                                                    
        </div>
      </div>
    </div>
</div>
@include('results.footer')