@section('title')
Apply for Scholarship
@endsection
@include('frontend.schheader')

@php
if($applicationStatus->step_save == 1 && $hash ==''){
  $readonlyUser = "readonly='readonly'";
  $disabledUser = "disabled";
  $disabledUser1 = 'disabled="disabled"';
}else{
  $readonlyUser='';   
  $disabledUser = '';
  $disabledUser1 = '';
}
@endphp

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          <i class="fa fa-user text-yellow"></i> Update Scholarship
          <small>Field mark with (*) are mandatory</small>
        </h1>
      </section>

      @if(session()->has('message'))
            <div class="alert alert-danger">
                {{ session()->get('message') }}
            </div>
        @endif

      @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    @if ( $status )
        <div class="alert alert-success" role="alert"> {{ $status }}</div>
    @endif  
    
      <!-- Main content -->
      <section class="content">

        <form action="{{route('scholarship', $hash)}}" class="scholarshipDetailForm" method="POST" enctype="multipart/form-data">
          <input type="hidden" name="userId" value="{{$userdetail->userId}}">
          <input type="hidden" name="server_date" value="{{date('Y-m-d h:i:s')}}">
          {{csrf_field()}}

        <!-- SELECT2 EXAMPLE -->
        <div class="box box-info">

          <!--<div class="box-header with-border">
            <h3 class="box-title">Enter Personal Information</h3>
          </div>-->

          <div class="box-body">
            <div class="row">
              <div class="col-lg-4">
                <div class="form-group">
                   <div class="form-group">
                  <label class="req-rd">Select Scholarship</label>
                    <select class="form-control select2" id="listscholarship" name="listscholarship" required style="width: 100%;">
                    <option value="">Select Scholarship</option>
                    @foreach ( $listscholarship as $ls )
                    @if($ls->status=='1')
                    <option value="{{$ls->id}}"> {{$ls->name}} </option>
                    @endif
                    @endforeach
                </select> 
                </div>
                </div>
              </div>
            </div>
          </div>

          <div class="box-header with-border">
            <h3 class="box-title">Enter Personal Information</h3>
          </div>

          <!-- /.box-header -->
          <div class="box-body">
            <div class="row">

              <div class="col-md-4">
                <!-- /.form-group -->
                <div class="form-group">
                  <label>Applicant Name</label>
                  <input type="text" name="full_name" id="full_name" class="form-control" value="{{$userdetail->name}}" required {{ $readonlyUser }} readonly>
                </div>

                <div class="form-group">
                  <label>CNIC/B-Form/Passport No.</label><br />
                  <input type="text" class="form-control" id="cnic" name="cnic" value="{{$userdetail->cnic}}" required readonly>
                </div>

                <div class="form-group">
                    <label class="contact-label"> Student Mobile No.(301252xxx)</label>
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-phone"></i>
                      </div>
                      @php
                      $carrier = '';
                      $phone = '';
                      if($applicantDetail->mobile){
                        $carrier = substr($applicantDetail->mobile, 2, 3);
                        $phone = substr($applicantDetail->mobile, 5, 7);
                      }
                      @endphp
                      <select name="carrier" id="carrier" class="form-control {{ ($userdetail->fkNationality == 1) ? 'ext-show' : 'ext-hide-2'}}" {{ ($userdetail->fkNationality == 1) ? 'required' : ''}} style="width: 30%;" {{$disabledUser1}}>
                        <option value="">Code</option>
                        <option value="300" {{ ($carrier == '300' || old('carrier') == '300') ? 'selected' : ''}}>0300</option>
                        <option value="301" {{ ($carrier == '301' || old('carrier') == '301') ? 'selected' : ''}}>0301</option>
                        <option value="302" {{ ($carrier == '302' || old('carrier') == '302') ? 'selected' : ''}}>0302</option>
                        <option value="303" {{ ($carrier == '303' || old('carrier') == '303') ? 'selected' : ''}}>0303</option>
                        <option value="304" {{ ($carrier == '304' || old('carrier') == '304') ? 'selected' : ''}}>0304</option>
                        <option value="305" {{ ($carrier == '305' || old('carrier') == '305') ? 'selected' : ''}}>0305</option>
                        <option value="306" {{ ($carrier == '306' || old('carrier') == '306') ? 'selected' : ''}}>0306</option>
                        <option value="307" {{ ($carrier == '307' || old('carrier') == '307') ? 'selected' : ''}}>0307</option>
                        <option value="308" {{ ($carrier == '308' || old('carrier') == '308') ? 'selected' : ''}}>0308</option>
                        <option value="309" {{ ($carrier == '309' || old('carrier') == '309') ? 'selected' : ''}}>0309</option>
                        <option value="310" {{ ($carrier == '310' || old('carrier') == '310') ? 'selected' : ''}}>0310</option>
                        <option value="311" {{ ($carrier == '311' || old('carrier') == '311') ? 'selected' : ''}}>0311</option>
                        <option value="312" {{ ($carrier == '312' || old('carrier') == '312') ? 'selected' : ''}}>0312</option>
                        <option value="313" {{ ($carrier == '313' || old('carrier') == '313') ? 'selected' : ''}}>0313</option>
                        <option value="314" {{ ($carrier == '314' || old('carrier') == '314') ? 'selected' : ''}}>0314</option>
                        <option value="315" {{ ($carrier == '315' || old('carrier') == '315') ? 'selected' : ''}}>0315</option>
                        <option value="316" {{ ($carrier == '316' || old('carrier') == '316') ? 'selected' : ''}}>0316</option>
                        <option value="317" {{ ($carrier == '317' || old('carrier') == '317') ? 'selected' : ''}}>0317</option>
                        <option value="318" {{ ($carrier == '318' || old('carrier') == '318') ? 'selected' : ''}}>0318</option>
                        <option value="320" {{ ($carrier == '320' || old('carrier') == '320') ? 'selected' : ''}}>0320</option>
                        <option value="321" {{ ($carrier == '321' || old('carrier') == '321') ? 'selected' : ''}}>0321</option>
                        <option value="322" {{ ($carrier == '322' || old('carrier') == '322') ? 'selected' : ''}}>0322</option>
                        <option value="323" {{ ($carrier == '323' || old('carrier') == '323') ? 'selected' : ''}}>0323</option>
                        <option value="324" {{ ($carrier == '324' || old('carrier') == '324') ? 'selected' : ''}}>0324</option>
                        <option value="330" {{ ($carrier == '330' || old('carrier') == '330') ? 'selected' : ''}}>0330</option>
                        <option value="331" {{ ($carrier == '331' || old('carrier') == '331') ? 'selected' : ''}}>0331</option>
                        <option value="332" {{ ($carrier == '332' || old('carrier') == '332') ? 'selected' : ''}}>0332</option>
                        <option value="333" {{ ($carrier == '333' || old('carrier') == '333') ? 'selected' : ''}}>0333</option>
                        <option value="334" {{ ($carrier == '334' || old('carrier') == '334') ? 'selected' : ''}}>0334</option>
                        <option value="335" {{ ($carrier == '335' || old('carrier') == '335') ? 'selected' : ''}}>0335</option>
                        <option value="336" {{ ($carrier == '336' || old('carrier') == '336') ? 'selected' : ''}}>0336</option>
                        <option value="337" {{ ($carrier == '337' || old('carrier') == '337') ? 'selected' : ''}}>0337</option>
                        <option value="340" {{ ($carrier == '340' || old('carrier') == '340') ? 'selected' : ''}}>0340</option>
                        <option value="341" {{ ($carrier == '341' || old('carrier') == '341') ? 'selected' : ''}}>0341</option>                        
                        <option value="342" {{ ($carrier == '342' || old('carrier') == '342') ? 'selected' : ''}}>0342</option>
                        <option value="343" {{ ($carrier == '343' || old('carrier') == '343') ? 'selected' : ''}}>0343</option>
                        <option value="344" {{ ($carrier == '344' || old('carrier') == '344') ? 'selected' : ''}}>0344</option>
                        <option value="345" {{ ($carrier == '345' || old('carrier') == '345') ? 'selected' : ''}}>0345</option>
                        <option value="346" {{ ($carrier == '346' || old('carrier') == '346') ? 'selected' : ''}}>0346</option>
                        <option value="347" {{ ($carrier == '347' || old('carrier') == '347') ? 'selected' : ''}}>0347</option>
                        <option value="348" {{ ($carrier == '348' || old('carrier') == '348') ? 'selected' : ''}}>0348</option>
                        <option value="349" {{ ($carrier == '349' || old('carrier') == '349') ? 'selected' : ''}}>0349</option>
                      </select>

                      <input type="text" class="form-control {{ ($userdetail->fkNationality == 1) ? 'ext-show' : 'ext-hide-2'}}" style="width: 70%;" onkeypress="return isNumberKey(event)" id="phone" name="phone" minlength="7" maxlength="7" value="{{$phone ?: old('phone')}}" {{ ($userdetail->fkNationality == 1) ? 'required' : ''}} {{ $readonlyUser }}>
                      <input type="number" class="form-control {{ ($userdetail->fkNationality == 2 || $userdetail->fkNationality == 3) ? 'ext-show' : 'ext-hide-2'}}" {{ ($userdetail->fkNationality == 2 || $userdetail->fkNationality == 3) ? 'required' : ''}} id="overseas_phone"  value="{{$applicantDetail->mobile ?: old('overseas_phone')}}" name="overseas_phone" {{ $readonlyUser }}>
                    </div>
                  </div> 

                <div class="form-group">
                  <label>Registration Number</label>
                  <input type="text" name="reg_number" id="reg_number" 
                    value="{{$scholarshipinformation->reg ?? old('reg_number')}}" class="form-control">
                </div>

                <div class="form-group">
                  <label class="req-rd">CGPA</label>
                  <input type="Number" min="0" max="4" name="cgpa" id="cgpa" step=".01" class="form-control" value="{{old('cgpa')}}">
                </div>
  

                  <div class="form-group">
                  <label class="req-rd">Fee of last Institution attended</label>
                  <input type="Number" min="0" name="last_inst_fee" id="last_inst_fee"  class="form-control" value="{{old('last_inst_fee')}}" required>
                </div>            

                <!-- /.form-group -->

                <div class="form-group">
                  <label class="req-rd">Year of Passing (FA/FSC) </label>
                  <input type="number" min="2000" max="2022" name="year_of_passing" id="year_of_passing" class="form-control" value="{{old('year_of_passing')}}"  required>
                </div>

              </div>
              <!-- /.col -->
              <div class="col-md-4">
              
                <div class="form-group">
                  <label>Marital Status</label>
                    <select class="form-control select2" id="marital_status" readonly name="marital_status" style="width: 100%;" required {{$disabledUser}}>
                    <option value="">Select</option>
                    <option value="Single" {{ ($applicantDetail->married == 'Single' || old('marital_status') == 'Single') ? 'selected' : ''}}>Single</option>
                    <option value="Married" {{ ($applicantDetail->married == 'Married' || old('marital_status') == 'Married') ? 'selected' : ''}}>Married</option>
                    <option value="Separated/Divorced" {{ ($applicantDetail->married == 'Separated/Divorced' || old('marital_status') == 'Separated/Divorced') ? 'selected' : ''}}>Separated/Divorced</option>
                    <option value="Widowed" {{ ($applicantDetail->married == 'Widowed' || old('marital_status') == 'Widowed') ? 'selected' : ''}}>Widowed</option>
                  </select>
              </div>
                

                <!-- <div class="form-group">
                  <label>Department</label>
                    <select name="department" id="department" class="form-control">
                     <option value="">Select Department</option>
                    </select> 
                </div> -->

                <div class="form-group">
                  <label>Nationality</label>
                    <select class="form-control select2" id="nationality" name="nationality" required style="width: 100%;" required {{ $disabledUser }}>
                    <option value="">Select Nationality</option>
                    @foreach ( $nationalilty as $n )
                      <option value="{{$n->id}}" {{ ($userdetail->fkNationality == $n->id) ? 'selected' : ''}}>{{$n->nationality}}</option>
                    @endforeach
                </select> 
                </div>

                <div class="form-group {{ ($userdetail->fkNationality == 1) ? 'ext-show' : 'ext-hide-2'}}">
                    <label>Domicile</label>
                    <select class="form-control select2" id="domicile" name="domicile" {{ ($userdetail->fkNationality == 1) ? 'required' : ''}} style="width: 100%;" {{ $disabledUser }}>
                      <option value="">Select Domicile</option>
                      @foreach ( $province as $n )
                        <option value="{{$n->pkProvId}}" {{ ($applicantDetail->fkDomicile == $n->pkProvId) ? 'selected' : ''}}>{{$n->provName}}</option>
                      @endforeach
                    </select>
                  </div>

                <div class="form-group">
                  <label class="req-rd">Semester(s) Completed</label>
                    <input type="Number" min="1" max="8" name="semester_complete" id="semester_complete" class="form-control" value="{{old('semester_complete')}}" required>
                </div>

                 <div class="form-group">
                  <label class="req-rd">Semester(s) left</label>
                    <input type="Number" min="1" max="8"type="Number" min="1" max="8" name="semester_left" id="semester_left"  class="form-control" required>
                </div>

                <div class="form-group">
                  <label class="req-rd">Name of last Institution attended</label>
                  <input type="text" name="last_inst_attend" id="last_inst_attend" class="form-control" required>
                </div>

                <div class="form-group">
                  <label class="req-rd">Total Marks in FA/FSC</label>
                  <input type="text" name="total_marks" id="total_marks" class="form-control" required>
                </div>
                 

                <!-- /.form-group -->



                <!-- /.form-group -->                
              </div>

              <div class="col-md-4">
                <!-- /.form-group -->

                 <div class="form-group">
                 <label>Father Name</label>
                  <input type="text" name="father_name" id="father_name" class="form-control" value="{{$applicantDetail->fatherName ?? old('father_name')}}" required {{ $readonlyUser }} readonly>
                </div>
                
                <div class="form-group">
                  <label>Father CNIC</label>
                    <input type="text" name="father_cnic" id="father_cnic" class="form-control" value="{{$applicantDetail->father_cnic ?? old('father_cnic')}}" required {{ $readonlyUser }} readonly>
                </div>

                <div class="form-group">
                  <label>Father Mobile No. </label>
                    <input type="text" name="father_mobile" id="father_mobile" class="form-control" value="{{$applicantDetail->fatherMobile ?? old('fatherMobile')}}" required {{ $readonlyUser }} readonly>
                </div>

                <div class="form-group">
                  <label>Father Status</label>
                    <input type="text" class="form-control" id="father_status" name="father_status" value="<?php echo ($applicantDetail->fatherStatus==1)?'Alive':'Deceased'; ?>" required readonly>
                </div>

                <div class="form-group">
                 <label>Father Occupation</label>
                  <input type="text" name="father_occupation" id="father_occupation" class="form-control" value="{{$applicantDetail->fatherOccupation ?? old('father_occupation')}}" required {{ $readonlyUser }} readonly>
                </div>

                <div class="form-group">
                   <label>Guaradian Name</label>
                  <input type="text" name="Guradian_Spouse" id="Guradian_Spouse" class="form-control" value="{{$applicantDetail->Guradian_Spouse ?? old('Guradian_Spouse')}}" required {{ $readonlyUser }} readonly>
                </div>  

                 <div class="form-group">
                  <label class="req-rd">Marks Obtain in FA/FSC</label>
                  <input type="text" name="marks_obtain" id="marks_obtain" class="form-control" required>
                </div>


              </div>



             

              <!-- /.col -->
            </div>
          </div>

          <div class="box-header with-border box box-info">
            <h3 class="box-title">Family Information</h3>
          </div>

          <!-- /.box-header -->
          <div class="box-body">
            <div class="row">

              <div class="col-md-3">
                <!-- /.form-group -->
                <div class="form-group">
                  <label>Total No. of dependend family members</label>
                   <input type="Number" class="form-control" id="dep_family_mem" name="dep_family_mem" value="{{$applicantDetail->dependants}}" required readonly>
                </div>               
                <!-- /.form-group -->

                <!-- /.form-group -->
                <div class="form-group">
                  <label class="req-rd">Income from Land/ Property</label>
                  <input type="Number" min="0" name="land_income" id="land_income" class="form-control" required>
                </div>               
                <!-- /.form-group -->

                <!-- /.form-group -->
                <div class="form-group">
                  <label class="req-rd">Any other Supporting Person</label>
                  <input type="text" name="supporting_person" id="supporting_person" class="form-control" required>
                </div>               
                <!-- /.form-group -->


              </div>

              <div class="col-md-3">
                <!-- /.form-group -->
                <div class="form-group">
                  <label class="req-rd">Family Member Studying</label>
                  <input type="Number" min="0" name="family_mem_studing" id="family_mem_studing" class="form-control" required >
                </div>               
                <!-- /.form-group -->

                <!-- /.form-group -->
                <div class="form-group">
                  <label class="req-rd">Misc.Income (from other sources)</label>
                  <input type="Number" min="0" name="misc_income" id="misc_income" class="form-control" required>
                </div>               
                <!-- /.form-group -->


                <!-- /.form-group -->
                <div class="form-group">
                  <label>Father Death Date <small>(in case of death) </small></label>
                  <input type="date" id="death_date" name="death_date" class="form-control">
                </div>               
                <!-- /.form-group -->

              </div>

              <div class="col-md-3">
                <!-- /.form-group -->
                <div class="form-group">
                  <label>Father/Husband/Guardian Income</label>
                  <input type="text" class="form-control" id="guardian_income" name="guardian_income" value="{{$applicantDetail->monthlyIncome}}" required readonly>
                </div>               
                <!-- /.form-group -->

                <!-- /.form-group -->
                <div class="form-group">
                  <label class="req-rd">Total Monthly Income</label>
                  <input style="font-weight: bolder;" type="Number" min="0" name="monthly_income" id="monthly_income" class="form-control" required readonly onchange="changeUpdate(this.value)">
                </div>               
                <!-- /.form-group -->


              </div>

              <div class="col-md-3">
                <!-- /.form-group -->
                <div class="form-group">
                  <label class="req-rd">Mother Income (if any)</label>
                  <input type="Number" min="0" name="mother_income" id="mother_income" class="form-control">
                </div>               
                <!-- /.form-group -->

                <!-- /.form-group -->
                <div class="form-group">
                  <label class="req-rd">Earning Family Members</label>
                  <input type="Number" min="0" name="family_mem_earning" id="family_mem_earning" class="form-control">
                </div>               
                <!-- /.form-group -->
              </div>

            </div>
          </div>


          <div class="box-header with-border box box-info">
            <h3 class="box-title">Family Expenditures (Monthly)</h3>
          </div>

          <!-- /.box-header -->
          <div class="box-body">
            <div class="row">

              <div class="col-md-3">
                <!-- /.form-group -->
                <div class="form-group">
                  <label class="req-rd">Gas Bill</label>
                  <input type="Number" min="0" name="gas_bill" id="gas_bill" class="form-control" required>
                </div>               
                <!-- /.form-group -->

                <!-- /.form-group -->
                <div class="form-group">
                  <label class="req-rd">Family's Expenditure on Education (Including your own)</label>
                  <input type="Number" min="0" name="edu_expenditure" id="edu_expenditure" class="form-control" required>
                </div>               
                <!-- /.form-group -->

                 <!-- /.form-group -->
                <div class="form-group">
                  <label class="req-rd">Monthly Pocket money Recieved from Home/ any Source</label>
                  <input type="Number" min="0" name="pocketmoney" id="pocketmoney" class="form-control"
                   required>
                </div>               
                <!-- /.form-group -->


              </div>

              <div class="col-md-3">
                
                <!-- /.form-group -->
                <div class="form-group">
                  <label class="req-rd">Electricity Bill</label>
                  <input type="Number" min="0" name="electricity_bill" id="electricity_bill" class="form-control" required>
                </div>               
                <!-- /.form-group -->

                <!-- /.form-group -->
                <div class="form-group">
                  <label class="req-rd">Family's Accommodation Expenses (Including your own)</label>
                  <input type="Number" min="0" name="family_accommodation" id="family_accommodation" class="form-control" required>
                </div>               
                <!-- /.form-group -->

                 <!-- /.form-group -->
                <div class="form-group">
                  <label class="req-rd">Total Monthly Expenditure</label>
                  <input style="font-weight: bolder;" type="Number" min="0" name="monthly_expensiture" id="monthly_expensiture" class="form-control" readonly>
                </div>               
                <!-- /.form-group -->

              </div>

              <div class="col-md-3">
                <!-- /.form-group -->
                <div class="form-group">
                  <label class="req-rd">Telephone Bill</label>
                  <input type="Number" min="0" name="telephone_bill" id="telephone_bill" class="form-control" required >
                </div>               
                <!-- /.form-group -->

                <!-- /.form-group -->
                <div class="form-group">
                  <label class="req-rd">Food Expenses</label>
                  <input type="Number" min="0" name="food_expense" id="food_expense" class="form-control" required>
                </div>               
                <!-- /.form-group -->

                 <!-- /.form-group -->
                <div class="form-group">
                  <label class="req-rd">Disposable monthly Income</label>
                  <input style="font-weight: bolder;" type="text" name="disposable_income" id="disposable_income"  class="form-control" required readonly>
                </div>               
                <!-- /.form-group -->

              </div>

              <div class="col-md-3">
                <!-- /.form-group -->
                <div class="form-group">
                  <label class="req-rd">Water Bill</label>
                  <input type="Number" min="0" name="water_bill" id="water_bill" class="form-control">
                </div>               
                <!-- /.form-group -->

                 <!-- /.form-group -->
                <div class="form-group">
                  <label class="req-rd">Medical Expenses</label>
                  <input type="Number" min="0" name="medical_expense" id="medical_expense" class="form-control" required>
                </div>               
                <!-- /.form-group -->

              </div>

            </div>
          </div>


          <div class="box-header with-border box box-info">
            <h3 class="box-title">Family Fixed Assets</h3>
          </div>

          <!-- /.box-header -->
          <div class="box-body">

            <div class="row">
              <div class="col-md-3">
                <label class="req-rd">Type of Vehicles</label>
                <div class="form-group">
                  <label class="checkbox-inline">
                    <input type="checkbox" name="vehiclecategory[]" value="Car">Car
                  </label>
                  <label class="checkbox-inline">
                    <input type="checkbox" name="vehiclecategory[]" value="Bike">Bike
                  </label>
                  <label class="checkbox-inline">
                    <input type="checkbox" name="vehiclecategory[]" value="Cycle">Cycle
                  </label>
                  <label class="checkbox-inline">
                    <input type="checkbox" name="vehiclecategory[]" value="AnyOther">AnyOther
                  </label>
                </div>
              </div>


              <div class="col-md-6">
                <label class="req-rd">Type of Land</label>
                <div class="form-group">
                  <label class="checkbox-inline">
                    <input type="checkbox" name="landtype[]" value="agriculture land">Agriculture Land
                  </label>
                  <label class="checkbox-inline">
                    <input type="checkbox" name="landtype[]" value="home">Home Land
                  </label>
                  <label class="checkbox-inline">
                    <input type="checkbox" name="landtype[]" value="other(plot and shop)">Other (Plot and Shop)
                  </label>
                  <label class="checkbox-inline">
                    <input type="checkbox" name="landtype[]" value="anyother">AnyOther
                  </label>
                </div>
              </div>
            </div>


            <div class="row">
              <div class="col-md-3">

                

                <!-- /.form-group -->
                <div class="form-group">
                  <label class="req-rd">No. of Vehicles</label>
                  <input type="Number" min="0" name="vehicles" id="vehicles" class="form-control" required>
                </div>               
                <!-- /.form-group -->

                <!-- /.form-group -->
                <div class="form-group">
                  <label class="req-rd">Vehicle Type and Model</label>
                  <input type="text" name="vehicle_type" id="vehicle_type" class="form-control" required>
                </div>               
                <!-- /.form-group -->

                <!-- /.form-group -->
                <div class="form-group">
                  <label class="req-rd">Vehicle Engine Capacity</label>
                  <input type="text" name="vehicle_engine_capacity" id="vehicle_engine_capacity" class="form-control" required>
                </div>               
                <!-- /.form-group -->
              </div>

              <div class="col-md-3">
                
                <!-- /.form-group -->
                <div class="form-group">
                  <label class="req-rd">Size of Land (Acres)</label>
                  <input type="text" name="land_size" id="land_size" class="form-control" required>
                </div>               
                <!-- /.form-group -->

                <!-- /.form-group -->
                <div class="form-group">
                  <label class="req-rd">Value of Land</label>
                  <input type="Number" min="0" name="land_value" id="land_value" class="form-control" required>
                </div>               
                <!-- /.form-group -->

                <!-- /.form-group -->
                <div class="form-group">
                  <label class="req-rd">Accommodation and Location</label>
                  <input type="text" name="accommodation_land" id="accommodation_land" class="form-control" required>
                </div>               
                <!-- /.form-group -->

              </div>

              <div class="col-md-3">
                <!-- /.form-group -->
                <div class="form-group">
                  <label class="req-rd">Type of Accommodation and size</label>
                  <input type="text" name="accommodation_type" id="accommodation_type" class="form-control" required>
                </div>               
                <!-- /.form-group -->

                <!-- /.form-group -->
                <div class="form-group">
                  <label class="req-rd">Value of Home</label>
                  <input type="Number" min="0" name="home_value" id="home_value" class="form-control" required>
                </div>               
                <!-- /.form-group -->

                <!-- /.form-group -->
                <div class="form-group">
                  <label class="req-rd">Status of Home</label>
                  <select name='home_status' rows='5' id='home_status' class='form-control select2'>
                      <option>--- Select Option ---</option>
                      <option value="joint">Joint</option>
                      <option value="rented">Rented</option>
                      <option value="self owned">Self Owned</option>
                      <option value="government employee">Government Employee</option>
                  </select>
                </div>               
                <!-- /.form-group -->
              </div>

              <div class="col-md-3">
                <!-- /.form-group -->
                <div class="form-group">
                  <label class="req-rd">Value of Property</label>
                  <input type="Number" min="0" name="property_value" id="property_value" class="form-control">
                </div>               
                <!-- /.form-group -->


                <!-- /.form-group -->
                <div class="form-group">
                  <label class="req-rd">Any other Property</label>
                  <input type="Number" min="0" name="other_property" id="other_property" class="form-control" required>
                </div>               
                <!-- /.form-group -->
              </div>

            </div>
          </div>


          <div class="box-header with-border box box-info">
            <h3 class="box-title">Family Current Assets</h3>
          </div>

          <!-- /.box-header -->
          <div class="box-body">
            <div class="row">

              <div class="col-md-3">
                <!-- /.form-group -->
                <div class="form-group">
                  <label class="req-rd">Bank Balance (Father/Mother/Guardian/Personal)</label>
                  <input type="Number" min="0" name="bank_balance" id="bank_balance" class="form-control"  required>
                </div>               


                <!-- /.form-group -->
                <div class="form-group">
                  <label class="req-rd">Did you have any Cattle? </label>
                  <select name='cattle_status' rows='5' id='cattle_status' class='form-control select2' onchange = "ShowHideDiv()">
                      <option>--- Select Option ---</option>
                      <option value="1">Yes</option>
                      <option value="0" selected>No</option>
                  </select>
                </div>               
              </div>

              <div class="col-md-3">
                
                 <!-- /.form-group -->
                <div class="form-group">
                  <label class="req-rd">Stock/Prize Bond</label>
                  <input type="Number" min="0" name="stock" id="stock" class="form-control" required>
                </div>               
                <!-- /.form-group -->

                <div class="form-group" id="noofcattles_div" style="display:none">
                     <label class="req-rd"> Number of Cattles </label>
                     <input type="Number"  min="0" name="no_of_cattles" id="no_of_cattles" class="form-control">
                </div>
              </div>

              <div class="col-md-3">
                <!-- /.form-group -->
                <div class="form-group">
                  <label class="req-rd">Misc</label>
                  <input type="Number" min="0" name="misc" id="misc" class="form-control" required>
                </div>               
                <!-- /.form-group -->

              </div>

              <div class="col-md-3">

                <!-- /.form-group -->
                <div class="form-group">
                  <label class="req-rd">Total Assets</label>
                  <input style="font-weight: bolder;" type="Number" min="0" name="total_assets" id="total_assets" class="form-control" readonly required>
                </div>               
                <!-- /.form-group -->
              </div>

            </div>
          </div>

           <div class="box-header with-border box box-info">
            <h3 class="box-title">Other Details</h3>
          </div>

          <!-- /.box-header -->
          <div class="box-body">
            <div class="row">
              
              <div class="col-md-3">
                <!-- /.form-group -->
                <div class="form-group">
                  <label class="req-rd">Enter Remarks</label>
                  <input type="text" name="remarks" id="remarks" class="form-control" required>
                </div>               
                <!-- /.form-group -->
              </div>
              
              <div class="col-md-3">
                <!-- /.form-group -->
                <div class="form-group">
                  <label >Present Address</label>
                  <textarea class="form-control" value="{{$applicantAddress->addressPmt ?? old('permanent_address')}}" name="present_address" id="present_address" rows="1" required {{ $disabledUser }} readonly>{{$applicantAddress->addressPmt ?? old('permanent_address')}}</textarea>
                </div>               
                <!-- /.form-group -->
              </div>

              <div class="col-md-3">
                <!-- /.form-group -->
                <div class="form-group">
                  <label>Permanent Address</label>
                  <textarea class="form-control" value="{{$applicantAddress->addressPo ?? old('mailing_address')}}" name="mailing_address" id="mailing_address" rows="1" required {{ $disabledUser }} readonly>{{$applicantAddress->addressPo ?? old('mailing_address')}}</textarea>
                </div>               
                <!-- /.form-group -->
              </div>

              <div class="col-md-3">
                <!-- /.form-group -->
                <div class="form-group">
                  <label>City</label>
                  @if($userdetail->fkNationality == 2 || $userdetail->fkNationality == 3)
                    <input class="form-control" name="mailing_city" id="mailing_city" value ="{{ ($applicantAddress->cityPo == '') ? old('mailing_city') : $applicantAddress->cityPo }}" style="width: 100%;" required {{ $disabledUser }}>
                  @endif
                  @if($userdetail->fkNationality == 1)
                    <select class="form-control select2" name="mailing_city" id="mailing_city" style="width: 100%;" required {{ $disabledUser }}>
                      <option value="">Select City</option>
                      @foreach ( $city as $n )
                        <option value="{{$n->city}}" {{ ($applicantAddress->cityPo == $n->city || old('mailing_city') == $n->city) ? 'selected' : ''}}>{{$n->city}}</option>
                      @endforeach
                    </select>
                  @endif                    
                </div>
                <!-- /.form-group --> 
              </div>

            </div>
          </div>

          <div class="box-header with-border box box-info">
            <h3 class="box-title">Other Details</h3>
          </div>

          <!-- /.box-header -->
          <div class="box-body">
            <div class="row">


              <div class="col-lg-12">
                <!-- /.form-group -->
                <div class="form-group">
                  <label class="req-rd">How were the admission / first Semester charges paid / any other source of financing?</label>
                  <textarea class="form-control" rows="2", cols="20" id="source_of_financing" name="source_of_financing" required></textarea>               
                <!-- /.form-group -->
              </div>
            </div>

              
              <div class="col-lg-12">
                <!-- /.form-group -->
                <div class="form-group">
                  <label class="req-rd">Request of the Student/ Reasons for Applying</label>
                  <textarea class="form-control" rows="4", cols="50" id="request" name="request">{{$scholarshipFamilyAssets->reason_for_apply ?? old('request')}}</textarea>               
                <!-- /.form-group -->
              </div>
            </div>
          </div>

          <div class="box-footer">
              <button class="btn btn-social btn-success" id="personal-detail-btn" type="submit"><i class="fa fa-save"></i> Apply</button>
          </div>
        </div>
      </form>
        <!-- /.box --> 
      </section>
      <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->



<script type='text/javascript' src='https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js'></script>


   

<script>

 function ShowHideDiv() {
        var cattle_status = document.getElementById("cattle_status");
        var noofcattles_div = document.getElementById("noofcattles_div");
        noofcattles_div.style.display = cattle_status.value == "1" ? "block" : "none";
    }




$(function() {
$("#bank_balance, #stock, #misc").on("keydown keyup", total_assets);

  function total_assets() {
  $("#total_assets").val(Number($("#bank_balance").val())+ Number($("#stock").val())+ Number($("#misc").val()));
  }
});



$(function() {
$("#guardian_income, #mother_income, #land_income, #misc_income").on("keydown keyup", monthly_income);

  function monthly_income() {
  $("#monthly_income").val(Number($("#guardian_income").val())+ Number($("#mother_income").val())+ Number($("#land_income").val())+ Number($("#misc_income").val()));
  }
});


$(function() {
$("#guardian_income, #mother_income, #land_income, #misc_income, #gas_bill, #electricity_bill,#telephone_bill,#water_bill,#edu_expenditure,#family_accommodation,#food_expense,#medical_expense,#pocketmoney").on("keydown keyup", disposable_income);

  function disposable_income() {
  $("#disposable_income").val(Number($("#guardian_income").val())+ Number($("#mother_income").val())+ Number($("#land_income").val())+ Number($("#misc_income").val()) - Number($("#gas_bill").val()) - Number($("#electricity_bill").val()) - Number($("#telephone_bill").val()) - Number($("#water_bill").val()) - Number($("#edu_expenditure").val()) - Number($("#family_accommodation").val()) - Number($("#food_expense").val()) - Number($("#medical_expense").val()) - Number($("#pocketmoney").val())   );
  }
});




$(function() {
    $("#gas_bill, #electricity_bill,#telephone_bill,#water_bill,#edu_expenditure,#family_accommodation,#food_expense,#medical_expense,#pocketmoney").on("keydown keyup", monthly_expensiture);

  function monthly_expensiture() {
  $("#monthly_expensiture").val(Number($("#gas_bill").val())+ Number($("#pocketmoney").val())+ Number($("#medical_expense").val())+ Number($("#food_expense").val())+ Number($("#family_accommodation").val())+ Number($("#edu_expenditure").val())+ Number($("#water_bill").val())+ Number($("#electricity_bill").val()) + Number($("#telephone_bill").val()));
  }
});


</script>
@include('frontend.footer')