<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Fee Challan</title>	
  <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
  <link href="{{public_path('css/fee-challan-front.css')}}" rel="stylesheet" type="text/css" media="all">
</head>
<body>
  <div class="container" id="fee-challan-container" style="height: 600px; max-height: 600px !important;">
  	<div class="dues-challan">
  		@foreach ( $copies as $copy )
		    <div class="single-column">
		    	<img src="{{public_path('images/challan-header.jpg')}}" class="header-image" alt="IIUI">
		    	<div class="bank-logo-container">
					@if($userdetail->fkNationality == 2 || $userdetail->fkNationality == 3)
						<img src="{{public_path('images/Hbl.jpg')}}" alt="Habib Bank Limited">
					@else
						@if ( $challanDetail->applicant->applicant->fkGenderId == 2)
							<img src="{{public_path('images/AlliedBank.png')}}" alt="Allied Bank Limited">
						@else
							<img src="{{public_path('images/Hbl.jpg')}}" alt="Habib Bank Limited">
						@endif 
					@endif
			    	
		    	</div>
		    	<div class="account-number">
					<span class="bold "> Account#: 
						@if($userdetail->fkNationality == 2 || $userdetail->fkNationality == 3)
							@if($challanType == 'PKR')
								{{'50007904002803'}}
							@else
								{{'50067900873510'}}
							@endif
							
						@else
							{{ $bankDetails->accountNo ?? '' }} 
						@endif					
					</span>
		    	</div>
				<table class="detail-table <?= ($userdetail->fkNationality == 2 || $userdetail->fkNationality == 3)? 'overseas-cls': ''; ?>">
					<tbody>
						<?php
							if(($userdetail->fkNationality == 2 || $userdetail->fkNationality == 3) &&  $challanType != 'PKR'){
						?>
						<tr>
							<td class="bold fixed-width">Swift Code:</td>
							<td>
								HABBLBBEXXX
							</td>
						</tr>
						<tr>
							<td class="bold fixed-width">IBAN:</td>
							<td>
								PK39HABB0050067900873510
							</td>
						</tr>
						<?php
							}
						?>
						<tr>
							<td class="bold fixed-width">Challan#:</td>
							<td>
    							{{$challanDetail->id ?? ''}}
							</td>
                        </tr>
						<tr>
							<td class="bold">CNIC:</td>
							<td>
    							{{$challanDetail->applicant->applicant->cnic ?? ''}}
							</td>
                        </tr>
                        <tr>
							<td class="bold">Due Date:</td>
							<td class="underline bold">{{ ($challanDetail->applicant->semester->pkSemesterId == 11)? '21-Jun-2019' :date_format(date_create($challanDetail->applicant->program->programscheduler->lastDate), 'd-M-Y') }}</td>
                        </tr>
                        <tr>
							<td class="bold">Issued Date:</td>
							<td>{{ date_format(date_create($challanDetail->applicant->created_at), 'd-M-Y') }}</td>
						</tr>
            {{-- <tr>
							<td class="bold">Form#: </td>
							<td>{{ $challanDetail->applicant->id ?? ''}}</td>
						</tr> --}}
                        <tr> 
							<td class="bold">Semester:</td>
							<td>@if($challanDetail->applicant->semester->pkSemesterId == 14 && $challanDetail->applicant->program->pkProgId == 157)
									{{ 'Fall-2020' }}
								@else
									{{ $challanDetail->applicant->semester->title ?? ''}}
								@endif
							</td>
						</tr>
						<tr>
							<td class="bold">Name: </td>
							<td>{{ strtoupper($challanDetail->applicant->applicant->name) ?? ''}}</td>
						</tr>
						<tr>
							<td class="bold">Father Name: </td>
							<td>{{ $challanDetail->applicant->applicant->applicantDetail->fatherName ?? ''}}</td>
						</tr>
						<tr>
							<td class="bold">Faculty:</td>
							<td>{{ $challanDetail->applicant->program->faculty->title }}</td>
						</tr>
						<tr>
							<td class="bold">Department:</td>
							<td>{{  $challanDetail->applicant->program->department->title }}</td>
						</tr>
						@if($challanDetail->applicant->applicant->fkLevelId != 1)
						<tr>
							<td class="bold">Program: </td>
							<td>{{ $challanDetail->applicant->program->title }} </td>
						</tr>
						@endif
					</tbody>
				</table>
				<table class="fee-distribution-table">
					<thead>
						<tr>
							<th>Particulars</th>
							<th class="border-left">Amount</th>
						</tr>
					</thead>
					@php
						if($userdetail->fkNationality == 1){
							$processing_fee = $challanDetail->admission_processing_fee;
						}
						if($userdetail->fkNationality == 2 || $userdetail->fkNationality == 3){
							// $processing_fee = $challanDetail->overseas_admission_processing_fee;
							$processing_fee = '$75';
						}
						if(($userdetail->fkNationality == 2 || $userdetail->fkNationality == 3) && $challanType == 'PKR'){
							// $processing_fee = $challanDetail->overseas_admission_processing_fee;
							$processing_fee = '22000';
						}
					@endphp
					<tbody>
						<tr>
							<td>Admission Processing Fee</td>
							<td class="border-left right-align">{{  $processing_fee }}</td>
						</tr>
						<tr>
						    <td class="bold total-amount">Total Amount</td>
							<td class="border-left right-align">
                                <span class="bold">
                                        {{  $processing_fee }}
                                </span>
                            </td>
						</tr>
					</tbody>
                </table>
				<div class="bank-instructions">
					<h4 class="bank-instructions-title">Instructions</h5>
					<ol class="bank-instructions-list">
						@if($challanDetail->applicant->applicant->fkLevelId == 1)
							{{-- <li>Candidates applying for undergraduate degree programs can choose upto five programs against the payment of a single application processing fee</li> --}}
						@endif
                        <li>Application Form will not be entertained without Original Deposit Slip (Admission office Copy) with Bank stamp</li>                        
                        <li>This Fee is Non-Refundable / Non transferable in anycase</li>
						@if($userdetail->fkNationality == 2 || $userdetail->fkNationality == 3)
							<li>Only US dollar currency acceptable</li>
							<li>Remittance can be made through banking channel only.</li>
							<li>Credit card or ATM transfer may not be made in this account.</li>
						@endif
                        {{-- <li>Overwriting is Not allowed</li>
						<li>Please enter full fee challan# at the time of punching</li> --}}
						{{-- <li>Bank processing fee is not required</li> --}}
					</ol>
				</div>
				<div class="copy-owner">
    				<span class="copy">{{ $copy }}</span>
				</div>
			</div> <!-- Ending single-column --> 
		@endforeach 
	</div> <!-- Ending dues challan --> 
  </div> <!-- Ending container --> 
</body>
</html>