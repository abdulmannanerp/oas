@section('title')
Update Password
@endsection
@include('frontend.header')


    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          <i class="fa fa-key text-red"></i> Change Password
          <small>Set new password for your account</small>
        </h1>
      </section>
      @if ($errors->any())
      <div class="alert alert-danger">
          <ul>
              @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
              @endforeach
          </ul>
      </div>
      @endif
      @if ( $status )
      <div class="alert alert-success" role="alert"> {{ $status }}</div>
      @endif    

  
      <!-- Main content -->
      <section class="content">
          <form action="{{ route('updatepassword', $hash)}}" method="POST">
              <input type="hidden" name="userId" value="{{$userdetail->userId}}">
              {{csrf_field()}} 
  
        <!-- SELECT2 EXAMPLE -->
        <div class="box box-info">
          <div class="box-header with-border">
            <h3 class="box-title">Enter Details</h3>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                      <label class="req-rd">New Password</label>
                      <input type="password" id="password" name="password" class="form-control" required>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                      <label class="req-rd">Retype New Password</label>
                      <input type="password" id="repassword" name="password_confirmation" class="form-control" required>
                    </div>
                </div>
            </div>
          <!-- /.row -->
          </div>
          <!-- /.box-body -->
          <div class="box-footer">
              <button class="btn btn-social btn-success" id="update-password-btn" type="submit"><i class="fa fa-save"></i> Update Password</button>
           </div>
        </form>
        </div>
        <!-- /.box --> 
      </section>
      <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

@include('frontend.footer')