@section('title')
Application Progress
@endsection
@include('frontend.schheader')

@php
if($applicationStatus->step_save == 1 && $hash ==''){
  $readonlyUser = "readonly='readonly'";
  $disabledUser = "disabled";
  $disabledUser1 = 'disabled="disabled"';
}else{
  $readonlyUser='';   
  $disabledUser = '';
  $disabledUser1 = '';
}
@endphp

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          <i class="fa fa-user text-yellow"></i> Scholarship Application Progress Details
          <small>Scholarship status Info</small>
        </h1>
      </section>



      @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    @if ( $status )
        <div class="alert alert-success" role="alert"> {{ $status }}</div>
    @endif  
    
      <!-- Main content -->
      <section class="content">

        <div class="alert alert-success" role="alert">
           <ul>
               <li><b>Student can print scholarship form once Verification  (Fee Verification and Conduct verification) has been done. </b></li>
           </ul> 
        </div>

         <!-- <div class="form-group">
            <button class="btn btn-social btn-success" id="emp_his-btn" type="submit"><i class="fa fa-save"></i> Apply for Qardh-e-Hasnah Scholarship</button>
         </div> -->               
          <!-- /.form-group -->
        
          <input type="hidden" name="userId" value="{{$userdetail->userId}}">
          <input type="hidden" name="server_date" value="{{date('Y-m-d h:i:s')}}">
          {{csrf_field()}}

        <!-- SELECT2 EXAMPLE -->
        <div class="box box-info">
          <div class="box-header with-border box box-info">
            <h3 class="box-title">Other Details</h3>
          </div>

          <!-- /.box-header -->
          <div class="box-body">
            <div class="row">
              <div class="col-lg-12" style="overflow-x:auto;">
                  <table id="example" class="table">
                    <thead>
                        <tr>
                            <th>Scholarship Name</th>
                            <th>Apply Date</th>
                            <th>Fee Verification</th>
                            <!-- <th>Fee Verification Remarks</th> -->
                            <th>Conduct Verification</th>
                            <!-- <th>Conduct Verification Remarks</th> -->
                            <th>Document Approval Status</th>
                            <!-- <th>Document Verification Remarks</th> -->
                            <th>Print</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                      @foreach ($SchProgress as $sp)
                      <tr>
                        <td>{{$sp->Listscholarship->name}}</td>
                        <td>{{ date('d-m-Y', strtotime($sp->created_at))}}</td>
                        <td>

                          <?php if($sp->feeverification == '0'){
                            ?> 
                            <label class="label label-warning">
                                <?php echo "In Progress"; ?>
                            </label>
                            <?php
                                
                            }else if($sp->feeverification == '1'){
                              ?>
                            <label class="label label-info">
                                <?php echo "Approved"; ?>
                            </label>
                            <?php
                            }else {
                              ?>
                            <label class="label label-danger">
                            <?php echo "Rejected"; ?>
                            </label>
                            <?php
                            }
                        ?>
                        </td>
                       <!--  <td>{{$sp->feeremarks}}</td> -->
                        <td>
                          <?php if($sp->conductverfication == '0'){
                            ?>  
                            <label class="label label-warning">
                                <?php echo "In Progress"; ?>
                            </label>
                            <?php
                                
                            }else if($sp->conductverfication == '1'){
                              ?>
                            <label class="label label-info">
                                <?php echo "Approved"; ?>
                            </label>
                            <?php
                            }else {
                              ?>
                            <label class="label label-danger">
                            <?php echo "Rejected"; ?>
                            </label>
                            <?php
                            }
                        ?>
                      </td>
                        <!-- <td>{{$sp->condverremarks}}</td> -->
                        <td>
                          <?php if($sp->status == '0'){
                            ?>  
                            <label class="label label-warning">
                                <?php echo "In Progress"; ?>
                            </label>
                            <?php
                                
                            }else if($sp->status == '1'){
                              ?>
                            <label class="label label-info">
                                <?php echo "Approved"; ?>
                            </label>
                            <?php
                            }else {
                              ?>
                            <label class="label label-danger">
                            <?php echo "Rejected"; ?>
                            </label>
                            <?php
                            }
                        ?>
                      </td>
                      
                        <!-- <td>{{$sp->remarks}}</td> -->
                        
                        <td>
                          @if($sp->conductverfication == '1')
                          <a target="_blank" href="{{route('scholarshipprintform',['applicant_id' => auth()->id(), 'scholarship_id'=> $sp->fkSchlrId])}}" name="save" class="btn btn-social btn-primary">
                            <i class="fa fa-print"></i> Print Scholarship Form
                            </a>
                           @endif
                        </td>
                      </tr>
                      @endforeach
                    </tbody>
                </table>
            </div>
          </div>
        </div>
        <!-- /.box --> 
      </section>
      <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

@include('frontend.footer')