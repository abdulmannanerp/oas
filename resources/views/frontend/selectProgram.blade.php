@section('title')
Select Program
@endsection
@include('frontend.header')
@php
if($applicationStatus->step_save == 1 && $hash ==''){
  $readonlyUser = "readonly='readonly'";
  $disabledUser = "disabled";
  $disabledUser1 = 'disabled="disabled"';
}else{
  $readonlyUser='';
  $disabledUser = '';
  $disabledUser1 = '';
}
@endphp
@php
// Dev Mannan: Dear Future My self i am so sorry about the code written below. Donot touch :-(    
@endphp
@php
if($applicationStatus->step_save == 1){
  $delete = 'none';
}else{
  $delete = '';
}
@endphp

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          <i class="fa fa-sitemap text-yellow"></i> Select Program(s)
          <small>Add / Edit Program(s)</small>
        </h1>
      </section>
      @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    @if ( $status )
        <div class="alert alert-success" role="alert"> {{ $status }}</div>
    @endif 
    @if ( $custom_error )
      <div class="alert alert-danger alert-quali" role="alert"> {{ $custom_error }}</div>
    @endif  
      <!-- Main content -->
      <section class="content">
        @if($userdetail->fkLevelId == 1)
          <h4>A candidate shall be allowed to apply for three different Programs of the same faculty with a single processing fee. If the student is applying for more than three programs or choosing programs of different faculties he/she shall deposit the processing fee separately.</h4>
        @endif
          @if($application)
            <div class="box box-success marg-btm">
                <div class="box-header with-border">
                  <h3 class="box-title">Applied Programs</h3>
                </div>
              <div class="box-body">
                <div class="row">
                    <ul class="todo-list applied">
            @foreach ($application as $app)
            <li>
              <span class="text">{{$app->program->title. ' ('. $app->program->duration . ' years)'}}
                @if($app->program->programscheduler->fkGenderId != 1)
                  @if($app->program->programscheduler->fkGenderId != $userdetail->fkGenderId)
                    @if($app->program->programscheduler->fkGenderId == 3)
                      <span class="text-danger">{{'Program is for Male only'}}</span>
                    @endif
                    @if($app->program->programscheduler->fkGenderId == 2)
                    <span class="text-danger">{{'Program is for Female only'}}</span>
                    @endif
                  @endif
                @endif
              </span>
              {{-- Dev Mannan: Re-apply commented and enable delete functionality --}}
              @if($delete != 'none')
                
                  <a href="{{route('deleteApplication',["id" => $app->id])}}"><i class="fa fa-trash-o del-ic"></i></a> 

              @endif
              @php /* @endphp
                @if($newapplicant == 'can_apply' && $app->program->programscheduler->status == 1)
                  <a class="btn btn-primary skin-blue ff-r" href="{{route('reApply',["id" => $app->id])}}">Re-Apply</a>
                @endif                
              @php */ @endphp
              </li>
                
            @endforeach
              </ul>
            </div>
          </div>
        </div>
          @endif

        <form action="{{route('selectprogram', $hash)}}" method="POST">
          <input type="hidden" name="userId" value="{{$userdetail->userId}}">
          <input type="hidden" name="recentProgram" id="recentProgram" value="">
          {{csrf_field()}}
  
        <!-- SELECT2 EXAMPLE -->
        <div class="box box-info">
          <div class="box-header with-border">
            <h3 class="box-title">Avaiable Programs <span class="text-danger txt-sml">Only those programs are active in which you are eligible </span>  <img src={{ url('uploads/images/expand_all.png') }} class="expand_all" alt="Expand / Collapse"></h3>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <div class="row">
            @php
            $fac = '';
            // $dep = '';
            $lev = '';
            $i = 0;
            $fsc_marks = 0;
            $ssc_marks = 0;
            $bsc_marks = 0;
            $llb_marks = 0;
            $bs_marks = 0;
            $ms_marks = 0;
            $clear_both = '';
            if($previousQualification->resultAwaiting != 1){
              if($previousQualification->HSSC_Total != '' && $previousQualification->HSSC_Composite != ''){
                $fsc_marks = $previousQualification->HSSC_Composite/$previousQualification->HSSC_Total*100; 
              }
              if($previousQualification->HSSC_Total1 != '' && $previousQualification->HSSC1 != '' && $previousQualification->HSSC_Total2 != '' && $previousQualification->HSSC2 != ''){
                $fsc_marks = ($previousQualification->HSSC1+$previousQualification->HSSC2)/($previousQualification->HSSC_Total1+$previousQualification->HSSC_Total2)*100; 
              }
            }else{
              // if($previousQualification->HSSC_Total1 != '' && $previousQualification->HSSC1 != '' ){
              //   $fsc_marks = $previousQualification->HSSC1/$previousQualification->HSSC_Total1*100; 
              // }
              $fsc_marks = 'showall';
            }

            if($previousQualification->SSC_Total != '' && $previousQualification->SSC_Composite != ''){
              $ssc_marks = $previousQualification->SSC_Composite/$previousQualification->SSC_Total*100; 
            }
            @endphp
            @foreach($programmeScheduler as $programschedule)
            @php
            $male_female = '';
            // Show hide levels on the basis of user level selection
              if($userdetail->fkLevelId == 1 && $programschedule->program->level->pkLevelId > 1){
                continue;
              }
              if($userdetail->fkLevelId == 2 && $programschedule->program->level->pkLevelId > 2){
                continue;
              }
              if($userdetail->fkLevelId == 3 && $programschedule->program->level->pkLevelId > 3){
                continue;
              }
              if($userdetail->fkLevelId == 4 && $programschedule->program->level->pkLevelId > 4){
                continue;
              }
            @endphp
            @if($lev != $programschedule->program->level->programLevel)
              @if($i != 0) 
                </div>
              @endif
              <div class="box-header with-border prog-box" data-toggle="collapse" data-target="#demo_{{$i}}">
                  <p class="prog-level faculty-title"> <i class="fa fa-angle-double-right"></i> {{$programschedule->program->level->programLevel}} <i class='more-less fa {{($userdetail->fkLevelId == $programschedule->program->level->pkLevelId)? "fa-minus": "fa-plus"}} pull-right'></i></p>
              </div>
            <div id="demo_{{$i}}" class='cls-faculty collapse {{($userdetail->fkLevelId == $programschedule->program->level->pkLevelId)? "in": ""}}'>
            @endif
            @if($fac != $programschedule->program->faculty->title)
                <p class="prog-faculty">Faculty of {{$programschedule->program->faculty->title}} </p>
            @endif
            {{-- @if($dep != $programschedule->program->department->title)
                <p class="prog-dept">{{$programschedule->program->department->title}}</p>
            @endif --}}
            @php
            // gender check
                if($programschedule->fkGenderId == $userdetail->fkGenderId || $programschedule->fkGenderId == 1){
                  $disabled = '';
                }else{
                  $disabled = 'disabled="disabled"';
                  if($programschedule->fkGenderId == 3){
                    $male_female = '<span class="text-danger txt-sml-prog">Only Male</span>';
                  }
                  if($programschedule->fkGenderId == 2){
                    $male_female = '<span class="text-danger txt-sml-prog">Only Female</span>';
                  }
                }
                // bs mathematics check
                if($fsc_marks != 'showall' && $fsc_marks < 50 && $programschedule->fkProgramId == 128){
                  $disabled = 'disabled="disabled"';
                }
                // bs physics(50), engineering(189) check
                if($previousQualification->HSC_Subject == 'Pre-Medical' && ($programschedule->fkProgramId == 50 || $programschedule->fkProgramId == 189 )){
                  $disabled = 'disabled="disabled"';
                }
                // engineering check
                if($fsc_marks != 'showall' && ($fsc_marks < 60 || $ssc_marks < 60) && $programschedule->fkProgramId == 189){
                  $disabled = 'disabled="disabled"';
                }elseif($fsc_marks == 'showall' && $ssc_marks < 60 && $programschedule->fkProgramId == 189){ // needs to be changed
                  $disabled = 'disabled="disabled"';
                }
                // all BS Program check
                if(($programschedule->program->level->pkLevelId == 1 || $programschedule->program->level->pkLevelId == 2) && $fsc_marks != 'showall' && $fsc_marks < $programschedule->program->reqPercentage){
                    $disabled = 'disabled="disabled"';
                }
                // for BA/LLB (Hons) Shariah & Law (5 years) 
                // if($programschedule->fkProgramId == 153){
                //   if($previousQualification->llb_total_marks == '' || $previousQualification->llb_obtained_marks == '' || $previousQualification->law_completion_date == ''){
                //       $disabled = 'disabled="disabled"';
                //   }
                // }
                // for all Master (MA/MSc/LLB (3 years) MBA (3.5/2 years)) programs
                if($programschedule->program->level->pkLevelId == 3 || $programschedule->program->level->pkLevelId == 2 ){
                  if($previousQualification->BA_Bsc_Total != '' && $previousQualification->BA_Bsc != ''){
                    if($previousQualification->BA_Bsc_Total > 10 && $previousQualification->BA_Bsc > 10){
                      $bsc_marks = $previousQualification->BA_Bsc/$previousQualification->BA_Bsc_Total*100; 
                      if($bsc_marks < $programschedule->program->reqPercentage){
                        $disabled = 'disabled="disabled"';
                      }
                    }else{                      
                      if($previousQualification->BA_Bsc < $programschedule->program->reqCGPA){
                        $disabled = 'disabled="disabled"';
                      }
                    }                    
                  }
                }
                // for all  MS / LLM 2 years MBA 1.5 years programs
                if($programschedule->program->level->pkLevelId == 4 ){
                  if($previousQualification->BA_Bsc_Total != '' && $previousQualification->BA_Bsc != '' && $previousQualification->MSc_Total != '' && $previousQualification->MSc != ''){
                    if($previousQualification->MSc > 10 && $previousQualification->MSc_Total > 10){
                      $bs_marks = $previousQualification->MSc/$previousQualification->MSc_Total*100;
                      if($bs_marks < $programschedule->program->reqPercentage){
                        $disabled = 'disabled="disabled"';
                      }
                    }else{
                      if($previousQualification->MSc < $programschedule->program->reqCGPA){
                        $disabled = 'disabled="disabled"';
                      }
                    }                    
                  }else{
                    if($previousQualification->BS > 10 && $previousQualification->BS_Total > 10){
                      $bs_marks = $previousQualification->BS/$previousQualification->BS_Total*100;
                      if($bs_marks < $programschedule->program->reqPercentage){
                        $disabled = 'disabled="disabled"';
                      }
                    }else{
                      if($previousQualification->BS < $programschedule->program->reqCGPA){
                        $disabled = 'disabled="disabled"';
                      }
                    } 
                  }
                }
                // for all PhD 3 years programs
                if($programschedule->program->level->pkLevelId == 5 ){
                  if($previousQualification->MS_Total != '' && $previousQualification->MS != ''){
                    if($previousQualification->MS > 10 && $previousQualification->MS_Total > 10){
                      $ms_marks = $previousQualification->MS/$previousQualification->MS_Total*100;
                      if($ms_marks < $programschedule->program->reqPercentage){
                        $disabled = 'disabled="disabled"';
                      }
                    }else{
                      if($previousQualification->MS < $programschedule->program->reqCGPA){
                        $disabled = 'disabled="disabled"';
                      }
                    }                    
                  }
                }
            @endphp
              <div class="col-md-4 {{$clear_both}}">
                <div class="form-group">
                  <input type="checkbox" value="{{$programschedule->fkProgramId}}" {{($application->contains('fkProgramId', $programschedule->fkProgramId))? 'checked disabled':''}} name="program[]" class="minimal prog_check_box" {{$disabled}}>
                  <span class="prog-nm prg-name-{{$programschedule->fkProgramId}}">{{$programschedule->program->title}}<small>{{ ' ('. $programschedule->program->duration . ' years)'}}</small></span>
                  <i class="fa fa-info-circle prog-tool" id="tool_prog_{{$programschedule->fkProgramId}}" title="{{$programschedule->program->requirements}}"></i>
                  @php echo $male_female @endphp
                  {{-- @if($programschedule->fkProgramId == 189 && ($userdetail->fkGenderId == 3 || $userdetail->fkGenderId == 2 ))
                  @php $clear_both = 'clear-both'; @endphp
                  <p class="marg-btm"><b>Enter SSC/O-level Marks</b></p> <hr class="mr-bt-tp" />
                  <div class="row">
                    <div class="col-md-12 marks-div">
                      <div class="col-md-3 mr-tp-5">
                        <label>Chemistry</label>
                      </div>
                      <div class="col-md-4">
                        <input class="form-control" type="number" onblur="checkMarks('ssc_chemistry_total','ssc_chemistry_obtained','')" placeholder="Total Marks" id="ssc_chemistry_total" step="0" min="0" value="{{($previousQualification->ssc_chemistry_total)? $previousQualification->ssc_chemistry_total  : ''}}" name="ssc_chemistry_total" {{($previousQualification->ssc_chemistry_total)? $disabledUser : ''}}>
                      </div>
                      <div class="col-md-5">
                        <input class="form-control" type="number" onblur="checkMarks('ssc_chemistry_total','ssc_chemistry_obtained','')" placeholder="Obtained Marks" id="ssc_chemistry_obtained" step="0" min="0" value="{{($previousQualification->ssc_chemistry_obtained)? $previousQualification->ssc_chemistry_obtained  : ''}}" name="ssc_chemistry_obtained" {{($previousQualification->ssc_chemistry_obtained)? $disabledUser : ''}}>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-12 marks-div">
                      <div class="col-md-3 mr-tp-5">
                        <label>Physics</label>
                      </div>
                      <div class="col-md-4">
                        <input class="form-control" type="number" onblur="checkMarks('ssc_physics_total','ssc_physics_obtained','')" placeholder="Total Marks" id="ssc_physics_total" step="0" min="0" value="{{($previousQualification->ssc_physics_total)? $previousQualification->ssc_physics_total  : ''}}" name="ssc_physics_total" {{($previousQualification->ssc_physics_total)? $disabledUser : ''}}>
                      </div>
                      <div class="col-md-5">
                        <input class="form-control" type="number" onblur="checkMarks('ssc_physics_total','ssc_physics_obtained','')" placeholder="Obtained Marks" id="ssc_physics_obtained" step="0" min="0" value="{{($previousQualification->ssc_physics_obtained)? $previousQualification->ssc_physics_obtained  : ''}}" name="ssc_physics_obtained" {{($previousQualification->ssc_physics_obtained)? $disabledUser : ''}}>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-12 marks-div">
                      <div class="col-md-3 mr-tp-5">
                        <label>Maths</label>
                      </div>
                      <div class="col-md-4">
                        <input class="form-control" type="number" onblur="checkMarks('ssc_maths_total','ssc_maths_obtained','')" placeholder="Total Marks" id="ssc_maths_total" step="0" min="0" value="{{($previousQualification->ssc_maths_total)? $previousQualification->ssc_maths_total  : ''}}" name="ssc_maths_total" {{($previousQualification->ssc_maths_total)? $disabledUser : ''}}>
                      </div>
                      <div class="col-md-5">
                        <input class="form-control" type="number" onblur="checkMarks('ssc_maths_total','ssc_maths_obtained','')" placeholder="Obtained Marks" id="ssc_maths_obtained" step="0" min="0" value="{{($previousQualification->ssc_maths_obtained)? $previousQualification->ssc_maths_obtained  : ''}}" name="ssc_maths_obtained" {{($previousQualification->ssc_maths_obtained)? $disabledUser : ''}}>
                      </div>
                    </div>
                  </div>
                  @endif --}}
                  @if($programschedule->fkProgramId == 189 && $userdetail->fkGenderId == 3)
                  <p class="marg-btm"><b>Preferences</b></p> <hr class="mr-bt-tp" />
                  <label class="radio-inline">BS Mechanical Engineering</label>
                  <br />
                  <label class="radio-inline">
                    <input type="radio"  name="first_radio" value="first" {{ ($application->contains('preferrence1', 132)) ? 'checked' : ''}} {{($application->contains('preferrence1', 132) || $application->contains('preferrence1', 131))? $disabledUser : ''}}>1st
                  </label>
                  <label class="radio-inline">
                    <input type="radio" name="first_radio" value="second" {{ ($application->contains('preferrence2', 132)) ? 'checked' : ''}} {{($application->contains('preferrence1', 132) || $application->contains('preferrence1', 131))? $disabledUser : ''}}>2nd
                  </label>
                  <label class="radio-inline">
                    <input type="radio" name="first_radio" value="third" {{ ($application->contains('preferrence3', 132)) ? 'checked' : ''}} {{($application->contains('preferrence1', 132) || $application->contains('preferrence1', 131) || $application->contains('preferrence1', 157))? $disabledUser : ''}}>3rd
                  </label>

                  <br />
                  <label class="radio-inline">BS Electrical Engineering</label>
                  <br />
                  <label class="radio-inline">
                    <input type="radio" name="second_radio" value="first" {{ ($application->contains('preferrence1', 131)) ? 'checked' : ''}} {{($application->contains('preferrence1', 132) || $application->contains('preferrence1', 131))? $disabledUser : ''}}>1st
                  </label>
                  <label class="radio-inline">
                    <input type="radio" name="second_radio" value="second" {{ ($application->contains('preferrence2', 131)) ? 'checked' : ''}} {{($application->contains('preferrence1', 132) || $application->contains('preferrence1', 131))? $disabledUser : ''}}>2nd
                  </label>
                  <label class="radio-inline">
                    <input type="radio" name="second_radio" value="third" {{ ($application->contains('preferrence3', 131)) ? 'checked' : ''}} {{($application->contains('preferrence1', 132) || $application->contains('preferrence1', 131) || $application->contains('preferrence1', 157))? $disabledUser : ''}}>3rd
                  </label>

                  <br />
                  <label class="radio-inline">BS Civil Engineering</label>
                  <br />
                  <label class="radio-inline">
                    <input type="radio" name="third_radio" value="first" {{ ($application->contains('preferrence1', 157)) ? 'checked' : ''}} {{($application->contains('preferrence1', 132) || $application->contains('preferrence1', 131) || $application->contains('preferrence1', 157))? $disabledUser : ''}}>1st
                  </label>
                  <label class="radio-inline">
                    <input type="radio" name="third_radio" value="second" {{ ($application->contains('preferrence2', 157)) ? 'checked' : ''}} {{($application->contains('preferrence1', 132) || $application->contains('preferrence1', 131) || $application->contains('preferrence1', 157))? $disabledUser : ''}}>2nd
                  </label>
                  <label class="radio-inline">
                    <input type="radio" name="third_radio" value="third" {{ ($application->contains('preferrence3', 157)) ? 'checked' : ''}} {{($application->contains('preferrence1', 132) || $application->contains('preferrence1', 131) || $application->contains('preferrence1', 157))? $disabledUser : ''}}>3rd
                  </label>

                  {{-- <div class="inline-pre">
                    <label>First Preference</label>
                    <select class="form-control" id="first_prefer" name="first_prefer" {{($application->contains('preferrence1', 132) || $application->contains('preferrence1', 131) || $application->contains('preferrence1', 157))? $disabledUser : ''}}>
                      <option value="">Select</option>
                      <option value="132" {{ ($application->contains('preferrence1', 132)) ? 'selected' : ''}}>BS Mechanical Engineering </option>
                      <option value="131" {{ ($application->contains('preferrence1', 131)) ? 'selected' : ''}}>BS Electrical Engineering</option>
                      <option value="157" {{ ($application->contains('preferrence1', 157)) ? 'selected' : ''}}>BS Civil Engineering</option>
                    </select>
                  </div>
                  <div class="inline-pre">
                    <label>Second Preference</label>
                    <select class="form-control" id="second_prefer" name="second_prefer" {{($application->contains('preferrence1', 132) || $application->contains('preferrence1', 131) || $application->contains('preferrence1', 157))? $disabledUser : ''}}>
                      <option value="">Select</option>
                      <option value="132" {{ ($application->contains('preferrence2', 132)) ? 'selected' : ''}}>BS Mechanical Engineering </option>
                      <option value="131" {{ ($application->contains('preferrence2', 131)) ? 'selected' : ''}}>BS Electrical Engineering</option>
                      <option value="157" {{ ($application->contains('preferrence2', 157)) ? 'selected' : ''}}>BS Civil Engineering</option>
                    </select>
                  </div>
                  <div class="inline-pre">
                    <label>Third Preference</label>
                    <select class="form-control" id="third_prefer" name="third_prefer" {{($application->contains('preferrence1', 132) || $application->contains('preferrence1', 131) || $application->contains('preferrence1', 157))? $disabledUser : ''}}>
                      <option value="">Select</option>
                      <option value="132" {{ ($application->contains('preferrence3', 132)) ? 'selected' : ''}}>BS Mechanical Engineering </option>
                      <option value="131" {{ ($application->contains('preferrence3', 131)) ? 'selected' : ''}}>BS Electrical Engineering</option>
                      <option value="157" {{ ($application->contains('preferrence3', 157)) ? 'selected' : ''}}>BS Civil Engineering</option>
                    </select>
                  </div> --}}
                  @endif
                  {{-- {{dump($programschedule->fkProgramId)}} --}}
                  @if($programschedule->fkProgramId == 174)
                  <div class="inline-pre">
                    <label>Experience in Years</label>
                    <input class="form-control" type="number" id="mba_experience" step=".01" value="{{($previousQualification->mba_experience)? $previousQualification->mba_experience  : ''}}" name="mba_experience" {{($previousQualification->mba_experience)? $disabledUser : ''}}>
                  </div>
                  @endif
                  {{-- Dev Mannan: Special check for BS Civil Engineering Program get marks from user --}}
                  {{-- @if($programschedule->fkProgramId == 157)
                  <div class="row">
                    <div class="col-md-12 marks-div">
                      <div class="col-md-3 mr-tp-5">
                        <label>Chemistry</label>
                      </div>
                      <div class="col-md-4">
                        <input class="form-control" type="number" onblur="checkMarks('chemistry_total','chemistry_obtained','')" placeholder="Total Marks" id="chemistry_total" step="0" min="0" value="{{($previousQualification->chemistry_total)? $previousQualification->chemistry_total  : ''}}" name="chemistry_total" {{($previousQualification->chemistry_total)? $disabledUser : ''}}>
                      </div>
                      <div class="col-md-5">
                        <input class="form-control" type="number" onblur="checkMarks('chemistry_total','chemistry_obtained','')" placeholder="Obtained Marks" id="chemistry_obtained" step="0" min="0" value="{{($previousQualification->chemistry_obtained)? $previousQualification->chemistry_obtained  : ''}}" name="chemistry_obtained" {{($previousQualification->chemistry_obtained)? $disabledUser : ''}}>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-12 marks-div">
                      <div class="col-md-3 mr-tp-5">
                        <label>Physics</label>
                      </div>
                      <div class="col-md-4">
                        <input class="form-control" type="number" onblur="checkMarks('physics_total','physics_obtained','')" placeholder="Total Marks" id="physics_total" step="0" min="0" value="{{($previousQualification->physics_total)? $previousQualification->physics_total  : ''}}" name="physics_total" {{($previousQualification->physics_total)? $disabledUser : ''}}>
                      </div>
                      <div class="col-md-5">
                        <input class="form-control" type="number" onblur="checkMarks('physics_total','physics_obtained','')" placeholder="Obtained Marks" id="physics_obtained" step="0" min="0" value="{{($previousQualification->physics_obtained)? $previousQualification->physics_obtained  : ''}}" name="physics_obtained" {{($previousQualification->physics_obtained)? $disabledUser : ''}}>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-12 marks-div">
                      <div class="col-md-3 mr-tp-5">
                        <label>Maths</label>
                      </div>
                      <div class="col-md-4">
                        <input class="form-control" type="number" onblur="checkMarks('maths_total','maths_obtained','')" placeholder="Total Marks" id="maths_total" step="0" min="0" value="{{($previousQualification->maths_total)? $previousQualification->maths_total  : ''}}" name="maths_total" {{($previousQualification->maths_total)? $disabledUser : ''}}>
                      </div>
                      <div class="col-md-5">
                        <input class="form-control" type="number" onblur="checkMarks('maths_total','maths_obtained','')" placeholder="Obtained Marks" id="maths_obtained" step="0" min="0" value="{{($previousQualification->maths_obtained)? $previousQualification->maths_obtained  : ''}}" name="maths_obtained" {{($previousQualification->maths_obtained)? $disabledUser : ''}}>
                      </div>
                    </div>
                  </div>
                  @endif --}}


                  @if($programschedule->fkProgramId == 65 || $programschedule->fkProgramId == 66)
                  <?php
                  if($programschedule->fkProgramId == 66){
                   $ms_phd_specialization = 'ms_usuludin_specialization'; 
                  }
                  if($programschedule->fkProgramId == 65){
                   $ms_phd_specialization = 'phd_usuludin_specialization'; 
                  }                  
                  ?>
                  <div class="inline-pre">
                    <label>Specialization</label>
                    <select class="form-control" id="{{$ms_phd_specialization}}" name="{{$ms_phd_specialization}}" {{($previousQualification->$ms_phd_specialization)? $disabledUser : ''}}>
                      <option value="">Select</option>
                      {{-- <option value="Tafseer & Quranic Studies" {{ ($previousQualification->$ms_phd_specialization == 'Tafseer & Quranic Studies') ? 'selected' : ''}}>Tafseer & Quranic Studies</option> --}}
                      {{-- <option value="Hadith & Its Sciences" {{ ($previousQualification->$ms_phd_specialization == 'Hadith & Its Sciences') ? 'selected' : ''}}>Hadith & Its Sciences</option> --}}
                      <?php
                      if($programschedule->fkProgramId == 66){
                      ?>
                        <option value="Comparative Religions" {{ ($previousQualification->$ms_phd_specialization == 'Comparative Religions') ? 'selected' : ''}}>Comparative Religions</option>
                        <option value="Aqidah & Philosophy" {{ ($previousQualification->$ms_phd_specialization == 'Aqidah & Philosophy') ? 'selected' : ''}}>Aqidah & Philosophy</option>
                        <option value="Dawah & Islamic Culture" {{ ($previousQualification->$ms_phd_specialization == 'Dawah & Islamic Culture') ? 'selected' : ''}}>Dawah & Islamic Culture</option>
                        <option value="Hadith & Its Sciences" {{ ($previousQualification->$ms_phd_specialization == 'Hadith & Its Sciences') ? 'selected' : ''}}>Hadith & Its Sciences</option>
                        <option value="Seerah & Islamic History" {{ ($previousQualification->$ms_phd_specialization == 'Seerah & Islamic History') ? 'selected' : ''}}>Seerah & Islamic History</option>
                        <option value="Tafseer & Quranic Studies" {{ ($previousQualification->$ms_phd_specialization == 'Tafseer & Quranic Studies') ? 'selected' : ''}}>Tafseer & Quranic Studies</option>
                      <?php 
                      }
                      if($programschedule->fkProgramId == 65){
                      ?>
                        <option value="Comparative Religions" {{ ($previousQualification->$ms_phd_specialization == 'Comparative Religions') ? 'selected' : ''}}>Comparative Religions</option>
                        <option value="Aqidah & Philosophy" {{ ($previousQualification->$ms_phd_specialization == 'Aqidah & Philosophy') ? 'selected' : ''}}>Aqidah & Philosophy</option>
                        <option value="Hadith & Its Sciences" {{ ($previousQualification->$ms_phd_specialization == 'Hadith & Its Sciences') ? 'selected' : ''}}>Hadith & Its Sciences</option>
                        <option value="Seerah & Islamic History" {{ ($previousQualification->$ms_phd_specialization == 'Seerah & Islamic History') ? 'selected' : ''}}>Seerah & Islamic History</option>
                      <?php 
                      }
                      ?>

                      {{-- <option value="Comparative Religions" {{ ($previousQualification->$ms_phd_specialization == 'Comparative Religions') ? 'selected' : ''}}>Comparative Religions</option>
                      <option value="Aqidah & Philosophy" {{ ($previousQualification->$ms_phd_specialization == 'Aqidah & Philosophy') ? 'selected' : ''}}>Aqidah & Philosophy</option>
                      <option value="Dawah & Islamic Culture" {{ ($previousQualification->$ms_phd_specialization == 'Dawah & Islamic Culture') ? 'selected' : ''}}>Dawah & Islamic Culture</option> --}}
                      {{-- <option value="Seerah" {{ ($previousQualification->$ms_phd_specialization == 'Seerah') ? 'selected' : ''}}>Seerah</option>
                      <option value="Islamic History" {{ ($previousQualification->$ms_phd_specialization == 'Islamic History') ? 'selected' : ''}}>Islamic History</option> --}}
                    </select>
                  </div>
                  @endif
                </div>
              </div>
            @php
            $lev = $programschedule->program->level->programLevel;
            $fac = $programschedule->program->faculty->title;
            // $dep = $programschedule->program->department->title;
            $i++;
            @endphp
              @endforeach
              <!-- /.col -->
            </div>
           <!-- /.row -->
          </div>
          <!-- /.box-body -->
          <div class="modal fade" id="myToolModal" role="dialog">
              <div class="modal-dialog modal-sm">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"></h4>
                  </div>
                  <div class="modal-body">
                    
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-primary" value="" data-dismiss="modal" id="i_agree">Agree</button>
                    <button type="button" class="btn btn-secondary" value="" data-dismiss="modal">Disagree</button>
                  </div>
                </div>
              </div>
            </div>
          <div class="box-footer">

            <?php
              // if($applicationStatus->step_save != 1){
              //   echo '<button class="btn btn-social btn-success" id="select-program-btn" type="submit"><i class="fa fa-save"></i> Save and Move Next</button>';
              // }
            ?>
            <button class="btn btn-social btn-success" id="select-program-btn" type="submit"><i class="fa fa-save"></i> Save and Move Next</button>
              
          </div>
        </form>
        </div>
        <!-- /.box --> 
      </section>
      <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

@include('frontend.footer')