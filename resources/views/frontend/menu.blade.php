<!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
		<!-- sidebar: style can be found in sidebar.less -->
		<section class="sidebar">
		  <!-- sidebar menu: : style can be found in sidebar.less -->
		  <ul class="sidebar-menu" data-widget="tree">
			<li class="{{ (Request::route()->getName() == 'personaldetail') ? 'active-li-menu' : '' }}">
				<a href="{{ route('personaldetail', $hash)}}" class="{{ (Request::route()->getName() == 'personaldetail') ? 'active' : '' }}">
					<i class="fa fa-user text-yellow"></i> <span>Personal Info</span>
					<span class="pull-right-container">
							</small><small class="label pull-right bg-green">{{($applicationStatus->step_personal == 1)? '&#x2714;': ''}}</small>
					
					</span>
				</a>
			</li>
			{{-- <li class="{{ (Request::route()->getName() == 'addressdetail') ? 'active-li-menu' : '' }}">
				<a href="{{ route('addressdetail', $hash)}}" class="{{ (Request::route()->getName() == 'addressdetail') ? 'active' : '' }}">
					<i class="fa fa-address-book-o text-aqua"></i> <span>Address</span>
					<span class="pull-right-container">
					<small class="label pull-right bg-green">{{($applicationStatus->step_address == 1)? '&#x2714;': ''}}</small>
					</span>
				</a>
			</li> --}}
			<li class="{{ (Request::route()->getName() == 'previousqualification')  ? 'active-li-menu' : '' }}">
				<a href="{{ route('previousqualification', $hash)}}" class="{{ (Request::route()->getName() == 'previousqualification')  ? 'active' : '' }}">
					<i class="fa fa-university text-red"></i> <span>Previous Qualification</span>
					<span class="pull-right-container">
					<small class="label pull-right bg-green">{{($applicationStatus->step_education == 1)? '&#x2714;': ''}}</small>
					</span>
				</a>
			</li>			
			<li class="{{ (Request::route()->getName() == 'selectprogram') ? 'active-li-menu' : '' }}">
				<a href="{{ route('selectprogram', $hash)}}" class="{{ (Request::route()->getName() == 'selectprogram') ? 'active' : '' }}">
					<i class="fa fa-sitemap text-yellow"></i> <span>Select Program</span>
					<span class="pull-right-container">
					<small class="label pull-right bg-green">{{($applicationStatus->step_programs == 1)? '&#x2714;': ''}}</small>
					</span>
				</a>
			</li>
			@if($applicationStatus->applicant->fkLevelId == 1)
				<li class="{{ (Request::route()->getName() == 'selectpreference') ? 'active-li-menu' : '' }}">
					<a href="{{ route('selectpreference', $hash)}}" class="{{ (Request::route()->getName() == 'selectpreference') ? 'active' : '' }}">
						<i class="fa fa-info text-yellow"></i> <span>Select Preference</span>
						<span class="pull-right-container">
						<small class="label pull-right bg-green">{{($applicationStatus->step_preference == 1)? '&#x2714;': ''}}</small>
						</span>
					</a>
				</li>
			@endif
			{{-- <li class="{{ (Request::route()->getName() == 'languageproficency') ? 'active-li-menu' : '' }}">
				<a href="{{ route('languageproficency', $hash)}}" class="{{ (Request::route()->getName() == 'languageproficency') ? 'active' : '' }}">
					<i class="fa fa-globe text-aqua"></i> <span>Language Proficiency</span>
					<span class="pull-right-container">
					<small class="label pull-right bg-green">{{($applicationStatus->step_language == 1)? '&#x2714;': ''}}</small>
					</span>
				</a>
			</li>
			<li class="{{ (Request::route()->getName() == 'aptitudetest') ? 'active-li-menu' : '' }}">
				<a href="{{ route('aptitudetest', $hash)}}" class="{{ (Request::route()->getName() == 'aptitudetest') ? 'active' : '' }}">
					<i class="fa fa-file-text-o text-red"></i> <span>Aptitude Test</span>
					<span class="pull-right-container">
					<small class="label pull-right bg-green">{{($applicationStatus->step_aptitude == 1)? '&#x2714;': ''}}</small>
					</span>
				</a>
			</li>  --}}
			{{-- <li class="{{ (Request::route()->getName() == 'familydetail') ? 'active-li-menu' : '' }}">
				<a href="{{ route('familydetail', $hash)}}" class="{{ (Request::route()->getName() == 'familydetail') ? 'active' : '' }}">
					<i class="fa fa-user-plus text-yellow"></i> <span>Family/Income Details</span>
					<span class="pull-right-container">
					<small class="label pull-right bg-green">{{($applicationStatus->step_family == 1)? '&#x2714;': ''}}</small>
					</span>
				</a>
			</li>	  --}}
			<li class="{{ (Request::route()->getName() == 'otherdetail') ? 'active-li-menu' : '' }}">
				<a href="{{ route('otherdetail', $hash)}}" class="{{ (Request::route()->getName() == 'otherdetail') ? 'active' : '' }}">
					<i class="fa fa-question-circle text-aqua"></i> <span>Family/Other Details</span>
					<span class="pull-right-container">
					<small class="label pull-right bg-green">{{($applicationStatus->step_other == 1)? '&#x2714;': ''}}</small>
					</span>
				</a>
			</li>
			<li class="{{ (Request::route()->getName() == 'savedetail') ? 'active-li-menu' : '' }}">
				<a href="{{ route('savedetail', $hash)}}" class="{{ (Request::route()->getName() == 'savedetail') ? 'active' : '' }}">
					<i class="fa fa-hdd-o text-yellow"></i> <span>Save</span>
					<span class="pull-right-container">
					<small class="label pull-right bg-green">{{($applicationStatus->step_save == 1)? '&#x2714;': ''}}</small>
					</span>
				</a>
			</li>
			@if($applicationStatus->step_save == 1)
			<li class="{{ (Request::route()->getName() == 'application') ? 'active-li-menu' : '' }}">
				<a href="{{ route('application', $hash)}}" class="{{ (Request::route()->getName() == 'application') ? 'active' : '' }}">
					<i class="fa fa-edit text-red"></i> <span>Application</span>
					<span class="pull-right-container">
					@if(!$applicationRollNumber->isEmpty())
						{{-- <small class="label pull-right"><span class="blink_me clr-wh"> Roll No Slip </span></small> --}}
					@endif	
					</span>
				</a>
			</li>
			@endif

			<li class="header imp-lnks">Important Links</li>
			<li><a target="_blank" href="{{ url('programes')}}"><i class="fa fa-sitemap text-red"></i> <span>Programs Offered</span></a></li>
			<li><a target="_blank" href="{{ url('fee-structure')}}"><i class="fa fa-money text-yellow"></i> <span>Fee Structure</span></a></li>
			<li><a target="_blank" href="https://www.iiu.edu.pk/?page_id=1121"><i class="fa fa-graduation-cap text-aqua"></i> <span>ScholarShips</span></a></li>
			<li><a target="_blank" href="{{ url('frontend/contactus')}}"><i class="fa fa-phone-square text-red"></i> <span>Contact Us</span></a></li>
			<li><a href="#" data-toggle="modal" data-target="#admissionReq"><i class="fa fa-info text-yellow"></i> <span>Important Instructions</span></a></li>
			<li><a href="{{ route('affidavit-specimen') }}"><i class="fa fa-info text-yellow"></i> <span>Undertaking Specimen</span></a></li>
		  </ul>
		</section>
		<!-- /.sidebar -->
	  </aside>