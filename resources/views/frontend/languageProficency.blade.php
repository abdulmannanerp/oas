@section('title')
Language Proficiency
@endsection
@include('frontend.header')
@php
if($applicationStatus->step_save == 1 && $hash ==''){
  $readonlyUser = "readonly='readonly'";
  $disabledUser = "disabled";
  $disabledUser1 = 'disabled="disabled"';
}else{
  $readonlyUser='';
  $disabledUser = '';
  $disabledUser1 = '';
}
@endphp
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          <i class="fa fa-globe text-aqua"></i> language Proficency
          <small>Add / Edit language Proficency</small>
        </h1>
      </section>
      @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    @if ( $status )
        <div class="alert alert-success" role="alert"> {{ $status }}</div>
    @endif  
  
      <!-- Main content -->
      <section class="content">
        <form action="{{route('languageproficency', $hash)}}" method="POST">
          <input type="hidden" name="userId" value="{{$userdetail->userId}}">
          {{csrf_field()}}
  
        <!-- SELECT2 EXAMPLE -->
        <div class="box box-info">
          <div class="box-header with-border">
            <h3 class="box-title">Language Details</h3>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <div class="row">
              <div class="col-md-4">
                <div class="form-group">
                  <label>Language</label><br />
                  <label>Arabic</label>
                </div>
                <!-- /.form-group -->
              </div>
              <!-- /.col -->
              <div class="col-md-4">
                <div class="form-group">
                  <label>Written</label>
                  <select class="form-control select2" id="arabic_written" name="arabic_written" style="width: 100%;" {{$disabledUser}}>
                    <option value="">Select</option>
                    <option value="1" {{ ($applicantDetail->arabicWritten == 1) ? 'selected' : ''}}>Fair</option>
                    <option value="2" {{ ($applicantDetail->arabicWritten == 2) ? 'selected' : ''}}>Good</option>
                    <option value="3" {{ ($applicantDetail->arabicWritten == 3) ? 'selected' : ''}}>Excellent</option>
                    </select>
                </div>
                <!-- /.form-group -->
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label>Spoken</label>
                  <select class="form-control select2" id="arabic_spoken" name="arabic_spoken" style="width: 100%;" {{$disabledUser}}>
                    <option value="">Select</option>
                    <option value="1" {{ ($applicantDetail->arabicSpoken == 1) ? 'selected' : ''}}>Fair</option>
                    <option value="2" {{ ($applicantDetail->arabicSpoken == 2) ? 'selected' : ''}}>Good</option>
                    <option value="3" {{ ($applicantDetail->arabicSpoken == 3) ? 'selected' : ''}}>Excellent</option>
                    </select>
                </div>
              </div>              
              <!-- /.col -->
            </div>
            <div class="row">
              <div class="col-md-4">
                <div class="form-group">
                  <label>Language</label><br />
                  <label>English</label>
                </div>
                <!-- /.form-group -->
              </div>
              <!-- /.col -->
              <div class="col-md-4">
                <div class="form-group">
                  <label>Written</label>
                  <select class="form-control select2" id="english_written" name="english_written" style="width: 100%;" {{$disabledUser}}>
                    <option value="">Select</option>
                    <option value="1" {{ ($applicantDetail->englishWritten == 1) ? 'selected' : ''}}>Fair</option>
                    <option value="2" {{ ($applicantDetail->englishWritten == 2) ? 'selected' : ''}}>Good</option>
                    <option value="3" {{ ($applicantDetail->englishWritten == 3) ? 'selected' : ''}}>Excellent</option>
                    </select>
                </div>
                <!-- /.form-group -->
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label>Spoken</label>
                  <select class="form-control select2" id="english_spoken" name="english_spoken" style="width: 100%;" {{$disabledUser}}>
                    <option value="">Select</option>
                    <option value="1" {{ ($applicantDetail->englishSpoken == 1) ? 'selected' : ''}}>Fair</option>
                    <option value="2" {{ ($applicantDetail->englishSpoken == 2) ? 'selected' : ''}}>Good</option>
                    <option value="3" {{ ($applicantDetail->englishSpoken == 3) ? 'selected' : ''}}>Excellent</option>
                    </select>
                </div>
              </div>              
              <!-- /.col -->
            </div>
            {{-- <div class="row">
              <div class="col-md-4">
                <div class="form-group">
                  <label>Language</label>
                  <input type="text" id="l3" name="l3" value="{{$applicantDetail->language3}}" class="form-control" {{$disabledUser}}>
                </div>
                <!-- /.form-group -->
              </div>
              <!-- /.col -->
              <div class="col-md-4">
                <div class="form-group">
                  <label>Written</label>
                  <select class="form-control select2" id="l3_written" name="l3_written" style="width: 100%;" {{$disabledUser}}>
                    <option value="">Select</option>
                    <option value="1" {{ ($applicantDetail->language3Written == 1) ? 'selected' : ''}}>Fair</option>
                    <option value="2" {{ ($applicantDetail->language3Written == 2) ? 'selected' : ''}}>Good</option>
                    <option value="3" {{ ($applicantDetail->language3Written == 3) ? 'selected' : ''}}>Excellent</option>
                    </select>
                </div>
                <!-- /.form-group -->
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label>Spoken</label>
                  <select class="form-control select2" id="l3_spoken" name="l3_spoken" style="width: 100%;" {{$disabledUser}}>
                    <option value="">Select</option>
                    <option value="1" {{ ($applicantDetail->language3Spoken == 1) ? 'selected' : ''}}>Fair</option>
                    <option value="2" {{ ($applicantDetail->language3Spoken == 2) ? 'selected' : ''}}>Good</option>
                    <option value="3" {{ ($applicantDetail->language3Spoken == 3) ? 'selected' : ''}}>Excellent</option>
                    </select>
                </div>
              </div>              
              <!-- /.col -->
            </div>
            <div class="row">
              <div class="col-md-4">
                <div class="form-group">
                  <label>Language</label>
                  <input type="text" id="l4" name="l4" value="{{$applicantDetail->language4}}" class="form-control" {{$disabledUser}}>
                </div>
                <!-- /.form-group -->
              </div>
              <!-- /.col -->
              <div class="col-md-4">
                <div class="form-group">
                  <label>Written</label>
                  <select class="form-control select2" id="l4_written" name="l4_written" style="width: 100%;" {{$disabledUser}}>
                    <option value="">Select</option>
                    <option value="1" {{ ($applicantDetail->language4Written == 1) ? 'selected' : ''}}>Fair</option>
                    <option value="2" {{ ($applicantDetail->language4Written == 2) ? 'selected' : ''}}>Good</option>
                    <option value="3" {{ ($applicantDetail->language4Written == 3) ? 'selected' : ''}}>Excellent</option>
                    </select>
                </div>
                <!-- /.form-group -->
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label>Spoken</label>
                  <select class="form-control select2" id="l4_spoken" name="l4_spoken" style="width: 100%;" {{$disabledUser}}>
                    <option value="">Select</option>
                    <option value="1" {{ ($applicantDetail->language4Spoken == 1) ? 'selected' : ''}}>Fair</option>
                    <option value="2" {{ ($applicantDetail->language4Spoken == 2) ? 'selected' : ''}}>Good</option>
                    <option value="3" {{ ($applicantDetail->language4Spoken == 3) ? 'selected' : ''}}>Excellent</option>
                    </select>
                </div>
              </div>              
              <!-- /.col -->
            </div> --}}
           <!-- /.row -->
          </div>
          <!-- /.box-body -->

          <div class="box-footer">
            <button class="btn btn-social btn-success" {{$disabledUser}} id="language-proficiency-btn" type="submit"><i class="fa fa-save"></i> Save and Move Next</button>
            <a class="btn btn-social btn-dropbox" href="{{ route('aptitudetest', $hash)}}">
                <i class="fa fa-fast-forward"></i> Skip this step
            </a>
          </div>
        </form>
        </div>
        <!-- /.box --> 
      </section>
      <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

@include('frontend.footer')