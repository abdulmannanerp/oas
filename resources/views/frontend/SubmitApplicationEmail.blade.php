<!DOCTYPE html>
<html lang="en-US">	
    <head>
        <meta charset="utf-8">	
    </head>	
    <body>
        Dear <strong>{{$name}}</strong>, <br /><br />
        Your Application(s) has been saved successfully<br /><br />
        <p><b>Note:</b></p>
        <ul>
          <li>Please enter the details of submitted challan form to complete the application submission process.</li>
          <li>You will get the Roll No. Slip in your online account once the document and fee has been verified.</li>
          <?php
          if($fkNationality == 1){
            $statement = '<li>For online application submission hard copies of the documents are NOT required.</li>';
          }
          if($fkNationality == 2 || $fkNationality == 3){
            $statement = '<li>Please follow following instructions at the time of form submission
        <ul>
            <li>Download Admission processing fee challan</li>
            <li>Submit required amount in the bank</li>
            </ul></li>
            <li>Please attach following required documents and send through email on overseas.admissions@iiu.edu.pk for Male candidates and  overseas.female.admissions@iiu.edu.pk for Female candidates
            <ul>
              <li>Paid Fee Challan</li>
              <li> Picture </li>
              <li> Passport </li>
              <li> All Certificates/degrees (10th Grade, 12th Grade, Bachelor, Master) </li>
              <li> No. Objection Certificate from M/o Foreign Affairs Or your Embassy in Pakistan </li>
              <li> Residential Permit (Aqamah)  </li>
              <li> NICOP (for Overseas Pakistani only).  </li>
              <li> Foreign Students Information Sheet (Required for NOC of HEC and study Visa)  </li>
            </ul>
          </li>';
          }
          ?>
          <?= $statement ?>
          <li>If you want to apply in other program(s). Go to Select Program page.</a></li>
        </ul><br /><br />
        <h3>Best Regards</h3>
        <h4>Admission Office</h4>
        <p>International Islamic University, Islamabad (IIUI)</p>
        <p>http://admission.iiu.edu.pk/</p>
        <br/><br/>
        <div style="font-size:smaller;background-color: #3c763d;color: #fff;">This communication contains information that is for the exclusive use of the intended recipient(s). If you are not the intended recipient, disclosure, copying, distribution or other use of, or taking of any action in reliance upon, this communication or the information in it is prohibited and may be unlawful. If you have received this communication in error please notify the sender by return email, delete it from your system and destroy any copies.</div>
    </body>
</html>