<!DOCTYPE html>
<html>
<head>
		
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>@yield('title')</title>
  <link rel="shortcut icon" href="/storage/images/favicon.ico" type="image/x-icon">
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="{{ url('frontendlte/bower_components/bootstrap/dist/css/bootstrap.min.css') }}">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{ url('frontendlte/bower_components/font-awesome/css/font-awesome.min.css') }}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="{{ url('frontendlte/bower_components/Ionicons/css/ionicons.min.css') }}">

  <!-- Daterange picker -->
  <link rel="stylesheet" href="{{ url('frontendlte/bower_components/bootstrap-daterangepicker/daterangepicker.css') }}">
  <!-- Date Picker -->
  <link rel="stylesheet" href="{{ url('frontendlte/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="{{ url('frontendlte/plugins/iCheck/all.css') }}">
  <!-- Bootstrap Color Picker -->
  <link rel="stylesheet" href="{{ url('frontendlte/bower_components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css') }}">
  <!-- Bootstrap time Picker -->
  <link rel="stylesheet" href="{{ url('frontendlte/plugins/timepicker/bootstrap-timepicker.min.css') }}">
  <!-- Select2 -->
  <link rel="stylesheet" href="{{ url('frontendlte/bower_components/select2/dist/css/select2.min.css') }}">

  <link rel="stylesheet" href="{{ url('frontendlte/dist/css/AdminLTE.min.css') }}">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="{{ url('frontendlte/dist/css/skins/_all-skins.min.css') }}">
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
  <link rel="stylesheet" href="{{ url('frontendlte/dist/css/custom.css') }}">
  <link rel="stylesheet" href="{{ url('frontendlte/dist/css/frontcustom.css') }}">
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
  
<!-- Global site tag (gtag.js) - Google Ads: 10831882412 -->
<script async src="https://www.googletagmanager.com/gtag/js?id=AW-10831882412"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'AW-10831882412');
</script>

<script>
  gtag('event', 'page_view', {
    'send_to': 'AW-10831882412',
    'value': 'replace with value',
    'items': [{
      'id': 'replace with value',
      'location_id': 'replace with value',
      'google_business_vertical': 'education'
    }]
  });
</script>

</head>
<body class="hold-transition skin-green sidebar-mini">
<div id="load"></div>
<div class="wrapper" id="for-loading">

  <header class="main-header">
    <!-- Logo -->
    <span class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>IIUI</b></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>IIUI</b> Scholarship</span>
    </span>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="/storage/{{($userdetail->applicantDetail->pic == '')? '/images/default.png': $userdetail->applicantDetail->pic}}" class="user-image">
              <span class="hidden-xs">{{$userdetail->name}}</span>
            </a>
             <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                {{-- <img src="/storage/{{($userdetail->applicantDetail->pic == '')? '/images/default.png': $userdetail->applicantDetail->pic}}" alt="Profile Pic" class="img-circle"> --}}
                <img src="{{ ($applicantDetail->pic) ? asset('storage/'.$applicantDetail->pic) : asset('storage/images/default.png') }}" alt="Profile Pic" class="img-circle">
                
                <p>{{$userdetail->name}}</p>
              </li>
              <!-- Menu Body -->
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  <a href="{{ route('updatepassword', $hash) }}" class="btn btn-default btn-flat">Update Password</a>
                </div>
                <div class="pull-right">
                  <form method="POST" action="{{route('applicantLogout')}}">
                    {{ csrf_field() }}
                    <button type="submit" class="btn btn-default btn-flat">Sign out</button>
                  </form>
                </div>
              </li>
            </ul>
          </li>
        </ul>
      </div>
    </nav>
  </header>
  @include('frontend.schmenu')


      <!-- Modal -->
<div class="modal fade" id="schReq" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h3 class="modal-title ff-l" id="exampleModalLabel">Important Instructions Before Applying</h3>
        <button type="button" class="close ff-r" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <div class="modal-body">
        <ul>
          <li>
              Please check Scholarship eligibility criteria carefully. Apply only if qualify as per requirement.
          </li>
          <li>
              Giving a false information will make ineligible for any kind of Scholarship or cancle any Scholarship
          </li>
          <li>
              Kindly attach all Documents required.
          </li>
        </ul>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

  