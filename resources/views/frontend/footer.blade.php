	<footer class="main-footer">
		<div class="pull-right hidden-xs">
		  <b>IIUI Admission System | Powered by IT Center</b>
		</div>
		<strong>Copyright &copy; <?= date("Y"); ?> IIUI.</strong> All rights reserved.
	  </footer>
	</div>
	<!-- ./wrapper -->
	
<!-- jQuery 3 -->
<script src="{{ url('frontendlte/bower_components/jquery/dist/jquery.min.js') }}"></script>
<!-- Bootstrap 3.3.7 -->
<script src="{{ url('frontendlte/bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
<!-- Select2 -->
<script src="{{ url('frontendlte/bower_components/select2/dist/js/select2.full.min.js') }}"></script>
<!-- InputMask -->
<script src="{{ url('frontendlte/plugins/input-mask/jquery.inputmask.js') }}"></script>
<script src="{{ url('frontendlte/plugins/input-mask/jquery.inputmask.date.extensions.js') }}"></script>
<script src="{{ url('frontendlte/plugins/input-mask/jquery.inputmask.extensions.js') }}"></script>
<!-- date-range-picker -->
<script src="{{ url('frontendlte/bower_components/moment/min/moment.min.js') }}"></script>
<script src="{{ url('frontendlte/bower_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
<!-- bootstrap datepicker -->
<script src="{{ url('frontendlte/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
<!-- bootstrap color picker -->
<script src="{{ url('frontendlte/bower_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js') }}"></script>
<!-- bootstrap time picker -->
<script src="{{ url('frontendlte/plugins/timepicker/bootstrap-timepicker.min.js') }}"></script>
<!-- SlimScroll -->
<script src="{{ url('frontendlte/bower_components/jquery-slimscroll/jquery.slimscroll.min.js') }}"></script>
<!-- iCheck 1.0.1 -->
<script src="{{ url('frontendlte/plugins/iCheck/icheck.min.js') }}"></script>
<!-- FastClick -->
<script src="{{ url('frontendlte/bower_components/fastclick/lib/fastclick.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ url('frontendlte/dist/js/adminlte.min.js') }}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{ url('frontendlte/dist/js/demo.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
<script src="{{ url('frontendlte/dist/js/custom.js') }}"></script>
<script>
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});
</script>
<script src="{{ url('frontendlte/dist/js/frontcustom.js') }}"></script>
</body>
</html>
	