@section('title')
    Personal Info
@endsection
@include('frontend.header')
@php
    if ($applicationStatus->step_save == 1 && $hash == '') {
        $readonlyUser = "readonly='readonly'";
        $disabledUser = 'disabled';
        $disabledUser1 = 'disabled="disabled"';
    } else {
        $readonlyUser = '';
        $disabledUser = '';
        $disabledUser1 = '';
    }
@endphp
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <i class="fa fa-user text-yellow"></i> Personal Info
            <small>Add / Edit Personal Info</small>
        </h1>
    </section>
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    @if ($status)
        <div class="alert alert-success" role="alert"> {{ $status }}</div>
    @endif
    <!-- Main content -->
    <section class="content">
        <form action="{{ route('personaldetail', $hash) }}" class="personalDetailForm" method="POST"
            enctype="multipart/form-data">
            <input type="hidden" name="userId" value="{{ $userdetail->userId }}">
            <input type="hidden" name="server_date" value="{{ date('Y-m-d h:i:s') }}">
            {{ csrf_field() }}
            <!-- SELECT2 EXAMPLE -->
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Enter Personal Info</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body personalDetailForm_updated">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Applying for</label>
                                <select class="form-control select2" id="level" name="level" style="width: 100%;"
                                    required {{ $disabledUser }}>
                                    <option value="">Select Level</option>
                                    @foreach ($level as $l)
                                        <option value="{{ $l->pkLevelId }}"
                                            {{ $userdetail->fkLevelId == $l->pkLevelId ? 'selected' : '' }}>
                                            {{ $l->programLevel }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <!-- /.form-group -->
                            <div class="form-group">
                                <label>Father Name</label>
                                <input type="text" name="father_name" id="father_name"
                                    value="{{ $applicantDetail->fatherName ?? old('father_name') }}"
                                    class="form-control" required {{ $readonlyUser }}>
                            </div>
                            <!-- /.form-group -->
                            <div class="form-group">
                                <label class="contact-label">
                                    {{ $userdetail->fkNationality == 1 ? 'Mobile No.(301252xxx)' : 'Contact No' }}</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-phone"></i>
                                    </div>
                                    @php
                                        $carrier = '';
                                        $phone = '';
                                        if ($applicantDetail->mobile) {
                                            $carrier = substr($applicantDetail->mobile, 2, 3);
                                            $phone = substr($applicantDetail->mobile, 5, 7);
                                        }
                                    @endphp
                                    <select name="carrier" id="carrier"
                                        class="form-control {{ $userdetail->fkNationality == 1 ? 'ext-show' : 'ext-hide-2' }}"
                                        {{ $userdetail->fkNationality == 1 ? 'required' : '' }} style="width: 30%;"
                                        {{ $disabledUser1 }}>
                                        <option value="">Code</option>
                                        <option value="300"
                                            {{ $carrier == '300' || old('carrier') == '300' ? 'selected' : '' }}>0300
                                        </option>
                                        <option value="301"
                                            {{ $carrier == '301' || old('carrier') == '301' ? 'selected' : '' }}>0301
                                        </option>
                                        <option value="302"
                                            {{ $carrier == '302' || old('carrier') == '302' ? 'selected' : '' }}>0302
                                        </option>
                                        <option value="303"
                                            {{ $carrier == '303' || old('carrier') == '303' ? 'selected' : '' }}>0303
                                        </option>
                                        <option value="304"
                                            {{ $carrier == '304' || old('carrier') == '304' ? 'selected' : '' }}>0304
                                        </option>
                                        <option value="305"
                                            {{ $carrier == '305' || old('carrier') == '305' ? 'selected' : '' }}>0305
                                        </option>
                                        <option value="306"
                                            {{ $carrier == '306' || old('carrier') == '306' ? 'selected' : '' }}>0306
                                        </option>
                                        <option value="307"
                                            {{ $carrier == '307' || old('carrier') == '307' ? 'selected' : '' }}>0307
                                        </option>
                                        <option value="308"
                                            {{ $carrier == '308' || old('carrier') == '308' ? 'selected' : '' }}>0308
                                        </option>
                                        <option value="309"
                                            {{ $carrier == '309' || old('carrier') == '309' ? 'selected' : '' }}>0309
                                        </option>
                                        <option value="310"
                                            {{ $carrier == '310' || old('carrier') == '310' ? 'selected' : '' }}>0310
                                        </option>
                                        <option value="311"
                                            {{ $carrier == '311' || old('carrier') == '311' ? 'selected' : '' }}>0311
                                        </option>
                                        <option value="312"
                                            {{ $carrier == '312' || old('carrier') == '312' ? 'selected' : '' }}>0312
                                        </option>
                                        <option value="313"
                                            {{ $carrier == '313' || old('carrier') == '313' ? 'selected' : '' }}>0313
                                        </option>
                                        <option value="314"
                                            {{ $carrier == '314' || old('carrier') == '314' ? 'selected' : '' }}>0314
                                        </option>
                                        <option value="315"
                                            {{ $carrier == '315' || old('carrier') == '315' ? 'selected' : '' }}>0315
                                        </option>
                                        <option value="316"
                                            {{ $carrier == '316' || old('carrier') == '316' ? 'selected' : '' }}>0316
                                        </option>
                                        <option value="317"
                                            {{ $carrier == '317' || old('carrier') == '317' ? 'selected' : '' }}>0317
                                        </option>
                                        <option value="318"
                                            {{ $carrier == '318' || old('carrier') == '318' ? 'selected' : '' }}>0318
                                        </option>
                                        <option value="319"
                                            {{ $carrier == '319' || old('carrier') == '319' ? 'selected' : '' }}>0319
                                        </option>
                                        <option value="320"
                                            {{ $carrier == '320' || old('carrier') == '320' ? 'selected' : '' }}>0320
                                        </option>
                                        <option value="321"
                                            {{ $carrier == '321' || old('carrier') == '321' ? 'selected' : '' }}>0321
                                        </option>
                                        <option value="322"
                                            {{ $carrier == '322' || old('carrier') == '322' ? 'selected' : '' }}>0322
                                        </option>
                                        <option value="323"
                                            {{ $carrier == '323' || old('carrier') == '323' ? 'selected' : '' }}>0323
                                        </option>
                                        <option value="324"
                                            {{ $carrier == '324' || old('carrier') == '324' ? 'selected' : '' }}>0324
                                        </option>
                                        <option value="327"
                                            {{ $carrier == '327' || old('carrier') == '327' ? 'selected' : '' }}>0327
                                        </option>
                                        <option value="330"
                                            {{ $carrier == '330' || old('carrier') == '330' ? 'selected' : '' }}>0330
                                        </option>
                                        <option value="331"
                                            {{ $carrier == '331' || old('carrier') == '331' ? 'selected' : '' }}>0331
                                        </option>
                                        <option value="332"
                                            {{ $carrier == '332' || old('carrier') == '332' ? 'selected' : '' }}>0332
                                        </option>
                                        <option value="333"
                                            {{ $carrier == '333' || old('carrier') == '333' ? 'selected' : '' }}>0333
                                        </option>
                                        <option value="334"
                                            {{ $carrier == '334' || old('carrier') == '334' ? 'selected' : '' }}>0334
                                        </option>
                                        <option value="335"
                                            {{ $carrier == '335' || old('carrier') == '335' ? 'selected' : '' }}>0335
                                        </option>
                                        <option value="336"
                                            {{ $carrier == '336' || old('carrier') == '336' ? 'selected' : '' }}>0336
                                        </option>
                                        <option value="337"
                                            {{ $carrier == '337' || old('carrier') == '337' ? 'selected' : '' }}>0337
                                        </option>
                                        <option value="340"
                                            {{ $carrier == '340' || old('carrier') == '340' ? 'selected' : '' }}>0340
                                        </option>
                                        <option value="341"
                                            {{ $carrier == '341' || old('carrier') == '341' ? 'selected' : '' }}>0341
                                        </option>
                                        <option value="342"
                                            {{ $carrier == '342' || old('carrier') == '342' ? 'selected' : '' }}>0342
                                        </option>
                                        <option value="343"
                                            {{ $carrier == '343' || old('carrier') == '343' ? 'selected' : '' }}>0343
                                        </option>
                                        <option value="344"
                                            {{ $carrier == '344' || old('carrier') == '344' ? 'selected' : '' }}>0344
                                        </option>
                                        <option value="345"
                                            {{ $carrier == '345' || old('carrier') == '345' ? 'selected' : '' }}>0345
                                        </option>
                                        <option value="346"
                                            {{ $carrier == '346' || old('carrier') == '346' ? 'selected' : '' }}>0346
                                        </option>
                                        <option value="347"
                                            {{ $carrier == '347' || old('carrier') == '347' ? 'selected' : '' }}>0347
                                        </option>
                                        <option value="348"
                                            {{ $carrier == '348' || old('carrier') == '348' ? 'selected' : '' }}>0348
                                        </option>
                                        <option value="349"
                                            {{ $carrier == '349' || old('carrier') == '349' ? 'selected' : '' }}>0349
                                        </option>
                                    </select>
                                    <input type="text"
                                        class="form-control {{ $userdetail->fkNationality == 1 ? 'ext-show' : 'ext-hide-2' }}"
                                        style="width: 70%;" onkeypress="return isNumberKey(event)" id="phone"
                                        name="phone" minlength="7" maxlength="7"
                                        value="{{ $phone ?: old('phone') }}"
                                        {{ $userdetail->fkNationality == 1 ? 'required' : '' }} {{ $readonlyUser }}>
                                    <input type="number"
                                        class="form-control {{ $userdetail->fkNationality == 2 || $userdetail->fkNationality == 3 ? 'ext-show' : 'ext-hide-2' }}"
                                        {{ $userdetail->fkNationality == 2 || $userdetail->fkNationality == 3 ? 'required' : '' }}
                                        id="overseas_phone"
                                        value="{{ $applicantDetail->mobile ?: old('overseas_phone') }}"
                                        name="overseas_phone" {{ $readonlyUser }}>
                                </div>
                            </div>
                        </div>
                        <!-- /.col -->
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Full Name</label>
                                <input type="text" name="full_name" id="full_name"
                                    value="{{ $userdetail->name ?? old('full_name') }}" class="form-control" required
                                    {{ $readonlyUser }}>
                            </div>
                            <!-- /.form-group -->
                            <div class="form-group">
                                <label>Email address</label> <span class="error" id="error_email"></span>
                                <input type="email" class="form-control" id="email" name="email"
                                    value="{{ $userdetail->email }}" required readonly>
                            </div>
                            <!-- /.form-group -->
                            <div class="form-group">
                                <label>CNIC/B-Form/Passport No.</label><br />
                                <input type="text" class="form-control" id="cnic" name="cnic"
                                    value="{{ $userdetail->cnic }}" required readonly>
                            </div>
                            <!-- /.form-group -->
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <div id="ghost"><img
                                        src="{{ $applicantDetail->pic ? asset('storage/' . $applicantDetail->pic) : asset('storage/images/default.png') }}"
                                        id="pic1" alt="Profile Pic" class="img-thum"></div>
                                <input type="file" accept="image/*" class="form-control " id="picture"
                                    name="picture" {{ $applicantDetail->pic ? '' : 'required' }}>
                                <label class="img-upload-cls">Upload passport size picture with Blue or White
                                    Background. <br />Max Size (4MB)</label>
                            </div>
                        </div>
                        <!-- /.col -->
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Date of Birth</label>
                                <div class="input-group date">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" class="form-control pull-right fltpckr"
                                        value="@if ($applicantDetail->DOB) {{ \Carbon\Carbon::parse($applicantDetail->DOB)->format('Y-m-d') }} @endif @if (old('date_of_birth')) {{ \Carbon\Carbon::parse(old('date_of_birth'))->format('Y-m-d') }} @endif"
                                        name="date_of_birth" required {{ $disabledUser1 }}>
                                </div>
                            </div>
                            <!-- /.form-group -->
                            <div class="form-group">
                                <label>Country</label>
                                <select class="form-control select2" id="country" required name="country"
                                    style="width: 100%;" {{ $disabledUser }}>
                                    <option value="">Select Country</option>
                                    <option value="162" selected> Pakistan </option>
                                    @foreach ($country as $n)
                                        {{--  <option value="{{$n->pkCountryId}}" 
                        @if ($applicantAddress->fkCountryIdPmt == $n->pkCountryId)
                          selected
                        @elseif($n->pkCountryId == '162')
                          selected
                        @endif
                        >{{$n->countryName}}
                      </option> --}}
                                        <option value="{{ $n->pkCountryId }}"
                                            {{ $applicantAddress->fkCountryIdPmt == $n->pkCountryId ? 'selected' : '' }}>
                                            {{ $n->countryName }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <!-- /.form-group -->
                        </div>
                        <!-- /.col -->
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Nationality</label>
                                <select class="form-control select2" id="nationality" name="nationality" required
                                    style="width: 100%;" required {{ $disabledUser }}>
                                    <option value="">Select Nationality</option>
                                    @foreach ($nationalilty as $n)
                                        <option value="{{ $n->id }}"
                                            {{ $userdetail->fkNationality == $n->id ? 'selected' : '' }}>
                                            {{ $n->nationality }}</option>
                                    @endforeach
                                </select>
                            </div>
                            @php
                                // dd($userdetail->fkNationality);
                            @endphp
                            <!-- /.form-group -->
                            <div class="form-group {{ $userdetail->fkNationality == 1 ? 'ext-show' : 'ext-hide-2' }}">
                                <label>Domicile</label>
                                <select class="form-control select2" id="domicile" name="domicile"
                                    {{ $userdetail->fkNationality == 1 ? 'required' : '' }} style="width: 100%;"
                                    {{ $disabledUser }}>
                                    <option value="">Select Domicile</option>
                                    @foreach ($province as $n)
                                        <option value="{{ $n->pkProvId }}"
                                            {{ $applicantDetail->fkDomicile == $n->pkProvId ? 'selected' : '' }}>
                                            {{ $n->provName }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div
                                class="form-group afghan_refugee {{ $userdetail->fkNationality == 2 || $userdetail->fkNationality == 3 ? 'ext-show' : 'ext-hide-2' }}">
                                <input type="checkbox" class="form-group" name="afghan_refugee" value="1"
                                    {{ $applicantDetail->afghan_refugee == 1 ? 'checked' : '' }} {{ $disabledUser }}>
                                Afghan Refugee
                            </div>
                            <!-- /.form-group -->
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Gender</label><br />
                                <label class="nohash">
                                    <input type="radio" name="gender" id="gender" value="3"
                                        {{ $userdetail->fkGenderId == 3 ? 'checked="checked"' : '' }}
                                        class="flat-red form-control" {{ $disabledUser }}> Male
                                </label>
                                <label class="nohash">
                                    <input type="radio" name="gender" id="gender" value="2"
                                        {{ $userdetail->fkGenderId == 2 ? 'checked="checked"' : '' }}
                                        class="flat-red form-control" {{ $disabledUser }}> Female
                                </label>
                                <label class="nohash">
                                    <input type="radio" name="gender" id="gender" value="others"
                                        {{ $userdetail->fkGenderId == 3 && $userdetail->gender_other == 'others' ? 'checked="checked"' : '' }}
                                        class="flat-red form-control" {{ $disabledUser }}> Others
                                </label>
                            </div>
                            <div class="form-group {{ $userdetail->fkNationality == 1 ? 'ext-show' : 'ext-hide-2' }}">
                                <label>District</label>
                                <select class="form-control select2" id="district" name="district"
                                    style="width: 100%;" {{ $userdetail->fkNationality == 1 ? 'required' : '' }}
                                    {{ $disabledUser }}>
                                    <option value="">Select District</option>
                                    @if ($district)
                                        <option value="{{ $district->pkDistId }}" selected>
                                            {{ $district->distName }}</option>
                                    @endif
                                </select>
                            </div>

                            <div
                                class="form-group nicop {{ $userdetail->fkNationality == 2 ? 'ext-show' : 'ext-hide-2' }}">
                                <label>NICOP Number</label>
                                <input type="text" class="form-control"
                                    {{ $userdetail->fkNationality == 2 ? 'required' : '' }} id="nicop"
                                    value="{{ $applicantDetail->nicop ?: old('nicop') }}" name="nicop"
                                    {{ $readonlyUser }}>
                            </div>
                            <!-- /.form-group -->
                        </div>
                        <!-- /.col -->
                    </div>

                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="req-rd">Religion</label>
                                <select class="form-control select2" id="religion" name="religion"
                                    style="width: 100%;" required {{ $disabledUser }}>
                                    <option value="">Select</option>
                                    <option value="Ahamdi"
                                        {{ $applicantDetail->religion == 'Ahamdi' || old('religion') == 'Ahamdi' ? 'selected' : '' }}>
                                        Ahamdi</option>
                                    <option value="Budhist"
                                        {{ $applicantDetail->religion == 'Budhist' || old('religion') == 'Budhist' ? 'selected' : '' }}>
                                        Budhist</option>
                                    <option value="Christianity"
                                        {{ $applicantDetail->religion == 'Christianity' || old('religion') == 'Christianity' ? 'selected' : '' }}>
                                        Christianity</option>
                                    <option value="Hinduism"
                                        {{ $applicantDetail->religion == 'Hinduism' || old('religion') == 'Hinduism' ? 'selected' : '' }}>
                                        Hinduism</option>
                                    <option value="Islam"
                                        {{ $applicantDetail->religion == 'Islam' || $applicantDetail->religion == '' ? 'selected' : '' }}>
                                        Islam</option>
                                    <option value="Parsi"
                                        {{ $applicantDetail->religion == 'Parsi' || old('religion') == 'Parsi' ? 'selected' : '' }}>
                                        Parsi</option>
                                    <option value="Sikh"
                                        {{ $applicantDetail->religion == 'Sikh' || old('religion') == 'Sikh' ? 'selected' : '' }}>
                                        Sikh</option>
                                    <option value="Others"
                                        {{ $applicantDetail->religion == 'Others' || old('religion') == 'Others' ? 'selected' : '' }}>
                                        Others</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <label class="req-rd">How did you find us?</label>
                                <label>
                                    <input type="radio" name="find_us" value="Newspaper"
                                        {{ $applicantDetail->find_us == 'Newspaper' ? 'checked' : '' }}
                                        class="flat-red form-control" {{ $disabledUser }} required> Newspaper
                                </label>
                                <label>
                                    <input type="radio" name="find_us" value="Facebook"
                                        {{ $applicantDetail->find_us == 'Facebook' ? 'checked' : '' }}
                                        class="flat-red form-control" {{ $disabledUser }}> Facebook
                                </label>
                                <label>
                                    <input type="radio" name="find_us" value="Twitter"
                                        {{ $applicantDetail->find_us == 'Twitter' ? 'checked' : '' }}
                                        class="flat-red form-control" {{ $disabledUser }}> Twitter
                                </label>
                                <label>
                                    <input type="radio" name="find_us" value="IIU Website"
                                        {{ $applicantDetail->find_us == 'IIU Website' ? 'checked' : '' }}
                                        class="flat-red form-control" {{ $disabledUser }}> IIU Website
                                </label>
                                <label>
                                    <input type="radio" name="find_us" value="Other"
                                        {{ $applicantDetail->find_us == 'Other' ? 'checked' : '' }}
                                        class="flat-red form-control" {{ $disabledUser }}> Other
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="req-rd">Want to avail univeristy transport facility?</label>
                                <label class="nohash">
                                    <input type="radio" name="transport_facility" id="transport_facility" value="1"
                                        {{ $applicantDetail->transport_facility == 1 ? 'checked="checked"' : '' }}
                                        class="flat-red form-control" {{ $disabledUser }}> Yes
                                </label>
                                <label class="nohash">
                                    <input type="radio" name="transport_facility" id="transport_facility" value="0"
                                        {{ $applicantDetail->transport_facility == 0 ? 'checked="checked"' : '' }}
                                        class="flat-red form-control" {{ $disabledUser }}> No
                                </label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="req-rd">Want to avail univeristy hostel facility?</label>
                                <label class="nohash">
                                    <input type="radio" name="hostel_facility" id="hostel_facility" value="1"
                                        {{ $applicantDetail->hostel_facility == 1 ? 'checked="checked"' : '' }}
                                        class="flat-red form-control" {{ $disabledUser }}> Yes
                                </label>
                                <label class="nohash">
                                    <input type="radio" name="hostel_facility" id="hostel_facility" value="0"
                                        {{ $applicantDetail->hostel_facility == 0 ? 'checked="checked"' : '' }}
                                        class="flat-red form-control" {{ $disabledUser }}> No
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->










                {{-- Dev Mannan: Code for disability starts --}}
                <div class="box-header box box-success">
                    <h3 class="box-title"> Disability Section</h3>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="req-rd">Disability / Type of disability</label>
                                <select class="form-control select2 disability_control" id="disability" name="disability"
                                    style="width: 100%;" required {{ $disabledUser }}>
                                    <option value="">Select</option>
                                    <option value="None"
                                        {{ $applicantDetail->disable == 'None' || old('disability') == 'None' ? 'selected' : '' }}>
                                        None</option>
                                    <option value="Sight"
                                        {{ $applicantDetail->disable == 'Sight' || old('disability') == 'Sight' ? 'selected' : '' }}>
                                        Sight</option>
                                    <option value="Hearing"
                                        {{ $applicantDetail->disable == 'Hearing' || old('disability') == 'Hearing' ? 'selected' : '' }}>
                                        Hearing</option>
                                    <option value="Speech"
                                        {{ $applicantDetail->disable == 'Speech' || old('disability') == 'Speech' ? 'selected' : '' }}>
                                        Speech</option>
                                    <option value="Physical"
                                        {{ $applicantDetail->disable == 'Physical' || old('disability') == 'Physical' ? 'selected' : '' }}>
                                        Physical</option>
                                    <option value="Mental"
                                        {{ $applicantDetail->disable == 'Mental' || old('disability') == 'Mental' ? 'selected' : '' }}>
                                        Mental</option>
                                    <option value="Others"
                                        {{ $applicantDetail->disable == 'Others' || old('disability') == 'Others' ? 'selected' : '' }}>
                                        Others</option>
                                </select>
                            </div>
                            <!-- /.form-group -->
                        </div>

                        <div class="col-md-4">
                            <div class="form-group frm_hide_show {{ $applicantDetail->disable == 'None' ? 'ext-hide_disable' : 'ext-show_disable' }}">
                                <label>Name of disability certificate issuing authority</label>
                                <input type="text" id="issuing_authority"
                                    value="{{ $applicantDetail->issuing_authority ?? old('issuing_authority') }}"
                                    name="issuing_authority" class="form-control" {{ $disabledUser }}>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group frm_hide_show {{ $applicantDetail->disable == 'None' ? 'ext-hide_disable' : 'ext-show_disable' }}">
                                <label>Nature of disability</label>
                                <input type="text" id="nature_of_disability"
                                    value="{{ $applicantDetail->nature_of_disability ?? old('nature_of_disability') }}"
                                    name="nature_of_disability" class="form-control" {{ $disabledUser }}>
                            </div>
                        </div>

                    </div>

                    <div class="row frm_hide_show {{ $applicantDetail->disable == 'None' ? 'ext-hide_disable' : 'ext-show_disable' }}">

                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Cause of disability</label>
                                <input type="text" id="cause_of_disability"
                                    value="{{ $applicantDetail->cause_of_disability ?? old('cause_of_disability') }}"
                                    name="cause_of_disability" class="form-control" {{ $disabledUser }}>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="disp-blck">Do you use wheelchair ?</label>
                                <label>
                                    <input type="radio" name="wheel_chair" value="1"
                                        {{ $applicantDetail->wheel_chair == 1 ? 'checked' : '' }}
                                        class="flat-red form-control" {{ $disabledUser }}> Yes
                                </label>
                                <label>
                                    <input type="radio" name="wheel_chair" value="0"
                                        {{ $applicantDetail->wheel_chair == 0 ? 'checked' : '' }}
                                        class="flat-red form-control" {{ $disabledUser }}> No
                                </label>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="disp-blck">Accommodation/Hostel at university required ?</label>
                                <label>
                                    <input type="radio" name="accomodation_required" value="1"
                                        {{ $applicantDetail->accomodation_required == 1 ? 'checked' : '' }}
                                        class="flat-red form-control" {{ $disabledUser }}> Yes
                                </label>
                                <label>
                                    <input type="radio" name="accomodation_required" value="0"
                                        {{ $applicantDetail->accomodation_required == 0 ? 'checked' : '' }}
                                        class="flat-red form-control" {{ $disabledUser }}> No
                                </label>
                            </div>
                        </div>

                    </div>

                    <div class="row frm_hide_show {{ $applicantDetail->disable == 'None' ? 'ext-hide_disable' : 'ext-show_disable' }}">

                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Accessibility requirement of undertaking the study and Exams at this university
                                    ?</label>
                                <input type="text" id="disabled_accessibility"
                                    value="{{ $applicantDetail->disabled_accessibility ?? old('disabled_accessibility') }}"
                                    name="disabled_accessibility" class="form-control" {{ $disabledUser }}>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Do you need special arrangements for entrance test or interview, if so, please
                                    specify ?</label>
                                <input type="text" id="special_arrangements"
                                    value="{{ $applicantDetail->special_arrangements ?? old('special_arrangements') }}"
                                    name="special_arrangements" class="form-control" {{ $disabledUser }}>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Describe about the special arrangements, made by the institute during your HSSC
                                    study ?</label>
                                <input type="text" id="hssc_arrangements"
                                    value="{{ $applicantDetail->hssc_arrangements ?? old('hssc_arrangements') }}"
                                    name="hssc_arrangements" class="form-control" {{ $disabledUser }}>
                            </div>
                        </div>

                    </div>
                </div>
                {{-- Dev Mannan: Code for disability ends --}}
                {{-- Dev Mannan: Code for overseas pakistani and overseas starts --}}
                <div
                    class="{{ $userdetail->fkNationality == 2 || $userdetail->fkNationality == 3 ? 'ext-show_new' : 'ext-hide-2_new' }}">
                    {{-- @if ($userdetail->fkNationality == 2 || $userdetail->fkNationality == 3) --}}
                    <div class="box-header box box-success">
                        <h3 class="box-title"> Overseas Section</h3>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Place Of Birth</label>
                                    <input type="text" id="place_of_birth"
                                        {{ $userdetail->fkNationality == 2 || $userdetail->fkNationality == 3 ? '' : '' }}
                                        value="{{ $applicantDetail->place_of_birth ?? old('place_of_birth') }}"
                                        name="place_of_birth" class="form-control" {{ $disabledUser }}>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Passport Number</label>
                                    <input type="text" id="passport_number"
                                        {{ $userdetail->fkNationality == 2 || $userdetail->fkNationality == 3 ? '' : '' }}
                                        value="{{ $applicantDetail->passport_number ?? old('passport_number') }}"
                                        name="passport_number" class="form-control" {{ $disabledUser }}>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Date Of Passport Issue</label>
                                    <input type="text" id="date_issue"
                                        {{ $userdetail->fkNationality == 2 || $userdetail->fkNationality == 3 ? '' : '' }}
                                        value="@if ($applicantDetail->date_issue) {{ \Carbon\Carbon::parse($applicantDetail->date_issue)->format('Y-m-d') }} @endif @if (old('date_issue')) {{ \Carbon\Carbon::parse(old('date_issue'))->format('Y-m-d') }} @endif"
                                        name="date_issue" class="form-control fltpckr1" {{ $disabledUser }}>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Place Of Passport Issue</label>
                                    <input type="text" id="place_issue"
                                        {{ $userdetail->fkNationality == 2 || $userdetail->fkNationality == 3 ? '' : '' }}
                                        value="{{ $applicantDetail->place_issue ?? old('place_issue') }}"
                                        name="place_issue" class="form-control" {{ $disabledUser }}>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>HEC NOC Letter Number</label>
                                    <input type="text" id="hec_letter_no"
                                        {{ $userdetail->fkNationality == 2 || $userdetail->fkNationality == 3 ? '' : '' }}
                                        value="{{ $applicantDetail->hec_letter_no ?? old('hec_letter_no') }}"
                                        name="hec_letter_no" class="form-control" {{ $disabledUser }}>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>HEC NOC Letter Date</label>
                                    <input type="text" id="hec_letter_date"
                                        {{ $userdetail->fkNationality == 2 || $userdetail->fkNationality == 3 ? '' : '' }}
                                        value="@if ($applicantDetail->hec_letter_date) {{ \Carbon\Carbon::parse($applicantDetail->hec_letter_date)->format('Y-m-d') }} @endif @if (old('hec_letter_date')) {{ \Carbon\Carbon::parse(old('hec_letter_date'))->format('Y-m-d') }} @endif"
                                        name="hec_letter_date" class="form-control fltpckr1" {{ $disabledUser }}>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Pakistani Visa No.</label>
                                    <input type="text" id="pakistani_visa"
                                        {{ $userdetail->fkNationality == 2 || $userdetail->fkNationality == 3 ? '' : '' }}
                                        value="{{ $applicantDetail->pakistani_visa ?? old('pakistani_visa') }}"
                                        name="pakistani_visa" class="form-control" {{ $disabledUser }}>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Type Of Visa</label>
                                    <input type="text" id="type_of_visa"
                                        {{ $userdetail->fkNationality == 2 || $userdetail->fkNationality == 3 ? '' : '' }}
                                        value="{{ $applicantDetail->type_of_visa ?? old('type_of_visa') }}"
                                        name="type_of_visa" class="form-control" {{ $disabledUser }}>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Visa Issue Date</label>
                                    <input type="text" id="visa_issue_date"
                                        {{ $userdetail->fkNationality == 2 || $userdetail->fkNationality == 3 ? '' : '' }}
                                        value="@if ($applicantDetail->visa_issue_date) {{ \Carbon\Carbon::parse($applicantDetail->visa_issue_date)->format('Y-m-d') }} @endif @if (old('visa_issue_date')) {{ \Carbon\Carbon::parse(old('visa_issue_date'))->format('Y-m-d') }} @endif"
                                        name="visa_issue_date" class="form-control fltpckr1" {{ $disabledUser }}>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Visa Expiry Date</label>
                                    <input type="text" id="visa_expiry_date"
                                        {{ $userdetail->fkNationality == 2 || $userdetail->fkNationality == 3 ? '' : '' }}
                                        value="@if ($applicantDetail->visa_expiry_date) {{ \Carbon\Carbon::parse($applicantDetail->visa_expiry_date)->format('Y-m-d') }} @endif @if (old('visa_expiry_date')) {{ \Carbon\Carbon::parse(old('visa_expiry_date'))->format('Y-m-d') }} @endif"
                                        name="visa_expiry_date" class="form-control fltpckr1" {{ $disabledUser }}>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{-- @endif --}}
                </div>
                {{-- Dev Mannan: Code for overseas pakistani and overseas ends --}}











                <div class="box-footer">
                    <button class="btn btn-social btn-success" {{ $disabledUser }} id="personal-detail-btn"
                        type="submit"><i class="fa fa-save"></i> Save and Move Next</button>
                </div>
        </form>
</div>
<!-- /.box -->
</section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->
@include('frontend.footer')
