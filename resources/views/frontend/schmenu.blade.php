<!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
		<!-- sidebar: style can be found in sidebar.less -->
		<section class="sidebar">
		  <!-- sidebar menu: : style can be found in sidebar.less -->
		  <ul class="sidebar-menu" data-widget="tree">
				<li>
					<a target="_blank" href="{{ url('/frontend/scholarship')}}"><i class="fa fa-graduation-cap text-aqua"></i> <span>ScholarShips</span></a>
				</li>

				<li>
					<a href="{{url('/frontend/progressDetail')}}"><i class="fa fa-line-chart	 text-aqua"></i>Application Progress</a>
				</li>

				<li>
					<a href="{{url('/frontend/qard-e-hasnah')}}"><i class="fa fa-line-chart	 text-aqua"></i>Apply for Qardh-e-Hasnah </a>
				</li>
			
				<!-- <li>
					<a target="_blank" href="{{ url('frontend/contactus')}}"><i class="fa fa-phone-square text-red"></i> <span>Contact Us</span></a>
				</li> -->
				<li>
					<a href="#" data-toggle="modal" data-target="#schReq"><i class="fa fa-info text-yellow"></i> <span>Important Instructions</span></a>
				</li>
		  </ul>
		</section>
		<!-- /.sidebar -->
	</aside>