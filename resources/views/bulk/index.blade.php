@extends('layouts.app')


@section('content')
    <div class="page-content row">
        <div class="page-content-wrapper m-t">
            <form action={{ route('bulkStore') }} method="POST">
                {{ csrf_field() }}
                <div class="form-group">
                    <label for="">Faculty</label>
                    <select name="faculty" id="faculty" class="form-control" required>
                        <option value="">--Select Faculty--</option>
                       @foreach ( $faculties as $faculty )
                            <option value="{{ $faculty->pkFacId}}">{{ $faculty->title }}</option>
                       @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="">Department</label>
                    <select name="department" id="department" class="form-control" required>
                        <option value="">--Select Department--</option>                        
                    </select>
                </div>
                <div class="form-group">
                    <label for="">Program</label>
                    <select name="program" id="programme" class="form-control" required>
                        <option value="">--Select Program--</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="">Gender</label>
                    <select name="gender" id="gender" class="form-control" required>
                        <option value="">--Select Gender--</option>
                        <option value="1">Male & Female</option>
                        <option value="2">Female</option>
                        <option value="3">Male</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="">Status</label>
                    <select name="status" class="form-control" id="status" required>
                        <option value="">--Select Status--</option>
                        @foreach ( $allStatus as $status )
                            <option value="{{ $status->pkAppStatusId}}">{{ $status->status}}</option>
                        @endforeach 
                    </select>
                </div>
                <div class="form-group">
                    <label for="">Type</label>
                    <div class="iradio_square-green" style="position: relative;">
                        <input type="radio" name="bulkType" value="email" class="minimal-red" style="position: absolute; opacity: 0;" data-parsley-multiple="bulkType" required>
                    </div> Email 
                    <div class="iradio_square-green" style="position: relative;">
                        <input type="radio" name="bulkType" value="sms" class="minimal-red" data-parsley-multiple="bulkType" style="position: absolute; opacity: 0;" required>
                    </div> SMS  
                </div>
                <div class="form-group">
                    <label for="">Subject</label>
                    <input type="text" name="subject" class="form-control" required>
                </div>
                <div class="form-group">
                    <label for="">Message</label>
                    <textarea class="form-control" rows="10" name="message" required></textarea>
                </div>
                <input type="submit" value="Send" class="btn btn-primary">
            </form>
        </div>
    </div>

    <script>
        $(function(){
            $('#faculty').on('change', function(){

               $.ajax({
                   method: 'post',
                   url: '{{ route('getDepartments') }}',
                   data: {
                       id: $(this).val()
                   },
                   success: function ( response ) {
                       $('#department').empty();
                       $('#department').append(
                            '<option value="">--Select Department--</option>'
                        );
                       $.each(response, function (key, value) {
                           $('#department').append(
                               '<option value="'+value.pkDeptId+'">'+ value.title+'</option>'
                           );
                       });
                   }
               });
            });

            $('#department').on('change', function(){
                $.ajax({
                    method: 'post',
                    url: '{{ route('getProgramme') }}',
                    data: {
                        id: $(this).val()
                    },
                    success: function (response) {
                        $('#programme').empty();
                        if ( response == '' ) {
                            $('#programme').append(
                                '<option value="">--Select Porgramme--</option>'
                            );
                            return;
                        }
                        $('#programme').append(
                            '<option value="">--Select Porgramme--</option>'
                        );
                        $.each(response, function (key, value) {
                            $('#programme').append(
                                '<option value="'+value.pkProgId+'">'+ value.title+'</option>'
                            );
                        });
                    }
                });
            });
        });
    </script>
@stop
