@extends('layouts.app')


@section('content')
 {!! Charts::styles() !!}
 
<?php 
	use App\Http\Controllers\DashboardController; 
?>
<div class="page-content row"> 
<div class="page-content-wrapper m-t">
 
<div class="sbox" style="border-top: none">
    <div class="sbox-title"> <h3>Admin Dashboard <small> Application Summary </small></h3></div>
	
	   <div class="sbox-content">

   
	<div class="row row-stat">
                            <div class="col-md-3">
                                <div class="panel panel-success-alt noborder">
                                    <div class="panel-heading noborder">
                                        
                                        <div class="panel-icon"><i class="fa fa-users"></i></div>
                                        <div class="media-body">
                                            <h5 class="md-title nomargin">Total Applicants</h5>
                                            <h1 class="mt5" class="pull-right">{{sprintf("%05s", $data->TotalApplied )}}</h1>
                                        </div><!-- media-body -->
                                        <hr>
                                        <div class="clearfix mt20">
                                            <div class="pull-left">
                                                <h5 class="md-title nomargin">Female</h5>
                                                <h4 class="nomargin" >	{{sprintf("%03s", $data->TotalAppliedFemale)}}
												</h4>
                                            </div>
                                            <div class="pull-right">
                                                <h5 class="md-title nomargin">Male</h5>
                                                <h4 class="nomargin">{{sprintf("%03s", $data->TotalAppliedMale)}}</h4>
                                            </div>
                                        </div>
                                        
                                    </div><!-- panel-body -->
                                </div><!-- panel -->
                            </div><!-- col-md-3 -->
                            
                            <div class="col-md-3">
                                <div class="panel panel-warning noborder">
                                    <div class="panel-heading noborder">
                                        
                                        <div class="panel-icon"><i class="fa fa-female"></i></div>
                                        <div class="media-body">
                                            <h5 class="md-title nomargin">Female </h5>
                                            <h1 class="mt5">{{sprintf("%04s", $data->TotalAppliedFemale)}}</h1>
                                        </div><!-- media-body -->
                                        <hr>
                                        <div class="clearfix mt20">
                                            <div class="pull-left">
                                                <h5 class="md-title nomargin">Approved</h5>
                                                <h4 class="nomargin">{{sprintf("%03s", $data->ApprovedFemale)}}</h4>
                                            </div>
                                            <div class="pull-right">
                                                <h5 class="md-title nomargin">Pending</h5>
                                                <h4 class="nomargin">{{sprintf("%03s", $data->DocumentsVerificationFemale)}}</h4>
                                            </div>
                                        </div>
                                        
                                    </div><!-- panel-body -->
                                </div><!-- panel -->
                            </div><!-- col-md-3 -->
                            
                            <div class="col-md-3">
                                <div class="panel panel-primary noborder" >
                                    <div class="panel-heading noborder" style="background-color: #428bca;">
                                       
                                        <div class="panel-icon"><i class="fa fa-male"></i></div>
                                        <div class="media-body">
                                            <h5 class="md-title nomargin">Male </h5>
                                            <h1 class="mt5">{{sprintf("%04s", $data->TotalAppliedMale)}}</h1>
                                        </div><!-- media-body -->
                                        <hr>
                                        <div class="clearfix mt20">
                                            <div class="pull-left">
                                                <h5 class="md-title nomargin">Approved</h5>
                                                <h4 class="nomargin">{{sprintf("%03s", $data->ApprovedMale)}}</h4>
                                            </div>
                                            <div class="pull-right">
                                                <h5 class="md-title nomargin">Pending</h5>
                                                <h4 class="nomargin">{{sprintf("%03s", $data->DocumentsVerificationMale)}}</h4>
                                            </div>
                                        </div>
                                        
                                    </div><!-- panel-body -->
                                </div><!-- panel -->
                            </div><!-- col-md-3 -->
							
							 <div class="col-md-3">
                                <div class="panel panel-dark noborder">
                                    <div class="panel-heading noborder">
                                       
                                        <div class="panel-icon"><i class="fa fa-globe"></i></div>
                                        <div class="media-body">
                                            <h5 class="md-title nomargin">Overseas </h5>
                                            <h1 class="mt5">{{sprintf("%04s", $data->Overseas)}}</h1>
                                        </div><!-- media-body -->
                                        <hr>
                                        <div class="clearfix mt20">
                                            <div class="pull-left">
                                                <h5 class="md-title nomargin">Male</h5>
                                                <h4 class="nomargin">{{sprintf("%04s", $data->OverseasFemale)}}</h4>
                                            </div>
                                            <div class="pull-right">
                                                <h5 class="md-title nomargin">Female</h5>
                                                <h4 class="nomargin">{{sprintf("%04s", $data->OverseasMale)}}</h4>
                                            </div>
                                        </div>
                                        
                                    </div><!-- panel-body -->
                                </div><!-- panel -->
                            </div><!-- col-md-3 -->
                        </div><!-- row -->
                        
	
	
    <div class="ribon-sximo">
        <section >

                <div class="row m-l-none m-r-none m-t  white-bg shortcut " >
                    <div class="col-sm-3  p-sm ribon-grey">
                        <span class="pull-left m-r-sm "><i class="icon-bar-chart"></i></span> 
                        <a href="{{ url('builder/create') }}"  class="clear">
                            <span class="h3 block m-t-xs"><strong>{{sprintf("%04s", $data->Approved)}}</strong>
                            </span> <small >Approved Applications </small>
                        </a>
                    </div>              
                    <div class="col-sm-3 p-sm ribon-grey2">
                        <span class="pull-left m-r-sm "><i class="glyphicon glyphicon-remove-circle"></i></span>
                        <a href="javascript:void(0)" class="clear " onclick="$('.unziped').toggle()">
                            <span class="h3 block m-t-xs"><strong> {{sprintf("%04s", $data->Rejected)}} </strong>
                            </span> <small > Total Rejected </small> 
                        </a>
                    </div>              
                    <div class="col-sm-3   p-sm ribon-grey3">
                        <span class="pull-left m-r-sm "><i class="glyphicon glyphicon-bell "></i></span><!--fa fa-opencart-->
                        <a >
                            <span class="h3 block m-t-xs"><strong> {{sprintf("%04s", $data->DocumentsVerification)}} </strong>
                            </span> <small >Document Verification Pending  </small> 
                        </a>
                    </div>                  
  
                    <div class="col-sm-3   p-sm ribon-grey4">
                        <span class="pull-left m-r-sm "><i class="fa fa-money"></i></span>
                        <a >
                            <span class="h3 block m-t-xs"><strong> {{sprintf("%04s", $data->Submitted)}}</strong>
                            </span> <small >Fee Pending </small> 
                        </a>
                    </div>    

                </div> 

        </section>          
    </div>
@isset($chart)	
    <div class="row">
		<div class="col-sm-6">
		<div class="app">
            <center> {!! $chart->html() !!} </center>
        </div>
        <!-- End Of Main Application -->
			   {!! Charts::scripts() !!}
        {!! $chart->script() !!}

      </div>
	  <div class="col-sm-6">
		<div class="app">
					<center> {!! $pie->html() !!} </center>
				</div>
        {!! $pie->script() !!}
	  </div>
	  
	</div> <!--End Row-->

@endisset

@isset($facStats)
	<div class="row">
	<div class="col-sm-8">
	<center>
	 <div class="sbox-title"> <i class="fa fa-university"></i> <h3>Facultywise Stats <small><a href=#>More Detail</a></small></h3></div>
	<table class="table table-striped table-hover" align="center">
                <tbody>
				<thead>
                <tr>
                    <td><strong>S.No</strong> </td>
                    <td><strong>Faculty</strong></td>
					<td align="center" width="10%"> <strong>Departments</strong></td>
					<td align="center" width="10%"> <strong>Male</strong></td>
					<td align="center" width="10%"> <strong>Female</strong></td>
					<td align="center" width="10%"> <strong>Total</strong></td>
                </tr>
				</thead>
           
	@foreach($facStats as $key => $fac)
		         <tr>
                    <td align="center">{{++$key}}) </td>
                    <td><b>{{$fac['title'] }}</b></td>
					<td align="center"> {{$fac['dept']}}</td>
					<td align="center"> {{$fac['male'] }}</td>
					<td align="center"> {{$fac['female'] }}</td>
					<td align="center"> <b>{{$fac['total'] }}</b></td>
                </tr>
	@endforeach
		         </tbody>
            </table>
	</center>
	</div>
	</div>
@endisset
</div>
</div>  



</div>
</div> 

                     
@stop