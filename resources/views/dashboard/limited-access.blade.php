@extends('layouts.app')

@section('content')
 
<div class="page-content row"> 
    <div class="page-content-wrapper m-t">
        <div class="sbox" style="border-top: none">
            <div class="sbox-title"> 
            	@if ( $groupId == 1 || $groupId == 5 || $groupId == 6 ) 
            		<a href={{route('showDashboardStats')}} class="pull-right">View Dashboard Stats</a>
            	@endif
                <h3> International Islamic University Islamabad </h3>
                <p>Welcome to Online Admission System</p>
            </div>
            <div class="sbox">
                <div class="sbox-content">
                    <div class="container">
                        <div class="row">
                            <div class="">
                                <div class="form-group">
                                    <form class="form-inline" name="semesterSelectorForm" id="semesterSelectorForm" action="{{route('dashboard')}}"  method="post">
                                            {{ csrf_field() }}
                                        Semester Data Want to Show: 
                                        <select class="form-control" name="semester_id" id="semester_id">
                                            <option> --Select Semester-- </option>
                                            @foreach ( $semester as $semester )
                                                <option value="{{ $semester->pkSemesterId }}" {{( Cache::get(auth()->id().'-active_semester') == $semester->pkSemesterId)? 'selected="selected"' : ''}}> 
                                                    {{ $semester->title }} 
                                                </option>
                                            @endforeach
                                        </select>
                                        <button type="submit" class="btn btn-default top-btn">Submit</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>  
    </div>
</div> 

                     
@stop