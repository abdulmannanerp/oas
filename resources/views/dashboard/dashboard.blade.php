@extends('layouts.app') 
@section('content') 

<div class="page-content row">
    <div class="page-content-wrapper m-t">
        <div class="sbox" style="border-top: none">
            <div class="sbox-title">
                <h3>Admission Dashboard  <small> Application Summary </small></h3>
                {{-- @if(auth()->id() == '1')
                <select class="stats-shift">
                    <option value="">--Select--</option>
                    <option value="Fall-2018">Fall-2018</option>
                </select>
                <button name="save" class="tips btn btn-sm btn-save bt-shift" title="Back">
                        <i class="fa  fa-paste"></i>Go 
                </button>
                @endif --}}
                <button class="btn btn-default flt-rgt print-dashboard print-hiding"><i class="fa fa-print" aria-hidden="true"></i></button>
                <button class="btn btn-default flt-rgt top-btn print-dashboard-color hvr-clr print-hiding"><i class="fa fa-print" aria-hidden="true"></i></button>
                <div class="pull-right">
                        
                    <div class="form-group">
                        <form name="semesterSelectorForm" id="semesterSelectorForm" action="{{route('getSpecificSemesterStats')}}"  method="post">
                            <select class="form-control" name="semesterSelector" id="semesterSelector" disabled>
                                <option> --Select Semester-- </option>
                                @foreach ( $semesters as $semester )
                                    <option value="{{ $semester->pkSemesterId }}" {{ ($semester->pkSemesterId == $selectedSemester ) ? 'selected' : '' }}> 
                                        {{ $semester->title }} 
                                    </option>
                                @endforeach
                            </select>
                        </form>
                    </div>
                </div>
            </div> <!-- Ending sbox-title --> 

            <div class="sbox-content">
                <div class="row row-stat">
                    <div class="col-md-12">
                        <a class="pull-right btn btn-link print-hiding" href="{{route('forceRefreshStats')}}">Refresh Stats</a>
                    </div>
                    <div class="col-md-4">
                        <div class="panel panel-success-alt noborder">
                            <div class="panel-heading noborder print-clr-1">
                                <div class="panel-icon">
                                    <i class="fa fa-users"></i>
                                </div>
                                <div class="media-body">
                                    <h5 class="md-title nomargin">Total Applications</h5>
                                    <h1 class="mt5">{{ str_pad ( $statsOverview['totalApplications'] , 5, 0, STR_PAD_LEFT ) }}</h1>
                                </div>
                                <hr>
                                <div class="clearfix mt20">
                                    <div class="row mt5">
                                        <div class="col-md-8">
                                            <h5 class="nomargin fir">Total Logins</h5>
                                        </div>

                                        <div class="col-md-4">
                                            <h4 class="nomargin sec"> {{ str_pad ( $statsOverview['totalFormsIssues'] , 5, 0, STR_PAD_LEFT) }}</h4>
                                        </div>
                                    </div> <!-- Ending row-->
                                    <div class="row mt5">
                                        <div class="col-md-8">
                                            <h5 class="nomargin fir">Applications (Fee Verified)</h5>
                                        </div>

                                        <div class="col-md-4"> 
                                            <h5 class="nomargin sec"> {{ str_pad ( $statsOverview['totalApplications'] , 5, 0, STR_PAD_LEFT ) }}</h5>
                                        </div>
                                    </div> <!-- Ending row-->

                                    <div class="row mt5">
                                        <div class="col-md-8">
                                            <h5 class="nomargin fir">Fee Pending / Invalid</h5>
                                        </div>

                                        <div class="col-md-4">
                                            <h5 class="nomargin sec"> {{ str_pad ( $statsOverview['totalFeeUnverified'] , 5, 0, STR_PAD_LEFT ) }}</h5>
                                        </div>
                                    </div> <!-- Ending row-->
                                    <div class="row mt5">
                                        <div class="col-md-8">
                                            <h5 class="nomargin fir">Documents Pending</h5>
                                        </div>

                                        <div class="col-md-4">
                                            <h5 class="nomargin sec"> {{ str_pad ( $statsOverview['totalDocumentsPending'] , 5, 0, STR_PAD_LEFT ) }}</h5>
                                        </div>
                                    </div> <!-- Ending row-->
                                    <hr class="mt5">
                                    <div class="row mt5">
                                        <div class="col-md-8">
                                            <h5 class="nomargin fir">Approved</h5>
                                        </div>

                                        <div class="col-md-4">
                                            <h5 class="nomargin sec">{{ str_pad ( $statsOverview['totalApprovedApplications'], 5, 0, STR_PAD_LEFT ) }}</h5>
                                        </div>
                                    </div> <!-- Ending row -->

                                    <div class="row mt5">
                                        <div class="col-md-8">
                                            <h5 class="nomargin fir">Rejected</h5>
                                        </div>

                                        <div class="col-md-4">
                                            <h5 class="nomargin sec">{{ str_pad ( $statsOverview['totalRejectedApplications'], 5, 0, STR_PAD_LEFT ) }}</h5>
                                        </div>
                                    </div> <!-- Ending row -->
                                    {{-- <hr class="mt5">
                                    <div class="row mt5">
                                        <div class="col-md-8">
                                            <h5 class="nomargin">Overseas</h5>
                                        </div>

                                        <div class="col-md-4">
                                            <h5 class="nomargin">{{ str_pad ( $statsOverview['totalOverseasApplications'], 5, 0, STR_PAD_LEFT ) }}</h5>
                                        </div>
                                    </div> --}}
                                </div> <!-- Ending Clerfix -->
                            </div> <!--Ending panel-heading -->
                           
                        </div><!-- Endign Panel -->    
                    </div> <!-- Ending box -->
                    
                    <div class="col-md-4">
                        <div class="panel panel-primary noborder">
                            <div class="panel-heading noborder print-clr-2" style="background-color: #428bca;">
                                <div class="panel-icon">
                                    <i class="fa fa-male"></i>
                                </div>
                                <div class="media-body">
                                    <h5 class="md-title nomargin">Applications (Male) </h5>
                                    <h1 class="mt5">{{ str_pad ( $maleApplicationsStats['totalMaleApplications'] , 5, 0, STR_PAD_LEFT ) }}</h1>
                                </div>
                                <!-- media-body -->
                                <hr>
                                <div class="clearfix mt20">
                                    <div class="row mt5">
                                        <div class="col-md-8">
                                            <h5 class="nomargin fir">Total Logins</h5>
                                        </div>

                                        <div class="col-md-4">
                                            <h4 class="nomargin sec"> {{ str_pad ( $maleApplicationsStats['totalMaleFormsIssued'], 5, 0, STR_PAD_LEFT ) }}</h4>
                                        </div>
                                    </div> <!-- Ending row-->
                                    <div class="row mt5">
                                        <div class="col-md-8">
                                            <h5 class="nomargin fir">Applications (Fee Verified)</h5>
                                        </div>

                                        <div class="col-md-4">
                                            <h5 class="nomargin sec"> {{ str_pad ( $maleApplicationsStats['totalMaleApplications'] , 5, 0, STR_PAD_LEFT ) }}</h5>
                                        </div>
                                    </div> <!-- Ending row-->

                                    <div class="row mt5">
                                        <div class="col-md-8">
                                            <h5 class="nomargin fir">Fee Pending / Invalid</h5>
                                        </div>

                                        <div class="col-md-4">
                                            <h5 class="nomargin sec"> {{ str_pad ( $maleApplicationsStats['totalFeeUnverifiedMaleApplications'] , 5, 0, STR_PAD_LEFT ) }}</h5>
                                        </div>
                                    </div> <!-- Ending row-->
                                    <div class="row mt5">
                                        <div class="col-md-8">
                                            <h5 class="nomargin fir">Documents Pending</h5>
                                        </div>

                                        <div class="col-md-4">
                                            <h5 class="nomargin sec"> {{ str_pad ( $maleApplicationsStats['totalMaleDocumentsPending'] , 5, 0, STR_PAD_LEFT ) }}</h5>
                                        </div>
                                    </div> <!-- Ending row-->
                                    <hr class="mt5">
                                    <div class="row mt5">
                                        <div class="col-md-8">
                                            <h5 class="nomargin fir">Approved</h5>
                                        </div>

                                        <div class="col-md-4">
                                            <h5 class="nomargin sec">{{ str_pad ( $maleApplicationsStats['totalMaleApprovedApplications'], 5, 0, STR_PAD_LEFT ) }}</h5>
                                        </div>
                                    </div> <!-- Ending row -->

                                    <div class="row mt5">
                                        <div class="col-md-8">
                                            <h5 class="nomargin fir">Rejected</h5>
                                        </div>

                                        <div class="col-md-4">
                                            <h5 class="nomargin sec">{{ str_pad ( $maleApplicationsStats['totalMaleRejectedApplications'], 5, 0, STR_PAD_LEFT ) }}</h5>
                                        </div>
                                    </div> <!-- Ending row -->
                                    {{-- <hr class="mt5">
                                    <div class="row mt5">
                                        <div class="col-md-8">
                                            <h5 class="nomargin">Overseas</h5>
                                        </div>

                                        <div class="col-md-4">
                                            <h5 class="nomargin">{{ str_pad ( $maleApplicationsStats['totalOverseasMaleApplications'], 5, 0, STR_PAD_LEFT ) }}</h5>
                                        </div>
                                    </div> --}}
                                </div> <!-- Ending clearfix mt20 --> 
                            </div> <!-- panel-body -->
                        </div> <!-- panel -->
                    </div> <!-- Ending box -->  

                    <div class="col-md-4">
                        <div class="panel panel-warning noborder">
                            <div class="panel-heading noborder print-clr-3">

                                <div class="panel-icon">
                                    <i class="fa fa-female"></i>
                                </div>
                                <div class="media-body">
                                    <h5 class="md-title nomargin">Applications (Female)</h5>
                                    <h1 class="mt5">{{ str_pad ( $femaleApplicationsStats['totalFemaleApplications'] , 5, 0, STR_PAD_LEFT ) }}</h1>

                                </div>
                                <hr>
                                <div class="clearfix mt20">
                                    <div class="row mt5">
                                        <div class="col-md-8">
                                            <h5 class="nomargin fir">Total Logins</h5>
                                        </div>

                                        <div class="col-md-4">
                                            <h4 class="nomargin sec"> {{ str_pad ( $femaleApplicationsStats['totalFemaleFormsIssued'], 5, 0, STR_PAD_LEFT ) }}</h4>
                                        </div>
                                    </div> <!-- Ending row-->
                                    <div class="row mt5">
                                        <div class="col-md-8">
                                            <h5 class="nomargin fir">Applications (Fee Verified)</h5>
                                        </div>

                                        <div class="col-md-4">
                                            <h5 class="nomargin sec"> {{ str_pad ( $femaleApplicationsStats['totalFemaleApplications'] , 5, 0, STR_PAD_LEFT ) }}</h5>
                                        </div>
                                    </div> <!-- Ending row-->

                                    <div class="row mt5">
                                        <div class="col-md-8">
                                            <h5 class="nomargin fir">Fee Pending / Invalid</h5>
                                        </div>

                                        <div class="col-md-4">
                                            <h5 class="nomargin sec"> {{ str_pad ( $femaleApplicationsStats['totalFeeUnverifiedFemaleApplications'] , 5, 0, STR_PAD_LEFT ) }}</h5>
                                        </div>
                                    </div> <!-- Ending row-->
                                    <div class="row mt5">
                                        <div class="col-md-8">
                                            <h5 class="nomargin fir">Documents Pending</h5>
                                        </div>

                                        <div class="col-md-4">
                                            <h5 class="nomargin sec"> {{ str_pad ( $femaleApplicationsStats['totalFemaleDocumentsPending'] , 5, 0, STR_PAD_LEFT ) }}</h5>
                                        </div>
                                    </div> <!-- Ending row-->
                                    <hr class="mt5">
                                    <div class="row mt5">
                                        <div class="col-md-8">
                                            <h5 class="nomargin fir">Approved</h5>
                                        </div>

                                        <div class="col-md-4">
                                            <h5 class="nomargin sec">{{ str_pad ( $femaleApplicationsStats['totalFemaleApprovedApplications'], 5, 0, STR_PAD_LEFT ) }}</h5>
                                        </div>
                                    </div> <!-- Ending row -->

                                    <div class="row mt5">
                                        <div class="col-md-8">
                                            <h5 class="nomargin fir">Rejected</h5>
                                        </div>

                                        <div class="col-md-4">
                                            <h5 class="nomargin sec">{{ str_pad ( $femaleApplicationsStats['totalFemaleRejectedApplications'], 5, 0, STR_PAD_LEFT ) }}</h5>
                                        </div>
                                    </div> <!-- Ending row -->
                                    {{-- <hr class="mt5">
                                    <div class="row mt5">
                                        <div class="col-md-8">
                                            <h5 class="nomargin">Overseas</h5>
                                        </div>

                                        <div class="col-md-4">
                                            <h5 class="nomargin">{{ str_pad ( $femaleApplicationsStats['totalOverseasFemaleApplications'], 5, 0, STR_PAD_LEFT ) }}</h5>
                                        </div>
                                    </div> --}}
                                </div>
                            </div>
                        </div>
                    </div> <!-- Ending box -->                                              
                </div> <!-- Ending parent row -->

                @if(auth()->guard('web')->id() == '1')            
                    <div class="row">
                        <div class="col-md-8 cntred-div">
                            <div class="panel panel-success-alt noborder currecny-print">
                                <div class="panel-heading noborder br-color print-clr-4">
                                    <div class="panel-icon">
                                        <i class="fa fa-money pd-mney"></i>
                                    </div>
                                    <div class="media-body">
                                        <h5 class="md-title nomargin">Estimated Revenue Generated So far</h5>
                                        <h1 class="mt5">
                                            {{number_format(
                                                ($totalRevenue['maleEngineeringApplications'] * 2000 ) + 
                                                ($totalRevenue['maleNonEngineeringApplications'] * 1500 ) + 
                                                ($totalRevenue['femaleEngineeringApplications'] * 1500 ) + 
                                                ($totalRevenue['femaleNonEngineeringApplications'] * 1500 )
                                                // ($totalRevenue['overseasMaleApplications'] * 6000) + 
                                                // ($totalRevenue['overseasFemaleApplications'] * 6000 )
                                            )}} PKR
                                        </h1>
                                    </div><hr class="custom-hr">
                                    
                                    <div class="clearfix">
                                        <div class="row">
                                            <div class="col-md-6 print-curr-male">
                                                <h4 class="algn-three">Male</h4>
                                                <div class="row">
                                                    <div class="col-md-6 algn-one">Total</div>
                                                    <div class="col-md-6 algn-two">
                                                        {{number_format(($totalRevenue['maleEngineeringApplications'] * 2000) + ($totalRevenue['maleNonEngineeringApplications'] * 1500))}}
                                                    </div>
                                                </div>

                                                {{-- <div class="row">
                                                    <div class="col-md-6 algn-one">Engineering</div>
                                                    <div class="col-md-6 algn-two">
                                                        {{number_format($totalRevenue['maleEngineeringApplications'] * 2000)}}
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6 algn-one">Others</div>
                                                    <div class="col-md-6 algn-two">
                                                        {{number_format($totalRevenue['maleNonEngineeringApplications'] * 1500)}}
                                                    </div>
                                                </div> --}}
                                            </div>

                                            <div class="col-md-6 print-curr-female">
                                                <h4 class="algn-three"> Female </h4>
                                                <div class="row">
                                                    <div class="col-md-6 algn-one"> Total </div>
                                                    <div class="col-md-6 algn-two">
                                                        {{number_format(($totalRevenue['femaleEngineeringApplications'] * 1500) + ($totalRevenue['femaleNonEngineeringApplications'] * 1500))}}
                                                    </div>
                                                </div>

                                                {{-- <div class="row">
                                                    <div class="col-md-6 algn-one"> Engineering </div>
                                                    <div class="col-md-6 algn-two">
                                                        {{number_format($totalRevenue['femaleEngineeringApplications'] * 1500)}}
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6 algn-one"> Others </div>
                                                    <div class="col-md-6 algn-two">
                                                        {{number_format($totalRevenue['femaleNonEngineeringApplications'] * 1500)}}
                                                    </div>
                                                </div> --}}
                                            </div>

                                            {{-- <div class="col-md-4">
                                                <h4 class="algn-three"> Overseas</h4>
                                                <div class="row">
                                                    <div class="col-md-6 algn-one"> Male </div>
                                                    <div class="col-md-6 algn-two">
                                                        {{number_format($totalRevenue['overseasMaleApplications'] * 6000)}}
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6 algn-one"> Female </div>
                                                    <div class="col-md-6 algn-two">
                                                        {{number_format($totalRevenue['overseasFemaleApplications'] * 6000)}}
                                                    </div>
                                                </div>
                                            </div> --}}
                                        </div> <!-- Ending row-->
                                    </div> <!-- Ending Clerfix -->     
                                </div> <!--Ending panel-heading -->
                            </div><!-- Endign Panel -->    
                        </div> <!-- Ending col-md-8 cntred-div --> 
                    </div> <!-- Ending row --> 
                @endif

                <div class="row print-hiding">
                    <div class="charts">
                        <div class="line-chart" style="height: 400px">
                            @php
                                $label = [];
                                $data = [];
                                foreach ( $topPrograms as $program ) {
                                    $label[] = $program['program']['title'];
                                    $data[] = $program['programCount'];    
                                }
                            @endphp
                            <canvas id="top-program-line-chart"></canvas>
                        </div>
                    </div>
                </div> <!-- Ending row --> 

                <div class="row map-printing">
                    <div id="container" style="height: 500px; min-width: 310px; max-width: 1000%; margin: 0 auto"></div>
                </div>

                <div class="row faculty-wise-printing">
                    <div class="col-sm-8 col-md-12">
                        <center>
                            <div class="sbox-title">
                                <i class="fa fa-university"></i>
                                <h3> Facultywise Stats </h3>
                            </div>
                            <table id="example" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>Faculty</th>
                                        <th>Departments</th>
                                        <th>Male</th>
                                        <th>Female</th>
                                        {{-- <th>Total</th> --}}
                                        <th>Fee Verified</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($facultyStats as $key => $value)
                                    <tr>
                                        <td><b>{{$value['title']}}</b></td>
                                        <td align="center"> {{ $value['count']}}</td>
                                        <td align="center"> {{ $value['totalMaleApplications'] }}</td>
                                        <td align="center"> {{ $value['totalFemaleApplications'] }}</td>
                                        {{-- <td align="center">{{ $value['totalMaleApplications'] + $value['totalFemaleApplications'] }}</td> --}}
                                        <td align="center"><b>{{ $value['feeVerifiedCount'] ?? '0' }}</b></td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </center>
                    </div> <!-- Ending col-sm-8 col-md-12 --> 
                </div> <!-- Ending row --> 

 
            </div> <!-- Ending sbox-content --> 
        </div> <!-- Ending sbox --> 
    </div> <!-- Ending page-content-wrapper m-t --> 
</div> <!-- Ending page-content row --> 
{{-- Dev Mannan: custom code for highmaps starts --}}
<script src="https://code.highcharts.com/maps/highmaps.js"></script>
<script src="https://code.highcharts.com/maps/modules/data.js"></script>
<script src="https://code.highcharts.com/maps/modules/drilldown.js"></script>
<script src="https://code.highcharts.com/maps/modules/exporting.js"></script>
<script src="https://code.highcharts.com/maps/modules/offline-exporting.js"></script>
<script src="{{url('js/pk-all.js')}}"></script>
{{-- <script src="{{url('js/maps.js')}}"></script> --}}

{{-- Dev Mannan: custom code for highmaps ends --}}
<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"></script> 

<script>
    $(document).ready(function() {
        $('#example').DataTable( {
        "paging":   false,
        "bInfo" : false
        } );

        var ctx = document.getElementById('top-program-line-chart').getContext('2d'),
            jsonData = "{{json_encode($data)}}",
            jsonLabels = "{{json_encode($label)}}",
            data = JSON.parse ( jsonData ),
            labels = JSON.parse(jsonLabels.replace(/&quot;/g,'"')),
            minValue =  Math.min.apply (null, data) - 10;

console.log(labels);
            var chart = new Chart(ctx, {
            // The type of chart we want to create
            type: 'line',

            // The data for our dataset
            data: {
                labels: labels,
                datasets: [{
                    label: "Top Programs",
                    // backgroundColor: '#1ab394',
                    borderColor: 'rgb(26,179,148)',
                    data: data,
                }]
            },
            options: {
                responsive: true,
                maintainAspectRatio: false,
                scales: {
                    yAxes: [{
                        display: true,
                        ticks: {
                            suggestedMin: minValue    // minimum will be 0, unless there is a lower value.
                        }
                    }],
                    xAxes: [{
                        ticks: {
                        autoSkip: false
                        }   
                    }]
                }
            }
        });
    }); //ending ready
</script>
{{-- Dev Mannan: Maps Code --}}
{{-- <script>
        var customLabel = {
            'Punjab': {
                color: '#6B5B95'
            },
            'Sindh': {
                color: '#DD4132'
            },
            'KPK':{
                color: '#9E1030'
            },
            'Balochistan': {
                color: '#FE840E'
            },
            'Federal Capital Territory': {
                color: '#FF6F61'
            },
            'F.A.T.A':{
                color: '#C62168'
            },
            'Azad Jammu and Kashmir':{
                color: '#8D9440'
            },
            'GilgitBaltistan':{
                color: '#FFD662'
            }
        };
        function initMap() {
        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 5.5,
            center: {lat: 30.9693492, lng: 70.9427914}
        });
        setMarkers(map);
        }
        function setMarkers(map) {
            var data = [];
            // var infoWindow = new google.maps.InfoWindow;
            $.ajax({
            type: "GET",
            url: '',
            success: function (response) {
                $.each(response, function (key, value) {
                data.push( {
                    district: value.district_name,
                    students: value.total_students,
                    lat: value.lat,
                    lng: value.lng,
                    province: value.prov_name
                });
            });
            var infoVindow = new google.maps.InfoWindow;
            for (var i = 0; i < data.length; i++) {
                var image = 'data:image/svg+xml,%3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20width%3D%2238%22%20height%3D%2238%22%20viewBox%3D%220%200%2038%2038%22%3E%3Cpath%20fill%3D%22%23808080%22%20stroke%3D%22%23ccc%22%20stroke-width%3D%22.5%22%20d%3D%22M34.305%2016.234c0%208.83-15.148%2019.158-15.148%2019.158S3.507%2025.065%203.507%2016.1c0-8.505%206.894-14.304%2015.4-14.304%208.504%200%2015.398%205.933%2015.398%2014.438z%22%2F%3E%3Ctext%20transform%3D%22translate%2819%2018.5%29%22%20fill%3D%22%23fff%22%20style%3D%22font-family%3A%20Arial%2C%20sans-serif%3Bfont-weight%3Abold%3Btext-align%3Acenter%3B%22%20font-size%3D%2212%22%20text-anchor%3D%22middle%22%3E' +  data[i].students  + '%3C%2Ftext%3E%3C%2Fsvg%3E';
                // console.log(image);
                var point = new google.maps.LatLng(
                    parseFloat(data[i].lat),
                    parseFloat(data[i].lng)
                    );
                var icon = customLabel[data[i].province] || {};
                var marker = new google.maps.Marker({
                    map: map,
                    position: point,
                    label: icon.label,
                    // icon: pinSymbol(icon.color)
                    icon: image
                });
                google.maps.event.addListener(marker, 'mouseover', (function(marker, i) {
                    return function() {
                        infoVindow.setContent
                            (
                                data[i].district
                            ); 
                        infoVindow.open(map, marker);
                    }
                })(marker, i));
            }
        }
        });
    }
    function pinSymbol(color) {
    return {
        path: 'M 0,0 C -2,-20 -10,-22 -10,-30 A 10,10 0 1,1 10,-30 C 10,-22 2,-20 0,0 z',
        // fillColor: color,
        fillOpacity: 0,
        strokeColor: '#000',
        strokeWeight: 2,
        scale: 1,
   };
}
    </script>
    <script async defer
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAhqTedXzxds3pH9xBw1EcNWzpQI-A3VsA&callback=initMap">
    </script> --}}


{{-- Dev Mannan: Custom code starts for maps --}}
<script>
    
		//function to parse db count
		function getParsedCount(data,id){

            // var parsedData = JSON.parse(data);

			var	count = 0;
			$.each( data , function ( key, value ) {
				if(id == parseInt(value.code))
					count =  parseInt(value.value) 
			});
			return count;
		}

/*
TODO:
- Check data labels after drilling. Label rank? New positions?
*/

var mapdata = Highcharts.geojson(Highcharts.maps['countries/pk/pk-all']),
    separators = Highcharts.geojson(Highcharts.maps['countries/pk/pk-all'], 'mapline'),
   
   // Some responsiveness
    small = $('#container').width() < 400;
	
	/* Set drilldown pointers
	$.each(mapdata, function (i) {
		this.drilldown = this.properties['who_prov_c'];
		this.value = i; // Non-random bogus data
	});*/
	
$.get('<?= route('mapp',['id'=> ''])?>', function (dbdata) {
	
	// Set drilldown pointers and count values
    $.each(mapdata, function (i) {
        this.drilldown = this.properties['who_prov_c'];
        // console.log(dbdata);
        // return false;
		this.value = getParsedCount(dbdata,this.properties['who_prov_c'])
	});

    // Instantiate the map
    Highcharts.mapChart('container', {

		chart: {
			events: {
				drilldown: function (e) {
					if (!e.seriesOptions) {
						var chart = this,
							mapKey = e.point.drilldown,
							// Handle error, the timeout is cleared on success
							fail = setTimeout(function () {
								if (!Highcharts.maps[mapKey]) {
									// chart.showLoading('<i class="icon-frown"></i> Failed loading ' + e.point.name);
									fail = setTimeout(function () {
										chart.hideLoading();
									}, 500);
								}
							}, 1000);

						// Show the spinner
						chart.showLoading('<i class="icon-spinner icon-spin icon-3x"></i>'); // Font Awesome spinner
                        // Load the drilldown map
                        var total_url = 'https://admission.iiu.edu.pk/public/js/'+mapKey+'.js';
						$.getScript(total_url, function () {

							data = Highcharts.geojson(Highcharts.maps[mapKey]);
                            // console.log(data);
                            var total_url_2 = 'https://admission.iiu.edu.pk/public/mapp/'+mapKey;
							$.get(total_url_2, function (dbdata) {

							// Set a count value
							$.each(data, function (i) {
								this.value = getParsedCount(dbdata,this.properties['who_dist_c'])			
							});

							// Hide loading and add series
							chart.hideLoading();
							clearTimeout(fail);
							chart.addSeriesAsDrilldown(e.point, {
								name: e.point.properties['who_prov_n'],
								data: data,
								dataLabels: {
									enabled: true,
									format: '{point.properties.map_label}<br>{point.value}'
								},
								tooltip: {
									pointFormat: '{point.properties.who_dist_n}<br> Students Applied: {point.value}'
								}
							});
							});
						});
					}
					console.log(e.point.properties['who_prov_n']);
					this.setTitle(null, { text: e.point.properties['who_prov_n'] });
				},
				drillup: function () {
					this.setTitle(null, { text: 'Pakistan' });
				}
			}
		},

        title: {
            text: 'Applicant Map'
        },
        credits: {
            enabled: false
        },
        exporting: { 
            enabled: false 
        },
		
		subtitle: {
			text: 'Pakistan',
			floating: true,
			align: 'right',
			y: 50,
			style: {
				fontSize: '16px'
			}
		},

		legend: small ? {} : {
			layout: 'vertical',
			align: 'right',
			verticalAlign: 'middle'
		},

		colorAxis: {
			min: 0,
			minColor: '#E6E7E8',
			maxColor: '#005645',
			stops: [
				[0, '#0074D9'],
				[0.3, '#3D9970'],
				[0.6, '#800000'],
				[1, '#fabebe']
			]
		},

		mapNavigation: {
			enabled: true,
			buttonOptions: {
				verticalAlign: 'bottom'
			}
		},

		plotOptions: {
			map: {
				states: {
					hover: {
						color: '#EEDD66'
					}
				}
			}
		},
		
		series: [{
			data: mapdata,
			name: 'PAK',
			dataLabels: {
				enabled: true,
				format: '{point.properties.map_label}<br>{point.value}'
			},
			tooltip: {
				pointFormat: '{point.properties.name}<br> Students Applied: {point.value}'
			}	
			
		}, {
			type: 'mapline',
			data: separators,
			color: 'silver',
			enableMouseTracking: false,
			animation: {
				duration: 500
			}
		}],
		
		drilldown: {
			activeDataLabelStyle: {
				color: '#FFFFFF',
				textDecoration: 'none',
				textOutline: '1px #000000'
			},
			drillUpButton: {
				relativeTo: 'spacingBox',
				position: {
					x: 0,
					y: 60
				}
			}
		}
		
    });
});

</script>
{{-- Dev Mannan: Custom code ends for maps --}}
@stop