@extends('layouts.app')
@section('content')
<div class="page-content row">
    <div class="page-content-wrapper m-t">
        <div class="sbox">
            <div class="sbox-title">
            <h1> Faculty: <b>{{$account->faculty->title }}</b>  / Type: <b>{{$account->bank_type }}</b> / Bank: <b>{{$account->bank_name }}</b> </h1>
            </div> <!-- Ending sbox-title --> 
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
            <div class="sbox-content "> 
                <div class="row">
                    <div class="col-sm-8 col-sm-offset-2">
                        @if ( $status )
                            <div class="alert alert-success" role="alert"> {{ $status }}</div>
                        @endif
                        <form action="{{route('edit-account')}}"  id="formfield" method="POST" class="quick-edit-form">
                            {{csrf_field()}}
                            <div class="form-group">
                                <input type="hidden" name="id" value="{{$account->id}}" class="form-control"/>
                                {{-- <input type="hidden" name="applicant" value="{{$application->fkApplicantId}}" class="form-control"/> --}}
                            </div>
                            <div class="form-group">
                                <label for="name">Admission Account</label>
                                <input type="number" required name="admission_account" class="form-control" value="{{$account->admission_account ?? ''}}"/>
                            </div>
                            <div class="form-group">
                                    <label for="name">Security Account</label>
                                    <input type="number" required name="security_account" class="form-control" value="{{$account->security_account ?? ''}}"/>
                            </div> 
                            <div class="form-group">
                                    <label for="name">Dues Account</label>
                                    <input type="number" required name="dues_account" class="form-control" value="{{$account->dues_account ?? ''}}"/>
                            </div>  
                            <div class="form-group">
                                    <label for="name">Status</label>
                                    <select name="status" class="form-control select2">
                                        <option value="1"  {{ ($account->status == 1) ? 'selected' : ''}}>Active</option>
                                        <option value="0"  {{ ($account->status == 0) ? 'selected' : ''}}>In Active</option>
                                    </select>
                            </div>                                                            

                        </form>
                        <div class="form-group">
                                <input type="submit" name="submit" id="submitBtn" class="btn btn-primary" value="Save Changes" data-toggle="modal" data-target="#confirm-submit"/>
                        </div>


                        <div class="modal fade" id="confirm-submit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        Confirm Submit
                                    </div>
                                    <div class="modal-body">
                                        Are you sure you want to submit details?
                                        </div>
                        
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                        <a href="#" id="submit" class="btn btn-success success">Submit</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> <!-- Ending row --> 
            </div> <!-- Ending sbox-content - 18 --> 
        </div> <!-- Ending sbox - 14 --> 
    </div> <!-- Ending page-content-wrapper --> 
</div> <!-- Ending page-content --> 
@stop