@extends('layouts.app')
@section('content')
    <div class="page-content-wrapper m-t">
        <div class="sbox">
            <div class="sbox-title">
                <h1> Faculty Accounts</h1>
            </div> <!-- Ending sbox-title --> 
            <div class="sbox-content "> 


<div class="table-responsive" style="min-height:300px;">
<table id="example" class="table table-striped table-bordered dt-responsive nowrap">
    <thead class="table-head">
        <tr>
            <th>Faculty</th>            
            <th>Type</th>
            <th> Bank </th>
            <th>Admission Account</th>
            <th>Security Account</th>
            <th>Dues Account</th>
            <th>Status</th>
            <th> Edit </th>
        </tr>
    </thead>
    <tbody>
        @foreach ( $result as $post )
            <tr>
            <td>{{$post->faculty->title}}</td>
            <td>{{$post->bank_type}}</td>
            <td>{{$post->bank_name}}</td>
            <td>{{$post->admission_account}}</td>
            <td>{{$post->security_account}}</td>
            <td>{{$post->dues_account}}</td>
            <td>{{($post->status == 1) ? 'Active' : 'In Active'}}</td>
            <td><a href={{ route('edit-account', ['id' => $post->id ]) }} target="_blank" class="btn btn-sm btn-primary">Edit</a></td>

            </tr>
        @endforeach
    </tbody>
    </table>
</div>



            </div> <!-- Ending sbox-content - 18 --> 
        </div> <!-- Ending sbox - 14 --> 
    </div>

<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap.min.js"></script>

<script>
        $(document).ready(function() {
            $('#example').DataTable( {
            "paging":   false,
            "bInfo" : false
            } );
        });
</script>
@stop
