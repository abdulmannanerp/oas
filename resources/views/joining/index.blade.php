@extends('layouts.app')

@section('content')
    <div class="page-content row">
        <div class="page-content-wrapper m-t">
            @if ( $status ) 
                <div class="alert alert-success mt5 mb5">
                    {{ $status }}
                </div>
            @endif
            <form action="{{route('joining')}}" method="POST">
                {{ csrf_field() }}
                <div class="form-group">
                        <label for="form-number">Enter Form Numbers</label>
                        <small>Multiple form numbers should be sperated by comma</small>
                        <textarea name="form-numbers"
                            id="form-number"
                            class="form-control"
                            placeholder="171005,758000,875520"
                            required></textarea>
                </div>
                <div class="form-group">
                    <input type="submit" class="btn btn-primary" value="Search" name="search">
                </div>
            </form>

            @if ($applications)
                @php $applicationIds = []; @endphp
                <table class="table">
                    <thead class="table-head table-bordered">
                        <tr>
                            <th>Form#</th>
                            <th>Name</th>
                            <th>Program</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach ($applications as $application)
                        <tr>
                            <td> {{ $application->id }}</td>
                            <td> {{ $application->applicant->name ?? ''}}</td>
                            <td> {{ $application->program->title ?? ''}}</td>
                        </tr>
                        @php $applicationIds[] = $application->id; @endphp
                    @endforeach
                    </tbody>
                </table>
                <form action="{{ route('confirmJoining') }}" method="POST">
                    <input type="hidden" value="{{ serialize($applicationIds)}}" name="application-ids">
                    <input type="submit" value="Confirm Joining" class="btn btn-sm btn-primary">
                </form>
            @endif
        </div>
    </div>
@stop
