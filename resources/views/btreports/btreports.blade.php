@extends('layouts.app')
@section('content')
<div class="page-content row">
    <div class="page-content-wrapper m-t">
        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
        
        @if ($find_us)
            <div class="sbox">
                <div class="sbox-title">
                    <h1> How did you find us Stats <small> </small></h1>
                </div>
                <div class="sbox-content">
                    <?php
                        foreach($find_us as $fs){
                            echo '<b>'.$fs->find_us. '</b> = '. $fs->total. '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'; 
                        }
                    ?>
                </div>
            </div>
        @endif
        <?php 
        
        ?>
        @if ($distinctBs)
            <div class="sbox">
                <div class="sbox-title">
                    <h1> All BS & LLB (Hons) 4 & 5 years distinct and verified count <small> </small></h1>
                </div>
                <div class="sbox-content">
                    {{count($distinctBs)}}
                </div>
            </div>
        @endif
        @if ($distinctBba)
            <div class="sbox">
                <div class="sbox-title">
                    <h1> BBA (2 years) distinct and verified count <small> </small></h1>
                </div>
                <div class="sbox-content">
                    {{count($distinctBba)}}
                </div>
            </div>
        @endif
        @if ($distinctMs)
            <div class="sbox">
                <div class="sbox-title">
                    <h1> MS/LLM/PMDC/MBA 2 years distinct and verified count <small> </small></h1>
                </div>
                <div class="sbox-content">
                    {{count($distinctMs)}}
                </div>
            </div>
        @endif
        @if ($distinctPhd)
            <div class="sbox">
                <div class="sbox-title">
                    <h1> PhD 3 years distinct and verified count <small> </small></h1>
                </div>
                <div class="sbox-content">
                    {{count($distinctPhd)}}
                </div>
            </div>
        @endif
        <?php 
        
        ?>
        <div class="sbox">
            <div class="sbox-title">
                <h1> Faculty / Department / Programs Stats <small> </small></h1>
                <button class="btn btn-default top-btn print-link print-hiding">Print</button>
            </div>
            <div class="sbox-content "> 
                <?php /* ?>
                <div class="container">
                    <form class="form-inline"  method="POST" action="{{route(\Request::route()->getName())}}">
                        {{ csrf_field() }}
                        <div class="form-group tp-pr-fr">
                            <label for="stats">Stats:</label>
                            <select class="form-control" id="stats" name="stats">
                                <option value="DESC" {{($request->stats == 'DESC')? 'selected="selected"': ''}}>Top</option>
                                <option value="ASC" {{($request->stats == 'ASC')? 'selected="selected"': ''}}>Bottom</option>

                            </select>
                        </div>
                        <div class="form-group tp-pr-se">
                            <label for="stats">Programs:</label>
                            <input class="form-control"  min="1" id="number" name="number" type="number" value="{{($request->number)? $request->number: '5'}}">
                        </div>
                        <div class="form-group tp-pr-th">
                            <label for="gender">Gender:</label>

                            <select class="form-control" id="gender" name="gender">
                                <option value="1" {{($request->gender == '1')? 'selected="selected"' : ''}}>Male & Female (Both)</option>
                                {{-- <option value="2"{{($request->gender == '2')? 'selected="selected"' : ''}}>Female</option>
                                <option value="3"{{($request->gender == '3')? 'selected="selected"' : ''}}>Male</option> --}}
                            </select>

                        </div>
                        <button type="submit" class="btn btn-default top-btn print-hiding">Submit</button>
                    </form>
                </div>
                <?php */ ?>
                {{-- Dev Mannan: Revised Code Starts --}}
                @if($total_applications)
                <table class="example btreport-head row-border" style="width:100%; border-collapse: collapse !important;">
                    <thead class="tbl-bg-clr">
                        <tr>
                            <th>Sr#.</th>
                            <th>Faculty</th>
                            <th class="lft-algn">Department</th>
                            <th class="lft-algn">Programs</th>
                            <th class="ml">Male</th>
                            <th class="fl">Female</th> 
                            <th>Total Appllicants</th>
                        </tr>
                    </thead>
                    <tfoot>
                            <tr>
                              <th>Sr#.</th>
                              <th>Faculty</th>
                              <th>Department</th>
                              <th>Programs</th>
                              <th>Male</th>
                              <th>Female</th>
                              <th>Total Applications</th>
                            </tr>
                          </tfoot>
                   <tbody>
                       @php $fac = ''; $dep = ''; @endphp
                        @foreach ( $total_applications as $ap )
                        @php 
                        $male_object = $applications_male->filter(function($value, $key) use($ap){
                            return $value->fkProgramId == $ap->fkProgramId;
                        })->first();
                        $female_object = $applications_female->filter(function($value, $key) use($ap){
                            return $value->fkProgramId == $ap->fkProgramId;
                        })->first();
                        if($male_object){
                            $tot_male = $male_object->total;
                        }else{
                            $tot_male = 0;
                        }
                        if($female_object){
                            $tot_female = $female_object->total;
                        }else{
                            $tot_female = 0;
                        }
                        @endphp
                        <tr id="{{$fac != $ap->faculty->title ? 'faculty-cls' : ''}}">
                            <td>{{ $loop->iteration }}</td>
                            <td>
                                {{$fac != $ap->faculty->title ? $ap->faculty->title : ''}}
                                @if(\Request::route()->getName() == 'btreports' && $fac != $ap->faculty->title)
                                    @if($ap->faculty->pkFacId == 1)
                                        {!! '<br />' !!} 
                                        {{'Male Academia=  22'}}
                                        {!! '<br />' !!} 
                                        {{'Female Academia=  19'}}
                                    @endif
                                    @if($ap->faculty->pkFacId == 8)
                                        {!! '<br />' !!} 
                                        {{'Male Academia=  26'}}
                                        {!! '<br />' !!} 
                                        {{'Female Academia=  15'}}
                                    @endif
                                    @if($ap->faculty->pkFacId == 9)
                                        {!! '<br />' !!} 
                                        {{'Male Academia=  27'}}
                                        {!! '<br />' !!} 
                                        {{'Female Academia=  22'}}
                                    @endif
                                @endif
                            </td>
                            <td class="lft-algn">
                                {{$dep != $ap->department->title ? $ap->department->title : ''}}
                            @if(\Request::route()->getName() == 'btreports' && $dep != $ap->department->title)
                                @if($ap->department->academia_male != '')
                                    {!! '<br />' !!} 
                                    {{'Male Academia= '.$ap->department->academia_male}}
                                @endif
                                @if($ap->department->academia_female != '')
                                    {!! '<br />' !!} 
                                    {{'Female Academia= '.$ap->department->academia_female }}
                                @endif
                            @endif
                            </td>
                            <td class="lft-algn">{{$ap->program->title}}</td>
                            <td class="ml">{{$tot_male}}</td>
                            <td class="fl">{{$tot_female}}</td> 
                            <td>{{$ap->total}}</td>
                        </tr>
                        @php $fac = $ap->faculty->title; $dep = $ap->department->title; @endphp
                        @endforeach
                   </tbody>
                </table>
                @endif
                {{-- Dev Mannan: Revised Code Ends --}}
                <?php /* ?>
                @if($maleapplications || $femaleapplications)
                @if($applications)

                <?php $i = 1; $t_male = 0; $t_female = 0; $grand_total = 0; $male_acad = 0; $female_acad = 0;?>
                <table class="example btreport-head" style="width:100%">
                    <thead class="tbl-bg-clr">
                        <tr>
                            <th>Sr#.</th>
                            <th>Faculty</th>
                            <th class="lft-algn">Department</th>
                            <?php 
                            if(\Request::route()->getName() == 'btreports'){
                            ?>
                            <th class="lft-algn">Male Academic</th>
                            <th class="lft-algn">Female Academic</th>
                            <?php } ?>
                            <th class="lft-algn">Programs</th>
                            <th class="ml">Male</th>
                            <th class="fl">Female</th> 
                            <th>Total Appllicants</th>
                        </tr>
                    </thead>
                    <tfoot>
                            <tr>
                              <th>Sr#.</th>
                              <th>Faculty</th>
                              <th>Department</th>
                              <?php 
                              if(\Request::route()->getName() == 'btreports'){
                              ?>
                              <th>Male Academia</th>
                              <th>Female Academia</th>
                              <?php } ?>
                              <th>Programs</th>
                              <th>Male</th>
                              <th>Female</th>
                              <th>Total Applications</th>
                            </tr>
                          </tfoot>
                   <tbody>
                    @foreach ( $applications as $ap )
                    <?php
                    $totalmale = 0;
                    $totalfemale = 0;
                    foreach ($maleapplications as $m) {
                        if ($ap->program == '') {
                            break;
                        }
                        if ($m->program == '') {
                            continue;
                        }

                        if ($ap->program == $m->program) {
                            $totalmale = $m->totalapplicants;
                            break;
                        }
                    }
                    foreach ($femaleapplications as $f) {
                        if ($ap->program == '') {
                            break;
                        }
                        if ($f->program == '') {
                            continue;
                        }
                        if ($ap->program == $f->program) {
                            $totalfemale = $f->totalapplicants;
                            break;
                        }
                    }
                    if(is_numeric($totalmale)){
                        $t_male += $totalmale;
                    }
                    if(is_numeric($totalfemale)){
                        $t_female += $totalfemale;
                    }
                    if(is_numeric($ap->totalapplicants)){
                        $grand_total += $ap->totalapplicants;
                    }
                    ?>
                    <tr class="">
                        <td><?= $i ?></td>
                        <td>{{$ap->faculty ?? 'N/A' }}</td>
                        <td class="lft-algn">{{$ap->department ?? 'N/A' }}</td>
                        <?php 
                        if(\Request::route()->getName() == 'btreports'){
                        ?>
                        <td class="lft-algn">{{$ap->academia_male ?? 'N/A' }}</td>
                        <td class="lft-algn">{{$ap->academia_female ?? 'N/A' }}</td>
                        <?php } ?>
                        <td class="lft-algn">{{$ap->program ?? 0 }}</td>
                        <td class="ml">{{$totalmale ?? 0 }}</td>
                        <td class="fl">{{$totalfemale ?? 0}}</td> 
                        <td>{{$ap->totalapplicants ?? 0}}</td>
                    </tr>
                    <?php $i++;?>
                    @endforeach
                   </tbody>
                </table>
                @endif
                @else
                @if($applications)
                <?php $i = 1; ?>
                <table class="example btreport-head" style="width:100%">
                    <thead class="tbl-bg-clr"><tr class=""><th >Sr#.</th><th>Programs</th><th class="lft-algn">Department</th><th class="lft-algn">Faculty</th> <th>Total Appllicants(Fee Verified)</th></tr></thead>
                    <tbody>
                    @foreach ( $applications as $ap )
                    <tr class=""><td><?= $i ?></td><td>{{$ap->program ?? 'N/A' }}</td><td class="lft-algn">{{$ap->department ?? 'N/A' }}</td><td class="lft-algn">{{$ap->faculty ?? 'N/A' }}</td> <td>{{$ap->totalapplicants ?? 'N/A'}}</td></tr>
                    <?php $i++; ?>
                    @endforeach
                    </tbody>
                </table>
                @endif
                @endif
                <?php */ ?>



            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap.min.js"></script>
<link href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet">
<script>
    $(document).ready(function() {
            var t = $('.example').DataTable( {
            "paging":   false,
            "bInfo" : false,
            "bSort" : false,
            "footerCallback": function (row, data, start, end, display) {
                var api = this.api(),
                intVal = function (i) {
                    return typeof i === 'string' ?
                        i.replace(/[, Rs]|(\.\d{2})/g,"")* 1 :
                        typeof i === 'number' ?
                        i : 0;
                },
                total4 = api.column(4).data().reduce(function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0);
                $(api.column(4).footer()).html(total4);
                total5 = api.column(5).data().reduce(function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0);
                $(api.column(5).footer()).html(total5);
                total6 = api.column(6).data().reduce(function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0);
                $(api.column(6).footer()).html(total6);
            }
            } );
            t.on( 'order.dt search.dt', function () {
            t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                cell.innerHTML = i+1;
            } );
        } ).draw();
    
    } );
    </script>
@stop