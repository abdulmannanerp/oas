@extends('layouts.app')
@section('content')
<div class="page-content row">
  <div class="page-content-wrapper m-t">
    @if ($errors->any())
    <div class="alert alert-danger">
      <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}
        </li>
        @endforeach
      </ul>
    </div>
    @endif
    <div class="sbox">
      <div class="sbox-title">
        <h1> Department Stats 
          <small> 
          </small>
        </h1>
        <button class="btn btn-default top-btn flt-rgt department-print print-hiding">Print</button>
      </div>
      <div class="sbox-content"> 
        <table id="example" class="table table-striped table-bordered dt-responsive dept-print-stats" style="width:100%">
            <thead class="tbl-bg-clr">
                <tr>
                    <th>Sr#</th>
                    <th>Department</th>
                    <th>Faculty</th>
                    <th>Male</th>
                    <th>Female</th> 
                    <th>Total Appllicants</th>
                    @if(\Request::route()->getName() == 'dpstats-acad')
                      <th>Male Academic</th>
                      <th>Female Academic</th>
                    @endif
                </tr>
            </thead>
            <tbody>
                {{-- Department of Environmental Sciences  --}}
          @foreach( $maleDepartmentApplicants as $maledept ) 
          <tr>
            <td align="center"></td>
            <td class="dpstat-sm">{{$maledept->title}}</td>
            <td class="dpstat-sm">{{$maledept->facti}}</td>
            <td align="center">{{$maledept->Male_Applicants}}</td>
            <?php 
            $female_dept = 0;
            foreach($femaleDepartmentApplicants as $femaledept){
                if ($maledept->title == $femaledept->title){
                    $female_dept=$femaledept->Female_Applicants;
                    $academics_female = $femaledept->academia_female;
                    break;
                }
            }
            ?>
            <td align="center">{{$female_dept}}
            </td>
            <td align="center">{{$maledept->Male_Applicants + $female_dept}}
            </td>
            @if(\Request::route()->getName() == 'dpstats-acad')
              <td align="center">{{$maledept->academia_male}}</td>
              <td align="center">{{$academics_female}}</td>
            @endif
          </tr>
          @endforeach
        </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap.min.js"></script>
<script>
    $(document).ready(function() {
        var t = $('#example').DataTable( {
        "paging":   false,
        "bInfo" : false
        } );
        t.on( 'order.dt search.dt', function () {
        t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
        } );
    } ).draw();
    
} );
    </script>
@stop
