

		 {!! Form::open(array('url'=>'program/savepublic', 'class'=>'form-horizontal','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}

	@if(Session::has('messagetext'))
	  
		   {!! Session::get('messagetext') !!}
	   
	@endif
	<ul class="parsley-error-list">
		@foreach($errors->all() as $error)
			<li>{{ $error }}</li>
		@endforeach
	</ul>		


<div class="col-md-12">
						<fieldset><legend> Program</legend>
				{!! Form::hidden('pkProgId', $row['pkProgId']) !!}					
									  <div class="form-group  " >
										<label for="Faculty" class=" control-label col-md-4 text-left"> Faculty </label>
										<div class="col-md-6">
										  <select name='fkFacId' rows='5' id='fkFacId' class='select2 '   ></select> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Department" class=" control-label col-md-4 text-left"> Department </label>
										<div class="col-md-6">
										  <select name='fkDepId' rows='5' id='fkDepId' class='select2 '   ></select> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Prerequisites" class=" control-label col-md-4 text-left"> Prerequisites </label>
										<div class="col-md-6">
										  <select name='fkReqId' rows='5' id='fkReqId' class='select2 '   ></select> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Program Level" class=" control-label col-md-4 text-left"> Program Level </label>
										<div class="col-md-6">
										  <select name='fkLeveliId' rows='5' id='fkLeveliId' class='select2 '   ></select> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Gender" class=" control-label col-md-4 text-left"> Gender </label>
										<div class="col-md-6">
										  <select name='fkGenderId' rows='5' id='fkGenderId' class='select2 '   ></select> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Title" class=" control-label col-md-4 text-left"> Title </label>
										<div class="col-md-6">
										  <input  type='text' name='title' id='title' value='{{ $row['title'] }}' 
						     class='form-control input-sm ' /> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Abbreviation" class=" control-label col-md-4 text-left"> Abbreviation </label>
										<div class="col-md-6">
										  <input  type='text' name='abbrev' id='abbrev' value='{{ $row['abbrev'] }}' 
						     class='form-control input-sm ' /> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
										</div> 	
										 <!-- Dev Mannan: Custom Code Starts -->					
										 <div class="form-group  " >
											<label for="Duration" class=" control-label col-md-4 text-left"> Duration </label>
											<div class="col-md-6">
													<input step="0.01" type='number' min="0" name='duration' id='duration' value='{{ $row['duration'] }}' 
																	class='form-control input-sm ' /> 
											</div> 
											<div class="col-md-2">

											</div>
									</div>
									<div class="form-group  " >
										<label for="Fee" class=" control-label col-md-4 text-left"> Fee </label>
										<div class="col-md-6">
												<input  type='number' min="0" name='fee' id='fee' value='{{ $row['fee'] }}' 
																class='form-control input-sm ' /> 
										</div> 
										<div class="col-md-2">

										</div>
									</div>
									<div class="form-group  " >
										<label for="Description" class=" control-label col-md-4 text-left"> Description </label>
										<div class="col-md-6">
														<textarea name='description' rows='5' id='description' class='form-control input-sm '  
														>{{ $row['description'] }}</textarea>  
										</div> 
										<div class="col-md-2">
										</div>
									</div>
									<!-- Dev Mannan: Custom Code Ends-->				
									  <div class="form-group  " >
										<label for="Status" class=" control-label col-md-4 text-left"> Status </label>
										<div class="col-md-6">
										  
					
					<input type='radio' name='status' value ='1'  @if($row['status'] == '1') checked="checked" @endif class='minimal-red' > Active 
					
					<input type='radio' name='status' value ='0'  @if($row['status'] == '0') checked="checked" @endif class='minimal-red' > Deactive  
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Modified" class=" control-label col-md-4 text-left"> Modified </label>
										<div class="col-md-6">
										  
				<div class="input-group m-b" style="width:150px !important;">
					{!! Form::text('modified', $row['modified'],array('class'=>'form-control input-sm date')) !!}
					<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
				</div> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="ModifiedBy User" class=" control-label col-md-4 text-left"> ModifiedBy User </label>
										<div class="col-md-6">
										  <input  type='text' name='modifiedBy' id='modifiedBy' value='{{ $row['modifiedBy'] }}' 
						     class='form-control input-sm ' /> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="DateIn" class=" control-label col-md-4 text-left"> DateIn </label>
										<div class="col-md-6">
										  
				<div class="input-group m-b" style="width:150px !important;">
					{!! Form::text('dateIn', $row['dateIn'],array('class'=>'form-control input-sm date')) !!}
					<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
				</div> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Required Percentage" class=" control-label col-md-4 text-left"> Required Percentage </label>
										<div class="col-md-6">
										  <input  type='text' name='reqPercentage' id='reqPercentage' value='{{ $row['reqPercentage'] }}' 
						     class='form-control input-sm ' /> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Required CGPA" class=" control-label col-md-4 text-left"> Required CGPA </label>
										<div class="col-md-6">
										  <input  type='text' name='reqCGPA' id='reqCGPA' value='{{ $row['reqCGPA'] }}' 
						     class='form-control input-sm ' /> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Requirements" class=" control-label col-md-4 text-left"> Requirements </label>
										<div class="col-md-6">
										  <textarea name='requirements' rows='5' id='requirements' class='form-control input-sm '  
				           >{{ $row['requirements'] }}</textarea> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> </fieldset>
			</div>
			
			

			<div style="clear:both"></div>	
				
					
				  <div class="form-group">
					<label class="col-sm-4 text-right">&nbsp;</label>
					<div class="col-sm-8">	
					<button type="submit" name="apply" class="btn btn-info btn-sm" ><i class="fa  fa-check-circle"></i> {{ Lang::get('core.sb_apply') }}</button>
					<button type="submit" name="submit" class="btn btn-primary btn-sm" ><i class="fa  fa-save "></i> {{ Lang::get('core.sb_save') }}</button>
				  </div>	  
			
		</div> 
		 <input type="hidden" name="action_task" value="public" />
		 {!! Form::close() !!}
		 
   <script type="text/javascript">
	$(document).ready(function() { 
		
		
		$("#fkFacId").jCombo("{!! url('program/comboselect?filter=tbl_oas_faculty:pkFacId:title') !!}",
		{  selected_value : '{{ $row["fkFacId"] }}' });
		
		$("#fkDepId").jCombo("{!! url('program/comboselect?filter=tbl_oas_department:pkDeptId:title') !!}&parent=fkFacId:",
		{  parent: '#fkFacId', selected_value : '{{ $row["fkDepId"] }}' });
		
		$("#fkReqId").jCombo("{!! url('program/comboselect?filter=tbl_oas_prerequisite:pkReqId:requisite') !!}",
		{  selected_value : '{{ $row["fkReqId"] }}' });
		
		$("#fkLeveliId").jCombo("{!! url('program/comboselect?filter=tbl_oas_prog_level:pkLevelId:programLevel') !!}",
		{  selected_value : '{{ $row["fkLeveliId"] }}' });
		
		$("#fkGenderId").jCombo("{!! url('program/comboselect?filter=tbl_oas_gender:pkGenderId:gender') !!}",
		{  selected_value : '{{ $row["fkGenderId"] }}' });
		 

		$('.removeCurrentFiles').on('click',function(){
			var removeUrl = $(this).attr('href');
			$.get(removeUrl,function(response){});
			$(this).parent('div').empty();	
			return false;
		});		
		
	});
	</script>		 
