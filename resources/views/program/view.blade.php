@extends('layouts.app')

@section('content')
<section class="page-header row">
	<h2> {{ $pageTitle }} <small> {{ $pageNote }} </small></h2>
	<ol class="breadcrumb">
		<li><a href="{{ url('') }}"> Dashboard </a></li>
		<li><a href="{{ url($pageModule) }}"> {{ $pageTitle }} </a></li>
		<li class="active"> View  </li>		
	</ol>
</section>
<div class="page-content row">
	<div class="page-content-wrapper no-margin">
	
	<div class="sbox">
		<div class="sbox-title clearfix">
			<div class="sbox-tools pull-left" >
		   		<a href="{{ ($prevnext['prev'] != '' ? url('program/'.$prevnext['prev'].'?return='.$return ) : '#') }}" class="tips btn btn-sm"><i class="fa fa-arrow-left"></i>  </a>	
				<a href="{{ ($prevnext['next'] != '' ? url('program/'.$prevnext['next'].'?return='.$return ) : '#') }}" class="tips btn btn-sm "> <i class="fa fa-arrow-right"></i>  </a>					
			</div>	

			<div class="sbox-tools" >
				@if($access['is_add'] ==1)
		   		<a href="{{ url('program/'.$id.'/edit?return='.$return) }}" class="tips btn btn-sm  " title="{{ __('core.btn_edit') }}"><i class="fa  fa-pencil"></i></a>
				@endif
				<a href="{{ url('program?return='.$return) }}" class="tips btn btn-sm  " title="{{ __('core.btn_back') }}"><i class="fa  fa-times"></i></a>		
			</div>
		</div>
		<div class="sbox-content">
			<div class="table-responsive">
				<table class="table table-striped " >
					<tbody>	
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('PkProgId', (isset($fields['pkProgId']['language'])? $fields['pkProgId']['language'] : array())) }}</td>
						<td>{{ $row->pkProgId}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Title', (isset($fields['title']['language'])? $fields['title']['language'] : array())) }}</td>
						<td>{{ $row->title}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Abbreviation', (isset($fields['abbrev']['language'])? $fields['abbrev']['language'] : array())) }}</td>
						<td>{{ $row->abbrev}} </td>
						
					</tr>

					<!-- Dev Mannan: Custom Code Starts -->

					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Duration', (isset($fields['duration']['language'])? $fields['duration']['language'] : array())) }}</td>
						<td>{{ $row->duration}} </td>
						
					</tr>
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Fee', (isset($fields['fee']['language'])? $fields['fee']['language'] : array())) }}</td>
						<td>{{ $row->fee}} </td>
						
					</tr>
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Description', (isset($fields['description']['language'])? $fields['description']['language'] : array())) }}</td>
						<td>{{ $row->description}} </td>
					</tr>

					<!-- Dev Mannan: Custom Code Ends -->
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Offered', (isset($fields['fkGenderId']['language'])? $fields['fkGenderId']['language'] : array())) }}</td>
						<td>{{ SiteHelpers::formatLookUp($row->fkGenderId,'fkGenderId','1:tbl_oas_gender:pkGenderId:gender') }} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Active', (isset($fields['status']['language'])? $fields['status']['language'] : array())) }}</td>
						<td>{{ SiteHelpers::userStatus($row->status) }} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Faculty', (isset($fields['fkFacId']['language'])? $fields['fkFacId']['language'] : array())) }}</td>
						<td>{{ SiteHelpers::formatLookUp($row->fkFacId,'fkFacId','1:tbl_oas_faculty:pkFacId:title') }} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Department', (isset($fields['fkDepId']['language'])? $fields['fkDepId']['language'] : array())) }}</td>
						<td>{{ SiteHelpers::formatLookUp($row->fkDepId,'fkDepId','1:tbl_oas_department:pkDeptId:title') }} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Prerequisites', (isset($fields['fkReqId']['language'])? $fields['fkReqId']['language'] : array())) }}</td>
						<td>{{ SiteHelpers::formatLookUp($row->fkReqId,'fkReqId','1:tbl_oas_prerequisite:pkReqId:requisite') }} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Program Level', (isset($fields['fkLeveliId']['language'])? $fields['fkLeveliId']['language'] : array())) }}</td>
						<td>{{ SiteHelpers::formatLookUp($row->fkLeveliId,'fkLeveliId','1:tbl_oas_prog_level:pkLevelId:programLevel') }} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Modified Date', (isset($fields['modified']['language'])? $fields['modified']['language'] : array())) }}</td>
						<td>{{ date('',strtotime($row->modified)) }} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('ModifiedBy User', (isset($fields['modifiedBy']['language'])? $fields['modifiedBy']['language'] : array())) }}</td>
						<td>{{ $row->modifiedBy}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('DateIn', (isset($fields['dateIn']['language'])? $fields['dateIn']['language'] : array())) }}</td>
						<td>{{ date('',strtotime($row->dateIn)) }} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Required Percentage', (isset($fields['reqPercentage']['language'])? $fields['reqPercentage']['language'] : array())) }}</td>
						<td>{{ $row->reqPercentage}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Required CGPA', (isset($fields['reqCGPA']['language'])? $fields['reqCGPA']['language'] : array())) }}</td>
						<td>{{ $row->reqCGPA}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Requirements', (isset($fields['requirements']['language'])? $fields['requirements']['language'] : array())) }}</td>
						<td>{{ $row->requirements}} </td>
						
					</tr>
				
					</tbody>	
				</table>   

			 	

			</div>
		</div>
	</div>
	</div>
</div>
@stop
