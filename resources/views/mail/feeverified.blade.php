<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <h4>Dear {{$name}},</h4>
    <p>Your Fee for {{ $program ?? '' }} has been verified for the online application and soon you will receive an email to download the Roll No Slip.</p>
    <br><br>
    <p>Regards,</p>
    <p>Admission Office</p>
    <p>International Islamic University, Islamabad (IIUI)</p>
    <a href="http://admission.iiu.edu.pk/">http://admission.iiu.edu.pk/</a>
</body>
</html>
