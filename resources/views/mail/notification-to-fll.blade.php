<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Documents Required For FET Programs Only</title>
</head>
<body>
    <p>Dear Applicant,</p>
    <p>
        Please share a scanned copy of HSSC Part-1 at coordinator.fet@iiu.edu.pk, If you've applied in one of the following programs.
    </p>
    <ul>
        <li>
            BS Mechanical Engineering
        </li>
        <li>
            BS Electrical Engineering
        </li>
        <li>
            BSc Mechanical Engineering Technology  
        </li>
        <li>
            BSc Electrical Engineering Technology
        </li>
        <li>
            BSc Civil Engineering Technology
        </li>
    </ul>
    <p>
        Students who have applied in other programs are not required to submit any document. 
    </p>
    <br><br>
    <p>Regards,</p>
    <p>Admission Office</p>
    <p>International Islamic University, Islamabad (IIUI)</p>
    <a href="http://admission.iiu.edu.pk/">http://admission.iiu.edu.pk/</a>
</body>
</html>