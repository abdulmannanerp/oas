<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Documents Requested</title>
</head>
<body>
    <p>Dear Applicant</p>
    <p>Asalamoalikom</p>
    <p>
        You are advised to email a scanned copy of your result of HSSC Part-1 at coordinator.fet@iiu.edu.pk immediately but not later than August 18, 2020. Please mention your name, father name and IIU Roll Number.
    </p>
    <br><br>
    <p>Regards,</p>
    <p>Admission Office</p>
    <p>International Islamic University, Islamabad (IIUI)</p>
    <a href="http://admission.iiu.edu.pk/">http://admission.iiu.edu.pk/</a>
</body>
</html>