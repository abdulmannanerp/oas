<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <p> Dear {{ $teacher->name }}, </p>
    <p>
        AOA, We hope and pray that you all would be hale and haughty by the grace of Allah SWT. As we all know, HEC has decided to launch online classes using LMS by using all relevant and supporting tools/techniques so involved in its adoption.  IIUI has also resolved to adhere to the policy guidelines of HEC in true letter and spirit. Preparation to this effect on part of the university authorities and faculties are under way with full zeal and zest.
    </p>
    <p>Your course are now available online in IIU LMS and can be accessed via the URLs given below:</p>
    <div class="courses">
        <ol>
        @foreach($teacher->courses as $course)
            <li>
                {{ $course->coursecode }} - {{ $course->title }} ({{ $course->section.'_'.$course->batchcode}}) | <a href="{{ route('courseViewed', ['courseId' => $course->id]) }}">
                    {{ $course->g_course_url}}
                </a>
            </li>
        @endforeach
        </ol>
    </div>
    <p>
        IIU technical committee for the Online Learning Management System has designed and developed two model course e-folders at the following links (Only with IIUI email address you will be able to access these courses) that would hopefully help faculty members in preparation of their respective courses.
    </p>
    <ul>
        <li>
            <a href="https://drive.google.com/drive/folders/1WTR4WK6UkiMBPRLZAxv568dijQvYaZSu?usp=sharing">Data Warehousing and Data Mining</a>
        </li>
        <li>
            <a href="https://drive.google.com/drive/folders/195vfesoUuhgnmca0GgBdiO5yeH1lVIFi?usp=sharing">Consumer Behavior</a>
        </li>
    </ul>
    <p>These e-folders include</p>
    <ul>
        <li>Lecture slides / notes </li>
        <li>Audio/ video recording of the lecture</li>
        <li>Reference Material</li>
        <li>Student Activity</li>
    </ul>
    <p>
        All the above resources are required to be uploaded week wise. These model courses also include Course Folder Preparation Guidelines - brief guide describing the structure and contents of the e-folder  and Model Course Checklist - a checklist to keep track of the resources being uploaded for each course in the e-folder. These Model Courses are representing the actual template that is to be followed for all the courses. 
    </p>
    <p>
        It is now requested to upload the course content and activities in the online courses shared above on or before 10th May, 2020. After this deadline the audit of the online courses will be started. Upon receiving the course approval notification from the course audit committee you can start enrolling students in your respective courses. Enrollment of students before the course approval is not recommended.
    </p>
    <p>
        Hopefully, there is no ambiguity left now for the preparation of course materials. Rest our team members are always available to guide you and answer your queries on given below email addresses.
    </p>
    <ul>
        <li> Dr. Fouzia Ajmal, Asst. Professor, fouzia.ajmal@iiu.edu.pk </li>
        <li> Dr. Usman Nasir,  Asst. Professor,  usman.nasir@iiu.edu.pk </li>
        <li> Dr. Asma mansoor,  Asst. Professor, asma.mansoor@iiu.edu.pk </li>
        <li> Dr. Zahid Mehmood, Deputy Director, zahid@iiu.edu.pk  </li>
        <li> Ms Nosheen Syed, Deputy Director, nosheen.syed@iiu.edu.pk </li>
    </ul>
    <p> May ALLAH keep us safe and end this pandemic situation as soon as possible. Ameen. </p>
    <p>
        Note: These e-folders of the courses have been reviewed and approved for sharing with all concerned specially with visiting faculty. It is as per the university guidelines issued by Director Academics.
    </p>
    <p>
        <strong>LMS Team, IIU</strong>
    </p>
    <p>
        <img height="1px" width="1px" src="{{route('emailViewed', ['id' => $teacher->id, 'aljamiaId' => $teacher->teacher_id_aljamia])}}"/>
    </p>
</body>
</html>