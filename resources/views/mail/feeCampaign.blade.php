<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
</head>
<body>
	<h4>Dear {{$name}},</h4>
	<p>
	Kindly deposit fee to confirm your application for IIUI admission. Last date to apply / fee submission is July 20, 2018.  Please ignore if already paid.
	</p>
	<p>
	Kindly report for test along with Roll No Slip / submitted Fee Challan (if test entry slip is not received) as per schedule given on www.iiu.edu.pk
	</p>
	 
	<strong>Admission Office, IIUI</strong>
</body>
</html>