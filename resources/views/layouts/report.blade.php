@extends('layouts.app')


@section('content')
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">

<div class="page-content row">
    <div class="page-content-wrapper m-t">
        {{-- <div class="pull-right">
            <div class="form-group">
                <form name="semesterSelectorForm" id="semesterSelectorForm" action="{{route('getReportFromSpecificSemester', ['reportName' => 'faculty'])}}"  method="post">
                    <select class="form-control" name="semesterSelector" id="semesterSelector">
                        <option> --Select Semester-- </option>
                        @foreach ( $semesters as $semester )
                            <option value="{{ $semester->pkSemesterId }}" {{ ($semester->pkSemesterId == $selectedSemester ) ? 'selected' : '' }}> 
                                {{ $semester->title }} 
                            </option>
                        @endforeach
                    </select>
                </form>
            </div>
        </div> --}}
        <h3>Overall Report</h3>
            {{-- <h4>Programwise Report</h4> --}}
            <form action="{{ route('faculty') }}" method="POST">
                <input type="hidden" name="semesterSelector" id="semesterSelector" value="{{Cache::get(auth()->id().'-active_semester')}}">
                <div class="form-group">
                    <select name="faculty" id="faculty" required>
                        <option value="">--Select Faculty--</option>
                        @foreach ( $faculties as $faculty )
                            <option value="{{$faculty->pkFacId}}">{{$faculty->title}}</option>
                        @endforeach
                    </select>
                    <select name="department" id="department" required>
                        <option value="">--Select Department--</option>
                        @if ( isset ( $departments) && isset ( $selectedDepartment) )
                            @foreach ( $departments as $department )
                                @if ( $department->pkDeptId == $selectedDepartment )
                                    <option value={{$department->pkDeptId}} selected> {{$department->title}} </option>
                                @else
                                    <option value={{$department->pkDeptId}}> {{$department->title}} </option>
                                @endif 
                            @endforeach 
                        @endif 
                    </select>
                    <select name="program" id="programme">
                        <option value="">--Select Program--</option>
                        @if ( isset ( $programs) && isset ( $selectedProgram) )
                            @foreach ( $programs as $program )
                                @if ( $program->pkProgId == $selectedProgram )
                                    <option value={{$program->pkProgId}} selected> {{$program->title.  ' ' . $program->duration . ' Year(s)'}} </option>
                                @else
                                    <option value={{$program->pkProgId}}> {{$program->title.  ' ' . $program->duration . ' Year(s)'}} </option>
                                @endif 
                            @endforeach 
                        @endif
                    </select>

                    <select name="overseas" id="nationality-list">
                        <option value="">--Applicant Nationality--</option>
                        <option value="1">Pakistani</option>
                        <option value="2">OverSeas Pakistani</option>
                        <option value="3">Overseas</option>
                    </select>

                    <select name="showSelector" id="semester-list" disabled>
                        <option value=""> --Select Semester-- </option>
                        @foreach ( $semesters as $semester )
                            <option value="{{ $semester->pkSemesterId }}" {{ ($semester->pkSemesterId == Cache::get(auth()->id().'-active_semester')) ? 'selected' : ''}}> 
                                {{ $semester->title }} 
                            </option>
                        @endforeach
                    </select>
                    <input type="submit" value="Get Report" id="get-report-btn">
                </div>
            </form>

    @yield('report-content')
    

    </div> <!-- Ending page content wrapper --> 
    </div> <!-- Ending page content row --> 
    <script>
        $(function(){
            document.getElementById('faculty').value = '{{ request('faculty') }}';
            document.getElementById('nationality-list').value = '{{ request('overseas') }}';
            $('#faculty').on('change', function(){
               $.ajax({
                   method: 'post',
                   url: '{{ route('getDepartments') }}',
                   data: {
                       id: $(this).val()
                   },
                   success: function ( response ) {
                       $('#department').empty();
                       $('#department').append(
                            '<option value="">--Select Department--</option>'
                        );
                       $.each(response, function (key, value) {
                           $('#department').append(
                               '<option value="'+value.pkDeptId+'">'+ value.title+'</option>'
                           );
                       });
                   }
               });
            });

            $('#department').on('change', function(){
                $.ajax({
                    method: 'post',
                    url: '{{ route('getProgramme') }}',
                    data: {
                        id: $(this).val()
                    },
                    success: function (response) {
                        $('#programme').empty();
                        if ( response == '' ) {
                            $('#programme').append(
                                '<option value="">--Select Porgramme--</option>'
                            );
                            return;
                        }
                        $('#programme').append(
                            '<option value="">--Select Porgramme--</option>'
                        );
                        $.each(response, function (key, value) {
                            $('#programme').append(
                                '<option value="'+value.pkProgId+'">'+ value.title+ ' - ' + value.duration + ' Year(s)</option>'
                            );
                        });
                    }
                });
            });
        });
    </script>
@stop
