<?php $sidebar = SiteHelpers::menus('sidebar'); ?>
<div id="sidebar-navigation" class="side-cus-cls">
    <div class="logo">
        <a href="{{route('dashboard')}}">           
            @if(file_exists(public_path().'/uploads/images/'.config('sximo.cnf_logo') ) && config('sximo.cnf_logo') !='')
                <img src="{{ asset('uploads/images/'.config('sximo.cnf_logo')) }}" alt="{{ config('sximo.cnf_appname') }}"  />
            @else
                {{ config('sximo.cnf_appname')}} 
            @endif  
        </a>    
    </div>

    <div class="sidebar-collapse">
        <nav role="navigation" class="navbar-default ">
            <ul id="sidemenu" class="nav expanded-menu">
                     
                <!-- Dev Mannan: Custom Code starts -->
                <?php  $allowed=0; ?>
                <!-- Dev Mannan: Custom Code ends -->
                @foreach ($sidebar as $menu)
                <!-- Dev Mannan: Custom Code starts -->
                <?php
// dump($menu);
                $allowed_uri = $_SERVER['REQUEST_URI'];
                $url_record  = array(
                    "/admin/report",
                    "/admin/report/faculty",
                    "/admin/btreports",
                    "/admin/verify",
                    '/admin/verify/verified',
                    '/admin/verify/unverified',
                    '/admin/verify/failed',
                    '/admin/docs',
                    '/admin/applicants'
                );
// echo $menu['menu_type']; 
                if ($menu['menu_type'] == 'external' && in_array($allowed_uri, $url_record)) {

                    if ($allowed == 0) {

                        if (count($menu['childs']) > 0) {
                            foreach ($menu['childs'] as $menu2) {
                                if ($menu2['url'] == $allowed_uri) {
                                    $allowed = 1;
                                }
                            }
                        } else {
                            if ($menu['url'] == $allowed_uri) {
                                $allowed = 1;

                            }
                        }
                    }
                } else {
                    $allowed = 1;
                }
                ?>
                <!-- Dev Mannan: Custom Code ends -->


                <li @if(Request::segment(1) == $menu['module']) class="active" @endif>



                   @if($menu['module'] =='separator')

                   <li class="separator"> <span> {{$menu['menu_name']}} </span></li>



                   @else

                   <a data-toggle="tooltip" title="{{  $menu['menu_name'] }}" data-placement="right"

                   @if($menu['menu_type'] =='external')

                   href="{{ $menu['url'] }}" 

                   @else

                   href="{{ URL::to($menu['module'])}}" 

                   @endif              



                   @if(count($menu['childs']) > 0 ) class="expand level-closed" @endif>

                   <i class="{{$menu['menu_icons']}}"></i> 

                   <span class="nav-label">                    

                    {{ (isset($menu['menu_lang']['title'][session('lang')]) ? $menu['menu_lang']['title'][session('lang')] : $menu['menu_name']) }}

                </span> 

                @if(count($menu['childs']))<span class="fa arrow"></span> @endif    

            </a> 

            @endif  

            @if(count($menu['childs']) > 0)

            <ul class="nav nav-second-level">

                @foreach ($menu['childs'] as $menu2)

                <li @if(Request::segment(1) == $menu2['module']) class="active" @endif>

                   <a  

                   @if($menu2['menu_type'] =='external')

                   href="{{ $menu2['url']}}" 

                   @else

                   href="{{ URL::to($menu2['module'])}}"  

                   @endif                                  

                   >



                   <i class="{{$menu2['menu_icons']}}"></i>

                   {{ (isset($menu2['menu_lang']['title'][session('lang')]) ? $menu2['menu_lang']['title'][session('lang')] : $menu2['menu_name']) }}

               </a> 

               @if(count($menu2['childs']) > 0)

               <ul class="nav nav-third-level">

                @foreach($menu2['childs'] as $menu3)

                <li @if(Request::segment(1) == $menu3['module']) class="active" @endif>

                   <a 

                   @if($menu['menu_type'] =='external')

                   href="{{ $menu3['url'] }}" 

                   @else

                   href="{{ URL::to($menu3['module'])}}" 

                   @endif                                      



                   >



                   <i class="{{$menu3['menu_icons']}}"></i> 

                   {{ (isset($menu3['menu_lang']['title'][session('lang')]) ? $menu3['menu_lang']['title'][session('lang')] : $menu3['menu_name']) }}                                         



               </a>

           </li>   

           @endforeach

       </ul>

       @endif                          

   </li>                           

   @endforeach

</ul>

@endif

</li>

@endforeach
<!-- Dev Mannan: Custom Code starts -->
<?php
if ($allowed == 0) {
    echo '<div class="alert alert-danger">
    <strong>Permission Denied!</strong> You dont have permission to access this page
    </div>';
    /*
    ?>
    <script>window.location = "/user/logout";</script>
    <?php
    */
    exit();
}
// echo 'fine'. $allowed;
// exit();
?>
<script>
// alert('You dont have permission to access this page');
</script>
<!-- Dev Mannan: Custom Code ends -->

</ul>   

</nav>

</div>

</div>