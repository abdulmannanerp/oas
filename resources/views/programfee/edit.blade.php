@extends('layouts.app')
@section('content')
<div class="page-content row">
    <div class="page-content-wrapper m-t">
        <div class="sbox">
            <div class="sbox-title">
                <h1> Edit Program Fee Structure </h1>
            </div> <!-- Ending sbox-title --> 

            <div class="sbox-content "> 
                <div class="row">
                    <div class="col-md-12">
                        @if ( $status )
                            <div class="alert alert-success" role="alert"> {{ $status }}</div>
                        @endif
                        <form action="{{route('edit-fee')}}" method="POST" class="quick-edit-form">
                            {{csrf_field()}}
                            <div class="form-group">
                                <input type="hidden" name="id" value="{{ (isset($ProgramFee[0]['fkProgramId'])) ? $ProgramFee[0]['fkProgramId'] : '' }}" class="form-control"/>
                            </div>
                            <div class="form-group">
                                <label for="name">Program</label> 
                                <select name="program" class="form-control select2">
                                    @foreach ( $program as $program )
                                    <option disabled value="{{$program->pkProgId}}" @if ($ProgramFee){{ ($ProgramFee[0]['fkProgramId'] == $program->pkProgId) ? 'selected' : ''}}@endif>
                                        {{ $program->title }}
                                    </option>
                                    @endforeach
                                </select>
                            </div>
                            @foreach ( $ProgramFee as $pfs )
                            <div class="section-gr">
                            <div class="row">
                            <div class="col-md-6">
                            <div class="form-group">
                                <label for="name">Account Type</label>
                            <select name="fee_type-{{$pfs->id}}" class="form-control select2">
                                    <option value="dues"  {{  ($pfs->fee_type == 'dues') ? 'selected' : ''}}>dues</option>
                                    <option value="security"  {{ ($pfs->fee_type == 'security') ? 'selected' : ''}}>security</option>
                                </select>
                            </div>
                            </div>
                            <div class="col-md-6">
                            <div class="form-group">
                                <label for="name">Fee</label>
                                <input type="text" name="fee-{{$pfs->id}}" class="form-control" value="{{$pfs->fee ?? ''}}"/>
                            </div>
                            </div>
                            </div>
                            <div class="row">
                                    <div class="col-md-6">
                            <div class="form-group">
                                    <label for="name">Status</label>
                                    <select name="status-{{$pfs->id}}" class="form-control select2">
                                        <option value="1"  {{ ($pfs->status == 1) ? 'selected' : ''}}>Publish</option>
                                        <option value="0"  {{ ($pfs->status == 0) ? 'selected' : ''}}>Un publish</option>
                                    </select>
                            </div>
                            </div>
                            <div class="col-md-6">
                            <div class="form-group">
                                    <label for="name">Fee Head</label>
                                    <select name="fkFeeCode-{{$pfs->id}}" class="form-control select3">
                                        @foreach ( $FeeCode as $fc )
                                        <option value="{{$fc->id}}" {{ ($pfs->fkFeeCode == $fc->id) ? 'selected' : ''}}>
                                            {{ $fc->description }}
                                        </option>
                                        @endforeach
                                    </select>
                            </div>
                            </div>
                            </div>
                            </div>
                            @endforeach
                            <div class="form-group">
                                <input type="submit" name="submit" class="btn btn-primary" value="Save Changes"/>
                            </div>

                        </form>
                    </div>
                </div> <!-- Ending row --> 
            </div> <!-- Ending sbox-content - 18 --> 
        </div> <!-- Ending sbox - 14 --> 
    </div> <!-- Ending page-content-wrapper --> 
</div> <!-- Ending page-content --> 
@stop