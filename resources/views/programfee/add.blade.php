@extends('layouts.app')
@section('content')
<div class="page-content row">
    <div class="page-content-wrapper m-t">
        <div class="sbox">
            <div class="sbox-title">
                <h1> Program Fee Structure</h1>
            </div> <!-- Ending sbox-title --> 
            <div class="sbox-content "> 
                <div class="row">
                    <div class="col-md-12">
                        @if ( $status )
                            <div class="alert alert-success" role="alert"> {{ $status }}</div>
                        @endif
                        <form action="{{route('add-fee')}}" method="POST" class="quick-edit-form">
                            {{csrf_field()}}
                            <div class="form-group">
                                <input type="hidden" name="id" value="@if($ProgramFee){{$ProgramFee->id ?? ''}}@endif" class="form-control"/>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                        <div class="form-group">
                                                <label for="name">Program</label> 
                                                @if($ProgramFee)
                                                <input type="text" class="form-control" value="{{$ProgramFee->program->title}}" readonly="readonly">
                                                @else
                                                <select name="program" class="form-control select2">
                                                    @foreach ( $program as $program )
                                                    <option value="{{$program->pkProgId}}" @if($ProgramFee){{ ($program->pkProgId == $ProgramFee->fkProgramId) ? 'selected' : ''}}@endif>{{ $program->title.' -- '.$program->duration.' Year(s)' }}</option>
                                                    @endforeach
                                                </select>
                                                @endif
                                            </div>
                                </div>
                                <div class="col-md-6">
                                        <div class="form-group">
                                                <label for="name">Admission Fee</label>
                                                <input type="number" name="admission_fee" required class="form-control" value="@if($ProgramFee){{$ProgramFee->admission_fee ?? ''}}@endif"/>
                                            </div>
                                </div>
                            </div>

                            <div class="row">
                                    <div class="col-md-6">
                                            <div class="form-group">
                                                    <label for="name">University Dues</label>
                                                    <input type="number" name="university_dues" required class="form-control" value="@if($ProgramFee){{$ProgramFee->university_dues ?? ''}}@endif"/>
                                                </div>
                                    </div>
                                    <div class="col-md-6">
                                            <div class="form-group">
                                                    <label for="name">Library Security</label>
                                                    <input type="number" name="library_security" required class="form-control" value="@if($ProgramFee){{$ProgramFee->library_security ?? ''}}@endif"/>
                                            </div>
                                    </div>
                            </div>
                            <div class="row">
                                    <div class="col-md-6">
                                            <div class="form-group">
                                                    <label for="name">Book Bank Security</label>
                                                    <input type="number" name="book_bank_security" required class="form-control" value="@if($ProgramFee){{$ProgramFee->book_bank_security ?? ''}}@endif"/>
                                            </div>
                                    </div>
                                    <div class="col-md-6">
                                            <div class="form-group">
                                                    <label for="name">Caution Money</label>
                                                    <input type="number" name="caution_money" required class="form-control" value="@if($ProgramFee){{$ProgramFee->caution_money ?? ''}}@endif"/>
                                            </div>
                                    </div>
                            </div>    
                            <div class="row">
                                <div class="col-md-6">
                                        <div class="form-group">
                                                <label for="name">Hostel Dues</label>
                                                <input type="number" name="hostel_dues" required class="form-control" value="@if($ProgramFee){{$ProgramFee->hostel_dues ?? ''}}@endif"/>
                                        </div>
                                </div>
                                <div class="col-md-6">
                                        <div class="form-group">
                                                <label for="name">Hostel Security</label>
                                                <input type="number" name="hostel_security" required class="form-control" value="@if($ProgramFee){{$ProgramFee->hostel_security ?? ''}}@endif"/>
                                        </div>
                                </div>
                            </div>
                            <div class="row">
                                    <div class="col-md-6">
                                            <div class="form-group">
                                                    <label for="name">Dues Arriers</label>
                                                    <input type="number" name="dues_arriers" required class="form-control" value="@if($ProgramFee){{$ProgramFee->dues_arriers ?? ''}}@endif"/>
                                                </div>
                                    </div>
                                    <div class="col-md-6">
                                            <div class="form-group">
                                                        <label for="name">Security Arriers</label>
                                                        <input type="number" name="security_arriers" required class="form-control" value="@if($ProgramFee){{$ProgramFee->security_arriers ?? ''}}@endif"/>
                                                </div>
                                    </div>
                            </div>
                            <div class="row">
                                     <div class="col-md-6">
                                            <div class="form-group">
                                                        <label for="name">Status</label>
                                                        <select name="status" class="form-control select2">
                                                            <option value="1" @if($ProgramFee){{ ($ProgramFee->status == 1) ? 'selected' : ''}}@endif>Active</option>
                                                            <option value="0" @if($ProgramFee){{ ($ProgramFee->status == 0) ? 'selected' : ''}}@endif>In Active</option>
                                                        </select>
                                                 </div>
                                    </div>
                            </div>
                            
                            
                           
                           
                            
                            
                            
                            <div class="form-group">
                                <input type="submit" name="submit" class="btn btn-primary" value="Save Changes"/>
                            </div>

                        </form>
                    </div>
                </div> <!-- Ending row --> 
            </div> <!-- Ending sbox-content - 18 --> 
        </div> <!-- Ending sbox - 14 --> 
    </div> <!-- Ending page-content-wrapper --> 
</div> <!-- Ending page-content --> 
@stop