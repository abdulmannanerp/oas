@extends('layouts.app')
@section('content')
    <div class="page-content-wrapper m-t">
        <div class="sbox">
            <div class="sbox-title">
                <div class="row">
                    <div class="col-md-6">
                        <h1> Program Fee Structure</h1>
                    </div>
                    <div class="col-md-6">
                        <a href={{ route('add-fee') }} class="btn btn-sm btn-primary" style="float: right">Add</a>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        @if ( $status )
                            <div class="alert alert-success" role="alert"> {{ $status }}</div>
                        @endif
                        <form action="{{route('program-fee')}}" method="POST" id="document-verification-from">
                                <select name="faculty" id="faculty" required>
                                    <option value="">--Select Faculty--</option>
                                    @foreach ( $faculties as $faculty )
                                        <option value="{{$faculty->pkFacId}}">{{$faculty->title}}</option>
                                    @endforeach
                                </select>
                                <select name="department" id="department" required>
                                    <option value="">--Select Department--</option>
                                    @if ( $selectedDepartments )
                                        @foreach ( $selectedDepartments as $department )
                                            <option value={{ $department->pkDeptId}}>{{$department->title}}</option>
                                        @endforeach
                                    @endif
                                </select>
                                <select name="programme" id="programme">
                                    <option value="">--Select Programme--</option>
                                    @if ( $selectedPorgrams )
                                        @foreach ( $selectedPorgrams as $program )
                                        <option value={{ $program->pkProgId}}>{{$program->title}}</option>
                                        @endforeach
                                    @endif
                                </select>
                                <input type="submit" value="Submit" id="get-docs-verification-records">
                            </form>
                        </div>
                </div>
            </div> <!-- Ending sbox-title --> 
            <div class="sbox-content "> 
<?php $prog=''; ?>
@if($ProgramFee)
<div class="table-responsive" style="min-height:300px;">
<table class="table table-hover table-bordered app-search-grid">
    <thead class="table-head">
        <tr width="100%">
            <th>Program</th> 
            <th>Semester</th>           
            <th>Admission Fee</th>
            <th>University Dues</th>
            <th>Library Security</th>
            <th>Book Bank Security</th>
            <th>Caution Money</th>
            <th>Hostel Dues</th>
            <th>Hostel Security</th>
            <th>Dues Arriers</th>
            <th>Security Arriers</th>
            <th>Status</th>
            <th> Edit </th>
        </tr>
    </thead>
    <tbody>
        @foreach ( $ProgramFee as $post )
        <?php $prog = $post->fkProgramId; ?>
            <tr width="100%">
            <td>{{$post->program->title}}</td>
            <td>{{$post->semester->title}}</td>
            <td>{{$post->admission_fee}}</td>
            <td>{{$post->university_dues}}</td>
            <td>{{$post->library_security}}</td>
            <td>{{$post->book_bank_security}}</td>
            <td>{{$post->caution_money}}</td>
            <td>{{$post->hostel_dues}}</td>
            <td>{{$post->hostel_security}}</td>
            <td>{{$post->dues_arriers}}</td>
            <td>{{$post->security_arriers}}</td>
            <td>{{($post->status == 1) ? 'Active' : 'In Active'}}</td>
            <td><a href={{ route('add-fee', ['id' => $post->id ]) }} target="_blank" class="btn btn-sm btn-primary">Edit</a></td>
            </tr>
        @endforeach
    </tbody>
    </table>
</div>
@endif


            </div> <!-- Ending sbox-content - 18 --> 
        </div> <!-- Ending sbox - 14 --> 
    </div>


    <script>
            $(function(){
                $('#faculty').on('change', function(){
    
                   $.ajax({
                       method: 'post',
                       url: '{{ route('getDepartmentsFee') }}',
                       data: {
                           id: $(this).val()
                       },
                       success: function ( response ) {
                           $('#department').empty();
                           $('#department').append(
                                '<option value="">--Select Department--</option>'
                            );
                           $.each(response, function (key, value) {
                               $('#department').append(
                                   '<option value="'+value.pkDeptId+'">'+ value.title+'</option>'
                               );
                           });
                       }
                   });
                });
    
                $('#department').on('change', function(){
                    $.ajax({
                        method: 'post',
                        url: '{{ route('getProgrammeFee') }}',
                        data: {
                            id: $(this).val()
                        },
                        success: function (response) {
                            $('#programme').empty();
                            if ( response == '' ) {
                                $('#programme').append(
                                    '<option value="">--Select Porgramme--</option>'
                                );
                                return;
                            }
                            $('#programme').append(
                                '<option value="">--Select Porgramme--</option>'
                            );
                            $.each(response, function (key, value) {
                                $('#programme').append(
                                    '<option value="'+value.pkProgId+'">'+ value.title+'</option>'
                                );
                            });
                        }
                    });
                });
    

    
                document.querySelector('#faculty').value = '{{request('faculty')}}';
                document.querySelector('#department').value = '{{request('department')}}';
                document.querySelector('#programme').value = '{{request('programme')}}';
                document.querySelector('#unverified-verified-filter').value = '{{request('verification-status')}}';
            });

        </script>
@stop
