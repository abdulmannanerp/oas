

	@include('results.header')
	<div class="container-fluid result-list">
		@if ( $records->count() );
		<div class="row">
			<div class="col-lg-2">
				@include('merit.front.tab')
			</div>
			<div class="col-lg-10">
				<div class="info-container">
						<div class="main-info">
							<h5 class="program-title">{{ $merit->merit->program->title. ' ('.$merit->merit->gender.') ' }}</h5>
							<h5 class="program-title">
								@if($merit->merit->type == 'Interview')
									Interview List
								@elseif($merit->merit->type == 'Result')
									{{$merit->list.' List'}}
								@endif
							</h5>
							{{-- <p class="cl-ov"> <span class="float-left">Published: {{ \Carbon\Carbon::parse($resultDetail->result->created_at)->format('d-m-Y') }}</span><span class="float-right">Last Date: <span class="list-date">{{ \Carbon\Carbon::parse($resultDetail->result->endDate)->format('d-m-Y') }}</span></span> </p> --}}
						</div>
						{{-- <p class="list-instructions"> {{ $resultDetail->result->instructions }}</p> --}}
					</div>
					<table class="table table-striped">
						<thead class="thead list-thead">
							<tr>
								<td>S.No</td>
								<td>Roll Number</td>
								<td>Name</td>
								<td>Father Name</td>
								<td>Offer Letter</td>
								<td>Challan Form</td>
							</tr>
						</thead>
						<tbody>
						<?php $i = 0; ?>	
						@foreach ( $records as $record )
							<tr>
								<td> {{ ++$i }} </td>
								<td> {{ $record->rollno }} </td>
								<td> {{ $record->getRollNo->applicant->name ?? '' }} </td>
								<td> {{ $record->getRollNo->applicant->detail->fatherName ?? '' }} </td>
								<td><a href="{{route('getResultDocs', ['rollno' => $record->rollno, 'doc' => 'offer-letter'])}}" target="_blank">Download Offer Letter</a></td>
								<td><a href="{{route('getResultDocs', ['rollno' => $record->rollno, 'doc' => 'fee-challan'])}}" target="_blank">Download Challan Form</a></td>
							</tr>
						@endforeach
						</tbody>
					</table>
			</div>
		</div>
		@else
			<div class="row" style="padding:20px 0">
				<div class="col-lg-2">
					<h3>No record found</h3>
				</div>
			</div>
		@endif
	</div>
	@include('results.footer')