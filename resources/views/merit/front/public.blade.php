@include('merit.front.header')
	<div class="container results-main-container">
		<h3 class="main-title">Admission Results Spring 2019</h3>
			<ul class="sub-nav">
				<li>Recent Results</li>
				<li>Admissions</li>
				<li>Fee Structure</li>
				<li>Instructions</li>
				<li>Joining Performa</li>
			</ul>
		<div class="col-lg-8 flt-lft">

			<form class="text-center border border-light res-frm" action={{route('postMeritListResults')}} method="POST">
				<input type="hidden" name="helper" value="list">
				<div class="list-group txt-cntr mb-3">
					<p class="list-group-item list-group-item-action heading-frm">Search by Program</p>
				</div>
				<select name="programme" id="programme" class="merit-program browser-default custom-select mb-3 select2" required>
					<option value="">-- Select Program -- </option>
					@foreach ( $programs as $program )
						<option value="{{$program->pkProgId}}">{{ $program->title }}</option>
					@endforeach
				</select>
				<button class="btn btn-info btn-block res-sub merit-program-submit-button merit-program-search-button" type="submit">Search</button>
			</form>
		</div> <!-- Ending col-lg-8 --> 
		<div class="col-lg-4 flt-lft">
			<div class="list-group res-frm txt-cntr">
				<div class="list-group txt-cntr mb-3">
					<p class="list-group-item list-group-item-action heading-frm">Search Merit Position</p>
				</div>
				<form class="text-center border border-light" action={{route('getResultByRollNumber')}} method="POST">
					<input type="hidden" name="helper" value="rollno">
					{{ csrf_field() }}
					<div class="form-group">
						<input type="number" class="form-control" name="rollnumber" placeholder="Enter Roll Number" required>
					</div>
					<button class="btn btn-info btn-block res-sub" type="submit">Search</button>
				</form>
			</div>
		</div>
		<div class="container list-container clr custom-lst announcements-panel">
			{{-- @if ( !empty( $results ) ) 
				<h4 class="announcements-heading p-3">Search Results:</h4>
				<div class="row">
				@foreach ( $results as $result )
					<div class="col-sm-6 col-md-12">
						<div class="card mb-3">
								<div class="card-header">{{ ($result->type == 'Interview') ? 'Interview List' : $result->listNumber. ' Merit list '}}</div>
							<div class="card-body">

								<a class="card-title" target="_blank" href={{route('displayLists', ['id' => $result->pkResultsId])}}>
										{{ $result->program->title }} - <b>{{ $result->gender }}</b>
										</a>

								@if($result->type != 'Interview')
									<p class="card-text"><b>Note:</b> Selected candidates are advised to collect their provisional offer letter along with fee challan form from the Admission office with effect from {{ \Carbon\Carbon::parse($result->startDate)->format('d-m-Y')}}</p>
								@endif
							</div>
							<div class="card-footer"><i class="fa fa-calendar" aria-hidden="true"></i><p class="card-text">Published: {{ \Carbon\Carbon::parse($result->created_at)->format('d-m-Y')}}</p></div>
						</div>
					</div>
				@endforeach
				</div> 
				<div class="list-group txt-cntr">
					<a href="{{ route('recent-results') }}" target="_blank" class="list-group-item list-group-item-action heading-frm">Recent Results</a>
				</div>
			@endif --}}
		</div> <!-- list-container -->


		{{-- <div class="list-group res-frm txt-cntr">
			<a href="{{ route('recent-results') }}" target="_blank" class="list-group-item list-group-item-action heading-frm">
				Recent Results
			</a>
			<a href="http://admission.iiu.edu.pk" target="_blank" class="list-group-item list-group-item-action heading-frm">
				Admissions
			</a>
			<a href="{{ route('fee-structure') }}" target="_blank" class="list-group-item list-group-item-action heading-frm">
				Fee Structure
			</a>
			<a href="http://www.iiu.edu.pk/admissions_custom/downloads/S16/instructions-s16-180116.pdf" target="_blank" class="list-group-item list-group-item-action heading-frm">
				Instructions
			</a>
			<a href="http://admission.iiu.edu.pk/admin/public/uploads/Joining_performa_F18.pdf" target="_blank" class="list-group-item list-group-item-action heading-frm">
				Joining Performa
			</a>
		</div> --}}

	</div> <!--Ending container --> 

	@include('merit.front.footer')