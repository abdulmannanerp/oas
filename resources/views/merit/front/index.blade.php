@extends('layouts.app')
@section('content')
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
	<div class="container">
		{{ $status }}
		<section class="page-header row">
			<h2> Results </h2>
		</section>
		<div class="page-content row">
			<div class="page-content-wrapper no-margin">
				@if($errors->any())
					<div class="alert alert-danger">
				        <ul>
				        	@foreach ( $errors->all() as $error ) 
				            	<li>{{ $error }}</li>
				            @endforeach
				        </ul>
				    </div>
				@endif
				<form method="POST" action={{( $result ) ? route('patchResult') : route('postResult')}} accept-charset="UTF-8" class="form-horizontal validated" enctype="multipart/form-data">
					{{ csrf_field() }}
					@if ($result)
						<input type="hidden" name="_method" value="PATCH">
						<input type="hidden" name="result-to-patch" value="{{$id}}">
					@endif
					<input type="hidden" value="{{auth()->id()}}" name="userId">
					<div class="sbox">
						<div class="sbox-content clearfix">
							<div class="form-group  ">
								<div class="row">
									<div class="col-md-4">
										<label for="Title" class=" control-label col-md-4 text-left"> Faculty </label>
									</div>
									<div class="col-md-6">
									  <select name="faculty" id="faculty" class="form-control input-sm" required>
									  	<option value="">--Select Faculty--</option>
									  	@foreach ( $faculties as $faculty )
									  		@if ( $result )
									  			<option {{ ($result->fkFacultyId == $faculty->pkFacId) ? 'Selected' : '' }} value="{{$faculty->pkFacId}}">{{$faculty->title}}</option>
									  		@else
									  			<option value="{{$faculty->pkFacId}}">{{$faculty->title}}</option>
									  		@endif
									  	@endforeach
									  </select>
									</div> 
								</div> <!-- Ending row -->
							</div> <!-- Ending form-group -->

							<div class="form-group  ">
								<div class="row">
									<div class="col-md-4">
										<label for="Title" class=" control-label col-md-4 text-left"> Department </label>
									</div>
									<div class="col-md-6">
									  <select name="department" id="department" class="form-control input-sm" required>
									  	<option value="">--Select Department--</option>
									  	@if ( $result )
										  	@foreach ( $selectedDepartments as $department )
										  		<option {{ ($result->fkDeptId == $department->pkDeptId) ? 'Selected' : '' }} value="{{ $department->pkDeptId }}"> {{$department->title}}</option>
										  	@endforeach
									  	@endif
									  </select>
									</div> 
								</div> <!-- Ending row -->
							</div> <!-- Ending form-group -->

							<div class="form-group  ">
								<div class="row">
									<div class="col-md-4">
										<label for="Title" class=" control-label col-md-4 text-left"> Program </label>
									</div>
									<div class="col-md-6">
									  <select name="programme" id="programme" class="form-control input-sm" required>
									  	<option value="">--Select Program--</option>
									  	@if ( $result ) 
										  	@foreach ( $selectedProgrames as $program )
										  		<option {{ ($result->fkProgrammeId == $program->pkProgId) ? 'Selected' : '' }} value="{{$program->pkProgId}}"> {{ $program->title }}</option>
										  	@endforeach
									  	@endif
									  </select>
									</div> 
								</div> <!-- Ending row -->
							</div> <!-- Ending form-group -->
						
							<div class="form-group  ">
								<div class="row">
									<div class="col-md-4">
										<label for="Title" class=" control-label col-md-4 text-left"> Gender </label>
									</div>
									<div class="col-md-6">
										<div class="iradio_square-green" style="position: relative;">
										  	<input type="radio" name="selectGender" value="Male" class="minimal-red" style="position: absolute; opacity: 0;" data-parsley-multiple="selectGender" {{ ($result && $result->gender == 'Male') ? 'checked="checked"' : ''}} required>
										</div> Male 
					
										<div class="iradio_square-green" style="position: relative;">
											<input type="radio" name="selectGender" value="Female" class="minimal-red" data-parsley-multiple="selectGender" style="position: absolute; opacity: 0;" {{ ($result && $result->gender == 'Female') ? 'checked="checked"' : ''}} required>
										</div> Female  
									</div> 
								</div> <!-- Ending row -->
							</div> <!-- Ending form-group -->

							<div class="form-group  ">
								<div class="row">
									<div class="col-md-4">
										<label for="Title" class=" control-label col-md-4 text-left"> Type </label>
									</div>
									<div class="col-md-6">
										<div class="iradio_square-green" style="position: relative;">
										  		<input type="radio" name="resultType" value="Result" class="minimal-red" style="position: absolute; opacity: 0;" data-parsley-multiple="resultType" {{ ($result && $result->type == 'Result') ? 'checked="checked"' : ''}} required>
										</div> Result 
					
										<div class="iradio_square-green" style="position: relative;">
											<input type="radio" name="resultType" value="Interview" class="minimal-red" data-parsley-multiple="resultType" style="position: absolute; opacity: 0;" {{ ($result && $result->type == 'Interview') ? 'checked="checked"' : ''}} required>
										</div> Interview  
									</div> 
								</div> <!-- Ending row -->
							</div> <!-- Ending form-group -->

							<div class="form-group  ">
								<div class="row">
									<div class="col-md-4">
										<label for="Title" class=" control-label col-md-4 text-left"> List Number </label>
									</div>
									<div class="col-md-6">
									  <select name="listNumber" id="listNumber" class="form-control input-sm" required>
									  	<option value=""> --Select List-- </option>
									  	<option value="First" {{ ($result && $result->listNumber == 'First') ? 'Selected' : '' }}>1st</option>
									  	<option value="Second" {{ ($result && $result->listNumber == 'Second') ? 'Selected' : '' }}>2nd</option>
									  	<option value="Third" {{ ($result && $result->listNumber == 'Third') ? 'Selected' : '' }}>3rd</option>
									  	<option value="Fourth" {{ ($result && $result->listNumber == 'Fourth') ? 'Selected' : '' }}>4th</option>
									  	<option value="Fifth" {{ ($result && $result->listNumber == 'Fifth') ? 'Selected' : '' }}>5th</option>
									  	<option value="Sixth" {{ ($result && $result->listNumber == 'Sixth') ? 'Selected' : '' }}>6th</option>
									  </select>
									</div> 
								</div> <!-- Ending row -->
							</div> <!-- Ending form-group -->

							<div class="form-group  ">
								<div class="row">
									<div class="col-md-4">
										<label for="Title" class=" control-label col-md-4 text-left"> Publication Option </label>
									</div>
									<div class="col-md-6">
									  <select name="publishStatus" id="publishStatus" class="form-control input-sm" required>
									  	<option value="publish">Publish</option>
									  </select>
									</div> 
								</div> <!-- Ending row -->
							</div> <!-- Ending form-group -->

							<div class="form-group">
								<div class="row">
									<div class="col-md-4">
										<label for="Start Date" class=" control-label col-md-4 text-left"> Start Date </label>
									</div>
									<div class="col-md-6">
										<input name="startDate" value="{{($result->startDate ?? '')}}" id="start-date" type="text" class="form-control input-sm ">
									</div> 
								</div> <!-- Ending row -->
							</div> <!-- Ending form-group -->

							<div class="form-group">
								<div class="row">
									<div class="col-md-4">
										<label for="End Date" class=" control-label col-md-4 text-left"> End Date </label>
									</div>
									<div class="col-md-6">
										<input name="endDate" value="{{($result->endDate ?? '')}}" id="end-date" type="text" class="form-control input-sm ">
									</div> 
								</div> <!-- Ending row -->
							</div> <!-- Ending form-group -->

							<div class="form-group  ">
								<div class="row">
									<div class="col-md-4">
										<label for="Title" class=" control-label col-md-4 text-left"> Select File </label>
									</div>
									<div class="col-md-6">
										<input name="fileToUplaod" type="file" class="form-control input-sm" {{ ($result) ? '' : 'required'}}>
									</div> 
								</div> <!-- Ending row -->
							</div> <!-- Ending form-group -->

							<div class="form-group  ">
								<div class="row">
									<div class="col-md-4">
										<label for="Title" class=" control-label col-md-4 text-left"> Instructions </label>
									</div>
									<div class="col-md-6">
										<textarea name="instructions" id="instructions" cols="30" rows="10" class="form-control input-sm" required>{{($result->instructions ?? '')}}</textarea>
									</div> 
								</div> <!-- Ending row -->
							</div> <!-- Ending form-group -->

						</div> <!-- Ending sbox-content -->
						<div class="sbox-title clearfix">
							<div class="sbox-tools pull-left">
								<button name="apply" class="tips btn btn-sm btn-apply" type="submit" title="Back"><i class="fa  fa-check"></i> Save </button>
							</div>
						</div>
					</div> <!-- Ending sbox -->
				</form>
			</div> <!-- Ending page-content-wrapper -->
		</div> <!-- Ending page-content row -->
	</div> <!-- Ending Container -->

	<script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
	<script>
        $(function(){
            $('#faculty').on('change', function(){

               $.ajax({
                   method: 'post',
                   url: '{{ route('getDepartments') }}',
                   data: {
                       id: $(this).val()
                   },
                   success: function ( response ) {
                       $('#department').empty();
                       $('#department').append(
                            '<option value="">--Select Department--</option>'
                        );
                       $.each(response, function (key, value) {
                           $('#department').append(
                               '<option value="'+value.pkDeptId+'">'+ value.title+'</option>'
                           );
                       });
                   }
               });
            });

            $('#department').on('change', function(){
                $.ajax({
                    method: 'post',
                    url: '{{ route('getProgramme') }}',
                    data: {
                        id: $(this).val()
                    },
                    success: function (response) {
                        $('#programme').empty();
                        if ( response == '' ) {
                            $('#programme').append(
                                '<option value="">--Select Porgramme--</option>'
                            );
                            return;
                        }
                        $('#programme').append(
                            '<option value="">--Select Porgramme--</option>'
                        );
                        $.each(response, function (key, value) {
                            $('#programme').append(
                                '<option value="'+value.pkProgId+'">'+ value.title+'</option>'
                            );
                        });
                    }
                });
            });

            $('#start-date').flatpickr();
	        $('#end-date').flatpickr();
        });
    </script>

@endsection