@include('results.header')
	<div class="container sbr-container">
		<a href="{{ route('getResultsMainPage') }}">Results</a>
		@if ( $results->count() < 1 )
			<h3>No Record Found.</h3>
		@endif

		@if ( $results->count() >= 1 )
			<h3>Searched Results:</h3>
			@foreach ( $results as $result )
				<p>{{ $result->result->instructions }} </p>
				<table class="table table-striped">
					<thead class="thead list-thead">
						<tr>
							<th>Roll No</th>
							<th>Name</th>
							<th>Father Name</th>
							<th>Type</th>
							<th>Program</th>
							<th> List #</th>
							<th>End Date</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td> {{ $result->rollno ?? '' }} </td>
							<td> {{ $result->name ?? '' }}</td>
							<td> {{ $result->fatherName ?? '' }} </td>
							<td> {{ $result->result->type ?? '' }} </td>
							<td> {{ $result->result->program->title ?? '' }}</td>
							<td> {{ $result->result->listNumber ?? '' }} </td>
							<td> {{ $result->result->endDate ?? '' }} </td>
						</tr>
					</tbody>
				</table>
				@if ( $result->result->type == 'Result' )
					<ul id="performa-and-offer-letter-list">
						<li>
							{{-- <a href="/get-results-docs/{{$result->rollno}}/performa">Joining Performa</a> --}}
							<a href="http://admission.iiu.edu.pk/admin/public/uploads/Joining_performa_F18.pdf" target="_blank">Joining Performa</a>
							
						</li>
						<li>
							<a href="/get-results-docs/{{$result->rollno}}/offer-letter" target="_blank">Offer Letter</a>
						</li>
						<li>
							<a href="/get-results-docs/{{$result->rollno}}/fee-challan" target="_blank">Fee Challan</a>
						</li>
						<li>
							<a href="http://www.iiu.edu.pk/admissions_custom/downloads/S16/instructions-s16-180116.pdf" target="_blank">Instructions</a>
						</li>
						<li>
							<a href="http://www.iiu.edu.pk/index.php?page_id=299" target="_blank">Free Structure</a>
						</li>
					</ul>
				@endif
				<hr>
			@endforeach
		@endif
	</div>
@include('results.footer')