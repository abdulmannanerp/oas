<div class="list-group res-frm txt-cntr st-tp">
    <a href="{{ route('recent-results') }}" target="_blank" class="list-group-item list-group-item-action heading-frm">Recent Results</a>
    {{-- <a href="#" target="_blank" class="list-group-item list-group-item-action heading-frm">Result</a> --}}
    <a href="/" target="_blank" class="list-group-item list-group-item-action heading-frm">Admissions</a>
    {{-- <a href="http://admission.iiu.edu.pk/programes" target="_blank" class="list-group-item list-group-item-action heading-frm">Programs Offered</a> --}}
    <a href="{{ route('fee-structure') }}" target="_blank" class="list-group-item list-group-item-action heading-frm">Fee Structure</a>
    <a href="{{ route('instructions') }}" target="_blank" class="list-group-item list-group-item-action heading-frm">Instructions</a>
    <a href="{{ route('affidavit-specimen') }}" target="_blank" class="list-group-item list-group-item-action heading-frm">Undertaking Specimen</a>
    {{-- <a href="http://admission.iiu.edu.pk/docs/Joining_performa_F19.docx" target="_blank" class="list-group-item list-group-item-action heading-frm">Joining Performa</a> --}}
</div>