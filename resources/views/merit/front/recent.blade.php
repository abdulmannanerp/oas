@include('results.header')

	@if ( $getLists)
	<div class="container-fluid main-container announcements-panel">
		<div class="container">
			<h4 class="announcements-heading text-center">Searched Results</h4>
			{{-- <h6 class="announcements-heading pull-right">
				<a href="#" target="_blank">All Results</a>
			</h6> --}}
			<div class="row">
				@foreach ( $getLists as $list )
				<div class="col-sm-6 col-md-4">
					<div class="card mb-3">
						<div class="card-header">{{ $list->list . ' '. $list->merit->type . ' List' }}</div>
						<div class="card-body">
								<a 	class="card-title" 
									target="_blank" 
									href="{{route('getSingleList', ['id' => $list->id])}}">
									{{ $list->merit->program->title ?? '' }} - <strong>{{ $list->merit->gender }}</strong>
								</a>
						</div>
						<div class="card-footer">
							<i class="fa fa-calendar" aria-hidden="true"></i>
							<p class="card-text">
								Published: {{ \Carbon\Carbon::parse($list->created_at)->format('d-m-Y')}}
							</p>
						</div>
					</div>
				</div>
				@endforeach
			</div> <!-- Ending row --> 
		</div> <!-- Ending container -->
	</div> <!-- Ending contianer-fluid main-container -->
	@endif
	
	@include('results.footer')