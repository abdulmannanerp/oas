<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Fee Challan</title>
  <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
  <link rel="stylesheet" href="{{asset('css/fee-challan.css')}}">
  <link rel="stylesheet" href="https://printjs-4de6.kxcdn.com/print.min.css">
  <script src="https://printjs-4de6.kxcdn.com/print.min.js"></script>
</head>
<body>
  <div class="print-and-download-container">
    <button onclick="printJS
    ({
      printable: 'fee-challan-container', 
      type:'html', 
      css: '{{asset('css/fee-challan.css')}}',
      documentTitle: 'Fee Challan',
      maxWidth: '1300'
    })">Print Challan Form</button>
    <form action="{{ route('getResultDocs', ['rollno' => $rollno, 'doc' => 'fee-challan'])}}" method="GET">
      <input type="hidden" value="true" name="download">
      <button>Download Offer Letter</button>
    </form>
  </div>
  <div class="container" id="fee-challan-container">
  	<div class="dues-challan">
  		@foreach ( $copies as $copy )
		    <div class="single-column">
		      	<div class="bank-title">{{ $accountNumber->bank_name ?? '' }} </div>
				<div class="account-number-container">
					<span class="credit-collectiton">For Credit of collection A/C#</span>
					<span class="account-number right">{{ $accountNumber->dues_account ?? '' }}</span>
				</div>
				<div class="challan-number">
					<span class="bold">Challan:</span>
					<span>120187649</span>
				</div>
				<div class="current-semester-container">
					<span class="bold">Semester:</span>
					<span>{{ $semester->title ?? ''}}</span>
				</div>
				<div class="dates-container">
					<div class="single-date">
						<span class="bold">Issued Date</span>
						<span>{{ \Carbon\Carbon::now()->format('d-M-Y') }}</span>
					</div>
					<div class="single-date">
						<span class="bold">Due Date</span>
						<span>16-Aug-18</span>
					</div>
				</div>
				<table class="applicants-detail-table">
					<tbody>
						<tr>
							<td class="bold">Roll No: </td>
							<td>{{ $rollno }}</td>
						</tr>
						<tr>
							<td class="bold">Students Name: </td>
							<td>{{ $applicant->applicant->name ?? ''}}</td>
						</tr>
						<tr>
							<td class="bold">Father's Name </td>
							<td>{{ $applicant->applicant->detail->fatherName ?? ''}}</td>
						</tr>
						<tr>
							<td class="bold">Faculty</td>
							<td>{{ $program->faculty->title }}</td>
						</tr>
						<tr>
							<td class="bold">Department</td>
							<td>{{ $program->department->title }}</td>
						</tr>
						<tr>
							<td class="bold">Program: </td>
							<td>{{ $program->title }} </td>
						</tr>
					</tbody>
				</table>

				<table class=fee-distribution-table>
					<thead>
						<tr>
							<th>Particulars</th>
							<th class="border-left">Amount</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>Admission Fee</td>
							<td class="border-left">{{ $fee->admission_fee }}</td>
						</tr>
						<tr>
							<td>University Dues</td>
							<td class="border-left">{{ $fee->university_dues }}</td>
						</tr>
						<tr>
							<td>Total Amount</td>
							<td class="border-left">{{ $fee->admission_fee + $fee->university_dues}}</td>
						</tr>
					</tbody>
				</table>
				<div class="copy-owner">
					<span class="copy">{{ $copy }}</span>
				</div>
			</div> <!-- Ending single-column --> 
		@endforeach 
	</div> <!-- Ending dues challan --> 

	<div class="security-challan">
		@foreach ( $copies  as $copy ) 
		    <div class="single-column">
		      	<div class="bank-title">{{$accountNumber->bank_name}}</div>
				<div class="account-number-container">
					<span class="credit-collectiton">For Credit of collection A/C#</span>
					<span class="account-number right">{{ $accountNumber->security_account ?? '' }}</span>
				</div>
				<div class="challan-number">
					<span class="bold">Challan:</span>
					<span>120187649</span>
				</div>
				<div class="current-semester-container">
					<span class="bold">Semester:</span>
					<span>{{ $semester->title ?? ''}}</span>
				</div>
				<div class="dates-container">
					<div class="single-date">
						<span class="bold">Issued Date</span>
						<span>{{ \Carbon\Carbon::now()->format('d-M-Y') }}</span>
					</div>
					<div class="single-date">
						<span class="bold">Due Date</span>
						<span>16-Aug-18</span>
					</div>
				</div>
				<table class="applicants-detail-table">
					<tbody>
						<tr>
							<td class="bold">Roll No: </td>
							<td>{{ $rollno }}</td>
						</tr>
						<tr>
							<td class="bold">Students Name: </td>
							<td>{{ $applicant->applicant->name ?? ''}}</td>
						</tr>
						<tr>
							<td class="bold">Father's Name </td>
							<td>{{ $applicant->applicant->detail->fatherName ?? ''}}</td>
						</tr>
						<tr>
							<td class="bold">Faculty</td>
							<td>{{ $program->faculty->title }}</td>
						</tr>
						<tr>
							<td class="bold">Department</td>
							<td>{{ $program->department->title }}</td>
						</tr>
						<tr>
							<td class="bold">Program: </td>
							<td>{{ $program->title }} </td>
						</tr>
					</tbody>
				</table>

				<table class=fee-distribution-table>
					<thead>
						<tr>
							<th>Particulars</th>
							<th class="border-left">Amount</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>Library Security</td>
							<td class="border-left">{{ $fee->library_security }}</td>
						</tr>
						<tr>
							<td>Book Bank Security</td>
							<td class="border-left">{{ $fee->book_bank_security }}</td>
						</tr>
						<tr>
							<td>Caution Money</td>
							<td class="border-left">{{ $fee->caution_money }}</td>
						</tr>
						<tr>
							<td>Hostel Security</td>
							<td class="border-left"></td>
						</tr>
						<tr>
							<td>Total Amount</td>
							<td class="border-left">{{ $fee->library_security + $fee->book_bank_security + $fee->caution_money  }}</td>
						</tr>
					</tbody>
				</table>
				<div class="copy-owner">
					<span class="copy">{{ $copy }}</span>
				</div>
			</div> <!-- Ending single-column --> 
		@endforeach
	</div> <!-- Ending dues challan --> 
  </div> <!-- Ending container --> 
</body>
</html>