<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Offer Letter</title>
  <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
  <link rel="stylesheet" href="{{asset('css/offer-letter.css')}}">
  <link rel="stylesheet" href="https://printjs-4de6.kxcdn.com/print.min.css">
  <script src="https://printjs-4de6.kxcdn.com/print.min.js"></script>
</head>
<body> 
  <div class="print-and-download-container">
    <button onclick="printJS
    ({
      printable: 'offer-letter-container', 
      type:'html', 
      css: '{{asset('css/offer-letter.css')}}',
      documentTitle: 'Offer Letter'
    })
    ">Print Offer Letter</button>
    <form action="{{ route('getResultDocs', ['rollno' => $rollno, 'doc' => 'offer-letter'])}}" method="GET">
      <input type="hidden" value="true" name="download">
      <button>Download Offer Letter</button>
    </form>
  </div>
  <div class="container" id="offer-letter-container">
    <header>
      <div class="img-container">
        <img src="{{asset('/uploads/images/logo.jpg')}}" class="logo" alt="IIUI Logo">
      </div><div class="name-address-container">
        <h2>International Islamic University</h2>
        <h3>Islamabad Pakistan</h3>
        <h4>P.O.Box 1243,  web:www.iiu.edu.pk, Email:admissions@iiu.edu.pk</h4>
        <h4>Tel. 051-9257988, 051-9019871, Fax: 051-9257915</h4>
      </div>
    </header>
    <p class="date-now float-right">{{ \Carbon\Carbon::now()->format('F d, Y') }}</p>
    <div class="student-details-container">
      <p><strong>Roll No:</strong> {{ $rollno }}</p>
      <p><strong>Student Name:</strong> {{ $applicant->applicant->name }}</p>
      <p><strong>Father Name:</strong> {{ $applicant->applicant->detail->fatherName}}</p>
      <p><strong>Program:</strong> {{ $applicant->application->program->title }}</p>  
    </div>
    
    <p class="subject">Subject: <strong>PROVISIONAL OFFER FOR ADMISSION TO ACADEMIC SEMESTER {{ strtoupper($semester->title) }}</strong> </p>
    <p>Assalam-o-Alaikum!</p>
    <p>Dear Student,</p>
    <ol class="instructions-list">
      <li>
          I am pleased to inform that you have been provisionally selected for admission to the above mentioned degree program at International Islamic University, Islamabad from {{ $semester->title }} semester commencing from {{ $options[0] ?? ''}}. Your admission shall be confirmed upon completion of all formalities.  Please note that if you do not meet the admission criteria as mentioned in the admission advertisement or if the information given by you in the admission form is found incomplete or incorrect, this admission offer shall stand withdrawn and admission shall be cancelled without any further notice.
      </li>
      <li>
        You are required to bring along the following documents at the time of joining at Admission Office, Administration Block, New Campus, Sector H-10, Islamabad:
        <ul>
          <li>
             Original academic certificates along with detailed marks certificates. 
          </li>
          <li>
             Attested photocopies of all academic certificates/DMCs.  (One set.).
          </li>
          <li>
            05 Passport size coloured photographs. 
          </li>
          <li>
            Original Fee Slip: (Before depositing fee please make sure that you duly qualify for admission as per required criteria, otherwise in case of cancellation of admission for any reason, tuition fee would be refunded as per notified fee refund policy only: please consult admission guide, page 46 for fee refund policy).
          </li>
          <li>
            Undertaking on Rs 30/- Stamp paper as per specimen.
          </li>
          <li>
            No Objection Certificate (NOC) from employer (for employee only).
          </li>
        </ul>
      </li>
      <li>
        Your admission shall be governed by Rules and Regulations of the University enforced from time to time. You are advised to familiarize with IIUI regulations governing your admission, studies and examinations accordingly.
      </li>
      <li>
        If provisionally admitted on the basis of awaiting result, you are required to submit result with required percentage of marks/CGPA at the time of joining.  In case your result is not declared, you will be allowed to submit the same as soon as possible but not later than {{ $options[1] ?? '' }}, failing which your admission shall be treated as cancelled automatically.  Furthermore previous degree must be submitted to the admission office after confirmation from parent institution and attestation by HEC within this stipulated time period. 
      </li>
      <li>
        In case you fail in any subject or fail to secure required marks or CGPA required for admission to the abovementioned program, you must not conceal the fact and immediately report to admission office for refund of tuition fee if paid, as per notified refund policy given in the Admission Guide (2017-18) of IIUI.  
      </li>
      <li>
        You are advised to deposit fee/dues from {{ date_format(date_create($startDate), 'd-M-Y') }} to {{ date_format(date_create($endDate), 'd-M-Y') }} and submit your joining report to the admission office. The admission will not be confirmed unless fee/dues are paid in time and joining slip is issued by the Admission Office. 
      </li>
      <li>
        After completion of all joining formalities, you will report to the coordination office of concerned Department/Faculty.
      </li>
      <li>
        Overseas students are required to report to Overseas Admission Section to complete all joining formalities i.e. NOC, Study Visa, Equivalence of academic record and information sheets etc.  
      </li>
    </ol>
    <p>We welcome you to IIU & wish you best of luck in future academic endeavors.</p>
    <p class="float-right">Addl Dir/Deputy Director ( Academics )</p>
  {{--   <p>
      Encls: Fee Challan
    </p> --}}
    <p> 
      (Auto Generated by Student Information System) 
    </p>    
  </div>
</body>
</html>