<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Fee Challan</title>
  <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
  <style>
  	.container {
		width: 100%;
		margin: 0 auto;
		font-family: 'Open Sans', sans-serif;
		clear:both;
		/*text-align:center;*/
	}

	.print-button button {
		padding: 10px;
		float: right;
		background-color: #ddd;
		border: 1px solid #ccc;
		border-radius: 2px;
		cursor: pointer;
		margin-left: 5px;
	}

	.right {
		float: right;
	}

	.bold {
		font-weight: bold;
	}

	.container .single-column {
		width: 22% !important;
		border: 1px dotted #ccc;
		display: inline-block;
		margin: 5px 0.5%;
		padding: 5px;
		text-align: left;
		font-size: 13px;
		line-height: 25px;
	}

	.bank-title {
		text-align: center;
		font-weight: bold;
		font-size: 16px;
	}

	.dues-challan, .security-challan {
		padding-top: 100px;
	}

	.security-challan {
		page-break-before: always;
	}

	.account-number-container {
		font-size: 10px;
	}

	.account-number-container .credit-collectiton {
		text-decoration: underline;
	}

	.fee-distribution-table {
		width: 100%;
		border-spacing: 0;
	}
	.fee-distribution-table {
		border: 1px solid #ccc;
	}
	.fee-distribution-table td {
		border-bottom: 1px solid #ccc;
	}

	.border-left {
		border-left: 1px solid #ccc;
		text-align: center;
	}

	.copy-owner {
		margin-top: 100px; 
		text-align: center; 
	}
	.copy-owner .copy {
		font-weight: bold;
		font-size: 16px;
	}

	@media print {
		@page { size: landscape; }
	}

  </style>
</head>
<body>
  <div class="container" id="fee-challan-container">
  	<div class="dues-challan">
  		@foreach ( $copies as $copy )
		    <div class="single-column">
		      	<div class="bank-title">{{ $accountNumber->bank_name ?? '' }} </div>
				<div class="account-number-container">
					<span class="credit-collectiton">For Credit of collection A/C#</span>
					<span class="account-number right">{{ $accountNumber->dues_account ?? '' }}</span>
				</div>
				<div class="challan-number">
					<span class="bold">Challan:</span>
					<span>120187649</span>
				</div>
				<div class="current-semester-container">
					<span class="bold">Semester:</span>
					<span>{{ $semester->title ?? ''}}</span>
				</div>
				<div class="dates-container">
					<div class="single-date">
						<span class="bold">Issued Date</span>
						<span>{{ \Carbon\Carbon::now()->format('d-M-Y') }}</span>
					</div>
					<div class="single-date">
						<span class="bold">Due Date</span>
						<span>16-Aug-18</span>
					</div>
				</div>
				<table class="applicants-detail-table">
					<tbody>
						<tr>
							<td class="bold">Roll No: </td>
							<td>{{ $rollno }}</td>
						</tr>
						<tr>
							<td class="bold">Students Name: </td>
							<td>{{ $applicant->applicant->name ?? ''}}</td>
						</tr>
						<tr>
							<td class="bold">Father's Name </td>
							<td>{{ $applicant->applicant->detail->fatherName ?? ''}}</td>
						</tr>
						<tr>
							<td class="bold">Faculty</td>
							<td>{{ $program->faculty->title }}</td>
						</tr>
						<tr>
							<td class="bold">Department</td>
							<td>{{ $program->department->title }}</td>
						</tr>
						<tr>
							<td class="bold">Program: </td>
							<td>{{ $program->title }} </td>
						</tr>
					</tbody>
				</table>

				<table class=fee-distribution-table>
					<thead>
						<tr>
							<th>Particulars</th>
							<th class="border-left">Amount</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>Admission Fee</td>
							<td class="border-left">{{ $fee->admission_fee }}</td>
						</tr>
						<tr>
							<td>University Dues</td>
							<td class="border-left">{{ $fee->university_dues }}</td>
						</tr>
						<tr>
							<td>Total Amount</td>
							<td class="border-left">{{ $fee->admission_fee + $fee->university_dues}}</td>
						</tr>
					</tbody>
				</table>
				<div class="copy-owner">
					<span class="copy">{{ $copy }}</span>
				</div>
			</div> <!-- Ending single-column --> 
		@endforeach 
	</div> <!-- Ending dues challan --> 

	<div class="security-challan">
		@foreach ( $copies  as $copy ) 
		    <div class="single-column">
		      	<div class="bank-title">{{$accountNumber->bank_name}}</div>
				<div class="account-number-container">
					<span class="credit-collectiton">For Credit of collection A/C#</span>
					<span class="account-number right">{{ $accountNumber->security_account ?? '' }}</span>
				</div>
				<div class="challan-number">
					<span class="bold">Challan:</span>
					<span>120187649</span>
				</div>
				<div class="current-semester-container">
					<span class="bold">Semester:</span>
					<span>{{ $semester->title ?? ''}}</span>
				</div>
				<div class="dates-container">
					<div class="single-date">
						<span class="bold">Issued Date</span>
						<span>{{ \Carbon\Carbon::now()->format('d-M-Y') }}</span>
					</div>
					<div class="single-date">
						<span class="bold">Due Date</span>
						<span>16-Aug-18</span>
					</div>
				</div>
				<table class="applicants-detail-table">
					<tbody>
						<tr>
							<td class="bold">Roll No: </td>
							<td>{{ $rollno }}</td>
						</tr>
						<tr>
							<td class="bold">Students Name: </td>
							<td>{{ $applicant->applicant->name ?? ''}}</td>
						</tr>
						<tr>
							<td class="bold">Father's Name </td>
							<td>{{ $applicant->applicant->detail->fatherName ?? ''}}</td>
						</tr>
						<tr>
							<td class="bold">Faculty</td>
							<td>{{ $program->faculty->title }}</td>
						</tr>
						<tr>
							<td class="bold">Department</td>
							<td>{{ $program->department->title }}</td>
						</tr>
						<tr>
							<td class="bold">Program: </td>
							<td>{{ $program->title }} </td>
						</tr>
					</tbody>
				</table>

				<table class=fee-distribution-table>
					<thead>
						<tr>
							<th>Particulars</th>
							<th class="border-left">Amount</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>Library Security</td>
							<td class="border-left">{{ $fee->library_security }}</td>
						</tr>
						<tr>
							<td>Book Bank Security</td>
							<td class="border-left">{{ $fee->book_bank_security }}</td>
						</tr>
						<tr>
							<td>Caution Money</td>
							<td class="border-left">{{ $fee->caution_money }}</td>
						</tr>
						<tr>
							<td>Hostel Security</td>
							<td class="border-left"></td>
						</tr>
						<tr>
							<td>Total Amount</td>
							<td class="border-left">{{ $fee->library_security + $fee->book_bank_security + $fee->caution_money  }}</td>
						</tr>
					</tbody>
				</table>
				<div class="copy-owner">
					<span class="copy">{{ $copy }}</span>
				</div>
			</div> <!-- Ending single-column --> 
		@endforeach
	</div> <!-- Ending dues challan --> 
  </div> <!-- Ending container --> 
</body>
</html>