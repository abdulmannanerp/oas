@extends('layouts.app')
@section('content')
	<div class="container">
		<section class="page-header row">
			<h2> Manage Results </h2>
		</section>
		<div class="page-content manage-results-page-section">
			<form action="{{route('manageResults')}}" method="POST">
				<select name="faculty" id="faculty" required>
					<option value="">-- Select Faculty -- </option>
					@foreach ( $faculties as $faculty )
						<option value="{{ $faculty->pkFacId }}">{{ $faculty->title }}</option>
					@endforeach
				</select>
				<select name="department" id="department" required>
					<option value="">-- Select Department -- </option>
				</select>
				<select name="programme" id="programme" required>
					<option value="">-- Select Program -- </option>
				</select>
				<button class="btn btn-primary" type="submit">Search</button>
			</form>
			@if ( $results )
			<div class="page-content-wrapper no-margin">
					<div class="single-result-manage">
						<table class="table table-hover table-bordered">
							<thead class="table-head">
								<tr>
									<th>S.No</th>
									<th>Title</th>
									<th>Status</th>
									<th>Action</th>
									<th>Edit</th>
								</tr>
							</thead>
							<tbody>
								<?php $serialNumber = 0; ?>
								@foreach ( $results as $result )
									<tr>
										<td>
											{{ ++$serialNumber }}
										</td>
										<td>
											<p>
									 		{{ ($result->type == 'Interview') ? 'Interview' : $result->listNumber.' list '}} 
									 		{{'of '. $result->gender. ' '. $result->program->title}}
									 		<span class="manage-published"> Published: {{ \Carbon\Carbon::parse($result->created_at)->format('d-m-Y') }}</span>
									 		</p>
									 		</td>
									 	<td>
									 		<span class="active-status">{{ ($result->active == '1') ? 'Active' : 'Suspended' }}</span>
									 	</td>
									 	<td>
									 		<form action={{route('changeStatus')}} id="change-status-form" method="POST">
												<input type="hidden" value="{{ $result->pkResultsId }}" name="result_id">
												<input type="hidden" name="current_status" id="current_status_input" value={{$result->active}}>
												<button type="submit" class="btn btn-{{ ($result->active == '1' ) ? 'warning' : 'primary' }}">
													{{ ($result->active == '1' ) ? 'Suspend' : 'Activate' }}
												</button>
											</form>
									 	</td>
									 	<td>
									 		<form action={{ route('getUploadResults', ['id' => $result->pkResultsId])}} id="edit-form">
									 			<input type="submit" class="btn btn-primary" value="Edit">
									 		</form>
									 	</td>
									</tr>
								@endforeach
							</tbody>
						</table>
					</div>
			</div> <!-- Ending page-content-wrapper -->
			@endif
		</div> <!-- Ending page-content row -->
	</div> <!-- Ending Container -->

	<script>
		$(function(){
			//refactor it
			$('#change-status-form button').on('click', function(){
				event.preventDefault();
				$this = $(this);
				changeStatusForm = $this.parent();
				$.ajax({
					method: 'POST',
					url: '{{route('changeStatus')}}',
					data: changeStatusForm.serialize(),
					success: function ( response ) {
						if ( response == '1' ) {
							//program is suspended, activate it
							$this.text('Suspend');	
							if ( $this.hasClass('btn-primary') ) {
								$this.removeClass('btn-primary');
								$this.addClass('btn-warning');
							}
							$this.parent().parent().prev().find('span.active-status').eq(0).text('Active');
							$this.prev().val(1);
						} else {
							//program is activated, suspend it
							$this.text('Activate');	
							if ( $this.hasClass('btn-warning') ) {
								$this.removeClass('btn-warning');
								$this.addClass('btn-primary');
							}
							$this.parent().parent().prev().find('span.active-status').eq(0).text('Suspended');
							$this.prev().val(0);
						}
					}
				});
			});

			$('#faculty').on('change', function(){
               $.ajax({
                   method: 'post',
                   url: '{{ route('getDepartmentsResults') }}',
                   data: {
                       id: $(this).val()
                   },
                   success: function ( response ) {
                   		console.log ( response );
                       $('#department').empty();
                       $('#department').append(
                            '<option value="">--Select Department--</option>'
                        );
                       $.each(response, function (key, value) {
                           $('#department').append(
                               '<option value="'+value.pkDeptId+'">'+ value.title+'</option>'
                           );
                       });
                   }
               });
            });

            $('#department').on('change', function(){
                $.ajax({
                    method: 'post',
                    url: '{{ route('getProgrammeResults') }}',
                    data: {
                        id: $(this).val()
                    },
                    success: function (response) {
                    	console.log ( response );
                        $('#programme').empty();
                        if ( response == '' ) {
                            $('#programme').append(
                                '<option value="">--Select Porgramme--</option>'
                            );
                            return;
                        }
                        $('#programme').append(
                            '<option value="">--Select Porgramme--</option>'
                        );
                        $.each(response, function (key, value) {
                            $('#programme').append(
                                '<option value="'+value.pkProgId+'">'+ value.title+'</option>'
                            );
                        });
                    }
                });
            });
		});
	</script>
@endsection