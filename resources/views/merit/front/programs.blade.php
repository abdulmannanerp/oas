@include('results.header')

	<div class="container-fluid fee-main-container">
		

<h4 class="row faculty-title d-flex justify-content-center pt-2 mb-2"> Programs Offered </h4>



	
			<div class="panel-group" id="accordion">
				@php
				$fac = '';
				$dep = '';
				$gender = '';
				$i = 0;
				// echo '<pre>'; 
				// print_r($programlisting);
				// echo '</pre>';
				@endphp
				@foreach($programlisting as $plist) 
					@if($fac != $plist->faculty->title) 
						@if($i != 0) 
							</div>
						@endif
						@if($i == 107)
						@php echo $i; @endphp
						@endif
						<div class="row faculty-title d-flex justify-content-center fee-clr bor-head pt-2" data-toggle="collapse" data-target="#demo-<?= $i ?>"><h4><?= ($plist->faculty->pkFacId != 8) ? 'Faculty of': ''; ?>  <?= $plist->faculty->title; ?></h4><i class="more-less fa fa-minus"></i></div>
						@if($plist->faculty->pkFacId == 6)
						<div class="row d-flex justify-content-center">
							<b>Note:</b> &nbsp; All Programs (MBA & BBA) of Faculty of Management Sciences are NBEAC Accredited.
						</div>
						@endif
						<div id="demo-<?= $i ?>" class="cls-faculty collapse show mb-2">
					@endif
						
					@if($dep != $plist->department->title) 
					<div class="row d-flex justify-content-center bor-head pt-2"><h4><?= $plist->department->title ?></h4></div>
							<div class="pr-main-div"><div class="pr-prog pr-style"><i>Program</i></div><div class="pr-gender pr-style">Offered To</div><div class="pr-req pr-style">Eligibility Criteria</div><div class="pr-date pr-style">Last Date</div><div class="pr-time pr-style">Test/Interview Date</div></div>
					@endif
					@if(is_object($plist->programscheduler))
					@if($plist->programscheduler->fkGenderId == 1) 
						@php $gender = 'Male & Female';  @endphp
					@elseif ($plist->programscheduler->fkGenderId == 2) 
						@php $gender = 'Female';  @endphp
					@elseif ($plist->programscheduler->fkGenderId == 3) 
						@php $gender = 'Male';  @endphp
					@endif
							@php
						$lastDate = new DateTime($plist->programscheduler->lastDate);
						
						$lastDate = $lastDate->format('d-M-Y');
						$testDateTime = new DateTime($plist->programscheduler->testDateTime);
						$testDateTime = $testDateTime->format('d-M-Y h:i A');
						 
						@endphp
						@if ($testDateTime == '30-Nov--0001 12:00 AM') 
							@php $testDateTime = ''; @endphp
						@else 
							 @php $testDateTime = '<b>Test:</b> ' . $testDateTime; @endphp
						@endif
						@php
						$interviewDateTime = new DateTime($plist->programscheduler->interviewDateTime);
						$interviewDateTime = $interviewDateTime->format('d-M-Y h:i A');
						@endphp
						
						@if ($interviewDateTime == '30-Nov--0001 12:00 AM')
							@php $interviewDateTime = ''; @endphp
						@else
							@php $interviewDateTime = '<b>Interview:</b> ' . $interviewDateTime; @endphp
						@endif
						@if ($plist->duration == 0)
							@php $duration = ''; @endphp
						@else
							@php $duration = ' (' . $plist->duration . ' Year)'; @endphp
						@endif
						@if ($plist->programscheduler->status == 1)
							@if ($plist->pkProgId == '189')
								<div class="pr-main-div"><div class="pr-prog"><i>BS Electrical Engineering (4 Year)</i></div><div class="pr-gender"><?= $gender ?></div><div class="pr-req">{!! $plist->requirements!!}</div><div class="pr-date">21-Dec-2018</div><div class="pr-time"><?= $testDateTime ?> <br /> <?= $interviewDateTime  ?> </div></div>  {{-- /*    Interview: */ --}}
							@else
								<div class="pr-main-div"><div class="pr-prog"><i><?= $plist->title . $duration ?></i></div><div class="pr-gender"><?= $gender ?></div><div class="pr-req">{!!$plist->requirements!!} </div><div class="pr-date"><?= $lastDate ?></div><div class="pr-time"><?= $testDateTime ?> <br /> <?= $interviewDateTime ?> </div></div>  {{-- /*    Interview: */ --}}
						@endif
						@if ($plist->pkProgId == '54')
							<!-- <div class="pr-main-div"><div class="pr-prog"><i>BS Mechanical Engineering (4 Year)</i></div><div class="pr-gender">Male</div><div class="pr-req">HSSC (Pre-Engineering) (Mathematics, Physics & Chemistry) or equivalent with minimum 60% marks and SSC (Science) or equivalent with minimum 60% marks.</div><div class="pr-date">21-Dec-2018</div><div class="pr-time"><b>Test: </b>04-Aug-2018 10:00 AM <br /> </div></div><div class="row d-flex justify-content-center bor-head pt-2"><h4>Department of Civil Engineering</h4></div><div class=""><div class="pr-prog pr-style"><i>Program</i></div><div class="pr-gender pr-style">Gender</div><div class="pr-req pr-style">Eligibility Criteria</div><div class="pr-date pr-style">Last Date</div><div class="pr-time pr-style">Test/Interview Date</div></div> <div class="pr-main-div"><div class="pr-prog"><i>BS Civil Engineering</i></div><div class="pr-gender">Male</div><div class="pr-req">HSSC (Pre-Engineering) (Mathematics, Physics & Chemistry) or equivalent with minimum 60% marks and SSC (Science) or equivalent with minimum 60% marks.</div><div class="pr-date">20-Jul-2018</div><div class="pr-time"><b>Test: </b>04-Aug-2018 10:00 AM <br /> </div></div> --> {{-- /*    Interview: */ --}}
						@endif
						@endif
						@endif
						@php
						$fac = $plist->faculty->title;
						$dep = $plist->department->title;
						$i++;
						@endphp
						
					@endforeach
					
		
		
				</div>
		
			











	</div> <!--Ending container --> 

@include('results.footer')