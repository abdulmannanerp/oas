<!DOCTYPE html>
<html>
<head>
	<title>Admission Results :: Fall 2018</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="" crossorigin="anonymous">
	<link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
	<link rel="stylesheet" href={{ url('css/merit-results.css') }}>
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
</head>
	
<body>
	<div class="container-fluid header">
		<img src={{ url('uploads/images/iiui-logo.jpg') }} id="iiui-top-banner" alt="iiui logo">
		<button type="login" class="btn btn-primary btn-sm center-block mylogin" id="lgn-btn">Login</button>
		<button type="login" class="btn btn-primary btn-sm center-block mylogin" id="lgn-btn" style="margin-right: 10px;">Signup</button>
	</div> <!-- Ending contianer-fluid header -->
	<nav class="navbar sticky-top navbar-expand-lg top-menu">
		<button class="navbar-toggler navbar-toggler-right ml-auto  float-xs-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		  </button>
		<div class="collapse navbar-collapse" id="navbarSupportedContent">
		  <ul class="navbar-nav mr-auto">
			<li class="nav-item active">
			  <a class="nav-link" href="http://admission.iiu.edu.pk/">Home <span class="sr-only">(current)</span></a>
			</li>
			{{-- <li class="nav-item">
			  <a class="nav-link" href="#">Link</a>
			</li> --}}
			<li class="nav-item dropdown">
			  <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				Faculties
			  </a>
			  <div class="dropdown-menu" aria-labelledby="navbarDropdown">
				<a class="dropdown-item al-fac-menu" href="#">All Faculties</a>
				<div class="dropdown-divider"></div>
				<a class="dropdown-item" href="http://www.iiu.edu.pk/?page_id=44" target="_blank">Arabic</a>
				<a class="dropdown-item" href="http://www.iiu.edu.pk/?page_id=50" target="_blank">Basic and Applied Sciences</a>
				<a class="dropdown-item" href="http://www.iiu.edu.pk/?page_id=56" target="_blank">Engineering & Technology</a>
				<a class="dropdown-item" href="http://www.iiu.edu.pk/?page_id=68" target="_blank">Islamic Studies (Usuluddin)</a>
				<a class="dropdown-item" href="http://www.iiu.edu.pk/?page_id=74" target="_blank">Languages Literature </a>
				<a class="dropdown-item" href="http://www.iiu.edu.pk/?page_id=80" target="_blank">Management Sciences</a>
				<a class="dropdown-item" href="http://www.iiu.edu.pk/?page_id=92" target="_blank">Social Sciences </a>
				<a class="dropdown-item" href="http://www.iiu.edu.pk/?page_id=62" target="_blank">International Institute of Islamic Economics </a>
				<a class="dropdown-item" href="http://www.iiu.edu.pk/?page_id=86" target="_blank">Shariah and Law</a>
			  </div>
			</li>
			<li class="nav-item">
			  <a class="nav-link" href="{{ url('programes') }}">Programs Offered</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="http://www.iiu.edu.pk/?page_id=299" target="_blank">Fee Structure</a>
			  </li>

			  <li class="nav-item">
				<a class="nav-link" href="{{ url('programes') }}">Eligibility Crateria</a>
			  </li>

			  <li class="nav-item">
				<a class="nav-link" href="http://www.iiu.edu.pk/?page_id=1121" target="_blank">ScholarShips</a>
			  </li>

			  <li class="nav-item dropdown">
				<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown2" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					Downloads</a>

					<div class="dropdown-menu" aria-labelledby="navbarDropdown2">
							<a class="dropdown-item" href="http://admission.iiu.edu.pk/admin/public/uploads/IIUI_Admission_Guide_2018.pdf" target="_blank">Admission Guide</a>
							<a class="dropdown-item" href="http://www.iiu.edu.pk/?page_id=1026#sample" target="_blank">Sample Papers</a>
					</div>
			  </li>
  
			  {{-- <li class="nav-item">
				<a class="nav-link" href="http://admission.iiu.edu.pk/rollnoslip">RollNo Slip </a>
			  </li>
  
			  <li class="nav-item">
				<a class="nav-link" href="http://admission.iiu.edu.pk/admin/public/results">Results</a>
			  </li> --}}
  
			  <li class="nav-item">
				<a class="nav-link" href="http://admission.iiu.edu.pk/contactus">Contact Us</a>
			  </li>
  
		  </ul>
		</div>
	  </nav>
