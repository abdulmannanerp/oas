@include('results.header')

	<div class="container-fluid fee-main-container">
		<h1 class="row row d-flex justify-content-center mt-2 mb-2 top-head" style="color: red;"> Under Construction </h1>
		<div class="row row d-flex justify-content-center mt-2 top-head">
			<h4>FEE STRUCTURE</h4>
		</div>
		<div class="row row d-flex justify-content-center mb-2 top-head">
			<h4>w.e.f {{$currentSemester[0]->title}}</h4>
		</div>
		<div class="row fee-view tp-fe-v">
			<div class="col-1">
				Sr #
			</div>
			<div class="col-3">
				Degree Program
			</div>
			<div class="col-1">
				Admission Fee
			</div>
			<div class="col-1">
				University Dues
			</div>
			<div class="col-1">
				Library Security
			</div>
			<div class="col-1">
				Book Bank Security
			</div>
			<div class="col-1">
				Caution Money
			</div>
			<div class="col-1">
				Total Fee
			</div>
			<div class="col-1">
				Hostel Dues
			</div>
			<div class="col-1">
				Hostel Security
			</div>
		</div>
		@php $fac=''; $dept= ''; $i = 0;  @endphp
		@foreach ($FeeView as $f)
			@if($fac != $f->faculty)
				@if($i != 0)
					</div>
				@endif
			<div class="row faculty-title d-flex justify-content-center fee-clr bor-head pt-2" data-toggle="collapse" data-target="#demo-<?= $i ?>"> <i class="more-less fa fa-minus"></i><h4>{{$f->faculty}}</h4></div>
			<div id="demo-<?= $i ?>" class="cls-faculty collapse show">
			@endif
			@if($dept != $f->department)
			<div class="row d-flex justify-content-center bor-head pt-2"><h4>{{$f->department}}</h4></div>
			@endif
			<div class="row fee-view">
				<div class="col-1 algn-cntre">
					{{ $loop->iteration }}
				</div>
				<div class="col-3">
					{{$f->program. ' '.$f->duration. ' year(s)'}}
				</div>
				<div class="col-1 algn-cntre">
					{{$f->admission_fee}}
				</div>
				<div class="col-1 algn-cntre">
					{{$f->university_dues}}
				</div>
				<div class="col-1 algn-cntre">
					{{$f->library_security}}
				</div>
				<div class="col-1 algn-cntre">
					{{$f->book_bank_security}}
				</div>
				<div class="col-1 algn-cntre">
					{{$f->caution_money}}
				</div>
				<div class="col-1 algn-cntre">
					{{$f->admission_fee+$f->university_dues+$f->library_security+$f->book_bank_security+$f->caution_money}}
				</div>
				<div class="col-1 algn-cntre">
					{{$f->hostel_dues}}
				</div>
				<div class="col-1 algn-cntre">
					{{$f->hostel_security}}
				</div>
			</div>
			

		@php
		$fac = $f->faculty;
		$dept = $f->department;
		$i++;
		@endphp
		@endforeach
	</div> <!--Ending container --> 

@include('results.footer')