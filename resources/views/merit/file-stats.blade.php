@extends('layouts.app')
@section('content')
	<div class="container">
		<section class="page-header row">
			<h2>File Statistics </h2>
		</section>
		<div class="page-content row">
			<div class="page-content-wrapper no-margin">
			<h3>File Statistics</h3>
			<h4>{{ $merit->program->title ?? ''}} </h4>
				<a href="{{route('viewFile', ['id' => $merit->id])}}" class="btn btn-link pull-right" target="_blank">View Uploaded File</a>
				<table class="table table-hover table-bordered">
					<thead class="table-head">
						<tr>
							<th> Total </th>
							<th> Unique </th>
							<th> Duplicate </th>
							<th> Invalid </th>
							<th> Not exists in current program</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td> {{ $totalCount }} </td>
							<td> {{ $uniqueCount }}</td>
							<td> {{ $duplicateCount }}</td>
							<td></td>
							<td></td>
						</tr>
					</tbody>
				</table>
				
				@if ( $duplicates )
					<hr>
					<h4>Duplicate Roll Numbers</h4>
					<table class="table table-hover table-bordered">
						<thead class="table-head">
							<tr>
								<th> Roll Numbers </th>
							</tr>
						</thead>
						<tbody>
							@foreach ( $duplicates as $value  )
							<tr>
								<td> {{ $value }} </td>
							</tr>
							@endforeach 
						</tbody>
					</table>
				@endif
				<form action="{{ route('storeMerit')}} " method="POST">
					@if ( !$duplicates )
						<input type="hidden" name="id" value="{{ $merit->id }}">
					@endif
					<button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-paste"></i> Save </button>
					<a href="{{route('merit', ['id' => $merit->id])}}" class="btn btn-default btn-sm">Reupload File</a>
				</form>
			</div> <!-- Ending page-content-wrapper -->
		</div> <!-- Ending page-content row -->
	</div> <!-- Ending Container -->
@endsection