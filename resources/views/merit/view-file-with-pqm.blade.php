@extends('layouts.app')
@section('content')
	<div class="container">
		<section class="page-header row">
			<h2>Result With PQM</h2>
		</section>
		<div class="page-content row">
			<div class="page-content-wrapper no-margin">
			<h3>{{ $merit->type . ' | ' . $merit->program->title . ' | ' . $merit->gender }}</h3>
			<form action="#" method="POST" class="pull-right">
				<input type="hidden" name="id" value="{{ $merit->id }}">
				<button name="save" type="submit" class="btn btn-sm btn-primary" title="revert"> Delete Merit </button> 
			</form>

			<a href="{{ route('manage', ['id' => $merit->id]) }}" class="btn btn-sm btn-primary pull-right mr5">Manage</a>
			<table class="table table-hover table-bordered">
				<thead class="table-head">
					<tr>
						<th> S.No </th>
						<th> Roll Number </th>
						<th> Marks </th>
						<th> PQM </th>
						<th> Total </th>
					</tr>
				</thead>
				<tbody>
					@php 
						$i=1; 
						$marks = ($merit->type == 'Result') ? 'test_marks' : 'interview_marks';
					@endphp
					@foreach ( $merit->meritList as $list ) 
					<tr>
						<td> {{ $i }} </td>
						<td> {{ $list->rollno }}</td>
						<td> {{ $list->$marks }}</td>
						<td> {{ $list->pqm }}</td>
						<td> {{ $list->total }}</td>
					</tr>
					@php $i++; @endphp
					@endforeach
				</tbody>
			</table>
			</div> <!-- Ending page-content-wrapper -->
		</div> <!-- Ending page-content row -->
	</div> <!-- Ending Container -->
@endsection