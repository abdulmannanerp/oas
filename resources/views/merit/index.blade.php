@extends('layouts.app')
@section('content')
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
	<div class="container">
		@if ( $status )
			<div class="alert alert-success">
				{{ $status }}
			</div>
		@endif
		<section class="page-header row">
			<h2> Merit </h2>
		</section>
		<div class="page-content row">
			<div class="page-content-wrapper no-margin">
				@if($error)
					<div class="alert alert-danger">
				        <p>{{$error}}</p>
				    </div>
				@endif
				<form method="POST" action="{{ route('postMerit') }}" accept-charset="UTF-8" class="form-horizontal validated" enctype="multipart/form-data">
					{{ csrf_field() }}
					<input type="hidden" value="{{auth()->id()}}" name="userId">
					<input type="hidden" name="id" value={{$merit ? $merit->id : ''}}>
					<div class="sbox">
						<div class="sbox-content clearfix">
							<div class="form-group  ">
								<div class="row">
									<div class="col-md-4">
										<label for="Title" class=" control-label col-md-4 text-left"> Programs </label>
									</div>
									<div class="col-md-6">
									  <select name="programme" id="programme" class="form-control input-sm select2" required>
									  	<option value="">--Select Program--</option>
								  		@foreach ( $programs as $program )
								  			<option value="{{$program->pkProgId}}" {{($merit && $merit->fkProgId == $program->pkProgId ) ? 'Selected' : ''}}> {{ $program->title}} </option>
								  		@endforeach
									  </select>
									</div> 
								</div> <!-- Ending row -->
							</div> <!-- Ending form-group -->
						
							<div class="form-group  ">
								<div class="row">
									<div class="col-md-4">
										<label for="Title" class=" control-label col-md-4 text-left"> Gender </label>
									</div>
									<div class="col-md-6">
										<div class="iradio_square-green" style="position: relative;">
										  	<input type="radio" name="selectGender" value="Male" class="minimal-red" style="position: absolute; opacity: 0;" data-parsley-multiple="selectGender"  required {{ $merit && $merit->gender=='Male' ? 'Checked' : ''}}>
										</div> Male 
					
										<div class="iradio_square-green" style="position: relative;">
											<input type="radio" name="selectGender" value="Female" class="minimal-red" data-parsley-multiple="selectGender" style="position: absolute; opacity: 0;" required {{ $merit && $merit->gender=='Female' ? 'Checked' : ''}}>
										</div> Female  
									</div> 
								</div> <!-- Ending row -->
							</div> <!-- Ending form-group -->

							<div class="form-group  ">
								<div class="row">
									<div class="col-md-4">
										<label for="Title" class=" control-label col-md-4 text-left"> Type </label>
									</div>
									<div class="col-md-6">
										<div class="iradio_square-green" style="position: relative;">
										  		<input type="radio" name="resultType" value="Result" class="minimal-red" style="position: absolute; opacity: 0;" data-parsley-multiple="resultType" required {{ $merit && $merit->type=='Result' ? 'Checked' : ''}}>
										</div> Result 
					
										<div class="iradio_square-green" style="position: relative;">
											<input type="radio" name="resultType" value="Interview" class="minimal-red" data-parsley-multiple="resultType" style="position: absolute; opacity: 0;" required {{ $merit && $merit->type=='Interview' ? 'Checked' : ''}}>
										</div> Interview  
									</div> 
								</div> <!-- Ending row -->
							</div> <!-- Ending form-group -->

							<div class="form-group  ">
								<div class="row">
									<div class="col-md-4">
										<label for="Title" class=" control-label col-md-4 text-left"> Factor </label>
									</div>
									<div class="col-md-6">
									  <input type="number" class="form-control" name="factor" value="{{ $merit && $merit->factor ? $merit->factor : 0 }}">
									</div> 
								</div> <!-- Ending row -->
							</div> <!-- Ending form-group -->


							<div class="form-group  ">
								<div class="row">
									<div class="col-md-4">
										<label for="Title" class=" control-label col-md-4 text-left"> Select File </label>
									</div>
									<div class="col-md-6">
										<input name="meritFile" type="file" class="form-control input-sm" accept=".csv" required>
										<a href="{{asset('uploads/sample/Merit.csv')}}" class="btn btn-link pull-right">Download Sample File</a>
									</div> 
								</div> <!-- Ending row -->
							</div> <!-- Ending form-group -->

							<div class="form-group  ">
								<div class="row">
									<div class="col-md-4">
										
									</div>
									<div class="col-md-6">
										<button name="apply" class="tips btn btn-sm btn-primary btn-apply" type="submit" title="Back"><i class="fa  fa-check"></i> Next </button>
									</div> 
								</div> <!-- Ending row -->
							</div> <!-- Ending form-group -->

						</div> <!-- Ending sbox-content -->
					</div> <!-- Ending sbox -->
				</form>
			</div> <!-- Ending page-content-wrapper -->
		</div> <!-- Ending page-content row -->
	</div> <!-- Ending Container -->

	<script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
	<script>
        $(function(){
            $('#faculty').on('change', function(){

               $.ajax({
                   method: 'post',
                   url: '{{ route('getDepartments') }}',
                   data: {
                       id: $(this).val()
                   },
                   success: function ( response ) {
                       $('#department').empty();
                       $('#department').append(
                            '<option value="">--Select Department--</option>'
                        );
                       $.each(response, function (key, value) {
                           $('#department').append(
                               '<option value="'+value.pkDeptId+'">'+ value.title+'</option>'
                           );
                       });
                   }
               });
            });

            $('#department').on('change', function(){
                $.ajax({
                    method: 'post',
                    url: '{{ route('getProgramme') }}',
                    data: {
                        id: $(this).val()
                    },
                    success: function (response) {
                        $('#programme').empty();
                        if ( response == '' ) {
                            $('#programme').append(
                                '<option value="">--Select Porgramme--</option>'
                            );
                            return;
                        }
                        $('#programme').append(
                            '<option value="">--Select Porgramme--</option>'
                        );
                        $.each(response, function (key, value) {
                            $('#programme').append(
                                '<option value="'+value.pkProgId+'">'+ value.title+'</option>'
                            );
                        });
                    }
                });
            });

            $('#start-date').flatpickr();
	        $('#end-date').flatpickr();
        });
    </script>

@endsection