@extends('layouts.app')
@section('content')
	<div class="container">
		<section class="page-header row">
			<h2> List Records </h2>
		</section>
		<div class="page-content row">
			<div class="page-content-wrapper no-margin">
				<table class="table table-hover table-bordered">
					<thead class="table-head">
						<tr>
							<th> S.No</th>
							<th> Roll Number </th>
							<th> Marks </th>
							<th> PQM </th>
							<th> Total </th>
						</tr>
					</thead>
					<tbody>
						@php $counter = 1; @endphp
						@foreach ( $records as $record )
						<tr>
							<td> {{ $counter }} </td>
							<td> {{ $record[0] }}</td>
							<td> {{ $record[1] }}</td>
							<td> {{ $record[2] ?? '' }}</td>
							<td> {{ $record[1] + $record[2] ?? '' }}</td>
						</tr>
						@php $counter++; @endphp
						@endforeach
					</tbody>
				</table>
				<form action="{{ route('storeMerit')}} " method="POST">
					<input type="hidden" name="data" value="{{ $data }}">
					<button name="save" type="submit" class="tips btn btn-sm btn-save btn-primary" title="Back"><i class="fa fa-paste"></i> Save </button> 
					<a href="{{route('merit')}}" class="btn btn-default btn-sm">Cancel</a>
				</form>
			</div> <!-- Ending page-content-wrapper -->
		</div> <!-- Ending page-content row -->
	</div> <!-- Ending Container -->
@endsection