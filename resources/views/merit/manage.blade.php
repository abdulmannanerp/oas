@extends('layouts.app')
@section('content')
	<div class="container">
		<section class="page-header row">
			<h2> Meirt Cut Point </h2>
		</section>
		@if ( $error ) 
			<div class="alert alert-danger mt5">
				{{ $error }}				
			</div>
		@endif
		@if ( $status ) 
			<div class="alert alert-success mt5">
				{{ $status }}				
			</div>
		@endif
		<div class="page-content row">
			<div class="page-content-wrapper no-margin">
				<h4> {{ $merit->program->title . ' - ' . $merit->gender .' - ' . $merit->type}}</h4>
				<form method="POST" action="{{ route('storeCutPoint') }}" accept-charset="UTF-8" class="form-horizontal validated" enctype="multipart/form-data">
					{{ csrf_field() }}
					<input type="hidden" value="{{ $merit->id}}" name="merit">
					<input type="hidden" value="{{auth()->id()}}" name="userId">
					<div class="sbox">
						<div class="sbox-content clearfix">							
							<div class="form-group">
								<div class="row">
									<div class="col-md-4">
										<label for="Title" class=" control-label col-md-4 text-left"> Cut Point </label>
									</div>
									<div class="col-md-6">
									  <input type="number" class="form-control" name="cutpoint" min="1"  required>
									</div> 
								</div> <!-- Ending row -->
							</div> <!-- Ending form-group -->
							
							<div class="form-group  ">
								<div class="row">
									<div class="col-md-4">
										<label for="Title" class=" control-label col-md-4 text-left"> List Number </label>
									</div>
									<div class="col-md-6">
									  <select name="listNumber" id="listNumber" class="form-control input-sm" required>
									  	<option value=""> --Select List-- </option>
									  	<option value="First">1st</option>
									  	<option value="Second">2nd</option>
									  	<option value="Third">3rd</option>
									  	<option value="Fourth">4th</option>
									  	<option value="Fifth">5th</option>
									  	<option value="Sixth">6th</option>
									  </select>
									</div> 
								</div> <!-- Ending row -->
							</div> <!-- Ending form-group -->
						</div> <!-- Ending sbox-content -->
						<div class="sbox-title clearfix">
							<div class="sbox-tools pull-left">
								<button name="apply" class="tips btn btn-sm btn-apply" type="submit" title="Back"><i class="fa  fa-check"></i> Save </button>
							</div>
						</div>
					</div> <!-- Ending sbox -->
				</form>
				@if ( $programCutPoints->count() )
					<h3>{{ $merit->program->title }} Cut Points</h3>
					<ul>
					@foreach ( $programCutPoints as $cutpoint ) 
						<li>
							{{ $cutpoint->list }} List at cut point {{ $cutpoint->cut_point }} created at {{ $cutpoint->created_at->format('d-M-Y H:i:s A') }}
							<br>
							<a href="{{route('publishOrSuspendList', ['id' => $cutpoint->id])}}" class="fff btn btn-sm btn-success mb5">
								{{ ($cutpoint->status == 1 ) ? 'Suspend' : 'Publish' }}
							</a>
							<a href="{{route('viewCutpointList', ['id' => $cutpoint->id, 'list' => $cutpoint->list])}}" target="_blank" class="btn btn-sm btn-primary mb5">View List</a>
						</li>
					@endforeach
					</ul>
				@else
					<p>No cut points found for selected program</p>
				@endif
				<hr>
				{{-- <h3>Recent Cut Points</h3>
				<ul>
				@foreach ( $recentCutPoints as $cutpoint )
					<li>
						{!! $cutpoint->faculty->title . ' > ' . $cutpoint->department->title . ' > '. $cutpoint->program->title . ' Cut Point at <strong>'. $cutpoint->cut_point . '</strong> on list <strong>' .$cutpoint->list .'</strong> By '. $cutpoint->user->first_name. ' '. $cutpoint->user->last_name !!}
					</li>
				@endforeach --}}
				</ul>
			</div> <!-- Ending page-content-wrapper -->
		</div> <!-- Ending page-content row -->
	</div> <!-- Ending Container -->

	<script>
        $(function(){
            $('#faculty').on('change', function(){

               $.ajax({
                   method: 'post',
                   url: '{{ route('getDepartments') }}',
                   data: {
                       id: $(this).val()
                   },
                   success: function ( response ) {
                       $('#department').empty();
                       $('#department').append(
                            '<option value="">--Select Department--</option>'
                        );
                       $.each(response, function (key, value) {
                           $('#department').append(
                               '<option value="'+value.pkDeptId+'">'+ value.title+'</option>'
                           );
                       });
                   }
               });
            });

            $('#department').on('change', function(){
                $.ajax({
                    method: 'post',
                    url: '{{ route('getProgramme') }}',
                    data: {
                        id: $(this).val()
                    },
                    success: function (response) {
                        $('#programme').empty();
                        if ( response == '' ) {
                            $('#programme').append(
                                '<option value="">--Select Porgramme--</option>'
                            );
                            return;
                        }
                        $('#programme').append(
                            '<option value="">--Select Porgramme--</option>'
                        );
                        $.each(response, function (key, value) {
                            $('#programme').append(
                                '<option value="'+value.pkProgId+'">'+ value.title+'</option>'
                            );
                        });
                    }
                });
            });
        });
    </script>
@endsection