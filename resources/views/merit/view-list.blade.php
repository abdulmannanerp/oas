@extends('layouts.app')
@section('content')
	<div class="container">
		<section class="page-header row">
			<h2>File Statistics </h2>
		</section>
		<div class="page-content row">
			<div class="page-content-wrapper no-margin">
			{{-- <h3>File Statistics of {{ $merit->program->title ?? ''}}</h3> --}}
				{{-- <a href="{{route('viewFile', ['id' => $merit->id])}}" class="btn btn-link pull-right">View Uploaded File</a> --}}
				<table class="table table-hover table-bordered">
					<thead class="table-head">
						<tr>
							<th> id </th>
							<th> Roll Number </th>
							<th> Test Marks </th>
							<th> Interview Marks</th>
							<th> PQM </th>
							<th> Total </th>
						</tr>
					</thead>
					<tbody>
						@php $i=1; @endphp
						@foreach ( $records as $record ) 
						<tr>
							<td> {{ $i }} </td>
							<td> {{ $record->rollno }}</td>
							<td> {{ $record->test_marks }}</td>
							<td> {{ $record->interview_marks}}</td>
							<td> {{ $record->pqm }} </td>
							<td> {{ $record->total }} </td>
						</tr>
						@php $i++; @endphp
						@endforeach
					</tbody>
				</table>
			</div> <!-- Ending page-content-wrapper -->
		</div> <!-- Ending page-content row -->
	</div> <!-- Ending Container -->
@endsection