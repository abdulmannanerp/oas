@extends('layouts.app')
@section('content')
	<div class="container">
		<section class="page-header row">
			<h2> Merit Listing </h2>
		</section>
		<div class="page-content row">
			<div class="page-content-wrapper no-margin">
        <form action="{{ route('searchListing') }}" method="POST">
          <div class="form-group">
            <label for="program">Select Program</label>
            <select name="program" id="program" class="select2 form-control">
              @foreach ( $lists as $list )
                <option value="{{$list->id}}">{{ $list->program->title}}</option>
              @endforeach
            </select>
          </div>
        <div class="form-group">
          <button class="btn btn-sm btn-primary">Search</button>
        </div>
        </form>
        <h3> Recent Cut Points  </h3>
				<ul class="recent-merit-listing">
				@foreach ( $lists as $list )
					<div class="single-merit-list">
						<li> {{ $list->program->title . ' - ' . $list->gender . ' - ' . $list->type }} </li>
            <p> {{ $list->created_at->format('d-M-Y H:i:s A')}} </p>
						<a href="{{route('manage', ['id' => $list->id])}}" class="btn btn-primary btn-sm mb5">Add Cut Point</a>
            {{-- <br> --}}
            {{-- <a href="#" class="btn btn-danger btn-sm">Delete Merit</a> --}}
					</div>
				@endforeach
				</ul>
			</div> <!-- Ending page-content-wrapper -->
		</div> <!-- Ending page-content row -->
	</div> <!-- Ending Container -->

	<script>
        $(function(){
            $('#faculty').on('change', function(){

               $.ajax({
                   method: 'post',
                   url: '{{ route('getDepartments') }}',
                   data: {
                       id: $(this).val()
                   },
                   success: function ( response ) {
                       $('#department').empty();
                       $('#department').append(
                            '<option value="">--Select Department--</option>'
                        );
                       $.each(response, function (key, value) {
                           $('#department').append(
                               '<option value="'+value.pkDeptId+'">'+ value.title+'</option>'
                           );
                       });
                   }
               });
            });

            $('#department').on('change', function(){
                $.ajax({
                    method: 'post',
                    url: '{{ route('getProgramme') }}',
                    data: {
                        id: $(this).val()
                    },
                    success: function (response) {
                        $('#programme').empty();
                        if ( response == '' ) {
                            $('#programme').append(
                                '<option value="">--Select Porgramme--</option>'
                            );
                            return;
                        }
                        $('#programme').append(
                            '<option value="">--Select Porgramme--</option>'
                        );
                        $.each(response, function (key, value) {
                            $('#programme').append(
                                '<option value="'+value.pkProgId+'">'+ value.title+'</option>'
                            );
                        });
                    }
                });
            });
        });
    </script>
@endsection