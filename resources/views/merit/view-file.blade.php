@extends('layouts.app')
@section('content')
	<div class="container">
		<section class="page-header row">
			<h2>File Statistics </h2>
		</section>
		<div class="page-content row">
			<div class="page-content-wrapper no-margin">
			<h3>File Statistics of {{ $merit->program->title ?? ''}}</h3>
				{{-- <a href="{{route('viewFile', ['id' => $merit->id])}}" class="btn btn-link pull-right">View Uploaded File</a> --}}
				<table class="table table-hover table-bordered">
					<thead class="table-head">
						<tr>
							<th> id </th>
							<th> Roll Number </th>
							<th> Marks </th>
						</tr>
					</thead>
					<tbody>
						@php $i=1; @endphp
						@foreach ( $records as $record ) 
						<tr>
							<td> {{ $i }} </td>
							<td> {{ $record[0] }}</td>
							<td> {{ $record[1] }}</td>
						</tr>
						@php $i++; @endphp
						@endforeach
					</tbody>
				</table>
				<form action="{{ route('storeMerit')}} " method="POST">
					<input type="hidden" name="id" value="{{ $merit->id }}">
					<button name="save" type="submit" class="tips btn btn-sm btn-save btn-primary" title="Back"> Save </button> 
					<a href="{{route('merit', ['id' => $merit->id])}}" class="btn btn-default btn-sm">Reupload File</a>
				</form>
			</div> <!-- Ending page-content-wrapper -->
		</div> <!-- Ending page-content row -->
	</div> <!-- Ending Container -->
@endsection