@extends('layouts.app')

@section('content')
    <div class="page-content row">
        <div class="page-content-wrapper m-t">
            @if ( count($records) != 0 )
                <div class="table-responsive" style="padding-bottom: 70px;">
                    <form action="{{route('failed')}}" method="POST" class="pull-right">
                        <button class="btn btn-primary btn-sm">Export To CSV</button>
                    </form>
                    <h3>Failed Records</h3>
                    <table class="table table-hover table-bordered app-search-grid" id="ManualVerifyTable">
                        <thead class="table-head">
                        <tr>
                            <th>Challan #</th>
                            <th>Created At</th>
                        </tr>
                        </thead>

                        <tbody>
                        @foreach ( $records as $record)
                            <tr>
                                <td>{{ $record->form_number }} </td>
                                <td> {{ $record->created_at->format('d-M-Y H:i:s')}} </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                {{$records->links()}}
            @else
                <h5>No records found</h5>
            @endif
        </div>
    </div>
@stop
