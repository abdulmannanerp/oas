@extends('layouts.app')


@section('content')
    <div class="page-content row">
        <div class="page-content-wrapper m-t">
            @if ( $status ) 
                <div class="alert alert-success mt5 mb5">
                    {{ $status }}
                </div>
            @endif

            <div class="alert alert-danger">
                <strong>Fee challans verified here must be re-consile with bank on daily basis. In case of any query kindly call/email at 2799/abdul.mannan@iiu.edu.pk</strong>
            </div>
            
            <form action="{{route('challanverify')}}" method="POST" id="application-numbers-form">
                {{ csrf_field() }}
                <div class="form-group">
                        <label for="">Enter Challan Numbers</label>
                        <textarea name="application_ids" id="application-ids-textbox" class="form-control" placeholder="171005,758000,875520" required></textarea>
                </div> <!-- Ending Form Group -->
                <div class="form-group">
                    <input type="submit" class="btn btn-primary" value="List Records" name="list_records">
                </div>
            </form>

            <form action="{{route('challanverify')}}" method="POST" enctype="multipart/form-data" id="upload-csv-form">
                <div class="form-group">
                    {{ csrf_field() }}
                    <label for="">Upload CSV</label>
                    <input type="file" name="csv" class="btn btn-default" accept=".csv" required>
                </div>
                <div class="form-group">
                    <input type="submit" class="btn btn-primary" value="Uplaod & List" name="upload_and_list_csv">
                </div>
            </form>

            @if ( session('verifiedApplications') )
                <h4 class="applicatoins-verified-relist">Following applications has been verified</h4>
                <ul>
                @foreach ( session('verifiedApplications') as $app )
                    <li> {{ $app->id }}</li>
                @endforeach
                </ul>
            @endif


            @if ( $applications )
                <div class="table-responsive">
                    <table class="table table-hover table-bordered app-search-grid" id="ManualVerifyTable">
                        <thead class="table-head">
                            <tr>
                                <th>Challan #</th>
                                <th>Name</th>
                                <th>Roll Number</th>
                                <th>CNIC</th>
                                <th>Faculty</th>
                                <th>Department</th>
                                <th>Programme</th>

                                <th>Admission Fee</th>
                                <th>University Dues</th>
                                <th>Library Security</th>
                                <th>Book Bank Security</th>
                                <th>Caution Money</th>

                                <th>Status</th>
                                <th>Verified By</th>
                                <th>Date</th>
                            </tr>
                        </thead>
                        <tbody>
                        @php $allApplications = []; @endphp
                        @foreach ( $applications as $application )
                            @if ( $application->applicant != '' )
                            <tr>
                                <td>
                                    @if($application->second_challan != '')
                                        {{ $application->second_challan }}
                                    @else
                                        {{ $application->id }}
                                    @endif
                                
                                </td>
                                 <td>{{ $application->applicant->applicant->name }}</td>

                                 <td>{{ $application->fkRollNumber }}</td>
                                 <td>{{ $application->applicant->applicant->cnic }}</td>

                                 <td>{{ $application->program->faculty->title?? '' }}</td>
                                 <td>{{ $application->program->department->title?? '' }}</td>
                                <td>{{ $application->program->title?? '' }}</td>

                                <td>{{$application->admission_fee?? ''}}</td>
                                <td>{{$application->university_dues?? ''}}</td>
                                <td>{{$application->library_security?? ''}}</td>
                                <td>{{$application->book_bank_security?? ''}}</td>
                                <td>{{$application->caution_money?? ''}}</td>

                                <td>
                                    @if ( $application->status >= 4)
                                        <span class="fee-already-verified">Fee Already Verified</span>
                                    @else
                                        <span class="fee-not-verified">Unverified</span>
                                    @endif
                                </td>
                                <td> {{ @$application->user->first_name .' '. @$application->user->last_name }} </th>
                                <td>
                                    @if ( $application->fee_verified_date )
                                        {{ date('d-M-Y h:i A', strtotime($application->fee_verified_date)) ?? '' }}
                                    @else
                                        <span></span>
                                    @endif
                                </td>
                            </tr>
                            @endif
                                @php $allApplications[] = $application->id; @endphp
                        @endforeach
                        </tbody>
                    </table>
                    <form action="{{route('challanverify')}}" method="POST">
                        <p class="incomplete-application-verification-message">(Incomplete application will not be verified)</p>
                        <input type="hidden" name="challanverify" value={{ serialize ( $allApplications) }}>
                        <input type="submit" value="Verify All" class="btn btn-primary">
                    </form>
                </div>
            @endif
            {{-- @if ( $applications_not_exists )
                <h3>Application(s) not found</h3>
                <ol>
                @foreach ( $applications_not_exists as $application)
                     <li> {{ $application }}</li>
                @endforeach
                </ol>
            @endif --}}
        </div>
    </div>
@stop
