@extends('layouts.app')


@section('content')
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">

    <div class="page-content row">
        <div class="page-content-wrapper m-t">

            <div class="alert alert-danger">
                <strong>Fee challans verified here must be re-consile with bank on daily basis. In case of any query kindly call/email at 2799/abdul.mannan@iiu.edu.pk</strong>
            </div>

            <h3>Semester Fee Verified</h3>
            <div class="filter-and-export">
                <form action="{{ route('chverified') }}" method="GET" class="varified-page-filter-form">
                    <table>
                        <tr><label for="filter-records">Filter Records:</label></tr>
                        <tr>
                            <td><input type="text" id="date-from" placeholder="Date From" name="start_date" required></td>
                            <td><input type="text" id="date-to" placeholder="Date To" name="end_date" required></td>
                            <td><input type="submit" class="btn btn-primary" value="Search"></td>
                        </tr>
                    </table>
                </form>
                <div class="verified-export-to-csv-btn pull-right">
                    <form action="{{route('chverified')}}" method="POST">
                        <input type="hidden" name="start_date" value={{$startDate ?? ''}}>
                        <input type="hidden" name="end_date" value={{$endDate ?? ''}}>
                        <input type="hidden" name="export" value='export'/>
                        <button type="submit" class="btn btn-primary btn-sm">Export to CSV</button>
                    </form>
                </div>
            </div>
            <div class="table-responsive">
                @if ( $applications->total() ) 
                    <h3>Semester Fee Verified Applications {{ '('.$applications->total().')' ?? '(0)' }}</h3>
                @endif
                
                @if ( count($applications) != 0 )
                
                <table class="table table-hover table-bordered app-search-grid" id="ManualVerifyTable">
                    <thead class="table-head">
                    <tr>
                        <th>Serial No</th>
                        <th>Challan #</th>
                        <th>Name</th>
                        <th>Roll #</th>
                        <th>CNIC</th>
                        <th>Faculty</th>
                        <th>Department</th>
                        <th class="table-program-column">Programme</th>
                        <th>Admission Fee</th>
                        <th>University Dues</th>
                        <th>Library Security</th>
                        <th>Book Bank Security</th>
                        <th>Caution Money</th>
                        <th>Total</th>
                        <th>Application Status</th>
                        <th>Verified By</th>
                        <th>Date</th>
                    </tr>
                    </thead>

                    <tbody>
                    @php $i = $applications->perPage() * ($applications->currentPage() - 1); @endphp
                    @foreach ( $applications as $application)
                        @if ( $application->applicant != '' )
                        <tr>
                            <td> {{ ++$i }} </td>
                            <td>
                                    @if($application->second_challan != '')
                                    {{ $application->second_challan }}
                                @else
                                    {{ $application->id }}
                                @endif
                            </td>
                            <td class="left-align">{{ $application->applicant->applicant->name }}</td>
                            <td>{{ $application->applicant->getRollNumber->id}}</td>
                            <td>{{ $application->applicant->applicant->cnic }}</td>
                            <td>{{ $application->program->faculty->title ?? '' }}</td>
                            <td>{{ $application->program->department->title ?? '' }}</td>
                            <td>{{ $application->program->title ?? '' }}</td>

                            <td>{{$application->admission_fee?? ''}}</td>
                            <td>{{$application->university_dues?? ''}}</td>
                            <td>{{$application->library_security?? ''}}</td>
                            <td>{{$application->book_bank_security?? ''}}</td>
                            <td>{{$application->caution_money?? ''}}</td>
                            <td>{{$application->admission_fee+$application->university_dues+$application->library_security+$application->book_bank_security+$application->caution_money}}</td>

                            <td><span class="fee-already-verified">{{ 'Verified' }}</span></td>                                
                            <td>{{ $application->user->first_name. ' '. $application->user->last_name }}</td>
                            <td>{{ date ( 'd-M-Y h:i A',  strtotime($application->fee_verified_date)) }}</td>
                        </tr>
                        @endif
                    @endforeach
                    </tbody>
                </table>
            </div>
                {{ $applications->appends(['start_date' => $startDate, 'end_date' =>  $endDate])->links() }}
            @else
                <h5>There are no application with verified fee</h5>
            @endif
        </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
    <script>
        $('#date-from').flatpickr();
        $('#date-to').flatpickr();
        document.querySelector('#date-from').value = '{{request('start_date')}}';
        document.querySelector('#date-to').value = '{{request('end_date')}}';
    </script>

@stop
