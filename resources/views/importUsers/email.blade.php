<!DOCTYPE html>
<html lang="en-US">	
    <head>
        <meta charset="utf-8">	
    </head>	
    <body>
        Respected <strong>{{$first_name .' '. $last_name}}</strong>, <br /><br />
        Hope this email finds you well. we are sharing online admission credentials with you. Please keep information handy and do contact IT office in case of any issue.<br /><br />
        URL: <a href="http://admission.iiu.edu.pk/admin">http://admission.iiu.edu.pk/admin</a><br />
        email: {{$email}} <br />
        password: {{$password}}

        <h3>Best Regards</h3>
        <h4>Admission Office</h4>
        <p>International Islamic University, Islamabad (IIUI)</p>
        <p>http://admission.iiu.edu.pk/</p>
        <br/><br/>
        <div style="font-size:smaller;background-color: #3c763d;color: #fff;">This communication contains information that is for the exclusive use of the intended recipient(s). If you are not the intended recipient, disclosure, copying, distribution or other use of, or taking of any action in reliance upon, this communication or the information in it is prohibited and may be unlawful. If you have received this communication in error please notify the sender by return email, delete it from your system and destroy any copies.</div>
    </body>
</html>