@extends('layouts.app')
@section('content')
	<div class="container">
		{{-- {{ $status }} --}}
		<section class="page-header row">
			<h2> Import Users </h2>
		</section>
		<div class="page-content row">
			<div class="page-content-wrapper no-margin">
				@if($errors->any())
					<div class="alert alert-danger">
				        <ul>
				        	@foreach ( $errors->all() as $error ) 
				            	<li>{{ $error }}</li>
				            @endforeach
				        </ul>
				    </div>
				@endif
				@if($alreadyInDb)
					<div class="alert alert-danger">
						<ul>
							<li>Following invalid Entries are not inserted. because they already exists</li>
							<li>{{ $alreadyInDb }}</li>
						</ul>
					</div>
				@endif
				<form method="POST" action={{route('user-import')}} accept-charset="UTF-8" class="form-horizontal validated" enctype="multipart/form-data">
					{{ csrf_field() }}
					<input type="hidden" value="{{auth()->id()}}" name="userId">
					<div class="sbox">
						<div class="sbox-content clearfix">
							<div class="form-group">
								<div class="row">
									<div class="col-md-4">
										<label for="Title" class=" control-label col-md-4 text-left"> Select File </label>
									</div>
									<div class="col-md-6">
										<input name="fileToUplaod" type="file" class="form-control input-sm" required>
									</div> 
								</div> <!-- Ending row -->
							</div> <!-- Ending form-group -->
							<div class="form-group">
								<div class="row">
									<div class="col-md-4">
										<label for="Title" class=" control-label col-md-4 text-left"> Group </label>
									</div>
									<div class="col-md-6">
										<select name="group" id="group" class="form-control input-sm" required>
											<option value="">--Select Group--</option>
											@foreach ( $groups as $group )
												<option value="{{$group->group_id}}">{{$group->name}}</option>
											@endforeach
										</select>
									</div> 
								</div> <!-- Ending row -->
							</div> <!-- Ending form-group -->
						</div> <!-- Ending sbox-content -->
						<div class="sbox-title clearfix">
							<div class="sbox-tools pull-left">
								<button name="apply" class="tips btn btn-sm btn-apply" type="submit" title="Back"><i class="fa  fa-check"></i> Import </button>
							</div>
						</div>
					</div> <!-- Ending sbox -->
				</form>
			</div> <!-- Ending page-content-wrapper -->
		</div> <!-- Ending page-content row -->
	</div> <!-- Ending Container -->
@endsection