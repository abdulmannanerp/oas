<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Fee Challan</title>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
    <link href="{{ asset('css/fee-challan.css') }}" rel="stylesheet" type="text/css" media="all">
    <link rel="stylesheet" href="https://printjs-4de6.kxcdn.com/print.min.css">
    <script src="https://printjs-4de6.kxcdn.com/print.min.js"></script>
</head>

<body>
    <div class="print-and-download-container">
        <button
            onclick="printJS
    ({
      printable: 'fee-challan-container', 
      type:'html', 
      css: '{{ asset('css/fee-challan.css') }}',
      documentTitle: 'Fee Challan',
      maxWidth: '1300'
    })">Print
            Challan Form</button>
        {{-- <form action="{{ route('getResultDocs', ['result' => $resultId, 'rollno' => $rollno, 'doc' => 'fee-challan'])}}" method="GET">
      <input type="hidden" value="true" name="download">
      <button>Download Challan Form</button>
    </form> --}}
    </div>
    <div class="container" id="fee-challan-container">
        @if ($challan_dues->total != 0)
            <div class="dues-challan">
                @foreach ($copies as $copy)
                    <div class="single-column">
                        <img src="{{ asset('images/challan-header.jpg') }}" class="header-image" alt="IIUI">
                        <div class="bank-logo-container">
                            @if ($gender == 2)
                                <img src="{{ asset('images/AlliedBank.png') }}" alt="Allied Bank Limited">
                                {{-- @elseif (($gender == 3 && $program->faculty->pkFacId  == 2) || ($gender == 3 && $program->faculty->pkFacId  == 10))
						<img src="{{asset('images/albaraka.png')}}" alt="Albaraka"> --}}
                            @else
                                <img src="{{ asset('images/Hbl.jpg') }}" alt="Habib Bank Limited">
                            @endif
                        </div>
                        <div class="account-number">
                            <span class="bold "> {{ $accountNumber->bank_name ?? '' }} Account#:
                                {{ $accountNumber->dues_account ?? '' }} </span>
                        </div>
                        <table class="detail-table">
                            <tbody>
                                <tr>
                                    <td class="bold fixed-width">Challan#:</td>
                                    <td>
                                        <b>
                                            @if ($challan_dues->second_challan)
                                                {{ $challan_dues->second_challan }}
                                            @else
                                                {{ $challan_dues->id ?? $challan_dues->second_challan }}
                                            @endif
                                        </b>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="bold">CNIC: </td>
                                    <td>{{ $applicant->applicant->cnic ?? '' }}</td>
                                </tr>
                                <tr>
                                    <td class="bold">Semester:</td>
                                    <td>
                                        @if ($challan_dues->fkSemesterId == 14 && $challan_dues->fkProgramId == 157)
                                            {{ 'Fall-2020' }}
                                        @else
                                            {{ $challan_dues->semester->title ?? '' }}
                                        @endif
                                        {{-- {{ $challan_dues->semester->title ?? ''}} --}}
                                    </td>
                                </tr>
                                <tr>
                                    <td class="bold">Issued Date:</td>
                                    <td>{{ $issuedDate ?? \Carbon\Carbon::now()->format('d-M-Y') }}</td>
                                </tr>
                                <tr>
                                    <td class="bold">Due Date:</td>
                                    <td class="underline bold">
                                        @if ($challan_dues->id == 486884 || $challan_dues->id == 486885)
                                            {{ '24-Aug-2022' }}
                                        @else
                                            {{ date_format(date_create($feeSubmissionLastDate), 'd-M-Y') }}
                                        @endif


                                    </td>
                                </tr>
                                <tr>
                                    <td class="bold">Roll No: </td>
                                    <td>{{ $rollno }}</td>
                                </tr>
                                <tr>
                                    <td class="bold">Name: </td>
                                    <td>{{ $applicant->applicant->name ?? '' }}</td>
                                </tr>
                                <tr>
                                    <td class="bold">Father's Name: </td>
                                    <td>{{ $applicant->applicant->detail->fatherName ?? '' }}</td>
                                </tr>
                                <tr>
                                    <td class="bold">Faculty:</td>
                                    <td>{{ $applicant->application->program->faculty->title }}</td>
                                </tr>
                                <tr>
                                    <td class="bold">Department:</td>
                                    @if ($applicant->application->fkProgramId == '189' ||
                                        $applicant->application->fkProgramId == '55' ||
                                        $applicant->application->fkProgramId == '56' ||
                                        $applicant->application->fkProgramId == '133')
                                        <td>{{ $result->result->program->department->title }} </td>
                                    @else
                                        <td>{{ $applicant->application->program->department->title }}</td>
                                    @endif
                                </tr>
                                <tr>
                                    <td class="bold">Program: </td>
                                    @if ($applicant->application->fkProgramId == '189' ||
                                        $applicant->application->fkProgramId == '55' ||
                                        $applicant->application->fkProgramId == '56' ||
                                        $applicant->application->fkProgramId == '133')
                                        <td>{{ $result->result->program->title }} </td>
                                    @else
                                        <td>{{ $applicant->application->program->title }} </td>
                                    @endif
                                    {{-- <td>{{ $applicant->application->program->title }} </td> --}}
                                </tr>
                            </tbody>
                        </table>

                        <table class="fee-distribution-table">
                            <thead>
                                <tr>
                                    <th>Particulars</th>
                                    <th class="border-left">Amount</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Admission Fee</td>
                                    <td class="border-left right-align">
                                        {{ $challan_dues->admission_fee ?? $fee->admission_fee }}</td>
                                </tr>
                                <tr>
                                    <td>University Dues</td>
                                    <td class="border-left right-align">
                                        {{ $challan_dues->university_dues ?? $fee->university_dues }}</td>
                                </tr>
                                {{-- For FBAS MALE --}}
                                {{-- @if (($program->faculty->pkFacId == 2 || $program->faculty->pkFacId == 10) && $gender != 2)
							<tr>
								<td>Library Security</td>
								<td class="border-left right-align">{{ $challan_security->library_security ?? $fee->library_security }}</td>
							</tr>
						@endif --}}
                                <tr>
                                    {{-- <td class="bold total-amount">Total Amount (PKR)</td>
							@if (($program->faculty->pkFacId == 2 || $program->faculty->pkFacId == 10) && $gender != 2)
								<td class="border-left right-align">
									<span class="bold" id="dues_total_zer">
											@if ($challan_dues)
											{{ $challan_dues->admission_fee + $challan_dues->university_dues + $challan_security->library_security }}
											@else
											{{ $fee->admission_fee + $fee->university_dues + $fee->library_security }}
											@endif
									</span>
								</td>
							@else --}}
                                    <td class="border-left right-align">
                                        <span class="bold" id="dues_total_zer">
                                            @if ($challan_dues)
                                                {{ $challan_dues->admission_fee + $challan_dues->university_dues }}
                                            @else
                                                {{ $fee->admission_fee + $fee->university_dues }}
                                            @endif
                                        </span>
                                    </td>
                                    {{-- @endif --}}
                                </tr>
                            </tbody>
                        </table>
                        <div class="bank-instructions">
                            <h4 class="bank-instructions-title">Instructions</h5>
                                <ol class="bank-instructions-list">
                                    <li>Please enter full fee challan# at the time of punching.</li>
                                    <li>All branches are requested to recieve the challan.</li>
                                    <li>Bank processing fee is not required.</li>
                                    <li>In case of any queries, Please contact fee section.</li>
                                </ol>
                        </div>
                        <div class="copy-owner">
                            {{-- <div class="bank-title">{{ $accountNumber->bank_name ?? '' }} </div> --}}
                            <span class="copy">{{ $copy }}</span>
                        </div>
                    </div> <!-- Ending single-column -->
                @endforeach
            </div> <!-- Ending dues challan -->
        @endif
        {{-- @if (!($program->faculty->pkFacId == 2 && $gender == 3) && !($program->faculty->pkFacId == 10 && $gender == 3)) --}}
            @if ($challan_security->total != 0)
                <div class="security-challan">
                    @foreach ($copies as $copy)
                        <div class="single-column">
                            <img src="{{ asset('images/challan-header.jpg') }}" class="header-image" alt="IIUI">
                            <div class="bank-logo-container">
                                @if ($gender == 2)
                                    <img src="{{ asset('images/AlliedBank.png') }}" alt="Allied Bank Limited">
                                    {{-- @elseif (($gender == 3 && $program->faculty->pkFacId  == 2) || ($gender == 3 && $program->faculty->pkFacId  == 10))
						<img src="{{asset('images/albaraka.png')}}" alt="Albaraka"> --}}
                                @else
                                    <img src="{{ asset('images/Hbl.jpg') }}" alt="Habib Bank Limited">
                                @endif
                            </div>
                            <div class="account-number">
                                <span class="bold "> {{ $accountNumber->bank_name ?? '' }} Account#:
                                    {{ $accountNumber->security_account ?? '' }} </span>
                            </div>
                            <table class="detail-table">
                                <tbody>
                                    <tr>
                                        <td class="bold fixed-width">Challan#:</td>
                                        <td>
                                            <b>
                                                @if ($challan_security->second_challan)
                                                    {{ $challan_security->second_challan }}
                                                @else
                                                    {{ $challan_security->id ?? $challan_security->second_challan }}
                                                @endif
                                            </b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="bold">CNIC: </td>
                                        <td>{{ $applicant->applicant->cnic ?? '' }}</td>
                                    </tr>
                                    <tr>
                                        <td class="bold">Semester:</td>
                                        <td>
                                            @if ($challan_security->fkSemesterId == 14 && $challan_security->fkProgramId == 157)
                                                {{ 'Fall-2020' }}
                                            @else
                                                {{ $challan_security->semester->title ?? '' }}
                                            @endif

                                            {{-- {{ $challan_security->semester->title ?? ''}} --}}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="bold">Issued Date:</td>
                                        <td>{{ $issuedDate ?? \Carbon\Carbon::now()->format('d-M-Y') }}</td>
                                    </tr>
                                    <tr>
                                        <td class="bold">Due Date:</td>
                                        <td class="underline bold">

                                            @if ($challan_dues->id == 486884 || $challan_dues->id == 486885)
                                                {{ '24-Aug-2022' }}
                                            @else
                                                {{ date_format(date_create($feeSubmissionLastDate), 'd-M-Y') }}
                                            @endif

                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="bold">Roll No: </td>
                                        <td>{{ $rollno }}</td>
                                    </tr>
                                    <tr>
                                        <td class="bold">Name: </td>
                                        <td>{{ $applicant->applicant->name ?? '' }}</td>
                                    </tr>
                                    <tr>
                                        <td class="bold">Father's Name: </td>
                                        <td>{{ $applicant->applicant->detail->fatherName ?? '' }}</td>
                                    </tr>
                                    <tr>
                                        <td class="bold">Faculty:</td>
                                        <td>{{ $applicant->application->program->faculty->title }}</td>
                                    </tr>
                                    <tr>
                                        <td class="bold">Department:</td>
                                        {{-- <td>{{ $applicant->application->program->department->title }}</td> --}}
                                        @if ($applicant->application->fkProgramId == '189' ||
                                            $applicant->application->fkProgramId == '55' ||
                                            $applicant->application->fkProgramId == '56' ||
                                            $applicant->application->fkProgramId == '133')
                                            <td>{{ $result->result->program->department->title }} </td>
                                        @else
                                            <td>{{ $applicant->application->program->department->title }}</td>
                                        @endif
                                    </tr>
                                    <tr>
                                        {{-- <td class="bold">Program: </td>
							<td>{{ $applicant->application->program->title }} </td> --}}
                                        <td class="bold">Program: </td>
                                        @if ($applicant->application->fkProgramId == '189' ||
                                            $applicant->application->fkProgramId == '55' ||
                                            $applicant->application->fkProgramId == '56' ||
                                            $applicant->application->fkProgramId == '133')
                                            <td>{{ $result->result->program->title }} </td>
                                        @else
                                            <td>{{ $applicant->application->program->title }} </td>
                                        @endif
                                    </tr>
                                </tbody>
                            </table>

                            <table class="fee-distribution-table">
                                <thead>
                                    <tr>
                                        <th>Particulars</th>
                                        <th class="border-left">Amount</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>Library Security</td>
                                        <td class="border-left right-align">
                                            {{ $challan_security->library_security ?? $fee->library_security }}</td>
                                    </tr>
                                    <tr>
                                        <td>Book Bank Security</td>
                                        <td class="border-left right-align">
                                            {{ $challan_security->book_bank_security ?? $fee->book_bank_security }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Caution Money</td>
                                        <td class="border-left right-align">
                                            {{ $challan_security->caution_money ?? $fee->caution_money }}</td>
                                    </tr>
                                    <tr>
                                        <td>Hostel Security</td>
                                        <td class="border-left"></td>
                                    </tr>
                                    <tr>
                                        <td class="bold total-amount">Total Amount (PKR)</td>
                                        <td class="border-left right-align bold" id="security_total_zer">
                                            @if ($challan_security)
                                                {{ $challan_security->library_security + $challan_security->book_bank_security + $challan_security->caution_money }}
                                            @else
                                                {{ $fee->library_security + $fee->book_bank_security + $fee->caution_money }}
                                            @endif
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <div class="bank-instructions">
                                <h4 class="bank-instructions-title">Instructions</h5>
                                    <ol class="bank-instructions-list">
                                        <li>Please enter full fee challan# at the time of punching.</li>
                                        <li>All branches are requested to receive the challan.</li>
                                        <li>Bank processing fee is not required.</li>
                                        <li>In case of any queries, Please contact fee section.</li>
                                    </ol>
                            </div>
                            <div class="copy-owner">
                                {{-- <div class="bank-title">{{$accountNumber->bank_name}}</div> --}}
                                <span class="copy">{{ $copy }}</span>
                            </div>
                        </div> <!-- Ending single-column -->
                    @endforeach
                </div> <!-- Ending dues challan -->
            @endif
        {{-- @endif --}}
    </div> <!-- Ending container -->
    {{-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script>
	$( document ).ready(function() {
    	var dues = $("#dues_total_zer").text().trim();
		if(dues == '0'){
			$('.dues-challan').hide();
		}
		var security = $("#security_total_zer").text().trim();
		if(security == '0'){
			$('.security-challan').hide();
		}
	});
	</script> --}}
</body>

</html>
