@extends('layouts.app')
@section('content')
	<div class="container">
		<section class="page-header row">
			<h2> Manage Results </h2>
		</section>
		<div class="page-content manage-results-page-section">
			<form action="{{route('manageResults')}}" method="POST">
				<select name="faculty" id="faculty" required>
					<option value="">-- Select Faculty -- </option>
					@foreach ( $faculties as $faculty )
						<option value="{{ $faculty->pkFacId }}">{{ $faculty->title }}</option>
					@endforeach
				</select>
				<select name="department" id="department" required>
					<option value="">-- Select Department -- </option>
				</select>
				<select name="programme" id="programme" required>
					<option value="">-- Select Program -- </option>
				</select>
				<select name="semester" id="result-semesterSelector" required>
					<option> --Select Semester-- </option>
					@foreach ( $semesters as $semester )
						<option value="{{ $semester->pkSemesterId }}"> {{ $semester->title }} </option>
					@endforeach
				</select>
				<button class="btn btn-primary" type="submit">Search</button>
			</form>
			@if ( $results )
			<div class="page-content-wrapper no-margin">
					<div class="single-result-manage">
						<table class="table table-hover table-bordered mr-tp-zero">
							<thead class="table-head">
								<tr>
									<th>S.No</th>
									<th>Title</th>
									<th>Published on</th>
									<th>Last modified</th>
									<th>Status</th>
									<th>Action</th>
									<th>Edit</th>
									<th>View</th>
									<th>Challan</th>
									@if(auth()->id() == 1 || auth()->id() == 20)
										<th>Comparison</th>
									@endif
								</tr>
							</thead>
							<tbody>
								<?php $serialNumber = 0; ?>
								@foreach ( $results as $result )
									<tr>
										<td>
											{{ ++$serialNumber }}
										</td>
										<td>
											<p>
									 		{{ ($result->type == 'Interview') ? 'Interview' : $result->listNumber.' list '}} 
									 		{{'of '. $result->gender. ' '. $result->program->title . ' - ' . $result->program->duration . ' year(s)'}}
									 		</p>
										 </td>
										 <td>
											{{ \Carbon\Carbon::parse($result->created_at)->format('d-m-Y') }}
										 </td>
										 <td>
											{{ \Carbon\Carbon::parse($result->updated_at)->format('d-m-Y') }}
										 </td>
									 	<td>
									 		<span class="active-status">{{ ($result->active == '1') ? 'Active' : 'Suspended' }}</span>
									 	</td>
									 	<td>
									 		<form action={{route('changeStatus')}} id="change-status-form" method="POST">
												<input type="hidden" value="{{ $result->pkResultsId }}" name="result_id">
												<input type="hidden" name="current_status" id="current_status_input" value={{$result->active}}>
												<button type="submit" class="sigma-btn btn btn-{{ ($result->active == '1' ) ? 'warning' : 'primary' }}">
													{{ ($result->active == '1' ) ? 'Suspend' : 'Activate' }}
												</button>
											</form>
									 	</td>
									 	<td>
									 		<form action={{ route('getUploadResults', ['id' => $result->pkResultsId])}} id="edit-form">
									 			<button type="submit" class="btn btn-primary sigma-btn"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></button>
									 		</form>
										 </td>
										 <td>
										 	<a href="{{ url('/results/'.$result->pkResultsId.'/list') }}" target="_black" type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></a>
										 </td>
										 <td>
											<form action={{route('changePublicState')}} id="change-state-public" method="POST">
												<input type="hidden" value="{{ $result->pkResultsId }}" name="result_id">
												<input type="hidden" name="current_status" id="current_status_input" value={{$result->public_access}}>
												<button type="submit" class="sigma-btn btn btn-{{ ($result->public_access == '0' ) ? 'warning' : 'primary' }}">
													{{ ($result->public_access == '0' ) ? 'Hide' : 'Show' }}
												</button>
											</form>
										 </td>
										@if(auth()->id() == 1 || auth()->id() == 20)
											<td>
												<a href="{{ url('admin/resultsListComparison/'.$result->fkProgrammeId.'/'.$result->gender.'/'.$semester->pkSemesterId) }}" target="_black" type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></a>
											</td>
										@endif
									</tr>
								@endforeach
							</tbody>
						</table>
					</div>
			</div> <!-- Ending page-content-wrapper -->
			@endif
		</div> <!-- Ending page-content row -->
	</div> <!-- Ending Container -->

	<script>
		$(function(){
			//refactor it
			$('#change-status-form button').on('click', function(){
				event.preventDefault();
				$this = $(this);
				changeStatusForm = $this.parent();
				$.ajax({
					method: 'POST',
					url: '{{route('changeStatus')}}',
					data: changeStatusForm.serialize(),
					success: function ( response ) {
						if ( response == '1' ) {
							//program is suspended, activate it
							$this.text('Suspend');	
							if ( $this.hasClass('btn-primary') ) {
								$this.removeClass('btn-primary');
								$this.addClass('btn-warning');
							}
							$this.parent().parent().prev().find('span.active-status').eq(0).text('Active');
							$this.prev().val(1);
						} else {
							//program is activated, suspend it
							$this.text('Activate');	
							if ( $this.hasClass('btn-warning') ) {
								$this.removeClass('btn-warning');
								$this.addClass('btn-primary');
							}
							$this.parent().parent().prev().find('span.active-status').eq(0).text('Suspended');
							$this.prev().val(0);
						}
					}
				});
			});
		});
		$(function(){
			$('#change-state-public button').on('click', function(){
				event.preventDefault();
				$this = $(this);
				changeStatusForm = $this.parent();
				$.ajax({
					method: 'POST',
					url: '{{route('changePublicState')}}',
					data: changeStatusForm.serialize(),
					success: function ( response ) {
						if ( response == '1' ) {
							//challan is visible, hide it
							$this.text('Show');	
							if ( $this.hasClass('btn-warning') ) {
								$this.removeClass('btn-warning');
								$this.addClass('btn-primary');
							}
							$this.parent().parent().prev().find('span.active-status').eq(0).text('Hide');
							$this.prev().val(1);
						} else {
							//challan is hidden now, show it
							$this.text('Hide');	
							if ( $this.hasClass('btn-primary') ) {
								$this.removeClass('btn-primary');
								$this.addClass('btn-warning');
							}
							$this.parent().parent().prev().find('span.active-status').eq(0).text('Show');
							$this.prev().val(0);

						}
					}
				});
			});

			$('#faculty').on('change', function(){
               $.ajax({
                   method: 'post',
                   url: '{{ route('getDepartmentsResults') }}',
                   data: {
                       id: $(this).val()
                   },
                   success: function ( response ) {
                   		console.log ( response );
                       $('#department').empty();
                       $('#department').append(
                            '<option value="">--Select Department--</option>'
                        );
                       $.each(response, function (key, value) {
                           $('#department').append(
                               '<option value="'+value.pkDeptId+'">'+ value.title+'</option>'
                           );
                       });
                   }
               });
            });

            $('#department').on('change', function(){
                $.ajax({
                    method: 'post',
                    url: '{{ route('getProgrammeResults') }}',
                    data: {
                        id: $(this).val()
                    },
                    success: function (response) {
                    	console.log ( response );
                        $('#programme').empty();
                        if ( response == '' ) {
                            $('#programme').append(
                                '<option value="">--Select Porgramme--</option>'
                            );
                            return;
                        }
                        $('#programme').append(
                            '<option value="">--Select Porgramme--</option>'
                        );
                        $.each(response, function (key, value) {
                            $('#programme').append(
                                '<option value="'+value.pkProgId+'">'+ value.title+' - '+ value.duration+ ' year(s)' + '</option>'
                            );
                        });
                    }
                });
            });
		});
	</script>
@endsection