		@section('title')
		Scholarship Details
		@endsection
		@include('results.schheader')

	<div class="container">
		
		<br>
		<br>
		<br>
        <div class="row">
		<div class="col-lg-2"></div>
		<div class="col-lg-8">
		  <div class="alert alert-danger" role="alert">
		      <h3 class="faculty-title d-flex justify-content-center pt-2 mb-2">Offered Scholarships </h3>
		  </div>
		</div>
		<div class="col-lg-2"></div>
	  </div>
@if ( $status )
    <div class="alert alert-danger text-center" role="alert"> {{ $status }}</div>
@endif
@if ( $status_ok )
    <div class="alert alert-success text-center" role="alert"> {{ $status_ok }}</div>
@endif
	  <div class="row">
		<div class="col-lg-2"></div>
		<div class="col-lg-8">
		  <button type="button" class="btn btn-sm btn-primary mb-2 ml-2 mr-2" data-toggle="modal" data-target="#signInModal"> Login into Scholarship Portal</button>
		  

		  <!-- <div class="alert alert-primary" role="alert">
		      <p class="faculty-title d-flex justify-content-center pt-2 mb-2">All Scholarships has been suspended till sunday due to technical upgradion of Portal.</p> 
		  </div> -->
		</div>
		<div class="col-lg-2"></div>
	  </div>
	
	<h5 class="row faculty-title d-flex justify-content-center pt-2 mb-2"></h5>

		<div class="row">
			<div class="col-lg-2"></div>
		<div class="col-lg-8" >
			<table id="example" class="table table-striped table-bordered" style="width:100%">
		  <thead class="thead-dark">
		    <tr>
		      
		      <th scope="col">Scholarship Name</th>
		      <th scope="col">Last Date</th>
		      <th scope="col">Apply</th> 
		    </tr>
		  </thead>
		  <tbody>

		  	@foreach ($SchView as $s)
		    <tr>
		      <td>{{ $s->name}}</td>
		      <td>{{ date('d-m-Y', strtotime($s->end_date))}}</td>
		      <td>
		      	<button type="button" class="btn btn-sm btn-primary mb-2 ml-2 mr-2" data-toggle="modal" data-target="#signInModal">Apply</button>
		      </td>
		    </tr>
            @endforeach
		  </tbody>
		</table>
		</div>
		<div class="col-lg-2"></div>
		</div>

		<br>
		<br>
	</div> 
	<!--Ending container --> 
{{-- @if (session('status'))
		<div class="alert alert-success" role="alert">
				{{ session('status') }}
		</div>
		@endif --}}
		<!-- The Modal -->
		<div class="modal fade" id="signInModal">
			<div class="modal-dialog modal-dialog-centered">
				<div class="modal-content">
					<!-- Modal Header -->
					<div class="modal-header" style="background-color:#015927">
						<h6 class="modal-title" style="color:white">Apply for Scholarship</h6>
						<button type="button" class="close" data-dismiss="modal">&times;</button>
					</div>
					<!-- Modal body -->
					<div class="modal-body">
						<form method="POST" action="{{route('schapplicantLogin')}}">
							{{ csrf_field() }}
							<div class="alert alert-primary" role="alert">
						      <p>Please use your admission login detail to login here</p> 
							</div>
							<div class="form-group row">
								<label for="staticEmail" class="col-sm-2 col-form-label">Email</label>
								<div class="col-sm-10">
									<input type="text" class="form-control" name="email" placeholder="Email/CNIC/B-Form/Passport No" required>
								</div>
							</div>
							<div class="form-group row">
								<label for="inputPassword" class="col-sm-2 col-form-label">Password</label>
								<div class="col-sm-10">
									<input type="password" class="form-control" name="password" placeholder="Password" required>
								</div>
							</div>
							<div class="form-group row">
								<label for="inputPassword" class="col-sm-2 col-form-label"></label>
								<div class="col-sm-10">
									<button type="submit" class="btn btn-primary btn-sm mb-2">Login</button>
									<a class="forgotPass pull-right" data-toggle="collapse" data-target="#collapseforgot" aria-expanded="false" aria-controls="collapseforgot">Forgot Password</a>
								</div>
							</div>
						</form>
						
						<div class="collapse" id="collapseforgot">

							<form method="POST" action="{{route('resetPassword')}}">
								{{csrf_field()}}
								<div class="form-group row">
									<label for="staticEmail" class="col-sm-2 col-form-label">Email</label>
									<div class="col-sm-10">
										<input type="text" class="form-control-plaintext pl-2" name="email" placeholder="email@example.com" required>
									</div>
								</div>
								<div class="form-group row">
									<label for="inputPassword" class="col-sm-2 col-form-label"></label>
									<div class="col-sm-10">
										<button type="submit" class="btn btn-primary btn-sm mb-2">Forgot Password</button>
									</div>
								</div>
							</form>
						</div>
					</div>
					<!-- Modal footer -->
					{{-- <div class="modal-footer">
						<button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Close</button>
					</div> --}}
					
				</div>
			</div>
		</div>

	<script type="text/javascript">	
		$(document).ready(function() {
    $('#example').DataTable();
} );
	</script>

@include('results.schfooter')