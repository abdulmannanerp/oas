@section('title')
Recent Results
@endsection
@include('results.header')

	@if ( $newAnnouncements)
	<div class="container-fluid main-container announcements-panel small-fluid bg-ff">
		<div class="row">
			<div class="col-md-12">
				{{-- <h6>
					Merit Lists for Different Undergraduate Programs are uploaded against limited seats. Admissions will be offered on a first come first serve basis. <b>Last date of fee submission has been extended till Tuesday, January 31, 2023, for selected programs. </b>
				</h6> --}}
			</div>
		</div>
		<div class="row">
			<div class="col-lg-2">
				@include('merit.front.tab')
			</div>
			<div class="col-lg-9">
			<h4 class="announcements-heading text-center">Recently Published Results {{($semester != '')? $semester: ''}}</h4>
			<div class="row">
				<table id="recent-results" class="display" style="width:100%">
					<thead>
						<tr>
							<th>Program</th>
							<th>List</th>
							<th>Published Date</th>
						</tr>
					</thead>
					<tbody>
				@foreach ( $newAnnouncements as $announcement )
				<tr>
					<td>
						<i class="fa fa-angle-double-right" aria-hidden="true"></i>						
						<span class="result-sze">
							{{ $announcement->program->title .' - '.   $announcement->program->duration . ' Year(s)' }} - {{ $announcement->gender }}
						</span>
					</td>
					<td>
						<a class="card-title" target="_blank" href={{route('displayLists', ['id' => $announcement->pkResultsId])}}>
							<i class="fa fa-external-link-square" aria-hidden="true"></i>	
							@if ($announcement->type == 'Interview')
								{{'Interview List'}}
							@elseif($announcement->type == 'Waiting')
								@if($announcement->list_text != NULL)
									{{$announcement->list_text}}
								@else
									{{'Waiting list'}}
								@endif
							@else
								@if($announcement->list_text != NULL)
									{{$announcement->list_text}}
								@else
									{{$announcement->listNumber. ' Merit list '}}
								@endif
							@endif					
							{{-- {{ ($announcement->type == 'Interview') ? 'Interview List' : $announcement->listNumber. ' Merit list '}} --}}
						</a>
					</td>
					<td>
						<i class="fa fa-calendar" aria-hidden="true"></i> {{ \Carbon\Carbon::parse($announcement->created_at)->format('d-m-Y')}}
					</td>
				</tr>
				{{-- <div class="col-sm-12 col-md-12 results-list">
					<div class="col-lg-6 flt-lft">
						<i class="fa fa-angle-double-right" aria-hidden="true"></i>						
						<span class="result-sze">
							{{ $announcement->program->title .' - '.   $announcement->program->duration . ' Year(s)' }} - {{ $announcement->gender }}
						</span>
					</div>
					<div class="col-lg-3 flt-lft">
							<a class="card-title" target="_blank" href={{route('displayLists', ['id' => $announcement->pkResultsId])}}>
							<i class="fa fa-external-link-square" aria-hidden="true"></i>						
							{{ ($announcement->type == 'Interview') ? 'Interview List' : $announcement->listNumber. ' Merit list '}}
							</a>
					</div>
					<div class="col-lg-3 flt-lft result-sze text-right">
							<i class="fa fa-calendar" aria-hidden="true"></i> Published: {{ \Carbon\Carbon::parse($announcement->created_at)->format('d-m-Y')}}
					</div>
				</div> --}}
				@endforeach
			</tbody>
			<tfoot>
				<tr>
					<th>Program</th>
					<th>List</th>
					<th>Published Date</th>
				</tr>
			</tfoot>
		</table>
			</div> <!-- Ending row --> 
			{{-- <div class="text-center"> {!! $newAnnouncements->render() !!}</div> --}}
			
		</div>
		</div> <!-- Ending container -->
	</div> <!-- Ending contianer-fluid main-container -->
	@endif
	
	@include('results.footer')