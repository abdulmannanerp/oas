<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Offer Letter</title>
  <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
  <style>
  .container {
    width: 100%;
    margin: 0 auto;
    font-family: 'Open Sans', sans-serif;
  }
  header {
    border-bottom: 1px solid #ccc;
  }
  .img-container, .name-address-container {
    display:inline-block;
  }
  .img-container {
    width: 10%;
  }
  .name-address-container {
    text-align: center;
    width: 82%;
    vertical-align: top;
  }
  .img-container img {
    height: 70px;
    width: 70px;
  }

  .name-address-container h1, 
  .name-address-container h2, 
  .name-address-container h3, 
  .name-address-container h4 {
    margin: 0;
  }


  .student-details-container p {
    margin:2px 0;
  }

  p.subject strong {
    text-decoration: underline;
  }

  #offer-letter-container {
    clear:both;
    font-size: 10.5px;
  }

  .instructions-list ul {
    margin-top: 0;
    padding-top: 0;
  }

  .instructions-list li {
    margin-bottom: 0;
  }

  .float-right {
    float:right;
  }

  .bold {
    font-weight: bold;
  }

  .center {text-align: center;}
  </style>
</head>
<body> 
  <div class="container" id="offer-letter-container">
    <header>
      <div class="img-container">
        <img src="{{asset('/uploads/images/logo.jpg')}}" class="logo" alt="IIUI Logo">
      </div><div class="name-address-container">
        <h2>International Islamic University</h2>
        <h3>Islamabad Pakistan</h3>
        <h4>P.O.Box 1243,  www.iiu.edu.pk, Email:admissions@iiu.edu.pk</h4>
        <h4>Tel. 051-9257988, 051-9019871, Fax: 051-9257915</h4>
      </div>
    </header>
    {{-- <p class="date-now float-right">{{ \Carbon\Carbon::now()->format('F d, Y') }}</p> --}}
    <div class="student-details-container">
      <p><strong>Roll No:</strong> {{ $rollno }}</p>
      <p><strong>Student Name:</strong> {{ $applicant->applicant->name }}</p>
      <p><strong>Father Name:</strong> {{ $applicant->applicant->detail->fatherName}}</p>
      {{-- <p><strong>Program:</strong> {{ $applicant->application->program->title }}</p>   --}}
      @if ( 
          $applicant->application->fkProgramId == '189' ||
          $applicant->application->fkProgramId == '55' ||
          $applicant->application->fkProgramId == '56' ||
          $applicant->application->fkProgramId == '133' 
        )
        <p><strong>Program:</strong> {{ $result->result->program->title }}</p>  
      @else
        <p><strong>Program:</strong> {{ $applicant->application->program->title }}</p>  
      @endif
    </div>
    
    <p class="subject">Subject: <strong>PROVISIONAL OFFER FOR ADMISSION TO ACADEMIC SEMESTER 
      {{-- {{ strtoupper($semester->title) }} --}}
      @if($applicant->application->semester->title == 'SPRING-2021' && $applicant->application->program->pkProgId == 157)
        {{ 'Fall-2020' }}
      @else
        {{ strtoupper($semester->title) }}
      @endif
    </strong> 
    </p>
    <p>Assalam-o-Alaikum!</p>
    <p>Dear Student,</p>
    <ol class="instructions-list">
      <li>
          I am pleased to inform that you have been provisionally selected for admission to the above mentioned degree program at International Islamic University, Islamabad from <span class="bold">
          @if($applicant->application->semester->title == 'SPRING-2021' && $applicant->application->program->pkProgId == 157)
            {{ 'Fall-2020' }}
          @else
            {{ strtoupper($semester->title) }}
          @endif
            {{-- {{ $semester->title }} --}}
          </span> semester commencing from <span class="bold">
            @if($applicant->application->semester->title == 'SPRING-2021' && $applicant->application->program->pkProgId == 157)
              {{ '11-Jan-2021' }}
            @else
              {{date_format(date_create($semesterStartDate), 'd-M-Y') }}
            @endif
            {{-- {{ date_format(date_create($semesterStartDate), 'd-M-Y') }} --}}
          </span>. Your admission shall be confirmed upon completion of all formalities.  Please note that if you do not meet the admission criteria as mentioned in the admission advertisement or if the information given by you in the admission form is found incomplete or incorrect, this admission offer shall stand withdrawn and admission shall be cancelled without any further notice.
      </li>
      <li>
        You are required to bring along the following documents at the time of joining at Admission Office, Administration Block, New Campus, Sector H-10, Islamabad:
        <ul>
          <li>
             Original academic certificates along with detailed marks certificates. 
          </li>
          <li>
             Attested photocopies of all academic certificates/DMCs.  (One set.).
          </li>
          <li>
            05 Passport size coloured photographs. 
          </li>
          <li>
            Original Fee Slip: (Before depositing fee please make sure that you duly qualify for admission as per required criteria, otherwise in case of cancellation of admission for any reason, tuition fee would be refunded as per notified fee refund policy only: please consult admission rules & regulations for fee refund policy).
          </li>
          <li>
            Undertaking as per specimen.
          </li>
          <li>
            No Objection Certificate (NOC) from employer (for employee only).
          </li>
          <li>
            Printed copy of application form, available in online admission system.
          </li>
        </ul>
      </li>
      <li>
        Your admission shall be governed by Rules and Regulations of the University enforced from time to time. You are advised to familiarize with IIUI regulations governing your admission, studies and examinations accordingly.
      </li>
      <li>
        If provisionally admitted on the basis of awaiting result, you are required to submit result with required percentage of marks/CGPA at the time of joining.  In case your result is not declared, you will be allowed to submit the same as soon as possible but not later than  <span class="bold">
          @if($applicant->application->semester->title == 'SPRING-2021' && $applicant->application->program->pkProgId == 157)
            {{ '10-Feb-2021' }}
          @else
            {{ date_format(date_create($documentSubmissionLastDate), 'd-M-Y') }}
          @endif
          {{-- {{ date_format(date_create($documentSubmissionLastDate), 'd-M-Y') }} --}}
        </span> failing which your admission shall be treated as cancelled automatically. Furthermore previous degree must be submitted to the admission office after confirmation from parent institution and attestation by HEC within this stipulated time period. 
      </li>
      <li>
        In case you fail in any subject or fail to secure required marks or CGPA required for admission to the above mentioned program, you must not conceal the fact and immediately report to admission office for refund of tuition fee if paid, as per notified refund policy of IIUI.  
      </li>
      <li>
        You are advised to deposit fee/dues till <span class="bold">{{ date_format(date_create($feeSubmissionLastDate), 'd-M-Y') }}</span> and submit your joining report to the admission office. The admission will not be confirmed unless fee/dues are paid in time and joining slip is issued by the Admission Office. 
      </li>
      <li>
        After completion of all joining formalities, you will report to the coordination office of concerned Department/Faculty.
      </li>
      <li>
        Overseas students are required to report to Overseas Admission Section to complete all joining formalities i.e. NOC, Study Visa, Equivalence of academic record and information sheets etc.  
      </li>
    </ol>
    <p>We welcome you to IIU & wish you best of luck in future academic endeavors.</p>
    <p class="float-right">Assistant Director/Deputy Director ( Academics )</p>
  {{--   <p>
      Encls: Fee Challan
    </p> --}}
    <div style="clear: both;"></div> <!-- Clear the float -->
    <div class="center clearfix"> 
      (Auto Generated by Online Admission System) 
    </div>  
  </div>
</body>
</html>