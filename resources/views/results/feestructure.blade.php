@section('title')
Fee Structure | New Students
@endsection
@include('results.header')
{{-- <script type="text/javascript">
    window.location.href = "https://admission.iiu.edu.pk/storage/images/Fee_Structure_Fall_2020-21.pdf";//here double curly bracket
</script> --}}
	<div class="container-fluid fee-main-container fee-prog-conatiner">
		{{-- <h1 class="row row d-flex justify-content-center mt-2 mb-2 top-head" style="color: red;"> Under Construction </h1> --}}
		<div class="row row d-flex justify-content-center mt-2 top-head">
			<h5>FEE STRUCTURE &nbsp;&nbsp;&nbsp;<a href="https://www.iiu.edu.pk/wp-content/uploads/2020/12/Information-Fee-22122020.pdf" target="_blank">Fee Schedule w.e.f. Spring-2021</a></h5>
		</div>
		{{-- <div class="row row d-flex justify-content-center mb-2">
			<h5>For New Students Only (w.e.f {{$currentSemester[0]->title}})</h5> <img src={{ url('uploads/images/expand_all.png') }} class="expand_all" alt="Expand / Collapse">
			<h5><a target="_blank" href="https://www.iiu.edu.pk/wp-content/uploads/2022/07/Fee-Weiver-FMS-06072022.pdf" >Fee Waiver for BBA, BSAF and MBA (Faculty of Management Sciences) w.e.f. Fall-2022</a></h5>
		</div> --}}
		<div class="sticky-top">
			<div class="row fee-view tp-fe-v">
				<div class="col-6">
				</div>
				<div class="col-3 algn-cntre">
					Refunadable Securities
				</div>
				<div class="col-3">
				</div>
			</div>
			<div class="row fee-view tp-fe-v">
				<div class="col-1">
					Sr #
				</div>
				<div class="col-3">
					Degree Program
				</div>
				<div class="col-1 algn-cntre">
					Admission Fee
				</div>
				<div class="col-1 algn-cntre">
					University Dues
				</div>
				<div class="col-1 algn-cntre">
					Library Security
				</div>
				<div class="col-1 algn-cntre">
					Book Bank Security
				</div>
				<div class="col-1 algn-cntre">
					Caution Money
				</div>
				<div class="col-1 algn-cntre">
					Total Fee
				</div>
				<div class="col-1 algn-cntre">
					Hostel Dues Per Month
				</div>
				<div class="col-1 algn-cntre">
					Hostel Security
				</div>
			</div>
		</div>
		@php $fac=''; $dept= ''; $i = 0;  @endphp
		@foreach ($FeeView as $f)
		<?php
		// echo $f->program->department->title; exit;
		?>
			@if($fac != $f->program->faculty->title)
				@if($i != 0)
					</div>
				@endif
			<div class="row faculty-title d-flex justify-content-center fee-clr bor-head pt-2" data-toggle="collapse" data-target="#demo-<?= $i ?>"> <i class="more-less fa fa-minus"></i><h6><?= ($f->program->faculty->pkFacId != 8) ? 'Faculty of ': ''; ?>{{$f->program->faculty->title}}</h6></div>
			@if($f->program->faculty->pkFacId == 6)
				<p class="row row d-flex justify-content-center"><a target="_blank" href="https://www.iiu.edu.pk/wp-content/uploads/2022/07/Fee-Weiver-FMS-06072022.pdf" >Fee Waiver for BBA, BSAF and MBA (Faculty of Management Sciences) w.e.f. Fall-2022</a></p>
			@endif
			<div id="demo-<?= $i ?>" class="cls-faculty collapse show">
			@endif
			@if($dept != $f->program->department->title)
			<div class="row d-flex justify-content-center bor-head pt-2"><h6>{{$f->program->department->title}}</h6></div>
			@endif
			<div class="row fee-view">
				<div class="col-1 algn-cntre">
					{{ $loop->iteration }}
				</div>
				<div class="col-3">
					{{$f->program->title. $f->program->description.' '.$f->program->duration. ' year(s)'}}
				</div>
				<div class="col-1 algn-cntre">
					{{$f->admission_fee ?: ''}}
				</div>
				<div class="col-1 algn-cntre">
					{{$f->university_dues ?: ''}}
				</div>
				<div class="col-1 algn-cntre">
					{{$f->library_security ?: ''}}
				</div>
				<div class="col-1 algn-cntre">
					{{$f->book_bank_security ?: ''}}
				</div>
				<div class="col-1 algn-cntre">
					{{$f->caution_money ?: ''}}
				</div>
				<div class="col-1 algn-cntre">
					{{$f->admission_fee+$f->university_dues+$f->library_security+$f->book_bank_security+$f->caution_money}}
				</div>
				<div class="col-1 algn-cntre">
					{{$f->hostel_dues ?: ''}}
				</div>
				<div class="col-1 algn-cntre">
					{{$f->hostel_security ?: ''}}
				</div>
			</div>
			

		@php
		$fac = $f->program->faculty->title;
		$dept = $f->program->department->title;
		$i++;
		@endphp
		@endforeach
	</div> <!--Ending container --> 

@include('results.footer')