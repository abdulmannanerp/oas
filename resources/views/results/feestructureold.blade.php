@section('title')
Fee Structure | Existing Students
@endsection
@include('results.header')

	<div class="container-fluid fee-main-container">
		{{-- <h1 class="row row d-flex justify-content-center mt-2 mb-2 top-head" style="color: red;"> Under Construction </h1> --}}
		<div class="row row d-flex justify-content-center mt-2 top-head">
			<h5>FEE STRUCTURE</h5>
		</div>
		<div class="row row d-flex justify-content-center mb-2">
			<h5>For Existing Students</h5> <img src={{ url('uploads/images/expand_all.png') }} class="expand_all" alt="Expand / Collapse">
		</div>
		<div class="sticky-top">
			<div class="row fee-view tp-fe-v">
				<div class="col-6">
				</div>
				<div class="col-3 algn-cntre">
					Refunadable Securities
				</div>
				<div class="col-3">
				</div>
			</div>
			<div class="row fee-view tp-fe-v">
				<div class="col-1">
					Sr #
				</div>
				<div class="col-3">
					Degree Program
				</div>
				<div class="col-1 algn-cntre">
					Admission Fee
				</div>
				<div class="col-1 algn-cntre">
					University Dues
				</div>
				<div class="col-1 algn-cntre">
					Library Security
				</div>
				<div class="col-1 algn-cntre">
					Book Bank Security
				</div>
				<div class="col-1 algn-cntre">
					Caution Money
				</div>
				<div class="col-1 algn-cntre">
					Total Fee
				</div>
				<div class="col-1 algn-cntre">
					Hostel Dues
				</div>
				<div class="col-1 algn-cntre">
					Hostel Security
				</div>
			</div>
		</div>
		@php $fac=''; $dept= ''; $i = 0;  @endphp
		@foreach ($FeeView as $f)
		<?php
		// echo $f->program->department->title; exit;
		?>
			@if($fac != $f->program->faculty->title)
				@if($i != 0)
					</div>
				@endif
			<div class="row faculty-title d-flex justify-content-center fee-clr bor-head pt-2" data-toggle="collapse" data-target="#demo-<?= $i ?>"> <i class="more-less fa fa-minus"></i><h6><?= ($f->program->faculty->pkFacId != 8) ? 'Faculty of ': ''; ?>{{$f->program->faculty->title}}</h6></div>
			<div id="demo-<?= $i ?>" class="cls-faculty collapse show">
			@endif
			@if($dept != $f->program->department->title)
			<div class="row d-flex justify-content-center bor-head pt-2"><h6>{{$f->program->department->title}}</h6></div>
			@endif
			<div class="row fee-view">
				<div class="col-1 algn-cntre">
					{{ $loop->iteration }}
				</div>
				<div class="col-3">
					{{$f->program->title. $f->program->description.' '.$f->program->duration. ' year(s)'}}
				</div>
				<div class="col-1 algn-cntre">
					{{$f->admission_fee ?: ''}}
				</div>
				<div class="col-1 algn-cntre">
					{{$f->university_dues ?: ''}}
				</div>
				<div class="col-1 algn-cntre">
					{{$f->library_security ?: ''}}
				</div>
				<div class="col-1 algn-cntre">
					{{$f->book_bank_security ?: ''}}
				</div>
				<div class="col-1 algn-cntre">
					{{$f->caution_money ?: ''}}
				</div>
				<div class="col-1 algn-cntre">
					{{$f->admission_fee+$f->university_dues+$f->library_security+$f->book_bank_security+$f->caution_money}}
				</div>
				<div class="col-1 algn-cntre">
					{{$f->hostel_dues ?: ''}}
				</div>
				<div class="col-1 algn-cntre">
					{{$f->hostel_security ?: ''}}
				</div>
			</div>
			

		@php
		$fac = $f->program->faculty->title;
		$dept = $f->program->department->title;
		$i++;
		@endphp
		@endforeach
	</div> <!--Ending container --> 

@include('results.footer')