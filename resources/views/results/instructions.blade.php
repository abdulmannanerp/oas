@section('title')
Instructions
@endsection
@include('results.header')
	<div class="container-fluid p50tb results-main-container announcements-panel small-fluid">
			<div class="row">
				<div class="col-lg-2">
					@include('merit.front.tab')
				</div>
				<div class="col-lg-10">
				<h5 class="text-center pt-3">(Office of the In-Charge (Admission))</h5>
				<h6 class="text-center">Procedures For Completion Of Admission For Selected Candidates</h6>
				<p class="text-justify">Successful candidates are advised to adopt the following steps to complete the admission process:</p>
				<p class="text-justify"><b>Step No.1:</b>	Print Admission Offer Letter and Challan Form from <a class="text-primary" href="{{route('getResultsMainPage')}}">website </a></p>
				<p class="text-justify"><b>Step No.2:</b>	Deposit the fee in the following Banks: </p>
				<ul>
					<li><b>MALE:</b>	Habib Bank Limited (HBL) and Al-Bakara Bank Limited (as	mentioned on challan form)</li>
					<li><b>FEMALE:</b>	Allied Bank Limited (ABL)</li>
				</ul>
				<p class="text-justify"><b>Step No.3:</b>	Print joining form from <a class="text-primary" href="{{route('getResultsMainPage')}}">website.</a>  Fill out the joining form and attached following documents:</p>
				<ul>
					<li>Attested photocopies of all academic certificate (one set)</li>
					<li>Five Photographs</li>
					<li>Original Fee Slip (Academics Copy)</li>
					<li>Copy of Admission Offer Letter</li>
					<li>No Objection Certificate (NOC) for employee only</li>
					<li>Must bring original documents along with you at the time of joining</li>
					<li>Undertaking as per specimen</li>
				</ul>
				<p class="text-justify"><b>Step No.4:</b>	After completing the aforementioned requirements, submit the Joining Form to the respective office(s) within the due date mentioned in the Admission Offer Letter</p>
				<p class="text-justify"><b>Step No.5:</b>	Obtain Enrolment Slip from Admission Office and submit a copy to the concerned department</p>
				<p class="text-justify">For further information & query please contact following phone numbers: </p>
				<ul>
					<li><b>MALE:</b> +923195213192, 051-9019749, 9019750, 9019619, 9019583</li>
					<li><b>FEMALE:</b> +923195213193, 051-9019324, 9019327, 9019877, 9019854</li>
				</ul>
				{{-- <p class="text-justify"><b>Note:</b>Candidate(s) must observe the following SOPs: -</p>
				<table class="table table-striped table-bordered" style="width:90%">
					<thead>
						<tr>
							<th>S#</th>
							<th>SOPs</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>1</td>
							<td>Wearing of Surgical Mask</td>
						</tr>
						<tr>
							<td>2</td>
							<td>Washing of Hands frequently with soap</td>
						</tr>
						<tr>
							<td>3</td>
							<td>3 to 6 feet Distance among individuals</td>
						</tr>
						<tr>
							<td>4</td>
							<td>Avoid sharing or exchange of personal belongings including pen, copy, books or mobile etc.</td>
						</tr>
						<tr>
							<td>5</td>
							<td>Avoid Clustering at Campus</td>
						</tr>
					</tbody>
				</table> --}}
				<p class="text-right" style="margin-top: 5em; margin-right:3em">Noman Mashal</p>
				<p class="text-right" style="margin-right:3em">In-Charge (Admissions)</p>
				</div>
			</div>
	</div> <!--Ending container --> 

@include('results.footer')
