<!DOCTYPE html>
<!DOCTYPE html>
<html lang="en">

<head>
    <title>@yield('title')</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity=""
        crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <link href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css" rel="stylesheet">
    <link rel="stylesheet" href={{ url('css/results.css') }}>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-17879535-4"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', 'UA-17879535-4');
    </script>


</head>

<body
    class="{{ Request::route()->getName() == 'programes' || Request::route()->getName() == 'fee-structure' ? 'remove-responsive-fp' : '' }}">
    <div class="container-fluid header">
        <img src={{ url('uploads/images/iiui-logo.jpg') }} id="iiui-top-banner" alt="iiui logo">
        <div class="topheader-btn">
            {{-- <button type="button" class="btn btn-sm btn-primary mb-2" data-toggle="modal" data-target="#signUpModal">Login</button> --}}
            <a href="/" class="btn btn-sm btn-primary mb-2 ml-2 mr-2">Signup</a>
        </div>
    </div> <!-- Ending contianer-fluid header -->
    <nav class="navbar sticky-top navbar-expand-lg top-menu">
        <button class="navbar-toggler navbar-toggler-right ml-auto  float-xs-right" type="button"
            data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
            aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="nav-link {{ Request::route()->getName() == 'signup' ? 'active' : '' }}"
                        href="/">Home <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Faculties
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        {{-- <a class="dropdown-item al-fac-menu" href="#">All Faculties</a>
						<div class="dropdown-divider"></div> --}}
                        <a class="dropdown-item" href="http://www.iiu.edu.pk/?page_id=44" target="_blank">Arabic</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="http://www.iiu.edu.pk/?page_id=50" target="_blank">Sciences</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="http://www.iiu.edu.pk/?page_id=56" target="_blank">Engineering &
                            Technology</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="http://www.iiu.edu.pk/?page_id=68" target="_blank">Islamic
                            Studies (Usuluddin)</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="http://www.iiu.edu.pk/?page_id=74" target="_blank">Languages
                            Literature </a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="http://www.iiu.edu.pk/?page_id=80" target="_blank">Management
                            Sciences</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="http://www.iiu.edu.pk/?page_id=92" target="_blank">Social
                            Sciences </a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="http://www.iiu.edu.pk/?page_id=62" target="_blank">International
                            Institute of Islamic Economics </a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="http://www.iiu.edu.pk/?page_id=86" target="_blank">Shariah and
                            Law</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="#" target="_blank">Computing</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="#" target="_blank">Education</a>
                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{ Request::route()->getName() == 'programes' ? 'active' : '' }}"
                        href="{{ url('programes') }}">Programs Offered/Eligibility Criteria</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{ Request::route()->getName() == 'fee-structure' ? 'active' : '' }}"
                        href="{{ route('fee-structure') }}">Fee Structure</a>
                    {{-- <a class="nav-link" target="_blank" href="{{ asset('storage/images/Fee_Structure_Fall_2020-21.pdf') }}">Fee Structure</a> --}}
                </li>
                {{-- <li class="nav-item dropdown">
					<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown2" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Fee Structure</a>
					<div class="dropdown-menu" aria-labelledby="navbarDropdown2">
						<a class="dropdown-item {{ (Request::route()->getName() == 'fee-structure') ? 'active' : '' }}" href="{{ route('fee-structure') }}">New Students</a>
						<div class="dropdown-divider"></div>
						<a class="dropdown-item {{ (Request::route()->getName() == 'fee-structure-old') ? 'active' : '' }}" href="{{ route('fee-structure-old') }}">Existing</a>
					</div>
				</li> --}}
                {{-- <li class="nav-item">
					<a class="nav-link {{ (Request::route()->getName() == 'programes') ? 'active' : '' }}" href="{{ url('programes') }}">Eligibility Crateria</a>
				</li> --}}

                <!-- Scholarship Page Change by Haider -->
                <li class="nav-item">
                    <a class="nav-link" href="https://www.iiu.edu.pk/?page_id=1121" target="_blank">ScholarShips</a>

                    <!-- <a class="nav-link {{ Request::route()->getName() == 'scholarshipdetail' ? 'active' : '' }}" href="{{ route('scholarshipdetail') }}">Scholarships</a> -->
                </li>
                <!-- Change End -->


                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown2" role="button"
                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Downloads</a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown2">
                        <a class="dropdown-item" href="http://admission.iiu.edu.pk/docs/IIUI_Admission_Guide_2021.pdf"
                            target="_blank">Admission Guide</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="http://www.iiu.edu.pk/?page_id=1026#sample"
                            target="_blank">Sample Papers</a>
                    </div>
                </li>
                {{-- <li class="nav-item">
					<a class="nav-link hov" data-toggle="modal" data-target="#rollNumberModal">RollNo Slip </a>
				</li> 
				--}}
                {{-- <li class="nav-item">
					<a class="nav-link res-front {{ (Request::route()->getName() == 'getResultsMainPage') ? 'active' : '' }}" href="{{url('results')}}">RESULTS</a>
				</li> --}}
                {{-- <li class="nav-item">
					<a class="nav-link {{ (Request::route()->getName() == 'getResultsMainPage') ? 'active' : '' }}" href="{{url('recent-results/11')}}">Results</a>				
				</li> --}}
                <li class="nav-item">
                    <a class="nav-link {{ Request::route()->getName() == 'contactus' ? 'active' : '' }}"
                        href="{{ url('frontend/contactus') }}">Contact Us</a>
                </li>
            </ul>
        </div>
    </nav>
    <?php
    
    $lastDate = \DB::select("SELECT lastDate from `tbl_oas_programme_schedule` LIMIT 1");
	$dte = strtotime($lastDate[0]->lastDate);
	$dte = date('d-M-Y',$dte);
    ?>
        
        <marquee direction="left" style="padding-top: 8px;">

         {{--  <span style="color: red;">This is to inform all the candidates that after considering the requests of multiple students for Program Change,  all the merit lists have been updated according to the available limited seats;  applicants are therefore advised to visit the updated <a target="_blank" href="https://admission.iiu.edu.pk/results">Merit Lists</a> for their admissions at IIUI. The last  date for fee submission is September 4, 2023. </span> --}}

         {{--  <span style="color: red;"><a target="_blank" href="https://www.iiu.edu.pk/?p=62912">Revised schedule for BS (CS, SE & IT)</a> </span>  --}}

    </marquee>
    {{-- <marquee direction="left" style="padding-top: 8px;"> Last date to apply: {{$dte}} Fee Challan with due date 02.01.2023 can be deposited on 03.01.2023 due to bank holiday</marquee> --}}
    {{-- @if (session('status'))
		<div class="alert alert-success" role="alert">
				{{ session('status') }}
		</div>
		@endif --}}
    <!-- The Modal -->
    <div class="modal fade" id="signUpModal">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header">
                    <h6 class="modal-title">Login</h6>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <!-- Modal body -->
                <div class="modal-body">
                    <form method="POST" action="{{ route('applicantLogin') }}">
                        {{ csrf_field() }}
                        <div class="form-group row">
                            <label for="staticEmail" class="col-sm-2 col-form-label">Email</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control-plaintext pl-2" name="email"
                                    placeholder="Email/CNIC/B-Form/Passport No" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputPassword" class="col-sm-2 col-form-label">Password</label>
                            <div class="col-sm-10">
                                <input type="password" class="form-control" name="password" placeholder="Password"
                                    required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputPassword" class="col-sm-2 col-form-label"></label>
                            <div class="col-sm-10">
                                <button type="submit" class="btn btn-primary btn-sm mb-2">Login</button>
                                <a class="forgotPass pull-right" data-toggle="collapse" data-target="#collapseforgot"
                                    aria-expanded="false" aria-controls="collapseforgot">Forgot Password</a>
                            </div>
                        </div>
                    </form>

                    <div class="collapse" id="collapseforgot">

                        <form method="POST" action="{{ route('resetPassword') }}">
                            {{ csrf_field() }}
                            <div class="form-group row">
                                <label for="staticEmail" class="col-sm-2 col-form-label">Email</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control-plaintext pl-2" name="email"
                                        placeholder="email@example.com" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputPassword" class="col-sm-2 col-form-label"></label>
                                <div class="col-sm-10">
                                    <button type="submit" class="btn btn-primary btn-sm mb-2">Forgot
                                        Password</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- Modal footer -->
                {{-- <div class="modal-footer">
						<button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Close</button>
					</div> --}}

            </div>
        </div>
    </div>

    <div class="modal fade" id="cnicModal">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header">
                    <h6 class="modal-title">Enter Your CNIC</h6>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <!-- Modal body -->
                <div class="modal-body">
                    <form method="GET" class="cnicClass" action="">
                        {{ csrf_field() }}
                        <div class="form-group row">
                            <label for="staticEmail" class="col-sm-2 col-form-label">CNIC</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control-plaintext pl-2 cnicUnique" name="cnic"
                                    placeholder="CNIC/B-Form/Passport No" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputPassword" class="col-sm-2 col-form-label"></label>
                            <div class="col-sm-10">
                                <button type="submit" class="btn btn-primary btn-sm mb-2">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
                <!-- Modal footer -->
                {{-- <div class="modal-footer">
						<button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Close</button>
					</div> --}}

            </div>
        </div>
    </div>

    {{-- <div class="modal fade" id="rollNumberModal">
			<div class="modal-dialog modal-dialog-centered">
				<div class="modal-content">
					<!-- Modal Header -->
					<div class="modal-header">
						<h6 class="modal-title">Admission Roll No Slip</h6>
						<button type="button" class="close" data-dismiss="modal">&times;</button>
					</div>
					<!-- Modal body -->
					<div class="modal-body">
						<form method="POST" action="{{route('publicRollNumberSlip')}}">
							{{ csrf_field() }}
							<div class="form-group row">
								<label for="staticEmail" class="col-sm-6 col-form-label text-right">CNIC/Passport-No/B-Form:</label>
								<div class="col-sm-6">
									<input type="text" class="form-control-plaintext pl-2" name="cnic" placeholder="CNIC/Passport-No/B-Form" required>
								</div>
							</div>
							<div class="form-group row">
								<label for="inputPassword" class="col-sm-6 col-form-label text-right">Form No:</label>
								<div class="col-sm-6">
									<input type="number" class="form-control" name="form_no" placeholder="Form No" required>
								</div>
							</div>
							<div class="form-group row">
								<div class="col-sm-12">
									<button type="submit" class="btn btn-primary btn-sm mb-2">Submit</button>
								</div>
							</div>
						</form>
					</div>
					<!-- Modal footer -->
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Close</button>
					</div>
					
				</div>
			</div>
		</div> --}}

    <!-- Modal -->
    <div class="modal fade" id="admissionReq" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title ff-l" id="exampleModalLabel_1">Important Instructzzzsions Before Applying
                        (Pakistani Candidates)</h3>
                    <button type="button" class="close ff-r" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <ul>
                        <li>Please check admission eligibility criteria carefully <a href="http://admission.iiu.edu.pk/programes" target="_blank">here</a>. Apply only if qualify as per
                            requirement.</li>
                        <li>Admission Processing Fee Rs.1500/- is non-refundable / non-adjustable.</li>
                        <li>
                            University Fee once paid is refunded only as per below mentioned HEC policy for all
                            candidates including those who applied / admitted on result awaiting basis:
                            <ul>
                                <li>
                                    Full (100%) fee refund (Excluding admission fee) up to 7 th day (September 1 to 7,
                                    2022 including Saturday &amp; Sunday) after commencement of classes as per academic
                                    calendar notified by the University.
                                </li>
                                <li>
                                    Half (50%) fee refund (Excluding admission fee) from 8 th – 15 th day (September 8
                                    to 15, 2022 including Saturday &amp; Sunday) after commencement of classes as
                                    notified by the University.
                                </li>
                                <li>
                                    No fee refund thereafter except refundable security (from 16th day onwards).
                                </li>
                                <li>
                                    The students on provisional admission (result awaiting status) failing to obtain the required percentage as per advertised eligibility criteria will be refunded 100% of fee within 10 days of the declaration of result from respective board.
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <div class="modal-header">
                    <h3 class="modal-title ff-l" id="exampleModalLabel_2">Important Instructions Before Applying
                        (Foreigner Candidates)</h3>
                </div>
                <div class="modal-body">
                    <ul>
                        <li>Please check admission eligibility criteria carefully. Apply only if qualify as per
                            requirement.</li>
                        <li>Please Deposit Non-refundable Admission Processing Fee USD $75/- or equal amount in Pak
                            Rupees by Swift/Electronic Fund Transfer through following bank details. </li>
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>Title of account</th>
                                    <th>IIUI Foreign Currency / Account</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Account No.</td>
                                    <td>5006-79008735-10</td>
                                </tr>
                                <tr>
                                    <td>Currency</td>
                                    <td>US $</td>
                                </tr>
                                <tr>
                                    <td>Bank</td>
                                    <td>Habib Bank Limited</td>
                                </tr>
                                <tr>
                                    <td>Branch Code</td>
                                    <td>5006</td>
                                </tr>
                                <tr>
                                    <td>Branch Address</td>
                                    <td>HBL, IBB-IIU, Islamabad Branch, Sector H-10, Islamabad, Pakistan.</td>
                                </tr>
                                <tr>
                                    <td>Swift Code</td>
                                    <td>HABBPKKA</td>
                                </tr>
                                <tr>
                                    <td>IBAN</td>
                                    <td>PK39-HABB-0050-0679-0087-3510</td>
                                </tr>
                            </tbody>
                        </table>
                        </li>
                        <li>Admission shall be granted to all foreign/Overseas Pakistani candidates in all degree
                            programs on the basis of paper qualification except F/o Engineering & Technology after
                            fulfillment the admission requirements.</li>
                        <li>All Refugees residing in Pakistan are required to appear in Entry Test/Interview for
                            admission in all degree programs as local candidates as per schedule</li>
                        <li>Please follow following instructions at the time of form submission
                            <ul>
                                <li>Download Admission processing fee challan</li>
                                <li>Submit required amount in the bank</li>
                            </ul>
                        </li>
                        <li>Please attach following required documents and send through email on
                            overseas.admissions@iiu.edu.pk for (Male candidates) and
                            overseas.female.admissions@iiu.edu.pk for (Female candidates)
                            <ul>
                                <li>Paid Fee Challan</li>
                                <li> Picture </li>
                                <li> Passport </li>
                                <li> All Certificates/degrees (10th Grade, 12th Grade, Bachelor, Master) </li>
                                <li> No. Objection Certificate from M/o Foreign Affairs Or your Embassy in Pakistan
                                </li>
                                <li> Residential Permit (Aqamah) </li>
                                <li> NICOP (for Overseas Pakistani only). </li>
                                <li> Foreign Students Information Sheet (Required for NOC of HEC and study Visa) </li>
                            </ul>
                        </li>
                        <li>
                            University Fee once paid is refunded only as per below mentioned HEC policy for all
                            candidates including those who applied / admitted on result awaiting basis:
                            <ul>
                                <li>
                                    Full (100%) fee refund (Excluding admission fee) up to 7 th day (September 1 to 7,
                                    2022 including Saturday &amp; Sunday) after commencement of classes as per academic
                                    calendar notified by the University.
                                </li>
                                <li>
                                    Half (50%) fee refund (Excluding admission fee) from 8 th – 15 th day (September 8
                                    to 15, 2022 including Saturday &amp; Sunday) after commencement of classes as
                                    notified by the University.
                                </li>
                                <li>
                                    No fee refund thereafter except refundable security (from 16th day onwards).
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
