@section('title')
	Result List
@endsection
	@include('results.header')
	@php 
		use App\Http\Controllers\ResultController;
	@endphp
	<div class="container-fluid result-list">
		@if ( $resultList->count() )
		<div class="page-content row cnicDanger">
			<div class="page-content-wrapper no-margin">
				@if($errors->any())
					<div class="alert alert-danger">
						<ul>
							@foreach ( $errors->all() as $error ) 
								<li>{{ $error }}</li>
							@endforeach
						</ul>
					</div>
				@endif
			</div>
		</div>
		<div class="row">
			{{-- <div class="col-lg-2">
				@include('merit.front.tab')
			</div> --}}
			<div class="col-lg-12">
				<div class="info-container">
						<div class="main-info">
							<h5 class="program-title">
								{{ $resultDetail->result->program->title. ' - ' . $resultDetail->result->program->duration . ' - Year(s) - ('.$resultDetail->result->gender.') ' }}
							</h5>
							{{-- <h6>
								Login to admission account to download Joining Performa and Admission Form from your <a href="{{url('frontend/application')}}">profile</a>
							</h6> --}}
							<h5 class="program-title">
								@if($resultDetail->result->type == 'Interview')
									Interview List
								@endif
								@if($resultDetail->result->type == 'Waiting')
									Waiting List
								@endif
								@if($resultDetail->result->type == 'Result' && $resultDetail->result->list_text == NULL )
									{{$resultDetail->result->listNumber.' List'}}
								@endif
								@if($resultDetail->result->list_text != NULL)
									{{$resultDetail->result->list_text}}
								@endif
							</h5>
							@if($resultDetail->result->type != 'Waiting')
								<p class="cl-ov"> <span class="float-left">Published: {{ \Carbon\Carbon::parse($resultDetail->result->created_at)->format('d-m-Y') }}</span><span class="float-right">Last Date: <span class="list-date">{{ \Carbon\Carbon::parse($resultDetail->result->endDate)->format('d-m-Y') }}</span></span> </p>
							@endif
						</div>
						<p class="list-instructions"> {{ $resultDetail->result->instructions }} <a href="https://admission.iiu.edu.pk/frontend/application/" target="_blank">Consent Form</a></p>
					</div>
					<table class="table table-striped result-lst-ad">
						<thead class="thead list-thead">
							<tr>
								<td>S.No</td>
								<td>Roll Number</td>
								<td>Name</td>
								<td>Father Name</td>
								@if ($resultDetail->result->type == 'Result' )
									<td>Offer Letter</td>
									{{-- <td>Challan Form</td> --}}
									@if ( $secondChallan->count() )
										<td>Arrears Challan</td>
									@endif										
									<td>Joining Performa</td>
									{{-- <td>Offer Letter/Joining Performa</td> --}}
									<td>Challan Form</td>
								@endif
							</tr>
						</thead>
						<tbody>
						<?php $i = 0; $inCorrectRollNumbers = []; ?>	
						@foreach ( $resultList as $list )
							<tr>
								<td> {{ ++$i }} </td>
								<td> {{ $list->rollno }} </td>
								<td> {{ $list->name }} </td>
								<td> {{ $list->fatherName }} </td>
								@if ( $resultDetail->result->type == 'Result' ) 
									{{-- Push Incorrect RollNumbers to Log --}}
									@php
									if ( 	
											$list->result->fkProgrammeId != $list->getRollno->application->fkProgramId && 
											$list->getRollno->application->fkProgramId != 189 
										) {
											$inCorrectRollNumbers[] = (integer)$list->rollno;
									}
									@endphp
									@if ( 
											((integer)$list->rollno  &&
											$list->result->fkProgrammeId == $list->getRollno->application->fkProgramId) ||
											$list->getRollno->application->fkProgramId == 189 ||
											$list->getRollno->application->fkProgramId == 55 ||
											$list->getRollno->application->fkProgramId == 56 ||
											$list->getRollno->application->fkProgramId == 133
										)
										@php 
											$url = explode('/',url()->current());
											$arr = array(124,112,47,46,113,48,302,303,312,457,458,584,712,767,766,772,773,774);
										@endphp
										@if(Auth::guard('web')->check())
											@php $arr = array(); @endphp
											{{-- Hello webb {{Auth::guard('web')->user()->name}} --}}
										{{-- @elseif(Auth::guard('applicant')->check())
											Hello userrr {{Auth::guard('applicant')->user()->name}} --}}
										@endif
										{{-- @if(!in_array($url[4], $arr)) --}}
{{-- {{dd(sizeof($arr), $list->result->public_access)}} --}}
										@if($list->result->public_access == 0 || sizeof($arr) == 0)
										
									<td>

										<button type="button" onClick="cnicData(<?=$list->fkResultId?>,<?= $list->rollno ?>, 'offer-letter')" class="btn btn-sm btn-primary mb-2" data-toggle="modal" data-target="#cnicModal">Download</button>

									</td>
									<td>

										<button type="button" onClick="cnicData(<?=$list->fkResultId?>,<?= $list->rollno ?>, 'performa')" class="btn btn-sm btn-primary mb-2" data-toggle="modal" data-target="#cnicModal">Download</button>

									</td>
									<?php /* ?>
									<td>
										<button type="button" onClick="cnicData(<?=$list->fkResultId?>,<?= $list->rollno ?>, 'docs')" class="btn btn-sm btn-primary mb-2" data-toggle="modal" data-target="#cnicModal">Download</button>

										{{-- <a href="{{route('getResultDocs', ['result' => $list->fkResultId, 'rollno' => $list->rollno, 'doc' => 'all'])}}" class="download-docs" target="_blank">
											Button
										</a> --}}
									</td>
									<?php */ ?>
									<td>
										<button type="button" onClick="cnicData(<?=$list->fkResultId?>,<?= $list->rollno ?>, 'fee-challan')" class="btn btn-sm btn-primary mb-2" data-toggle="modal" data-target="#cnicModal">Download</button>
									</td>
									{{-- Dev Mannan: Second Challan Starts --}}

									@if ( $secondChallan->count() )
									<td>
									@php
										$exist = ResultController::secondChallan($list->rollno);
									@endphp
										@if(!($exist->isEmpty()))
											
													<a href="{{route('getResultDocs', ['result' => $list->fkResultId, 'rollno' => $list->rollno, 'doc' => 'fee-challan-second'])}}" class="download-docs" target="_blank">
														<img src="{{asset('images/download-icon.svg')}}" class="download-icon" alt="Downlaod">
														<i class="fa fa-cloud-download-alt"></i>
														Arrears Challan
													</a>
											
										@endif
									</td>
									@endif
									{{-- Dev Mannan: Second Challan Ends --}}
									@else
									<td colspan = "3">
										Please Contact Admission Office
									</td>
									@endif
									@endif
								@endif
							</tr>
						@endforeach
						@php
							// Log::info('------------------- Start '.$resultDetail->result->program->title.'-------------------');
							// Log::info($inCorrectRollNumbers);
							// Log::info('------------------- End '.$resultDetail->result->program->title.'-------------------');
						@endphp
						</tbody>
					</table>
			</div>
		</div>
		@else
			<div class="row" style="padding:20px 0">
				<div class="col-lg-2">
					<h3>No record found</h3>
				</div>
			</div>
		@endif
	</div>
	@include('results.footer')