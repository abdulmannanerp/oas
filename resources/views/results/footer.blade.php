<div class="footer row">
    <div class="container-fluid">
        <div class="row w3_footer_grids">
            <div class="col-md-3 col-sm-12 w3_footer_grid">
                <h3>Admission (Male)</h3>
                
                <ul class="address">
                    <li><i class="fa fa-phone-square" aria-hidden="true"></i>&nbsp;+92-51-9019583, +92-51-9019750, +92-51-9019567</li>
                    <li><i class="fa fa-envelope" aria-hidden="true"></i><a href="mailto:admissions@iiu.edu.pk"> admissions@iiu.edu.pk</a></li>
                    {{-- <li><i class="fa fa-print" aria-hidden="true"></i> &nbsp;+92-51-9257915</li> --}}
                </ul>
            </div>
            <div class="col-md-3 col-sm-12 w3_footer_grid">
                <h3>Admission (Female)</h3>
                
                <ul class="address">
                    <li><i class="fa fa-phone-square" aria-hidden="true"></i>&nbsp;+92-51-9257985, +92-51-9019327, +92-51-9019324, +92-51-9019871, +92-51-9019854</li>
                    <li><i class="fa fa-envelope" aria-hidden="true"></i><a href="mailto:admissions.fc@iiu.edu.pk"> admissions.fc@iiu.edu.pk</a></li>
                    <li><i class="fa fa-print" aria-hidden="true"></i> &nbsp;+92-51-9257929</li>
                </ul>
            </div>
            <div class="col-md-3 col-sm-12 w3_footer_grid">
                <h3>Overseas (Male &amp; Female)</h3>
                
                <ul class="address">
                    
                    <li><i class="fa fa-phone-square" aria-hidden="true"></i>&nbsp;+92-51-9019565</li>
                    <li><i class="fa fa-envelope" aria-hidden="true"></i><a href="mailto:overseas.admissions@iiu.edu.pk"> overseas.admissions@iiu.edu.pk</a></li>
                    {{-- <li><i class="fa fa-print" aria-hidden="true"></i> &nbsp;+92-51-9257915</li> --}}
                </ul>
            </div>
            <div class="col-md-3 col-sm-12 w3_footer_grid">
                <h3>Queries</h3>
                <ul class="info"> 
                    <li><i class="fa fa-question-circle" style="color:#3F835A;" aria-hidden="true"></i><a href="{{ url('frontend/faq') }}">&nbsp; FAQs</a></li>
                    <li><i class="fa fa-question-circle" style="color:#3F835A;" aria-hidden="true"></i><a href="https://www.iiu.edu.pk/?page_id=15123">&nbsp; Report an Issue</a></li>
                    <li><i class="fa fa-envelope" style="color:#3F835A;" aria-hidden="true"></i><a href="{{ url('frontend/contactus') }}">&nbsp; Contact Us</a></li>
                    <li><i class="fa fa-envelope" style="color:#3F835A;" aria-hidden="true"></i><a href="mailto:fee.section@iiu.edu.pk">&nbsp; For Fee relared queries email at: fee.section@iiu.edu.pk</a></li>
                </ul>
                
                
            </div>
            <div class="clearfix"> </div>
        </div>
    </div>
    
    <div class="footer-copy">
        
        <div class="container">
            <p>© <?= date('Y') ?> IIUI Admission System. All rights reserved | Powerd by <!--<a href="http://www.iiu.edu.pk/?page_id=16403" target="_blank"> -->IT Center IIUI<!--</a>--></p>
        </div>
    </div>
    {{-- <span style="float:right; color: #999">{{number_format( (microtime(true) - LARAVEL_START), 2 )}}</span> --}}
</div>
<div class="footer-botm row">
    <div class="container-fluid">
        <div class="w3layouts-foot">
            <ul class="ft-icons">
                <li><a href="https://www.facebook.com/iiu.isbpk/" target="_blank" class="w3_agile_facebook"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                <li><a href="https://twitter.com/iiui_official?lang=en" target="_blank" class="agile_twitter"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                <li><a href="https://pk.linkedin.com/company/iiui" target="_blank" class="w3_agile_dribble"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                <!--<li><a href="https://www.instagram.com/explore/locations/262176346/international-islamic-university-islamabad/" target="_blank" class="w3_agile_dribble"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>-->
            </ul>
        </div>
        <div class="payment-w3ls">	
                <!--<img src="images/card.png" alt=" " class="img-responsive">-->

        </div>
        <div class="clearfix"> </div>
    </div>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    </script>
	<script src={{url('js/results.js')}}></script>
	<script>
		$(function(){
            $('.select2').select2();
			$('#faculty').on('change', function(){
               $.ajax({
                   method: 'post',
                   url: '{{ route('getDepartmentsResults') }}',
                   data: {
                       id: $(this).val()
                   },
                   success: function ( response ) {
                   		console.log ( response );
                       $('#department').empty();
                       $('#department').append(
                            '<option value="">--Select Department--</option>'
                        );
                       $.each(response, function (key, value) {
                           $('#department').append(
                               '<option value="'+value.pkDeptId+'">'+ value.title+'</option>'
                           );
                       });
                   }
               });
            });

            $('#department').on('change', function(){
                $.ajax({
                    method: 'post',
                    url: '{{ route('getProgrammeResults') }}',
                    data: {
                        id: $(this).val()
                    },
                    success: function (response) {
                    	console.log ( response );
                        $('#programme').empty();
                        if ( response == '' ) {
                            $('#programme').append(
                                '<option value="">--Select Porgramme--</option>'
                            );
                            return;
                        }
                        $('#programme').append(
                            '<option value="">--Select Porgramme--</option>'
                        );
                        $.each(response, function (key, value) {
                            $('#programme').append(
                                '<option value="'+value.pkProgId+'">'+ value.title+'</option>'
                            );
                        });
                    }
                });
            });
		});
		$('#lgn-btn').click(function() {
   			window.location = "http://admission.iiu.edu.pk/login";
		});
        // When the user scrolls down 20px from the top of the document, show the button
        window.onscroll = function() {scrollFunction()};

        function scrollFunction() {
            if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
                document.getElementById("topBtn").style.display = "block";
            } else {
                document.getElementById("topBtn").style.display = "none";
            }
        }

        // When the user clicks on the button, scroll to the top of the document
        function topFunction() {
            document.body.scrollTop = 0; // For Safari
            document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
        }

        $(function(){
            $('.faculty-title').on('click', function(){
                $this = $(this);
                // console.log ( $this.find('i'));
                if ( $this.find('i').eq(0).hasClass('fa-minus') ) {
                    $this.find('i').eq(0).removeClass('fa-minus').addClass('fa-plus');
                }else{
                    $this.find('i').eq(0).removeClass('fa-plus').addClass('fa-minus');
                }
            })
        });


        $(document).ready(function() {
            $('#recent-results').DataTable({
                order: [],
                columnDefs: [
                    { width: "600px", targets: 0 },
                    { width: "150px", targets: 1 },
                    { width: "150px", targets: 2}
                ]
            });

            $('#results-compare').DataTable({
                "pageLength": 100
            });
            
        } );
                </script>
                
<button onclick="topFunction()" id="topBtn" title="Go to top"><i class="fa fa-angle-up up-ic-fnt"></i></span></button>
</body>
</html>