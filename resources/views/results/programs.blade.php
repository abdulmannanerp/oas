@section('title')
Programs Offered
@endsection
@include('results.header')

<div class="container-fluid fee-main-container fee-prog-conatiner">


	<h5 class="row faculty-title d-flex justify-content-center pt-2 mb-2"> Programs Offered {{$semester->title}} <img src={{ url('uploads/images/expand_all.png') }} class="expand_all" alt="Expand / Collapse"></h5>
	{{-- <p class="msg-div mar-cn"> --}}
	{{-- <p>
		The candidates applying for undergraduate degree programs can choose upto five programs against the payment of a single application processing fee and there is no admission test for Undergraduate programs except for the programs offered by the Faculty of Engineering & Technology and Faculty of Computing. 
	</p> --}}
	<p>
		For Graduate Degree Programs Candidates: <br />
		<b>For PHD:</b> <br />
		GRE/HAT General or equivalent test, conducted by testing bodies accredited by HEC, with at least 60% score.
		<ul>
			<li>Admission in any PHD Program shall not be allowed on hope certificate.</li>
			<li>Admission shall not be allowed if the CGPA is below 3.00 in MS or equivalent Degree Program.</li>
			<li>Marks distribution in GRE type test shall be as under:-</li>
			<li>General = 40% marks</li>
			<li>Subject = 60% marks</li>
		</ul>
		Pattern of the General part of the test shall be in line with the GRE general test conducted by ETS.
	</p>
	<p>
		<b>For MS:</b><br />
		Passing of GRE General type or equivalent entry test conducted by the university (department) with a passing score of 50%. <br /> OR <br />
		GRE/HAT General/equivalent test, conducted by testing bodies accredited by HEC, with at least 50% score.
		<ul>
			<li>Admission in any MS or equivalent Program shall not be allowed on a hope certificate.</li>
			<li>Marks distribution in GRE type test shall be as under:-</li>
			<li>General = 40% marks</li>
			<li>Subject = 60% marks</li>
		</ul>
		Pattern of the General part of the test shall be in line with the GRE general test conducted by ETS.
	</p>
	<p>
		<b>The University reserves the right to drop any program without assigning any reason.</b>
	</p>
	
		<div class="panel-group" id="accordion">
		@php
		$fac = '';
		$dep = '';
		$gender = '';
		$i = 0;
				// echo '<pre>'; 
				// print_r($programlisting);
				// echo '</pre>';
		@endphp
		@foreach($programlisting as $plist) 
		@if($fac != $plist->faculty->title) 
		@if($i != 0) 
	</div>
	@endif
	@if($i == 107)
	@php echo $i; @endphp
	@endif
	<div class="row faculty-title d-flex justify-content-center fee-clr bor-head pt-2" data-toggle="collapse" data-target="#demo-<?= $i ?>"><h5><?= ($plist->faculty->pkFacId != 8) ? 'Faculty of': ''; ?>  <?= $plist->faculty->title; ?></h5><i class="more-less fa fa-minus"></i></div>
	<div id="demo-<?= $i ?>" class="cls-faculty collapse show mb-2">
		@if($plist->faculty->pkFacId == 6)
		{{-- <div class="row d-flex justify-content-center">
			<b>Note:</b> &nbsp; All Programs (MBA & BBA) of Faculty of Management Sciences are NBEAC Accredited.
		</div> --}}
		@endif
		@endif

		@if($dep != $plist->department->title) 
		<div class="row d-flex justify-content-center bor-head pt-2"><h6><?= $plist->department->title ?></h6></div>

		<div class="pr-main-div"><div class="pr-prog pr-style"><i>Program</i></div><div class="pr-gender pr-style">Offered To</div><div class="pr-req pr-style">Eligibility Criteria</div> <!-- <div class="pr-date pr-style">Last Date</div> <div class="pr-time pr-style">Test/Interview Date</div> --> </div>
		@endif
		@if(is_object($plist->programscheduler))
		@if($plist->programscheduler->fkGenderId == 1) 
		@php $gender = 'Male & Female';  @endphp
		@elseif ($plist->programscheduler->fkGenderId == 2) 
		@php $gender = 'Female';  @endphp
		@elseif ($plist->programscheduler->fkGenderId == 3) 
		@php $gender = 'Male';  @endphp
		@endif
		@php
		$lastDate = new DateTime($plist->programscheduler->lastDate);

		$lastDate = $lastDate->format('d-M-Y');
		$testDateTime = new DateTime($plist->programscheduler->testDateTime);
		$testDateTime = $testDateTime->format('d-M-Y h:i A');

		@endphp
		@if ($testDateTime == '30-Nov--0001 12:00 AM') 
		@php $testDateTime = ''; @endphp
		@else 
		@php $testDateTime = '<b>Test:</b> ' . $testDateTime; @endphp
		@endif
		@php
		$interviewDateTime = new DateTime($plist->programscheduler->interviewDateTime);
		if($plist->pkProgId == 162){
			$interviewDateTime = $interviewDateTime->format('d-M-Y h:i A'); // h:i A (added)
		}else{
			$interviewDateTime = $interviewDateTime->format('d-M-Y h:i A');
		}
		@endphp

		@if ($interviewDateTime == '30-Nov--0001 12:00 AM' || $interviewDateTime == '30-Nov-0000 12:00 AM')
			@php $interviewDateTime = ''; @endphp
		@else
			@php $interviewDateTime = '<b>Interview:</b> ' . $interviewDateTime; @endphp
		@endif
		@php $testInstructionsPopup = ''; @endphp
		{{-- @if($plist->pkProgId == '56' || $plist->pkProgId == '55' || $plist->pkProgId == '133' || $plist->pkProgId == '189')
			@php $testInstructionsPopup = '<button type="button" class="btn btn-sm btn-primary mb-2" data-toggle="modal" data-target="#testScheduleModal">Test/Interview Date <img src="https://www.iiu.edu.pk/wp-content/uploads/images/default/new.gif" alt="NEW"></button>'; @endphp
		@endif --}}
		@if ($plist->duration == 0)
			@php $duration = ''; @endphp
		@else
		@if($plist->pkProgId == '196')
			@php $ss = '' @endphp
		@else
			@php $ss = 's' @endphp
		@endif
		@php $duration = ' (' . $plist->duration . ' Year'.$ss.')'; @endphp
		@endif
		@if ($plist->programscheduler->status == 1)
		@if ($plist->pkProgId == '189')
			<div class="pr-main-div"><div class="pr-prog"><i>BS Electrical Engineering (4 Years)</i></div><div class="pr-gender"><?= $gender ?></div><div class="pr-req">{!! $plist->requirements!!} <!-- </div><div class="pr-date">07-Aug-2023</div>  <div class="pr-time"> <?= $interviewDateTime . $testInstructionsPopup  ?> <b>Test: </b>15-Aug-2023 10:00 AM <br />  </div>  --> </div> 
		@else
		<div class="pr-main-div"><div class="pr-prog"><i><?= '<a name="'.$plist->abbrev.'"></a>'.$plist->title . $plist->description . $duration ?></i></div><div class="pr-gender"><?= $gender ?></div><div class="pr-req">{!!$plist->requirements!!} </div> <!-- <div class="pr-date"><?= $lastDate; ?></div>  <div class="pr-time"><?= $testDateTime  ?> <br /> <?= $interviewDateTime . $testInstructionsPopup ?> </div> --> </div>  {{-- /*    Interview: */ --}}

		@if($plist->pkProgId == 126) 
			<div class="row d-flex justify-content-center">
				<b>Note:</b> &nbsp; *All Pre-Medical candidates must pass deficiency courses of Mathematics of 6 credit hours within one year of their regular studies.
			</div>
		@endif
		
		@endif
		@if ($plist->pkProgId == '55')
			<div class="pr-main-div"><div class="pr-prog"><i>BS Mechanical Engineering (4 Years)</i></div><div class="pr-gender">Male</div><div class="pr-req">HSSC (Pre-Engineering) (Mathematics, Physics & Chemistry) or with combination of Physics, Mathematics and Computer Studies/Computer Science (ICS) with Chemistry as a remedial subject/course in the 1st semester after admission with minimum 60% marks and SSC (Science) or equivalent with minimum 60% marks.</div> <!-- <div class="pr-date">07-Aug-2023</div>  <div class="pr-time"> <?php /* ?> <button type="button" class="btn btn-sm btn-primary mb-2" data-toggle="modal" data-target="#testScheduleModal">Test/Interview Date <img src="https://www.iiu.edu.pk/wp-content/uploads/images/default/new.gif" alt="NEW"> </button> <?php */ ?>  <b>Test: </b>15-Aug-2023 10:00 AM <br /> </div> -->  </div>
		{{-- <div class="row d-flex justify-content-center bor-head pt-2"><h4>Department of Civil Engineering</h4></div><div class=""><div class="pr-prog pr-style"><i>Program</i></div><div class="pr-gender pr-style">Gender</div><div class="pr-req pr-style">Eligibility Criteria</div><div class="pr-date pr-style">Last Date</div><div class="pr-time pr-style">Test/Interview Date</div></div> <div class="pr-main-div"><div class="pr-prog"><i>BS Civil Engineering</i></div><div class="pr-gender">Male</div><div class="pr-req">HSSC (Pre-Engineering) (Mathematics, Physics & Chemistry) or equivalent with minimum 60% marks and SSC (Science) or equivalent with minimum 60% marks.</div><div class="pr-date">30-Jul-2021</div><div class="pr-time"> <button type="button" class="btn btn-sm btn-primary mb-2" data-toggle="modal" data-target="#testScheduleModal">Test/Interview Date</button> <!-- <b>Test: </b>04-Aug-2018 10:00 AM <br /> -->  </div></div>  <!--  Interview: --> --}}
		@endif
		@if ($plist->pkProgId == '133')
		
			<div class="pr-main-div"><div class="pr-prog"><i>BS Civil Engineering</i></div><div class="pr-gender">Male</div><div class="pr-req">HSSC (Pre-Engineering) (Mathematics, Physics & Chemistry) or with combination of Physics, Mathematics and Computer Studies/Computer Science (ICS) with Chemistry as a remedial subject/course in the 1st semester after admission with minimum 60% marks and SSC (Science) or equivalent with minimum 60% marks.</div> <!-- <div class="pr-date">07-Aug-2023</div>  <div class="pr-time"> <?php /* ?> <button type="button" class="btn btn-sm btn-primary mb-2" data-toggle="modal" data-target="#testScheduleModal">Test/Interview Date <img src="https://www.iiu.edu.pk/wp-content/uploads/images/default/new.gif" alt="NEW"></button>  <?php */ ?> <b>Test: </b>15-Aug-2023 10:00 AM <br /> </div> -->  </div>
		
		@endif
		@endif
		@endif
		@php
		$fac = $plist->faculty->title;
		$dep = $plist->department->title;
		$i++;
		@endphp

		@endforeach

		
		
	</div>


	<div class="modal fade info-test" id="testScheduleModal">
		<div class="modal-dialog modal-dialog-centered">
			<div class="modal-content">
				<!-- Modal Header -->
				<div class="modal-header">
					<h6 class="modal-title">
						<b><p class="txt-align-cntr-imp mr-auto">TEST SCHEDULE (BS & BSC PROGRAMS)</p></b>
						<p class="mr-bt-ze"><b>Time Schedule for On-Campus Computer-based Admission Test of BS Engineering and BSc Technology Program </b></p>
					</h6>
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>
				<!-- Modal body -->
				<div class="modal-body">
					<table class="table table-bordered test-table">
						<thead class="thead-dark">
						  <tr>
							<th colspan="3">Male</th>
						  </tr>
						</thead>
						<tbody>
						  <tr>
							<td><b>Roll Numbers</b></td>
							<td><b>Date</b></td>
							<td><b>Time</b></td>
						  </tr>
						  <tr>
							<td>73970 – 78366</td>
							<td>Saturday 28 August, 2021</td>
							<td>10:00 AM – 12:00 Noon</td>
						  </tr>
						  <tr>
							<td>78368 – 85113</td>
							<td>Saturday 28 August, 2021</td>
							<td>02:00 PM – 04:00 PM</td>
						  </tr>
						  <tr>
							<td>85114 – 86189</td>
							<td>Sunday 29 August, 2021</td>
							<td>10:00 AM – 12:00 Noon</td>
						  </tr>
						  <tr>
							<td>86190 – 90904</td>
							<td>Sunday 29 August, 2021</td>
							<td>02:00 PM – 04:00 PM</td>
						  </tr>
						  <tr>
							<td>90905 – 95962</td>
							<td>Monday 30 August, 2021</td>
							<td>10:00 AM – 12:00 Noon</td>
						  </tr>
						  <tr>
							<td>95963 – 97000</td>
							<td>Monday 30 August, 2021</td>
							<td>01:00 PM – 03:00 PM</td>
						  </tr>
						  <tr>
							<td>97001 – Onwards</td>
							<td>Monday 30 August, 2021</td>
							<td>03:30 PM – 05:30 PM</td>
						  </tr>
						</tbody>
					  </table>

					  <table class="table table-bordered test-table">
						<thead class="thead-dark">
						  <tr>
							<th colspan="3">FEMALE</th>
						  </tr>
						</thead>
						<tbody>
						  <tr>
							<td><b>Roll Numbers</b></td>
							<td><b>Date</b></td>
							<td><b>Time</b></td>
						  </tr>
						  <tr>
							<td>75765 – 81912</td>
							<td>Saturday 28 August, 2021</td>
							<td>10:00 AM – 12:00 Noon</td>
						  </tr>
						  <tr>
							<td>81919 – 94525</td>
							<td>Saturday 28 August, 2021</td>
							<td>02:00 PM – 04:00 PM</td>
						  </tr>
						  <tr>
							<td>94526 – Onwards</td>
							<td>Monday 30 August, 2021</td>
							<td>10:00 AM – 12:00 Noon</td>
						  </tr>
						</tbody>
					  </table>

					<p><b>Kindly follow the instructions:</b></p>
					<ol class="ol-just" start="1">
						<li>Computer based test will be of 100 MCQs (Physics 35 MCQs, Maths 35 MCQs, Chemistry 20 MCQs and English 10 MCQs).</li>
						<li>Reach the test place 30 min before the test time.</li>
						<li>Bring your roll number slip and CNIC on the day of test. You will NOT be allowed to sit for the computer-based test if you do not have your roll number slip.</li>
						<li>No negative marking.</li>
						<li>Mobile phones and calculators will not be allowed.</li>
						<li>Applicants should wear mask and observe COVID-19 SOPs.</li>
						<li>In case of any query contact PS to Dean FET: 051-9257949.</li>
					</ol>
				</div>
				<!-- Modal footer -->
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Close</button>
				</div>
				
			</div>
		</div>
	</div>










</div> <!--Ending container --> 

@include('results.footer')