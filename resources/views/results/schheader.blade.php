<!DOCTYPE html>
<html lang="en">
<head>
	<title>@yield('title')</title>
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="" crossorigin="anonymous">
	<link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
	<link href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css" rel="stylesheet">
	<link rel="stylesheet" href={{ url('css/results.css') }}>
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
</head>
<body class="{{(Request::route()->getName() == 'programes' || Request::route()->getName() == 'fee-structure') ? 'remove-responsive-fp' : '' }}">
	<div class="container-fluid header">
		<img src={{ asset("storage/images/scholarship/iiui-sch-new.jpg") }} id="iiui-top-banner" alt="iiui logo">
		<div class="topheader-btn">
			<!-- <a href="/" class="btn btn-sm btn-primary mb-2 ml-2 mr-2">Signup</a> -->
		</div>
	</div> <!-- Ending contianer-fluid header -->
	<nav class="navbar sticky-top navbar-expand-lg top-menu">
		<button class="navbar-toggler navbar-toggler-right ml-auto  float-xs-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse" id="navbarSupportedContent">
			<ul class="navbar-nav mr-auto">

				<!-- Scholarship Page Change by Haider -->
				<li class="nav-item">
					<!-- <a class="nav-link" href="#" target="_blank">ScholarShips</a> -->
					
<!--					<a class="nav-link {{ (Request::route()->getName() == 'scholarshipdetail') ? 'active' : '' }}" href="{{ route('scholarshipdetail') }}">Scholarships</a>-->
				</li>
				<!-- Change End -->
			</ul>
		</div>
	</nav>


