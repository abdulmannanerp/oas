@section('title')
	Result List
@endsection
	@include('results.header')
	@php 
		use App\Http\Controllers\ResultController;
	@endphp
	<div class="container-fluid result-list">
		@if ( $result->count() )
		<div class="page-content row cnicDanger">
			<div class="page-content-wrapper no-margin">
				@if($errors->any())
					<div class="alert alert-danger">
						<ul>
							@foreach ( $errors->all() as $error ) 
								<li>{{ $error }}</li>
							@endforeach
						</ul>
					</div>
				@endif
			</div>
		</div>
		<div class="row">
			{{-- <div class="col-lg-2">
				@include('merit.front.tab')
			</div> --}}
			<div class="col-lg-12">
					<div class="info-container text-center">
						@php
						echo "<div class='program-compare'><h5>Program: ".$programTitle->title."</h5></div>";
						echo $text;	
						@endphp
					</div>
					<table id="results-compare" class="display" style="width:100%">
						<thead class="thead list-thead">
							<tr>
								<td>S.No</td>
								<td>Roll Number</td>
								<td>Name</td>
								<td>Father Name</td>
								<td>PQM</td>
								<td>Admission Offered</td>
								<td>List Number</td>
							</tr>
						</thead>
						<tbody>
						<?php $i = 0; $inCorrectRollNumbers = [];  $remaining = 0; $notSelected = 0; ?>	
						@foreach ( $result as $list )
							<tr>
								<td> {{ ++$i }} </td>
								<td> {{ $list->getRollNumber->id }} </td>
								<td> {{ $list->applicant->name }} </td>
								<td> {{ $list->applicant->detail->fatherName }} </td>
								<td> {{ $list->pqm }} </td>
								<td>
									@php
										echo (isset($list->getRollNumber->rollNo) && $list->getRollNumber->rollNo->result->type == 'Result') ? '<span class="text-success"><b>Yes</b></span>' : '<span class="text-danger"><b>No</b></span>';
									@endphp  
								</td>
								<td>{{isset($list->getRollNumber->rollNo->result->listNumber)? $list->getRollNumber->rollNo->result->listNumber : ''}}</td>
							</tr>
						@endforeach
						@php
							// Log::info('------------------- Start '.$resultDetail->result->program->title.'-------------------');
							// Log::info($inCorrectRollNumbers);
							// Log::info('------------------- End '.$resultDetail->result->program->title.'-------------------');
						@endphp
						</tbody>
					</table>
			</div>
		</div>
		@else
			<div class="row" style="padding:20px 0">
				<div class="col-lg-2">
					<h3>No record found</h3>
				</div>
			</div>
		@endif
	</div>
	@include('results.footer')