@section('title')
Results
@endsection
@include('results.header')

	<div class="container-fluid p50tb results-main-container announcements-panel small-fluid bg-ff mt-3">
		
		<div class="row">
			<div class="col-md-12">
				{{-- <h6>
					Merit Lists for Different Undergraduate Programs are uploaded against limited seats. Admissions will be offered on a first come first serve basis. <b>Last date of fee submission has been extended till Tuesday, January 31, 2023, for selected programs. </b>
				</h6> --}}
			</div>
		</div>
		<div class="row">

			<div class="col-lg-2">
				@include('merit.front.tab')
			</div>

			<div class="col-lg-9">	
				<div class="row main-title bord-btm">
					<div class="col-md-6 pd-lfz">
						<h5 class="text-left">Admission Results {{$title}} </h5> 
					</div>
					<div class="col-md-6">
						@if($programs->isNotEmpty())
							<a href="{{route('recent-results')}}" class="blink_me float-right" target="_blank">Recent Results</a>
							{{-- <a href="https://www.iiu.edu.pk/wp-content/uploads/2021/09/Notice-LLB-hons-LAT-030921.pdf" class="blink_me float-right" target="_blank">Notice For BA/LLB (Hons) Shariah & Law Applicants</a> --}}
						@else
							<span class="blink_me float-right">No Results Available</span>
						@endif
					</div>
				</div>

				<div class="row">
					<div class="col-md-12 text-center search-results-section pad-zer pt-3">
						<form class="border border-light res-frm" action={{route('displayResults')}} method="POST">
							<select name="programme" id="programme" class="select2 browser-default custom-select mb-3 float-left" required style="width: 350px; margin: 10px;">
								<option value="">-- Select Program -- </option>
								@foreach ( $programs as $program ) 
									<option value="{{$program->program->pkProgId}}"> {{ $program->program->title.' - '.$program->program->duration. ' Year(s)'}} </option>
								@endforeach 
							</select>
							<button class="btn btn-info res-sub select2-submit sbbm" type="submit">Search</button>
						</form>
					</div> 
				</div>

				<div class="container list-container clr custom-lst announcements-panel bg-ff">
					@if ( !empty( $results ) ) 
					<div class="row">
						<h5 class="announcements-heading mb-3 mt-3">Search Results</h5>
					</div>
						<div class="row">
							@foreach ( $results as $result )



							<div class="col-sm-12 col-md-12 results-list">
								<div class="col-lg-7 flt-lft">
									<i class="fa fa-angle-double-right" aria-hidden="true"></i>						
									<span class="result-sze">
										{{ $result->program->title .' - '.$result->program->duration. ' Year(s)' }} - {{ $result->gender }}
									</span>
								</div>
								<div class="col-lg-2 flt-lft">
										<a class="card-title" target="_blank" href={{route('displayLists', ['id' => $result->pkResultsId])}}>
										<i class="fa fa-external-link-square" aria-hidden="true"></i>	
										@if ($result->type == 'Interview')
											{{'Interview List'}}

										@elseif($result->type == 'Waiting')
											@if($result->list_text != NULL)
												{{$result->list_text}}
											@else
												{{'Waiting list'}}
											@endif


										@else
											@if($result->list_text != NULL)
												{{$result->list_text}}
											@else
												{{$result->listNumber. ' Merit list '}}
											@endif
										@endif					
										{{-- {{ ($result->type == 'Interview') ? 'Interview List' : $result->listNumber. ' Merit list '}} --}}
										</a>
								</div>
								<div class="col-lg-3 flt-lft result-sze text-right">
										<i class="fa fa-calendar" aria-hidden="true"></i> Published: {{ \Carbon\Carbon::parse($result->created_at)->format('d-m-Y')}}
								</div>
							</div>



							
							
							@endforeach
						</div> 
					@endif
				</div> <!-- list-container -->

				<p style="padding-top: 80px;"></p>
			</div>
		</div>

		


	</div> <!--Ending container --> 

	@include('results.footer')
