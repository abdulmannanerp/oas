<div class="footer row">
    <div class="container-fluid">
        <div class="row w3_footer_grids">
            <div class="col-md-4 col-sm-12 w3_footer_grid">
                <h3>About Us</h3>
                <p style="color:white; text-align: justify;">
                    With scarce grant resources, creating options for new, diverse, and multiple funding streams is essential for the sustainability and development of universities.
The Office of the University Advancement has recently been established in International Islamic University in conformity with Higher Education Commission (HEC) advices. The rationale behind is to amplify the IIUI alumni and fund raising activities for elevating the education for needy students.
                </p>
            </div>
            <div class="col-md-4 col-sm-12 w3_footer_grid">
                <h3>Scholarship (Male)</h3>
                
                <ul class="address">
                    <li><i class="fa fa-phone-square" aria-hidden="true"></i>&nbsp;+92-51-9257985, +92-51-9019327, +92-51-9019871</li>
                    <li><i class="fa fa-envelope" aria-hidden="true"></i><a href="mailto:Scholarships.fc@iiu.edu.pk"> Scholarships.fc@iiu.edu.pk</a></li>
                    <li><i class="fa fa-print" aria-hidden="true"></i> &nbsp;+92-51-9257929</li>
                </ul>
            </div>

            <div class="col-md-4 col-sm-12 w3_footer_grid">
                <h3>Scholarship (Female)</h3>
                
                <ul class="address">
                    <li><i class="fa fa-phone-square" aria-hidden="true"></i>&nbsp;+92-51-9257985, +92-51-9019327, +92-51-9019871</li>
                    <li><i class="fa fa-envelope" aria-hidden="true"></i><a href="mailto:Scholarships.fc@iiu.edu.pk"> Scholarships.fc@iiu.edu.pk</a></li>
                    <li><i class="fa fa-print" aria-hidden="true"></i> &nbsp;+92-51-9257929</li>
                </ul>
            </div>
            <div class="clearfix"> </div>
        </div>
    </div>
    
    <div class="footer-copy">
        
        <div class="container">
            <p>© <?= date('Y') ?> IIUI Scholarship System. All rights reserved | Powerd by <!--<a href="http://www.iiu.edu.pk/?page_id=16403" target="_blank"> -->IT Center IIUI<!--</a>--></p>
        </div>
    </div>
    {{-- <span style="float:right; color: #999">{{number_format( (microtime(true) - LARAVEL_START), 2 )}}</span> --}}
</div>
<!-- <div class="footer-botm row">
    <div class="container-fluid">
        <div class="w3layouts-foot">
            <ul class="ft-icons">
                <li><a href="https://www.facebook.com/iiu.isbpk/" target="_blank" class="w3_agile_facebook"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                <li><a href="https://twitter.com/iiui_official?lang=en" target="_blank" class="agile_twitter"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                <li><a href="https://pk.linkedin.com/company/iiui" target="_blank" class="w3_agile_dribble"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                <li><a href="https://www.instagram.com/explore/locations/262176346/international-islamic-university-islamabad/" target="_blank" class="w3_agile_dribble"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
            </ul>
        </div>
        <div class="payment-w3ls">	
                <img src="images/card.png" alt=" " class="img-responsive">

        </div>
        <div class="clearfix"> </div>
    </div>
</div> -->

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    </script>
	<script src={{url('js/results.js')}}></script>
	<script>
		$(function(){
            $('.select2').select2();
			$('#faculty').on('change', function(){
               $.ajax({
                   method: 'post',
                   url: '{{ route('getDepartmentsResults') }}',
                   data: {
                       id: $(this).val()
                   },
                   success: function ( response ) {
                   		console.log ( response );
                       $('#department').empty();
                       $('#department').append(
                            '<option value="">--Select Department--</option>'
                        );
                       $.each(response, function (key, value) {
                           $('#department').append(
                               '<option value="'+value.pkDeptId+'">'+ value.title+'</option>'
                           );
                       });
                   }
               });
            });

            $('#department').on('change', function(){
                $.ajax({
                    method: 'post',
                    url: '{{ route('getProgrammeResults') }}',
                    data: {
                        id: $(this).val()
                    },
                    success: function (response) {
                    	console.log ( response );
                        $('#programme').empty();
                        if ( response == '' ) {
                            $('#programme').append(
                                '<option value="">--Select Porgramme--</option>'
                            );
                            return;
                        }
                        $('#programme').append(
                            '<option value="">--Select Porgramme--</option>'
                        );
                        $.each(response, function (key, value) {
                            $('#programme').append(
                                '<option value="'+value.pkProgId+'">'+ value.title+'</option>'
                            );
                        });
                    }
                });
            });
		});
		$('#lgn-btn').click(function() {
   			window.location = "http://Scholarship.iiu.edu.pk/login";
		});
        // When the user scrolls down 20px from the top of the document, show the button
        window.onscroll = function() {scrollFunction()};

        function scrollFunction() {
            if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
                document.getElementById("topBtn").style.display = "block";
            } else {
                document.getElementById("topBtn").style.display = "none";
            }
        }

        // When the user clicks on the button, scroll to the top of the document
        function topFunction() {
            document.body.scrollTop = 0; // For Safari
            document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
        }

        $(function(){
            $('.faculty-title').on('click', function(){
                $this = $(this);
                // console.log ( $this.find('i'));
                if ( $this.find('i').eq(0).hasClass('fa-minus') ) {
                    $this.find('i').eq(0).removeClass('fa-minus').addClass('fa-plus');
                }else{
                    $this.find('i').eq(0).removeClass('fa-plus').addClass('fa-minus');
                }
            })
        });


        $(document).ready(function() {
            $('#recent-results').DataTable({
                order: [],
                columnDefs: [
                    { width: "600px", targets: 0 },
                    { width: "150px", targets: 1 },
                    { width: "150px", targets: 2}
                ]
            });

            $('#results-compare').DataTable({
                "pageLength": 100
            });
            
        } );
                </script>
                
<button onclick="topFunction()" id="topBtn" title="Go to top"><i class="fa fa-angle-up up-ic-fnt"></i></span></button>
</body>
</html>