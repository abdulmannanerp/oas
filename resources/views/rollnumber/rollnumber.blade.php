<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Print Slip IIU :: Admission</title>
<link href="https:/stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
</head>

<body>
<style>
body{
	font-size:16px;
	line-height:20px;
	color:#000;
	font-family:Arial, Helvetica, sans-serif;
	}
	p{
		margin:0px 0px 2px 0px;
		}
.admission-form-main{
	width:700px;
	 margin:0 auto;
	}
.admission-form-main p{
	font-weight:bold;
	}
.print-button{
text-decoration:none;
color:#000;
font-weight:bold;
font-size:14px;
margin-top:5px;
}		
table {
    border-collapse: collapse;
    border-spacing: 0;
    width: 100%;
    border: 1px solid #ddd;
}

th, td {
    text-align: left;
    padding:5px 5px;
	border: 1px solid #ddd;
}
.table-main{
	padding-bottom:10px;
	width:100%;
	}
.row::after {
    content: "";
    clear: both;
    display: table;
}
[class*="col-"] {
    float: left;
   
}
.col-1 {width: 8.33%;}
.col-2 {width: 16.66%;}
.col-3 {width: 25%;}
.col-4 {width: 33.33%;}
.col-5 {width: 41.66%;}
.col-6 {width: 50%;}
.col-7 {width: 58.33%;}
.col-8 {width: 66.66%;}
.col-9 {width: 75%;}
.col-10 {width: 83.33%;}
.col-11 {width: 91.66%;}
.col-12 {width: 100%;}
.btn-default {
    color: #333;
    background-color: #fff !important;
    border-color: #ccc !important;
	margin-right:5em;
}

.btn {
    display: inline-block;
    padding: 6px 12px;
    margin-bottom: 0;
    font-size: 14px;
    font-weight: 400;
    line-height: 1.42857143;
    text-align: center;
    white-space: nowrap;
    vertical-align: middle;
    -ms-touch-action: manipulation;
    touch-action: manipulation;
    cursor: pointer;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    background-image: none;
    border: 1px solid transparent;
    border-radius: 4px;
}

	
@media (max-width:500px){ 
.row, .text-right{
	text-align:center !important;
	}
.col-1, .col-2, col-3, .col-4, .col-5, .col-6, .col-7, .col-8, .col-9, .col-10, .col-11, .col-12{
	width:100%;
	}
.table-main{
	width:100%;
	overflow:scroll;
	
	}
.table-main tr td:first-child{
	background-color:#eee !important;
	}	
.admission-form-main {
	width:100%;
	
	}
.table-main table{
	width:700px;
	}		

}	
@media print { 
#printpagebutton{
	display:none;	
	
	}
.name-mail {
	padding-bottom:5px;
	}
.table-main{
	padding-bottom:5px;
	width:100%;
	}
body{
	font-size:12px;
	}
table{
	width:100% !important;
	overflow:hidden !important;
	}
.admission-form-main {
	width:98% !important;
	overflow:hidden !important;
	
	}
	body{
	font-size:16px;
	line-height:20px;
	color:#000;
	font-family:Arial, Helvetica, sans-serif;
	}	

}
.text-center{
	text-align:center;
	}
.text-right{
	text-align:right;
	}
/* .row p{
	font-size:12px;
	
	}	 */
h3{
	line-height:22px;
	margin:0;
	}	
.name-mail {
	padding-bottom:10px;
	}	
.name-mail p, .decalartion p{
	font-weight:normal !important;
	}	
.name-mail p span{
	font-weight:bold !important;
	}
.decalartion p{
	line-height:20px;
	margin-bottom:15px;
	}				
	.decalartion{
		padding-top:15px;
		}	
.top-bar p{
	line-height:20px;
  }
  #pic1{
  border: 1px solid #ddd;
  width: 140px;
  height: 140px;
  }
</style>
<a id="printpagebutton" class="btn btn-default" style="float:right; margin-top: 1em;" type="button"  onclick="printpage()"/><i class="fa fa-print"></i> Print</a>
<div class="admission-form-main">
  <div class="row top-bar" style="padding-bottom:15px;">
    <div class="col-2" style="padding-top:10px;width: 12.66%;"><img style="width:100px;"src="/storage/images/iiulogo.jpg" /></div>
    <div class="col-8 text-center">
      <img src="/storage/images/jami.jpg" />
      <p style="font-size: 15px;">INTERNATIONAL ISLAMIC UNIVERSITY, ISLAMABAD</p>
      {{-- <p>TEST ENTRY FOR {{strtoupper($oas_applications->applicant->programLevel->programLevel)}}  PROGRAMME</p> --}}
      <small><span>H-10 Campus Islamabad, Pakistan, www.iiu.edu.pk <br />E-mail: admissions@iiu.edu.pk</span></small> 
      <h3>Roll No Slip - (
        @if($oas_applications->fkSemesterId == 14 && $oas_applications->fkProgramId == 157)
          {{ 'Fall-2020' }}
        @else
          {{$oas_applications->semester->title}}
        @endif
        )</h3>
    </div>
      <?php 
      // $exploded = explode('.', $oas_applications->applicant->applicantDetail->pic);
      // $pic = str_replace('.'.end($exploded), "_thumb.jpg" , $oas_applications->applicant->applicantDetail->pic);
      ?>
    <div class="col-2 text-right"> 
      {{-- <img  width="150" height="130" src="http://admission.iiu.edu.pk/{{$oas_applications->applicant->applicantDetail->pic}}" />  --}}
      <img src="/storage/{{($oas_applications->applicant->applicantDetail->pic == '')? '/images/default.png': $oas_applications->applicant->applicantDetail->pic}}" id="pic1" alt="Profile Pic" class="img-thum">
    </div>
  </div>
  <?php
  // dd();
  ?>
  <div class="table-main">
    <table>
      <tr>
        <td><b>Roll No</b></td>
      <td colspan="3"><b>{{$oas_applications->getRollNumber->id}}</b></td>
      </tr>

      <tr>
        <td>Program </td>
      <td><b>{{$oas_applications->program->title}}</b></td> 
        <td>Faculty </td>
        <td>{{$oas_applications->program->faculty->title}}</td>
      </tr>  
      <tr>
        <td>Name </td>
        <td>{{ucwords($oas_applications->applicant->name)}}</td>
        <td>Father Name</td>
        <td>{{ucwords($oas_applications->applicant->applicantDetail->fatherName)}}</td>
      </tr> 
      <tr>
        <td>CNIC/B-Form No</td>
        <td>{{ucwords($oas_applications->applicant->cnic)}} </td>
        <td>Test Date</td>
      <td>
        @if($active_semester == $oas_applications->fkSemesterId)
          @if($oas_applications->program->programscheduler->testDateTime != '0000-00-00 00:00:00')
            <b>{{date("d-m-Y H:i:s",strtotime($oas_applications->program->programscheduler->testDateTime))}}</b>
          @endif
          @if($oas_applications->program->pkProgId == 189 && !is_null($oas_applications->getRollNumber->engineering))
            <b>{{date("d-m-Y H:i:s",strtotime($oas_applications->program->programscheduler->testDateTime))}}</b>
            {{-- <b>{{$oas_applications->getRollNumber->engineering->date .' '. $oas_applications->getRollNumber->engineering->time .' '. $oas_applications->getRollNumber->engineering->location}}</b> --}}
          @endif
          @if($oas_applications->program->pkProgId == 223 || $oas_applications->program->pkProgId == 126 || $oas_applications->program->pkProgId == 127)
            @php echo 'The test schedule is available on this <a target="_blank" href="https://www.iiu.edu.pk/?p=62912">link</a>' @endphp
          @endif
        @endif
      </td>
      </tr>
    </table>
  </div>
  
  <div class="row">
  <div class="col-12">
  <p>Postal Address of the Candidate:</p>
  <div class="table-main">
    <table>
      <tr>
        <td>Name </td>
        <td>{{ucwords($oas_applications->applicant->name)}}</td>
      </tr>
      <tr>
        <td>Address </td>
      <td>{{ucwords($oas_applications->applicant->applicantAddress->addressPo)}}</td>
      </tr>
      <tr>
        <td>City</td>
        <td>{{ucwords($oas_applications->applicant->applicantAddress->cityPo)}}</td>
      </tr>
      {{-- <tr>
        <td>Phone No</td>
        <td>{{ucwords($oas_applications->applicant->applicantAddress->phonePo)}}</td>
      </tr> --}}
    </table>
  </div>
  <p>This roll number slip is issued provisionally and doesn't confer any self eligibility for admission even after
clearance of admission test.</p>
  
  </div>
 </div> 
  
  <span style="border: 2px solid #000;display: inline-block;padding: 5px;"><h4 style="line-height: 0px;margin-top: 15px;">Instructions:</h4><ol class="roll_ol">
      {{-- <li>
        You must bring this Roll no. slip and Photo ID document for verification to exam hall. Photo ID document can be CNIC, passport, SSC or HSSC mark sheet/certificate, Driving License etc. That has your recent photograph and can be used for verification purposes.
      </li> --}}
      {{-- <li>
        <b>Note:</b> Candidate(s) are advised to appear before the entry test with strict observance of SOPs; wearing of face mask, frequent use of hand sanitizer, maintenance of social distancing etc. Please do not appear if have any symptoms such as fever cough or sneezing. 
      </li> --}}
      <li>
        Candidate should apply only if eligible. Admission to ineligible candidate either due to over sight or due to concealment of fact by the candidate is liable to cancellation at any stage.
      </li>
      {{-- <li>
          Possession of Cell Phones in the Examination hall is not allowed. 
      </li> --}}
    </ol>
  </span>
  <div class="col-4 text-center" style="padding-top:70px; float:right;" >
      <p style="padding-bottom:10px; border-top:1px solid #333; line-height:20px">Signature of the Applicant</p>
      {{-- <p style=" border-top:1px solid #333; line-height:20px">Receiver's Signature and stamp</p> --}}
      </div>
</div>
<style>
.roll_ol li{
  text-align: justify;
  font-size: 13px;
}
.roll_ol{
  margin: 0px;
}
</style>
<script src="http://admission.iiu.edu.pk/assets/challanassets/print.js"></script>

<script>
    function printpage() {
        //Get the print button and put it into a variable
        var printButton = document.getElementById("printpagebutton");
        //Set the print button visibility to 'hidden' 
        printButton.style.visibility = 'hidden';
        //Print the page content
        window.print()
        //Set the print button to 'visible' again 
        //[Delete this line if you want it to stay hidden after printing]
        printButton.style.visibility = 'visible';
    }
</script>
</body>
</html>
