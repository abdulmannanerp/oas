@extends('layouts.app')
@section('content')
    <div class="page-content row printable">
        <div class="page-content-wrapper m-t">
        	<div class="sbox">
				<div class="sbox-title">
						<h3>Signup Stats</h3>
						<button class="btn btn-default flt-rgt top-btn" onclick="printIt()"><i class="fa fa-print" aria-hidden="true"></i></button>
				</div>
				<div class="sbox-content">
					<div class="row">
						<div class="col-xs-12">
							<div id="chart-container">
								<canvas id="graphCanvas"></canvas>
							</div>
						</div>
					</div> <!-- Ending row -->
        		</div>
			</div> <!-- Ending container --> 
			<br />
			
			{{-- <div class="sbox">
				<div class="sbox-title">
						<h3>Program Shift</h3>
				</div>
				<div class="sbox-content">
					<div class="row">
						<div class="col-xs-12">
							<div id="chart-container">
								<canvas id="graphCanvasShift"></canvas>
							</div>
						</div>
					</div> <!-- Ending row -->
        		</div>
			</div> <!-- Ending container -->  --}}
			
        </div> <!-- Ending page-content-wrapper --> 
	</div> <!-- Ending page-content-row --> 


	<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.bundle.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.3/jspdf.debug.js"></script>
    <script>
		$(document).ready(function () {
			showGraph();
			// showGraphShift();
        });


        function showGraph(){
				var data = <?= $applicants ?>;
				console.log(data);
                    var name = [];
                    var marks = [];

                    for (var i in data) {
                        name.push(data[i].date);
                        marks.push(data[i].id);
                    }

                    var chartdata = {
                        labels: name,
                        datasets: [
                            {
                                label: 'Sign Ups',
                                backgroundColor: '#49e2ff',
                                borderColor: '#46d5f1',
                                hoverBackgroundColor: '#CCCCCC',
                                hoverBorderColor: '#666666',
                                data: marks
                            }
                        ]
                    };

                    var graphTarget = $("#graphCanvas");

                    var barGraph = new Chart(graphTarget, {
                        type: 'bar',
						data: chartdata,
						options: {
							"hover": {
							"animationDuration": 0
							},
							"animation": {
							"duration": 1,
							"onComplete": function() {
								var chartInstance = this.chart,
								ctx = chartInstance.ctx;

								ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
								ctx.textAlign = 'center';
								ctx.textBaseline = 'bottom';

								this.data.datasets.forEach(function(dataset, i) {
								var meta = chartInstance.controller.getDatasetMeta(i);
								meta.data.forEach(function(bar, index) {
									var data = dataset.data[index];
									ctx.fillText(data, bar._model.x, bar._model.y - 5);
								});
								});
							}
							},
							legend: {
							"display": true
							},
							tooltips: {
							"enabled": true
							}
						}
					});
		}


		// function showGraphShift(){
		// 		var data = <?php /* echo $logs */ ?>;
		// 		console.log(data);
        //             var name = [];
        //             var marks = [];

        //             for (var i in data) {
        //                 name.push(data[i].date);
        //                 marks.push(data[i].id);
        //             }

        //             var chartdata = {
        //                 labels: name,
        //                 datasets: [
        //                     {
        //                         label: 'Program Shift',
        //                         backgroundColor: '#49e2ff',
        //                         borderColor: '#46d5f1',
        //                         hoverBackgroundColor: '#CCCCCC',
        //                         hoverBorderColor: '#666666',
        //                         data: marks
        //                     }
        //                 ]
        //             };

        //             var graphTarget = $("#graphCanvasShift");

        //             var barGraph = new Chart(graphTarget, {
        //                 type: 'bar',
		// 				data: chartdata,
		// 				options: {
		// 					"hover": {
		// 					"animationDuration": 0
		// 					},
		// 					"animation": {
		// 					"duration": 1,
		// 					"onComplete": function() {
		// 						var chartInstance = this.chart,
		// 						ctx = chartInstance.ctx;

		// 						ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
		// 						ctx.textAlign = 'center';
		// 						ctx.textBaseline = 'bottom';

		// 						this.data.datasets.forEach(function(dataset, i) {
		// 						var meta = chartInstance.controller.getDatasetMeta(i);
		// 						meta.data.forEach(function(bar, index) {
		// 							var data = dataset.data[index];
		// 							ctx.fillText(data, bar._model.x, bar._model.y - 5);
		// 						});
		// 						});
		// 					}
		// 					},
		// 					legend: {
		// 					"display": true
		// 					},
		// 					tooltips: {
		// 					"enabled": true
		// 					}
		// 				}
		// 			});
		// }
		

    </script>
@stop
