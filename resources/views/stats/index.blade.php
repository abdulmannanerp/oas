@extends('layouts.app')
@section('content')
    <div class="page-content row">
        <div class="page-content-wrapper m-t">
        	<div class="sbox">
					<div class="sbox-title">
							
					</div>
				<div class="sbox-content">
        		<div class="row">
					<div class="col-xs-3">
						<form action=""></form>
        					<h4 id="stats-comparison-heading">Stats Comparison</h4>
        					<p class="stats-comparison-single-feature stats-comparison-feature-heading">Fee Confirmed</p>
        					<p class="stats-comparison-single-feature stats-comparison-feature-heading">Total Applied</p>
        					<p class="stats-comparison-single-feature stats-comparison-feature-heading">Male Applicant</p>
        					<p class="stats-comparison-single-feature stats-comparison-feature-heading">Female Applicant</p>
        					<p class="stats-comparison-single-feature stats-comparison-feature-heading">Revenue (Rs)</p>
					</div>
        			<div class="col-xs-3">
        				<form action="">
        					<select clsas="semester" name="semester" id="first-filter" class="form-control">
        						<option value="">--Select Semester--</option>
        						@foreach ( $semesters as $semester )
        							<option value="{{ $semester->pkSemesterId}}"> {{ $semester->title }}</option>
        						@endforeach
        					</select>
        					{{-- <hr> --}}
        					<div class="stats-wrapper"></div>
        				</form>
        			</div>
        			<div class="col-xs-3">
        				<form action="">
        					<select clsas="semester" name="semester" id="second-filter" class="form-control">
        						<option value="">--Select Semester--</option>
        						@foreach ( $semesters as $semester )
        							<option value="{{ $semester->pkSemesterId}}"> {{ $semester->title }}</option>
        						@endforeach
        					</select>
        					{{-- <hr> --}}
        					<div class="stats-wrapper"></div>
        				</form>
        			</div>
        			<div class="col-xs-3">
    					<h4 id="stats-comparison-heading">Difference / Percentage </h4>
    					<div class="difference-percentage-section"></div>
    					
					</div>
				</div> <!-- Ending row -->
        	</div>
        	</div> <!-- Ending container --> 
        </div> <!-- Ending page-content-wrapper --> 
    </div> <!-- Ending page-content-row --> 
    <script>
    	$(function(){
    		var firstFilter = [];
    		var secondFilter  = [];
    		var difference = [];
    		var percentage = [];
    		$('#first-filter, #second-filter').on('change', function(){
    			var $this = $(this);
    			var statsWrapper = $this.next('.stats-wrapper').eq(0);
    			if ( $this.val() != '' ) 
    			{
    				$.ajax({
    					url: '{{route('getStatsOfASemester')}}',
    					method: 'POST', 
    					data: {
    						id:  $this.val()
    					},
    					success: function ( response ) {
    						statsWrapper.empty();
    						if (  $this.attr('id') == 'first-filter' ) { firstFilter = []; }
							if ( $this.attr('id') == 'second-filter' ) { secondFilter = []; }

    						$.each(response, function (key, value){
    							if ( response == '' ) {
    								statsWrapper.append('<p class="stats-comparison-single-feature"> No record for selected semester </p>');
    							} else {

	    							if (  $this.attr('id') == 'first-filter' ) 
	    							{
	    								firstFilter.push ( value.meta_value );
	    							}
	    							if ( $this.attr('id') == 'second-filter' )
	    							{
	    								secondFilter.push(value.meta_value);
	    							}

	    							if ( key == 0 && value.meta_key == 'Fee Confirmed')
	    								statsWrapper.append('<p class="stats-comparison-single-feature">' + value.meta_value + '</p>');
	    							else if ( key == 1 && value.meta_key == 'Total Applied')
	    								statsWrapper.append('<p class="stats-comparison-single-feature">' + value.meta_value + '</p>');
	    							else if ( key == 2 && value.meta_key == 'Male Applicant')
	    								statsWrapper.append('<p class="stats-comparison-single-feature">' + value.meta_value + '</p>');
	    							else if ( key == 3 && value.meta_key == 'Female Applicant')
	    								statsWrapper.append('<p class="stats-comparison-single-feature">' + value.meta_value + '</p>');
	    							else if ( key == 4 && value.meta_key == 'Revenue (Rs)')
	    								statsWrapper.append('<p class="stats-comparison-single-feature">' + value.meta_value + '</p>');
    							} //ending else
    						}); //ending foreach

    						//calculate the differnece
    						if ( $this.attr('id') == 'second-filter' ) {
    							firstFilter = firstFilter.map( x => parseInt(x));
    							secondFilter = secondFilter.map(x => parseInt(x));
    							if ( firstFilter != '' && secondFilter != '' ) 
    							{
    								difference = [];
    								percentage = [];
    								secondFilter.forEach( function ( element, index) {
    									difference.push ( element - firstFilter[index]);
    								});
    								difference.forEach ( function ( element, index) {
    									percentage.push ( (element / firstFilter[index])* 100 );
    								});

    								$('.difference-percentage-section').empty();
    								difference.forEach( function(element, index){
    									$('.difference-percentage-section').append(
    										'<p class="stats-comparison-single-feature">'+'<strong>'+element+'</strong>' +' / '+ percentage[index].toFixed(0) +'%</p>'
    									);
    								});
    							}
    						}
    					} //ending success
    				});
    			}
    		});
    	});
    </script>
@stop
