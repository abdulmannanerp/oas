@extends('layouts.app')
@section('content')
    <div class="page-content row">
        <div class="page-content-wrapper m-t">
        	<div class="sbox">
					<div class="sbox-title">
					<div class="row">
						<?php $s_fr = ''; $s_to = ''; ?>
							<div class="col-md-2">
								<h4 id="stats-comparison-heading">Stats Comparison</h4>
							</div>
							<form id="appSearching" method="POST" action="{{route('program-stats')}}" >
							{{ csrf_field() }}
							<input type="hidden" value="1" name="form_submit">
			
							<div class="col-md-2 from-print">
								<select clsas="semester" required="required" name="semester_from" id="first-filter" class="form-control">
										<option value="">--Select Semester--</option>
										@foreach ( $semesters as $semester )
											<option value="{{ $semester->pkSemesterId}}" <?php if($semester_from){ if($semester->pkSemesterId == $semester_from){ echo 'selected'; $s_fr = $semester->title;}}  ?>> {{ $semester->title }}</option>
										@endforeach
								</select>
							</div>
							<div class="col-md-2 to-print">
								<select clsas="semester" required="required" name="semester_to" id="second-filter" class="form-control">
										<option value="">--Select Semester--</option>
										@foreach ( $semesters as $semester )
											<option value="{{ $semester->pkSemesterId}}" <?php if($semester_to){ if($semester->pkSemesterId == $semester_to){ echo 'selected'; $s_to = $semester->title;}}  ?> > {{ $semester->title }}</option>
										@endforeach
								</select>
							</div>
							{{-- <div class="col-md-2">
								<select clsas="semester" required="required" name="pds" id="third-filter" class="form-control">
										<option value="">--Select--</option>
										<option value="Programs" {{ ($pds =='Programs') ? 'selected' : ''}}>Programs</option>
										<option value="Departments" {{ ($pds == 'Departments') ? 'selected' : ''}}>Departments</option>
										<option value="Faculty" {{ ($pds == 'Faculty') ? 'selected' : ''}}>Faculty</option>
								</select>	
							</div> --}}
							<div class="col-md-3">
								<div class="sbox-tools">
									<button name="save" class="tips btn btn-sm btn-save p-save print-hiding" title="Back">
											<i class="fa  fa-paste"></i>Search 
									</button>
								</div>	
							</div>
						</form>	
						<button class="btn btn-default flt-rgt program-print print-hiding"><i class="fa fa-print" aria-hidden="true"></i></button>
						<button class="btn btn-default top-btn flt-rgt hvr-clr program-print-color print-hiding"><i class="fa fa-print" aria-hidden="true"></i></button>
						<span><a class="bt-pre-stat print-hiding" href="{{route('refreshPreviousStats')}}">Refresh Stats</a></span>
									
						</div>
					</div>
					<?php if($semester_from){ ?>
				<div class="sbox-content">
				
        		<div class="row">
					<table style="width:100%;table-layout: fixed;position: sticky;top: 63px;z-index: 10;" class="program-stats-tbl table table-hover table-bordered table-stat">
						<tbody class="table-head bg-clr-top">
						<tr class="ttp-tbr">
							<td class="pr-wd pr-cl">Programs</td>
						  <td colspan="2" class="cls-wd pr-cl">Applied</td>
						  <td colspan="2" class="cls-wd pr-cl">Fee Confirmed</td>
						  <td colspan="2" class="cls-wd pr-cl">Selected</td>
						  <td colspan="2" class="cls-wd pr-cl">Joined</td>
	

						  <td class="cls-wd pr-cl ap-ad">Applied</td>
						  <td class="cls-wd pr-cl ap-ad">Fee Confirmed</td>
						  <td class="cls-wd pr-cl ap-ad">Selected</td>
						  <td class="cls-wd pr-cl ap-ad">Joined</td>
						</tr>
						<tr class="">
							<td class="pr-wd pr-cl"></td>
							<td class="pr-cl">{{$s_fr}}</td>
							<td class="pr-cl">{{$s_to}}</td>
							<td class="pr-cl">{{$s_fr}}</td>
							<td class="pr-cl">{{$s_to}}</td>
							<td class="pr-cl">{{$s_fr}}</td>
							<td class="pr-cl">{{$s_to}}</td>
							<td class="pr-cl">{{$s_fr}}</td>
							<td class="pr-cl">{{$s_to}}</td>

							<td colspan="4" class="diff pr-cl">Difference</td>	
						</tr>
					</tbody>
				</table>


						@if ( $Stats )
						@php $fac=''; $dept= ''; $i = 0; $total_selected1 = 0;$total_joined1=0;  @endphp
							@foreach ( $Stats as $p )
							@php
							// echo 'aaa'.$p->applied_total; exit;
								if($p->applied_total > 0){
									$total_applied = $p->applied_total;
								}else{
									$total_applied = $p->applied_male+$p->applied_female;
								}
								if($p->fee_confirmed_total > 0){
									$total_fee_confirmed = $p->fee_confirmed_total;
								}else{
									$total_fee_confirmed = $p->fee_confirmed_male+$p->fee_confirmed_female;
								}
								if($p->selected_total > 0){
									$total_selected = $p->selected_total;
								}else{
									$total_selected = $p->selected_male+$p->selected_female;
								}
								if($p->joined_total > 0){
									$total_joined = $p->joined_total;
								}else{
									$total_joined = $p->joined_male+$p->joined_female;
								}
							@endphp
							
							
								<?php 
								// if($pds == 'Programs'){
									$total_applied1 = 0;
									$total_fee_confirmed1 = 0;
									$title = $p->program->title;
									if(array_search($p->fkProgramId, array_column($Stats1, 'fkProgramId')) !== False){
									$key = array_search($p->fkProgramId, array_column($Stats1, 'fkProgramId'));
										if($Stats1[$key]['applied_total'] > 0){
											$total_applied1 = $Stats1[$key]['applied_total'];
										}else{
											$total_applied1 = $Stats1[$key]['applied_male']+$Stats1[$key]['applied_female'];
										}
										if($Stats1[$key]['fee_confirmed_total'] > 0){
											$total_fee_confirmed1 = $Stats1[$key]['fee_confirmed_total'];
										}else{
											$total_fee_confirmed1 = $Stats1[$key]['fee_confirmed_male']+$Stats1[$key]['fee_confirmed_female'];
										}
										if($Stats1[$key]['selected_total'] > 0){
											$total_selected1 = $Stats1[$key]['selected_total'];
										}else{
											$total_selected1 = $Stats1[$key]['selected_male']+$Stats1[$key]['selected_female'];
										}
										if($Stats1[$key]['joined_total'] > 0){
											$total_joined1 = $Stats1[$key]['joined_total'];
										}else{
											$total_joined1 = $Stats1[$key]['joined_male']+$Stats1[$key]['joined_female'];
										}
	
									}
								// }elseif('Departments'){
								// 	// dd($p->department);
								// 	$title = $p->title;
								// 	if(array_search($p->id, array_column($Stats1, 'id')) !== False){
								// 	$key = array_search($p->id, array_column($Stats1, 'id'));
								// 		if($Stats1[$key]->applied_total > 0){
								// 			$total_applied1 = $Stats1[$key]->applied_total;
								// 		}else{
								// 			$total_applied1 = $Stats1[$key]->applied_male+$Stats1[$key]->applied_female;
								// 		}
								// 		if($Stats1[$key]->fee_confirmed_total > 0){
								// 			$total_fee_confirmed1 = $Stats1[$key]->fee_confirmed_total;
								// 		}else{
								// 			$total_fee_confirmed1 = $Stats1[$key]->fee_confirmed_male+$Stats1[$key]->fee_confirmed_female;
								// 		}
								// 		if($Stats1[$key]->selected_total > 0){
								// 			$total_selected1 = $Stats1[$key]->selected_total;
								// 		}else{
								// 			$total_selected1 = $Stats1[$key]->selected_male+$Stats1[$key]->selected_female;
								// 		}
								// 		if($Stats1[$key]->joined_total > 0){
								// 			$total_joined1 = $Stats1[$key]->joined_total;
								// 		}else{$
								// 			$total_joined1 = $Stats1[$key]->joined_male+$Stats1[$key]->joined_female;
								// 		}
	
								// 	}
								// }elseif('Faculty'){
								// 	$title = $p->faculty->title;
								// 	if(array_search($p->id, array_column($Stats1, 'id')) !== False){
								// 	$key = array_search($p->id, array_column($Stats1, 'id'));
								// 		if($Stats1[$key]->applied_total > 0){
								// 			$total_applied1 = $Stats1[$key]->applied_total;
								// 		}else{
								// 			$total_applied1 = $Stats1[$key]->applied_male+$Stats1[$key]->applied_female;
								// 		}
								// 		if($Stats1[$key]->fee_confirmed_total > 0){
								// 			$total_fee_confirmed1 = $Stats1[$key]->fee_confirmed_total;
								// 		}else{
								// 			$total_fee_confirmed1 = $Stats1[$key]->fee_confirmed_male+$Stats1[$key]->fee_confirmed_female;
								// 		}
								// 		if($Stats1[$key]->selected_total > 0){
								// 			$total_selected1 = $Stats1[$key]->selected_total;
								// 		}else{
								// 			$total_selected1 = $Stats1[$key]->selected_male+$Stats1[$key]->selected_female;
								// 		}
								// 		if($Stats1[$key]->joined_total > 0){
								// 			$total_joined1 = $Stats1[$key]->joined_total;
								// 		}else{$
								// 			$total_joined1 = $Stats1[$key]->joined_male+$Stats1[$key]->joined_female;
								// 		}
	
								// 	}
								// }
								?>
			@if($fac != $p->faculty->title)
				@if($i != 0)
					</div>
				@endif
			<?php /* ?>
			<div class="faculty-title fee-clr bor-head pt-2" data-toggle="collapse" data-target="#demo-<?= $i ?>"> <i class="more-less fa fa-plus"></i><h4>{{$p->faculty->title}}</h4></div>
			<?php */ ?>
			<?php
			$key = array_search($p->faculty->title, array_column($faculty, 'title'));
			$key1 = array_search($p->faculty->title, array_column($faculty1, 'title'));
			if($faculty[$key]->applied_total > 0){
				$f_total_applied = $faculty[$key]->applied_total;
			}else{
				$f_total_applied = $faculty[$key]->applied_male+$faculty[$key]->applied_female;
			}
			if($faculty[$key]->fee_confirmed_total > 0){
				$f_total_fee_confirmed = $faculty[$key]->fee_confirmed_total;
			}else{
				$f_total_fee_confirmed = $faculty[$key]->fee_confirmed_male+$faculty[$key]->fee_confirmed_female;
			}
			if($faculty[$key]->selected_total > 0){
				$f_total_selected = $faculty[$key]->selected_total;
			}else{
				$f_total_selected = $faculty[$key]->selected_male+$faculty[$key]->selected_female;
			}
			if($faculty[$key]->joined_total > 0){
				$f_total_joined = $faculty[$key]->joined_total;
			}else{
				$f_total_joined = $faculty[$key]->joined_male+$faculty[$key]->joined_female;
			}


			if($faculty1[$key1]->applied_total > 0){
				$f_total_applied1 = $faculty1[$key1]->applied_total;
			}else{
				$f_total_applied1 = $faculty1[$key1]->applied_male+$faculty1[$key1]->applied_female;
			}
			if($faculty1[$key1]->fee_confirmed_total > 0){
				$f_total_fee_confirmed1 = $faculty1[$key1]->fee_confirmed_total;
			}else{
				$f_total_fee_confirmed1 = $faculty1[$key1]->fee_confirmed_male+$faculty1[$key1]->fee_confirmed_female;
			}
			if($faculty1[$key1]->selected_total > 0){
				$f_total_selected1 = $faculty1[$key1]->selected_total;
			}else{
				$f_total_selected1 = $faculty1[$key1]->selected_male+$faculty1[$key1]->selected_female;
			}
			if($faculty1[$key1]->joined_total > 0){
				$f_total_joined1 = $faculty1[$key1]->joined_total;
			}else{
				$f_total_joined1 = $faculty1[$key1]->joined_male+$faculty1[$key1]->joined_female;
			}
			?>
			<table style="width:100%;table-layout: fixed;" class="program-stats-tbl table table-hover table-bordered table-stat">
				<tbody>

			<tr class="ac-pr cus-ttf">
					<td class="pr-wd"> <i data-toggle="collapse" data-target="#demo-<?= $i ?>" style="cursor: pointer;" class="more-less morr-lss fa fa-plus"></i> {{$p->faculty->title}} <span class="glyphicon glyphicon-stats my-fn"></span></td>
					<td class="cls-wd bg-bl">{{$f_total_applied}}</td>
					<td class="cls-wd bg-gr">{{$f_total_applied1}}</td>
					<td class="cls-wd bg-bl">{{$f_total_fee_confirmed}}</td>
					<td class="cls-wd bg-gr">{{$f_total_fee_confirmed1}}</td>
					<td class="cls-wd bg-bl">{{$f_total_selected}}</td>
					<td class="cls-wd bg-gr">{{$f_total_selected1}}</td>
					<td class="cls-wd bg-bl">{{$f_total_joined}}</td>
					<td class="cls-wd bg-gr">{{$f_total_joined1}}</td>

					<?php

					if(($f_total_applied1 - $f_total_applied) < 0){
						echo '<td class="cls-wd dwn-clr">'.($f_total_applied1 - $f_total_applied).'<span class="glyphicon glyphicon-arrow-down"></span></td>';
					}else{
						echo '<td class="cls-wd up-clr">'.($f_total_applied1 - $f_total_applied).'<span class="glyphicon glyphicon-arrow-up"></span></td>';
					}
					if(($f_total_fee_confirmed1 - $f_total_fee_confirmed) < 0){
						echo '<td class="cls-wd dwn-clr">'.($f_total_fee_confirmed1 - $f_total_fee_confirmed).'<span class="glyphicon glyphicon-arrow-down"></span></td>';
					}else{
						echo '<td class="cls-wd up-clr">'.($f_total_fee_confirmed1 - $f_total_fee_confirmed).'<span class="glyphicon glyphicon-arrow-up"></span></td>';
					}
			
					if(($f_total_selected1 - $f_total_selected) < 0){
						echo '<td class="cls-wd dwn-clr">'.($f_total_selected1 - $f_total_selected).'<span class="glyphicon glyphicon-arrow-down"></span></td>';
					}else{
						echo '<td class="cls-wd up-clr">'.($f_total_selected1 - $f_total_selected).'<span class="glyphicon glyphicon-arrow-up"></span></td>';
					}
			
					if(($f_total_joined1 - $f_total_joined) < 0){
						echo '<td class="cls-wd dwn-clr">'.($f_total_joined1 - $f_total_joined).'<span class="glyphicon glyphicon-arrow-down"></span></td>';
					}else{
						echo '<td class="cls-wd up-clr">'.($f_total_joined1 - $f_total_joined).'<span class="glyphicon glyphicon-arrow-up"></span></td>';
					}
			
					?>

				</tr>
				</tbody>
			</table>
			<div id="demo-<?= $i ?>" class="cls-faculty collapse">
			@endif
			@if($dept != $p->department->title)
			<div><h4 class="prog-dep">{{$p->department->title}}</h4></div>



			<?php
			$f_key = array_search($p->department->title, array_column($department, 'title'));
			$f_key1 = array_search($p->department->title, array_column($department1, 'title'));
			if($department[$f_key]->applied_total > 0){
				$d_total_applied = $department[$f_key]->applied_total;
			}else{
				$d_total_applied = $department[$f_key]->applied_male+$department[$f_key]->applied_female;
			}
			if($department[$f_key]->fee_confirmed_total > 0){
				$d_total_fee_confirmed = $department[$f_key]->fee_confirmed_total;
			}else{
				$d_total_fee_confirmed = $department[$f_key]->fee_confirmed_male+$department[$f_key]->fee_confirmed_female;
			}
			if($department[$f_key]->selected_total > 0){
				$d_total_selected = $department[$f_key]->selected_total;
			}else{
				$d_total_selected = $department[$f_key]->selected_male+$department[$f_key]->selected_female;
			}
			if($department[$f_key]->joined_total > 0){
				$d_total_joined = $department[$f_key]->joined_total;
			}else{
				$d_total_joined = $department[$f_key]->joined_male+$department[$f_key]->joined_female;
			}


			if($department1[$f_key1]->applied_total > 0){
				$d_total_applied1 = $department1[$f_key1]->applied_total;
			}else{
				$d_total_applied1 = $department1[$f_key1]->applied_male+$department1[$f_key1]->applied_female;
			}
			if($department1[$f_key1]->fee_confirmed_total > 0){
				$d_total_fee_confirmed1 = $department1[$f_key1]->fee_confirmed_total;
			}else{
				$d_total_fee_confirmed1 = $department1[$f_key1]->fee_confirmed_male+$department1[$f_key1]->fee_confirmed_female;
			}
			if($department1[$f_key1]->selected_total > 0){
				$d_total_selected1 = $department1[$f_key1]->selected_total;
			}else{
				$d_total_selected1 = $department1[$f_key1]->selected_male+$department1[$f_key1]->selected_female;
			}
			if($department1[$f_key1]->joined_total > 0){
				$d_total_joined1 = $department1[$f_key1]->joined_total;
			}else{
				$d_total_joined1 = $department1[$f_key1]->joined_male+$department1[$f_key1]->joined_female;
			}
			?>
<table style="width:100%;table-layout: fixed;" class="program-stats-tbl table table-hover table-bordered table-stat">
	<tbody>

<tr class="ac-pr cus-ttf">
		<td class="pr-wd">{{$p->department->title}}<span class="glyphicon glyphicon-stats my-fn"></span></td>
		<td class="cls-wd bg-bl">{{$d_total_applied}}</td>
		<td class="cls-wd bg-gr">{{$d_total_applied1}}</td>
		<td class="cls-wd bg-bl">{{$d_total_fee_confirmed}}</td>
		<td class="cls-wd bg-gr">{{$d_total_fee_confirmed1}}</td>
		<td class="cls-wd bg-bl">{{$d_total_selected}}</td>
		<td class="cls-wd bg-gr">{{$d_total_selected1}}</td>
		<td class="cls-wd bg-bl">{{$d_total_joined}}</td>
		<td class="cls-wd bg-gr">{{$d_total_joined1}}</td>

		<?php

		if(($d_total_applied1 - $d_total_applied) < 0){
			echo '<td class="cls-wd dwn-clr">'.($d_total_applied1 - $d_total_applied).'<span class="glyphicon glyphicon-arrow-down"></span></td>';
		}else{
			echo '<td class="cls-wd up-clr">'.($d_total_applied1 - $d_total_applied).'<span class="glyphicon glyphicon-arrow-up"></span></td>';
		}
		if(($d_total_fee_confirmed1 - $d_total_fee_confirmed) < 0){
			echo '<td class="cls-wd dwn-clr">'.($d_total_fee_confirmed1 - $d_total_fee_confirmed).'<span class="glyphicon glyphicon-arrow-down"></span></td>';
		}else{
			echo '<td class="cls-wd up-clr">'.($d_total_fee_confirmed1 - $d_total_fee_confirmed).'<span class="glyphicon glyphicon-arrow-up"></span></td>';
		}

		if(($d_total_selected1 - $d_total_selected) < 0){
			echo '<td class="cls-wd dwn-clr">'.($d_total_selected1 - $d_total_selected).'<span class="glyphicon glyphicon-arrow-down"></span></td>';
		}else{
			echo '<td class="cls-wd up-clr">'.($d_total_selected1 - $d_total_selected).'<span class="glyphicon glyphicon-arrow-up"></span></td>';
		}

		if(($d_total_joined1 - $d_total_joined) < 0){
			echo '<td class="cls-wd dwn-clr">'.($d_total_joined1 - $d_total_joined).'<span class="glyphicon glyphicon-arrow-down"></span></td>';
		}else{
			echo '<td class="cls-wd up-clr">'.($d_total_joined1 - $d_total_joined).'<span class="glyphicon glyphicon-arrow-up"></span></td>';
		}

		?>

		</tr>
	</tbody>
</table>



<div><h4 class="prog-dep">Programs</h4></div>
			
			@endif
			<table style="width:100%;table-layout: fixed;" class="program-stats-tbl table table-hover table-bordered table-stat">
				<tbody>

			<tr class="ac-pr cus-ttf">
					<td class="pr-wd">{{$title}}<span class="glyphicon glyphicon-stats my-fn"></span></td>
					<td class="cls-wd bg-bl">{{$total_applied ?? ''}}</td>
					<td class="cls-wd bg-gr">{{$total_applied1 ?? ''}}</td>
					<td class="cls-wd bg-bl">{{$total_fee_confirmed ?? ''}}</td>
					<td class="cls-wd bg-gr">{{$total_fee_confirmed1 ?? ''}}</td>
					<td class="cls-wd bg-bl">{{$total_selected ?? ''}}</td>
					<td class="cls-wd bg-gr">{{$total_selected1 ?? ''}}</td>
					<td class="cls-wd bg-bl">{{$total_joined ?? ''}}</td>
					<td class="cls-wd bg-gr">{{$total_joined1 ?? ''}}</td>

					<?php

					if(($total_applied1 - $total_applied) < 0){
						echo '<td class="cls-wd dwn-clr">'.($total_applied1 - $total_applied).'<span class="glyphicon glyphicon-arrow-down"></span></td>';
					}else{
						echo '<td class="cls-wd up-clr">'.($total_applied1 - $total_applied).'<span class="glyphicon glyphicon-arrow-up"></span></td>';
					}
					if(($total_fee_confirmed1 - $total_fee_confirmed) < 0){
						echo '<td class="cls-wd dwn-clr">'.($total_fee_confirmed1 - $total_fee_confirmed).'<span class="glyphicon glyphicon-arrow-down"></span></td>';
					}else{
						echo '<td class="cls-wd up-clr">'.($total_fee_confirmed1 - $total_fee_confirmed).'<span class="glyphicon glyphicon-arrow-up"></span></td>';
					}

					if(($total_selected1 - $total_selected) < 0){
						echo '<td class="cls-wd dwn-clr">'.($total_selected1 - $total_selected).'<span class="glyphicon glyphicon-arrow-down"></span></td>';
					}else{
						echo '<td class="cls-wd up-clr">'.($total_selected1 - $total_selected).'<span class="glyphicon glyphicon-arrow-up"></span></td>';
					}

					if(($total_joined1 - $total_joined) < 0){
						echo '<td class="cls-wd dwn-clr">'.($total_joined1 - $total_joined).'<span class="glyphicon glyphicon-arrow-down"></span></td>';
					}else{
						echo '<td class="cls-wd up-clr">'.($total_joined1 - $total_joined).'<span class="glyphicon glyphicon-arrow-up"></span></td>';
					}

					?>

					</tr>
				</tbody>
			</table>

			@php
			$fac = $p->faculty->title;
			$dept = $p->department->title;
			$i++;
			@endphp


	</tbody>
	</table>

							@endforeach

						@endif
						<?php } ?>

				</div> <!-- Ending row -->
			</div>
		
        	</div> <!-- Ending container --> 
        </div> <!-- Ending page-content-wrapper --> 
	</div> <!-- Ending page-content-row --> 


	<!-- Modal -->
<div class="modal fade" id="statsModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
	  <div class="modal-content">
		<div class="modal-header">
		  <h5 class="modal-title flt-lft" id="exampleModalLabel"></h5>
		  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			<span aria-hidden="true">&times;</span>
		  </button>
		</div>
		<div class="modal-body">
			<canvas id="myChart" width="400" height="200"></canvas>
		</div>
		<div class="modal-footer">
		  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
		</div>
	  </div>
	</div>
  </div>


{{-- <script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"></script>  --}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"></script> 
<script>
		// $(document).ready(function() {
		// 	$('#example').DataTable( {
		// 	"paging":   false,
		// 	"bInfo" : false,
		// 	"autoWidth": true
		// 	} );
		// });
		$(function(){
            $('.morr-lss').on('click', function(){
                $this = $(this);
                // console.log ( $this.find('i'));
                if ( $this.hasClass('fa-minus') ) {
                    $this.removeClass('fa-minus').addClass('fa-plus');
                }else{
                    $this.removeClass('fa-plus').addClass('fa-minus');
                }
            })
		});
		

		$( ".my-fn" ).click(function() {
			$('#myChart').remove(); // this is my <canvas> element
			 $('.modal-body').append('<canvas id="myChart" width="400" height="200"></canvas>');
			// console.log(myBarChart);
			// myLineChart.reset();
			// if (myBarChart) { console.log('sdsdsd'); myBarChart.destroy(); myBarChart.clear();$("canvas#myChart").remove();}
		var s_fr = '<?= $s_fr; ?>';
		var s_to = '<?= $s_to; ?>';
		var canvas = document.getElementById('myChart');
		var applied_1 = '';
		var applied_2 = '';
		var fee_confirmed_1 = '';
		var fee_confirmed_2 = '';
		var selected_1 = '';
		var selected_2 = '';
		var joined_1 = '';
		var joined_2 = '';
		var prog = '';
		prog = $(this).parent().text();
		$('.modal-title').text(prog+ ' Data Trend');
 		applied_1 = $(this).parent().next().text();
		applied_2 = $(this).parent().next().next().text();
		fee_confirmed_1 = $(this).parent().next().next().next().text();
		fee_confirmed_2 = $(this).parent().next().next().next().next().text();
		selected_1 = $(this).parent().next().next().next().next().next().text();
		selected_2 = $(this).parent().next().next().next().next().next().next().text();
		joined_1 = $(this).parent().next().next().next().next().next().next().next().text();
		joined_2 = $(this).parent().next().next().next().next().next().next().next().next().text();

		var data = {
			labels: ["Applied "+s_fr, "Applied "+s_to, "Fee Confirmed "+s_fr, "Fee Confirmed "+s_to, "Selected "+s_fr, "Selected "+s_to, "Joined "+s_fr, "Joined "+s_to],
			datasets: [
				{
					// label: "My First dataset",
					backgroundColor:  [
						"rgba(75,192,192, 0.4)",
						"rgba(1, 223, 111, 0.4)",
						"rgba(75,192,192, 0.4)",
						"rgba(1, 223, 111, 0.4)",
						"rgba(75,192,192, 0.4)",
						"rgba(75,192,192, 0.4)",
						"rgba(1, 223, 111, 0.4)",
						"rgba(75,192,192, 0.4)"
					],
					borderColor:  [
						"rgba(75,192,192, 0.4)",
						"rgba(1, 223, 111, 0.4)",
						"rgba(75,192,192, 0.4)",
						"rgba(1, 223, 111, 0.4)",
						"rgba(75,192,192, 0.4)",
						"rgba(75,192,192, 0.4)",
						"rgba(1, 223, 111, 0.4)",
						"rgba(75,192,192, 0.4)"
					],
					borderWidth: 2,
					hoverBackgroundColor: [
						"rgba(75,192,192, 0.4)",
						"rgba(1, 223, 111, 0.4)",
						"rgba(75,192,192, 0.4)",
						"rgba(1, 223, 111, 0.4)",
						"rgba(75,192,192, 0.4)",
						"rgba(75,192,192, 0.4)",
						"rgba(1, 223, 111, 0.4)",
						"rgba(75,192,192, 0.4)"
					],
					hoverBorderColor: [
						"rgba(75,192,192, 0.4)",
						"rgba(1, 223, 111, 0.4)",
						"rgba(75,192,192, 0.4)",
						"rgba(1, 223, 111, 0.4)",
						"rgba(75,192,192, 0.4)",
						"rgba(75,192,192, 0.4)",
						"rgba(1, 223, 111, 0.4)",
						"rgba(75,192,192, 0.4)"
					],
					data: [applied_1, applied_2, fee_confirmed_1, fee_confirmed_2, selected_1, selected_2, joined_1, joined_2],
				}
			]
		};
		var option = {
			scales: {
				yAxes:[{
						stacked:true,
					gridLines: {
						display:true,
					color:"rgba(255,99,132,0.2)"
					}
				}],
				xAxes:[{
						gridLines: {
						display:false
					},
					categoryPercentage: 1.0,
            		barPercentage: 1.0
				}]
			},
		legend: {
			display: false
			}
		};

		var myBarChart = Chart.Bar(canvas,{
			data:data,
		options:option
		});
		$('#statsModal').modal('show');
});



</script>
@stop
