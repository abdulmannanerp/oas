@extends('layouts.app')

@section('content')
<section class="page-header row">
	<h2> {{ $pageTitle }} <small> {{ $pageNote }} </small></h2>
	<ol class="breadcrumb">
		<li><a href="{{ url('') }}"> Dashboard </a></li>
		<li><a href="{{ url($pageModule) }}"> {{ $pageTitle }} </a></li>
		<li class="active"> Form  </li>		
	</ol>
</section>
<div class="page-content row">
	<div class="page-content-wrapper no-margin">

	{!! Form::open(array('url'=>'permissions?return='.$return, 'class'=>'form-horizontal validated','files' => true )) !!}
	<div class="sbox">
		<div class="sbox-title clearfix">
			<div class="sbox-tools " >
				<a href="{{ url($pageModule.'?return='.$return) }}" class="tips btn btn-sm "  title="{{ __('core.btn_back') }}" ><i class="fa  fa-times"></i></a> 
			</div>
			<div class="sbox-tools pull-left" >
				<button name="apply" class="tips btn btn-sm btn-apply  "  title="{{ __('core.btn_back') }}" ><i class="fa  fa-check"></i> {{ __('core.sb_apply') }} </button>
				<button name="save" class="tips btn btn-sm btn-save"  title="{{ __('core.btn_back') }}" ><i class="fa  fa-paste"></i> {{ __('core.sb_save') }} </button> 
			</div>
		</div>	
		<div class="sbox-content clearfix">
	<ul class="parsley-error-list">
		@foreach($errors->all() as $error)
			<li>{{ $error }}</li>
		@endforeach
	</ul>		
<div class="col-md-12">
						<fieldset><legend> Permissions</legend>
				{!! Form::hidden('id', $row['id']) !!}					
									  <div class="form-group  " >
										<label for="User" class=" control-label col-md-4 text-left"> User </label>
										<div class="col-md-6">
										  <select name='fkUserId' rows='5' id='fkUserId' class='select2 '   ></select> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Faculty" class=" control-label col-md-4 text-left"> Faculty </label>
										<div class="col-md-6">
										  <select name='fkFacultyId[]' multiple rows='5' id='fkFacultyId' class='select2 '   ></select> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Department" class=" control-label col-md-4 text-left"> Department </label>
										<div class="col-md-6">
										  <select name='fkDepartmentId[]' multiple rows='5' id='fkDepartmentId' class='select2 '   ></select> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Gender" class=" control-label col-md-4 text-left"> Gender </label>
										<div class="col-md-6">
										  <select name='fkGenderId' rows='5' id='fkGenderId' class='select2 '   ></select> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Nationality" class=" control-label col-md-4 text-left"> Nationality </label>
										<div class="col-md-6">
										  <select name='fkNationality[]' multiple rows='5' id='fkNationality' class='select2 '   ></select> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> </fieldset>
			</div>
			
			

		</div>
	</div>
	<input type="hidden" name="action_task" value="save" />
	{!! Form::close() !!}
	</div>
</div>		
	
		 
   <script type="text/javascript">
	$(document).ready(function() { 
		
		
		
		$("#fkUserId").jCombo("{!! url('permissions/comboselect?filter=tb_users:id:username') !!}",
		{  selected_value : '{{ $row["fkUserId"] }}' });
		
		$("#fkFacultyId").jCombo("{!! url('permissions/comboselect?filter=tbl_oas_faculty:pkFacId:title') !!}",
		{  selected_value : '{{ $row["fkFacultyId"] }}' });
		
		$("#fkDepartmentId").jCombo("{!! url('permissions/comboselect?filter=tbl_oas_department:pkDeptId:title') !!}",
		{  selected_value : '{{ $row["fkDepartmentId"] }}' });
		
		$("#fkGenderId").jCombo("{!! url('permissions/comboselect?filter=tbl_oas_gender:pkGenderId:gender') !!}",
		{  selected_value : '{{ $row["fkGenderId"] }}' });
		
		$("#fkNationality").jCombo("{!! url('permissions/comboselect?filter=tbl_oas_nationality:id:nationality') !!}",
		{  selected_value : '{{ $row["fkNationality"] }}' });
		 		 

		$('.removeMultiFiles').on('click',function(){
			var removeUrl = '{{ url("permissions/removefiles?file=")}}'+$(this).attr('url');
			$(this).parent().remove();
			$.get(removeUrl,function(response){});
			$(this).parent('div').empty();	
			return false;
		});		
		
	});
	</script>		 
@stop