

		 {!! Form::open(array('url'=>'permissions/savepublic', 'class'=>'form-horizontal','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}

	@if(Session::has('messagetext'))
	  
		   {!! Session::get('messagetext') !!}
	   
	@endif
	<ul class="parsley-error-list">
		@foreach($errors->all() as $error)
			<li>{{ $error }}</li>
		@endforeach
	</ul>		


<div class="col-md-12">
						<fieldset><legend> Permissions</legend>
				{!! Form::hidden('id', $row['id']) !!}					
									  <div class="form-group  " >
										<label for="User" class=" control-label col-md-4 text-left"> User </label>
										<div class="col-md-6">
										  <select name='fkUserId' rows='5' id='fkUserId' class='select2 '   ></select> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Faculty" class=" control-label col-md-4 text-left"> Faculty </label>
										<div class="col-md-6">
										  <select name='fkFacultyId[]' multiple rows='5' id='fkFacultyId' class='select2 '   ></select> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Department" class=" control-label col-md-4 text-left"> Department </label>
										<div class="col-md-6">
										  <select name='fkDepartmentId[]' multiple rows='5' id='fkDepartmentId' class='select2 '   ></select> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Gender" class=" control-label col-md-4 text-left"> Gender </label>
										<div class="col-md-6">
										  <select name='fkGenderId' rows='5' id='fkGenderId' class='select2 '   ></select> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Nationality" class=" control-label col-md-4 text-left"> Nationality </label>
										<div class="col-md-6">
										  <select name='fkNationality[]' multiple rows='5' id='fkNationality' class='select2 '   ></select> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> </fieldset>
			</div>
			
			

			<div style="clear:both"></div>	
				
					
				  <div class="form-group">
					<label class="col-sm-4 text-right">&nbsp;</label>
					<div class="col-sm-8">	
					<button type="submit" name="apply" class="btn btn-info btn-sm" ><i class="fa  fa-check-circle"></i> {{ Lang::get('core.sb_apply') }}</button>
					<button type="submit" name="submit" class="btn btn-primary btn-sm" ><i class="fa  fa-save "></i> {{ Lang::get('core.sb_save') }}</button>
				  </div>	  
			
		</div> 
		 <input type="hidden" name="action_task" value="public" />
		 {!! Form::close() !!}
		 
   <script type="text/javascript">
	$(document).ready(function() { 
		
		
		$("#fkUserId").jCombo("{!! url('permissions/comboselect?filter=tb_users:id:username') !!}",
		{  selected_value : '{{ $row["fkUserId"] }}' });
		
		$("#fkFacultyId").jCombo("{!! url('permissions/comboselect?filter=tbl_oas_faculty:pkFacId:title') !!}",
		{  selected_value : '{{ $row["fkFacultyId"] }}' });
		
		$("#fkDepartmentId").jCombo("{!! url('permissions/comboselect?filter=tbl_oas_department:pkDeptId:title') !!}",
		{  selected_value : '{{ $row["fkDepartmentId"] }}' });
		
		$("#fkGenderId").jCombo("{!! url('permissions/comboselect?filter=tbl_oas_gender:pkGenderId:gender') !!}",
		{  selected_value : '{{ $row["fkGenderId"] }}' });
		
		$("#fkNationality").jCombo("{!! url('permissions/comboselect?filter=tbl_oas_nationality:id:nationality') !!}",
		{  selected_value : '{{ $row["fkNationality"] }}' });
		 

		$('.removeCurrentFiles').on('click',function(){
			var removeUrl = $(this).attr('href');
			$.get(removeUrl,function(response){});
			$(this).parent('div').empty();	
			return false;
		});		
		
	});
	</script>		 
