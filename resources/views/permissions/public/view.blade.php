<div class="m-t" style="padding-top:25px;">	
    <div class="row m-b-lg animated fadeInDown delayp1 text-center">
        <h3> {{ $pageTitle }} <small> {{ $pageNote }} </small></h3>
        <hr />       
    </div>
</div>
<div class="m-t">
	<div class="table-responsive" > 	

		<table class="table table-striped table-bordered" >
			<tbody>	
		
			
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Id', (isset($fields['id']['language'])? $fields['id']['language'] : array())) }}</td>
						<td>{{ $row->id}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('User', (isset($fields['fkUserId']['language'])? $fields['fkUserId']['language'] : array())) }}</td>
						<td>{{ SiteHelpers::formatLookUp($row->fkUserId,'fkUserId','1:tb_users:id:username') }} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Faculty', (isset($fields['fkFacultyId']['language'])? $fields['fkFacultyId']['language'] : array())) }}</td>
						<td>{{ $row->fkFacultyId}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Department', (isset($fields['fkDepartmentId']['language'])? $fields['fkDepartmentId']['language'] : array())) }}</td>
						<td>{{ $row->fkDepartmentId}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Program', (isset($fields['fkProgramId']['language'])? $fields['fkProgramId']['language'] : array())) }}</td>
						<td>{{ $row->fkProgramId}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Gender', (isset($fields['fkGenderId']['language'])? $fields['fkGenderId']['language'] : array())) }}</td>
						<td>{{ SiteHelpers::formatLookUp($row->fkGenderId,'fkGenderId','1:tbl_oas_gender:pkGenderId:gender') }} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Nationality', (isset($fields['fkNationality']['language'])? $fields['fkNationality']['language'] : array())) }}</td>
						<td>{{ $row->fkNationality}} </td>
						
					</tr>
						
					<tr>
						<td width='30%' class='label-view text-right'></td>
						<td> <a href="javascript:history.go(-1)" class="btn btn-primary"> Back To Grid <a> </td>
						
					</tr>					
				
			</tbody>	
		</table>   

	 
	
	</div>
</div>	