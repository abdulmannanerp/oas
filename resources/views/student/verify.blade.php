@extends('layouts.app')


@section('content')
    <div class="page-content row">
        <div class="page-content-wrapper m-t">
            <form action="{{route('postVerify')}}" method="POST" id="application-numbers-form">
                {{ csrf_field() }}
                <div class="form-group">
                        <label for="">Enter Roll Numbers</label>
                        <textarea name="application_ids" id="application-ids-textbox" class="form-control" placeholder="171005,758000,875520" required></textarea>
                </div> <!-- Ending Form Group -->
                <div class="form-group">
                    <input type="submit" class="btn btn-primary" value="List Records" name="list_records">
                    {{--<input type="submit" class="btn btn-warning" value="Quick Verify" name="quick_verify">--}}
                </div>
            </form>

            <form action="{{route('postVerify')}}" method="POST" enctype="multipart/form-data" id="upload-csv-form">
                <div class="form-group">
                    {{ csrf_field() }}
                    <label for="">Upload CSV</label>
                    <input type="file" name="csv" class="btn btn-default" accept=".csv" required>
                </div>
                <div class="form-group">
                    <input type="submit" class="btn btn-primary" value="Uplaod & List" name="upload_and_list_csv">
                    {{--<input type="submit" class="btn btn-warning" value="Uplaod & Verify" name="upload_csv">--}}
                </div>
            </form>

           
        </div>
    </div>
@stop
