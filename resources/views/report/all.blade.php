@extends('layouts.app')


@section('content')
    {{--<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">--}}
    <?php
        $male = 3;
        $female = 2;
        $all = 1;
    ?>
    <div class="page-content row">
        <div class="page-content-wrapper m-t">
            <h3>Overall Report</h3> 
            @if (isset ( $application ) )
            @if ( $currentUserGenderPermission == 1 ) 
            <div class="table-responsive" style="padding-bottom: 30px;">
                <h4>Overall Stats</h4>
                <table class="table table-hover table-bordered app-search-grid" id="ManualVerifyTable">
                    <thead class="table-head">
                    <tr>
                        <th>Total Applied</th>
                        <th>Fee Pending</th>
                        <th>Fee Confirmed</th>
                        <th>Documents Verified</th>
                        <th>Rejected</th>
                    </tr>
                    </thead>

                    <tbody>
                    <tr>
                        <td>
                            {{ $allApplications }}
                        </td>
                        <td>
                            {{ $feePending = $application->getStats('=', '3')->count() }}
                        </td>
                        <td>
                            {{ $feeConfirmed = $application->getStats('>=', '4')->count() }}
                        </td>
                        <td>
                            {{ $documentsVerified = $application->getStats('=', '5')->count() }}
                        </td>
                        <td>
                            {{ $rejected = $application->getStats('=', '6')->count() }}
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
            @endif

            <!-- Ending Overall Report -->
            <div class="table-responsive">
                <h4>Gender-wise Overall Stats</h4>
                <table class="table table-hover table-bordered app-search-grid" id="ManualVerifyTable">
                    <thead class="table-head">
                    <tr>
                        <th colspan="2">Total Applied</th>
                        <th colspan="2">Fee Pending</th>
                        <th colspan="2">Fee Confirmed</th>
                        <th colspan="2">Documents Verified</th>
                        <th colspan="2">Rejected</th>
                    </tr>
                    <tr>
                        <th>Male</th>
                        <th>Female</th>

                        <th>Male</th>
                        <th>Female</th>

                        <th>Male</th>
                        <th>Female</th>

                        <th>Male</th>
                        <th>Female</th>

                        <th>Male</th>
                        <th>Female</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>
                            @if ( $currentUserGenderPermission == $male || $currentUserGenderPermission == $all )
                                {{ $allApplicatoinWithMaleApplicants }}
                            @else
                                {{'N/A'}}
                            @endif
                        </td>
                        <td>
                            @if ( $currentUserGenderPermission == $female || $currentUserGenderPermission == $all)
                                {{ $allApplicatoinWithFemaleApplicants }}
                            @else
                                {{'N/A'}}
                            @endif
                        </td>
                        <td>
                            @if ( $currentUserGenderPermission == $male || $currentUserGenderPermission == $all )
                                <?php $feePendingMale = $application->getOverAllStatusByGender('=', '3', '3')->count(); ?>
                                {{ is_object($feePendingMale) ? '0' : $feePendingMale }}
                            @else
                                {{'N/A'}}
                            @endif
                        </td>
                        <td>
                            @if ( $currentUserGenderPermission == $female || $currentUserGenderPermission == $all )
                                <?php $feePendingFemale = $application->getOverAllStatusByGender('=', '3', '2')->count(); ?>
                                {{ is_object($feePendingFemale) ? '0' : $feePendingFemale }}
                            @else
                                {{ 'N/A' }}
                            @endif
                        </td>
                        <td>
                            @if ( $currentUserGenderPermission == $male || $currentUserGenderPermission == $all )
                                <?php $feeConfirmedMale = $application->getOverAllStatusByGender('>=', '4', '3')->count();; ?>
                                {{ is_object($feeConfirmedMale) ? '0' : $feeConfirmedMale  }}
                            @else
                                {{ 'N/A' }}
                            @endif
                        </td>
                        <td>
                            @if ( $currentUserGenderPermission == $female || $currentUserGenderPermission == $all )
                                <?php $feeConfirmedFemale = $application->getOverAllStatusByGender('>=', '4', '2')->count();; ?>
                                {{ is_object($feeConfirmedFemale) ? '0' : $feeConfirmedFemale  }}
                            @else
                                {{'N/A'}}
                            @endif
                        </td>
                        <td>
                            @if ( $currentUserGenderPermission == $male || $currentUserGenderPermission == $all )
                                <?php $documentsVerifiedMale = $application->getOverAllStatusByGender('=', '5', '3')->count();; ?>
                                {{ is_object($documentsVerifiedMale) ? '0' : $documentsVerifiedMale  }}
                            @else
                                {{'N/A'}}
                            @endif
                        </td>
                        <td>
                            @if ( $currentUserGenderPermission == $female || $currentUserGenderPermission == $all )
                                <?php $documentVerifiedFemale = $application->getOverAllStatusByGender('=', '5', '2')->count();; ?>
                                {{ is_object($documentVerifiedFemale) ? '0' : $documentVerifiedFemale  }}
                            @else
                                {{'N/A'}}
                            @endif
                        </td>
                        <td>
                            @if ( $currentUserGenderPermission == $male || $currentUserGenderPermission == $all )
                                <?php $rejectedMale = $application->getOverAllStatusByGender('=', '6', '3')->count();; ?>
                                {{ is_object($rejectedMale) ? '0' : $rejectedMale  }}
                            @else
                                {{'N/A'}}
                            @endif
                        </td>
                        <td>
                            @if ( $currentUserGenderPermission == $female || $currentUserGenderPermission == $all ) 
                                <?php $rejectedFemale = $application->getOverAllStatusByGender('=', '6', '2')->count();; ?>
                                {{ is_object($rejectedFemale) ? '0' : $rejectedFemale  }}
                            @else
                                {{'N/A'}}
                            @endif
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
            @else
                <p>No Record(s) Found.</p>
            @endif
        </div>
    </div>
@stop
