@extends('layouts.app')
@section('content')
    <div class="page-content row">
        <div class="page-content-wrapper m-t">
            <div class="row">
                <div class="pull-right">
                    <div class="form-group">
                        <form name="semesterSelectorForm" id="semesterSelectorForm"
                            action="{{ route('getReportFromSpecificSemester', ['reportName' => 'topLevelReport']) }}"
                            method="post">
                            <select class="form-control" name="semesterSelector" id="semesterSelector" disabled>
                                <option> --Select Semester-- </option>
                                @foreach ($semesters as $semester)
                                    <option value="{{ $semester->pkSemesterId }}"
                                        {{ $semester->pkSemesterId == Cache::get(auth()->id() . '-active_semester') ? 'selected' : '' }}>
                                        {{ $semester->title }}
                                    </option>
                                @endforeach
                            </select>
                        </form>
                    </div>
                </div>
            </div>
            <div class="table-responsive" style="padding-bottom: 30px;">
                <table id="shashky" class="display nowrap" style="width:100%">
                    <thead class="tbl-bg-clr">
                        <tr>
                            <th></th>
                            <th>Total Applied Male</th>
                            <th>Total Applied Female</th>

                            <th>Fee Pending Male</th>
                            <th>Fee Pending Female</th>

                            <th>Fee Confirmed Male</th>
                            <th>Fee Confirmed Female</th>

                            <th>Fee Confirmed Male Origional</th>
                            <th>Fee Confirmed Female Origional</th>

                            <th>Documents Verified Male</th>
                            <th>Documents Verified Female</th>

                            <th>Rejected Male</th>
                            <th>Rejected Female</th>
                        </tr>
                    </thead>
                    <tbody>
                        {{-- /** Starting Faculty */ --}}
                        @foreach ($faculties as $faculty)
                            <?php
                            if (session('gid') == 9) {
                                if ($faculty->pkFacId != $allowedFacultyForFacultyUser) {
                                    continue;
                                }
                            }
                            ?>
                            <tr>
                                <td class="fac-clr">Faculty of: {{ $faculty->title }}</td>
                                <td> {{ $faculty->applications()->GetOverAllStatusByGender('>=', '1', '3')->count() }}
                                </td>
                                <td> {{ $faculty->applications()->GetOverAllStatusByGender('>=', '1', '2')->count() }}
                                </td>

                                <td> {{ $faculty->applications()->GetOverAllStatusByGender('=', '3', '3')->count() }} </td>
                                <td> {{ $faculty->applications()->GetOverAllStatusByGender('=', '3', '2')->count() }} </td>

                                <td> {{ $faculty->applications()->GetOverAllStatusByGender('>=', '4', '3')->count() }}
                                </td>
                                <td> {{ $faculty->applications()->GetOverAllStatusByGender('>=', '4', '2')->count() }}
                                </td>

                                <td></td>
                                <td></td>

                                <td> {{ $faculty->applications()->GetOverAllStatusByGender('=', '5', '3')->count() }}
                                </td>
                                <td> {{ $faculty->applications()->GetOverAllStatusByGender('=', '5', '2')->count() }}
                                </td>

                                <td> {{ $faculty->applications()->GetOverAllStatusByGender('=', '6', '3')->count() }}
                                </td>
                                <td> {{ $faculty->applications()->GetOverAllStatusByGender('=', '6', '2')->count() }}
                                </td>
                            </tr>
                            @foreach ($faculty->departments as $department)
                                @if ($department->status == 1)
                                    <tr>
                                        <td class="dep-clr">{{ $department->title }}</td>
                                        <td> {{ $department->applications()->GetOverAllStatusByGender('>=', '1', '3')->count() }}
                                        </td>
                                        <td> {{ $department->applications()->GetOverAllStatusByGender('>=', '1', '2')->count() }}
                                        </td>

                                        <td> {{ $department->applications()->GetOverAllStatusByGender('=', '3', '3')->count() }}
                                        </td>
                                        <td> {{ $department->applications()->GetOverAllStatusByGender('=', '3', '2')->count() }}
                                        </td>

                                        <td> {{ $department->applications()->GetOverAllStatusByGender('>=', '4', '3')->count() }}
                                        </td>
                                        <td> {{ $department->applications()->GetOverAllStatusByGender('>=', '4', '2')->count() }}
                                        </td>

                                        <td></td>
                                        <td></td>

                                        <td> {{ $department->applications()->GetOverAllStatusByGender('=', '5', '3')->count() }}
                                        </td>
                                        <td> {{ $department->applications()->GetOverAllStatusByGender('=', '5', '2')->count() }}
                                        </td>

                                        <td> {{ $department->applications()->GetOverAllStatusByGender('=', '6', '3')->count() }}
                                        </td>
                                        <td> {{ $department->applications()->GetOverAllStatusByGender('=', '6', '2')->count() }}
                                        </td>
                                    </tr>
                                @endif

                                @foreach ($department->programme as $program)
                                    @if ($program->status == 1)
                                        <tr>
                                            <td class="pro-clr">Program: {{ $program->title }}</td>
                                            <td> {{ $program->applications()->GetOverAllStatusByGender('>=', '1', '3')->count() }}
                                            </td>
                                            <td> {{ $program->applications()->GetOverAllStatusByGender('>=', '1', '2')->count() }}
                                            </td>

                                            <td> {{ $program->applications()->GetOverAllStatusByGender('=', '3', '3')->count() }}
                                            </td>
                                            <td> {{ $program->applications()->GetOverAllStatusByGender('=', '3', '2')->count() }}
                                            </td>

                                            <td> {{ $program->applications()->GetOverAllStatusByGender('>=', '4', '3')->count() }}
                                            </td>
                                            <td> {{ $program->applications()->GetOverAllStatusByGender('>=', '4', '2')->count() }}
                                            </td>



                                            <td> {{ $program->applications()->GetOverAllStatusByGenderOrigional('>=', '4', '3')->count() }}
                                            </td>
                                            <td> {{ $program->applications()->GetOverAllStatusByGenderOrigional('>=', '4', '2')->count() }}
                                            </td>




                                            <td> {{ $program->applications()->GetOverAllStatusByGender('=', '5', '3')->count() }}
                                            </td>
                                            <td> {{ $program->applications()->GetOverAllStatusByGender('=', '5', '2')->count() }}
                                            </td>

                                            <td> {{ $program->applications()->GetOverAllStatusByGender('=', '6', '3')->count() }}
                                            </td>
                                            <td> {{ $program->applications()->GetOverAllStatusByGender('=', '6', '2')->count() }}
                                            </td>
                                        </tr>
                                    @endif
                                @endforeach
                            @endforeach
                        @endforeach
                        {{-- /** Ending Faculty */ --}}
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <link href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/buttons/1.6.4/css/buttons.dataTables.min.css" rel="stylesheet">

    <script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.4/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.4/js/buttons.flash.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.4/js/buttons.html5.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.4/js/buttons.print.min.js"></script>

    <script>
        $(document).ready(function() {
            $('#shashky').DataTable({
                "pageLength": 1000,
                "aaSorting": [],
                "order": [],
                dom: 'Bfrtip',
                "columnDefs": [{
                    "searchable": false,
                    "orderable": false,
                    "targets": 0
                }],
                buttons: [
                    'copy', 'csv', 'excel' //, 'pdf', 'print'
                ]
            });
        });
    </script>
@stop
