@extends('layouts.report')
@section('report-content')

<div class="table-responsive">
    {{-- Program Title = $fileName --}}
    <h4>{{$fileName}}</h4>
    
    <table class="table table-hover table-bordered app-search-grid">
        <thead class="table-head">
        <tr>
            {{-- Hiding columns from faculty/department user
            session('gid') == 9 => Faculty User --}}  
            
            {{-- @if(session('gid') != '9' || session('eid') == 'dean.fet@iiu.edu.pk') --}}
                <th colspan="2">Form Issued</th>
                <th colspan="2">Fee Pending</th>
            {{-- @endif --}}
            <th colspan="2">Fee Verified</th>
            <th colspan="2">Approved</th>
            {{-- @if(session('gid') != '9' || session('eid') == 'dean.fet@iiu.edu.pk') --}}
                <th colspan="2">Rejected</th>
            {{-- @endif --}}
        </tr>
        <tr>
            {{-- @if(session('gid') != '9' || session('eid') == 'dean.fet@iiu.edu.pk') --}}
                <th>Male</th>
                <th>Female</th>

                <th>Male</th>
                <th>Female</th>
            {{-- @endif --}}
            <th>Male</th>
            <th>Female</th>

            <th>Male</th>
            <th>Female</th>

            {{-- @if(session('gid') != '9' || session('eid') == 'dean.fet@iiu.edu.pk') --}}
                <th>Male</th>
                <th>Female</th>
            {{-- @endif --}}
        </tr>
        </thead>
        <tbody>
            <tr>
                {{-- @if(session('gid') != '9' || session('eid') == 'dean.fet@iiu.edu.pk') --}}
                <td>
                    @if ( $currentUserGenderPermission == $bothMaleAndFemale || $currentUserGenderPermission == $male )
                    @if ( $programId != 115 ) 
                        {{ $programStats['maleCount'] }} 
                        
                        <form class="reports-form" action={{route('postCreateCsvForPrograms')}} method="POST">
                            <input type="hidden" name="programId" value={{$programId}}>
                            <input type="hidden" name="operator" value={{$nationalityOperator}}>
                            <input type="hidden" name="nationality" value={{ $nationality }} >
                            <input type="hidden" name="semester" value={{$selectedSemester}}>
                            <input type="hidden" name="helper" value={{ 'all-male-applicants' }} >
                            <input type="hidden" name="fileName" value={{ $fileName.' - All Male Applicants ' }} >
                            <button type="submit">
                                <i class="icon-cloud-download2"></i>
                            </button>
                        </form>
                    @endif
                    @else
                        {{'N/A'}}
                    @endif
                </td>
                <td>
                    @if ( $currentUserGenderPermission == $bothMaleAndFemale || $currentUserGenderPermission == $female )
                        {{ $programStats['femaleCount'] }} 
                        
                        <form class="reports-form" action={{route('postCreateCsvForPrograms')}} method="POST">
                            <input type="hidden" name="programId" value={{$programId}}>
                            <input type="hidden" name="operator" value={{$nationalityOperator}}>
                            <input type="hidden" name="nationality" value={{ $nationality }} >
                            <input type="hidden" name="semester" value={{$selectedSemester}}>
                            <input type="hidden" name="helper" value={{ 'all-female-applicants' }} >
                            <input type="hidden" name="fileName" value={{ $fileName.' - All Female Applicants ' }} >
                            <button type="submit">
                                <i class="icon-cloud-download2"></i>
                            </button>
                        </form>
                    @else
                        {{'N/A'}}
                    @endif
                </td>
                
                <td>
                    @if ( $currentUserGenderPermission == $bothMaleAndFemale || $currentUserGenderPermission == $male )
                        {{ $programStats['feePendingMale'] }}

                        <form class="reports-form" action={{route('postCreateCsvForProgramsGeneral')}} method="POST">
                            <input type="hidden" name="programId" value={{$programId}}>
                            <input type="hidden" name="operator" value={{ '=' }}>
                            <input type="hidden" name="status" value={{ '3' }}>
                            <input type="hidden" name="gender" value={{ '3' }}>
                            <input type="hidden" name="nationalityOperator" value={{ $nationalityOperator }} >
                            <input type="hidden" name="nationality" value={{ $nationality }} >
                            <input type="hidden" name="semester" value={{$selectedSemester}}>
                            <input type="hidden" name="fileName" value={{ $fileName.' - Fee Pending Male ' }} >
                            <button type="submit">
                                <i class="icon-cloud-download2"></i>
                            </button>
                        </form>
                    @else
                    {{'N/A'}}
                    @endif
                </td>

                <td>
                    @if ( $currentUserGenderPermission == $bothMaleAndFemale || $currentUserGenderPermission == $female )
                    {{ $programStats['feePendingFemale'] }}

                        <form class="reports-form" action={{route('postCreateCsvForProgramsGeneral')}} method="POST">
                            <input type="hidden" name="programId" value={{$programId}}>
                            <input type="hidden" name="operator" value={{ '=' }}>
                            <input type="hidden" name="status" value={{ '3' }}>
                            <input type="hidden" name="gender" value={{ '2' }}>
                            <input type="hidden" name="nationalityOperator" value={{ $nationalityOperator }} >
                            <input type="hidden" name="nationality" value={{ $nationality }} >
                            <input type="hidden" name="semester" value={{$selectedSemester}}>
                            <input type="hidden" name="fileName" value={{ $fileName.' - Fee Pending Female ' }} >
                            <button type="submit">
                                <i class="icon-cloud-download2"></i>
                            </button>
                        </form>
                    @else
                    {{'N/A'}}
                    @endif
                </td>
             {{--   @endif   Ednign @if(session('gid') != '9') --}}
                <td>
                    @if ( $currentUserGenderPermission == $bothMaleAndFemale || $currentUserGenderPermission == $male )
                    {{ $programStats['feeVerifiedMale'] }}

                        <form class="reports-form" action={{route('postCreateCsvForProgramsGeneral')}} method="POST">
                            <input type="hidden" name="programId" value={{$programId}}>
                            <input type="hidden" name="operator" value={{ '>=' }}>
                            <input type="hidden" name="status" value={{ '4' }}>
                            <input type="hidden" name="gender" value={{ '3' }}>
                            <input type="hidden" name="nationalityOperator" value={{ $nationalityOperator }} >
                            <input type="hidden" name="nationality" value={{ $nationality }} >
                            <input type="hidden" name="semester" value={{$selectedSemester}}>
                            <input type="hidden" name="fileName" value={{ $fileName.' - Fee Confirmed Male ' }} >
                            <button type="submit">
                                <i class="icon-cloud-download2"></i>
                            </button>
                        </form>
                    @else
                    {{'N/A'}}
                    @endif
                </td>
                <td>
                    @if ( $currentUserGenderPermission == $bothMaleAndFemale || $currentUserGenderPermission == $female )
                    {{ $programStats['feeVerifiedFemale'] }}

                        <form class="reports-form" action={{route('postCreateCsvForProgramsGeneral')}} method="POST">
                            <input type="hidden" name="programId" value={{$programId}}>
                            <input type="hidden" name="operator" value={{ '>=' }}>
                            <input type="hidden" name="status" value={{ '4' }}>
                            <input type="hidden" name="gender" value={{ '2' }}>
                            <input type="hidden" name="nationalityOperator" value={{ $nationalityOperator }} >
                            <input type="hidden" name="nationality" value={{ $nationality }} >
                            <input type="hidden" name="semester" value={{$selectedSemester}}>
                            <input type="hidden" name="fileName" value={{ $fileName.' - Fee Confirmed Female ' }} >
                            <button type="submit">
                                <i class="icon-cloud-download2"></i>
                            </button>
                        </form>
                    @else
                    {{'N/A'}}
                    @endif
                </td>
                

                <td>
                    @if ( $currentUserGenderPermission == $bothMaleAndFemale || $currentUserGenderPermission == $male )
                    {{ $programStats['documentsVerifiedMale'] }}
                   
                        <form class="reports-form" action={{route('postCreateCsvForProgramsGeneral')}} method="POST">
                            <input type="hidden" name="programId" value={{$programId}}>
                            <input type="hidden" name="operator" value={{ '=' }}>
                            <input type="hidden" name="status" value={{ '5' }}>
                            <input type="hidden" name="gender" value={{ '3' }}>
                            <input type="hidden" name="nationalityOperator" value={{ $nationalityOperator }} >
                            <input type="hidden" name="nationality" value={{ $nationality }} >
                            <input type="hidden" name="semester" value={{$selectedSemester}}>
                            <input type="hidden" name="fileName" value={{ $fileName.' - Documents Verified Male ' }} >
                            <button type="submit">
                                <i class="icon-cloud-download2"></i>
                            </button>
                        </form>
                    @else
                    {{'N/A'}}
                    @endif
                </td>
                <td>
                    @if ( $currentUserGenderPermission == $bothMaleAndFemale || $currentUserGenderPermission == $female )
                    {{ $programStats['documentsVerifiedFemale'] }}
                    
                        <form class="reports-form" action={{route('postCreateCsvForProgramsGeneral')}} method="POST">
                            <input type="hidden" name="programId" value={{$programId}}>
                            <input type="hidden" name="operator" value={{ '=' }}>
                            <input type="hidden" name="status" value={{ '5' }}>
                            <input type="hidden" name="gender" value={{ '2' }}>
                            <input type="hidden" name="nationalityOperator" value={{ $nationalityOperator }} >
                            <input type="hidden" name="nationality" value={{ $nationality }} >
                            <input type="hidden" name="semester" value={{$selectedSemester}}>
                            <input type="hidden" name="fileName" value={{ $fileName.' - Documents Verified Female ' }} >
                            <button type="submit">
                                <i class="icon-cloud-download2"></i>
                            </button>
                        </form>
                    @else
                    {{'N/A'}}
                    @endif
                </td>

                {{-- @if(session('gid') != '9' || session('eid') == 'dean.fet@iiu.edu.pk') --}}
                <td>
                    @if ( $currentUserGenderPermission == $bothMaleAndFemale || $currentUserGenderPermission == $male )
                    {{ $programStats['documentsRejectedMale'] }}
                    
                    <form class="reports-form" action={{route('postCreateCsvForProgramsGeneral')}} method="POST">
                            <input type="hidden" name="programId" value={{$programId}}>
                            <input type="hidden" name="operator" value={{ '=' }}>
                            <input type="hidden" name="status" value={{ '6' }}>
                            <input type="hidden" name="gender" value={{ '3' }}>
                            <input type="hidden" name="nationalityOperator" value={{ $nationalityOperator }} >
                            <input type="hidden" name="nationality" value={{ $nationality }} >
                            <input type="hidden" name="semester" value={{$selectedSemester}}>
                            <input type="hidden" name="fileName" value={{ $fileName.' - Documents Rejected Male ' }} >
                            <button type="submit">
                                <i class="icon-cloud-download2"></i>
                            </button>
                        </form>
                    @else
                    {{'N/A'}}
                    @endif
                </td>

                <td>
                    @if ( $currentUserGenderPermission == $bothMaleAndFemale || $currentUserGenderPermission == $female )
                    {{ $programStats['documentsRejectedFemale'] }}
                    
                        <form class="reports-form" action={{route('postCreateCsvForProgramsGeneral')}} method="POST">
                            <input type="hidden" name="programId" value={{$programId}}>
                            <input type="hidden" name="operator" value={{ '=' }}>
                            <input type="hidden" name="status" value={{ '6' }}>
                            <input type="hidden" name="gender" value={{ '2' }}>
                            <input type="hidden" name="nationalityOperator" value={{ $nationalityOperator }} >
                            <input type="hidden" name="nationality" value={{ $nationality }} >
                            <input type="hidden" name="semester" value={{$selectedSemester}}>
                            <input type="hidden" name="fileName" value={{ $fileName.' - Documents Rejected Female ' }} >
                            <button type="submit">
                                <i class="icon-cloud-download2"></i>
                            </button>
                        </form>
                    @else
                    {{'N/A'}}
                    @endif
                </td>
               {{-- @endif {{-- Ednign @if(session('gid') != '9') --}}
            </tr>
        </tbody>
    </table>
</div> <!-- Ending ending div.table-responsive --> 
@stop