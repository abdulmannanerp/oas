@extends('layouts.report')
@section('report-content')

@if( session('gid') != '9') 

<div class="table-responsive" style="padding-top: 30px;">
    <h4> {{ 'Faculty of '.$facultyTitle ?? '' }} </h4>
    <table class="table table-hover table-bordered app-search-grid" id="ManualVerifyTable">
        <thead  class="table-head">
        <tr>
            <th colspan="2">Form Issued</th>
            <th colspan="2">Fee Pending</th>
            <th colspan="2">Fee Verified</th>
            <th colspan="2">Approved</th>
            <th colspan="2">Rejected</th>
        </tr>
        <tr>
            <th>Male</th>
            <th>Female</th>

            <th>Male</th>
            <th>Female</th>

            <th>Male</th>
            <th>Female</th>

            <th>Male</th>
            <th>Female</th>

            <th>Male</th>
            <th>Female</th>
        </tr>
        </thead>
        <tbody>
        <tr>

            <td>
                {{$facultyStats['maleCount']}}
                @if ( $currentUserGenderPermission == $bothMaleAndFemale || $currentUserGenderPermission == $male )
                    <form class="reports-form" action={{route('postCreateCsvReport')}} method="POST" }}>
                        <input type="hidden" name="facultyId" value={{$id}}>
                        <input type="hidden" name="nationalityId" value={{$tempNationality}}>
                        <input type="hidden" name="semester" value={{$selectedSemester}}>
                        <input type="hidden" name="helperWord" value={{'all-male-applicants'}}>
                        <input type="hidden" name="fileName" value={{'All Male Applicants'}}>
                        <button type="submit">
                            <i class="icon-cloud-download2"></i>
                        </button>
                    </form>
                @endif
            </td>
            <td>
                {{$facultyStats['femaleCount']}}
                @if ( $currentUserGenderPermission == $bothMaleAndFemale || $currentUserGenderPermission == $female )
                    <form class="reports-form" action={{route('postCreateCsvReport')}} method="POST" }}>
                        <input type="hidden" name="facultyId" value={{$id}}>
                        <input type="hidden" name="nationalityId" value={{$tempNationality}}>
                        <input type="hidden" name="semester" value={{$selectedSemester}}>
                        <input type="hidden" name="helperWord" value={{'all-female-applicants'}}>
                        <input type="hidden" name="fileName" value={{'All Female Applicants'}}>
                        <button type="submit">
                            <i class="icon-cloud-download2"></i>
                        </button>
                    </form>
                @endif
            </td>

            <td>
                {{$facultyStats['feePendingMale']}}
                @if ( $currentUserGenderPermission == $bothMaleAndFemale || $currentUserGenderPermission == $male )
                    <form class="reports-form" action={{route('postCreateCsvReport')}} method="POST" }}>
                        <input type="hidden" name="facultyId" value={{$id}}>
                        <input type="hidden" name="nationalityId" value={{$tempNationality}}>
                        <input type="hidden" name="semester" value={{$selectedSemester}}>
                        <input type="hidden" name="helperWord" value={{'fee-pending-male'}}>
                        <input type="hidden" name="fileName" value={{'Fee Pending Male'}}>
                        <button type="submit">
                            <i class="icon-cloud-download2"></i>
                        </button>
                    </form>
                @endif
            </td>

            <td>
                {{$facultyStats['feePendingFemale']}}
                @if ( $currentUserGenderPermission == $bothMaleAndFemale || $currentUserGenderPermission == $female )
                    <form class="reports-form" action={{route('postCreateCsvReport')}} method="POST" }}>
                        <input type="hidden" name="facultyId" value={{$id}}>
                        <input type="hidden" name="nationalityId" value={{$tempNationality}}>
                        <input type="hidden" name="semester" value={{$selectedSemester}}>
                        <input type="hidden" name="helperWord" value={{'fee-pending-female'}}>
                        <input type="hidden" name="fileName" value={{'Fee Pending Female'}}>
                        <button type="submit">
                            <i class="icon-cloud-download2"></i>
                        </button>
                    </form>
                @endif
            </td>

            <td>
                {{ $facultyStats['feeVerifiedMale'] }}
                @if ( $currentUserGenderPermission == $bothMaleAndFemale || $currentUserGenderPermission == $male )
                    <form class="reports-form" action={{route('postCreateCsvReport')}} method="POST" }}>
                        <input type="hidden" name="facultyId" value={{$id}}>
                        <input type="hidden" name="nationalityId" value={{$tempNationality}}>
                        <input type="hidden" name="semester" value={{$selectedSemester}}>
                        <input type="hidden" name="helperWord" value={{'fee-confirmed-male'}}>
                        <input type="hidden" name="fileName" value={{'Fee Confirmed Male'}}>
                        <button type="submit">
                            <i class="icon-cloud-download2"></i>
                        </button>
                    </form>
                @endif
            </td>

            <td>
                {{ $facultyStats['feeVerifiedFemale'] }}
                @if ( $currentUserGenderPermission == $bothMaleAndFemale || $currentUserGenderPermission == $female )
                    <form class="reports-form" action={{route('postCreateCsvReport')}} method="POST" }}>
                        <input type="hidden" name="facultyId" value={{$id}}>
                        <input type="hidden" name="nationalityId" value={{$tempNationality}}>
                        <input type="hidden" name="semester" value={{$selectedSemester}}>
                        <input type="hidden" name="helperWord" value={{'fee-confirmed-female'}}>
                        <input type="hidden" name="fileName" value={{'Fee Confirmed Female'}}>
                        <button type="submit">
                            <i class="icon-cloud-download2"></i>
                        </button>
                    </form>
                @endif
            </td>

            <td>
                {{ $facultyStats['documentsVerifiedMale'] }}
                @if ( $currentUserGenderPermission == $bothMaleAndFemale || $currentUserGenderPermission == $male )
                    <form class="reports-form" action={{route('postCreateCsvReport')}} method="POST" }}>
                        <input type="hidden" name="facultyId" value={{$id}}>
                        <input type="hidden" name="nationalityId" value={{$tempNationality}}>
                        <input type="hidden" name="semester" value={{$selectedSemester}}>
                        <input type="hidden" name="helperWord" value={{'docs-verified-male'}}>
                        <input type="hidden" name="fileName" value={{'Documents Verified Male'}}>
                        <button type="submit">
                            <i class="icon-cloud-download2"></i>
                        </button>
                    </form>
                @endif
            </td>

            <td>
                {{ $facultyStats['documentsVerifiedFemale'] }}
                @if ( $currentUserGenderPermission == $bothMaleAndFemale || $currentUserGenderPermission == $female )
                    <form class="reports-form" action={{route('postCreateCsvReport')}} method="POST" }}>
                        <input type="hidden" name="facultyId" value={{$id}}>
                        <input type="hidden" name="nationalityId" value={{$tempNationality}}>
                        <input type="hidden" name="semester" value={{$selectedSemester}}>
                        <input type="hidden" name="helperWord" value={{'docs-verified-female'}}>
                        <input type="hidden" name="fileName" value={{'Documents Verified Female'}}>
                        <button type="submit">
                            <i class="icon-cloud-download2"></i>
                        </button>
                    </form>
                @endif
            </td>

            <td>
                {{ $facultyStats['documentsRejectedMale'] }}
                @if ( $currentUserGenderPermission == $bothMaleAndFemale || $currentUserGenderPermission == $male )
                    <form class="reports-form" action={{route('postCreateCsvReport')}} method="POST" }}>
                        <input type="hidden" name="facultyId" value={{$id}}>
                        <input type="hidden" name="nationalityId" value={{$tempNationality}}>
                        <input type="hidden" name="semester" value={{$selectedSemester}}>
                        <input type="hidden" name="helperWord" value={{'rejected-male'}}>
                        <input type="hidden" name="fileName" value={{'Documents Rejected Male'}}>
                        <button type="submit">
                            <i class="icon-cloud-download2"></i>
                        </button>
                    </form>
                @endif
            </td>

            <td>
                {{ $facultyStats['documentsRejectedFemale'] }}
                @if ( $currentUserGenderPermission == $bothMaleAndFemale || $currentUserGenderPermission == $female )
                    <form class="reports-form" action={{route('postCreateCsvReport')}} method="POST" }}>
                        <input type="hidden" name="facultyId" value={{$id}}>
                        <input type="hidden" name="nationalityId" value={{$tempNationality}}>
                        <input type="hidden" name="semester" value={{$selectedSemester}}>
                        <input type="hidden" name="helperWord" value={{'rejected-female'}}>
                        <input type="hidden" name="fileName" value={{'Documents Rejected Female'}}>
                        <button type="submit">
                            <i class="icon-cloud-download2"></i>
                        </button>
                    </form>
                @endif
            </td>

        </tr>
        </tbody>
    </table>
</div> <!-- Ending table-responsive --> 
@else
    <a href={{ route('faculty') }}>Unauthorized Access</a>
@endif

@stop
