@extends('layouts.app')


@section('content')
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">

    <div class="page-content row">
        <div class="page-content-wrapper m-t">
            <h3>Overall Report</h3>
                <h4>Programwise Report</h4>
                <form action="{{ route('faculty') }}" method="POST">
                    <div class="form-group">
                        <select name="faculty" id="faculty" required>
                            <option value="">--Select Faculty--</option>
                            @foreach ( $faculties as $faculty )
                                <option value="{{$faculty->pkFacId}}">{{$faculty->title}}</option>
                            @endforeach
                        </select>
                        <select name="department" id="department">
                            <option value="">--Select Department--</option>
                        </select>
                        <select name="program" id="programme">
                            <option value="">--Select Program--</option>
                        </select>

                        <select name="overseas" id="nationality-list">
                            <option value="">--Applicant Nationality--</option>
                            <option value="1">Pakistani</option>
                            <option value="2">OverSeas Pakistani</option>
                            <option value="3">Overseas</option>
                        </select>
                        <input type="submit" value="Get Report" id="get-report-btn">
                    </div>

                </form>
                @if ( $method == 'post')

                    <?php
                        $nationalityOperator = '=';
                        $nationality = $tempNationality = request('overseas');
                        $male = '3';
                        $female = '2';
                        $bothMaleAndFemale = '1';
                        
                        if ( $nationality == '' ) {
                            $nationalityOperator = '>=';
                            $nationality = 1;
                            $tempNationality = 0; // A helper variable that will be passed to reportController for overall faculty stats
                        }

                    ?>
                {{-- Dev Mannan: Faculty Users check --}}
                @if ( $statsToDisplay == 'faculty' )
                @if(session('gid') != '9')
                <div class="table-responsive" style="padding-top: 70px;">
                    <h4> {{ 'Faculty of '.$facultyTitle ?? '' }} </h4>
                <table class="table table-hover table-bordered app-search-grid" id="ManualVerifyTable">
                    <thead  class="table-head">
                    <tr>
                        <th colspan="2">Form Issued</th>
                        <th colspan="2">Fee Pending</th>
                        <th colspan="2">Fee Verified</th>
                        <th colspan="2">Approved</th>
                        <th colspan="2">Rejected</th>
                    </tr>
                    <tr>
                        <th>Male</th>
                        <th>Female</th>

                        <th>Male</th>
                        <th>Female</th>

                        <th>Male</th>
                        <th>Female</th>

                        <th>Male</th>
                        <th>Female</th>

                        <th>Male</th>
                        <th>Female</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>

                        <td>
                            {{$applicationsWithMaleApplicants}}
                            @if ( $currentUserGenderPermission == $bothMaleAndFemale || $currentUserGenderPermission == $male )
                                <form class="reports-form" action={{route('postCreateCsvReport')}} method="POST" }}>
                                    <input type="hidden" name="facultyId" value={{$id}}>
                                    <input type="hidden" name="nationalityId" value={{$tempNationality}}>
                                    <input type="hidden" name="helperWord" value={{'all-male-applicants'}}>
                                    <input type="hidden" name="fileName" value={{'All Male Applicants'}}>
                                    <button type="submit">
                                        <i class="icon-cloud-download2"></i>
                                    </button>
                                </form>
                            @endif
                        </td>
                        <td>
                            {{$applicationsWithFemaleApplicants}}
                            @if ( $currentUserGenderPermission == $bothMaleAndFemale || $currentUserGenderPermission == $female )
                                <form class="reports-form" action={{route('postCreateCsvReport')}} method="POST" }}>
                                    <input type="hidden" name="facultyId" value={{$id}}>
                                    <input type="hidden" name="nationalityId" value={{$tempNationality}}>
                                    <input type="hidden" name="helperWord" value={{'all-female-applicants'}}>
                                    <input type="hidden" name="fileName" value={{'All Female Applicants'}}>
                                    <button type="submit">
                                        <i class="icon-cloud-download2"></i>
                                    </button>
                                </form>
                            @endif
                        </td>

                        <td>
                            {{$applicant['feePendingMale']}}
                            @if ( $currentUserGenderPermission == $bothMaleAndFemale || $currentUserGenderPermission == $male )
                                <form class="reports-form" action={{route('postCreateCsvReport')}} method="POST" }}>
                                    <input type="hidden" name="facultyId" value={{$id}}>
                                    <input type="hidden" name="nationalityId" value={{$tempNationality}}>
                                    <input type="hidden" name="helperWord" value={{'fee-pending-male'}}>
                                    <input type="hidden" name="fileName" value={{'Fee Pending Male'}}>
                                    <button type="submit">
                                        <i class="icon-cloud-download2"></i>
                                    </button>
                                </form>
                            @endif
                        </td>

                        <td>
                            {{$applicant['feePendingFemale']}}
                            @if ( $currentUserGenderPermission == $bothMaleAndFemale || $currentUserGenderPermission == $female )
                                <form class="reports-form" action={{route('postCreateCsvReport')}} method="POST" }}>
                                    <input type="hidden" name="facultyId" value={{$id}}>
                                    <input type="hidden" name="nationalityId" value={{$tempNationality}}>
                                    <input type="hidden" name="helperWord" value={{'fee-pending-female'}}>
                                    <input type="hidden" name="fileName" value={{'Fee Pending Female'}}>
                                    <button type="submit">
                                        <i class="icon-cloud-download2"></i>
                                    </button>
                                </form>
                            @endif
                        </td>

                        <td>
                            {{ $applicant['feeConfirmedMale'] }}
                            @if ( $currentUserGenderPermission == $bothMaleAndFemale || $currentUserGenderPermission == $male )
                                <form class="reports-form" action={{route('postCreateCsvReport')}} method="POST" }}>
                                    <input type="hidden" name="facultyId" value={{$id}}>
                                    <input type="hidden" name="nationalityId" value={{$tempNationality}}>
                                    <input type="hidden" name="helperWord" value={{'fee-confirmed-male'}}>
                                    <input type="hidden" name="fileName" value={{'Fee Confirmed Male'}}>
                                    <button type="submit">
                                        <i class="icon-cloud-download2"></i>
                                    </button>
                                </form>
                            @endif
                        </td>

                        <td>
                            {{ $applicant['feeConfirmedFemale'] }}
                            @if ( $currentUserGenderPermission == $bothMaleAndFemale || $currentUserGenderPermission == $female )
                                <form class="reports-form" action={{route('postCreateCsvReport')}} method="POST" }}>
                                    <input type="hidden" name="facultyId" value={{$id}}>
                                    <input type="hidden" name="nationalityId" value={{$tempNationality}}>
                                    <input type="hidden" name="helperWord" value={{'fee-confirmed-female'}}>
                                    <input type="hidden" name="fileName" value={{'Fee Confirmed Female'}}>
                                    <button type="submit">
                                        <i class="icon-cloud-download2"></i>
                                    </button>
                                </form>
                            @endif
                        </td>

                        <td>
                            {{ $applicant['documentsVerifiedMale'] }}
                            @if ( $currentUserGenderPermission == $bothMaleAndFemale || $currentUserGenderPermission == $male )
                                <form class="reports-form" action={{route('postCreateCsvReport')}} method="POST" }}>
                                    <input type="hidden" name="facultyId" value={{$id}}>
                                    <input type="hidden" name="nationalityId" value={{$tempNationality}}>
                                    <input type="hidden" name="helperWord" value={{'docs-verified-male'}}>
                                    <input type="hidden" name="fileName" value={{'Documents Verified Male'}}>
                                    <button type="submit">
                                        <i class="icon-cloud-download2"></i>
                                    </button>
                                </form>
                            @endif
                        </td>

                        <td>
                            {{ $applicant['documentsVerifiedFemale'] }}
                            @if ( $currentUserGenderPermission == $bothMaleAndFemale || $currentUserGenderPermission == $female )
                                <form class="reports-form" action={{route('postCreateCsvReport')}} method="POST" }}>
                                    <input type="hidden" name="facultyId" value={{$id}}>
                                    <input type="hidden" name="nationalityId" value={{$tempNationality}}>
                                    <input type="hidden" name="helperWord" value={{'docs-verified-female'}}>
                                    <input type="hidden" name="fileName" value={{'Documents Verified Female'}}>
                                    <button type="submit">
                                        <i class="icon-cloud-download2"></i>
                                    </button>
                                </form>
                            @endif
                        </td>

                        <td>
                            {{ $applicant['rejectedMale'] }}
                            @if ( $currentUserGenderPermission == $bothMaleAndFemale || $currentUserGenderPermission == $male )
                                <form class="reports-form" action={{route('postCreateCsvReport')}} method="POST" }}>
                                    <input type="hidden" name="facultyId" value={{$id}}>
                                    <input type="hidden" name="nationalityId" value={{$tempNationality}}>
                                    <input type="hidden" name="helperWord" value={{'rejected-male'}}>
                                    <input type="hidden" name="fileName" value={{'Documents Rejected Male'}}>
                                    <button type="submit">
                                        <i class="icon-cloud-download2"></i>
                                    </button>
                                </form>
                            @endif
                        </td>

                        <td>
                            {{ $applicant['rejectedFemale'] }}
                            @if ( $currentUserGenderPermission == $bothMaleAndFemale || $currentUserGenderPermission == $female )
                                <form class="reports-form" action={{route('postCreateCsvReport')}} method="POST" }}>
                                    <input type="hidden" name="facultyId" value={{$id}}>
                                    <input type="hidden" name="nationalityId" value={{$tempNationality}}>
                                    <input type="hidden" name="helperWord" value={{'rejected-female'}}>
                                    <input type="hidden" name="fileName" value={{'Documents Rejected Female'}}>
                                    <button type="submit">
                                        <i class="icon-cloud-download2"></i>
                                    </button>
                                </form>
                            @endif
                        </td>

                    </tr>
                    </tbody>
                </table>
                </div>
                @endif
                @else


                    @foreach ( $departments as $dept )
                    {{-- Dev Mannan: Check for faculty users --}}
                    @if(session('gid') == '9')
                        @if(!in_array($dept->pkDeptId, $departmentPermissions) && $departmentPermissions != 'not-found')
                        <?php continue; ?>
                        @endif
                    @endif
                    <?php
                        if ( $dept->status != 1 )
                            continue;
                        // $facultyTitle = App\Model\Faculty::find($id)->title ?? '';
                        $department = App\Model\Department::find ( $dept->pkDeptId );
                        $departmentTitle = $department->title ?? '';
                        $fileName = $departmentTitle;
                        $applicationIdsInDepartment = $department->applications->pluck('id');;
                        $applicationIdsList = [];
                        foreach ( $applicationIdsInDepartment as $id ) {
                            $applicationIdsList[] = $id;
                        }

                        $applicationIdsList = implode (',', $applicationIdsList  );

                        $applicationsWithMaleApplicants = App\Model\Application::with(['applicant'])->whereIn('id', $applicationIdsInDepartment)->whereHas('applicant', function( $query) use ($nationalityOperator, $nationality) {
                            $query->where('fkGenderId', 3)->where('fkNationality', $nationalityOperator, $nationality);
                        })->get();

                        $applicationsWithFemaleApplicants = App\Model\Application::with(['applicant'])->whereIn('id', $applicationIdsInDepartment)->whereHas('applicant', function( $query) use ($nationalityOperator, $nationality) {
                            $query->where('fkGenderId', 2)->where('fkNationality', $nationalityOperator, $nationality);
                        })->get();

                    ?>
                        <div class="table-responsive" style="padding-top: 30px;">
                            <h4>{{$department->title}}</h4>
                            <table class="table table-hover table-bordered app-search-grid" id="ManualVerifyTable">
                                <thead class="table-head">
                                <tr>
                                    @if(session('gid') != '9') 
                                    <th colspan="2">Form Issued</th>
                                    <th colspan="2">Fee Pending</th>
                                    <th colspan="2">Fee Verified</th>
                                    @endif
                                    <th colspan="2">Approved</th>
                                    @if(session('gid') != '9')
                                    <th colspan="2">Rejected</th>
                                    @endif
                                </tr>
                                <tr>
                                    @if(session('gid') != '9')
                                    <th>Male</th>
                                    <th>Female</th>

                                    <th>Male</th>
                                    <th>Female</th>

                                    <th>Male</th>
                                    <th>Female</th>
                                    @endif

                                    <th>Male</th>
                                    <th>Female</th>

                                    @if(session('gid') != '9')

                                    <th>Male</th>
                                    <th>Female</th>
                                    @endif
                                </tr>
                                </thead>
                                <tbody>
                                    
                                    @if ( $applicationIdsList)
                                <tr>
                                        @if(session('gid') != '9')

                                    <td>
                                        @if ( $currentUserGenderPermission == $bothMaleAndFemale || $currentUserGenderPermission == $male )
                                            {{$applicationsWithMaleApplicants->count()}}
                                            <form class="reports-form" action={{route('postCreateCsvForDepartments')}} method="POST">
                                                <input type="hidden" name="applicationsIds" value={{$applicationIdsList}}>
                                                <input type="hidden" name="operator" value={{$nationalityOperator}}>
                                                <input type="hidden" name="nationality" value={{ $nationality }} >
                                                <input type="hidden" name="helper" value= {{ 'all-male-applicants' }} >
                                                <input type="hidden" name="fileName" value={{ $fileName . ' - All Male Applicants' }}>

                                                <button type="submit">
                                                    <i class="icon-cloud-download2"></i>
                                                </button>
                                            </form>
                                        @else
                                            {{'N/A'}}
                                        @endif
                                    </td>
                                    
                                    <td>
                                        @if ( $currentUserGenderPermission == $bothMaleAndFemale || $currentUserGenderPermission == $female )
                                            {{$applicationsWithFemaleApplicants->count()}}
                                            <form class="reports-form" action={{route('postCreateCsvForDepartments')}} method="POST">
                                                <input type="hidden" name="applicationsIds" value={{$applicationIdsList}}>
                                                <input type="hidden" name="operator" value={{$nationalityOperator}}>
                                                <input type="hidden" name="nationality" value={{ $nationality }} >
                                                <input type="hidden" name="helper" value= {{ 'all-female-applicants' }} >
                                                <input type="hidden" name="fileName" value={{ $fileName . ' - All Female Applicants ' }}>

                                                <button type="submit">
                                                    <i class="icon-cloud-download2"></i>
                                                </button>
                                            </form>
                                        @else
                                            {{'N/A'}}
                                        @endif
                                    </td>

                                    <td>
                                        @if ( $currentUserGenderPermission == $bothMaleAndFemale || $currentUserGenderPermission == $male )
                                            {{App\Model\Application::whereIn('id', $applicationIdsInDepartment)->getStatusByGender('=', '3', '3', $nationalityOperator, $nationality)->count()}}
                                            
                                            <form class="reports-form" action={{route('postCreateCsvForDepartmentsGeneral')}} method="POST">
                                                <input type="hidden" name="applicationIds" value={{$applicationIdsList}}>
                                                <input type="hidden" name="operator" value={{'='}}>
                                                <input type="hidden" name="status" value={{ '3' }} >
                                                <input type="hidden" name="gender" value={{ '3' }} >
                                                <input type="hidden" name="nationalityOperator" value={{ $nationalityOperator }} >
                                                <input type="hidden" name="nationality" value={{ $nationality }} >
                                                <input type="hidden" name="fileName" value={{ $fileName . ' - Fee Pending Male ' }}>

                                                <button type="submit">
                                                    <i class="icon-cloud-download2"></i>
                                                </button>
                                            </form>

                                        @else
                                            {{'N/A'}}
                                        @endif
                                    </td>

                                    <td>
                                        @if ( $currentUserGenderPermission == $bothMaleAndFemale || $currentUserGenderPermission == $female )
                                            {{App\Model\Application::whereIn('id', $applicationIdsInDepartment)->getStatusByGender('=', '3', '2', $nationalityOperator, $nationality)->count()}}
                                            
                                            <form class="reports-form" action={{route('postCreateCsvForDepartmentsGeneral')}} method="POST">
                                                <input type="hidden" name="applicationIds" value={{$applicationIdsList}}>
                                                <input type="hidden" name="operator" value={{'='}}>
                                                <input type="hidden" name="status" value={{ '3' }} >
                                                <input type="hidden" name="gender" value={{ '2' }} >
                                                <input type="hidden" name="nationalityOperator" value={{ $nationalityOperator }} >
                                                <input type="hidden" name="nationality" value={{ $nationality }} >
                                                <input type="hidden" name="fileName" value={{ $fileName . ' - Fee Pending Female ' }}>

                                                <button type="submit">
                                                    <i class="icon-cloud-download2"></i>
                                                </button>
                                            </form>
                                        @else
                                            {{'N/A'}}
                                        @endif
                                    </td>

                                    <td>
                                        @if ( $currentUserGenderPermission == $bothMaleAndFemale || $currentUserGenderPermission == $male )
                                            {{App\Model\Application::whereIn('id', $applicationIdsInDepartment)->getStatusByGender('>=', '4', '3', $nationalityOperator, $nationality)->count()}}
                                            
                                            <form class="reports-form" action={{route('postCreateCsvForDepartmentsGeneral')}} method="POST">
                                                <input type="hidden" name="applicationIds" value={{$applicationIdsList}}>
                                                <input type="hidden" name="operator" value={{'>='}}>
                                                <input type="hidden" name="status" value={{ '4' }} >
                                                <input type="hidden" name="gender" value={{ '3' }} >
                                                <input type="hidden" name="nationalityOperator" value={{ $nationalityOperator }} >
                                                <input type="hidden" name="nationality" value={{ $nationality }} >
                                                <input type="hidden" name="fileName" value={{ $fileName . ' - Fee Confirmed Male ' }}>

                                                <button type="submit">
                                                    <i class="icon-cloud-download2"></i>
                                                </button>
                                            </form>
                                        @else
                                            {{'N/A'}}
                                        @endif
                                    </td>
                                    <td>
                                        @if ( $currentUserGenderPermission == $bothMaleAndFemale || $currentUserGenderPermission == $female )
                                            {{App\Model\Application::whereIn('id', $applicationIdsInDepartment)->getStatusByGender('>=', '4', '2', $nationalityOperator, $nationality)->count()}}

                                            <form class="reports-form" action={{route('postCreateCsvForDepartmentsGeneral')}} method="POST">
                                                <input type="hidden" name="applicationIds" value={{$applicationIdsList}}>
                                                <input type="hidden" name="operator" value={{'>='}}>
                                                <input type="hidden" name="status" value={{ '4' }} >
                                                <input type="hidden" name="gender" value={{ '2' }} >
                                                <input type="hidden" name="nationalityOperator" value={{ $nationalityOperator }} >
                                                <input type="hidden" name="nationality" value={{ $nationality }} >
                                                <input type="hidden" name="fileName" value={{ $fileName . ' - Fee Confirmed Female ' }}>

                                                <button type="submit">
                                                    <i class="icon-cloud-download2"></i>
                                                </button>
                                            </form>
                                        @else
                                            {{'N/A'}}
                                        @endif
                                    </td>
                                    @endif

                                    <td>
                                        @if ( $currentUserGenderPermission == $bothMaleAndFemale || $currentUserGenderPermission == $male )
                                            {{App\Model\Application::whereIn('id', $applicationIdsInDepartment)->getStatusByGender('=', '5', '3', $nationalityOperator, $nationality)->count()}}
                                            
                                            <form class="reports-form" action={{route('postCreateCsvForDepartmentsGeneral')}} method="POST">
                                                <input type="hidden" name="applicationIds" value={{$applicationIdsList}}>
                                                <input type="hidden" name="operator" value={{'='}}>
                                                <input type="hidden" name="status" value={{ '5' }} >
                                                <input type="hidden" name="gender" value={{ '3' }} >
                                                <input type="hidden" name="nationalityOperator" value={{ $nationalityOperator }} >
                                                <input type="hidden" name="nationality" value={{ $nationality }} >
                                                <input type="hidden" name="fileName" value={{ $fileName . ' - Documents Verified Male ' }}>

                                                <button type="submit">
                                                    <i class="icon-cloud-download2"></i>
                                                </button>
                                            </form>
                                        @else
                                            {{'N/A'}}
                                        @endif
                                    </td>

                                    <td>
                                        @if ( $currentUserGenderPermission == $bothMaleAndFemale || $currentUserGenderPermission == $female )
                                            {{App\Model\Application::whereIn('id', $applicationIdsInDepartment)->getStatusByGender('=', '5', '2', $nationalityOperator, $nationality)->count()}}
                                            
                                            <form class="reports-form" action={{route('postCreateCsvForDepartmentsGeneral')}} method="POST">
                                                <input type="hidden" name="applicationIds" value={{$applicationIdsList}}>
                                                <input type="hidden" name="operator" value={{'='}}>
                                                <input type="hidden" name="status" value={{ '5' }} >
                                                <input type="hidden" name="gender" value={{ '2' }} >
                                                <input type="hidden" name="nationalityOperator" value={{ $nationalityOperator }} >
                                                <input type="hidden" name="nationality" value={{ $nationality }} >
                                                <input type="hidden" name="fileName" value={{ $fileName . ' - Documents Verified Female ' }}>

                                                <button type="submit">
                                                    <i class="icon-cloud-download2"></i>
                                                </button>
                                            </form>
                                        @else
                                            {{'N/A'}}
                                        @endif
                                    </td>
                                    @if(session('gid') != '9')
                                    <td>
                                        @if ( $currentUserGenderPermission == $bothMaleAndFemale || $currentUserGenderPermission == $male )
                                            {{App\Model\Application::whereIn('id', $applicationIdsInDepartment)->getStatusByGender('=', '6', '3', $nationalityOperator, $nationality)->count()}}
                                            
                                            <form class="reports-form" action={{route('postCreateCsvForDepartmentsGeneral')}} method="POST">
                                                <input type="hidden" name="applicationIds" value={{$applicationIdsList}}>
                                                <input type="hidden" name="operator" value={{'='}}>
                                                <input type="hidden" name="status" value={{ '6' }} >
                                                <input type="hidden" name="gender" value={{ '3' }} >
                                                <input type="hidden" name="nationalityOperator" value={{ $nationalityOperator }} >
                                                <input type="hidden" name="nationality" value={{ $nationality }} >
                                                <input type="hidden" name="fileName" value={{ $fileName . ' - Documents Rejected Male ' }}>

                                                <button type="submit">
                                                    <i class="icon-cloud-download2"></i>
                                                </button>
                                            </form>
                                        @else
                                            {{'N/A'}}
                                        @endif
                                    </td>
                                    <td>
                                        @if ( $currentUserGenderPermission == $bothMaleAndFemale || $currentUserGenderPermission == $female )
                                            {{App\Model\Application::whereIn('id', $applicationIdsInDepartment)->getStatusByGender('=', '6', '2', $nationalityOperator, $nationality)->count()}}                                            
                                            
                                            <form class="reports-form" action={{route('postCreateCsvForDepartmentsGeneral')}} method="POST">
                                                <input type="hidden" name="applicationIds" value={{$applicationIdsList}}>
                                                <input type="hidden" name="operator" value={{'='}}>
                                                <input type="hidden" name="status" value={{ '6' }} >
                                                <input type="hidden" name="gender" value={{ '2' }} >
                                                <input type="hidden" name="nationalityOperator" value={{ $nationalityOperator }} >
                                                <input type="hidden" name="nationality" value={{ $nationality }} >
                                                <input type="hidden" name="fileName" value={{ $fileName . ' - Documents Rejected Female ' }}>

                                                <button type="submit">
                                                    <i class="icon-cloud-download2"></i>
                                                </button>
                                            </form>
                                        @else
                                            {{'N/A'}}
                                        @endif
                                    </td>
                                    @endif
                                </tr>
                                @else
                                <tr>
                                    <td>No records found for this department</td>
                                </tr>
                                @endif
                                </tbody>
                            </table>
                        </div>
                        {{-- Starting Program --}}
                        @if ( $dept->programme )
                        @foreach ( $dept->programme as $program )
                        @if ( $program->status == 0 )
                            <?php continue; ?>
                        @endif
                        <?php
                            $programId = $program->pkProgId;
                            $fileName  = $program->title;
                            $programApplicationsWithMaleApplicants = App\Model\Application::with(['applicant'])->where('fkProgramId', $programId)->whereHas('applicant', function( $query) use ($nationalityOperator, $nationality) {
                                $query->where('fkGenderId', 3)->where('fkNationality', $nationalityOperator, $nationality);
                            })->get();

                            $programApplicationsWithFemaleApplicants = App\Model\Application::with(['applicant'])->where('fkProgramId', $programId)->whereHas('applicant', function( $query) use ($nationalityOperator, $nationality) {
                                $query->where('fkGenderId', 2)->where('fkNationality', $nationalityOperator, $nationality);
                            })->get();
                        ?>
                        <div class="table-responsive">    
                            <h5>Program: {{$program->title}}</h5>
                            
                            <table class="table table-hover table-bordered app-search-grid">
                                <thead class="table-head">
                                <tr>
                                    @if(session('gid') != '9')
                                    <th colspan="2">Form Issued</th>
                                    <th colspan="2">Fee Pending</th>
                                    <th colspan="2">Fee Verified</th>
                                    @endif
                                    <th colspan="2">Approved</th>
                                    @if(session('gid') != '9')
                                    <th colspan="2">Rejected</th>
                                    @endif
                                </tr>
                                <tr>
                                    @if(session('gid') != '9')
                                    <th>Male</th>
                                    <th>Female</th>

                                    <th>Male</th>
                                    <th>Female</th>

                                    <th>Male</th>
                                    <th>Female</th>
                                    @endif

                                    <th>Male</th>
                                    <th>Female</th>
                                    @if(session('gid') != '9')

                                    <th>Male</th>
                                    <th>Female</th>
                                    @endif
                                </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        @if(session('gid') != '9')
                                        <td>
                                            @if ( $currentUserGenderPermission == $bothMaleAndFemale || $currentUserGenderPermission == $male )
                                                {{ $programApplicationsWithMaleApplicants->count() }} 
                                                
                                                <form class="reports-form" action={{route('postCreateCsvForPrograms')}} method="POST">
                                                    <input type="hidden" name="programId" value={{$programId}}>
                                                    <input type="hidden" name="operator" value={{$nationalityOperator}}>
                                                    <input type="hidden" name="nationality" value={{ $nationality }} >
                                                    <input type="hidden" name="helper" value={{ 'all-male-applicants' }} >
                                                    <input type="hidden" name="fileName" value={{ $fileName.' - All Male Applicants ' }} >
                                                    <button type="submit">
                                                        <i class="icon-cloud-download2"></i>
                                                    </button>
                                                </form>
                                            @else
                                                {{'N/A'}}
                                            @endif
                                        </td>
                                        <td>
                                            @if ( $currentUserGenderPermission == $bothMaleAndFemale || $currentUserGenderPermission == $female )
                                                {{ $programApplicationsWithFemaleApplicants->count() }}
                                                
                                                <form class="reports-form" action={{route('postCreateCsvForPrograms')}} method="POST">
                                                    <input type="hidden" name="programId" value={{$programId}}>
                                                    <input type="hidden" name="operator" value={{$nationalityOperator}}>
                                                    <input type="hidden" name="nationality" value={{ $nationality }} >
                                                    <input type="hidden" name="helper" value={{ 'all-female-applicants' }} >
                                                    <input type="hidden" name="fileName" value={{ $fileName.' - All Female Applicants ' }} >
                                                    <button type="submit">
                                                        <i class="icon-cloud-download2"></i>
                                                    </button>
                                                </form>
                                            @else
                                                {{'N/A'}}
                                            @endif
                                        </td>
                                        
                                        <td>
                                            @if ( $currentUserGenderPermission == $bothMaleAndFemale || $currentUserGenderPermission == $male )
                                            {{App\Model\Application::where('fkProgramId', $programId)->getStatusByGender('=', '3', '3', $nationalityOperator, $nationality)->count()}}

                                                <form class="reports-form" action={{route('postCreateCsvForProgramsGeneral')}} method="POST">
                                                    <input type="hidden" name="programId" value={{$programId}}>
                                                    <input type="hidden" name="operator" value={{ '=' }}>
                                                    <input type="hidden" name="status" value={{ '3' }}>
                                                    <input type="hidden" name="gender" value={{ '3' }}>
                                                    <input type="hidden" name="nationalityOperator" value={{ $nationalityOperator }} >
                                                    <input type="hidden" name="nationality" value={{ $nationality }} >
                                                    <input type="hidden" name="fileName" value={{ $fileName.' - Fee Pending Male ' }} >
                                                    <button type="submit">
                                                        <i class="icon-cloud-download2"></i>
                                                    </button>
                                                </form>
                                            @else
                                            {{'N/A'}}
                                            @endif
                                        </td>

                                        <td>
                                            @if ( $currentUserGenderPermission == $bothMaleAndFemale || $currentUserGenderPermission == $female )
                                            {{App\Model\Application::where('fkProgramId', $programId)->getStatusByGender('=', '3', '2', $nationalityOperator, $nationality)->count()}}

                                                <form class="reports-form" action={{route('postCreateCsvForProgramsGeneral')}} method="POST">
                                                    <input type="hidden" name="programId" value={{$programId}}>
                                                    <input type="hidden" name="operator" value={{ '=' }}>
                                                    <input type="hidden" name="status" value={{ '3' }}>
                                                    <input type="hidden" name="gender" value={{ '2' }}>
                                                    <input type="hidden" name="nationalityOperator" value={{ $nationalityOperator }} >
                                                    <input type="hidden" name="nationality" value={{ $nationality }} >
                                                    <input type="hidden" name="fileName" value={{ $fileName.' - Fee Pending Female ' }} >
                                                    <button type="submit">
                                                        <i class="icon-cloud-download2"></i>
                                                    </button>
                                                </form>
                                            @else
                                            {{'N/A'}}
                                            @endif
                                        </td>
                                        
                                        <td>
                                            @if ( $currentUserGenderPermission == $bothMaleAndFemale || $currentUserGenderPermission == $male )
                                            {{App\Model\Application::where('fkProgramId', $programId)->getStatusByGender('>=', '4', '3', $nationalityOperator, $nationality)->count()}}

                                                <form class="reports-form" action={{route('postCreateCsvForProgramsGeneral')}} method="POST">
                                                    <input type="hidden" name="programId" value={{$programId}}>
                                                    <input type="hidden" name="operator" value={{ '>=' }}>
                                                    <input type="hidden" name="status" value={{ '4' }}>
                                                    <input type="hidden" name="gender" value={{ '3' }}>
                                                    <input type="hidden" name="nationalityOperator" value={{ $nationalityOperator }} >
                                                    <input type="hidden" name="nationality" value={{ $nationality }} >
                                                    <input type="hidden" name="fileName" value={{ $fileName.' - Fee Confirmed Male ' }} >
                                                    <button type="submit">
                                                        <i class="icon-cloud-download2"></i>
                                                    </button>
                                                </form>
                                            @else
                                            {{'N/A'}}
                                            @endif
                                        </td>
                                        <td>
                                            @if ( $currentUserGenderPermission == $bothMaleAndFemale || $currentUserGenderPermission == $female )
                                            {{App\Model\Application::where('fkProgramId', $programId)->getStatusByGender('>=', '4', '2', $nationalityOperator, $nationality)->count()}}

                                                <form class="reports-form" action={{route('postCreateCsvForProgramsGeneral')}} method="POST">
                                                    <input type="hidden" name="programId" value={{$programId}}>
                                                    <input type="hidden" name="operator" value={{ '>=' }}>
                                                    <input type="hidden" name="status" value={{ '4' }}>
                                                    <input type="hidden" name="gender" value={{ '2' }}>
                                                    <input type="hidden" name="nationalityOperator" value={{ $nationalityOperator }} >
                                                    <input type="hidden" name="nationality" value={{ $nationality }} >
                                                    <input type="hidden" name="fileName" value={{ $fileName.' - Fee Confirmed Female ' }} >
                                                    <button type="submit">
                                                        <i class="icon-cloud-download2"></i>
                                                    </button>
                                                </form>
                                            @else
                                            {{'N/A'}}
                                            @endif
                                        </td>
                                        @endif

                                        <td>
                                            @if ( $currentUserGenderPermission == $bothMaleAndFemale || $currentUserGenderPermission == $male )
                                            {{App\Model\Application::where('fkProgramId', $programId)->getStatusByGender('=', '5', '3', $nationalityOperator, $nationality)->count()}}
                                           
                                                <form class="reports-form" action={{route('postCreateCsvForProgramsGeneral')}} method="POST">
                                                    <input type="hidden" name="programId" value={{$programId}}>
                                                    <input type="hidden" name="operator" value={{ '=' }}>
                                                    <input type="hidden" name="status" value={{ '5' }}>
                                                    <input type="hidden" name="gender" value={{ '3' }}>
                                                    <input type="hidden" name="nationalityOperator" value={{ $nationalityOperator }} >
                                                    <input type="hidden" name="nationality" value={{ $nationality }} >
                                                    <input type="hidden" name="fileName" value={{ $fileName.' - Documents Verified Male ' }} >
                                                    <button type="submit">
                                                        <i class="icon-cloud-download2"></i>
                                                    </button>
                                                </form>
                                            @else
                                            {{'N/A'}}
                                            @endif
                                        </td>
                                        <td>
                                            @if ( $currentUserGenderPermission == $bothMaleAndFemale || $currentUserGenderPermission == $female )
                                            {{App\Model\Application::where('fkProgramId', $programId)->getStatusByGender('=', '5', '2', $nationalityOperator, $nationality)->count()}}
                                            
                                                <form class="reports-form" action={{route('postCreateCsvForProgramsGeneral')}} method="POST">
                                                    <input type="hidden" name="programId" value={{$programId}}>
                                                    <input type="hidden" name="operator" value={{ '=' }}>
                                                    <input type="hidden" name="status" value={{ '5' }}>
                                                    <input type="hidden" name="gender" value={{ '2' }}>
                                                    <input type="hidden" name="nationalityOperator" value={{ $nationalityOperator }} >
                                                    <input type="hidden" name="nationality" value={{ $nationality }} >
                                                    <input type="hidden" name="fileName" value={{ $fileName.' - Documents Verified Female ' }} >
                                                    <button type="submit">
                                                        <i class="icon-cloud-download2"></i>
                                                    </button>
                                                </form>
                                            @else
                                            {{'N/A'}}
                                            @endif
                                        </td>
                                        @if(session('gid') != '9')
                                        <td>
                                            @if ( $currentUserGenderPermission == $bothMaleAndFemale || $currentUserGenderPermission == $male )
                                            {{App\Model\Application::where('fkProgramId', $programId)->getStatusByGender('=', '6', '3', $nationalityOperator, $nationality)->count()}}
                                            
                                            <form class="reports-form" action={{route('postCreateCsvForProgramsGeneral')}} method="POST">
                                                    <input type="hidden" name="programId" value={{$programId}}>
                                                    <input type="hidden" name="operator" value={{ '=' }}>
                                                    <input type="hidden" name="status" value={{ '6' }}>
                                                    <input type="hidden" name="gender" value={{ '3' }}>
                                                    <input type="hidden" name="nationalityOperator" value={{ $nationalityOperator }} >
                                                    <input type="hidden" name="nationality" value={{ $nationality }} >
                                                    <input type="hidden" name="fileName" value={{ $fileName.' - Documents Rejected Male ' }} >
                                                    <button type="submit">
                                                        <i class="icon-cloud-download2"></i>
                                                    </button>
                                                </form>
                                            @else
                                            {{'N/A'}}
                                            @endif
                                        </td>

                                        <td>
                                            @if ( $currentUserGenderPermission == $bothMaleAndFemale || $currentUserGenderPermission == $female )
                                            {{App\Model\Application::where('fkProgramId', $programId)->getStatusByGender('=', '6', '2', $nationalityOperator, $nationality)->count()}}
                                            
                                                <form class="reports-form" action={{route('postCreateCsvForProgramsGeneral')}} method="POST">
                                                    <input type="hidden" name="programId" value={{$programId}}>
                                                    <input type="hidden" name="operator" value={{ '=' }}>
                                                    <input type="hidden" name="status" value={{ '6' }}>
                                                    <input type="hidden" name="gender" value={{ '2' }}>
                                                    <input type="hidden" name="nationalityOperator" value={{ $nationalityOperator }} >
                                                    <input type="hidden" name="nationality" value={{ $nationality }} >
                                                    <input type="hidden" name="fileName" value={{ $fileName.' - Documents Rejected Female ' }} >
                                                    <button type="submit">
                                                        <i class="icon-cloud-download2"></i>
                                                    </button>
                                                </form>
                                            @else
                                            {{'N/A'}}
                                            @endif
                                        </td>
                                        @endif
                                    </tr>
                                </tbody>
                            </table>
                            </div>
                        @endforeach
                        @endif
                        {{-- Ending Program --}}
                    @endforeach
                @endif
                @endif
            </div>
        </div>
    <script>
        $(function(){
            document.getElementById('faculty').value = '{{ request('faculty') }}';
            document.getElementById('nationality-list').value = '{{ request('overseas') }}';
            $('#faculty').on('change', function(){
                console.log ( 'changed');
               $.ajax({
                   method: 'post',
                   url: '{{ route('getDepartments') }}',
                   data: {
                       id: $(this).val()
                   },
                   success: function ( response ) {
                       $('#department').empty();
                       $('#department').append(
                            '<option value="">--Select Department--</option>'
                        );
                       $.each(response, function (key, value) {
                           $('#department').append(
                               '<option value="'+value.pkDeptId+'">'+ value.title+'</option>'
                           );
                       });
                   }
               });
            });

            $('#department').on('change', function(){
                $.ajax({
                    method: 'post',
                    url: '{{ route('getProgramme') }}',
                    data: {
                        id: $(this).val()
                    },
                    success: function (response) {
                        $('#programme').empty();
                        if ( response == '' ) {
                            $('#programme').append(
                                '<option value="">--Select Porgramme--</option>'
                            );
                            return;
                        }
                        $('#programme').append(
                            '<option value="">--Select Porgramme--</option>'
                        );
                        $.each(response, function (key, value) {
                            $('#programme').append(
                                '<option value="'+value.pkProgId+'">'+ value.title+ ' - ' + value.duration + 'Year(s)</option>'
                            );
                        });
                    }
                });
            });
        });
    </script>
@stop
