@extends('layouts.app')
@section('content')
<div class="page-content row">
    <div class="page-content-wrapper m-t">
        <div class="sbox">
            <div class="sbox-title">
                <h1> Previus Stats Structure</h1>
            </div> <!-- Ending sbox-title --> 
            <div class="sbox-content "> 
                <div class="row">
                    <div class="col-md-12">
                        @if ( $status )
                            <div class="alert alert-success" role="alert"> {{ $status }}</div>
                        @endif
                        <form action="{{route('add-previous')}}" method="POST" class="quick-edit-form">
                            {{csrf_field()}}
                            <div class="row">
                                <div class="col-md-6">
                                        <div class="form-group">
                                                <label for="name">Program</label> 
                                                <select name="program" class="form-control select2">
                                                    @foreach ( $program as $program )
                                                    <option value="{{$program->pkProgId}}">{{ $program->title }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                </div>
                                <div class="col-md-6">
                                        <div class="form-group">
                                                <label for="name">Semester</label> 
                                                <select name="semester" class="form-control select2">
                                                    @foreach ( $semester as $semester )
                                                    <option value="{{$semester->pkSemesterId}}">{{ $semester->title }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                       
                                </div>
                            </div>

                            <div class="row">
                                    <div class="col-md-6">
                                            <div class="form-group">
                                                    <label for="name">Applied Male</label>
                                                    <input type="number" name="applied_male" class="form-control" value=""/>
                                                </div>
                                            
                                    </div>
                                    <div class="col-md-6">
                                            <div class="form-group">
                                                    <label for="name">Applied Female</label>
                                                    <input type="number" name="applied_female" class="form-control" value=""/>
                                                </div>
                                           
                                    </div>
                            </div>
                            <div class="row">
                                    <div class="col-md-6">
                                            <div class="form-group">
                                                    <label for="name">Fee Confirmed Male</label>
                                                    <input type="number" name="fee_confirmed_male" class="form-control" value=""/>
                                            </div>
                                            
                                    </div>
                                    <div class="col-md-6">
                                            <div class="form-group">
                                                    <label for="name">Fee Confirmed Female</label>
                                                    <input type="number" name="fee_confirmed_female" class="form-control" value=""/>
                                            </div>
                                           
                                    </div>
                            </div>    
                            <div class="row">
                                <div class="col-md-6">
                                        <div class="form-group">
                                                <label for="name">Selected Male</label>
                                                <input type="number" name="selected_male" class="form-control" value=""/>
                                        </div>
                                        
                                </div>
                                <div class="col-md-6">
                                        <div class="form-group">
                                                <label for="name">Selected Female</label>
                                                <input type="number" name="selected_female" class="form-control" value=""/>
                                        </div>
                                        
                                </div>
                            </div>
                            <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                                <label for="name">Joined Male</label>
                                                <input type="number" name="joined_male" class="form-control" value=""/>
                                        </div>
                                        
                                    </div>
                                    <div class="col-md-6">
                                            <div class="form-group">
                                                    <label for="name">Joined Female</label>
                                                    <input type="number" name="joined_female" class="form-control" value=""/>
                                            </div>
                                    </div>
                            </div>
                            <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="name">Applied Total</label>
                                            <input type="number" name="applied_total" class="form-control" value=""/>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                            <div class="form-group">
                                                    <label for="name">Fee Confirmed Total</label>
                                                    <input type="number" name="fee_confirmed_total" class="form-control" value=""/>
                                                </div>
                                    </div>
                            </div>
                            <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="name">Selected Total</label>
                                            <input type="number" name="selected_total" class="form-control" value=""/>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                            <div class="form-group">
                                                    <label for="name">Joined Total</label>
                                                    <input type="number" name="joined_total" class="form-control" value=""/>
                                                </div>
                                    </div>
                            </div>
                            
                            
                           
                           
                            
                            
                            
                            <div class="form-group">
                                <input type="submit" name="submit" class="btn btn-primary" value="Save Changes"/>
                            </div>

                        </form>
                    </div>
                </div> <!-- Ending row --> 
            </div> <!-- Ending sbox-content - 18 --> 
        </div> <!-- Ending sbox - 14 --> 
    </div> <!-- Ending page-content-wrapper --> 
</div> <!-- Ending page-content --> 
@stop