@extends('layouts.app')
@section('content')
<div class="page-content row">
    <div class="page-content-wrapper m-t">
        <h3>Course Audit Form</h3>
        <div class="row">
            <form action="{{ route('googleCourseAudit') }}" method="POST" class="form-inline">
                {{ csrf_field() }}
            <input type="hidden" name="courseId" value="{{request('courseId')}}">
                <input type="hidden" name="teacherId" value="{{request('teacherId')}}">
            <br>
            <table class="table table-bordered audit-report">
                <thead class="table-head">
                    <tr>
                        <th>No#</th>
                        <th>Statement</th>
                        <th>Feedback</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>1</td>
                        <td>A printable syllabus is available to learners (MS WORD, PDF, HTML)</td>
                        <td>
                            <label class="radio-inline"><input type="radio" name="q1" @if(isset($audit)){{($audit->q1 == 10)? 'checked="checked"' : ''}} @endif value="10" required>Sufficiently Present</label>
                            <label class="radio-inline"><input type="radio" name="q1" @if(isset($audit)){{($audit->q1 == 5)? 'checked="checked"' : ''}} @endif value="5" required>Revision Required</label>
                            <label class="radio-inline"><input type="radio" name="q1" @if(isset($audit)){{($audit->q1 == 0)? 'checked="checked"' : ''}} @endif value="0" required>Not Applicable</label>
                        </td>
                    </tr>
                    <tr>
                        <td>2</td>
                        <td>Course objectives/outcomes are clearly defined in the course outline.</td>
                        <td>
                            <label class="radio-inline"><input type="radio" name="q2" value="10" @if(isset($audit)){{($audit->q2 == 10)? 'checked="checked"' : ''}} @endif required>Sufficiently Present</label>
                            <label class="radio-inline"><input type="radio" name="q2" value="5" @if(isset($audit)){{($audit->q2 == 5)? 'checked="checked"' : ''}} @endif required>Revision Required</label>
                            <label class="radio-inline"><input type="radio" name="q2" value="0" @if(isset($audit)){{($audit->q2 == 0)? 'checked="checked"' : ''}} @endif required>Not Applicable</label>
                        </td>
                    </tr>
                    <tr>
                        <td>3</td>
                        <td>Where available, Open Educational Resources, free, or low-cost materials are used</td>
                        <td>
                            <label class="radio-inline"><input type="radio" name="q3" value="10" @if(isset($audit)){{($audit->q3 == 10)? 'checked="checked"' : ''}} @endif required>Sufficiently Present</label>
                            <label class="radio-inline"><input type="radio" name="q3" value="5" @if(isset($audit)){{($audit->q3 == 5)? 'checked="checked"' : ''}} @endif required>Revision Required</label>
                            <label class="radio-inline"><input type="radio" name="q3" value="0" @if(isset($audit)){{($audit->q3 == 0)? 'checked="checked"' : ''}} @endif required>Not Applicable</label>
                        </td>
                    </tr>
                    <tr>
                        <td>4</td>
                        <td>Expectations for interaction (grade weighting, and timings) and regular feedback from the instructor are clearly stated either in the instructions, assignments, quizzes, etc</td>
                        <td>
                            <label class="radio-inline"><input type="radio" name="q4" value="10" @if(isset($audit)){{($audit->q4 == 10)? 'checked="checked"' : ''}} @endif required>Sufficiently Present</label>
                            <label class="radio-inline"><input type="radio" name="q4" value="5" @if(isset($audit)){{($audit->q4 == 5)? 'checked="checked"' : ''}} @endif required>Revision Required</label>
                            <label class="radio-inline"><input type="radio" name="q4" value="0" @if(isset($audit)){{($audit->q4 == 0)? 'checked="checked"' : ''}} @endif required>Not Applicable</label>
                        </td>
                    </tr>
                    <tr>
                        <td>5</td>
                        <td>A clear delineation of course grading policies, i.e, rubrics, as well as the consequences of the late submissions, must be given in the course information area or syllabus</td>
                        <td>
                            <label class="radio-inline"><input type="radio" name="q5" value="10" @if(isset($audit)){{($audit->q5 == 10)? 'checked="checked"' : ''}} @endif required>Sufficiently Present</label>
                            <label class="radio-inline"><input type="radio" name="q5" value="5" @if(isset($audit)){{($audit->q5 == 5)? 'checked="checked"' : ''}} @endif required>Revision Required</label>
                            <label class="radio-inline"><input type="radio" name="q5" value="0" @if(isset($audit)){{($audit->q5 == 0)? 'checked="checked"' : ''}} @endif required>Not Applicable</label>
                        </td>
                    </tr>        
                    <tr>
                        <td>6</td>
                        <td>Number of Weeks Present &nbsp;<input type="number" min="0" name="no_of_weeks" step="any" value="@if(isset($audit)){{$audit->no_of_weeks}}@endif" required="required"></td>
                        <td>
                            <label class="radio-inline"><input type="radio" name="q6" value="10" @if(isset($audit)){{($audit->q6 == 10)? 'checked="checked"' : ''}} @endif required>Sufficiently Present</label>
                            <label class="radio-inline"><input type="radio" name="q6" value="5" @if(isset($audit)){{($audit->q6 == 5)? 'checked="checked"' : ''}} @endif required>Revision Required</label>
                            <label class="radio-inline"><input type="radio" name="q6" value="0" @if(isset($audit)){{($audit->q6 == 0)? 'checked="checked"' : ''}} @endif required>Not Applicable</label>
                        </td>
                    </tr>        
                    <tr>
                        <td>7</td>
                        <td>Lecture Notes / Slides Present</td>
                        <td>
                            <label class="radio-inline"><input type="radio" name="q7" value="10" @if(isset($audit)){{($audit->q7 == 10)? 'checked="checked"' : ''}} @endif required>Sufficiently Present</label>
                            <label class="radio-inline"><input type="radio" name="q7" value="5" @if(isset($audit)){{($audit->q7 == 5)? 'checked="checked"' : ''}} @endif required>Revision Required</label>
                            <label class="radio-inline"><input type="radio" name="q7" value="0" @if(isset($audit)){{($audit->q7 == 0)? 'checked="checked"' : ''}} @endif required>Not Applicable</label>
                        </td>
                    </tr>        
                    <tr>
                        <td>8</td>
                        <td>Reference Material Present</td>
                        <td>
                            <label class="radio-inline"><input type="radio" name="q8" value="10" @if(isset($audit)){{($audit->q8 == 10)? 'checked="checked"' : ''}} @endif required>Sufficiently Present</label>
                            <label class="radio-inline"><input type="radio" name="q8" value="5" @if(isset($audit)){{($audit->q8 == 5)? 'checked="checked"' : ''}} @endif required>Revision Required</label>
                            <label class="radio-inline"><input type="radio" name="q8" value="0" @if(isset($audit)){{($audit->q8 == 0)? 'checked="checked"' : ''}} @endif required>Not Applicable</label>
                        </td>
                    </tr>        
                    <tr>
                        <td>9</td>
                        <td>Lecture Recordings Present</td>
                        <td>
                            <label class="radio-inline"><input type="radio" name="q9" value="10" @if(isset($audit)){{($audit->q9 == 10)? 'checked="checked"' : ''}} @endif required>Sufficiently Present</label>
                            <label class="radio-inline"><input type="radio" name="q9" value="5" @if(isset($audit)){{($audit->q9 == 5)? 'checked="checked"' : ''}} @endif required>Revision Required</label>
                            <label class="radio-inline"><input type="radio" name="q9" value="0" @if(isset($audit)){{($audit->q9 == 0)? 'checked="checked"' : ''}} @endif required>Not Applicable</label>
                        </td>
                    </tr>        
                    <tr>
                        <td>10</td>
                        <td>Student Activity Present</td>
                        <td>
                            <label class="radio-inline"><input type="radio" name="q10" value="10" @if(isset($audit)){{($audit->q10 == 10)? 'checked="checked"' : ''}} @endif required>Sufficiently Present</label>
                            <label class="radio-inline"><input type="radio" name="q10" value="5" @if(isset($audit)){{($audit->q10 == 5)? 'checked="checked"' : ''}} @endif required>Revision Required</label>
                            <label class="radio-inline"><input type="radio" name="q10" value="0" @if(isset($audit)){{($audit->q10 == 0)? 'checked="checked"' : ''}} @endif required>Not Applicable</label>
                        </td>
                    </tr>                                        
                </tbody>
            </table>
            <h5>Comments</h5>
            <textarea id="comments" name="comments" rows="3" cols="132">
                @if(isset($audit)){{$audit->comments}}@endif
            </textarea>
            <br />
            <input type="submit" value="Save" class="btn btn-primary">
            </form>
        </div>
    </div>
</div>
@endsection

