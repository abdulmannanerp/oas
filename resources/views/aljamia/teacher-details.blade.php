@extends('layouts.app')

@section('content')
<div class="page-content row">
    <div class="page-content-wrapper m-t">
        <h3>{{ $teacher->name }}</h3>
        <div class="row">
            {{-- {{ dd($teacher->courses )}} --}}
            <table class="table table-bordered">
                <thead class="table-head">
                    <tr>
                        <th> Aljamia Id</th>
                        <th> Full Name</th>
                        <th> Official Email </th>
                        <th> Secondary Email </th>
                        <th> CNIC </th>
                        <th> Mobile </th>
                        <th> Status </th>
                        <th> Gender </th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td> {{ $teacher->teacher_id_aljamia }} </td>
                        <td> {{ $teacher->name }} </td>
                        <td> {{ $teacher->email }} </td>
                        <td> {{ $teacher->other_email }} </td>
                        <td> {{ $teacher->cnic }} </td>
                        <td> {{ $teacher->mobile }} </td>
                        <td> {{ $teacher->status }} </td>
                        <td> {{ $teacher->gender }} </td>
                    </tr>
                </tbody>
            </table>
            @if ( $teacher->courses )
                <h3> Courses </h3>  
                <ul>
                    @foreach ($teacher->courses as $course )
                    <li> 
                        {{ $course->coursecode }} &mdash;
                        {{ $course->title }}
                        <a href="{{ route('alJamiaCompleteCourseInfo', ['id' => $course->id, 'courseCode' => $course->coursecode])}}" target="_blank">
                            <i title="complete info" class="fa fa-info-circle"></i>
                        </a>
                    </li>
                    @endforeach
                </ul>
            @endif
        </div>
    </div>
</div>
@endsection
@section('scripts')
   
@endsection
