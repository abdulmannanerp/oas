@extends('layouts.app')

@section('content')
<div class="page-content row">
    <div class="page-content-wrapper m-t">
        <h3>Course Listing Report</h3>
        <div class="row">
            <form action="{{ route('courseListingReport') }}" method="POST" class="form-inline">
                {{ csrf_field() }}
                <select name="faculty" id="facluty" class="form-control">
                    <option value="">-- Select Faculty -- </option>
                    @foreach ( $faculties as $faculty )
                        <option value="{{ $faculty->faccode }}" {{ ($faculty->faccode == $selectedFaculty) ? 'selected' : '' }}>{{ $faculty->facname }}</option>
                    @endforeach
                </select>
                <select name="department" id="department" class="form-control">
                    <option value="">-- Select Department --</option>
                    @if( $departments )
                        @foreach ($departments as $department)
                            <option value="{{$department->depcode}}" {{($department->depcode == $selectedDepartment ? 'selected' : '')}}>{{ $department->depname }} </option>
                        @endforeach
                    @endif
                </select>
                <select name="program" id="program" class="form-control">
                    <option value="">-- Select Program --</option>
                    @if ($programs)
                        @foreach ($programs as $program)
                            <option value="{{$program->acadprogcode}}" {{($program->acadprogcode == $selectedProgram) ? 'selected' : ''}}>{{ $program->acadprogname }} </option>
                        @endforeach
                    @endif
                </select>
                <select name="batch" id="batch" class="form-control">
                    <option value="">-- Select Batch --</option>
                    @if( $batches )
                        @foreach ( $batches as $batch )
                            <option value="{{ $batch->batchcode }}" {{ ($batch->batchcode == $selectedBatch) ? 'selected' : '' }}>{{ $batch->batchname }} </option>
                        @endforeach
                    @endif
                </select>
                <input type="submit" value="Get Report" class="btn btn-primary">
            </form>
            <br>
            @isset($courses->courses)
            Total: {{ $courses->courses->count() }}
            <table class="table table-bordered">
                <thead class="table-head">
                    <tr>
                        <th>Course Code</th>
                        <th>Course Title</th>
                        <th>Section</th>
                        <th>Teacher Name</th>
                        <th>Drive/Class Url</th>
                        <th>Action</th>
                    </tr>
                </thead>
                @foreach ($courses->courses as $course)
                    <tr>
                        <td> {{ $course->coursecode ?? '' }}</td>
                        <td> 
                            {{ $course->title ?? '' }}
                            <a href="{{ route('alJamiaCompleteCourseInfo', ['id' => $course->id, 'courseCode' => $course->coursecode])}}" target="_blank">
                                <i title="complete info" class="fa fa-info-circle"></i>
                            </a>
                        </td>
                        <th> {{ $course->section ?? '' }} </th>
                        <th> 
                            {{ $course->teacher->name ?? '' }} 
                            <a href="{{route('alJamiaCompleteTeacherInfo', [
                                'id' => $course->teacher->id,
                                // 'aljamiaId' => $course->teacher->teacher_id_aljamia
                                ])}}" target="_blank">
                                <i title="complete info" class="fa fa-info-circle"></i>
                            </a>
                        </th>
                        <th> 
                            <a href="{{ $course->g_drive_url }}" target="_blank">
                                {{-- <i class="fa fa-cloud"></i> --}}
                                <img src="https://img.icons8.com/color/24/000000/google-drive.png" title="Google Drive"/>
                            </a> &mdash;
                            <a href="{{ $course->g_course_url }}" target="_blank">
                                {{-- <i class="fa fa-google"></i> --}}
                                <img src="https://img.icons8.com/color/24/000000/google-classroom.png" title="Google Class"/>
                            </a>
                        </th>
                        <th>
                                {{-- audit team --}}
                                <a href="{{ route('googleCourseAudit', [
                                    'courseId' => $course->id,
                                    'teacherId'=> $course->teacher_id
                                ]) }}" class="btn btn-primary btn-sm">
                                    Verify
                                </a>
                                <a href="#" class="btn btn-primary btn-sm">Approve</a>
                            @if ($course->g_class_created)
                                {{-- <a href="#" class="btn btn-primary btn-sm">Activate</a> --}}
                                <a href="{{ route('googleUpdateCourseStatus', [
                                    'id' => $course->g_class_id,
                                    'status' => 'PROVISIONED',
                                    'courseId' => $course->id
                                ]) }}" class="btn btn-warning btn-sm">Suspend</a>
                                <a href="{{ route('googleDeleteCourse', [
                                    'id' => $course->g_class_id,
                                    'courseId' => $course->id
                                ]) }}" class="btn btn-danger btn-sm">Delete</a>
                            @else
                                <a href="{{ route('googleCourseCreate', [
                                    'name' => $course->title,
                                    'section' => $course->alias(),
                                    'courseState' => 'ACTIVE',
                                    'alias' => $course->alias(),
                                    'courseId' => $course->id
                                    ]) }}" class="btn btn-primary btn-sm">Create</a>
                            @endif
                        </th>
                    </tr>
                @endforeach
            </table>
            @endif
        </div>
    </div>
</div>
@endsection
@section('scripts')
    <script>
        $('#facluty').on('change', function() {
            $this = $(this);
            $.ajax({
                url: '{{route('getAljamiaDepartments')}}',
                data: {
                    id: $this.val()
                },
                success: function (response) {
                    $('#department').empty();
                    if (response) {
                        let departments = '<option value="">-- Select Department --</option>';
                        $.each(response, function (key, value) {
                            departments += `<option value= ${value.depcode}> ${value.depname} </option>`;
                        });
                        $('#department')
                            .empty()
                            .append(departments);
                    }
                }
            });
        });

        $('#department').on('change', function() {
            $this = $(this);
            $.ajax({
                url: '{{route('getAljamiaPrograms')}}',
                data: {
                    id: $this.val()
                },
                success: function (response) {
                    $('#program').empty();
                    if (response) {
                        let programs = '<option value="">-- Select Program --</option>';
                        $.each(response, function (key, value) {
                            programs += `<option value= ${value.acadprogcode}> ${value.acadprogname} </option>`;
                        });
                        $('#program')
                            .empty()
                            .append(programs);
                    }
                }
            });
        });

        $('#program').on('change', function() {
            $this = $(this);
            $.ajax({
                url: '{{route('getAljamiaBatches')}}',
                data: {
                    id: $this.val()
                },
                success: function (response) {
                    $('#batch').empty();
                    if (response) {
                        let programs = '<option value="">-- Select Batch --</option>';
                        $.each(response, function (key, value) {
                            programs += `<option value= ${value.batchcode}> ${value.batchname} </option>`;
                        });
                        $('#batch')
                            .empty()
                            .append(programs);
                    }
                }
            });
        });
    </script>
@endsection
