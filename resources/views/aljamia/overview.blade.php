@extends('layouts.app')

@section('content')
<div class="page-content row">
    <div class="page-content-wrapper m-t">
        <h3>Course Dasahboard</h3>
        <div class="row">
            <div class="col-md-6">
                <div class="panel panel-success-alt noborder">
                    <div class="panel-heading noborder print-clr-1">
                        <div class="panel-icon">
                            <i class="fa fa-flask"></i>
                        </div>
                        <div class="media-body">
                            <h5 class="md-title nomargin">Total Courses</h5>
                            <h1 class="mt5">07191</h1>
                        </div>
                        <hr>
                        <div class="clearfix mt20">
                            Active Courses:
                            <br> 
                            Inactive Course
                        </div>
                    </div> <!--Ending panel-heading -->
                </div><!-- Endign Panel -->    
            </div> <!-- Ending col -->

            <div class="col-md-6">
                <div class="panel panel-primary noborder">
                    <div class="panel-heading noborder print-clr-1">
                        <div class="panel-icon">
                            <i class="fa fa-user-circle"></i>
                        </div>
                        <div class="media-body">
                            <h5 class="md-title nomargin">Total Teachers</h5>
                            <h1 class="mt5">07191</h1>
                        </div>
                        <hr>
                        <div class="clearfix mt20">
                            Active Courses:
                            <br> 
                            Inactive Course
                        </div>
                    </div> <!--Ending panel-heading -->
                </div><!-- Endign Panel -->    
            </div> <!-- Ending col -->
        </div>
    </div>
</div>
@endsection
@section('scripts')
   
@endsection
