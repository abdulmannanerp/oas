@extends('layouts.app')

@section('content')
<div class="page-content row">
    <div class="page-content-wrapper m-t">
        <h3>Course Details</h3>
        <div class="row">
            <table class="table table-bordered">
                <tbody>
                    <tr>
                        <td>Id</td>
                        <td> {{ $course->id }} </td>
                    </tr>
                    <tr>
                        <td>Course Teacher</td>
                        <td>
                            @if ($course->teacher )
                                {{ $course->teacher->name }}
                                <a href="{{ route('alJamiaCompleteTeacherInfo', ['id' => $course->teacher->id])}}" target="_blank">
                                    <i title="complete info" class="fa fa-info-circle"></i>
                                </a>
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <td>Faculty</td>
                        <td> {{ $course->faccode }} </td>
                    </tr>
                    <tr>
                        <td>Department</td>
                        <td> {{ $course->depcode }} </td>
                    </tr>
                    <tr>
                        <td>Program</td>
                        <td> {{ $course->acadprogcode }} </td>
                    </tr>
                    <tr>
                        <td>Batch</td>
                        <td> {{ $course->batchcode }} </td>
                    </tr>
                    <tr>
                        <td>Semester</td>
                        <td> {{ $course->semster }} </td>
                    </tr>
                    <tr>
                        <td>Title</td>
                        <td> {{ $course->title }} </td>
                    </tr>
                    <tr>
                        <td>Section</td>
                        <td> {{ $course->section }} </td>
                    </tr>
                    <tr>
                        <td>Credit Hours</td>
                        <td> {{ $course->credit_hours }} </td>
                    </tr>
                    <tr>
                        <td>Number of regisetered student (Aljamia)</td>
                        <td> {{ $course->no_of_registered_students_aljamia }} </td>
                    </tr>
                    <tr>
                        <td>Google Class Created</td>
                        <td> {{ ($course->g_class_created == 0) ? 'No' : 'Yes' }} </td>
                    </tr>
                    <tr>
                        <td>Google Class Id</td>
                        <td> {{ $course->g_class_id }} </td>
                    </tr>
                    <tr>
                        <td>Number of registered students (GoogleClass)</td>
                        <td> {{ $course->g_registered_students }} </td>
                    </tr>
                    <tr>
                        <td>Enrollment Code</td>
                        <td> {{ $course->g_enrollment_code }} </td>
                    </tr>
                    <tr>
                        <td>Drive Url</td>
                        <td> {{ $course->g_drive_url }} </td>
                    </tr>
                    <tr>
                        <td>Course Url</td>
                        <td> {{ $course->g_course_url }} </td>
                    </tr>
                    <tr>
                        <td>Course Initial Email</td>
                        <td> {{ ($course->initial_email_sent == 0) ? 'No' : 'Yes' }} </td>
                    </tr>
                    <tr>
                        <td>Course Initial Email Viewed</td>
                        <td> {{ ($course->initial_email_viewed == 0) ? 'No' : 'Yes' }} </td>
                    </tr>
                    <tr>
                        <td>Course Initial Email Sent @</td>
                        <td> {{ $course->initial_email_sent_at }} </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
@section('scripts')
   
@endsection
