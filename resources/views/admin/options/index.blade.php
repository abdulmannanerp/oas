@extends('layouts.app')
@section('content')
	<div class="container">
		<section class="page-header row">
			<h2> Options </h2>
		</section>
		<div class="page-content manage-options-section">
			@if ( $optionDeleted )
				<div class="alert alert-danger mt5">
					Option Deleted!
				</div>
			@endif
			<a href="{{route('createOptions')}}" class="btn btn-primary btn-sm mt5 mb5">Create Option</a>
			<table class="table table-hover table-bordered">
				<thead class="table-head">
					<tr>
						<th>Option Title</th>
						<th>Option Value</th>
						<th>Manage</th>
					</tr>
				</thead>
				<tbody>
					@foreach ( $options as $option )
						<tr>
							<td>{{$option->title}}</td>
							<td>{{$option->value}}</td>
							<td>
								<a href="{{route('createOptions', ['id' => $option->id])}}">Edit </a>
								<a href="{{route('deleteOptions', ['id' => $option->id])}}"> | Delete</a>
							</td>
						</tr>
					@endforeach
				</tbody>
			</table>
		</div> <!-- Ending page-content row -->
	</div> <!-- Ending Container -->

	
@endsection