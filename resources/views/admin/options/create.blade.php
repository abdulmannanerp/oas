@extends('layouts.app')
@section('content')
	<div class="container">
		<section class="page-header row">
			<h2> Create/Upate Options </h2>
		</section>
		<div class="mt5 page-content manage-options-section">
			@if ( $status )
				<div class="alert alert-success" role="alert">
					{{ $status}}
				</div>
			@endif
			{{-- <h4>Create Option</h4> --}}
			<a href="{{ route('options')}}" class="btn btn-primary btn-sm mt5 mb5">View Options</a>
			<p>
				<small>Please write title in lower case, words seperated by dashes (-). i.e. <strong>admission-start-date</strong></small>
			</p>
			<form action="{{route('storeOptions')}}" method="POST">
				{{ csrf_field() }}
				<div class="form-group">
					<label for="Option Title">Title</label>
					<input type="text" class="form-control" name="title" value="{{ ($option) ? $option->title : ''}}" required>
				</div>
				<div class="form-group">
					<label for="Option Title">Value</label>
					<input type="text" class="form-control" name="value" value="{{ ($option) ? $option->value : ''}}" required>
					<small>Date format: YYYY-MM-DD</small>
				</div>
				<div class="form-group">
					<input type="submit" value="Save Option" class="btn btn-primary">
				</div>
			</form>
		</div> <!-- Ending page-content row -->
	</div> <!-- Ending Container -->

	
@endsection