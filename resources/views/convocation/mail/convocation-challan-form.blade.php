<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
</head>
<body>
	<p>
		<strong> Dear Applicant, </strong>
	</p>
	<p>
		You application has been submitted successfully. You can print you challan form by visiting the following link.
	</p>
	<p>
		<strong>IMPORTANT INSTRUCTIONS </strong>
	</p>
	<p>
		Student must bring original challan form to collect the invitation card and Robes. Academics copy will be submitted by the student at the time of collection of invitation card. without Challan form Invitation card and robes will not be delivered to the students.
	</p>
	<a href="{{route('generateChallanForm', ['id' => $convocation->id])}}">Challan Form</a>
</body>
</html>