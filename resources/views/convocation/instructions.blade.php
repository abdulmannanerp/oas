@extends('convocation.app')
@section('content')
	<div class="container instructions-container">
		{{-- <h5 class="registration-instructions">Registration will be started soon</h5> --}}
		<a class="register btn btn-success pull-right" href="{{route('register-convocation')}}">Register</a>
		<div class="main-heading">
			<h3> Guidelines & Instructions for Online Registration of</h3>
			<h4>11TH CONVOCATION 2019</h4>
		</div> <!-- Ending main-heading --> 
		<p><strong>Dear Graduate,</strong></p>
		<p>Assalam-o-Alaikum!</p>
		<p>
			Please make sure before start online registration, the graduate must have the following information/documentation with him/her:-
		</p>
		<div class="initial-instructions">
			<ul>
				<li>
					Select correct passing out year (academic session)
				</li>
				<li>
					Full Name of graduate as per Matriculation certificate / Transcript (if received)
				</li>
				<li>
					Graduate CNIC No.
				</li>
				<li>
					Name of the degree program as per mentioned on Transcript (if received)
				</li>
				<li>
					Current residential address
				</li>
				<li>
					If the graduate wants to invite his/her guest to attend the convocation, their CNIC No. must be entered correctly
				</li>
			</ul>
		</div>
		<p>
			For attending the 11th Convocation (2019), graduates who fulfill the following conditions will be eligible for participation.
		</p>
		<div class="eligibility-conditions">
				<ul>
					<li>
						Have graduated during the Academic Year 2015-16, 2016-17 and 2017-18 (from 01.09.2015 to 31.08.2018)
					</li>
					<li>
						In case he/she has obtained Transcript, attested copy must be attached/upload.
					</li>
					<li>
						Deposit Rs. 1000/- (Rupees one thousand only) per degree fee/registration fee (Non refundable).
					</li>
					<li>
						Deposit Rs. 1000/- (Rupees one thousand only) per guest registration fee (Non refundable).
					</li>
					<li>
						Deposit Rs. 2000/- (Rupees Two Thousand Only) security fee for convocation robes. Rs.1500/- is refundable on return of Robes after deduction of Rs. 500/- as rental charges. 
					</li>
					<li>
						Student must bring original challan form to collect the invitation card and Robes. Invitation card will not be given if original challan form not shown.
					</li>
					<li>
						In case you desire that your parents/spouse may be invited to attend the convocation, his/her CNIC No. must be entered correctly. The participation of the guest(s) is subject to the space available in the convention centre.
					</li>
					<li>
						Participation in the convocation by the students and their guest(s) will be subject to the security clearance by the Security Agencies of Pakistan.
					</li>
					<li>
						Convocation robes must be returned after attending the convocation at outside the designated desks of convention hall and security fee may be claimed. 
					</li>
					<li>
						Please bring original Challan form at the time of collecting Gowns & Robs. (date will be announced later) 
					</li>
					<li style="color: red;">
						Those students who have not yet passed Hifz test, are also eligible to participate in the convocation subject to fullfilment of rest of the requirements
					</li>
				</ul>
			</div>	
		<div class="procedure-for-transcript" id="procedure-for-transcript">
			<h4><strong>Procedure for Transcript:</strong></h4>
			<ul>
				<li>
					Obtain Clearance/NOC form first and get it cleared from the offices of the University mentioned in the form.
				</li>
				<li>
					Pass the prescribed Hifz Test and mention the date of passing the Hifz Test for issuance of degree.
				</li>
				{{-- <li>
					Clearance/NOC form duly completed in all respects must be deposited before 6th February 2019; otherwise, you will not be allowed to attend the convocation.
				</li> --}}
			</ul>
		</div>
		<div class="name-and-designation">
			<div class="name">
				<span>Inam-ul-Haq</span>
			</div>
			<div class="designation">
				<span>Director (Examinations) </span>
			</div>
		</div>
		{{-- <div class="footer">
			<p>I shall abide by the terms and conditions as defined by the university.</p>
		</div> --}}
	</div>
@endsection