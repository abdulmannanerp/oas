@extends('layouts.app')


@section('content')
    <div class="page-content row">
        <div class="page-content-wrapper m-t">
            <div class="page-title">
                <h3>Convocation Fee Verification</h3>
            </div>
            @if ( $status ) 
                <div class="alert alert-success mt5 mb5">
                    {{ $status }}
                </div>
            @endif
            <form action="{{route('convocation-verify')}}" method="POST" id="application-numbers-form">
                {{ csrf_field() }}
                <div class="form-group">
                        <label for="">Enter Application Numbers</label>
                        <textarea name="application_ids" id="application-ids-textbox" class="form-control" placeholder="171005,758000,875520" required></textarea>
                </div> <!-- Ending Form Group -->
                <div class="form-group">
                    <input type="submit" class="btn btn-primary" value="List Records" name="list_records">
                </div>
            </form>

            <form action="{{route('convocation-verify')}}" method="POST" enctype="multipart/form-data" id="upload-csv-form">
                <div class="form-group">
                    {{ csrf_field() }}
                    <label for="">Upload CSV</label>
                    <input type="file" name="csv" class="btn btn-default" accept=".csv" required>
                </div>
                <div class="form-group">
                    <input type="submit" class="btn btn-primary" value="Uplaod & List" name="upload_and_list_csv">
                </div>
            </form>

            @if ( session('verifiedApplications') )
                <h4 class="applicatoins-verified-relist">Following applications has been verified</h4>
                <ul>
                @foreach ( session('verifiedApplications') as $app )
                    <li> {{ $app->id }}</li>
                @endforeach
                </ul>
            @endif


            @if ( $applications )
                <div class="table-responsive">
                    <table class="table table-hover table-bordered app-search-grid" id="ManualVerifyTable">
                        <thead class="table-head">
                            <tr>
                                <th>Challan#</th>
                                <th>Name</th>
                                <th>Programme</th>
                                <th>Status</th>
                                <th>Fee</th>
                                <th>Verified By</th>
                                <th>Date</th>
                            </tr>
                        </thead>
                        <tbody>
                        @php $allApplications = []; @endphp
                        @foreach ( $applications as $application )
                            <tr>
                                <td>{{ '19'.$application->id.'19' }}</td>
                                <td>{{ $application->name }}</td>
                                <td>{{ $application->program->title ?? '' }}</td>
                                <td>
                                    @if ( $application->fkCurrentStatus >= 4 )
                                        <span class="fee-already-verified">Fee Already Verified</span>
                                    @else
                                        <span class="fee-not-verified">Unverified</span>
                                    @endif
                                </td>
                                <td> {{ $application->fee ?? '' }} </td>
                                <td> {{ $application->user->first_name ?? '' }} </th>
                                <td>
                                    {{ $application->feeVerified ?? ''}}
                                </td>
                            </tr>
                            @php $allApplications[] = $application->id; @endphp
                        @endforeach
                        </tbody>
                    </table>
                    <form action="{{route('convocation-verify')}}" method="POST">
                        <p class="incomplete-application-verification-message">(Incomplete application will not be verified)</p>
                        <input type="hidden" name="verify" value={{ serialize ( $allApplications) }}>
                        <input type="submit" value="Verify All" class="btn btn-primary">
                    </form>
                </div>
            @endif
            {{-- @if ( $applications_not_exists )
                <h3>Application(s) not found</h3>
                <ol>
                @foreach ( $applications_not_exists as $application)
                     <li> {{ $application }}</li>
                @endforeach
                </ol>
            @endif --}}
        </div>
    </div>
@stop
