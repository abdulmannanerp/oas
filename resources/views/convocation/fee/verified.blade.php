@extends('layouts.app')


@section('content')
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">

    <div class="page-content row">
        <div class="page-content-wrapper m-t">
            <div class="page-title">
                <h3>Convocation Fee Verified</h3>                
            </div>
            <div class="filter-and-export">
                <form action="{{ route('convocation-verified') }}" method="GET" class="varified-page-filter-form">
                    <table>
                        <tr><label for="filter-records">Filter Records:</label></tr>
                        <tr>
                            <td><input type="text" id="date-from" placeholder="Date From" name="start_date" required></td>
                            <td><input type="text" id="date-to" placeholder="Date To" name="end_date" required></td>
                            <td><input type="submit" class="btn btn-primary" value="Search"></td>
                        </tr>
                    </table>
                </form>
                <div class="verified-export-to-csv-btn pull-right">
                    <form action="{{route('convocation-verified')}}" method="POST">
                        <input type="hidden" name="start_date" value={{$startDate ?? ''}}>
                        <input type="hidden" name="end_date" value={{$endDate ?? ''}}>
                        <input type="hidden" name="export" value='export'/>
                        <button type="submit" class="btn btn-primary btn-sm">Export to CSV</button>
                    </form>
                </div>
            </div>
            <div class="table-responsive">
                @if ( $applications->total() ) 
                    <h3>Verified Applications {{ '('.$applications->total().')' ?? '(0)' }}</h3>
                @endif
                
                @if ( count($applications) != 0 )
                
                <table class="table table-hover table-bordered app-search-grid" id="ManualVerifyTable">
                    <thead class="table-head">
                    <tr>
                        <th>Serial No</th>
                        <th>Challan #</th>
                        <th>Name</th>
                        <th class="table-program-column">Programme</th>
                        <th>Application Status</th>
                        <th>Fee</th>
                        <th>Verified By</th>
                        <th>Date</th>
                    </tr>
                    </thead>

                    <tbody>
                    @php $i = $applications->perPage() * ($applications->currentPage() - 1); @endphp
                    @foreach ( $applications as $application)
                        <tr>
                            <td> {{ ++$i }} </td>
                            <td>{{ '19'.$application->id.'19' }}</td>
                            <td class="left-align">{{ $application->name }}</td>
                             <td>	{{ $application->program->title ?? '' }}	</td>
                            <td><span class="fee-already-verified">{{ 'Verified' }}</span></td>   
                            <td>{{ $application->fee ?? '' }} </td>                             
                            <td>{{ $application->user->first_name. ' '. $application->user->last_name }}</td>
                            <td>{{ date ( 'd-M-Y h:i A',  strtotime($application->feeVerified)) }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
                {{ $applications->appends(['start_date' => $startDate, 'end_date' =>  $endDate])->links() }}
            @else
                <h5>There are no application with verified fee</h5>
            @endif
        </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
    <script>
        $('#date-from').flatpickr();
        $('#date-to').flatpickr();
        document.querySelector('#date-from').value = '{{request('start_date')}}';
        document.querySelector('#date-to').value = '{{request('end_date')}}';
    </script>

@stop
