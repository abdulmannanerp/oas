<!DOCTYPE html>
<html lang="en">
<head>
	<title>Convocation - International Islamic University</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="shortcut icon" type="image/png" href="/favicon.png"/>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
	<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
	<link rel="stylesheet" href="{{asset('convocation/css/app.css?ver=1.1')}}">
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-132468320-1"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());

	  gtag('config', 'UA-132468320-1');
	</script>
</head>
<body data-spy="scroll" data-target=".navbar" data-offset="50">
	<nav class="navbar navbar-inverse navbar-fixed-top">
		<div class=" header container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar5">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand active" href="http://iiu.edu.pk"><img style="width: 64px;" src="{{ asset('convocation/images/iiuilogo.png')}}" alt="IIUI">
					<span>11th CONVOCATION, 2019</span>
				</a>
			</div>
			{{-- <div id="navbar5" class="navbar-collapse collapse">
				<ul class="nav navbar-nav navbar-right">
					<li><a href="#studentsDetails">Student Details</a></li>
					<li><a  href="#bankDetails">Bank Details</a></li>
					<li><a  href="#guestDetails">Guest Details</a></li>
				</ul>
			</div> --}}
		</div> <!--/.container-fluid -->
	</nav>
	
	@yield('content')

	<footer class="footer" >
		<div class="container-fluid">
			<div class="row">
				<div class="col-sm-12 col-md-4 for-male-students">
					<div class="for-male-students-label">
						For Male Students
					</div>
					<h3>Mr. Ali Rafey</h3>
					<p>
						Incharge Convocation Registration Desk Room No. 225
					</p>
					<p>
						Admin Block H-10/4 Islamabad.
					</p>
					<p>
						Phone #: 9019546, 9257997
					</p>
					<p>
						Email: ali.raafie@iiu.edu.pk
					</p>
				</div>
				<div class="col-sm-12 col-md-4 for-male-students">
					<div class="for-male-students-label">
						For Male Students
					</div>
					<h3>Syed Naveed Ehtisham</h3>
					<p>
						(Addl. Director Academics)
					</p>
					<p>Admin Block, Sector H-10/4, Islamabad</p>
					<p>
						Phone #: 9019616
					</p>
				</div>
				<div class="col-sm-12 col-md-4 for-female-students">
					<div class="for-female-students-label">
						For Female Students
					</div>
					<h3>Ms. Nosheen Syed</h3>
					<p>
						Deputy Director ( A&E )
					</p>
					<p>
						Phone #: 9019324
					</p>
					<h3>Ms. Madiha Akram</h3>
					<p>
						Deputy Director Exams
					</p>
					<p>Room# S-005, Admin Block H-10/4 Islamabad. Phone# 9019877</p>
					<p>
						Email: madiha.akram@iiu.edu.pk
					</p>
				</div>
			</div>
		</div>
		{{-- <h4>
			<div class="footer-copyright text-center" style="padding:20px;">© 2019
			<a href="https://iiu.edu.pk"> International Islamic University, Islamabad, Pakistan</a>
		</h4> --}}
	</footer>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="http://opoloo.github.io/jquery_upload_preview/assets/js/jquery.uploadPreview.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
	<script src="{{asset('convocation/js/app.js?ver=1.1')}}"></script>
</body>
</html>  