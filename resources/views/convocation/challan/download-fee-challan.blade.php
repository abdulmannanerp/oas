<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Fee Challan</title>
	<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
	<link href="{{asset('convocation/css/challan/download-fee-challan.css')}}" rel="stylesheet" type="text/css" media="all">  
  </style>
</head>
<body>
  <div class="container" id="fee-challan-container">
  	<div class="dues-challan">
  		@foreach ( $copies as $copy )
		    <div class="single-column">
		    	<img src="{{asset('convocation/images/challan-header.jpg')}}" class="header-image" alt="IIUI">
		    	<div class="bank-logo-container">
			    	@if ( $convocation->gender == 'Female')
			    		<img src="{{asset('convocation/images/AlliedBank.png')}}" alt="Allied Bank Limited">
			    	@else
			    		<img src="{{asset('convocation/images/Hbl.jpg')}}" alt="Habib Bank Limited">
			    	@endif 
		    	</div>
		    	<div class="account-number">
		    		@if ( $convocation->gender == 'Female' )
		    			<span class="bold ">ABL Account#: 0020000143260150</span>
		    		@else 
		    			<span class="bold ">HBL Account#: 50060006111001</span>
		    		@endif
		    	</div>
				<table class="detail-table">
					<tbody>
						<tr>
							<td class="bold fixed-width">Challan#:</td>
							<td>19{{$convocation->id}}19</td>
						</tr>
						<tr>
							<td class="bold">Session:</td>
							<td>{{ $convocation->academic_session ?? '' }}</td>
						</tr>
						<tr>
							<td class="bold">Issued Date:</td>
							<td>{{ $convocation->created_at->format('d-M-Y') }}</td>
						</tr>
						<tr>
							<td class="bold">Due Date:</td>
							<td class="underline bold">06-Feb-2019</td>
						</tr>
						<tr>
							<td class="bold">Name: </td>
							<td>{{ $convocation->name ?? ''}}</td>
						</tr>
						<tr>
							<td class="bold">Father's Name: </td>
							<td>{{ $convocation->fathers_name ?? ''}}</td>
						</tr>
						<tr>
							<td class="bold">Program: </td>
							<td>{{ $convocation->program->title }} </td>
						</tr>
					</tbody>
				</table>

				<table class="fee-distribution-table">
					<thead>
						<tr>
							<th>Particulars</th>
							<th class="border-left">Amount</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>Registration Fee</td>
							<td class="border-left right-align">{{ $degree_registration_fee }}</td>
						</tr>
						<tr>
							<td>Security Fee</td>
							<td class="border-left right-align">{{ $secruity_fee }}</td>
						</tr>
						<tr>
							<td>Guests Fee</td>
							<td class="border-left right-align">{{ $guests_fee }}</td>
						</tr>
						<tr>
							<td class="bold total-amount">Total Amount (PKR)</td>
							<td class="border-left right-align">
								<span class="bold">{{ $degree_registration_fee + $secruity_fee + $guests_fee }}</span>
							</td>
						</tr>
					</tbody>
				</table>
				<div class="bank-instructions">
					<h4 class="bank-instructions-title">Instructions</h5>
					<ol class="bank-instructions-list">
						<li>Please enter full fee challan# at the time of punching.</li>
						<li>All branches are requested to recieve the challan.</li>
						<li>In case of any queries, Please contact fee section.</li>
					</ol>
				</div>
				<div class="copy-owner">
					<span class="copy">{{ $copy }}</span>
				</div>
			</div> <!-- Ending single-column --> 
		@endforeach 
	</div> <!-- Ending dues challan --> 
  </div> <!-- Ending container --> 
</body>
</html>