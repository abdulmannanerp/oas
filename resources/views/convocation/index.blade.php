@extends('convocation.app')
@section('content')
	<div id="main" class="container">
		@if ($errors->any())
		    <div class="alert alert-danger">
		        <ul>
		            @foreach ($errors->all() as $error)
		                <li>{{ $error }}</li>
		            @endforeach
		        </ul>
		    </div>
		@endif
		<form id="regForm" action="{{ route('postConvocation')}}" method="POST" enctype="multipart/form-data">
		{{ csrf_field() }}
		<fieldset>
			<div class="row">
				@if ( $status ) 
					<div class="alert alert-success">
						{{ $status }}
					</div>
				@endif
				<div id="studentsDetails" class="col-md-12 makeCenter">
					<h3>REGISTRATION FORM</h3>
				</div>
				<div class="col-md-12 makeCenter">
					<h4>
						<strong>(Student Details)</storng>
					</h4>         
				</div>
				<div class="col-md-12">
					<div class="well form-horizontal">
						<div class="row">
							<div class="col-md-9">
								<div class="row form-group">
									<div class="col-md-4">
										<label class="control-label">Academic Session:</label>
									</div>	
									<div class="col-md-8" style="margin-top:7px;">
										<div class="col-md-3 form-radio form-radio-inline">
											<input class="form-radio-input" type="radio" id="session1" name="academic_session" value="2015-16" {{ old('academic_session') == '2015-16' ? 'checked' : '' }} required>
											<label class="form-radio-label" for="session1">2015-16</label>
										</div>
										<div class="col-md-3 form-radio form-radio-inline">
											<input class="form-radio-input"  type="radio" id="session1" name="academic_session" value="2016-17" {{ old('academic_session') == '2016-17' ? 'checked' : '' }} required>
											<label class="form-radio-label" for="session1">2016-17</label>
										</div>
										<div class="col-md-3 form-radio form-radio-inline">
											<input class="form-radio-input" type="radio" id="session1" name="academic_session" value="2017-18" {{ old('academic_session') == '2017-18' ? 'checked' : '' }} required>
											<label class="form-radio-label" for="session1">2017-18</label>
										</div>
									</div>
								</div>

								<div class="row form-group">
									<div class="col-md-4">
										<label class="control-label">Name:</label>
									</div>
									<div class="col-md-8">
										<div class="input-group">
											<span class="input-group-addon">
												<i class="glyphicon glyphicon-user blue"></i>
											</span>
											<input name="name" placeholder="Enter Full Name" class="form-control" type="text" value="{{ old('name') }}" required>
										</div>
									</div>	
								</div>
								<div class="row form-group">
									<div class="col-md-4">
										<label class="control-label">Father's Name:</label>
									</div>
									<div class="col-md-8 ">
										<div class="input-group">
											<span class="input-group-addon">
												<i class="glyphicon glyphicon-user blue"></i>
											</span>
											<input name="fathers_name" placeholder="Enter Father's Name" class="form-control" type="text" value="{{ old('fathers_name') }}" required>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-3">
								<div class="row">
									<div class="col-md-12" id="image-preview">
										<label for="image-upload" id="image-label">Choose Photo</label>
										<input type="file" accept="image/*" name="image" id="image-upload"/ required>
									</div>
								</div>
								<div class="row">
									<span class="warning-message">Max Size 5MB</span>
								</div>
							</div>
						</div>	
						<div class="row form-group">
							<div class="col-md-3">
								<label class="control-label">CNIC/Passport#:</label>
							</div>
							<div class="col-md-6 ">
								<div class="input-group">
									<span class="input-group-addon">
										<i class="glyphicon glyphicon-credit-card blue"></i>
									</span>
									<input name="cnic" placeholder="Enter CNIC/Passport Number" class="form-control" type="text" value="{{ old('cnic') }}" required>
								</div>
							</div>
							{{-- <div class="col-md-3">
								<label class="control-label">(only Digits)</label>
							</div> --}}
						</div>

						<div class="row form-group">
							<div class="col-md-3">
								<label class="control-label">Name Of Degree Program:</label>
							</div>
							<div class="col-md-6 ">
								<div class="input-group">
									<span class="input-group-addon">
										<i class="glyphicon glyphicon-education blue"></i>
									</span>
									<select name="degree" class="form-control select-degree" required>
										<option value="">-- Select Degree Title -- </option>
										@foreach ( $programs as $program )
											<option value="{{ $program->pkProgId }}" {{ ( $program->pkProgId == old('degree') ? 'selected' : '') }}>{{ $program->title . ' - ' . $program->duration . ' Year(s) ' }}</option>
										@endforeach
									</select>
									{{-- <input name="degree" placeholder="Enter degree title" class="form-control" type="text" value="{{ old('degree') }}" required> --}}
								</div>
							</div>
							{{-- <div class="col-md-3">
								<label class="control-label">Completed at IIU</label>
							</div> --}}
						</div>

						<div class="row form-group">
							<div class="col-md-3">
								<label class="control-label">Registration No:</label>
							</div>
							<div class="col-md-6 ">
								<div class="input-group">
									<span class="input-group-addon">
										<i class="glyphicon glyphicon-registration-mark blue"></i>
									</span>
									<input name="registration_number" placeholder="123-FBAS/BSCS/F17" class="form-control" type="text" value="{{ old('registration_number') }}" required>
								</div>
							</div>
						</div>

						<div class="row form-group">
							<div class="col-md-3">
								<label class="control-label">Residential Address:</label>
							</div>
							<div class="col-md-6 ">
								<div class="input-group">
									<span class="input-group-addon">
										<i class="glyphicon glyphicon-map-marker blue"></i>
									</span>
									<textarea name="residential_address" placeholder="Address" class="form-control" type="text" required> {{ old('residential_address') }}</textarea>
								</div>
							</div>
						</div>

						<div class="row form-group">
							<div class="col-md-3">
								<label  class="control-label">Phone:</label>
							</div>
							<div class="col-md-9 ">
								<div class="col-md-1">
									<label class="control-label">Home:</label>
								</div>
								<div class="col-md-3">
									<div class="input-group">
										<span class="input-group-addon">
											<i class="glyphicon glyphicon-earphone blue"></i>
										</span>
										<input name="home_phone" placeholder="051999999" class="form-control" type="number" value="{{ old('home_phone') }}">
									</div>
								</div>
								<div class="col-md-1">
									<label class="control-label">Office:</label>
								</div>
								<div class="col-md-3">
									<div class="input-group">
										<span class="input-group-addon">
											<i class="glyphicon glyphicon-phone-alt blue"></i>
										</span>
										<input name="office_phone" placeholder="0518888888" class="form-control" type="number" value="{{ old('office_phone') }}">
									</div>
								</div> 
								<div class="col-md-1">
									<label class="control-label">Mobile:</label>
								</div>
								<div class="col-md-3">
									<div class="input-group">
										<span class="input-group-addon">
											<i class="glyphicon glyphicon-phone blue"></i>
										</span>
										<input name="mobile_phone" placeholder="033312341234" class="form-control" type="number" value="{{ old('mobile_phone') }}" required>
									</div>
								</div> 
							</div>
						</div>

						<div class="row form-group">
							<div class="col-md-3">
								<label class="control-label">Email:</label>
							</div>
							<div class="col-md-6">
								<div class="input-group">
									<span class="input-group-addon">
										<i class="glyphicon glyphicon-envelope blue"></i>
									</span>
									<input name="email" placeholder="abc@xyz.com" class="form-control" type="email" value="{{ old('email') }}" requried>
								</div>
							</div>
						</div>

						<div class="row form-group">
							<div class="col-md-3">
								<label class="control-label">Gender:</label>
							</div>
							<div class="col-md-9" style="margin-top:7px;">
								<div class="col-md-2 form-radio form-radio-inline">
									<input class="form-radio-input" type="radio" name="gender" value="Male" {{ (old('gender') == 'Male') ? 'Checked' : ''}} required>
									<label class="form-radio-label" for="inlineCheckbox1">Male</label>
								</div>
								<div class="col-md-2 form-radio form-radio-inline">
									<input class="form-radio-input" type="radio" name="gender" value="Female" {{ (old('gender') == 'Female') ? 'Checked' : '' }} required>
									<label class="form-radio-label" for="inlineCheckbox2">Female</label>
								</div>
							</div>		
						</div>

						<div class="row form-group">
							<div class="col-md-3">
								<label class="control-label">Transcript Recieved:</label>
							</div>
							<div class="col-md-9" style="margin-top:7px;">
								<div class="col-md-2 form-radio form-radio-inline">
									<input class="form-radio-input is_transcript_recieved" type="radio" name="is_transcript_recieved" value="Yes" required>
									<label class="form-radio-label" for="inlineCheckbox1">Yes</label>
								</div>
								<div class="col-md-2 form-radio form-radio-inline">
									<input class="form-radio-input is_transcript_recieved" type="radio" name="is_transcript_recieved" value="No" required>
									<label class="form-radio-label" for="inlineCheckbox2">No</label>
								</div>
								<div class="alert-message hidden">
									<a href="{{route('convocation-instructions-page')}}" target="_blank">Procedure for Transcript</a>
								</div>
							</div>		
						</div>

						<div class="row form-group upload-transcript-container hidden">
							<div class="col-md-3">
								<label class="control-label">Upload Transcript</label>
							</div>
							<div class="col-md-4 ">
								<div class="input-group">
									<span class="input-group-addon">
										<i class="glyphicon glyphicon-file blue"></i>
									</span>
									<input name="upload_transcript" id="upload_transcript" accept="image/*" placeholder="upload Certificate" class="form-control" type="file">
								</div>
								<span class="warning-message">Max Size 5MB</span>
							</div>
						</div>

						<div class=" row form-group">
							<div class="col-md-3">
								<label class="control-label">Degree Recieved:</label>
							</div>
							<div class="col-md-9" style="margin-top:7px;">
								<div class="col-md-2 form-radio form-radio-inline">
									<input class="form-radio-input degree-recieved" type="radio" name="is_degree_recieved" value="Yes" required>
									<label class="form-radio-label" for="inlineCheckbox1">Yes</label>
								</div>
								<div class="col-md-2 form-radio form-radio-inline">
									<input class="form-radio-input degree-recieved" type="radio" name="is_degree_recieved" value="No" required>
									<label class="form-radio-label" for="inlineCheckbox2">No</label>
								</div>
							</div>		
						</div>

						<div class="row form-group hidden" id="serial-number-container">
							<div class="col-md-3">
								<label class="control-label">Serial Number</label>
							</div>
							<div class="col-md-4 ">
								<div class="input-group">
									<span class="input-group-addon">
										<i class="glyphicon glyphicon-education blue"></i>
									</span>
									<input name="degree_serial_number" id="degree-serial-number" placeholder="Enter Degree Serial Number" class="form-control" type="number">
								</div>
							</div>
{{-- 							<div id="bankDetails" class="col-md-4">
							</div> --}}
						</div>


						{{-- <div class="row form-group hidden" id="matric_certificate_container">
							<div class="col-md-3">
								<label style="text-align:left" class="control-label">Matric Certificate (Only if degree not received)</label>
							</div>
							<div class="col-md-4 ">
								<div class="input-group">
									<span class="input-group-addon">
										<i class="glyphicon glyphicon-file blue"></i>
									</span>
									<input id="matric_certificate" name="matric_certificate" placeholder="upload Certificate" class="form-control" type="file"></div>
							</div>
						</div> --}}
					</div>
				</div>


				{{-- <div class="col-md-12 makeCenter" >
					<h4><b>(Bank Details)</b></h4>         
				</div>

				<div  class="col-md-12">
					<div class="well form-horizontal">

						<div class=" row form-group">
							<div class="col-md-12">
								<label class="control-label">Registration Fee (Rs. 1,000/-) [Non-refundable]:</label>
							</div>
						</div>

						<div class="row form-group">
							<div class="col-md-3">
								<label class="control-label">Bank Challan No:</label>
							</div>
							<div class="col-md-3">
								<div class="input-group">
									<span class="input-group-addon">
										<i class="glyphicon glyphicon-duplicate blue"></i>
									</span>
									<input name="bank_challan_1" placeholder="Reg Fee Chalallan #" class="form-control" type="number" value="{{ old('bank_challan_1') }}" required>
								</div>
							</div>
							<div class="col-md-1">
								<label class="col-md-3 control-label">Dated:</label>
							</div>
							<div class="col-md-3">
								<div class="input-group">
									<span class="input-group-addon">
										<i class="glyphicon glyphicon-calendar blue"></i>
									</span>
									<input name="bank_challan_1_submission_date"  class="form-control" type="date" value="{{ old('bank_challan_1_submission_date') }}" required>
								</div>
							</div>	
						</div>

						<div class="row form-group">
							<div class="col-md-12">
								<label class="control-label">Robes/Cap Security (Rs. 2,000/-):</label>
							</div>
						</div>

						<div class="row form-group">
							<div class="col-md-3">
								<label class="control-label">Bank Challan No:</label>
							</div>
							<div class="col-md-3">
								<div class="input-group">
									<span class="input-group-addon">
										<i class="glyphicon glyphicon-duplicate blue"></i>
									</span>
									<input name="bank_challan_2" placeholder="Robes/Cap Security Challan #" class="form-control" type="number" value="{{ old('bank_challan_2') }}" required>
								</div>
							</div>
							<div class="col-md-1">
								<label class="control-label">Dated:</label>
							</div>
							<div class="col-md-3">
								<div class="input-group">
									<span class="input-group-addon"><i class="glyphicon glyphicon-calendar blue"></i>
								</span>
								<input name="bank_challan_2_submission_date"  class="form-control" type="date" value="{{ old('bank_challan_2_submission_date') }}" required>
							</div>
							</div>
						</div>

						<div class=" row form-group">
							<div class="col-md-12">
								<label class="control-label">Guest Fee (Rs. 1,000/- per guest) [Non-refundable]:</label>
							</div>
						</div>

						<div class="row form-group">
							<div class="col-md-3">
								<label class="control-label">Bank Challan No:</label>
							</div>
							<div class="col-md-3">
								<div class="input-group">
									<span class="input-group-addon"><i class="glyphicon glyphicon-duplicate blue"></i>
								</span>
								<input name="bank_challan_3" placeholder="Guest Fee Challan #" class="form-control" type="number" value="{{ old('bank_challan_3') }}">
							</div>
							</div>
							<div class="col-md-1">
								<label class="control-label">Dated:</label>
							</div>
							<div class="col-md-3">
								<div class="input-group">
									<span class="input-group-addon">
										<i class="glyphicon glyphicon-calendar blue"></i>
									</span>
									<input name="bank_challan_3_submission_date"  class="form-control"  type="date" value="{{ old('bank_challan_3_submission_date') }}">
								</div>
							</div>						
						</div>
					</div>
				</div> --}}		

				<div class="col-md-12 makeCenter">
					<h4><b>(Guest Details - Max 2 Allowed)</b></h4> 							
					<h5> (Blood Relations Only (Mother/Father/Sister/Brother/Spouse)</h5>
				</div>

				<div class="col-md-12">
					<div class="well form-horizontal">
						<div class="col-md-12 makeCenter">
							<label class="control-label">Guest No: 1</label>
						</div>
						<div class="row form-group">
							<div class="col-md-3">
								<label class="control-label">Name of Guest:</label>
							</div>
							<div class="col-md-6">
								<div class="input-group">
									<span class="input-group-addon"><i class="glyphicon glyphicon-user blue"></i>
								</span>
								<input name="guest_1_name" placeholder="Guest 1 Name" class="form-control guest_1_name"  type="text" value="{{ old('guest_1_name') }}" >
							</div>
							</div>
						</div>

						<div class="row form-group">
							<div class="col-md-3">
								<label class="control-label">CNIC No:</label>
							</div>
							<div class="col-md-6">
								<div class="input-group">
									<span class="input-group-addon">
										<i class="glyphicon glyphicon-credit-card blue"></i>
									</span>
									<input name="guest_1_cnic" placeholder="Guest 1 CNIC" class="form-control guest_1_cnic" type="number" value="{{ old('guest_1_cnic')}}">
								</div>
							</div>
							{{-- <div class="col-md-3">
								<label class="control-label">(Attested copy of CNIC)</label>
							</div> --}}
						</div>

						<div class="row form-group">
							<div class="col-md-3">
								<label class="control-label">Relationship:</label>
							</div>
							<div class="col-md-6">
								<div class="input-group">
									<span class="input-group-addon">
										<i class="glyphicon glyphicon-user blue"></i>
									</span>
									<select name="guest_1_relationship" class="form-control">
										<option value="">--Select Relationship--</option>
										<option value="Mother">Mother</option>
										<option value="Father">Father</option>
										<option value="Sister">Sister</option>
										<option value="Brother">Brother</option>
										<option value="Spouse">Spouse</option>
									</select>
									{{-- <input name="guest_1_relationship" placeholder="Guest 1 Relationsihp" class="form-control" type="text" value="{{ old('guest_1_relationship') }}"> --}}
								</div>
							</div>
						</div>

						<div class="row form-group">
							<div class="col-md-3">
								<label class="control-label">Contact No:</label>
							</div>
							<div class="col-md-6">
								<div class="input-group">
									<span class="input-group-addon">
										<i class="glyphicon glyphicon-earphone blue"></i>
									</span>
									<input name="guest_1_contact" placeholder="Guest 1 Contact #" class="form-control" type="number" value="{{ old('guest_1_contact') }}">
								</div>
							</div>
						</div>

						<div id="guest2" class="col-md-12 makeCenter">
							<label class="control-label">Guest No: 2</label>
						</div>

						<div class="row form-group">
							<div class="col-md-3">
								<label class="control-label">Name of Guest:</label>
							</div>
							<div class="col-md-6">
								<div class="input-group">
									<span class="input-group-addon">
										<i class="glyphicon glyphicon-user blue"></i>
									</span>
									<input name="guest_2_name" placeholder="Guest 2 Name" class="form-control guest_2_name" type="text" value="{{ old('guest_2_name')}}">
								</div>
							</div>
						</div>

						<div class="row form-group">
							<div class="col-md-3">
								<label class="control-label">CNIC No:</label>
							</div>
							<div class="col-md-6">
								<div class="input-group">
									<span class="input-group-addon">
										<i class="glyphicon glyphicon-credit-card blue"></i>
									</span>
									<input name="guest_2_cnic" placeholder="Guest 2 CNIC" class="form-control guest_2_cnic" type="number" value="{{ old('guest_2_cnic')}}">
								</div>
							</div>
							{{-- <div class="col-md-3">
								<label class="control-label">(Attested copy of CNIC)</label>
							</div> --}}
						</div>	

						<div class="row form-group">
							<div class="col-md-3">
								<label class="control-label">Relationship:</label>
							</div>
							<div class="col-md-6">
								<div class="input-group">
									<span class="input-group-addon">
										<i class="glyphicon glyphicon-user blue"></i>
									</span>
									<select name="guest_2_relationship" class="form-control">
										<option value="">--Select Relationship--</option>
										<option value="Mother">Mother</option>
										<option value="Father">Father</option>
										<option value="Sister">Sister</option>
										<option value="Brother">Brother</option>
										<option value="Spouse">Spouse</option>
									</select>
									{{-- <input name="guest_2_relationship" placeholder="Guest 2 Relationship" class="form-control" type="text" value="{{ old('guest_2_relationship') }}"> --}}
								</div>
							</div>
						</div>

						<div class="row form-group">
							<div class="col-md-3">
								<label class="control-label">Contact No:</label>
							</div>
							<div class="col-md-6">
								<div class="input-group">
									<span class="input-group-addon">
										<i class="glyphicon glyphicon-earphone blue"></i>
									</span>
									<input name="guest_2_contact" placeholder="Guest 2 Contact #" class="form-control" type="number" value="{{ old('guest_2_contact')}}">
								</div>
							</div>
						</div>

					</div>
				</div>
				<div class="col-md-12">

					<div class="well col-md-12">
						<label class="control-label">Note.</label>
						<li class="col-md-offset-1">For each degree fee is Rs. 1,000/-.</li>
						<li class="col-md-offset-1">The students of BA/LLB (Hons) Shariah & Law will deposit Rs.2,000/- for two degrees i.e. B.A and L.L.B (Shariah & Law).</li>
						<br>
						<div class="col-md-5">
							<label class="control-label">I shall abide by the 
								<a href="http://convocation.iiu.edu.pk/" target="_blank">terms and conditions.</a>
							</label>

							<input class="form-radio-input" type="radio" id="condAgree" name="Conditions" value="Agree" required="required">
							<label class="form-radio-label" for="condAgree">Agree</label>
						</div>
						<div class="col-md-7">
							<div class="row ">
								{{-- <div class="col-md-2 form-radio form-radio-inline">
									
								</div> --}}
								<div class="col-md-2 ">
									<button type="submit" class="btn btn-primary">Submit</button>
								</div>
							</div>
						</div>	
					</div>
				</div>
			</div>
		</fieldset>
		</form>		
	</div>
@endsection