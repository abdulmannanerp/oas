<div class="m-t" style="padding-top:25px;">	
    <div class="row m-b-lg animated fadeInDown delayp1 text-center">
        <h3> {{ $pageTitle }} <small> {{ $pageNote }} </small></h3>
        <hr />       
    </div>
</div>
<div class="m-t">
	<div class="table-responsive" > 	

		<table class="table table-striped table-bordered" >
			<tbody>	
		
			
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('PkScheduleId', (isset($fields['pkScheduleId']['language'])? $fields['pkScheduleId']['language'] : array())) }}</td>
						<td>{{ $row->pkScheduleId}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Program', (isset($fields['fkProgramId']['language'])? $fields['fkProgramId']['language'] : array())) }}</td>
						<td>{{ SiteHelpers::formatLookUp($row->fkProgramId,'fkProgramId','1:tbl_oas_programme:pkProgId:title') }} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Program Level', (isset($fields['fkLeveliId']['language'])? $fields['fkLeveliId']['language'] : array())) }}</td>
						<td>{{ SiteHelpers::formatLookUp($row->fkLeveliId,'fkLeveliId','1:tbl_oas_prog_level:pkLevelId:programLevel') }} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Gender', (isset($fields['fkGenderId']['language'])? $fields['fkGenderId']['language'] : array())) }}</td>
						<td>{{ SiteHelpers::formatLookUp($row->fkGenderId,'fkGenderId','1:tbl_oas_gender:pkGenderId:gender') }} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Semester', (isset($fields['fkSemesterId']['language'])? $fields['fkSemesterId']['language'] : array())) }}</td>
						<td>{{ SiteHelpers::formatLookUp($row->fkSemesterId,'fkSemesterId','1:tbl_oas_semester:pkSemesterId:title') }} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Title', (isset($fields['title']['language'])? $fields['title']['language'] : array())) }}</td>
						<td>{{ $row->title}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Status', (isset($fields['status']['language'])? $fields['status']['language'] : array())) }}</td>
						<td>{{ SiteHelpers::userStatus($row->status) }} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Last Date', (isset($fields['lastDate']['language'])? $fields['lastDate']['language'] : array())) }}</td>
						<td>{{ date('Y-m-d H:i:s',strtotime($row->lastDate)) }} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Test Date Time', (isset($fields['testDateTime']['language'])? $fields['testDateTime']['language'] : array())) }}</td>
						<td>{{ date('Y-m-d H:i:s',strtotime($row->testDateTime)) }} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Interview DateTime', (isset($fields['interviewDateTime']['language'])? $fields['interviewDateTime']['language'] : array())) }}</td>
						<td>{{ date('Y-m-d H:i:s',strtotime($row->interviewDateTime)) }} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Instructions', (isset($fields['instructions']['language'])? $fields['instructions']['language'] : array())) }}</td>
						<td>{{ $row->instructions}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Modified', (isset($fields['modified']['language'])? $fields['modified']['language'] : array())) }}</td>
						<td>{{ date('',strtotime($row->modified)) }} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Modified By', (isset($fields['modifiedBy']['language'])? $fields['modifiedBy']['language'] : array())) }}</td>
						<td>{{ $row->modifiedBy}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('DateIn', (isset($fields['dateIn']['language'])? $fields['dateIn']['language'] : array())) }}</td>
						<td>{{ date('',strtotime($row->dateIn)) }} </td>
						
					</tr>
						
					<tr>
						<td width='30%' class='label-view text-right'></td>
						<td> <a href="javascript:history.go(-1)" class="btn btn-primary"> Back To Grid <a> </td>
						
					</tr>					
				
			</tbody>	
		</table>   

	 
	
	</div>
</div>	