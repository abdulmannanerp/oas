@extends('layouts.app')

@section('content')
    <div class="page-content row">
        <div class="page-content-wrapper m-t">
            <h4>Documents Verification</h4>
            <form action="{{route('getApplications')}}" method="POST" id="document-verification-from">
                <select name="faculty" id="faculty" required>
                    <option value="">--Select Faculty--</option>
                    @foreach ( $faculties as $faculty )
                        <option value="{{$faculty->pkFacId}}">{{$faculty->title}}</option>
                    @endforeach
                </select>
                <select name="department" id="department" required>
                    <option value="">--Select Department--</option>
                    @if ( $selectedDepartments )
                        @foreach ( $selectedDepartments as $department )
                            <option value={{ $department->pkDeptId}}>{{$department->title}}</option>
                        @endforeach
                    @endif
                </select>
                <select name="programme" id="programme">
                    <option value="">--Select Programme--</option>
                    @if ( $selectedPorgrams )
                        @foreach ( $selectedPorgrams as $program )
                        <option value={{ $program->pkProgId}}>{{$program->title.' - '. $program->duration. ' Year(s)'}}</option>
                        @endforeach
                    @endif
                </select>
                <select name="verification-status" id="unverified-verified-filter" required>
                    <option value="">--Select Status--</option>
                    <option value="unverified">Unverified</option>
                    <option value="verified">Verified</option>
                </select>
                <input type="hidden" id="page_num" name="page_num" value="">

                <input type="submit" value="Submit" id="get-docs-verification-records">
            </form>
            <form action="{{route('export-docs-verification-data')}}" method="POST" id="docs-verification-export" class="clearfix pull-right">
                <input type="hidden" name="faculty" value={{request('faculty')}}>
                <input type="hidden" name="department" value={{request('department')}}>
                <input type="hidden" name="programme" value={{request('programme')}}>
                <input type="hidden" name="verification-status" value={{request('verification-status')}}>
                <button type="submit" class="btn btn-primary">Export To CSV</button>                
            </form>
            @if ( isset ($applications) )

               
 

                
                @isset ( $selectedFilters )
                        <h5 id="current-selected-filters-heading">{{ $selectedFilters['facultyTitle'] .' / '. $selectedFilters['departmentTitle'] .' / '. $selectedFilters['programmeTitle'] }}</h5>
                    @endisset
                    {{-- <div class="total-records">
                        Total  Records {{ count ($applications) }}
                    </div> --}}
                    {{-- Dev Mannan: Code starts  --}}
<div class="table-responsive" style="padding: 0px;">
    <h4>{{$selectedFilters['departmentTitle']}}</h4>
        <table class="table table-hover table-bordered app-search-grid" id="ManualVerifyTable" style="margin-bottom: 0px;">
           <thead class="table-head" style="background-color: #3f98ce;">
              <tr>
                 <th colspan="2">Form Issued</th>
                 <th colspan="2">Fee Pending</th>
                 <th colspan="2">Fee Verified</th>
                 <th colspan="2">Approved</th>
                 <th colspan="2">Rejected</th>
              </tr>
              <tr>
                 <th>Male</th>
                 <th>Female</th>
                 <th>Male</th>
                 <th>Female</th>
                 <th>Male</th>
                 <th>Female</th>
                 <th>Male</th>
                 <th>Female</th>
                 <th>Male</th>
                 <th>Female</th>
              </tr>
           </thead>
           <tbody>
              <tr>
              <td>{{$departmentApplicationsWithMaleApplicants}}</td>
              <td>{{$departmentApplicationsWithFemaleApplicants}}</td>
              <td>{{$applicant['departmentfeePendingMale']}}</td>
              <td>{{$applicant['departmentfeePendingFemale']}}</td>
              <td>{{ $applicant['departmentFeeConfirmedMale']}}</td>
              <td>{{ $applicant['departmentFeeConfirmedFemale']}}</td>
              <td>{{ $applicant['departmentDocumentsVerifiedMale']}}</td>
              <td>{{ $applicant['departmentDocumentsVerifiedFemale']}}</td>
              <td>{{ $applicant['departmentRejectedMale']}}</td>
              <td>{{ $applicant['departmentRejectedFemale']}}</td>
              </tr>
           </tbody>
        </table>
     </div>
     @if($selectedFilters['programmeTitle'])
     <div class="table-responsive" style="padding: 0px;">
        <h4>{{$selectedFilters['programmeTitle']}}</h4>
            <table class="table table-hover table-bordered app-search-grid" id="ManualVerifyTable" style="margin-bottom: 0px;">
               <thead class="table-head" style="background-color: #3f98ce;">
                  <tr>
                     <th colspan="2">Form Issued</th>
                     <th colspan="2">Fee Pending</th>
                     <th colspan="2">Fee Verified</th>
                     <th colspan="2">Approved</th>
                     <th colspan="2">Rejected</th>
                  </tr>
                  <tr>
                     <th>Male</th>
                     <th>Female</th>
                     <th>Male</th>
                     <th>Female</th>
                     <th>Male</th>
                     <th>Female</th>
                     <th>Male</th>
                     <th>Female</th>
                     <th>Male</th>
                     <th>Female</th>
                  </tr>
               </thead>
               <tbody>
                  <tr>
                  <td>{{$programApplicationsWithMaleApplicants}}</td>
                  <td>{{$programApplicationsWithFemaleApplicants}}</td>
                  <td>{{$applicant['feePendingMale']}}</td>
                  <td>{{$applicant['feePendingFemale']}}</td>
                  <td>{{ $applicant['feeConfirmedMale']}}</td>
                  <td>{{ $applicant['feeConfirmedFemale']}}</td>
                  <td>{{ $applicant['documentsVerifiedMale']}}</td>
                  <td>{{ $applicant['documentsVerifiedFemale']}}</td>
                  <td>{{ $applicant['rejectedMale']}}</td>
                  <td>{{ $applicant['rejectedFemale']}}</td>
                  </tr>
               </tbody>
            </table>
         </div>
     @endif

    {{-- Dev Mannan: Code Ends --}}
    <div class="text-center"> {!! $applications->render() !!}</div>
                        <div class="table-responsive clearfix" style="padding-top: 30px; clear:both">
                    <table class="table table-hover table-bordered app-search-grid">
                        <thead class="table-head">
                            <tr width="100%">
                                <th class="srn">Sr#</th>
                                <th  class="frm"> Form No </th>			
                                <th width="12%" class="algn-left">Name</th>
                                <th class="cni">CNIC</th>
                                <th width="14%" class="algn-left">Program(s)</th>
                                <th class="app">Application</th>
                                <th class="sta">Status</th>
                                <th  class="doc">Documents</th>
                                <th  class="rno">Roll No Slip</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php
                        if($applications){
                        $i = ($applications->perPage()) * ($applications->currentPage() - 1);
                        ++$i;
                    }
                        ?>
                            @foreach ( $applications as $application )
                                @if ( $application->applicant != '' )
                                <tr width="100%">
                                    <td class="srn"> <?= $i; ?></td>
                                <td class="frm"> {{ $application->id }}</td>
                                <td width="12%" class="algn-left">{{ $application->applicant->name }}</td>
                                <td class="cni">{{ $application->applicant->cnic }}</td>
                                <td width="14%" class="algn-left">{{ $application->program->title }}</td>
                                <td class="app">
                                    <div class="sbox-tools  clearfix cus-div flt-none">
                                        @if($application->fkCurrentStatus > '1')
                                        <a target="_blank" href="{{route('applicantprintform',['oas_app_id'=>$application->id])}}" name="save" class="tips btn btn-sm btn-save" title="Back">Print </a>
                                        @endif
                                        <?php
                                        $param = base64_encode(auth()->id() . '/' . $application->applicant->userId);
                                        ?>
                                        <?php /* <a target="_blank" href="http://admission.iiu.edu.pk/AdminLogin/{{$param}}" name="save" class="tips btn btn-sm btn-save" title="Back">Modify </a> */ ?>
                                        <a name="save" id="{{$param}}" class="tips btn btn-sm btn-save md-fy"  title="Back">Modify </a>
                                    </div>
                                </td>
                                <td class="sta status-{{ $application->id }}"><span class="{{ $application->status->status }}">{{ $application->status->status }}</span></td>
                                <td class="doc verified-{{ $application->id }}">
                                    @if ($application->feeVerified)
                                    <?php /* ?>
                                      @if ($ap->doumentsVerified)
                                      <strong class="verified">Verified!</strong>

                                      @elseif($ap->comments)
                                      <span class="clr-rej">Rejected: </span>{{$ap->comments}}
                                      @else
                                      <?php */ ?>
                                    @if ((!$application->doumentsVerified && !$application->comments) || (session()->get('gid') == 1 || session()->get('gid') == 6 || session()->get('gid') == 4 || session()->get('gid') == 5))
                                    <div class="sbox-tools  clearfix cus-div flt-none">
                                        <a name="save" id="{{ $application->id }}" class="tips btn btn-sm btn-save btn-verify" data-toggle="modal" data-target="#verifyModal" title="Back"><?= ($application->fkCurrentStatus > 4)? 'Re-Verify': 'Verify' ?> </a>
                                    </div>
                                    @endif
                                    <?php /* ?>

                                      @endif
                                     * <?php */ ?>

                                    @else
                                    Fee Not Verified
                                    @endif
                                </td>
                                <td  class="rno">
                                        @if($application->fkCurrentStatus == '5')
                                        <div class="sbox-tools  clearfix cus-div flt-none">
                                            <a target="_blank" href="{{route('rollnumberprint',['oas_app_id'=>$application->id])}}" name="" class="tips btn btn-sm btn-save" title="Back">Print</a>
                                        </div>
                                        @endif
                                </td>
                                </tr>
                                @endif
                                <?php $i++ ?>
                            @endforeach
                           
                            
                             <div class="modal fade" id="verifyModal" role="dialog">
                            <div class="modal-dialog">
                                <!-- Modal content-->
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title">Verification/Rejection</h4>
                                    </div>
                                    <div class="modal-body">
                                        <p>*Fill comments section only in case of rejection</p>
                                        <label for="comment">Comment:</label>
                                        <textarea class="form-control" rows="5" id="comment"></textarea>
                                        <button id="" class="btn success boot-btn verify-it">Verify</button>
                                        <button id="" class="btn danger boot-btn reject-it">Reject</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </tbody>
                    </table>
                </div>
            @endif
        </div>
    </div>

    <script>
        $(function(){
            $('#faculty').on('change', function(){

               $.ajax({
                   method: 'post',
                   url: '{{ route('getDepartments') }}',
                   data: {
                       id: $(this).val()
                   },
                   success: function ( response ) {
                       $('#department').empty();
                       $('#department').append(
                            '<option value="">--Select Department--</option>'
                        );
                       $.each(response, function (key, value) {
                           $('#department').append(
                               '<option value="'+value.pkDeptId+'">'+ value.title+'</option>'
                           );
                       });
                   }
               });
            });

            $('#department').on('change', function(){
                $.ajax({
                    method: 'post',
                    url: '{{ route('getProgramme') }}',
                    data: {
                        id: $(this).val()
                    },
                    success: function (response) {
                        $('#programme').empty();
                        if ( response == '' ) {
                            $('#programme').append(
                                '<option value="">--Select Porgramme--</option>'
                            );
                            return;
                        }
                        $('#programme').append(
                            '<option value="">--Select Porgramme--</option>'
                        );
                        $.each(response, function (key, value) {
                            $('#programme').append(
                                '<option value="'+value.pkProgId+'">'+ value.title+' - '+value.duration+' Year(s)</option>'
                            );
                        });
                    }
                });
            });

            $('#accept-doc-form,#reject-doc-form').on('submit', function(){
                event.preventDefault();
                var data =  $(this).serialize();
                $.ajax({
                    url: '{{route('update-docs-status')}}',
                    method: 'post',
                    data: {
                        data: data
                    },
                    success: function(response) {
                        console.log ( response );
                        if ( response == 5) {
                            $('#accept-doc-form').find('button[type="submit"]').text('Verified');
                        }
                        if ( response == 6 ) {
                            $('.close-modal').trigger('click');
                            $('.reject-btn').text('Rejected');
                        }
                    }
                });
            });

            document.querySelector('#faculty').value = '{{request('faculty')}}';
            document.querySelector('#department').value = '{{request('department')}}';
            document.querySelector('#programme').value = '{{request('programme')}}';
            document.querySelector('#unverified-verified-filter').value = '{{request('verification-status')}}';
        });
        $(document).ready(function() 
 {
    $('ul.pagination li a').click(function(e) 
    { 
        e.preventDefault();
        var text = $(this).text();
        $('#page_num').val(text);
        // console.log(text);
     $('#document-verification-from').submit();

    });
 });
    </script>
@stop
