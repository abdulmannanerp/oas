@extends('layouts.app')
@section('content')
<div class="container">
    <div class="page-content row">
        <div class="page-content-wrapper m-t">
            <h4>Document Verification</h4>
            <div class="filters-row">
                <form action="{{route('documentVerification')}}" method="GET" class="form form-inline">
                    <input type="hidden" name="search" value="search">
                    {{-- {{ csrf_field() }} --}}
                    <select name="faculty" id="faculty" class="form-control" required>
                        <option value="">-- Select Facultly -- </option>
                        @foreach ( $faculties as $faculty )
                            <option value="{{$faculty->pkFacId}}" {{( $faculty->pkFacId == $selectedFaculty) ? 'selected' : ''}}>
                                {{$faculty->title}}
                            </option>
                        @endforeach
                    </select>
                    <select name="department" id="department" class="form-control" required>
                        <option value="">-- Select Department -- </option>
                        @if ( $departments )
                            @foreach ( $departments as $department )
                                <option value="{{ $department->pkDeptId }}" {{( $department->pkDeptId == $selectedDepartment) ? 'selected' : ''}}> 
                                    {{ $department->title }} 
                                </option>
                            @endforeach
                        @endif 
                    </select>
                    <select name="program" id="program" class="form-control">
                        <option value="">-- Select Program -- </option>
                        @if ( $programs ) 
                            @foreach ( $programs as $program ) 
                                <option value="{{ $program->pkProgId }}" {{( $program->pkProgId == $selectedProgram) ? 'selected' : ''}}>
                                    {{ $program->title . ' ' . $program->duration . ' Year(s)' }} 
                                </option>
                            @endforeach
                        @endif
                    </select>
                    <select name="status" id="status" class="form-control" required>
                        <option value="">-- Select Status -- </option>
                        <option value="verified" {{ ($selectedStatus == 'verified' ? 'selected' : '') }}>Verified</option>
                        <option value="unverified" {{ ($selectedStatus == 'unverified' ? 'selected' : '') }}> Unverified</option>
                    </select>
                    <input type="submit" value="Search" class="btn btn-primary"/>
                </form>
            </div> <!-- Eding filters-row -->
            @if ( $applications )
            <?php /* ?>
            <div class="row pull-right">
                <ul>
                    <li>
                        <a href="#" id="generate-stats" data-faculty="{{request('faculty')}}" data-department="{{request('department')}}" data-program="{{ request('program') }}" data-url="{{route('generateStats')}}">View Stats</a>
                    </li>
                    <li> <a href="{{ route('documentVerification', [
                        'search' => request('search'),
                        'faculty' => request('faculty'),
                        'department' => request('department'),
                        'program' => request('program'),
                        'status' => request('status'),
                        'export' => true ]) }}">Export Results</a> </li>
                </ul>
            </div>
            <?php */ ?>
            <div class="applications-listing">
                <table class="table table-hover table-bordered">
                    <thead class="table-head">
                        <tr>
                            <th>S.No</th>
                            <th>Form#</th>
                            <th>Name</th>
                            <th>CNIC</th>
                            <th>Program</th>
                            <th>Action</th>
                            <th>Status</th>
                            <th>Documents Verify</th>
                            <th>Roll No Slip</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php $i = $applications->perPage() * ($applications->currentPage() -1); @endphp
                        @foreach ( $applications as $application ) 
                            <tr>
                                <td> {{ $i+1 }} </td>
                                <td> {{ $application->id }} </td>
                                <td> {{ $application->applicant->name ?? '' }} </td>
                                <td> {{ $application->applicant->cnic ?? '' }} </td>
                                <td> {{ $application->program->title }} </td>
                                <td>
                                    @if ( $application->fkCurrentStatus >=2 )
                                        <a href="{{route('applicantprintform', ['oas_app_id' => $application->id])}}" target="_blank" class="btn btn-primary btn-sm m3">Print</a>
                                    @endif
                                    @php
                                        $param = base64_encode(auth()->guard('web')->id() . '/' . $application->applicant->userId);
                                    @endphp
                                    <a href="#" id={{$param}} class="btn btn-primary btn-sm m3 md-fy">Modify</a>
                                </td>
                                <td> 
                                    <span class="{{ ($application->status->status == 'Approved' ) ? 'green' : 'red' }}">
                                        {{ $application->status->status }} 
                                    </span> 
                                </td>
                                <td>
                                    @if ( $application->fkCurrentStatus < 4)
                                        <span> Fee Not Verified </span>
                                    @else
                                        <button class="btn btn-primary docs-verification-btn btn-sm" data-form="{{ $application->id}}">
                                            {{ ($application->fkCurrentStatus == 4 ) ? 'Verify' : 'Reverify'  }}
                                        </button>
                                    @endif
                                    
                                </td>
                                <td>
                                    @if ( $application->fkCurrentStatus == 5 )
                                        <a href="{{route('rollnumberprint', ['oas_app_id' => $application->id])}}" class="btn btn-primary btn-sm" target="_blank">Roll# Slip</a>
                                    @endif
                                </td>
                            </tr>
                            @php $i++; @endphp
                        @endforeach
                    </tbody>
                </table>
            </div> <!--Ending applications listign -->
            @endif
            @if ( $selectedStatus ) 
                {{ $applications->appends([
                    'search' => 'search',
                    'faculty' => $selectedFaculty,
                    'department' => $selectedDepartment,
                    'program' => $selectedProgram,
                    'status' => $selectedStatus
                    ])->links()
                }}
            @endif
        </div> <!-- Ending page-content-wrapper m-t -->
    </div> <!-- Ending page-content row -->

    <div id="verificaiton-modal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Document Verification</h4>
                </div>
                <div class="modal-body">
                    <form action="#" method="POST" id="docsVerificationForm">
                        <input type="hidden" id="applicationIdModalField" name="applicationId" value="">
                        <div class="form-group">
                            <label for="">Enter Comments</label>
                            <br>
                            <small class="color-red">*Mandatory in case of rejection</small>
                            <textarea name="comments" id="docs-verify-reject-comment" cols="30" rows="10" class="form-control"></textarea>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="modalVerifyRejctBtn btn btn-success btn-sm" data-url="{{route('ajaxVerifyReject')}}">Approve</button>
                            <button type="submit" class="modalVerifyRejctBtn btn btn-danger btn-sm" data-url="{{route('ajaxVerifyReject')}}">Reject</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div> <!-- Ending Modal -->
    <div id="statistics-modal" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Statistics</h4>
                </div>
                <div class="modal-body" id="statistics-modal-body">
                    
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div> <!-- Ending Modal -->

</div> <!-- Ending container -->
<script>
    $(function() {
        $('#generate-stats').on('click', function(){
            event.preventDefault();
            $.ajax({
                url: $(this).data('url'),
                method: 'Get',
                data: {
                    program: $(this).data('program'),
                    department: $(this).data('department')
                },
                success: function ( response ) {
                    $('#statistics-modal-body').empty();
                    $('#statistics-modal').modal('show');
                    response = JSON.parse(response);
                    var table = '';
                    $.each ( response , function ( key, value ) {
                        if ( value.name ) {
                            table += `
                            <h4> ${value.name} </h4>
                            <table class="table table-hover table-bordered">
                                <thead class="table-head">
                                    <tr>
                                        <th colspan="2">Form Issued</th>
                                        <th colspan="2">Fee Pending</th>
                                        <th colspan="2">Fee Verified</th>
                                        <th colspan="2">Approved</th>
                                        <th colspan="2">Rejected</th>
                                    </tr>
                                    <tr>
                                        <th>Male</th>
                                        <th>Female</th>
                                        <th>Male</th>
                                        <th>Female</th>
                                        <th>Male</th>
                                        <th>Female</th>
                                        <th>Male</th>
                                        <th>Female</th>
                                        <th>Male</th>
                                        <th>Female</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td> ${value.male} </td>
                                        <td> ${value.female} </td>
                                        <td> ${value.feePendingMale} </td>
                                        <td> ${value.feePendingFemale} </td>
                                        <td> ${value.feeVerifiedMale} </td>
                                        <td> ${value.feeVerifiedFemale} </td>
                                        <td> ${value.documentsVerifiedMale} </td>
                                        <td> ${value.documentsVerifiedFemale} </td>
                                        <td> ${value.documentsRejectedMale} </td>
                                        <td> ${value.documentsRejectedFemale} </td>
                                    </tr>
                                </tbody>
                            </table>`;
                        }
                    });
                    $('#statistics-modal-body').append( table );
                }
            })
            $('#statsModal').modal('show');
        });

        $('#faculty').on('change', function(){
            $.ajax({
                method: 'post',
                url: '{{ route('getDepartments') }}',
                data: {
                    id: $(this).val(),
                    _token: "{{csrf_token()}}"
                },
                success: function ( response ) {
                    $('#department').empty();
                    $('#department').append(
                        '<option value="">--Select Department--</option>'
                    );
                    $.each(response, function (key, value) {
                        $('#department').append(
                            '<option value="'+value.pkDeptId+'">'+ value.title+'</option>'
                        );
                    });
                }
            });
        });

        $('#department').on('change', function(){
            $.ajax({
                method: 'post',
                url: '{{ route('getProgramme') }}',
                data: {
                    id: $(this).val(),
                    _token: "{{ csrf_token() }}"
                },
                success: function (response) {
                    $('#program').empty();
                    if ( response == '' ) {
                        $('#program').append(
                            '<option value="">--Select Porgramme--</option>'
                        );
                        return;
                    }
                    $('#program').append(
                        '<option value="">--Select Porgramme--</option>'
                    );
                    $.each(response, function (key, value) {
                        $('#program').append(
                            '<option value="'+value.pkProgId+'">'+ value.title+' - '+value.duration+' Year(s)</option>'
                        );
                    });
                }
            });
        });
    });
</script
@endsection
