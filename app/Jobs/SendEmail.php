<?php

namespace App\Jobs;

use App\Model\oasEmail;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class SendEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $facultyAbbrev;
    protected $data;
    protected $to;
    protected $subject;
    protected $view;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($facultyAbbrev, $data, $to, $subject, $view)
    {
        $this->facultyAbbrev = $facultyAbbrev;
        $this->data = $data;
        $this->to = $to;
        $this->subject = $subject;
        $this->view = $view;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        oasEmail::email($this->facultyAbbrev, $this->data, $this->to, $this->subject, $this->view);
    }
}
