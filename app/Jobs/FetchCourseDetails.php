<?php

namespace App\Jobs;

use GuzzleHttp\Client;
use App\Aljamia\Course;
use App\Aljamia\Program;
use App\Aljamia\Teacher;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class FetchCourseDetails implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $batch;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($batch)
    {
        $this->batch = $batch;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $client = new Client();
        $batch = $this->batch;
        $url = 'http://usis.iiu.edu.pk:64453/demo/getAllCourses/'.env('ALJAMIA_TOKEN').'/'.$batch->faccode.'/'.$batch->depcode.'/'.$batch->acadprogcode.'/'.$batch->batchcode;
        $request = $client->request('GET', $url);
        $courses = json_decode((string)$request->getBody());
        if ($courses) {
            $program = Program::where('acadprogcode', $batch->acadprogcode)->first();
            foreach ($courses as $response) {
                $teacher = '';
                //check if unique identifier is presnt for teacher
                if ($response->CNIC || $response->TEACHEREMAIL) {
                    $teacher = Teacher::where('cnic', $response->CNIC)
                    ->where('email', $response->TEACHEREMAIL)
                    ->first();
                }
                if (!$teacher) {
                    $teacherCreatedAt = ($response->CREATEDATE) ?
                        date_format(date_create($response->CREATEDATE), 'Y-m-d H:i:s') : null;
                    $teacherUpdatedAt = ($response->UPDATEDATE) ?
                        date_format(date_create($response->CREATEDATE), 'Y-m-d H:i:s') : null;
                    $teacher = Teacher::create([
                        'teacher_id_aljamia' => $response->TEACHERID,
                        'name' => $response->TEACHERNAME,
                        'email' => $response->TEACHEREMAIL,
                        'cnic' => $response->CNIC,
                        'other_email' => $response->OTHEREMAIL,
                        'mobile' => $response->MOBILENO,
                        'status' => $response->TEACHERSTATUS,
                        'gender' => $response->SEX,
                        'teacher_created_at' => $teacherCreatedAt,
                        'teacher_updated_at' => $teacherUpdatedAt
                    ]);
                }
                $courseCreatedAt = ($response->COURSECREATEDATE) ?
                        date_format(date_create($response->COURSECREATEDATE), 'Y-m-d H:i:s') : null;
                $courseUpdatedAt = ($response->COURSEUPDATEDATE) ?
                    date_format(date_create($response->COURSEUPDATEDATE), 'Y-m-d H:i:s') : null;
                $gender = 'F';
                if (strpos($program->acadprogname, '\'') !== false || strpos($program->acadprogname, '.') !== false) {
                    $gender = 'M';
                }
                $course = Course::create([
                    'teacher_id' => $teacher->id,
                    'faccode' => $batch->faccode,
                    'depcode' => $batch->depcode,
                    'acadprogcode' => $batch->acadprogcode,
                    'batchcode' => $batch->batchcode,
                    'coursecode' => $response->COURSECODE,
                    'semester' => $response->SEMCODE,
                    'title' => $response->COURSENAME,
                    'section' => $response->SECTION,
                    'gender' => $gender,
                    'course_created_at' => $courseCreatedAt,
                    'course_updated_at' => $courseUpdatedAt
                ]);
            }
        }
    }
}
