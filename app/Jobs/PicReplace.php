<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use App\Model\ApplicantDetail;
use App\Traits\BankChallanDetail;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class PicReplace implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels, BankChallanDetail;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $appli = ApplicantDetail::whereNotNull('pic')->get();
        // $appli = array("images/30328.BMP", "images/12967.png", "images/p_12967.JPG");
        foreach($appli as $ap){
            $new_name = str_replace("images/","",$ap->pic);
            if (copy(storage_path('app/public/'.$ap->pic), storage_path('app/public/images/FALL-2019/profile/'.$new_name))) {
                ApplicantDetail::where('id', $ap->id)->update(
                    [
                        'pic' => 'images/FALL-2019/profile/'.$new_name
                    ]
                );
                unlink(storage_path('app/public/'.$ap->pic));
            }
        }
    }
}
