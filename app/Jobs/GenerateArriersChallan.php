<?php

namespace App\Jobs;

use App\Model\ChallanDetail;
use App\Model\ProgramFee;
use App\Traits\BankChallanDetail;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use League\Csv\Writer;
use SplTempFileObject;

class GenerateArriersChallan implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels, BankChallanDetail;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $csv = Writer::createFromFileObject(new SplTempFileObject());
        $csv->insertOne(['phone', 'Email']);
        $currentDate = date('d-m-Y h:i:s A');
        $fileName = 'Arrier-Challan-Data-'.$currentDate.'.csv';

        $programFee = ProgramFee::get();
        foreach($programFee as $pf){
            $challanDetail = ChallanDetail::whereIn('challan_type', array(3))->where('fkProgramId', $pf->fkProgramId)->get();
            if($challanDetail){                                                                                
                foreach($challanDetail as $cd){
                    if($cd->challan_type == 3 && $cd->library_security != 5000){
                        // create arriers due challan
                        $challanInfo = $this->insertInfo(
                            $cd->fkRollNumber, $cd->fkApplicationtId, $cd->fkSemesterId, $cd->fkProgramId, '4', '', '', $pf->dues_arriers, '', '', '', $pf->dues_arriers
                        );
                        // create arriers security challan
                        $challanInfo = $this->insertInfo(
                            $cd->fkRollNumber, $cd->fkApplicationtId, $cd->fkSemesterId, $cd->fkProgramId, '5', '', '', '', $pf->security_arriers, '', '', $pf->security_arriers
                        );
                        $csv->insertOne(
                            [
                                $cd->application->applicant->applicantDetail->mobile ?? '',
                                $cd->application->applicant->email ?? ''
                            ]
                        );
                    }
                }
            }
        }
        // $csv->output($fileName);
        file_put_contents(storage_path('app/abl/'.$fileName), $csv);
    }
}
