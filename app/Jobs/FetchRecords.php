<?php

namespace App\Jobs;

use App\Model\Applicant;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class FetchRecords implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $applicationsToReject = [];
        $phoneNumbers = [];
        Applicant::with('application', 'detail')
            ->chunk(20, function ($applicants) use ($applicationsToReject, $phoneNumbers) {
            foreach ( $applicants as $key => $applicant ) {
                if ( count ( $applicant->application ) ) {
                    foreach ( $applicant->application as $app ) {
                        if ( $app->fkCurrentStatus >=3 )
                            $applicationsToReject[] = $key;
                    }
                }
            }
            $filteredApplicants = $applicants->reject(function ($value, $key ) use ( $applicationsToReject) {
                if ( in_array($key, $applicationsToReject) ) 
                    return true;
                return false;
            });
             foreach ( $filteredApplicants as $filtered ) {
                if ( $filtered->detail  ) {
                    $phoneNumbers[] = $filtered->detail->mobile;
                }
            }
            file_put_contents(storage_path('ApplicantPhoneNumbers.json'), json_encode($phoneNumbers), FILE_APPEND);
        });
    }
}
