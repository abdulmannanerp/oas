<?php

namespace App\Jobs;

use GuzzleHttp\Client;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class MigrateSingleRecordToAljamia implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $info;

    public function __construct($info)
    {
        $this->info = $info;
    }

    
    public function handle()
    {
        $client = new Client([
            'base_uri' => 'http://111.68.97.170:64464/'
        ]);
        $client->request('POST', '/migrate', [
            'json' => $this->info
        ]);
    }
}
