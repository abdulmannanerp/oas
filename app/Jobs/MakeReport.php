<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;
use League\Csv\Writer;

class MakeReport implements ShouldQueue
{
    public $applicants;
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct( $applicants )
    {
        $this->applicants = $applicants;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $csv = Writer::createFromPath(storage_path('app/public/make-report.csv'));
        foreach ( $this->applicants as $applicant ) {
            $name = $this->splitName($applicant->name);
            $first = $middle = $last = '';
            if ( count( $name ) >= 3 ) {
                $first = $name[0];
                $middle = $name[1];
                $last = $name[2];
            }
            if ( count( $name ) <= 2 ) {
                $first = $name[0];
                $last = $name[1];;
            }
            $csv->insertOne(
                [
                    ($applicant->fkGenderId == 3 ) ? 'Mr.' : 'Mrs.', //title
                    $first ?? '', //first 
                    $middle ?? '', //middle
                    $last ?? '', //last
                    ' ', //status
                    ($applicant->fkGenderId == 3 ) ? 'M' : 'F', //gender
                    ' ', //blood group
                    ' ', //disability
                    date('d-m-Y', strtotime($applicant->detail->DOB)), //dob
                    $applicant->address->country->countryName ?? '', //country
                    $applicant->address->cityPo ?? '', //city
                    $applicant->detail->district->distName ?? '',//domicile district, 
                    ' ', //domicile province
                    ' ', //area
                    ' ', //religion
                    $applicant->cnic, //cnic
                    $applicant->detail->fatherName, //father name
                    ' ', //father cnic
                    ($applicant->detail->fatherStatus == 1) ? 'Alive' : 'Deceased', //father status
                    $applicant->detail->fatherOccupation, //father occupation
                    $applicant->address->addressPmt, //permanent address
                    $applicant->address->addressPo, //mailing address
                    $applicant->email, //email
                    $applicant->detail->mobile, //mobile
                    ' ', //social contact, 
                    $this->getApplicationsIdArray($applicant), //application id
                    ' ', //admission date
                    '2018', //admission year
                    $this->getFaculties($applicant), //faculty
                    $this->getDepartments($applicant), //department
                    $this->getLevel($applicant), //level!
                    'Regular', //degree type
                    $this->getLevel($applicant), //degree!
                    $this->getPrograms($applicant), //degree title
                    ' ', //type
                    'Morning', //session type
                    'Semester', //exam type
                    'Fall', //intake type
                    'Main', //campus category
                    'International Islamic University H-10/4 Islamabad', //Univeristy
                    ' ' //student status
                ]
            );
        }
    }

    public function getLevel($applicant) 
    {
        $title = $applicant->application->pluck('program.level.title');
        return (string) implode(",", array($title)); 
    }

    public function getPrograms($applicant) 
    {
        $title = $applicant->application->pluck('program.title');
        return (string) implode(",", array($title)); 
    }

    public function getDepartments($applicant)
    {
        $title = $applicant->application->pluck('program.faculty.title');
        return (string) implode(",", array($title)); 
    }

    public function getFaculties($applicant)
    {
        $title = $applicant->application->pluck('program.department.title');
        return (string) implode(",", array($title)); 
    }

    public function getApplicationsIdArray($applicant) 
    {
        $ids = $applicant->application->pluck('id');
        return (string) implode(",", array($ids)); 
    }

    public function splitName($name)
    {
        return explode(' ', $name);
    }
}
