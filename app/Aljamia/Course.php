<?php

namespace App\Aljamia;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    protected $guarded = [];
    protected $table = 'aljamia_courses';

    public function teacher()
    {
        return $this->belongsTo('App\Aljamia\Teacher', 'teacher_id');
    }

    public function faculty()
    {
        return $this->belongsTo('App\Aljamia\Faculty', 'faccode', 'faccode');
    }

    public function department()
    {
        return $this->belongsTo('App\Aljamia\Department', 'depcode', 'depcode');
    }

    public function program()
    {
        return $this->belongsTo('App\Aljamia\Program', 'acadprogcode' ,'acadprogcode');
    }

    public function alias()
    {
        $gender = 'F';
        if (strpos($this->department->depname, '\'') !== false || strpos($this->department->depname, '.') !== false) {
            $gender = 'M';
        }
        
        return
            $this->faculty->abbrev . '-' .
            $this->department->depname. '-' .
            $gender. '-' .
            rand(10, 99).$this->id.rand(10, 99);
    }
    
}
