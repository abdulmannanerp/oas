<?php

namespace App\Aljamia;

use Illuminate\Database\Eloquent\Model;

class Batch extends Model
{
    protected $guarded = [];
    protected $table = 'aljamia_batch';
    
    public function courses()
    {
        return $this->hasMany('App\Aljamia\Course', 'batchcode', 'batchcode');
    }
}
