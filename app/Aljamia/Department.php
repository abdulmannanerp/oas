<?php

namespace App\Aljamia;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    protected $guarded = [];
    protected $table = 'aljamia_department';
    
    public function courses()
    {
        return $this->hasMany('App\Aljamia\Course', 'depcode', 'depcode');
    }
}
