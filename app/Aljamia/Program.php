<?php

namespace App\Aljamia;

use Illuminate\Database\Eloquent\Model;

class Program extends Model
{
    protected $guarded = [];
    protected $table = 'aljamia_program';
    
    public function courses()
    {
        return $this->hasMany('App\Aljamia\Course', 'acadprogcode', 'acadprogcode');
    }
}
