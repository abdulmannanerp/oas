<?php

namespace App\Aljamia;

use Illuminate\Database\Eloquent\Model;

class Audit extends Model
{
    protected $guarded = [];
    protected $table = 'aljamia_audit';
    
}
