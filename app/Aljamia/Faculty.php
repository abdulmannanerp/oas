<?php

namespace App\Aljamia;

use Illuminate\Database\Eloquent\Model;

class Faculty extends Model
{
    protected $guarded = [];
    protected $table = 'aljamia_faculty';
    // protected $primaryKey = 'faccode';

    public function departments()
    {
        return $this->hasMany('App\Aljamia\Department', 'faccode', 'faccode');
    }

    public function programs()
    {
        return $this->hasMany('App\Aljamia\Program', 'faccode', 'faccode');
    }

    public function bathces()
    {
        return $this->hasMany('App\Aljamia\Batch', 'faccode', 'faccode');
    }

    public function courses()
    {
        return $this->hasMany('App\Aljamia\Course', 'faccode', 'faccode');
    }
}
