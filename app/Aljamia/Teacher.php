<?php

namespace App\Aljamia;

use Illuminate\Database\Eloquent\Model;

class Teacher extends Model
{
    protected $guarded = [];
    protected $table = 'aljamia_teachers';

    public function courses()
    {
        return $this->hasMany('App\Aljamia\Course', 'teacher_id');
    }
}
