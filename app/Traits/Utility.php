<?php
namespace App\Traits;

use App\Model\{Faculty,Department,Programme};

trait Utility {

    public function getAllFaculties()
    {
        return Faculty::all();
    }

    public function getAllDepartments()
    {
        return Department::all();
    }

    public function getAllPrograms()
    {
        return Programme::all();
    }

    public function getDepartmentsByFaculty( $facultyId )
    {
        return ( $facultyId ) ? Dapartment::where('fkFacId', $facultyId)->get() : '';
    }

    public function getProgramByDepartment( $departmentId )
    {
        return ( $departmentId ) ? Programme::where('fkDepId', $departmentId )->get() : '';
    }

    public function getAllFacultiesByPermission( $userId )
    {
        
    }

    public function getAllDepartmentsByPermission( $userId )
    {

    }

    public function getProgramsByPermission( $userId )
    {
        
    }

    public function getAllDepartmentsByPermissionByFaculty( $userId, $facultyId )
    {

    }

    public function getProgramsByPermissionByDepartment( $userId, $departmentId)
    {

    }
}

