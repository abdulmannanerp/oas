<?php
namespace App\Traits;
use App\Model\ChallanDetail;

trait BankChallanDetail{

    public function insertInfo(
		$fkRollNumber = '', 
		$fkApplicationtId = '',
		$fkSemesterId = '', 
		$fkProgramId = '', 
		$challan_type ='', 
		$admission_processing_fee = '',
    	$admission_fee = '' ,
    	$university_dues = '',
    	$library_security = '' ,
    	$book_bank_security = '' ,
    	$caution_money = '' ,
		$total = '',
		$second_challan = ''
		){
		$result = ChallanDetail::create([
    		'fkRollNumber' => $fkRollNumber,
    		'fkApplicationtId' => $fkApplicationtId, 
    		'fkSemesterId' => $fkSemesterId,
    		'fkProgramId' => $fkProgramId,
    		'challan_type' => $challan_type,
    		'admission_processing_fee' => $admission_processing_fee,
    		'admission_fee' => $admission_fee,
    		'university_dues' => $university_dues,
    		'library_security' => $library_security,
    		'book_bank_security' => $book_bank_security,
    		'caution_money' => $caution_money,
			'total' => $total,
			'second_challan' => $second_challan
    	]);
    }


}