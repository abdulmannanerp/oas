<?php
namespace App\Traits;

trait PQM
{
    public function calculatePqm(
        $id,
        $faculty,
        $hsscTitle,
        $hsscResultAwaiting,
        $program,
        $llbObtainedMarks,
        $llbTotalMarks,
        $level,
        $interviewDateTime,
        $preReq = 0,
        $sscTotal = 0,
        $sscObtained = 0,
        $hsscTotal = 0,
        $hsscObtained = 0,
        $hsscTotalPart1 = 0,
        $hsscTotalPart2 = 0, //
        $hsscObtainedPart1 = 0,
        $hsscObtainedPart2 = 0, //
        $baBscTotal = 0,
        $baBscObtained = 0,
        $mscTotal = 0,
        $mscObtained = 0,
        $bsTotal = 0,
        $bsObtained = 0,
        $msTotal = 0,
        $msObtained = 0
    ) {
        if ($level == 1 || $level == 2) {
            /* BS & Diploma */
            return self::calculatePqmForUnderGraduate($faculty, $hsscTitle, $hsscResultAwaiting, $program, $llbObtainedMarks, $llbTotalMarks, $sscObtained, $sscTotal, $hsscObtained, $hsscTotal, $hsscObtainedPart1, $hsscTotalPart1, $hsscObtainedPart2, $hsscTotalPart2);
        } elseif ($level == 3) {
            /* MA,  MSc */
            return self::calculatePqmForMaMsc($hsscObtained, $hsscTotal, $hsscObtainedPart1, $hsscTotalPart1, $hsscObtainedPart2, $hsscTotalPart2, $baBscObtained, $baBscTotal);
        } elseif ($level == 4) {
            /* Masters */
            return self::calculatePqmForMasters($hsscObtained, $hsscTotal, $hsscObtainedPart1, $hsscTotalPart1, $hsscObtainedPart2, $hsscTotalPart2, $mscObtained, $mscTotal, $bsObtained, $bsTotal);
        } elseif ($level == 5) {
            /* PostGraduate */
            return self::calculatePqmForPostGraduate($mscObtained, $mscTotal, $bsObtained, $bsTotal, $msObtained, $msTotal);
        }
    }
    

    public static function calculatePqmForUnderGraduate($faculty, $hsscTitle, $hsscResultAwaiting, $program, $llbObtainedMarks, $llbTotalMarks, $sscObtained, $sscTotal, $hsscObtained, $hsscTotal, $hsscObtainedPart1, $hsscTotalPart1, $hsscObtainedPart2, $hsscTotalPart2)
    {
        if ($faculty == 'Engineering & Technology') {
            return self::calculatePqmForEngineering($sscObtained, $sscTotal, $hsscObtained, $hsscTotal, $hsscObtainedPart1, $hsscTotalPart1, $hsscObtainedPart2, $hsscTotalPart2, $hsscTitle, $hsscResultAwaiting);
        }

        if (trim($program) == 'BA/LLB (Hons) Shariah & Law' || trim($program) == 'LLB') {
            return self::calculatePqmForBaLLb($llbObtainedMarks, $llbTotalMarks, $sscObtained, $sscTotal, $hsscObtained, $hsscTotal, $hsscObtainedPart1, $hsscTotalPart1, $hsscObtainedPart2, $hsscTotalPart2, $hsscTitle, $hsscResultAwaiting);
        }

        $ssc40Percent = 0;
        $hssc60Percent = 0;
        if ($sscTotal != '' || $sscTotal != 0) { 
            $ssc40Percent = ((float)$sscObtained/(float)$sscTotal)*40;
        }
        /**
         * If HSSC title is A-Level and result awaiting is marked, Give 50% of total
         */
        if ($hsscTitle == 'A-Level' && $hsscResultAwaiting == 1) {
            $hssc60Percent = 30;
        }elseif ($hsscTotalPart1 !== '' && $hsscTotalPart2 !== '') {
            $hssc60Percent = (((float)$hsscObtainedPart1+(float)$hsscObtainedPart2) / ((float)$hsscTotalPart1+(float)$hsscTotalPart2))*60;
        } elseif ($hsscTotalPart1 !== '' || $hsscTotalPart1 !== 0) {
            if($hsscResultAwaiting == 1){
                $hssc60Percent = 30;
            }
            if($hsscTotalPart1 != ''){
                $hssc60Percent = (float)$hsscObtainedPart1 / ((float)$hsscTotalPart1)*60;
            }
        }
        $pqm = $ssc40Percent + $hssc60Percent;
        return $pqm = number_format((float)$pqm, 2, '.', '');
    }

    /**
     * Calculate for engineering program
     */
    public static function calculatePqmForEngineering($sscObtained, $sscTotal, $hsscObtained, $hsscTotal, $hsscObtainedPart1, $hsscTotalPart1, $hsscObtainedPart2, $hsscTotalPart2, $hsscTitle, $hsscResultAwaiting)
    {
        $ssc40Percent = 0;
        $hssc60Percent = 0;
        if ($sscTotal != '' || $sscTotal != 0) {
            $ssc40Percent = ((float)$sscObtained/(float)$sscTotal)*15;
        }
        if ($hsscTitle == 'A-Level' && $hsscResultAwaiting == 1) {
            $hssc60Percent = 12.5;
        } elseif ($hsscTotalPart1 !== '' || $hsscTotalPart1 != 0) {
            $hssc60Percent = (float)$hsscObtainedPart1 / ((float)$hsscTotalPart1)*25;
        }
        $pqm = $ssc40Percent + $hssc60Percent;
        return $pqm = number_format((float)$pqm, 2, '.', '');
    }

    public static function calculatePqmForBaLLb($llbObtainedMarks, $llbTotalMarks, $sscObtained, $sscTotal, $hsscObtained, $hsscTotal, $hsscObtainedPart1, $hsscTotalPart1, $hsscObtainedPart2, $hsscTotalPart2, $hsscTitle, $hsscResultAwaiting)
    {
        $ssc40Percent = 0;
        $hssc60Percent = 0;
        if ($sscTotal != '' || $sscTotal != 0) {
            $ssc40Percent = ((float)$sscObtained/(float)$sscTotal)*15;
        }
        if ($hsscTitle == 'A-Level' && $hsscResultAwaiting == 1) {
            $hssc60Percent = 12.5;
        }elseif ($hsscTotalPart1 !== '' && $hsscTotalPart2 !== '') {
            $hssc60Percent = (((float)$hsscObtainedPart1+(float)$hsscObtainedPart2) / ((float)$hsscTotalPart1+(float)$hsscTotalPart2))*25;
        }elseif ($hsscTotalPart1 !== '' || $hsscTotalPart1 != 0) {
            $hssc60Percent = (float)$hsscObtainedPart1 / ((float)$hsscTotalPart1)*25;
        }

        $lat60Percent = 0;

        if ($llbObtainedMarks && $llbTotalMarks) {
            $lat60Percent = (float)$llbObtainedMarks/ ((float)$llbTotalMarks) * 60;
        }
        // $pqm = $ssc15Percent + $hssc25Percent + $lat60Percent;
        $pqm = $ssc40Percent + $hssc60Percent +  $lat60Percent;
        return $pqm = number_format((float)$pqm, 2, '.', '');
    }

    public static function calculatePqmForMaMsc($hsscObtained, $hsscTotal, $hsscObtainedPart1, $hsscTotalPart1, $hsscObtainedPart2, $hsscTotalPart2, $baBscObtained, $baBscTotal)
    {

        $hssc40Percent = 0;
        if ($hsscTotal !== '' || $hsscTotal != 0) {
            $hssc40Percent = ((float)$hsscObtained/(float)$hsscTotal)*40;
        } elseif ($hsscTotalPart1 !== '' && $hsscTotalPart2 !== '') {
            $hssc40Percent = (((float)$hsscObtainedPart1+(float)$hsscObtainedPart2) / ((float)$hsscTotalPart1+(float)$hsscTotalPart2))*40;
        } elseif ($hsscTotalPart1 !== '') {
            $hssc40Percent = (float)$hsscObtainedPart1 / ((float)$hsscTotalPart1)*40;
        }

        $bsBsc60Percent = 0;
        if ($baBscTotal !='' || $baBscTotal != 0) {
            $bsBsc60Percent = ((float)$baBscObtained/(float)$baBscTotal)*60;
        }
        $pqm = $hssc40Percent + $bsBsc60Percent;
        return number_format((float)$pqm, 2, '.', '');
    }

    public static function calculatePqmForMasters($hsscObtained, $hsscTotal, $hsscObtainedPart1, $hsscTotalPart1, $hsscObtainedPart2, $hsscTotalPart2, $mscObtained, $mscTotal, $bsObtained, $bsTotal)
    {
        $hssc20Percent = 0;
        if ($hsscTotal !== '' || $hsscTotal != 0) {
            $hssc20Percent = ((float)$hsscObtained/(float)$hsscTotal)*20;
        } elseif ($hsscTotalPart1 !== '' && $hsscTotalPart2 !== '') {
            $hssc20Percent = (((float)$hsscObtainedPart1+(float)$hsscObtainedPart2) / ((float)$hsscTotalPart1+(float)$hsscTotalPart2))*20;
        } elseif ($hsscTotalPart1 !== '') {
            $hssc20Percent = (float)$hsscObtainedPart1 / ((float)$hsscTotalPart1)*20;
        }
        $msc40Percent = '';
        $bs40Percent = '';
        $previousDegree40Percent = 0;
        if ($mscTotal && $bsTotal) {
            $bs40Percent =  ((float)$bsObtained/(float)$bsTotal)*40;
            $msc40Percent =  ((float)$mscObtained/(float)$mscTotal)*40;
            if ($msc40Percent >= $bs40Percent) {
                $previousDegree40Percent = $msc40Percent;
            } else {
                $previousDegree40Percent = $bs40Percent;
            }
        } elseif ($bsTotal) {
            $previousDegree40Percent = $bs40Percent =  ((float)$bsObtained/(float)$bsTotal)*40;
        } elseif ($mscTotal) {
            $previousDegree40Percent = $msc40Percent =  ((float)$mscObtained/(float)$mscTotal)*40;
        }
        // $pqm = $hssc20Percent + $previousDegree40Percent;  
        $pqm = $previousDegree40Percent;  // Dev Mannan: Only use pqm for previous degree 40% not 60%
        return number_format((float)$pqm, 2, '.', '');
    }

    public static function calculatePqmForPostGraduate($mscObtained, $mscTotal, $bsObtained, $bsTotal, $msObtained, $msTotal)
    {
        $msc20Percent = '';
        $bs20Percent = '';
        $previousDegree20Percent = 0;
        if ($mscTotal && $bsTotal) {
            $bs20Percent =  ((float)$bsObtained/(float)$bsTotal)*20;
            $msc20Percent =  ((float)$mscObtained/(float)$mscTotal)*20;
            if ($msc20Percent >= $bs20Percent) {
                $previousDegree20Percent = $msc20Percent;
            } else {
                $previousDegree20Percent = $bs20Percent;
            }
        } elseif ($bsTotal) {
            $previousDegree20Percent = $bs20Percent =  ((float)$bsObtained/(float)$bsTotal)*20;
        } elseif ($mscTotal) {
            $previousDegree20Percent = $msc20Percent =  ((float)$mscObtained/(float)$mscTotal)*20;
        }

        
        $ms40Percent = 0;
        if ($msTotal) {
            $ms40Percent = ((float)$msObtained / (float)$msTotal)*40;
        }
          
        // $pqm = $previousDegree20Percent + $ms40Percent;
        $pqm = $ms40Percent; // Dev Mannan: Only use pqm for previous degree 40% not 60%
        return number_format((float)$pqm, 2, '.', '');
    }
}
