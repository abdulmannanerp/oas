<?php
namespace App\Traits;

trait PQM {

	public function calculatePqm (
        $id,
        $level,
        $interviewDateTime,
        $preReq=0,
        $sscTotal = 0 ,
        $sscObtained = 0,
        $isHsscResultAwaiting = false, // Dev Mannan: Column Added for fall 2021
        $hsscTotal = 0,
        $hsscObtained = 0,
        $hsscTotalPart1 = 0 ,
        $hsscTotalPart2 = 0 ,
        $hsscObtainedPart1 = 0 ,
        $hsscObtainedPart2 = 0,
        $baBscTotal = 0,
        $baBscObtained = 0,
        $mscTotal = 0,
        $mscObtained = 0 ,
        $bsTotal = 0,
        $bsObtained = 0,
        $msTotal = 0,
        $msObtained = 0 )
    {
        if ( $level == 1 || $level == 2 ) {
           /* BS & Diploma */
           return Self::calculatePqmForUnderGraduate ( $sscObtained,$sscTotal,$hsscObtained,$hsscTotal,$hsscObtainedPart1,$hsscTotalPart1,$hsscObtainedPart2, $hsscTotalPart2, $isHsscResultAwaiting);
        } else if ( $level == 3 ) {
            /* MA,  MSc */
            if ( $interviewDateTime !== '0000-00-00 00:00:00' ) {
                return Self::calculatePqmForMaMsc( $baBscObtained, $baBscTotal);
            } else {
                return Self::calculatePqmForUnderGraduate ( $sscObtained, $sscTotal, $hsscObtained, $hsscTotal, $hsscObtainedPart1, $hsscTotalPart1, $hsscObtainedPart2, $hsscTotalPart2, $isHsscResultAwaiting );
            }
        } else if ( $level == 4 ) {
            /* Masters */
            return Self::calculatePqmForMasters($mscObtained, $mscTotal, $bsObtained, $bsTotal);
        }
        else if ( $level == 5 ) {
            /* PostGraduate */
            return Self::calculatePqmForPostGraduate($msObtained, $msTotal);
        }
    }
    
    public static function calculatePqmForUnderGraduate($sscObtained, $sscTotal, $hsscObtained, $hsscTotal, $hsscObtainedPart1, $hsscTotalPart1, $hsscObtainedPart2,$hsscTotalPart2, $isHsscResultAwaiting )
    {
        $ssc15Percent = 0;
        $hssc25Percent = 0;
        if ( $sscTotal != '' || $sscTotal != 0 ) {
            $ssc15Percent = ( (float)$sscObtained/(float)$sscTotal )*15;
        }
        if(!$isHsscResultAwaiting){
            if ( $hsscTotal !== '' || $hsscTotal != 0 ) {
                $hssc25Percent = ( (float)$hsscObtained/(float)$hsscTotal )*25;
            } else if ( $hsscTotalPart1 !== '' && $hsscTotalPart2 !== '' ) {
                $hssc25Percent = ( ( (float)$hsscObtainedPart1+(float)$hsscObtainedPart2 ) / ( (float)$hsscTotalPart1+(float)$hsscTotalPart2) )*25; 
            } else if ( $hsscTotalPart1 !== '' ) {
                $hssc25Percent = (float)$hsscObtainedPart1 / ( (float)$hsscTotalPart1 )*25; 
            }
        }else{
            $hssc25Percent = 25/2;
        }
        
        $pqm = $ssc15Percent + $hssc25Percent;

    return $pqm = number_format((float)$pqm, 2, '.', '');
    }

    public static function calculatePqmForMaMsc($baBscObtained, $baBscTotal) 
    {
        if ( $baBscTotal !='' || $baBscTotal != 0 ) {
            $pqm = ( (float)$baBscObtained/(float)$baBscTotal )*30;
            return $pqm = number_format((float)$pqm, 2, '.', '');  
        }
        return 0;
    }

    public static function calculatePqmForMasters($mscObtained, $mscTotal, $bsObtained, $bsTotal)
    {
        $msc30Percent = '';
        $bs30Percent = '';
        $previousDegree30Percent = 0;
        if ( $mscTotal && $bsTotal ) {
            $bs30Percent =  ((float)$bsObtained/(float)$bsTotal)*30;
            $msc30Percent =  ((float)$mscObtained/(float)$mscTotal)*30;
            if ( $msc30Percent >= $bs30Percent ) {
                $previousDegree30Percent = $msc30Percent;
            }
            else {
                $previousDegree30Percent = $bs30Percent; 
            }
        }
        else if ( $bsTotal ) {
            $previousDegree30Percent = $bs30Percent =  ((float)$bsObtained/(float)$bsTotal)*30;   
        }
        else if ( $mscTotal ) {
            $previousDegree30Percent = $msc30Percent =  ((float)$mscObtained/(float)$mscTotal)*30;
        }

        // $pqm = $previousDegree30Percent;
        return number_format((float)$previousDegree30Percent, 2, '.', '');
    }

    public static function calculatePqmForPostGraduate($msObtained, $msTotal)
    {
        $previousDegree30Percent = 0;
        if ( $msTotal )
            $previousDegree30Percent = ((float)$msObtained / (float)$msTotal)*30;
          
        // $pqm = $previousDegree30Percent;   
        return number_format((float)$previousDegree30Percent, 2, '.', '');
    }
}