<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class TeacherCourseCreated extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;
    public $teacher;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($teacher)
    {
        $this->teacher = $teacher;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Google Class Created')->view('mail.teacher-course-created');
    }
}
