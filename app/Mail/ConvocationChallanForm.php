<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ConvocationChallanForm extends Mailable
{
    use Queueable, SerializesModels;
    public $convocation;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct( $convocation )
    {
        $this->convocation = $convocation;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('convocation.mail.convocation-challan-form');
    }
}
