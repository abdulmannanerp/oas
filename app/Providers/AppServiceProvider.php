<?php

namespace App\Providers;

use App\Model\Semester;
use App\Model\Application;
use App\Model\ApplicationStatus;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        date_default_timezone_set('Asia/Karachi');

        // Using Closure based composers...
        View::composer('*', function ($view) {
            $semester = Semester::where('status', 1)->first();
            $applicationStatus = ApplicationStatus::where('fkApplicantId', auth()->id())->where('fkSemesterId', $semester->pkSemesterId)->first();
            $applicationRollNumber = Application::where('fkApplicantId', auth()->id())->where('fkSemesterId', $semester->pkSemesterId)->where('fkCurrentStatus', '>=', 5)->where('fkCurrentStatus', '!=', 6)->where('fkCurrentStatus', '!=', 7)->get();
            $view->with('applicationStatus', $applicationStatus )->with('applicationRollNumber', $applicationRollNumber );
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
