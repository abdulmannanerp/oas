<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Backup extends Model
{
    protected $table = 'tbl_backup';
    protected $guarded = [];
}
