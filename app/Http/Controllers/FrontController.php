<?php

namespace App\Http\Controllers;

use App\Model\oasEmail;
use App\Model\Nationality;
use App\Model\ProgramLevel;


class FrontController extends Controller{
    public function index (){   
    	return view('frontend.main');
    }
    public function faq(){
        return view('frontend.faq');
    }
    public function contactus(){
        $status = request()->session()->get('status');
        if ( request()->method() == 'POST' ) {
            $this->validate(request(), [
                'department' => 'required',
                'name' => 'required',
                'email' => 'required',
                'phone' => 'required',
                'message' => 'required'
            ]);
            
            if(request('department') == 'Admission (Male)' || request('department') == 'Overseas(Male & Female)'){
                $abbr = 'AdmissionMale';
                $mail = 'admissionalert1@iiu.edu.pk';
            }else{
                $abbr = 'AdmissionFemale';
                $mail = 'admissionalert2@iiu.edu.pk';
            }
            $data = array('department' => request('department'), 
            'name' => request('name'), 
            'email' => request('email'), 
            'phone' => request('phone'), 
            'cnic' => request('cnic'),
            'mess' => trim(request('message')));
            // dd($data);
            oasEmail::email($abbr, $data, $mail, 'IIU Admission Contact Form', 'frontend.ContactUsEmail', 'cc_to_admission');
            return back()->with('status', 'Thanks for Contacting Us. Soon you will get reply from IIU Support team');
        }else{
        return view('frontend.contactus', compact('status'));
        }
    }
}
