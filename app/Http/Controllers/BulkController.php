<?php

namespace App\Http\Controllers;

use App\Jobs\SendEmail;
use App\Jobs\SendSMS;
use App\Model\Application;
use App\Model\Faculty;
use App\Model\Status;
use Carbon\Carbon;
use Illuminate\Http\Request;
use League\Csv\Writer;
use SplTempFileObject;


class BulkController extends Controller
{

	public function index()
	{
		$faculties = Faculty::select('pkFacId', 'title')->get();
		$allStatus = Status::select('pkAppStatusId','status')->get();
		return view ('bulk.index', compact('faculties', 'allStatus'));
	}

    public function store()
    {
    	$faculty = request('faculty');
    	$department = request('department');
    	$program = request('program');
    	$gender = request('gender');
    	$status = request('status');
    	$type = request('bulkType');
    	$subject = request('subject');
    	$message = request('message');

        $operator = '';
        if ( $status == 1 || $status == 3 || $status==5 || $status == 6 )
            $operator = '=';
        else
            $operator = '>=';

        // $facultyAbbrev = Faculty::find( $faculty )->value('abbrev');
        $facultyAbbrev = Faculty::where('pkFacId', $faculty)->first();
        $facultyAbbrev = $facultyAbbrev->abbrev;
    	$applications = $this->getApplicants($program, $gender, $status, $operator);
    	if ( $type == 'email' ) 
    		$this->email ( $applications, $facultyAbbrev, $subject, $message );
        if ( $type == 'sms' )
            $this->sms($applications, $message);
    	return back();
    }

    public function getApplicants($program, $gender, $status, $operator)
    {
    	return Application::with(['applicant', 'applicant.detail'])->whereHas('applicant', function($query) use ($program, $gender, $status) {
    		$query->where('fkGenderId', $gender);
    	})->where('fkProgramId', $program)->where('fkCurrentStatus',$operator, $status)->get();
    }

    public function email( $applications, $abbrev, $subject, $message )
    {
    	foreach ( $applications as $application ) {
    		$to =  $application->applicant->email;
    		$data = array(
                'name' => $application->applicant->name, 
                'body' => $message
            );
    		// dd ( $to, $data, $abbrev, $subject, $message);
    		if (filter_var($to, FILTER_VALIDATE_EMAIL )) {
    			// $job = (new SendEmail($abbrev, $data, $to, $subject, 'emails.bulk'));
       //          dispatch_now($job);
                SendEmail::dispatch(
                    $abbrev, 
                    $data, 
                    $to, 
                    $subject, 
                    'emails.bulk'
                );
    		}
    	}
    }	

    public function sms($applications, $message)
    {
        $message = urlencode ( $message );
        foreach ( $applications as $application ) 
        {
           $mobile = $this->formatMobileNumber ( $application->applicant->detail->mobile );
           if (strlen( $mobile ) == 12 ) {
                $csv = Writer::createFromFileObject(new SplTempFileObject());
                foreach ( $applications as $application )
                {
                     $csv->insertOne([$application->applicant->name, $mobile]);
                }
                $csv->output('Export.csv');
                die;
           }
        }        
    }

    public function formatMobileNumber( $mobile )
    {
        $splitString = str_split ( $mobile );
        if( $splitString[0] == '0' && $splitString[1] == '0' && $splitString[2] == '9' && $splitString[3] == '2' ) {
            return $this->format0092Numbers($splitString);
        }
        if( $splitString[0] == '0' && $splitString[1] == '3' ) {
            return $this->format03Numbers($splitString);
        }
        if( $splitString[0] == '9' && $splitString[1] == '2' ) {
            return $mobile;
        }        
    }

    public function format0092Numbers($mobile) {
        unset ($mobile[0]);
        unset($mobile[1]);
        return implode('', $mobile);
    }

    public function format03Numbers($mobile) 
    {
        unset ($mobile[0]);
        array_unshift($mobile, '9', '2');
        return implode('', $mobile);   
    }    
}
