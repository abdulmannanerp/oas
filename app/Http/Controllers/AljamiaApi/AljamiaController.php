<?php

namespace App\Http\Controllers\AljamiaApi;

use App\Aljamia\Batch;
use GuzzleHttp\Client;
use App\Aljamia\Course;
use App\Aljamia\Faculty;
use App\Aljamia\Program;
use App\Aljamia\Teacher;
use App\Aljamia\Department;
use Illuminate\Http\Request;
use App\Jobs\FetchCourseDetails;
use App\Http\Controllers\Controller;

class AljamiaController extends Controller
{
    public function client() {
    }

    public function getFaculties()
    {
        $client = new Client();
        $url = 'http://usis.iiu.edu.pk:64453/demo/apifaculty/'.env('ALJAMIA_TOKEN');
        $request = $client->request('GET', $url);
        $response = json_decode((string)$request->getBody());
        foreach ($response as $key => $value) {
            Faculty::create([
                'faccode' => $value->FACCODE,
                'facname' => $value->FACNAME
            ]);
        }
        return 'Data Imported Successfully';
    }

    public function getDepartments()
    {
        $client = new Client();
        $url = 'http://usis.iiu.edu.pk:64453/demo/apidept/'.env('ALJAMIA_TOKEN');
        $request = $client->request('GET', $url);
        $response = json_decode((string)$request->getBody());
        foreach ($response as $key => $value) {
            Department::create([
                'depcode' => $value->DEPCODE,
                'faccode' => $value->FACCODE,
                'depname' => $value->DEPNAME,
                'depdesc' => $value->DEPDESC,
                'depalias' => $value->DEPALIAS
            ]);
        }
        return 'Data Imported Successfully';
    }

    public function getPrograms()
    {
        $client = new Client();
        $url = 'http://usis.iiu.edu.pk:64453/demo/apiacadprog/'.env('ALJAMIA_TOKEN');
        $request = $client->request('GET', $url);
        $response = json_decode((string)$request->getBody());
        foreach ($response as $key => $value) {
            Program::create([
                'acadprogcode' => $value->ACADPROGCODE,
                'faccode' => $value->FACCODE,
                'depcode' => $value->DEPCODE,
                'acadprogname' => $value->ACADPROGNAME
            ]);
        }
        return 'Data Imported Successfully';
    }

    public function getBatches()
    {
        $client = new Client();
        $url = 'http://usis.iiu.edu.pk:64453/demo/apibatchname/'.env('ALJAMIA_TOKEN');
        $request = $client->request('GET', $url);
        $response = json_decode((string)$request->getBody());
        foreach ($response as $key => $value) {
            Batch::create([
                'batchcode' => $value->BATCHCODE,
                'faccode' => $value->FACCODE,
                'depcode' => $value->DEPCODE,
                'acadprogcode' => $value->ACADPROGCODE,
                'batchname' => $value->BATCHNAME
            ]);
        }
        return 'Data Imported Successfully';
    }

    public function getCourses()
    {
        // http://usis.iiu.edu.pk:64453/demo/getAllCourses/967289d0dbcfecb9bf8f2cff3c7ec767302d944e79d3618a36dd7e5aef27dcc1c1bf69f28ba5232e5a85419fad433df4959e3ba98d312a34cb27b1b9acab3cfdnCw9o9MNGBY7mKCoLZYMzcUM0bv7MNzsrJG68zOYXRcoaewDsjpnsa9g2JYfUK5h/FAS0/FASDCS0/DCS4/FASDCSBSCS4981/
        $batches = Batch::where('faccode', 'FAS0')->get();
        foreach ($batches as $batch) {
            $when = now()->addSeconds(30);
            FetchCourseDetails::dispatch($batch)->delay($when);
        }
        return 'Pushed to queue';
    }
}
