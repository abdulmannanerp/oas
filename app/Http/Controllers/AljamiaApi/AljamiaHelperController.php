<?php

namespace App\Http\Controllers\AljamiaApi;

use App\Aljamia\Batch;
use App\Aljamia\Course;
use App\Aljamia\Program;
use App\Aljamia\Teacher;
use App\Aljamia\Department;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AljamiaHelperController extends Controller
{
    public function getDepartments()
    {
        $faculty = request('id');
        if ($faculty) {
            return Department::where('faccode', $faculty)->get();
        }
        return;
    }

    public function getPrograms()
    {
        $department = request('id');
        if ($department) {
            return Program::where('depcode', $department)->get();
        }
        return;
    }

    public function getBatches()
    {
        $program = request('id');
        if ($program) {
            return Batch::where('acadprogcode', $program)->get();
        }
        return;
    }

    public function getTeacherInfo($id, $aljamiaId = null)
    {
        $teacher = Teacher::with(['courses'])->where('id', $id)->first();
        return view('aljamia.teacher-details', compact('teacher'));
    }

    public function getCourseInfo($id, $courseCode)
    {
        $course = Course::with(['teacher'])->where('id', $id)->where('coursecode', $courseCode)->first();
        return view('aljamia.course-details', compact('course'));
    }
}
