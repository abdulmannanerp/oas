<?php

namespace App\Http\Controllers\AljamiaApi;

use App\Aljamia\Batch;
use App\Aljamia\Faculty;
use App\Aljamia\Program;
use App\Aljamia\Department;
use App\Aljamia\Audit;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AljamiaReportController extends Controller
{
    public function courseListingReport()
    {
        $courses = $departments = $programs = $batches = '';
        $faculties = Faculty::all();
        $selectedFaculty = request('faculty');
        if ($selectedFaculty) {
            $departments = Department::where('faccode', $selectedFaculty)->get();
        }
        $selectedDepartment = request('department');
        if ($selectedDepartment) {
            $programs = Program::where('depcode', $selectedDepartment)->get();
        }
        $selectedProgram = request('program');
        if ($selectedProgram) {
            $batches = Batch::where('acadprogcode' ,$selectedProgram)->get();
        }
        $selectedBatch = request('batch');

        if (request()->method() == 'POST') {
            $courses = $this->buildCourseListingReport(
                $selectedFaculty,
                $selectedDepartment,
                $selectedProgram,
                $selectedBatch
            );
        }
        return view('aljamia.course-listing-report', [
            'courses' => $courses,
            'faculties' => $faculties,
            'selectedFaculty' => $selectedFaculty,
            'departments' => $departments,
            'selectedDepartment' => $selectedDepartment,
            'programs' => $programs,
            'selectedProgram' => $selectedProgram,
            'batches' => $batches,
            'selectedBatch' => $selectedBatch
        ]);
    }

    public function buildCourseListingReport($faculty, $department, $program, $batch)
    {
        if ($faculty && $department && $program && $batch) { //all filters selected
            return Batch::with(['courses', 'courses.teacher'])
                ->where('batchcode', $batch)
                ->where('faccode', $faculty)
                ->where('depcode', $department)
                ->where('acadprogcode', $program)
                ->first();
        } elseif ($faculty && $department && $program) { //all filters selected
            return Program::with(['courses', 'courses.teacher'])
                ->where('faccode', $faculty)
                ->where('depcode', $department)
                ->where('acadprogcode', $program)
                ->first();
        } elseif ($faculty && $department) { //only faculty & department selected
            return Department::with(['courses', 'courses.teacher'])
                ->where('faccode', $faculty)
                ->where('depcode', $department)
                ->first();
        } elseif ($faculty) { //only faculty selected
            return Faculty::with(['courses', 'courses.teacher'])
                ->where('faccode', $faculty)
                ->first();
        }
    }

    public function courseListingOverview()
    {
        return view('aljamia.overview');
    }
    public function courseAudit(){
        if (request()->method() == 'POST') {
            $audit = Audit::where('course_id', request('courseId'))->where('teacher_id', request('teacherId'))->where('user_id', auth()->id())->first();
            if($audit){
                Audit::where('id', $audit->id)->update(
                    [
                        'user_id'=> auth()->id(),
                        'q1' => request('q1'),
                        'q2' => request('q2'),
                        'q3' => request('q3'),
                        'q4' => request('q4'),
                        'q5' => request('q5'),
                        'q6' => request('q6'),
                        'q7' => request('q7'),
                        'q8' => request('q8'),
                        'q9' => request('q9'),
                        'q10' => request('q10'),
                        'no_of_weeks' => request('no_of_weeks'),
                        'course_id' => request('courseId'),
                        'semester' => 'SPR-2020',
                        'teacher_id' => request('teacherId'),
                        'comments' => trim(request('comments'))                   
                    ]
                );
            }else{
                Audit::create([
                    'user_id'=> auth()->id(),
                    'q1' => request('q1'),
                    'q2' => request('q2'),
                    'q3' => request('q3'),
                    'q4' => request('q4'),
                    'q5' => request('q5'),
                    'q6' => request('q6'),
                    'q7' => request('q7'),
                    'q8' => request('q8'),
                    'q9' => request('q9'),
                    'q10' => request('q10'),
                    'no_of_weeks' => request('no_of_weeks'),
                    'course_id' => request('courseId'),
                    'semester' => 'SPR-2020',
                    'teacher_id' => request('teacherId'),
                    'comments' => trim(request('comments'))
    
                ]);
            }
            return back()->with('status', 'Information updated successfully!');
        }
        $audit = Audit::where('course_id', request('courseId'))->where('teacher_id', request('teacherId'))->where('user_id', auth()->id())->first();
        return view('aljamia.audit', compact('audit'));
    }
}
