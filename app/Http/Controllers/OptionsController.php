<?php

namespace App\Http\Controllers;

use App\Model\Option;
use Illuminate\Http\Request;

class OptionsController extends Controller
{
    public function index() 
    {
		$optionDeleted = request()->session()->get('optionDeleted');
    	$options = Option::all();
    	return view('admin.options.index')->with('options', $options)->with('optionDeleted', $optionDeleted);
    }

    public function create($id = '')
    {
    	$option = '';
    	if ( $id ) {
    		$option = Option::find($id);
    	}
    	$status = request()->session()->get('status');
    	return view('admin.options.create')->withStatus($status)->withOption($option);
    }

    public function store()
    {
    	$option = Option::where('title', request('title'))->first();
    	if ( !$option ) {
    		Option::create([
	    		'title' => request('title'),
	    		'value' => request('value')
    		]);
    		return back()->with('status', 'Option Saved');
    	}
    	$option->value = request('value');
    	$option->save();
    	return back()->with('status', 'Option Updated');
	}
	
	public function delete( $id ) 
	{
		Option::destroy($id);
		return back()->with('optionDeleted', 'Option deleted Successfully');
	}
}
