<?php

namespace App\Http\Controllers;

use App\Model\FeeCode;
use App\Model\ProgramFee;
use App\Model\Faculty;
use App\Model\Programme;
use App\Model\Department;
use App\Model\Permission;

class ProgramFeeController extends Controller{


    public function index($status=''){
		$permissions = $this->getCurrentUserPermissions();
		if($permissions->fkUserId == 1 || $permissions->fkUserId == 38 || $permissions->fkUserId == 55){
		}else{
			return 'You donot have permission to access this page';
			exit;
		}
		$selectedFilters = [
            'facultyId' => request('faculty'),
            'facultyTitle' => Faculty::find(request('faculty'))->title ?? '',
            'departmentId' => request('department') ?? '',
            'departmentTitle' => Department::find(request('department'))->title ?? '',
            'programmeId' => request('programme') ?? '',
            'programmeTitle' => Programme::find(request('programme'))->title ?? ''
		];
		$selectedDepartments = '';
        $selectedPorgrams = '';
        if ($selectedFilters['facultyId'] != '') {
			$facultyId = $selectedFilters['facultyId'];
			
            $selectedDepartments = $this->getDepartments($facultyId);
        }
        if ($selectedFilters['departmentId'] != '') {
            $departmentId = $selectedFilters['departmentId'];
            $selectedPorgrams = $this->getProgramme($departmentId);
        }
		
		$program = Programme::all();
		$faculties = Faculty::where('status', 1)->get();
		if (request('programme') != '') {
			$ProgramFee = ProgramFee::where('fkProgramId', request('programme'))->get();
			
		}else{
			$ProgramFee = ProgramFee::get();
		}
		return view('programfee.view', compact('ProgramFee', 'program','faculties', 'selectedDepartments', 'selectedPorgrams','status'));
		
	}
	public function addFee($id =''){
		$status = request()->session()->get('status');
		if ( request()->method() == 'POST' ) {
				if(request('id')){
					ProgramFee::where('id', request('id'))->update(
						[
							'admission_fee' =>request('admission_fee'),
							'university_dues' =>request('university_dues'),
							'library_security' =>request('library_security'),
							'book_bank_security' =>request('book_bank_security'),
							'caution_money' =>request('caution_money'),
							'hostel_dues' =>request('hostel_dues'),
							'hostel_security' =>request('hostel_security'),
							'dues_arriers' => request('dues_arriers'),
							'security_arriers' => request('security_arriers'),
							'status' => request('status'),
							'fkSemesterId' => 10,
							'updated_by' => auth()->id()
						]
					);
					$id = request('id');
				}else{
					$ProgramFee = ProgramFee::where('fkProgramId',request('program'))->get();
					if(count($ProgramFee) > 0){
						return back()->with('status', 'Information Already Exists!');
					}
					$Programme = Programme::where('pkProgId',request('program'))->first();
					ProgramFee::create(
						[
							'fkProgramId' => request('program'),
							'fkFacId' => $Programme->fkFacId,
							'fkDepId' => $Programme->fkDepId,
							'admission_fee' =>request('admission_fee'),
							'university_dues' =>request('university_dues'),
							'library_security' =>request('library_security'),
							'book_bank_security' =>request('book_bank_security'),
							'caution_money' =>request('caution_money'),
							'hostel_dues' =>request('hostel_dues'),
							'hostel_security' =>request('hostel_security'),
							'dues_arriers' => request('dues_arriers'),
							'security_arriers' => request('security_arriers'),
							'status' => request('status'),
							'fkSemesterId' => 10,
							'updated_by' => auth()->id()
						]
					);
					return back()->with('status', 'Information updated successfully!');
				}
		}
		$program = Programme::select('pkProgId', 'title', 'duration')->get();
		if ( $id ) {
			$ProgramFee = ProgramFee::where('id',$id)->first();
		}else{
			$ProgramFee='';
		}
		return view ('programfee.add', compact('ProgramFee', 'program', 'status'));    
		
	}

	public function getDepartments($facultyId = '')
    {
        $id = (request('id')) ? request('id') : $facultyId;
        $userHasPermissions = $this->getPermissionsOfCurrentUser();
        $allowedDepartmentsToCurrentUser = $userHasPermissions->fkDepartmentId;

        if ($id) {
            if ($allowedDepartmentsToCurrentUser != '') {
                return Department::where('fkFacId', $id)->whereIn('pkDeptId', explode(',', $allowedDepartmentsToCurrentUser))->get();
            } else {
                return Department::where('fkFacId', $id)->get();
            }
        }
    }


    public function getProgramme($departmentId = '')
    {
        $id = (request('id')) ? request('id') : $departmentId;
        if ($id) {
            return Programme::where('fkDepId', $id)->get();
        }
	}
	
	public function getPermissionsOfCurrentUser()
    {
        return Permission::where('fkUserId', auth()->id())->first();
    }

	  
}
