<?php

namespace App\Http\Controllers;

use App\Model\Merit;
use Illuminate\Http\Request;

class MeritResultController extends Controller
{
    public function index()
    {
    	$programs = Merit::with(['program' => function ( $query ) {
    		$query->select('pkProgId', 'title')->get();
    	}])->select('fkProgId')->distinct()->get();
    	return view ('merit.result.index', compact('programs'));
    }

    public function getResultByProgram( $programId )
    {

    }

    public function searchByRollNumber ( $rollno ) 
    {

    }
}
