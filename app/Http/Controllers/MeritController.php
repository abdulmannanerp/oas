<?php

namespace App\Http\Controllers;

use App\Model;
use App\Model\{Faculty, Merit, MeritList, MeritManage, Result, Programme, RollNumber};
use Illuminate\Support\Facades\Storage;
use League\Csv\Reader;
use League\Csv\Writer;

class MeritController extends Controller
{
    public function index ( $id = '' )  
    {   
        $merit = '';
        if ( $id ) {
            $merit = Merit::find($id);
        }
        $programs = Programme::where('status', '1')->get(); 
        $status = request()->session()->get('status');
        $error = request()->session()->get('error');
    	return view('merit.index', compact('merit', 'programs', 'status', 'error'));
    }

    public function store()
    {
        extract( request()->all() );
        if ( $id ) {
            $merit = $this->updateMerit ( request()->all() );
            return redirect()->route('filestats', ['id' => $merit->id]);
        }
        $meritAlreadyExists = $this->checkIfMeritAlreadyExists( request()->all() );        
        if ( $meritAlreadyExists ) {
            return redirect()->route('merit', ['id' => $meritAlreadyExists->id])->with('error', 'Merit Already Exists');
        }


        $filename = $programme. '-'. 
                    $selectGender. '-'. 
                    $resultType. '-'. 
                    $userId. '-'.  
                    \Carbon\Carbon::now(). '.csv';
    	$pathToFile = request()->file('meritFile')->storeAs('merit', $filename);

        $program = Programme::where('pkProgId', $programme)->select('pkProgId', 'fkFacId', 'fkDepId')->first();
        $merit = Merit::create([
            'fkFacId' => $program->fkFacId,
            'fkDeptId' => $program->fkDepId,
            'fkProgId' => $programme,
            'gender' => $selectGender,
            'type' => $resultType,
            'factor' => $factor,
            'path' => $pathToFile, 
            'user_id' => $userId
        ]);

        return redirect()->route('filestats', ['id' => $merit->id]);
    }

    public function updateMerit( $data )
    {
        $data = (object)$data;
        $merit  = Merit::find($data->id);
        if ( $merit ) {
            $filename = $merit->fkProgId. '-'. 
                    $merit->gender. '-'. 
                    $merit->type. '-'. 
                    $merit->user_id. '-'.  
                    \Carbon\Carbon::now(). '.csv';
            // $pathToFile = $data->meritFile->storeAs('merit', $filename);
            $pathToFile = Storage::putFileAs(
                'merit', $data->meritFile, $filename
            );
            $program = Programme::where('pkProgId', $merit->fkProgId)->select('pkProgId', 'fkFacId', 'fkDepId')->first();
        }
        
        $merit->fkFacId = $program->fkFacId;
        $merit->fkDeptId = $program->fkDepId;
        $merit->fkProgId = $data->programme;
        $merit->gender = $data->selectGender;
        $merit->type = $data->resultType;
        $merit->factor = $data->factor;
        $merit->path = $pathToFile;
        $merit->user_id = $data->userId;
        $merit->save();
        return $merit;
    }

    public function checkIfMeritAlreadyExists( $data )
    {
        $data = (object)($data);
        return Merit::where([
            ['fkProgId', $data->programme],
            ['gender', $data->selectGender],
            ['type', $data->resultType]
        ])->first();
    }

    public function fileStats( $meritId )
    {
        $merit = Merit::with(['program' => function($query){
            $query->select('pkProgId', 'title')->get();
        }])->find( $meritId );
        if ( $merit ) {
            $csv = Reader::createFromPath(storage_path('app/'.$merit->path), 'r');
            $records = $csv->setOffset(1)->fetchAll();

            $rollNumbers = $marks = [];
            foreach ( $records as $record ) {
                $rollNumbers[] = $record[0];
                $marks[] = $record[1];
            }

            $totalCount = count($rollNumbers);
            $unique = array_unique ( $rollNumbers );
            $uniqueCount = count (array_unique ( $rollNumbers ));
            $duplicates = array_unique(array_diff_assoc($rollNumbers, array_unique($rollNumbers)));
            $duplicateCount = $totalCount-$uniqueCount;
            return view('merit.file-stats', compact('totalCount', 'unique', 'uniqueCount', 'duplicates', 'duplicateCount', 'merit'));
        }
    }

    public function viewFile ( $meritId ) 
    {
        $merit = Merit::with(['program'])->find($meritId);
        if ( $merit ) {
            $csv = Reader::createFromPath(storage_path('app/'.$merit->path), 'r');
            $records = $csv->setOffset(1)->fetchAll();
            return view('merit.view-file', compact('merit', 'records'));
        }
    }

    
    public function storeMerit()  
    {
        extract( request()->all() );
        $merit = Merit::find ( $id );
        if ( $merit ) {
            $csv = Reader::createFromPath(storage_path('app/'.$merit->path), 'r');
            $records = $csv->setOffset(1)->fetchAll();
            $rollNumbers = $marks = $rollNumberIndexedMarks = [];
            foreach ( $records as $record ) {
                $rollNumbers[] = $record[0];
                $marks[] = $record[1];
                $rollNumberIndexedMarks[$record[0]] = $record[1];
            }

            /* $rollNumberIndexedMarks = [];
            Marks can not be brought by query below, that's why this array is introducted to do every thing by one query */
            $meritListRecords = RollNumber::with(['previousQualification' => function($query){
                $query->select('id', 'fkApplicantId', 'pqm')->get();
            }])->whereIn('id', $rollNumbers)->get();

            $test_or_interview = ($merit->type == 'Result') ? 'test_marks' : 'interview_marks';
            foreach ( $meritListRecords as $record ) {
                MeritList::create([
                    'merit_id' =>  $merit->id ?? '',
                    'rollno' => $record->id ?? '',
                    $test_or_interview =>  $rollNumberIndexedMarks[$record->id] ?? '',
                    'pqm' => $record->previousQualification->pqm ?? '',
                    'total' => ((float)$rollNumberIndexedMarks[$record->id] + (float)$record->previousQualification->pqm) ?? ''
                ]);
            }
        }
    	return redirect()->route('viewFileWithPqm', ['id' => $merit->id]);
    }

    public function viewFileWithPqm( $id ) 
    {
        $merit = Merit::with(['program','meritList'])->find($id);
        if ( $merit ) {
            return view('merit.view-file-with-pqm', compact('merit'));
        }
    }

    public function getGeneratedMeritLists()
    {
        $lists = Merit::with(['program'])->latest()->take(20)->get();
        return view('merit.listing', compact('lists'));
    }

    public function manage( $id )
    {
        $merit = Merit::with(['program'])->find($id);
        if ( $merit ) {
            $status = request()->session()->get('status') ?? '';
            $error = request()->session()->get('error') ?? '';
            $programCutPoints = MeritManage::where('merit_id' , $id)->get();
            // $recentCutPoints = MeritManage::with([
            //     'faculty' => function( $query )
            //     { 
            //         $query->select('pkFacId', 'title')->get();
            //     }, 
            //     'department' => function( $query )
            //     {
            //         $query->select('pkDeptId', 'title')->get();
            //     }, 
            //     'program' => function ( $query )
            //     {
            //         $query->select('pkProgId', 'title')->get();
            //     }, 
            //     'user' => function ( $query ) {
            //         $query->select('id', 'first_name', 'last_name', 'email')->get();
            //     }
            // ])->latest()->take(10)->get();
            return view('merit.manage', compact('merit', 'status', 'error', 'programCutPoints'));
        }   
    }

    public function storeMeritCutpoint() 
    {
        extract ( request()->all() );
        $ifListExists = MeritManage::where('merit_id', $merit)->where('list', $listNumber)->first();
        if ( $ifListExists ) {
            return back()->with('error', $listNumber.' list already exists');
        }
        MeritManage::create([
            'user_id' => $userId,
            'merit_id' => $merit,
            'cut_point' => $cutpoint,
            'list' => $listNumber
        ]);
        return back()->with('status', 'Cut Point Added Successfully');
    }

    public function searchListing()
    {
        extract( request()->all() );
        return redirect()->route('manage', ['id' => $program]);
    }

    /** Results */
    public function resultsIndex()
    {
        $programsWithMeritUploaded = Merit::select('fkProgId')->distinct()->get()->pluck('fkProgId');
        $programs = Programme::whereIn('pkProgId', $programsWithMeritUploaded)->get();
        return view('merit.front.public', compact('programs'));
    }

    public function postMeritResultListing()
    {
        if ( request('helper') == 'list' ) {
            return $this->getSearchedResults( request('programme') );
        } else if ( request('helper') == 'rollno' ) {
            return $this->getResultByRollNumber( request('rollno') );
        }
    }

    public function getSearchedResults( $programId ) 
    {
        return redirect()->route('getMeritResultListing', ['programId' => $programId]);
    }

    public function getMeritResultListing( $programId ) 
    {
        $merit = Merit::where('fkProgId', $programId)->get()->pluck('id');
        $getLists = MeritManage::with(['merit', 'merit.program'])->whereIn('merit_id', $merit)->where('status', '1')->get();
        if ( $getLists ) {
            return view('merit.front.recent', compact('getLists'));    
        }
    }

    public function getSingleList( $id )  
    {
        $merit = MeritManage::with(['merit', 'merit.program'])->find($id);
        if ( $merit ) {
            $allLists = MeritManage::where('merit_id', $merit->merit_id)->get();
            $offset = $this->getOffset($merit->list, $allLists);
            $records = MeritList::with(['getRollNo', 'getRollNo.applicant', 'getRollNo.applicant.detail'])->where('merit_id' , $merit->merit_id)->orderBy('total', 'desc')->offset($offset)->take($merit->cut_point)->get();
            return view('merit.front.list', compact('merit', 'records'));
        }
    }

    public function getResultByRollNumber( $rollno ) 
    {
        return 'get result by rollno';
    }

    public function publishOrSuspendList( $id )
    {
        $manage = MeritManage::find($id);
        if ( $manage ) {
            $manage->status = !($manage->status);
            $manage->save();
        }
        return back();
    }

    /* View cut point file */
    public function viewList ( $id, $listToShow )  
    {
        $merit = MeritManage::with(['merit'])->find($id);
        $allLists = MeritManage::where('merit_id', $merit->merit_id)->get();
        if ( $merit ) {
            $recordsToShow = $merit->cut_point;
            switch ($listToShow) {
                case 'First':
                    $offset = $this->getOffset('First', $allLists);
                    return $this->viewCutPointList ($offset, $merit);
                    break;

                case 'Second':
                    $offset = $this->getOffset('Second', $allLists);
                    return $this->viewCutPointList ($offset, $merit);
                    break;

                case 'Third':
                    $offset = $this->getOffset('Third', $allLists);
                    return $this->viewCutPointList ($offset, $merit);
                    break;

                case 'Fourth':
                    $offset = $this->getOffset('Fourth', $allLists);
                    return $this->viewCutPointList ($offset, $merit);
                    break;

                case 'Fifth':
                    $offset = $this->getOffset('Fifth', $allLists);
                    return $this->viewCutPointList ($offset, $merit);
                    break;

                case 'Sixth':
                    $offset = $this->getOffset('Sixth', $allLists);
                    return $this->viewCutPointList ($offset, $merit);
                    break;
                
                default:
                    $offset = $this->getOffset();
                    return $this->viewCutPointList ($offset, $merit);
                    break;
            }
        }
    }

    public function getOffset($Currentlist, $allLists)
    {
        $offset = [];
        foreach ( $allLists as $list ) {
             if ( $list->list == $Currentlist ) {
                break;
            }
            $offset[] = $list->cut_point;
        }
        return array_sum($offset);
    }

    public function viewCutPointList($offset, $merit)
    {
        $records = MeritList::where('merit_id' , $merit->merit_id)->orderBy('total', 'desc')->offset($offset)->take($merit->cut_point)->get();
        return view ('merit.view-list', compact('records'));
    }   
}
