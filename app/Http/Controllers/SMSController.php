<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SMSController extends Controller
{
    public function index()
    {
        $status = request()->session()->get('status');
        return view ( 'sms.index')->with('status', $status);
    }

    public function store()
    {
        $phone = request('phone');
        $body = request('body');
//        $url = "http://api.bizsms.pk/api-send-branded-sms.aspx?username=iiu@bizsms.pk&pass=i1u3drtj99&text=".$body."&masking=IIU&destinationnum=".$phone."&language=English";
        $url = "http://api.bizsms.pk/api-send-branded-sms.aspx?username=iiu@bizsms.pk&pass=i1u3drtj99&text=Awesome&masking=IIU&destinationnum=923338999963&language=English";

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 300,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_HTTPHEADER => array(
                "Cache-Control: no-cache",
                "Postman-Token: 0f4b3a3f-c9e2-40fc-85a4-971282bbce35",
                "Content-Length: 825"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            dd ( $err );
        } else {
            dd ( $response );
            return redirect()->back()->with('status', 'Message sent');
        }
    }
}
