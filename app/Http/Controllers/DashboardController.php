<?php 

namespace App\Http\Controllers;

use App\Http\Controllers;
use App\Model\Semester;
use App\Model\{Applicant, Application, Faculty, Permission, Programme, Department};
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Cache;

class DashboardController extends Controller
{
	protected $getCount = [];
	protected $facultyStatsArray = [];
	protected $female = 2;
	protected $male = 3;
	protected $pakistani = 1;
	protected $overseasPakistani = 2;
	protected $overseas = 3;
	protected $selectedSemester;

    public function __construct()
    {
        //$this->selectedSemester = $this->getCurrentSemester()->pkSemesterId;
    }
	
	public function index(Request $request)
	{
		
		$groupId = session()->get('gid');
		$semester = Semester::where('pkSemesterId', '>', '10')->get();
		
		$active_semester = Semester::where('status', '1')->first()->pkSemesterId;
		$cache_semester = Cache::get(auth()->id().'-active_semester');
		
		if(empty($cache_semester)){
			Cache::forever(auth()->id().'-active_semester', $active_semester);
		}
		if ( request()->method() == 'POST' ) {
			Cache::forever(auth()->id().'-active_semester', $request->semester_id);
		}
		return view('dashboard.limited-access', compact('groupId', 'semester'));
	}

	public function refreshStatsAndAddToCache( $semester = '' )
	{
		$selectedSemester = $this->selectedSemester = $semester;
		if ( $semester == '' ) 
			$selectedSemester = $this->selectedSemester = Semester::where('status', '1')->first()->pkSemesterId;
		Cache::forever($this->selectedSemester.'-stats-overview', $this->getStatsOverview());
		Cache::forever($this->selectedSemester.'-male-applications-stats', $this->getMaleApplicationsStats());
		Cache::forever($this->selectedSemester.'-female-applications-stats', $this->getFemaleApplicationsStats());
		Cache::forever($this->selectedSemester.'-total-revenue-stats',$this->getTotalRevenueStats());
		Cache::forever($this->selectedSemester.'-top-programs', $this->getTopPrograms());
		Cache::forever($this->selectedSemester.'-faculty-stats',$this->getFacultyStats());
		return 'Cache Refreshed';
	}

	public function forceRefreshStats()
	{
		$this->refreshStatsAndAddToCache();
		return $this->dashboardStats();
	}

	public function getStatsOverview()
	{
		/** Total Applications = Fee Verified Applications */
		$allStats = [];
		$allStats['totalFormsIssues'] = Application::select('id', 'fkApplicantId')
			->where('fkSemesterId', $this->selectedSemester)
			->whereHas('applicant')
			->count();

		$allStats['totalApplications'] = Application::where('fkCurrentStatus', '>=', '4')
			->where('fkSemesterId', $this->selectedSemester)
			->whereNotNull('feeVerified')
			->count();

		$allStats['totalFeeUnverified'] = Application::where('fkCurrentStatus', '>', '1')
			->where('fkSemesterId', $this->selectedSemester)
			->where('fkCurrentStatus', '<', '4')
			->whereNull('feeVerified')
			->whereNotNull('bankDetails')
			->count();

		$allStats['totalDocumentsPending'] = Application::where('fkCurrentStatus', '=', '4')
			->where('fkSemesterId', $this->selectedSemester)
			->whereNotNull('feeVerified')
			->count();

		$allStats['totalApprovedApplications'] = Application::where('fkCurrentStatus', '=', '5')
			->where('fkSemesterId', $this->selectedSemester)
			->count();

		$allStats['totalRejectedApplications'] = Application::where('fkCurrentStatus', '=', '6')
			->where('fkSemesterId', $this->selectedSemester)
			->count();

		// $allStats['totalOverseasApplications'] = Application::where('fkSemesterId', $this->selectedSemester)
		// 	->whereHas('applicant', function ($query) {
		// 		$query->where('fkNationality', '3');
		// 	})->count();

		return $allStats;
	}

	public function getMaleApplicationsStats()
	{
		/** Total Applications = Fee Verified Applications */
		$allMaleStats = [];
		$allMaleStats['totalMaleApplications'] = Application::where('fkSemesterId', $this->selectedSemester)
			->whereHas('applicant', function ($query) {
				$query->where('fkGenderId', $this->male);
			})
			->where('fkCurrentStatus', '>=', '4')
			->whereNotNull('feeVerified')
			->count();

		$allMaleStats['totalMaleFormsIssued'] = Application::where('fkSemesterId', $this->selectedSemester)
			->whereHas('applicant', function ($query) {
				$query->where('fkGenderId', $this->male);
			})->count();

		$allMaleStats['totalFeeUnverifiedMaleApplications'] = Application::where('fkSemesterId', $this->selectedSemester)
			->whereHas('applicant', function ($query) {
				$query->where('fkGenderId', $this->male);
			})->where('fkCurrentStatus', '>', '1')
			->where('fkCurrentStatus', '<', '4')
			->whereNull('feeVerified')
			->whereNotNull('bankDetails')
			->count();

		$allMaleStats['totalMaleDocumentsPending'] = Application::where('fkSemesterId', $this->selectedSemester)
			->whereHas('applicant', function ($query) {
				$query->where('fkGenderId', $this->male);
			})
			->where('fkCurrentStatus', '=', '4')
			->whereNotNull('feeVerified')
			->count();

		$allMaleStats['totalMaleApprovedApplications'] = Application::where('fkSemesterId', $this->selectedSemester)
			->whereHas('applicant', function ($query) {
				$query->where('fkGenderId', $this->male);
			})
			->where('fkCurrentStatus', '=', '5')
			->count();

		$allMaleStats['totalMaleRejectedApplications'] = Application::where('fkSemesterId', $this->selectedSemester)
			->whereHas('applicant', function ($query) {
				$query->where('fkGenderId', $this->male);
			})
			->where('fkCurrentStatus', '=', '6')
			->count();

		// $allMaleStats['totalOverseasMaleApplications'] = Application::where('fkSemesterId', $this->selectedSemester)
		// 	->whereHas('applicant', function ($query) {
		// 		$query->where('fkNationality', '3')->where('fkGenderId', $this->male);
		// 	})->count();

		return $allMaleStats;
	}

	public function getFemaleApplicationsStats()
	{
		/** Total Applications = Fee Verified Applications */
		$allFemaleStats = [];
		$allFemaleStats['totalFemaleApplications'] = Application::where('fkSemesterId', $this->selectedSemester)
			->whereHas('applicant', function ($query) {
				$query->where('fkGenderId', $this->female);
			})
			->where('fkCurrentStatus', '>=', '4')
			->whereNotNull('feeVerified')
			->count();

		$allFemaleStats['totalFemaleFormsIssued'] = Application::where('fkSemesterId', $this->selectedSemester)
			->whereHas('applicant', function ($query) {
				$query->where('fkGenderId', $this->female);
			})->count();

		$allFemaleStats['totalFeeUnverifiedFemaleApplications'] = Application::where('fkSemesterId', $this->selectedSemester)
			->whereHas('applicant', function ($query) {
				$query->where('fkGenderId', $this->female);
			})->where('fkCurrentStatus', '>', '1')
			->where('fkCurrentStatus', '<', '4')
			->whereNull('feeVerified')
			->whereNotNull('bankDetails')
			->count();

		$allFemaleStats['totalFemaleApprovedApplications'] = Application::where('fkSemesterId', $this->selectedSemester)
			->whereHas('applicant', function ($query) {
				$query->where('fkGenderId', $this->female);
			})
			->where('fkCurrentStatus', '=', '5')
			->count();

		$allFemaleStats['totalFemaleDocumentsPending'] = Application::where('fkSemesterId', $this->selectedSemester)
			->whereHas('applicant', function ($query) {
				$query->where('fkGenderId', $this->female);
			})
			->where('fkCurrentStatus', '=', '4')
			->whereNotNull('feeVerified')
			->count();

		$allFemaleStats['totalFemaleRejectedApplications'] = Application::where('fkSemesterId', $this->selectedSemester)
			->whereHas('applicant', function ($query) {
				$query->where('fkGenderId', $this->female);
			})
			->where('fkCurrentStatus', '=', '6')
			->count();

		// $allFemaleStats['totalOverseasFemaleApplications'] = Application::where('fkSemesterId', $this->selectedSemester)
		// 	->whereHas('applicant', function ($query) {
		// 		$query->where('fkNationality', '3')->where('fkGenderId', $this->female);
		// 	})->count();

		return $allFemaleStats;
	}

	public function getTotalRevenueStats()
	{
		$revenueStats = [];
		$revenueStats['maleEngineeringApplications'] = Application::where('fkSemesterId', $this->selectedSemester)
			->whereHas('applicant', function ($query) {
				$query->where('fkGenderId', $this->male)->where('fkNationality', '=', '1');
			})
			->where('fkCurrentStatus', '>=', '4')
			->where('fkProgramId', '=', '189')
			->count();

		$revenueStats['maleNonEngineeringApplications'] = Application::where('fkSemesterId', $this->selectedSemester)
			->whereHas('applicant', function ($query) {
				$query->where('fkGenderId', $this->male)->where('fkNationality', '=', '1');
			})
			->where('fkCurrentStatus', '>=', '4')
			->where('fkProgramId', '!=', '189')
			->count();

		$revenueStats['femaleEngineeringApplications'] = Application::where('fkSemesterId', $this->selectedSemester)
			->whereHas('applicant', function ($query) {
				$query->where('fkGenderId', $this->female)->where('fkNationality', '=', '1');
			})
			->where('fkCurrentStatus', '>=', '4')
			->where('fkProgramId', '=', '189')
			->count();

		$revenueStats['femaleNonEngineeringApplications'] = Application::where('fkSemesterId', $this->selectedSemester)
			->whereHas('applicant', function ($query) {
				$query->where('fkGenderId', $this->female)->where('fkNationality', '=', '1');
			})
			->where('fkCurrentStatus', '>=', '4')
			->where('fkProgramId', '!=', '189')
			->count();

		// $revenueStats['overseasMaleApplications'] = Application::where('fkSemesterId', $this->selectedSemester)
		// 	->whereHas('applicant', function ($query) {
		// 		$query->where('fkGenderId', $this->male)->whereIn('fkNationality', [$this->overseasPakistani, $this->overseas]);
		// 	})
		// 	->where('fkCurrentStatus', '>=', '4')->count();

		// $revenueStats['overseasFemaleApplications'] = Application::where('fkSemesterId', $this->selectedSemester)
		// 	->whereHas('applicant', function ($query) {
		// 		$query->where('fkGenderId', $this->female)->whereIn('fkNationality', [$this->overseasPakistani, $this->overseas]);
		// 	})
		// 	->where('fkCurrentStatus', '>=', '4')
		// 	->count();

		return $revenueStats;
	}

	public function getTopPrograms($recordLimit = 5)
	{
		return Application::with(['program'])
			->where('fkSemesterId', $this->selectedSemester)
			->select('id','fkProgramId', DB::raw('COUNT(fkProgramId) AS programCount'))
			->groupBy('fkProgramId')
			->orderBy('programCount', 'DESC')
			->limit($recordLimit)
			->where('fkCurrentStatus', '>=', 4)
			->get()
			->toArray();
	}

	public function getFacultyStats()
	{
		$allFacultiesStats = [];
		$faculties = Faculty::all();

		foreach ($faculties as $faculty) {
			$facultyStats = [];
			$facultyStats['title'] = $faculty->title;
			// $facultyStats['feeVerifiedCount'] = $faculty->applications()->where('fkSemesterId', $this->selectedSemester)->where('fkCurrentStatus', '>=', '4')->count();
			$facultyStats['feeVerifiedCount'] = Application::where('fkSemesterId', $this->selectedSemester)
				->where('fkFacId', $faculty->pkFacId)
				->where('fkCurrentStatus', '>=', 4)
				->count();
			// $facultyStats['count'] = $faculty->departments->count();
			$facultyStats['count'] = Department::where('fkFacId', $faculty->pkFacId)->count();
			// $applications = $faculty->applications->where('fkSemesterId', $this->selectedSemester)->pluck('id');

			// $facultyStats['totalMaleApplications'] = Application::with(['applicant'])->where('fkSemesterId', $this->selectedSemester)->whereIn('id', $applications)->whereHas('applicant', function ($query) {
			// 	$query->where('fkGenderId', 3);
			// })->get()->count();

			$facultyStats['totalMaleApplications'] = Application::with(['applicant'])
				->where('fkSemesterId', $this->selectedSemester)
				->where('fkFacId', $faculty->pkFacId)
				->whereHas('applicant', function($query){
					$query->where('fkGenderId', $this->male);
				})
				->count();

			$facultyStats['totalFemaleApplications'] = Application::with(['applicant'])
				->where('fkSemesterId', $this->selectedSemester)
				->where('fkFacId', $faculty->pkFacId)
				->whereHas('applicant', function ($query) {
					$query->where('fkGenderId', $this->female);
				})
				->count();

			array_push($allFacultiesStats, $facultyStats);
		}
		return $allFacultiesStats;
	}
	
	public function dashboardStats( $semester = '' ) 
	{
		$selectedSemester = $this->selectedSemester = $semester;
		if ( $semester == '' ) 
			$selectedSemester = $this->selectedSemester = Semester::where('status', '1')->first()->pkSemesterId;
		if (!Cache::has($this->selectedSemester.'-faculty-stats')) {
			$this->refreshStatsAndAddToCache();
		}
		$statsOverview = Cache::get($this->selectedSemester.'-stats-overview');
		$maleApplicationsStats = Cache::get($this->selectedSemester.'-male-applications-stats');
		$femaleApplicationsStats = Cache::get($this->selectedSemester.'-female-applications-stats');
		$totalRevenue = Cache::get($this->selectedSemester.'-total-revenue-stats');
		$topPrograms = Cache::get($this->selectedSemester.'-top-programs');
		$facultyStats = Cache::get($this->selectedSemester.'-faculty-stats');
		$semesters = Semester::all();
		return view('dashboard.dashboard', compact('statsOverview', 'maleApplicationsStats', 'femaleApplicationsStats', 'totalRevenue', 'topPrograms', 'facultyStats', 'semesters', 'selectedSemester'));
	}

	public function getSpecificSemesterStats()
	{
		return redirect()->route('showDashboardStats', ['semester' => request('semesterSelector')]);
	}

	/** Needs to be fixed */
	public function mapQuery($id=''){
		if($id){
			$q="SELECT 
			d.pkDistId as code, count(d.pkDistId) as value
			FROM 
			tbl_oas_applications as ap
			inner join 
			tbl_oas_applicant_detail as ad
			on ap.fkApplicantId = ad.fkApplicantId
			inner join 
			tbl_oas_district as d
			on ad.fkDistrict=d.pkDistId
			inner join 
			tbl_oas_province as p
			on d.fkProvId=p.pkProvId
			where d.fkProvId=$id
			AND
			ap.fkCurrentStatus >= 4
			AND
			ap.fkSemesterId = ".Cache::get(auth()->id().'-active_semester')."
			 group by d.pkDistId";

		}else{
			$q="SELECT 
			p.pkProvId as code, count(ad.id) as value
			FROM 
			tbl_oas_applications as ap
			inner join 
			tbl_oas_applicant_detail as ad
			on ap.fkApplicantId = ad.fkApplicantId
			inner join 
			tbl_oas_district as d
			on ad.fkDistrict=d.pkDistId
			inner join 
			tbl_oas_province as p
			on d.fkProvId=p.pkProvId
			where ap.fkCurrentStatus >= 4
			AND
			ap.fkSemesterId = ".Cache::get(auth()->id().'-active_semester')."
			 group by p.pkProvId";
			$query = \DB::select($q);
		}
		$query = \DB::select($q);
		return $query;
	}

	public function parseToXML($htmlStr){
    	$xmlStr=str_replace('<','&lt;',$htmlStr);
    	$xmlStr=str_replace('>','&gt;',$xmlStr);
    	$xmlStr=str_replace('"','&quot;',$xmlStr);
    	$xmlStr=str_replace("'",'&#39;',$xmlStr);
    	$xmlStr=str_replace("&",'&amp;',$xmlStr);
    	return $xmlStr;
	}
}