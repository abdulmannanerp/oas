<?php

namespace App\Http\Controllers;

use DB;
use App\User;
use Carbon\Carbon;
use App\Model\Status;
use League\Csv\Reader;
use League\Csv\Writer;
use SplTempFileObject;
use App\Jobs\SendEmail;
use App\Model\oasEmail;
use App\Model\Programme;
use App\Model\Permission;
use League\Csv\Statement;
use App\Model\Application;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\FeeVerificationFailed;
use App\Model\ChallanDetail;
use App\Model\Semester;
use App\Jobs\SendSMS;


class ChallanVerificationController extends Controller
{
    protected $verified = [];
    protected $unverified = [];
    protected $verified_applications = '';
    protected $unverified_applications = '';

    protected $export_applications_with_fee_verified;

    public function index() {
        $status = request()->session()->get('status');
        $applications = '';

        if ( request()->method() == 'POST' ) {
            if ( request('challanverify') ) {
                $unSerialzedApplicationIds = unserialize( request('challanverify') );
                $data = ChallanDetail::whereIn('id', $unSerialzedApplicationIds)->groupBy('fkRollNumber')->get();
                $abbrev = 'AdmissionMale';
                $data_email = array('name' => 'dummy');
                DB::transaction(function () use ( $unSerialzedApplicationIds, $data, $abbrev, $data_email ) {
                    $applications = ChallanDetail::whereIn('id', $unSerialzedApplicationIds)->update(
                        [
                            'fee_verified_by' => auth()->guard('web')->id(),
                            'fee_verified_date' => date('Y-m-d H-i-s'),
                            'status' => 4
                        ]
                    );
                    foreach($data as $d){
                        $message = '';
                        $message = urlencode('Thank you for selecting IIUI for advancement of your educational endeavours. In order to confirm your joining/admission, you will be required to submit all the credentials, 5 passport sized photograhps, domicile, CNIC/B-form and original educational documents (for verification only) within 15 days,  but not later than 30th September. Admission Office, IIUI.');
                        try {
                            SendEmail::dispatch($abbrev, $data_email, $d->rollNumber->applicat->email, 'IIU Admission Offer', 'applications.offer');
                            SendSMS::dispatch($message, $d->rollNumber->applicat->detail->mobile);
                        } catch (Exception $e) {
                            Log::info('Unable to send semester challan verification email and message to ' .$d->rollNumber->applicat->userId .' '. $e);
                        }
                    }
                });
                return back()->with('status', 'Records Verified Successfully' );
            }
            if ( request()->hasFile('csv') ) {
                $applicationIds = $this->getRecoredsFromCsv();
                $applications = $this->checkPermissionsAndListRecords( $applicationIds );
            } else {
                $applicationIds = request('application_ids');
                $applicationIds = $this->parseDataToArray( $applicationIds );
                $applications = $this->checkPermissionsAndListRecords( $applicationIds );
            }
        }
        return view ('challan.verify', compact('status', 'applications'));
    }


    public function checkPermissionsAndListRecords ( $applicationIds ) 
    {
        $permissionsArray = $this->convertPermissiontoArray();
        $applications = ChallanDetail::with([
            'applicant','applicant.applicant','program',
            'user' => function ( $query ) {
                $query->select('*');
            }])->whereHas('applicant.applicant', function ( $query ) use ( $permissionsArray ) {
                $query->whereIn('fkGenderId', $permissionsArray);
            })->whereIn('id', $applicationIds )->orWhere(function($query) use($applicationIds){
                $query->whereIn('second_challan', $applicationIds);
            })->select('*')->get();
        return $applications;
    }

    public function getRecoredsFromCsv()
    {
        $applicationIds = [];
        $file = request()->file('csv');
        request('csv')->storeAs('public', 'fee-verification-file.csv');
        $csv = Reader::createFromPath(storage_path('app/public/') . 'fee-verification-file.csv', 'r');
        $records = $csv->setOffset(1)->fetchAll();
        if ( $records ) {
            foreach ( $records as $record ) {
                $applicationIds[] = $record[0];
            }
        }
        return $applicationIds;
    }

    public function parseDataToArray( $data ) {
        $data = trim ( $data );
        $delimeter = ' ';
        if ( strpos ( $data, ',') !== false ) : 
            $delimeter = ',';
        endif;
        if ( strpos($data, PHP_EOL) !== false ) : 
            $delimeter = PHP_EOL;
        endif;
        if ( $delimeter == PHP_EOL ) {
            return preg_split("/\\r\\n|\\r|\\n/", $data);
        }
        return  explode($delimeter, $data);
    }

    public function verified()
    {
        $startDate = request('start_date');
        $endDate = request('end_date');
        $export = request('export');
        $applications = $this->getApplicationsByStatus('fee_verified_date', $startDate, $endDate, '>=', '4', $export);
        if ( request()->method() == 'POST' ) {
            $this->exportRecordsToCsv( $applications, 'semester-fee-verified-' );
        }
        return view ('challan.verified', compact('applications', 'startDate', 'endDate'));
    }

    public function getApplicationsByStatus($column, $startDate, $endDate, $operator, $status, $export)
    {
        $permissionsArray = $this->convertPermissiontoArray();
        $semester = Semester::where('status', 1)->first();
        if ( $startDate && $endDate ) {
            /* Avoid appending multiple time to date $startDate = $startDate.' 00:00:00'; */
            if ( count ( explode ( ' ', $startDate ) ) <= 1 )  {
                $startDate = $startDate.' 00:00:00';
                $endDate = $endDate.' 23:59:59';
            }
            $applications =  ChallanDetail::with(['applicant','applicant.applicant', 'program', 'user'])->whereHas('applicant.applicant', function ( $query ) use ( $permissionsArray ) {
                $query->whereIn('fkGenderId', $permissionsArray);
            })->whereBetween($column, [$startDate, $endDate])->whereIn('challan_type', array(2, 3))->where('fkSemesterId', $semester->pkSemesterId)->where('status', $operator, $status)->orderBy($column, 'DESC');
            if ( $export ) {
                return $applications->get();
            }
            return $applications->paginate(100);
        } else {
            $applications = ChallanDetail::with(['applicant','applicant.applicant', 'program', 'user'])->whereHas('applicant.applicant', function ( $query ) use ( $permissionsArray ) {
                $query->whereIn('fkGenderId', $permissionsArray);
            })->where('fkSemesterId', $semester->pkSemesterId)->whereIn('challan_type', array(2, 3))->where('status', $operator, $status)->orderBy($column, 'DESC');
            if ( $export ) {
                return $applications->get();
            }
            return $applications->paginate(100);
        }
    }

    public function convertPermissiontoArray() {
        $permissions = $this->getCurrentUserPermissions()->fkGenderId;
        $permissionsArray = $this->parseDataToArray($permissions);
        if ( $permissions == 1 ) {
            $permissionsArray = ['2', '3'];
        }
        return $permissionsArray;
    }

    public function exportRecordsToCsv( $applications, $filename )
    {
        $csv = Writer::createFromFileObject(new SplTempFileObject());
        if ( $filename == 'semester-fee-verified-' ) {
            $csv->insertOne(['Challan Number', 'Name','Roll #','CNIC','Faculty','Department', 'Program','Admission Fee','University Dues','Library Security','Book Bank Security','Caution Money','Total', 'Verification Date']);
            foreach ( $applications as $application ) {
                if($application->second_challan != ''){
                $ch_idd = $application->second_challan;
                }else{
                 $ch_idd = $application->id;
                }
                $csv->insertOne(
                    [
                        $ch_idd ?? '', 
                        $application->applicant->applicant->name ?? '',
                        $application->applicant->getRollNumber->id ?? '',
                        $application->applicant->applicant->cnic ?? '',
                        $application->program->faculty->title ?? '',
                        $application->program->department->title ?? '',
                        $application->program->title ?? '',
                        $application->admission_fee?? '',
                        $application->university_dues?? '',
                        $application->library_security?? '',
                        $application->book_bank_security?? '',
                        $application->caution_money?? '',
                        $application->admission_fee+$application->university_dues+$application->library_security+$application->book_bank_security+$application->caution_money,
                        date ( 'd-M-Y h:i A',  strtotime($application->fee_verified_date)) ?? ''
                    ]
                );
            }
        } else {
            $csv->insertOne(['Challan Number', 'Name','Roll #','CNIC','Faculty','Department', 'Program','Admission Fee','University Dues','Library Security','Book Bank Security','Caution Money','Total']);
            foreach ( $applications as $application ) {
                if($application->second_challan != ''){
                    $ch_idd = $application->second_challan;
                    }else{
                     $ch_idd = $application->id;
                    }

                $csv->insertOne(
                    [
                        $ch_idd ?? '', 
                        $application->applicant->applicant->name ?? '',
                        $application->applicant->getRollNumber->id ?? '',
                        $application->applicant->applicant->cnic ?? '',                        
                        $application->program->faculty->title ?? '',
                        $application->program->department->title ?? '',
                        $application->program->title ?? '',
                        $application->admission_fee?? '',
                        $application->university_dues?? '',
                        $application->library_security?? '',
                        $application->book_bank_security?? '',
                        $application->caution_money?? '',
                        $application->admission_fee+$application->university_dues+$application->library_security+$application->book_bank_security+$application->caution_money
                    ]
                );
            }
        }
        
        $currentUser = User::find(auth()->id());
        $currentDate = date('d-m-Y h:i:s A');
        $message = "Report generated by $currentUser->first_name $currentUser->last_name on $currentDate via Online Admission System";
        $csv->insertOne([$message]);
        $fileName = $filename.$currentDate.'.csv';
        $csv->output($fileName);
        die();
    }

    public function unverified()
    {
        $startDate = request('start_date');
        $endDate = request('end_date');
        $export = request('export');        
        $applications = $this->getApplicationsByStatus('fee_verified_date', $startDate, $endDate, '=', '1', $export);
        if ( request()->method() == 'POST' ) {
            $this->exportRecordsToCsv($applications, 'semester-fee-unverified-', $export);
        }
        return view ('challan.unverified', compact('applications', 'startDate', 'endDate'));
    }

    public function addApplicatoinNotFound( $applications ) 
    {
        $userId = auth()->id();
        foreach ( $applications as $application ) {
            FeeVerificationFailed::create([
                'user_id' => $userId,
                'form_number' => $application
            ]);
        } 
    }
    public function failed()
    {
        if ( request()->method() == 'POST' ) {
            $records = FeeVerificationFailed::where('user_id', auth()->id())->get();
            $this->exportFailedRecords( $records );
        }
        $records = FeeVerificationFailed::where('user_id', auth()->id())->paginate(100);
        return view ('fee.failed', compact('records'));
    }

    public function exportFailedRecords( $records )
    {
        $csv = Writer::createFromFileObject(new SplTempFileObject());
        $csv->insertOne(['Application_Number', 'Created At']);
        foreach ( $records as $record ) {
            $csv->insertOne(
                [
                    $record->form_number ?? '',
                    $record->created_at->format('d-M-Y H:i:s') ?? ''
                ]
            );
        }
        $currentUser = User::find(auth()->id());
        $currentDate = \Carbon\Carbon::now()->format('d-M-Y H:i:s');
        $message = "Report generated by $currentUser->first_name $currentUser->last_name on $currentDate via Online Admission System";
        $csv->insertOne([$message]);
        $fileName = 'failed_records '.$currentDate.'.csv';
        $csv->output($fileName);
        die();
    }

    

    

    

    public function verifyAll() {
        $application_ids = request('application_ids');
        $application_ids_array = explode(',', $application_ids);
        
        $allApplicationsToSendEmail = Application::with('applicant')->whereIn('id', $application_ids_array)->get();
        
        foreach ( $allApplicationsToSendEmail as $app ) {

            $programId = $app->fkProgramId;
            if ( $programId != '' ) {
                $programme = Programme::where('pkProgId', $programId)->first();
                
                $abbrev = $programme->faculty->abbrev;
                $name = $app->applicant->name;
                $programTitle = $programme->title;
                $data = array('name' => $name, 'program' => $programTitle);
                $to = $app->applicant->email;
                $subject = 'Fee Verified';
                $view = 'mail.feeverified';
                SendEmail::dispatch(
                    $abbrev, 
                    $data, 
                    $to, 
                    'Fee Verified', 
                    'mail.feeverified'
                );
            }
        }

        $verifiedApplications = Application::whereIn('id', $application_ids_array)->where('fkCurrentStatus', '>=', '2')->whereNull('feeVerified')->get();
        $applications = '';
        DB::transaction(function () use ( $application_ids_array) {
            $applications = Application::whereIn('id', $application_ids_array)
                ->where('fkCurrentStatus', '>=', '2')
                ->whereNull('feeVerified')
                ->update(
                    [
                        'fkFeeVerifiedBy' => auth()->id(),
                        'feeVerified' => date('Y-m-d H-i-s'),
                        'fkCurrentStatus' => 4
                    ]
                );
        },3);
        
        if ( $applications  == 0 )
            return redirect()->back()->with('status', $applications .' record(s) verified');

        return redirect()->back()->with('verifiedApplications', $verifiedApplications)->with('status', 'Record(s) verified');
    }

    public function exportToCsv() {
        $applications_with_free_verification_failed = FeeVerificationFailed::all();

        $application_numbers = [];
        foreach ( $applications_with_free_verification_failed as $app) {
            $application_numbers[] = $app->form_number;
        }
        $cast_application_numbers_to_array = array_map(function($num){
            return (array)$num;
        }, $application_numbers);

        $csv = Writer::createFromFileObject(new SplTempFileObject());
        $csv->insertOne(['Application_Number', 'Attempt By']);
        foreach ( $applications_with_free_verification_failed as $application ) {
            $firstName = $application->user->first_name ?? '';
            $lastName = $application->user->last_name ?? '';
            $fullName = $firstName.' '.$lastName;
            $csv->insertOne(
                [
                    $application->form_number ?? '', 
                    $fullName
                ]
            );
        }
        $currentUser = User::find(auth()->id());
        $currentDate = date('d-m-Y h:i:s A');
        $message = "Report generated by $currentUser->first_name $currentUser->last_name on $currentDate via Online Admission System";
        $csv->insertOne([$message]);
        $currentDate = date ('d-m-Y h-i-s A');
        $fileName = 'users_with_records_not_found '.$currentDate.'.csv';
        $csv->output($fileName);
    }

    public function add_not_found_application_ids_to_database($applications_not_found)
    {
        foreach ( $applications_not_found as $app ) {
            FeeVerificationFailed::firstOrCreate([
                'user_id' => auth()->id(),
                'form_number' => $app
            ]);
        }
    }

    public function getUserPermissions()
    {
        $authId = auth()->id();
        return Permission::where('fkUserId', $authId)->first()->fkGenderId;
    }

    public function export_applications_with_fee_verified( $applications, $unverified = '' )
    {
        $csv = Writer::createFromFileObject(new SplTempFileObject());

        if ( $unverified == 'unverified' ) {
            $csv->insertOne(['Application_Number', 'Name', 'Status', 'CNIC', 'Program Title', 'Bank Details', 'Branch Code', 'Date']);
            foreach ( $applications as $application ) {
                $date = date_create ( $application->feeSubmitted );
                $formattedDate = date_format ( $date, 'd-m-Y');
                $csv->insertOne(
                    [
                        $application->id ?? '', 
                        $application->applicant->name ?? '',
                        $application->status->status ?? '',
                        $application->applicant->cnic ?? '',
                        $application->program->title ?? '',
                        $application->bankDetails ?? '',
                        $application->branchCode ?? '',
                        $formattedDate ?? ''
                    ]
                );
            }
            $currentUser = User::find(auth()->id());
            $currentDate = date('d-m-Y h:i:s A');
            $message = "Report generated by $currentUser->first_name $currentUser->last_name on $currentDate via Online Admission System";
            $csv->insertOne([$message]);
            $currentDate = date ('d-m-Y h-i-s A');
            $fileName = 'applications_with_fee_unverified '.$currentDate.'.csv';
            $csv->output($fileName);
        } else {
            $csv->insertOne(['Application_Number', 'Name', 'CNIC', 'Program', 'Semester', 'Verified By', 'Date']);
            foreach ( $applications as $application ) {
                $csv->insertOne
                (
                    [
                        $application->id,
                        $application->applicant->name, 
                        $application->applicant->cnic ?? '',
                        $application->program->title ?? '',
                        $application->semester->title ?? '',
                        $application->user->first_name. ' '. $application->user->last_name,
                        $application->feeVerified
                    ]
                );
            }
            $currentUser = User::find(auth()->id());
            $currentDate = date('d-m-Y h:i:s A');
            $message = "Report generated by $currentUser->first_name $currentUser->last_name on $currentDate via Online Admission System";
            $csv->insertOne([$message]);
            $currentDate = date ('d-m-Y h-i-s A');
            $fileName = 'applications_with_fee_verified '.$currentDate.'.csv';
            $csv->output($fileName);
        }
        die;
    }
}
