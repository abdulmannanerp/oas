<?php

namespace App\Http\Controllers;

use Validator;
use Carbon\Carbon;
use App\Traits\PQM;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Model\{Logger, Applicant, Permission, UserLastLogin, ScholarshipDetail, Listscholarship, ScholarshipInformation, ScholarshipFamilyDetail, ScholarshipFamilyAssets};

class SchFeeVerification extends Controller
{
    protected $currentUserPermissions;
    protected $facultyPermissions;
    protected $departmentPermissions;
    protected $genderPermissions;
    protected $nationalityPermissions;
    protected $selectedSemester;

    public function __construct()
    {
        // $this->selectedSemester = $this->getCurrentSemester()->pkSemesterId;
    }

    public function index()
    {

        $this->assignPermissions();
        $SchView = ScholarshipDetail::get();


        $scholarshipname = Listscholarship::find($SchView[0]->fkSchlrId);
        $ScholarshipInformation = ScholarshipInformation::where('fkApplicantId', $SchView[0]->fkApplicantId)->get(); 
        $Applicant = Applicant::find($SchView[0]->fkApplicantId);
       
        // dd($ScholarshipInformation);


        return view('scholarship.feeverification', compact('SchView', 'Applicant','scholarshipname', 'ScholarshipInformation'));
    }

    public function delete($id){

        ScholarshipDetail::where('fkApplicantId',$id)->delete();
        ScholarshipInformation::where('fkApplicantId',$id)->delete();
        ScholarshipFamilyDetail::where('fkApplicantId',$id)->delete();
        ScholarshipFamilyAssets::where('fkApplicantId',$id)->delete();
        return redirect('feeverification');
    }
    
        public function edit($id)
    {
        $SchView = ScholarshipDetail::find($id);
        $scholarshipname=Listscholarship::find($SchView['fkSchlrId']);
        
       // dd($scholarshipname);


        $Applicant= Applicant::where('userId',$SchView['fkApplicantId'])->first();
        return view('scholarship.editfeeverification', compact('scholarshipname','Applicant','SchView'));
    }

    public function update($id)
    {
        $SchView=ScholarshipDetail::find($id);
        $SchView->feeverification=request('fee_ver_status');
        $SchView->feeremarks=request('fee_remarks');
        $SchView->update();
        return redirect('/admin/feeverification/');
    }



       public function printScholarship($applicantID,  $scholarshipId )
    {
        
       $current_uri = request()->segments();

        if ( isset($applicantID) && isset($scholarshipId)) {
            $authenticateUser = auth()->id();
            $applicant = Applicant::where('userId',$applicantID)->first();

         $nationalilty = Nationality::where('nationalitystatus', 1)->get();
         $listscholarship = Listscholarship::get();
         $status = request()->session()->get('status');
         $city = City::all();
         $Scholarshipdetail = ScholarshipDetail::where('fkApplicantId', $applicantID)->first();
         $scholarshipname = Listscholarship::find($scholarshipId);
         $scholarshipinformation = ScholarshipInformation::where('fkApplicantId', $applicantID)->first();
         
         $scholarshipFamilyDetail = ScholarshipFamilyDetail::where('fkApplicantId', $applicantID)->first();
         
         $scholarshipFamilyAssets = ScholarshipFamilyAssets::where('fkApplicantId', $applicantID)->first();
         $applicantAddress = ApplicantAddress::where('fkApplicantId', $applicantID)->first();
         $applicantDetail = ApplicantDetail::where('fkApplicantId', $applicantID)->first();



      return view('frontend.printScholarship', compact('nationalilty', 'applicant', 'applicantDetail', 'applicantAddress', 'scholarshipFamilyAssets', 'scholarshipFamilyDetail', 'scholarshipinformation', 'Scholarshipdetail', 'city', 'listscholarship','scholarshipname' ));


          


            if ( !in_array($request->oas_app_id, $authentcatedUserApplications ) ) 
                return '401 Unauthorized Access';
        }
       
        $oas_applications = Application::where('id', $request->oas_app_id)->first();
        $oas_applicatdetail = ApplicantDetail::where('fkApplicantId',$oas_applications->fkApplicantId)->first();
        // $this->thumbPrint('http://admission.iiu.edu.pk/'.$oas_applicatdetail->pic); 
        return view('frontend.printScholarship', compact('oas_applications'));
    }



    


    public function assignPermissions()
    {
        $this->currentUserPermissions = $this->getCurrentUserPermissions();
        $this->facultyPermissions = explode(',', $this->currentUserPermissions->fkFacultyId);
        $this->departmentPermissions = explode(',',$this->currentUserPermissions->fkDepartmentId);
        $this->genderPermissions = $this->currentUserPermissions->fkGenderId;
        $this->nationalityPermissions = explode(',',$this->currentUserPermissions->fkNationality);
    }

    public function checkIfHasFullAccess() {
        if 
        (  count ( $this->facultyPermissions ) == 9 && 
            $this->departmentPermissions[0] == '' && 
            $this->genderPermissions == 1 && 
            (count( $this->nationalityPermissions) == 3 || 
            $this->nationalityPermissions[0] == '' ||  $this->nationalityPermissions[0] == 'NULL') 
        )
            return true;
        return false;
    }

    public function checkIfHasLimitedAccess( $applications )
    {
        $filtered = $applications->filter(function ( $value, $key ) {
            return $this->filterApplication ( $value );
        });
        return $filtered;
    }


    public function checkAccessLevelAndGetApplication( $applications ) 
    {
        return ( $this->checkIfHasFullAccess() ) ? $applications : $this->checkIfHasLimitedAccess($applications);   
    }

    public function paginateResults($applications, $paginate)
    {
        $currentPage = LengthAwarePaginator::resolveCurrentPage();
        $currentPageResults = $applications->slice(($currentPage-1) * $paginate, $paginate)->all();
        $result = new LengthAwarePaginator($currentPageResults, count($applications), $paginate);
        $result->setPath(request()->url());
        return $result;
    }

    public function getSearchByName ( $name, $paginate )
    {
        $applications = Application::with(['applicant', 'program', 'status'])->where('fkSemesterId', $this->selectedSemester)->where(function ( $query ) {
                $query->where('a_status', '>=', 1)->orWhereNull('a_status');
            })->whereHas('applicant', function( $query) use ($name) {
            $query->where('name', 'like', '%'.$name.'%');
        })->get();
        if ( $applications->count() ) {
            $applications = $this->checkAccessLevelAndGetApplication ( $applications );
            return $this->paginateResults( $applications, $paginate );
        }
        return '';
    }

    public function getSearchByCnic ( $cnic,$paginate ) {
        $applications = Application::with(['applicant', 'program', 'status'])->where('fkSemesterId', $this->selectedSemester)->where(function ( $query ) {
                $query->where('a_status', '>=', 1)->orWhereNull('a_status');
            })->whereHas('applicant', function( $query) use ($cnic) {
            $query->where('cnic', $cnic );
        })->get();
        if ( $applications->count() ) {
            $applications = $this->checkAccessLevelAndGetApplication ( $applications );
            return $this->paginateResults( $applications, $paginate );
        }
        return '';
    }

    public function returnApplicationsView($filter, $searchable, $paginate, $applications, $selectedSemester)
    {
        $semesters = Semester::where('pkSemesterId', '>', '10')->get();
        $curr_sem = Semester::where('status', '1')->first();
        return view('scholarship.addscholarship', compact('filter', 'searchable', 'paginate', 'applications', 'semesters', 'selectedSemester', 'curr_sem'));
    }
    public static function returnListNumber($programId, $rollNumber){
        // Result::with('list')->where('fkProgrammeId', $programId)
        $sql = "SELECT pkResultsId from tbl_oas_results rs inner join tbl_oas_result_list rl on rs.pkResultsId=rl.fkResultId  where rs.fkProgrammeId=".$programId." and rs.fkProgrammeId in(131, 132) and rl.rollno=".$rollNumber;
        $id = \DB::select($sql);
        if($id){
            return $id[0]->pkResultsId;
        }            
    }

    public function getSpecificSemesterApplications()
    {
        return redirect()->route('scholarships', ['semester' => request('semesterSelector')]);
    }

    public function viewLog($id ='', $fkApplicantId = ''){
        $current_semes = Semester::where('status', '1')->first()->pkSemesterId;
        $previousQualificationLog = UserLogQualification::where('applicant_id', request('fkApplicantId'))->where('fkSemesterId', $current_semes)->where('description', 'Previous Qualification')->get();
        $documentsLog = UserLogQualification::where('application_id', request('id'))->where('fkSemesterId', $current_semes)->where('description', 'Document Verify')->get();
        // $previousQualificationLog = unserialize($previousQualificationLog->existing_value);
        // dd($previousQualificationLog, $documentsLog);
        return view ('applications.viewLog', compact('previousQualificationLog', 'documentsLog'));    
    }

    public function editApplication($id ='')
    {
        $status = request()->session()->get('status');
        if ( request()->method() == 'POST' ) {
            $this->validate(request(), [
                'email' => 'required|unique:tbl_users,email,'.request('applicant').',userId',
                'cnic' => 'required|unique:tbl_users,cnic,'.request('applicant').',userId'
            ]);

            if (  request('id') ) {
                /** update users program */
                $application = Application::where('id', request('id'))->first();
                $logger = Logger::where('user_id', request('applicant'))->where('description', 'Program Updated')->get();
                if ( $application->fkProgramId != request('program') && request('program') ) {
                    Logger::create(
                        [
                            'user_id' => request('applicant'),
                            'application_id' => request('id'),
                            'modified_by' => auth()->id(),
                            'existing_value' => $application->fkProgramId,
                            'updated_value' => request('program'),
                            'description' => 'Program Updated'
                        ]
                    );
                $newProgram = Programme::where('pkProgId', request('program'))->first();
                // Dev Mannan: Code for application program, department and faculty update
                Application::where('id', request('id'))->update(
                    [
                        'fkProgramId' => request('program'),
                        'fkDepId' => $newProgram->fkDepId,
                        'fkFacId' => $newProgram->fkFacId
                    ]
                    );
                    $this->updateFeeChallan(request('id'), request('program'), $application->fkSemesterId);
                // Dev Mannan: Code ends
                }

            }
            /** Update only user info */
            $applicant = Applicant::where('userId', request('applicant'))->first();
            Applicant::where('userId', request('applicant'))->update(
                [
                    'name' => request('name'),
                    'cnic' => request('cnic'),
                    'email' => request('email'),
                    'status' => request('status'),
                    'password' => (request('password')) ?  bcrypt(request('password')) : $applicant->password
                ]
            );
            return back()->with('status', 'Information updated successfully!');
        }

        if ( $id ) {
            $application = Application::with(['applicant'])->find($id);
            $programs = Programme::select('pkProgId', 'title')->get();
            $current_semes = Semester::where('status', '1')->first()->pkSemesterId;
            $logs = Logger::with(['user', 'existingProgram', 'updatedProgram'])->where('application_id', $id)->latest()->get();
            return view ('applications.edit', compact('application', 'programs', 'status', 'logs', 'current_semes'));    
        }
        return redirect()->route('scholarships');
    }


    public function superadmin(Request $request, Application $application)
    {
        $application = Application::where('id', $request->id)->first();
        $logs = UserLastLogin::where('userId', $application->fkApplicantId)->get();
        // dump($logs);
        $fee_verify = 'NULL';
        $docverify = 'NULL';
        if ($application->docsVerifyAdmin) {
            $docverify = $application->docsVerifyAdmin->username;
        }
        if ($application->user) {
            $fee_verify = $application->user->username;
        }
        return response()->json(['application' => $application, 'fee_verified_by' => $fee_verify, 'documents_verified_by' => $docverify, 'logs'=>$logs]);
    }

    public function superadminid(Request $request){
        if($request->id){
            $users = \DB::table('tb_users')->where('id','=',$request->id)->get()->first();
            return response()->json(['username'=>$users->username]);
        }
    }
}