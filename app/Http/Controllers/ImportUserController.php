<?php 

namespace App\Http\Controllers;

use App\Jobs\SendEmail;
use App\Model\oasEmail;
use App\Model\Permission;
use App\Models\Core\Users;
use Illuminate\Http\Request;
use Validator, Input, Redirect ; 
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use League\Csv\{Reader, Writer, Statement};

class ImportUserController extends Controller {

	
	public function index()
	{
		$alreadyInDb = '';
		if (request()->hasFile('fileToUplaod')) {
			request('fileToUplaod')->storeAs('public', 'user-import.csv');
			$csv = Reader::createFromPath(storage_path('app/public/') . 'user-import.csv', 'r');
			$records = $csv->setOffset(1)->fetchAll();
			if ( $records ) {
				foreach ( $records as $record ) {
					$email = trim($record[0]);
					$first_name = trim($record[1]);
					$last_name = trim($record[2]);
					$faculty = trim($record[3]);
					$department = trim($record[4]);
					$gender = trim($record[5]);

					$already_exists = Users::where('email', $email)->first();
					if($already_exists == null){
						// echo 'record do not exists';
						$rand_pwd = rand(1000000000, 9999999999);
						// $user_name = $first_name.$last_name;
						$id = \DB::table('tb_users')->insertGetId(
							[
								'email' => $email, 
								'password' => Hash::make($rand_pwd),
								'username' => $this->generateRandomString(),
								'group_id' => request('group'),
								'first_name' => $first_name,
								'last_name' => $last_name,
								'pwd' => $rand_pwd,
								'active' => 1
								]
						);
						\DB::table('tb_permissions')->insert(
							[
								'fkUserId' => $id, 
								'fkFacultyId' => $faculty,
								'fkDepartmentId' => $department,
								'fkGenderId' => $gender,
								'fkNationality' => '1,2,3'
							]
						);

					// dd($id);
					$data = [
						'first_name' => $first_name,
						'last_name' => $last_name,
						'email' => $email,
						'password' => $rand_pwd
					];
					$abbr = 'AdmissionMale';
					SendEmail::dispatch(
						$abbr,
						$data,
						$email,
						'Online Admission Credentials',
						'importUsers.email'
					);
					// oasEmail::email($abbr, $data, $email, 'Online Admission Credentials', 'importUsers.email');

					}else{
						// echo 'record exist';
						$alreadyInDb = $alreadyInDb. ' , '.$email;
                		continue;
						
					}
				}
			}
		// dd($applicationIds);
		}
		$groups = \DB::table('tb_groups')->get();
		return view('importUsers.index', compact('groups', 'alreadyInDb'));

	}

	public function generateRandomString($length = 12) {
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$charactersLength = strlen($characters);
		$randomString = '';
		for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[rand(0, $charactersLength - 1)];
		}
		return $randomString;
	}
	
	
	
}