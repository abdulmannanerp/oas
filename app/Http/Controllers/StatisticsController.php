<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Application;
use App\Model\Applicant;
use App\Model\ApplicantDetail;
use App\Model\Department;


class StatisticsController extends Controller
{


  protected $currentSemester;

  public function __construct()
  {
    $this->currentSemester = $this->getCurrentSemester()->pkSemesterId;
  }

  public function index(Request $request)
  { 
    $applications_male = Application::with(['applicant'])
      ->select(\DB::raw('COUNT(fkProgramId) AS total, fkProgramId, fkDepId, fkFacId'))
      ->where('fkCurrentStatus', '>=', 4)
      ->where('fkSemesterId', $this->currentSemester)
      ->whereHas('applicant', function( $query ){
        $query->where('fkGenderId', 3);
    })->groupBy('fkProgramId')
    ->orderBy('fkFacId', 'ASC')
    ->orderBy('fkDepId', 'ASC')
    ->orderBy('fkProgramId', 'DESC')
    ->where('new_application_status', 'new') // Special clause for new applied candidates
    ->get();
    $applications_female = Application::with(['applicant'])
      ->select(\DB::raw('COUNT(fkProgramId) AS total, fkProgramId, fkDepId, fkFacId'))
      ->where('fkCurrentStatus', '>=', 4)
      ->where('fkSemesterId', $this->currentSemester)
      ->whereHas('applicant', function( $query ){
        $query->where('fkGenderId', 2);
    })->groupBy('fkProgramId')
    ->orderBy('fkFacId', 'ASC')
    ->orderBy('fkDepId', 'ASC')
    ->orderBy('fkProgramId', 'DESC')
    ->where('new_application_status', 'new') // Special clause for new applied candidates
    ->get();
    $total_applications = Application::with(['applicant'])
      ->select(\DB::raw('COUNT(fkProgramId) AS total, fkProgramId, fkDepId, fkFacId'))
      ->where('fkCurrentStatus', '>=', 4)
      ->where('fkSemesterId', $this->currentSemester)
      ->whereHas('applicant', function( $query ){
        $query->whereIn('fkGenderId', array( 2, 3));
    })->groupBy('fkProgramId')
    ->orderBy('fkFacId', 'ASC')
    ->orderBy('fkDepId', 'ASC')
    ->orderBy('fkProgramId', 'DESC')
    ->where('new_application_status', 'new') // Special clause for new applied candidates
    ->get();

    
    // How you find us stats query
    $find_us = "SELECT COUNT(tp.id) AS total, find_us  
    FROM 
    tbl_oas_applicant_detail as tp
    inner join 
    tbl_users as tu
    on 
    tp.fkApplicantId=tu.userId
    inner Join
    tbl_oas_applications as tap
    on 
    tu.userId=tap.fkApplicantId
    where 
    tap.fkSemesterId= $this->currentSemester
    and 
    tp.find_us is not NULL
    group by 
    tp.find_us";
    $find_us = \DB::select($find_us);

    $distinctBs = "SELECT COUNT(ta.fkApplicantId) as total
    FROM 
    tbl_oas_applications as ta 
    INNER JOIN 
    tbl_users as tu
    on 
    ta.fkApplicantId=tu.userId
    where 
    ta.fkSemesterId=$this->currentSemester AND
    tu.fkLevelId=1 AND
    ta.fkFeeVerifiedBy is not null
    GROUP BY ta.fkApplicantId";
    $distinctBs = \DB::select($distinctBs);

    $distinctBba = "SELECT COUNT(ta.fkApplicantId) as total
    FROM 
    tbl_oas_applications as ta 
    INNER JOIN 
    tbl_users as tu
    on 
    ta.fkApplicantId=tu.userId
    where 
    ta.fkSemesterId=$this->currentSemester AND
    tu.fkLevelId=3 AND
    ta.fkFeeVerifiedBy is not null
    GROUP BY ta.fkApplicantId";
    $distinctBba = \DB::select($distinctBba);

    $distinctMs = "SELECT COUNT(ta.fkApplicantId) as total
    FROM 
    tbl_oas_applications as ta 
    INNER JOIN 
    tbl_users as tu
    on 
    ta.fkApplicantId=tu.userId
    where 
    ta.fkSemesterId=$this->currentSemester AND
    tu.fkLevelId=4 AND
    ta.fkFeeVerifiedBy is not null
    GROUP BY ta.fkApplicantId";
    $distinctMs = \DB::select($distinctMs);

    $distinctPhd = "SELECT COUNT(ta.fkApplicantId) as total
    FROM 
    tbl_oas_applications as ta 
    INNER JOIN 
    tbl_users as tu
    on 
    ta.fkApplicantId=tu.userId
    where 
    ta.fkSemesterId=$this->currentSemester AND
    tu.fkLevelId=5 AND
    ta.fkFeeVerifiedBy is not null
    GROUP BY ta.fkApplicantId";
    $distinctPhd = \DB::select($distinctPhd);
    // foreach($find_us as $fs){
    //   echo $fs->total. ' = '. $fs->find_us. '<br />'; 
    // }
    // exit;
    return view('btreports.btreports', compact('total_applications','applications_male', 'applications_female',  'request', 'find_us', 'distinctBs', 'distinctBba', 'distinctMs', 'distinctPhd'));
  }

  public function topBottomStats(Request $request){
    if($request->number){
      $limit = ' '.$request->number;
    }else{
      $limit = ' 5';  
    }
    if($request->gender && $request->gender != 1){
      $gender = ' and u.fkGenderId = '.$request->gender.' ';
    }else{
      $gender=' ';
    }
    if($request->stats){
      $order = $request->stats;
    }else{
      $order = ' DESC';
    }
    if ($request->gender && $request->stats && $request->number) {

      if ($request->gender == 1) {
        $total="SELECT p.title as program, d.title as department, f.title as faculty, count(a.id) as totalapplicants, d.academia_male as academia_male, d.academia_female as academia_female 
        FROM 
        tbl_users as u
        inner join 
        tbl_oas_applications as a
        on u.userId=a.fkApplicantId
        inner join 
        tbl_oas_programme as p
        on p.pkProgId=a.fkProgramId
        inner join
        tbl_oas_department as d
        on d.pkDeptId=p.fkDepId
        inner join
        tbl_oas_faculty as f
        on f.pkFacId=d.fkFacId
        where a.fkCurrentStatus >= 4
        and a.fkSemesterId = $this->currentSemester
        group by  program,department,faculty
        order by totalapplicants $order
        limit $limit";
        
        $male_sql="SELECT p.title as program, d.title as department, f.title as faculty,  count(a.id) as totalapplicants, d.academia_male as academia_male, d.academia_female as academia_female  
        FROM 
        tbl_users as u
        inner join 
        tbl_oas_applications as a
        on u.userId=a.fkApplicantId
        inner join 
        tbl_oas_programme as p
        on p.pkProgId=a.fkProgramId
        inner join
        tbl_oas_department as d
        on d.pkDeptId=p.fkDepId
        inner join
        tbl_oas_faculty as f
        on f.pkFacId=d.fkFacId
        where a.fkCurrentStatus >= 4
        and a.fkSemesterId = $this->currentSemester
        and u.fkGenderId = 3
        group by program,department,faculty
        order by totalapplicants $order";
        
        $female_sql="SELECT p.title as program, d.title as department, f.title as faculty, count(a.id) as totalapplicants, d.academia_male as academia_male, d.academia_female as academia_female  
        FROM 
        tbl_users as u
        inner join 
        tbl_oas_applications as a
        on u.userId=a.fkApplicantId
        inner join 
        tbl_oas_programme as p
        on p.pkProgId=a.fkProgramId
        inner join
        tbl_oas_department as d
        on d.pkDeptId=p.fkDepId
        inner join
        tbl_oas_faculty as f
        on f.pkFacId=d.fkFacId
        where a.fkCurrentStatus >= 4
        and a.fkSemesterId = $this->currentSemester
        and u.fkGenderId = 2
        group by program,department,faculty
        order by totalapplicants $order";
        $applications = \DB::select($total);
        $maleapplications = \DB::select($male_sql);
        $femaleapplications = \DB::select($female_sql);
        } else {
        $total="SELECT p.title as program, d.title as department, f.title as faculty, count(a.id) as totalapplicants
        FROM 
        tbl_users as u
        inner join 
        tbl_oas_applications as a
        on u.userId=a.fkApplicantId
        inner join 
        tbl_oas_programme as p
        on p.pkProgId=a.fkProgramId
        inner join
        tbl_oas_department as d
        on d.pkDeptId=p.fkDepId
        inner join
        tbl_oas_faculty as f
        on f.pkFacId=d.fkFacId
        where a.fkCurrentStatus >= 4
        and a.fkSemesterId = $this->currentSemester
        $gender
        group by program,department,faculty
        order by totalapplicants $order
        limit $limit";
        $applications = \DB::select($total);
        $maleapplications = '';
        $femaleapplications = '';
      }
    } else {
      $total="SELECT p.title as program, d.title as department, f.title as faculty, count(a.id) as totalapplicants, d.academia_male as academia_male, d.academia_female as academia_female 
      FROM 
      tbl_users as u
      inner join 
      tbl_oas_applications as a
      on u.userId=a.fkApplicantId
      inner join 
      tbl_oas_programme as p
      on p.pkProgId=a.fkProgramId
      inner join
        tbl_oas_department as d
        on d.pkDeptId=p.fkDepId
        inner join
        tbl_oas_faculty as f
        on f.pkFacId=d.fkFacId
      where a.fkCurrentStatus >= 4
      and a.fkSemesterId = $this->currentSemester
      group by program,department,faculty
      order by totalapplicants $order
      limit $limit";
      
      $male_sql="SELECT p.title as program, d.title as department, f.title as faculty, count(a.id) as totalapplicants, d.academia_male as academia_male, d.academia_female as academia_female
      FROM 
      tbl_users as u
      inner join 
      tbl_oas_applications as a
      on u.userId=a.fkApplicantId
      inner join 
      tbl_oas_programme as p
      on p.pkProgId=a.fkProgramId
      inner join
        tbl_oas_department as d
        on d.pkDeptId=p.fkDepId
        inner join
        tbl_oas_faculty as f
        on f.pkFacId=d.fkFacId
      where a.fkCurrentStatus >= 4
      and a.fkSemesterId = $this->currentSemester
      and u.fkGenderId = 3
      group by program,department,faculty
      order by totalapplicants $order";
      
      $female_sql="SELECT p.title as program, d.title as department, f.title as faculty, count(a.id) as totalapplicants, d.academia_male as academia_male, d.academia_female as academia_female 
      FROM 
      tbl_users as u
      inner join 
      tbl_oas_applications as a
      on u.userId=a.fkApplicantId
      inner join 
      tbl_oas_programme as p
      on p.pkProgId=a.fkProgramId
      inner join
        tbl_oas_department as d
        on d.pkDeptId=p.fkDepId
        inner join
        tbl_oas_faculty as f
        on f.pkFacId=d.fkFacId
      where a.fkCurrentStatus >= 4
      and a.fkSemesterId = $this->currentSemester
      and u.fkGenderId = 2
      group by program,department,faculty
      order by totalapplicants $order";
      $applications = \DB::select($total);
      $maleapplications = \DB::select($male_sql);
      $femaleapplications = \DB::select($female_sql);
    }
    $rec1 = "SELECT sum(academia_male) as total_male_academia FROM tbl_oas_department";
    $rec2 = "SELECT sum(academia_female) as total_female_academia FROM tbl_oas_department";
    $male_acaademics_sum = \DB::select($rec1);
    $female_acaademics_sum = \DB::select($rec2);
    return view('btreports.top-bottom', compact('applications', 'request', 'maleapplications', 'femaleapplications', 'male_acaademics_sum', 'female_acaademics_sum'));


  }

  public function department(Request $request)
  {
    $maleDepartmentApplicants = \DB::select( \DB::raw("SELECT dep.title, fac.title as facti, count(app.fkProgramId) as 'Male_Applicants', dep.academia_male, dep.academia_female 
    FROM 
    tbl_oas_faculty as fac
    INNER JOIN
    tbl_oas_department dep 
    on fac.pkFacId=dep.fkFacId
    INNER JOIN 
    tbl_oas_programme prog 
    on dep.pkDeptId=prog.fkDepId 
    INNER JOIN 
    tbl_oas_applications app 
    on app.fkProgramId=prog.pkProgId 
    INNER JOIN 
    tbl_users usr
    on app.fkApplicantId=usr.userId
    WHERE fkCurrentStatus >=4
    and app.fkSemesterId = $this->currentSemester
    AND usr.fkGenderId=3
    GROUP BY dep.pkDeptId
    ORDER BY Male_Applicants  DESC") );

    $femaleDepartmentApplicants = \DB::select( \DB::raw("SELECT dep.title, fac.title as facti, count(app.fkProgramId) as 'Female_Applicants', dep.academia_male, dep.academia_female  
    FROM 
    tbl_oas_faculty as fac
    INNER JOIN
    tbl_oas_department dep 
    on fac.pkFacId=dep.fkFacId
    INNER JOIN 
    tbl_oas_programme prog 
    on dep.pkDeptId=prog.fkDepId 
    INNER JOIN 
    tbl_oas_applications app 
    on app.fkProgramId=prog.pkProgId 
    INNER JOIN 
    tbl_users usr
    on app.fkApplicantId=usr.userId
    WHERE fkCurrentStatus >=4
    and app.fkSemesterId = $this->currentSemester
    AND usr.fkGenderId=2
    GROUP BY dep.pkDeptId
    ORDER BY Female_Applicants  DESC") );
       return view('btreports.dpstats', compact('maleDepartmentApplicants', 'femaleDepartmentApplicants'));
  }

}
