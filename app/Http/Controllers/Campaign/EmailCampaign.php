<?php

namespace App\Http\Controllers\Campaign;

use App\Http\Controllers\Controller;
use App\Jobs\SendEmail;
use App\Model\Application;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class EmailCampaign extends Controller
{
    public function getFacultyAbbrev()
    {
        return ['FA', 'FBAS', 'FET', 'FIS', 'FLL', 'FMS', 'FSS', 'IIIE', 'FSL'];
    }

    public function sendEmail() 
    {
    	$facultyToSendEmailFrom = 'FSS';
    	$applications = $this->sendEmailWhereCurrentStatusIsBelowThree();
    	foreach ( $applications as $application ) {
    		$data = ['name' => $application->name ];
    		$to = $application->email;
    		$subject = 'Fee Reminder';
    		$view = 'mail.feeCampaign';
			if (filter_var($application->email, FILTER_VALIDATE_EMAIL)) {
			 	// $job = (new SendEmail($facultyToSendEmailFrom, $data, $to, $subject, $view))
     //                ->delay(Carbon::now()->addSeconds(5));
     //            dispatch_now($job);
                SendEmail::dispatch(
                    $facultyToSendEmailFrom, 
                    $data, 
                    $to,
                    $subject,
                    $view
                );
			}
    	}
    	return 'Campaign Commpleted';
    }

    public function sendEmailReminderToProgram()
    {
        return Application::whereHas('applicant', function ($query) {
            $query->addSelect('email');
        })->where('fkCurrentStatus', '5')->where('fkProgramId', '196')->select('id')->get();
    }

    public function sendEmailWhereCurrentStatusIsBelowThree() {
    	return DB::select("SELECT distinct user.name,user.email FROM tbl_users user
			JOIN tbl_oas_applications app 
			ON user.userId = app.fkApplicantId
			WHERE app.fkCurrentStatus < 3" 
		);
    }
}
