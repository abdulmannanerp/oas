<?php

namespace App\Http\Controllers;

use App\Model\ProgramFee;
use App\Model\Programme;
use App\Model\ProgrammeScheduler;
use App\Model\Semester;


class FeeViewController extends Controller
{
    public function index()
    {
		$FeeView = ProgramFee::where('status',1)->orderBy('fkFacId', 'ASC')->orderBy('fkDepId','ASC')->get();
		$sql1 = "SELECT title from tbl_oas_semester where status=1";
		$currentSemester = \DB::select($sql1);
    	return view('results.feestructure', compact('FeeView', 'currentSemester'));
	}
	public function programes(){
		$semester = Semester::where('status', 1)->first();
		$programlisting = Programme::where('status', 1)->whereHas('department', function ($query) {
            $query->where('status', 1);
        })->orderBy('fkFacId', 'ASC')->orderBy('fkDepId', 'ASC')->orderBy('title', 'DESC')->get();
		return view('results.programs', compact('programlisting', 'semester'));
	}
		  
}
