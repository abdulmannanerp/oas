<?php

namespace App\Http\Controllers\Convocation;

use App\Http\Controllers\Controller;
use App\Mail\ConvocationChallanForm;
use App\Model\Convocation;
use App\Model\Programme;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Validator;

class ConvocationController extends Controller
{
    public function index()
    {
        $programs = Programme::all();
    	$status = request()->session()->get('status');
    	return view ( 'convocation.index', compact('status', 'programs'));
    }

    public function listInstructions()
    {
        return view ( 'convocation.instructions' );
    }

    public function store()
    {
        request()->validate([
            'academic_session'               => 'required',
            'name'                           => 'required',
            'fathers_name'                   => 'required',
            'image'                          => 'required|image',
            'cnic'                           => 'required|unique:convocations',
            'degree'                         => 'required',
            'registration_number'            => 'required',
            'residential_address'            => 'required',
            'mobile_phone'                   => 'required',
            'email'                          => 'required',
            'gender'                         => 'required',
            'is_transcript_recieved'         => 'required',
            'is_degree_recieved'             => 'required',
            'Conditions'                     => 'required'
        ]);

    	$directoryName = Convocation::count() + 1;
    	$imagePath = $matricCertificatePath = $transcriptPath = '';
    	if ( request()->hasFile('image') ) {
            $imagePath = request()->file('image')->store($directoryName);
        }
        if ( request()->hasFile('upload_transcript') ) {
            $transcriptPath = request()->file('upload_transcript')->store($directoryName);
        }
        if ( request()->hasFile('matric_certificate') ) {
			$matricCertificatePath = request()->file('matric_certificate')->store($directoryName);
    	}
        
    	$convocation = Convocation::create([
            'academic_session'               => request('academic_session'),
            'name'                           => request('name'),
            'fathers_name'                   => request('fathers_name'),
            'cnic'                           => request('cnic'),
            'degree'                         => request('degree'),
            'registration_number'            => request('registration_number'),
            'residential_address'            => request('residential_address'),
            'home_phone'                     => request('home_phone'),
            'office_phone'                   => request('office_phone'),
            'mobile_phone'                   => request('mobile_phone'),
            'email'                          => request('email'),
            'gender'                         => request('gender'),
            'is_degree_recieved'             => request('is_degree_recieved'),
            'degree_serial_number'           => request('degree_serial_number'),
            'is_transcript_recieved'         => request('is_transcript_recieved'),
            'transcript_path'                => $transcriptPath,
            'image_path'                     => $imagePath,
            'matric_certificate_path'        => $matricCertificatePath, 
            'bank_challan_1'                 => request('bank_challan_1'),
            'bank_challan_1_submission_date' => request('bank_challan_1_submission_date'),
            'bank_challan_2'                 => request('bank_challan_2'),
            'bank_challan_2_submission_date' => request('bank_challan_2_submission_date'),
            'bank_challan_3'                 => request('bank_challan_3'),
            'bank_challan_3_submission_date' => request('bank_challan_3_submission_date'),
            'guest_1_name'                   => request('guest_1_name'),
            'guest_1_cnic'                   => request('guest_1_cnic'),
            'guest_1_relationship'           => request('guest_1_relationship'),
            'guest_1_contact'                => request('guest_1_contact'),
            'guest_2_name'                   => request('guest_2_name'),
            'guest_2_cnic'                   => request('guest_2_cnic'),
            'guest_2_relationship'           => request('guest_2_relationship'),
            'guest_2_contact'                => request('guest_2_contact')
    	]);

        
        if ( $convocation ) {
            @Mail::to($convocation->email)->cc('admission.notifications@iiu.edu.pk')->send(new ConvocationChallanForm($convocation));
        }
        return redirect()->route('generateChallanForm', ['id' => $convocation->id]);
    }

    public function generateChallanForm( $convocation ) 
    {
        $convocation = Convocation::with(['program'])->find($convocation);

        $copies = ['Bank Copy', 'Accounts Copy', 'Academics Copy', 'Student Copy'];
        $degree_registration_fee = 1000;
        if ( trim($convocation->degree) == '153' ) {
            $degree_registration_fee = 2000;
        }
        $secruity_fee = 2000;
        $guests_fee = 0;
        if ( $convocation->guest_1_name ) {
            $guests_fee +=1000;
        }
        if ( $convocation->guest_2_name ) {
            $guests_fee +=1000;
        }
        if ( request('download') ) {
           $pdf = PDF::loadView('convocation.challan.download-fee-challan',
                compact('convocation', 'copies', 'degree', 'degree_registration_fee', 'secruity_fee', 'guests_fee')
            )->setPaper('a4', 'landscape');;
           return $pdf->download('Fee-Challan.pdf');
        }
        return view('convocation.challan.fee-challan', 
            compact('convocation', 'copies', 'degree', 'degree_registration_fee', 'secruity_fee', 'guests_fee')
        );
    }
}