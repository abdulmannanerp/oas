<?php

namespace App\Http\Controllers\Convocation;

use App\Model\Convocation;
use App\Http\Controllers\Controller;
use App\Jobs\SendEmail;
use App\Model\Application;
use App\Model\FeeVerificationFailed;
use App\Model\Permission;
use App\Model\Programme;
use App\Model\Status;
use App\Model\oasEmail;
use App\User;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use League\Csv\Reader;
use League\Csv\Statement;
use League\Csv\Writer;
use SplTempFileObject;


class ConvocationVerificationController extends Controller
{
    protected $verified = [];
    protected $unverified = [];
    protected $verified_applications = '';
    protected $unverified_applications = '';

    protected $export_applications_with_fee_verified;

    public function index()
    {
        $status = request()->session()->get('status');
        $applications = '';

        if (request()->method() == 'POST') {
            if (request('verify')) {
                $unSerialzedApplicationIds = unserialize(request('verify'));
                DB::transaction(function () use ($unSerialzedApplicationIds) {
                    $applications = Convocation::whereIn('id', $unSerialzedApplicationIds)->update(
                        [
                            'fkFeeVerifiedBy' => auth()->id(),
                            'feeVerified' => date('Y-m-d H-i-s'),
                            'fkCurrentStatus' => 4
                        ]
                    );
                // $this->sendFeeVerificationEmail($unSerialzedApplicationIds);
                });
                return back()->with('status', 'Records Verified Successfully');
            }
            if (request()->hasFile('csv')) {
                $applicationIds = $this->getRecoredsFromCsv();
                $applicationIds = $this->trimIdentifierFromApplicationIds($applicationIds);
                $applications = $this->checkPermissionsAndListRecords($applicationIds);
            } else {
                $applicationIds = request('application_ids');
                $applicationIds = $this->parseDataToArray($applicationIds);
                $applicationIds = $this->trimIdentifierFromApplicationIds($applicationIds);
                $applications = $this->checkPermissionsAndListRecords($applicationIds);
            }
        }
        return view('convocation.fee.verify', compact('status', 'applications'));
    }

    public function trimIdentifierFromApplicationIds($applications)
    {
        return array_map(function ($id) {
            $trimmedValue = substr($id, 2);
            return $trimmedValue = substr($trimmedValue, 0, strlen($trimmedValue) - 2);
        }, $applications);
    }

    public function checkPermissionsAndListRecords($applicationIds)
    {
        $permissionsArray = $this->convertPermissiontoArray();
        $applications = '';
        //Has all permisison
        if (count($permissionsArray) == 2 || $permissionsArray[0] == 1) {
            $applications = Convocation::with(['program'])->whereIn('id', $applicationIds)->get();
        } else if ($permissionsArray[0] == 2) {
            $applications = Convocation::with(['program'])->whereIn('id', $applicationIds)->where('gender', 'Female')->get();
        } else if ($permissionsArray[0] == 3) {
            $applications = Convocation::with(['program'])->whereIn('id', $applicationIds)->where('gender', 'Male')->get();
        }
        return $applications;
    }

    public function getRecoredsFromCsv()
    {
        $applicationIds = [];
        $file = request()->file('csv');
        request('csv')->storeAs('public', 'convocation-fee-verification-file.csv');
        $csv = Reader::createFromPath(storage_path('app/public/') . 'convocation-fee-verification-file.csv', 'r');
        $records = $csv->setOffset(1)->fetchAll();
        if ($records) {
            foreach ($records as $record) {
                $applicationIds[] = $record[0];
            }
        }
        return $applicationIds;
    }

    /**
     * Covert the string to array depending up what delimeter is used
     * @param  [string] $data
     * @return [array]
     */
    public function parseDataToArray($data)
    {
        $data = trim($data);
        $delimeter = ' ';
        if (strpos($data, ',') !== false) :
            $delimeter = ',';
        endif;
        if (strpos($data, '-') !== false) :
            $delimeter = '-';
        endif;
        if (strpos($data, PHP_EOL) !== false) :
            $delimeter = PHP_EOL;
        endif;
        if ($delimeter == PHP_EOL) {
            return preg_split("/\\r\\n|\\r|\\n/", $data);
        }
        return explode($delimeter, $data);
    }

    public function verified()
    {
        $startDate = request('start_date');
        $endDate = request('end_date');
        $export = request('export');
        $applications = $this->getApplicationsByStatus('feeVerified', $startDate, $endDate, '>=', '4', $export);
        if (request()->method() == 'POST') {
            $this->exportRecordsToCsv($applications, 'fee-verified-');
        }
        return view('convocation.fee.verified', compact('applications', 'startDate', 'endDate'));
    }

    public function unverified()
    {
        $startDate = request('start_date');
        $endDate = request('end_date');
        $export = request('export');
        $applications = $this->getApplicationsByStatus('uploaded_challan', $startDate, $endDate, '<', '4', $export);
        if (request()->method() == 'POST') {
            $this->exportRecordsToCsv($applications, 'fee-unverified-', $export);
        }
        return view('convocation.fee.unverified', compact('applications', 'startDate', 'endDate'));
    }

    public function getApplicationsByStatus($column, $startDate, $endDate, $operator, $status, $export)
    {
        $permissionsArray = $this->convertPermissiontoArray();
        if ($startDate && $endDate) {
            /* Avoid appending multiple time to date $startDate = $startDate.' 00:00:00'; */
            if (count(explode(' ', $startDate)) <= 1) {
                $startDate = $startDate . ' 00:00:00';
                $endDate = $endDate . ' 23:59:59';
            }
            if (count($permissionsArray) == 2 || $permissionsArray[0] == 1) {
                $applications = Convocation::with(['program', 'program.faculty', 'program.department', 'user'])->whereBetween($column, [$startDate, $endDate])->where('fkCurrentStatus', $operator, $status)->orderBy($column, 'DESC');
            } else if ($permissionsArray[0] == 2) {
                $applications = Convocation::with(['program', 'program.faculty', 'program.department', 'user'])->whereBetween($column, [$startDate, $endDate])->where('fkCurrentStatus', $operator, $status)->where('gender', 'Female')->orderBy($column, 'DESC');
            } else if ($permissionsArray[0] == 3) {
                $applications = Convocation::with(['program', 'program.faculty', 'program.department', 'user'])->whereBetween($column, [$startDate, $endDate])->where('fkCurrentStatus', $operator, $status)->where('gender', 'Male')->orderBy($column, 'DESC');
            }
            if ($export) {
                return $applications->get();
            }
            return $applications->paginate(100);
        } else {
            if (count($permissionsArray) == 2 || $permissionsArray[0] == 1) {
                $applications = Convocation::with(['program', 'user'])->where('fkCurrentStatus', $operator, $status)->orderBy($column, 'DESC');
            } else if ($permissionsArray[0] == 2) {
                $applications = Convocation::with(['program', 'user'])->where('fkCurrentStatus', $operator, $status)->where('gender', 'Female')->orderBy($column, 'DESC');
            } else if ($permissionsArray[0] == 3) {
                $applications = Convocation::with(['program', 'user'])->where('fkCurrentStatus', $operator, $status)->where('gender', 'Male')->orderBy($column, 'DESC');
            }
            if ($export) {
                return $applications->get();
            }
            return $applications->paginate(100);
        }
    }

    public function convertPermissiontoArray()
    {
        $permissions = $this->getCurrentUserPermissions()->fkGenderId;
        $permissionsArray = $this->parseDataToArray($permissions);
        if ($permissions == 1) {
            $permissionsArray = ['2', '3'];
        }
        return $permissionsArray;
    }

    public function exportRecordsToCsv($applications, $filename)
    {
        $csv = Writer::createFromFileObject(new SplTempFileObject());
        if ($filename == 'fee-verified-') {
            $csv->insertOne(['Challan#', 'Name', 'CNIC', 'Reg#', 'Faculty', 'Department', 'Program', 'Fee', 'Verification Date']);
            foreach ($applications as $application) {
                $csv->insertOne(
                    [
                        '19' . $application->id . '19' ?? '',
                        $application->name ?? '',
                        $application->cnic ?? '',
                        $application->registration_number ?? '',
                        $application->program->faculty->title ?? '',
                        $application->program->department->title ?? '',
                        $application->program->title ?? '',
                        $application->fee ?? '',
                        date('d-M-Y h:i A', strtotime($application->feeVerified)) ?? ''
                    ]
                );
            }
        } else {
            $csv->insertOne(['Challan#', 'Name', 'CNIC', 'Reg#', 'Faculty', 'Department', 'Program', 'Fee']);
            foreach ($applications as $application) {
                $csv->insertOne(
                    [
                        '19' . $application->id . '19' ?? '',
                        $application->name ?? '',
                        $application->cnic ?? '',
                        $application->registration_number ?? '',
                        $application->program->faculty->title ?? '',
                        $application->program->department->title ?? '',
                        $application->program->title ?? '',
                        $application->fee ?? ''
                    ]
                );
            }
        }

        $currentUser = User::find(auth()->id());
        $currentDate = date('d-m-Y h:i:s A');
        $message = "Report generated by $currentUser->first_name $currentUser->last_name on $currentDate via Online Admission System";
        $csv->insertOne([$message]);
        $fileName = $filename . $currentDate . '.csv';
        $csv->output($fileName);
        die();
    }

    public function addApplicatoinNotFound($applications)
    {
        $userId = auth()->id();
        foreach ($applications as $application) {
            FeeVerificationFailed::create([
                'user_id' => $userId,
                'form_number' => $application
            ]);
        }
    }
    public function failed()
    {
        if (request()->method() == 'POST') {
            $records = FeeVerificationFailed::where('user_id', auth()->id())->get();
            $this->exportFailedRecords($records);
        }
        $records = FeeVerificationFailed::where('user_id', auth()->id())->paginate(100);
        return view('fee.failed', compact('records'));
    }

    public function exportFailedRecords($records)
    {
        $csv = Writer::createFromFileObject(new SplTempFileObject());
        $csv->insertOne(['Application_Number', 'Created At']);
        foreach ($records as $record) {
            $csv->insertOne(
                [
                    $record->form_number ?? '',
                    $record->created_at->format('d-M-Y H:i:s') ?? ''
                ]
            );
        }
        $currentUser = User::find(auth()->id());
        $currentDate = \Carbon\Carbon::now()->format('d-M-Y H:i:s');
        $message = "Report generated by $currentUser->first_name $currentUser->last_name on $currentDate via Online Admission System";
        $csv->insertOne([$message]);
        $fileName = 'failed_records ' . $currentDate . '.csv';
        $csv->output($fileName);
        die();
    }

    public function export_applications_with_fee_verified($applications, $unverified = '')
    {
        $csv = Writer::createFromFileObject(new SplTempFileObject());

        if ($unverified == 'unverified') {
            $csv->insertOne(['Application_Number', 'Name', 'Status', 'CNIC', 'Program Title', 'Bank Details', 'Branch Code', 'Date']);
            foreach ($applications as $application) {
                $date = date_create($application->feeSubmitted);
                $formattedDate = date_format($date, 'd-m-Y');
                $csv->insertOne(
                    [
                        $application->id ?? '',
                        $application->applicant->name ?? '',
                        $application->status->status ?? '',
                        $application->applicant->cnic ?? '',
                        $application->program->title ?? '',
                        $application->bankDetails ?? '',
                        $application->branchCode ?? '',
                        $formattedDate ?? ''
                    ]
                );
            }
            $currentUser = User::find(auth()->id());
            $currentDate = date('d-m-Y h:i:s A');
            $message = "Report generated by $currentUser->first_name $currentUser->last_name on $currentDate via Online Admission System";
            $csv->insertOne([$message]);
            $currentDate = date('d-m-Y h-i-s A');
            $fileName = 'applications_with_fee_unverified ' . $currentDate . '.csv';
            $csv->output($fileName);
        } else {
            $csv->insertOne(['Application_Number', 'Name', 'CNIC', 'Program', 'Semester', 'Verified By', 'Date']);
            foreach ($applications as $application) {
                $csv->insertOne(
                    [
                        $application->id,
                        $application->applicant->name,
                        $application->applicant->cnic ?? '',
                        $application->program->title ?? '',
                        $application->semester->title ?? '',
                        $application->user->first_name . ' ' . $application->user->last_name,
                        $application->feeVerified
                    ]
                );
            }
            $currentUser = User::find(auth()->id());
            $currentDate = date('d-m-Y h:i:s A');
            $message = "Report generated by $currentUser->first_name $currentUser->last_name on $currentDate via Online Admission System";
            $csv->insertOne([$message]);
            $currentDate = date('d-m-Y h-i-s A');
            $fileName = 'applications_with_fee_verified ' . $currentDate . '.csv';
            $csv->output($fileName);
        }
        die;
    }

    /**
     * Generate challan number of existing records
     */
    public function generateChallanOfExistingRecords()
    {
        $records = Convocation::all();
        foreach ($records as $record) {
            $record->update([
                'challan_number' => '19' . $record->id . '19'
            ]);
        }
    }

    /** 
     * Generate fee of existing records
     */
    public function generateFeeOfExistingRecords()
    {
        $records = Convocation::all();
        foreach ($records as $record) {
            $fee = $this->calculateConvocationFee($record->degree, $record->guest_1_name, $record->guest_2_name);
            $record->update([
                'fee' => $fee
            ]);
        }
    }

    public function calculateConvocationFee($degree, $guest1, $guest2)
    {
        $degree_registration_fee = 1000;
        $secruity_fee = 2000;
        $guests_fee = 0;
        if ($degree == '153') {
            $degree_registration_fee = 2000;
        }
        if ($guest1) {
            $guests_fee += 1000;
        }
        if ($guest2) {
            $guests_fee += 1000;
        }

        return $degree_registration_fee + $secruity_fee + $guests_fee;
    }

    public function generateReport($type = 'all')
    {
        $records = Convocation::with(['program', 'program.department', 'program.faculty'])->get();
        if ($type == 'all') {
            $this->generateAllReport($records);
        }
        if ($type == 'guests') {
            $this->generateGuestsReport($records);
        }
    }

    public function generateAllReport($records)
    {
        $csv = Writer::createFromFileObject(new SplTempFileObject());
        $csv->insertOne([
            'Challan#', 'Session', 'Name', 'Father Name', 'CNIC', 'Faculty', 'Department', 'Program', 'Registration Number', 'Address', 'Phone', 'Email', 'Gender', 'Guest 1 Name', 'Guest 1 CNIC', 'Guset 1 Relation', 'Guest 1 Contact', 'Guest 2 Name', 'Guest 2 CNIC', 'Guest 2 Relation', 'Guest 2 Contact'
        ]);
        foreach ($records as $record) {
            $csv->insertOne([
                $record->challan_number,
                $record->academic_session,
                $record->name,
                $record->fathers_name,
                $record->cnic,
                $record->program->faculty->title ?? '',
                $record->program->department->title ?? '',
                $record->program->title ?? '',
                $record->registration_number,
                $record->residential_address,
                $record->mobile_phone,
                $record->email,
                $record->gender,
                $record->guest_1_name,
                $record->guest_1_cnic,
                $record->guest_1_relationship,
                $record->guest_1_contact,
                $record->guest_2_name,
                $record->guest_2_cnic,
                $record->guest_2_relationship,
                $record->guest_2_contact
            ]);
        }
        $csv->output('Convocation-Full-Report.csv');
        die;
    }
}
