<?php

namespace App\Http\Controllers;

use App\Model;
use App\Model\{Faculty, Merit, MeritList, MeritManage, Result, Programme, RollNumber, Application};
use Illuminate\Support\Facades\Storage;
use League\Csv\Reader;
use League\Csv\Writer;
use SplTempFileObject;

class QuotaController extends Controller
{
    public function index ( $id = '' ){   
        $programs = Programme::where('status', '1')->get(); 
        $status = request()->session()->get('status');
        $error = request()->session()->get('error');
    	return view('quota.index', compact('programs', 'status', 'error'));
    }

    public function store(){
        $path = request()->file('fileToUplaod')->store('quota');
        $csv = Reader::createFromPath(storage_path('app/') . $path, 'r');
        $records = $csv->setOffset(1)->fetchAll();

        $invalid = '';
        // request('programme')
        foreach ($records as $record) {
            $recrd = preg_replace('/[^0-9]/', '', $record[0]);
            // check roll number
            $roll_Number = RollNumber::where('id', $recrd)->first();
            if($roll_Number){
                Application::where('id', $roll_Number->fkApplicationId)->update(
                    [
                        'pqm' => $record[2],
                        'testMarks' => $record[3],
                        'totalMarks' => $record[4]
                    ]
                );
            }
        } //endforeach
        return back()->with('status', 'Uploaded Successfully');
    }
    public function download(){
        // request('programme')
        // request('selectGender')
        $applications = Application::with(['applicant', 'program', 'applicant.detail', 
        'applicant.detail.district', 'getRollNumber'])
        ->where('fkProgramId', request('programme'))
        ->whereNotNull('totalMarks')
        ->getStatusByGender('>=', '1', request('selectGender'), '>=', 1);
        // dd($applications);
        // $fileName = $facultyName. '-'.$fileName;
        $csv = Writer::createFromFileObject(new SplTempFileObject());
        $csv->insertOne(
            [
                'S.No', 
                'Form No', 
                'Roll No',
                'Name',
                'CNIC',
                'Father Name',
                'Mobile No',
                'Email',
                'Domicile',
                'PQM',
                'Test Marks',
                'Total Marks'
            ]
        );
        $serialNumber = 1;
        foreach ( $applications as $application ) {
            $csv->insertOne(
                [
                    $serialNumber ?? '',
                    $application->id ?? '',
                    $application->getRollNumber->id ?? '',
                    $application->applicant->name ?? '',
                    $application->applicant->cnic ?? '',
                    $application->applicant->detail->fatherName ?? '',
                    $application->applicant->detail->mobile ?? '',
                    $application->applicant->email ?? '',
                    $application->applicant->detail->province->provName ?? '',
                    $application->pqm ?? '',
                    $application->testMarks ?? '',
                    $application->totalMarks ?? ''
                ]
            );
            $serialNumber++;
        }
        $fileName='export.csv';
        $csv->output($fileName);
        die;
    }
}
