<?php

namespace App\Http\Controllers;

use League\Csv\Writer;
use App\Jobs\MakeReport;
use App\Model\Applicant;
use App\Model\ResultList;
use App\Model\Application;
use App\Model\PreviousQualification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Traits\PQM;

class TestController extends Controller
{
    public function getIncorrectPrograms()
    {
        $applications = Application::with(['applicant', 'program', 'program.programscheduler'])->get();
        $ids = [];
        $dateCreated = [];
        foreach ($applications as $application) {
            if ($application->program) {
                if ($application->applicant->fkGenderId != $application->program->programscheduler->fkGenderId && $application->program->programscheduler->fkGenderId != 1) {
                    $ids[] = $application->id;
                    $dateCreated[] = $application->created_at;
                }
            }
        }
        dd($ids, $dateCreated);
    }
    public function index($id)
    {
        $application = Application::find($id);
        $feeVerifiedStatus      = ($application->fkCurrentStatus >= 4)   ? 'Yes' : 'No';
        $documentVerifiedStatus = ($application->fkCurrentStatus >= 5) ? 'Yes' : 'No';
        $documentRejectedStatus = ($application->fkCurrentStatus == 6) ? 'Yes' : 'No';

        //PQM Vars
        $level = $application->program->fkLeveliId;
        $preReq = $application->program->fkReqId;
        $sscTotal = $application->applicant->previousQualification->SSC_Total;
        $sscObtained = $application->applicant->previousQualification->SSC_Composite;

        $hsscTotal = $application->applicant->previousQualification->HSSC_Total;
        $hsscObtained = $application->applicant->previousQualification->HSSC_Composite;
        $hsscTotalPart1 = $application->applicant->previousQualification->HSSC_Total1;
        $hsscTotalPart2 = $application->applicant->previousQualification->HSSC_Total2;
        $hsscObtainedPart1 = $application->applicant->previousQualification->HSSC1;
        $hsscObtainedPart2 = $application->applicant->previousQualification->HSSC2;

        $baBscTotal = $application->applicant->previousQualification->BA_Bsc_Total;
        $baBscObtained = $application->applicant->previousQualification->BA_Bsc;
            
        $mscTotal = $application->applicant->previousQualification->MSc_Total;
        $mscObtained = $application->applicant->previousQualification->MSc;
        $bsTotal = $application->applicant->previousQualification->BS_Total;
        $bsObtained = $application->applicant->previousQualification->BS;

        $msTotal = $application->applicant->previousQualification->MS_Total;
        $msObtained = $application->applicant->previousQualification->MS;

        $interviewDateTime = $application->program->programscheduler->interviewDateTime;


        return $this->calculatePqm(
            $application->id,
            $level,
            $interviewDateTime,
            $preReq,
            $sscTotal,
            $sscObtained,
            $hsscTotal,
            $hsscObtained,
            $hsscTotalPart1,
            $hsscTotalPart2,
            $hsscObtainedPart1,
            $hsscObtainedPart2,
            $baBscTotal,
            $baBscObtained,
            $mscTotal,
            $mscObtained,
            $bsTotal,
            $bsObtained,
            $msTotal,
            $msObtained
        );
    }

    public static function calculatePqm(
        $id,
        $level,
        $interviewDateTime,
        $preReq=0,
        $sscTotal = 0,
        $sscObtained = 0,
        $hsscTotal = 0,
        $hsscObtained = 0,
        $hsscTotalPart1 = 0,
        $hsscTotalPart2 = 0,
        $hsscObtainedPart1 = 0,
        $hsscObtainedPart2 = 0,
        $baBscTotal = 0,
        $baBscObtained = 0,
        $mscTotal = 0,
        $mscObtained = 0,
        $bsTotal = 0,
        $bsObtained = 0,
        $msTotal = 0,
        $msObtained = 0
    ) {
        if ($level == 1 || $level == 2) {
            /* BS & Diploma */
            return Self::calculatePqmForUnderGraduate($sscObtained, $sscTotal, $hsscObtained, $hsscTotal, $hsscObtainedPart1, $hsscTotalPart1, $hsscObtainedPart2, $hsscTotalPart2);
        } elseif ($level == 3) {
            /* MA,  MSc */
            if ($interviewDateTime !== '0000-00-00 00:00:00') {
                return Self::calculatePqmForMaMsc($baBscObtained, $baBscTotal);
            } else {
                return Self::calculatePqmForUnderGraduate($sscObtained, $sscTotal, $hsscObtained, $hsscTotal, $hsscObtainedPart1, $hsscTotalPart1, $hsscObtainedPart2, $hsscTotalPart2);
            }
        } elseif ($level == 4) {
            /* Masters */
            return Self::calculatePqmForMasters($mscObtained, $mscTotal, $bsObtained, $bsTotal);
        } elseif ($level == 5) {
            /* PostGraduate */
            return Self::calculatePqmForPostGraduate($msObtained, $msTotal);
        }
    }
    
    public static function calculatePqmForUnderGraduate($sscObtained, $sscTotal, $hsscObtained, $hsscTotal, $hsscObtainedPart1, $hsscTotalPart1, $hsscObtainedPart2, $hsscTotalPart2)
    {
        $ssc15Percent = 0;
        $hssc25Percent = 0;
        if ($sscTotal != '' || $sscTotal != 0) {
            $ssc15Percent = ((float)$sscObtained/(float)$sscTotal)*15;
        }
        if ($hsscTotal != '' || $hsscTotal != 0) {
            $hssc25Percent = ((float)$hsscObtained/(float)$hsscTotal)*25;
        } elseif ($hsscTotalPart1 != '') {
            $hssc25Percent = (float)$hsscObtainedPart1 / ((float)$hsscTotalPart1)*25;
        } elseif ($hsscTotalPart1 != '' || $hsscTotalPart2 != '') {
            $hssc25Percent = (((float)$hsscObtainedPart1+(float)$hsscObtainedPart2) / ((float)$hsscTotalPart1+(float)$hsscTotalPart2))*25;
        }
        $pqm = $ssc15Percent + $hssc25Percent;
        dd('undergrad '.$pqm);
        return $pqm = number_format((float)$pqm, 2, '.', '');
    }

    public static function calculatePqmForMaMsc($baBscObtained, $baBscTotal)
    {
        if ($baBscTotal !='' || $baBscTotal != 0) {
            $pqm = ((float)$baBscObtained/(float)$baBscTotal)*30;
            return $pqm = number_format((float)$pqm, 2, '.', '');
        }
        return 0;
    }

    public static function calculatePqmForMasters($mscObtained, $mscTotal, $bsObtained, $bsTotal)
    {
        $msc30Percent = '';
        $bs30Percent = '';
        $previousDegree30Percent = 0;
        if ($mscTotal && $bsTotal) {
            $bs30Percent =  ((float)$bsObtained/(float)$bsTotal)*30;
            $msc30Percent =  ((float)$mscObtained/(float)$mscTotal)*30;
            if ($msc30Percent >= $bs30Percent) {
                $previousDegree30Percent = $msc30Percent;
            } else {
                $previousDegree30Percent = $bs30Percent;
            }
        } elseif ($bsTotal) {
            $previousDegree30Percent = $bs30Percent =  ((float)$bsObtained/(float)$bsTotal)*30;
        } elseif ($mscTotal) {
            $previousDegree30Percent = $msc30Percent =  ((float)$mscObtained/(float)$mscTotal)*30;
        }

        // $pqm = $previousDegree30Percent;
        return number_format((float)$previousDegree30Percent, 2, '.', '');
    }

    public static function calculatePqmForPostGraduate($msObtained, $msTotal)
    {
        $previousDegree30Percent = 0;
        if ($msTotal) {
            $previousDegree30Percent = ((float)$msObtained / (float)$msTotal)*30;
        }
          
        // $pqm = $previousDegree30Percent;
        return number_format((float)$previousDegree30Percent, 2, '.', '');
    }

    public function epReport()
    {
        $applicants = Applicant::with(['previousQualification'])->whereHas('application', function ($query) {
            $query->where('fkCurrentStatus', '>=', '4');
        })->offset(20000)->take(6998)->get();
        $csv = Writer::createFromPath(storage_path('app/public/ep-report.csv'), 'w+');
        $csv->insertOne(
            ['CNIC','Level','Degree Type','Degree','Degree Title','Type','Session Type','Exam Type','Degree Awarding Institute', 'Obtain Marks', 'Total Marks','Year','Country']
        );
        foreach ($applicants as $applicant) {
            if ($applicant->previousQualification->SSC_Composite) {
                $csv->insertOne(
                    [
                        $applicant->cnic,
                        'Secondary School Certificate',
                        ' ', //degree type
                        $applicant->previousQualification->SSC_Title, // degree
                        'Matriculation', //degree title
                        ' ', //type
                        ' ', //session type
                        'Annual', //exam type
                        $applicant->previousQualification->SSC_From, // degree awarding institute
                        $applicant->previousQualification->SSC_Composite,
                        $applicant->previousQualification->SSC_Total,
                        ' ', //year
                        ' ' //country
                    ]
                );
            } //ending ssc

            if (($applicant->previousQualification->HSSC_Total) ||
                ($applicant->previousQualification->HSSC_Total1) ||
                ($applicant->previousQualification->HSSC_Total2)) {
                $hsscTotal = $hsscObtained = '';
                if ($applicant->previousQualification->HSSC_Total1) {
                    $hsscTotal = (integer)$applicant->previousQualification->HSSC_Total1 + (integer)$applicant->previousQualification->HSSC_Total2;
                    $hsscObtained = (integer)$applicant->previousQualification->HSSC1 + (integer)$applicant->previousQualification->HSSC2;
                } else {
                    $hsscTotal = $applicant->previousQualification->HSSC_Total;
                    $hsscObtained = $applicant->previousQualification->HSSC_Composite;
                }

                $csv->insertOne(
                    [
                        $applicant->cnic,
                        'Higher Secondary School Certificate',
                        ' ', //degree type
                        $applicant->previousQualification->HSC_Title, // degree
                        'Intermediate', //degree title
                        ' ', //type
                        ' ', //session type
                        'Annual', //exam type
                        $applicant->previousQualification->HSC_From, // degree awarding institute
                        $hsscObtained,
                        $hsscTotal,
                        ' ', //year
                        ' ' //country
                    ]
                );
            }

            if ($applicant->previousQualification->BA_Bsc_Total) {
                $csv->insertOne(
                    [
                        $applicant->cnic,
                        'Bachelor (14 Years) Degree',
                        ' ', //degree type
                        $applicant->previousQualification->BA_Bsc_Title, //degree
                        $applicant->previousQualification->BA_Bsc_Subject, // degree title
                        ' ', //type
                        ' ', //session type
                        $applicant->previousQualification->BSc_Exam_System, //exam type
                        $applicant->previousQualification->BA_Bsc_From, // degree awarding institute
                        $applicant->previousQualification->BA_Bsc,
                        $applicant->previousQualification->BA_Bsc_Total,
                        ' ', //year
                        ' ' //country
                    ]
                );
            }

            if ($applicant->previousQualification->MSc_Total) {
                $csv->insertOne(
                    [
                        $applicant->cnic,
                        'Master (16 Years) Degree',
                        ' ', //degree type
                        $applicant->previousQualification->MSc_Title, // degree
                        $applicant->previousQualification->MSc_Subject, //degree title
                        
                        ' ', //type
                        ' ', //session type
                        $applicant->previousQualification->Msc_Exam_System, //exam type
                        $applicant->previousQualification->MSc_From, // degree awarding institute
                        $applicant->previousQualification->MSc,
                        $applicant->previousQualification->MSc_Total,
                        ' ', //year
                        ' ' //country
                    ]
                );
            }

            if ($applicant->previousQualification->BS_Total) {
                $csv->insertOne(
                    [
                        $applicant->cnic,
                        'Bachelor (16 Years) Degree',
                        ' ', //degree type
                        $applicant->previousQualification->BS_Title, //degree
                        $applicant->previousQualification->BS_Subject, // degree title
                        ' ', //type
                        ' ', //session type
                        $applicant->previousQualification->BS_Exam_System, //exam type
                        $applicant->previousQualification->BS_From, // degree awarding institute
                        $applicant->previousQualification->BS,
                        $applicant->previousQualification->BS_Total,
                        ' ', //year
                        ' ' //country
                    ]
                );
            }

            if ($applicant->previousQualification->MS_Total) {
                $csv->insertOne(
                    [
                        $applicant->cnic,
                        $applicant->previousQualification->MS_Title,
                        ' ', //degree type
                        'MS', //degree
                        $applicant->previousQualification->MS_Subject, // degree title
                        
                        ' ', //type
                        ' ', //session type
                        $applicant->previousQualification->BS_Exam_System, //exam type
                        $applicant->previousQualification->MS_From, // degree awarding institute
                        $applicant->previousQualification->MS,
                        $applicant->previousQualification->MS_Total,
                        ' ', //year
                        ' ' //country
                    ]
                );
            }
        }
    }

    public function getResultList()
    {
        $result = ResultList::select('rollno')->get()->pluck('rollno')->toArray();
        return array_map(function ($rollno) {
            return (integer)$rollno;
        }, $result);
    }

    public function buildReport()
    {
        $rollNumbers = $this->getResultList();
        $applicants = Applicant::with(['application' , 'application.program', 'application.program.level', 'application.program.department', 'application.program.faculty', 'application.program.programscheduler', 'previousQualification', 'detail', 'detail.district', 'detail.district.province','address', 'address.country', 'application.getRollNumber'])->whereHas('application', function ($query) {
            $query->where('fkCurrentStatus', '>=', 4);
        })->offset(20000)->take(6999)->get();
        // foreach ( $applicants as $app )
        // {
        //     $test = $app->application->pluck('id');
        //     dump( (string) implode(",", array($test)) );
        // }
        // die();
        $csv = Writer::createFromPath(storage_path('app/public/make-report.csv'), 'w+');
        $csv->insertOne(
            ['Title','First Name','Middle Name','Last Name','Marital Status','Gender','Blood Group','Disability','DOB', 'Country', 'City','Domicile District','Domicile Province','Area','Religion','CNIC','Fathers Name','Father CNIC','Status','Father Occupation','Permanent Address','Mailing Address','Email','Mobile Phone','Social Contact','Application No','Admission Date','AdmissionYear','Faculty', 'Department','Level','Degree Type','Degree','Degree Title','Type','Session Type','Exam Type','Intake Type','Campus Category','University / SubCampus/Institute','Student Status']
        );

        foreach ($applicants as $applicant) {
            foreach ($applicant->application as $application) {
                $name = $this->splitName($applicant->name);
                $first = $middle = $last = '';
                if (count($name) >= 3) {
                    $first = $name[0] ?? '';
                    $middle = $name[1] ?? '';
                    $last = $name[2] ?? '';
                }
                if (count($name) <= 2) {
                    $first = $name[0] ?? '';
                    $last = $name[1] ?? '';
                }
                $status = 'Not Offered';
                $rn = $this->rollno($applicant)[0];
                if ($rn) {
                    if (in_array($rn, $rollNumbers)) {
                        $status = 'Offered';
                    }
                }

                $csv->insertOne(
                    [
                        ($applicant->fkGenderId == 3) ? 'Mr.' : 'Ms.', //title
                        $first ?? '', //first
                        $middle ?? '', //middle
                        $last ?? '', //last
                        ' ', //status
                        ($applicant->fkGenderId == 3) ? 'M' : 'F', //gender
                        ' ', //blood group
                        ' ', //disability
                        date('m-d-Y', strtotime($applicant->detail->DOB)), //dob
                        $applicant->address->country->iso ?? '', //country
                        $applicant->address->cityPo ?? '', //city
                        $applicant->detail->district->distName ?? '',//domicile district,
                        $applicant->detail->district->province->provName ?? '', //domicile province
                        ' ', //area
                        ' ', //religion
                        trim($applicant->cnic) ?? '', //cnic
                        $applicant->detail->fatherName ?? '', //father name
                        ' ', //father cnic
                        ($applicant->detail->fatherStatus == 1) ? 'Alive' : 'Deceased', //father status
                        $applicant->detail->fatherOccupation ?? '', //father occupation
                        $applicant->address->addressPmt ?? '', //permanent address
                        $applicant->address->addressPo ?? '', //mailing address
                        $applicant->email ?? '', //email
                        $applicant->detail->mobile ?? '', //mobile
                        ' ', //social contact,
                        $application->id, //application id
                        // $application->getRollNumber->id ?? '', //rollno
                        ' ', //admission date
                        '2018', //admission year
                        $application->program->faculty->title ?? '' , //faculty
                        $application->program->department->title ?? '', //department
                        $application->program->level->description ?? '', //level!
                        'Regular', //degree type
                        $application->program->level->programLevel ?? '', //degree!
                        $application->program->title ?? '', //degree title
                        '', //type
                        '', //session type
                        'Semester', //exam type
                        'Fall', //intake type
                        'Main', //campus category
                        'International Islamic University H-10/4 Islamabad', //Univeristy
                        $status
                    ]
                );
            }
        }
        // MakeReport::dispatch($applicants);
    }

    public function getLevel($applicant)
    {
        $title = $applicant->application->pluck('program.level.programLevel');
        return (string) implode(",", array($title));
    }

    public function getLevelDescription($applicant)
    {
        $title = $applicant->application->pluck('program.level.description');
        return (string) implode(",", array($title));
    }

    public function getPrograms($applicant)
    {
        $title = $applicant->application->pluck('program.title');
        return (string) implode(",", array($title));
    }

    public function rollno($applicant)
    {
        return $applicant->application->pluck('getRollNumber.id');
    }

    public function getDepartments($applicant)
    {
        $title = $applicant->application->pluck('program.faculty.title');
        return (string) implode(",", array($title));
    }

    public function getFaculties($applicant)
    {
        $title = $applicant->application->pluck('program.department.title');
        return (string) implode(",", array($title));
    }

    public function getApplicationsIdArray($applicant)
    {
        $ids = $applicant->application->pluck('id');
        return (string) implode(",", array($ids));
    }

    public function splitName($name)
    {
        return explode(' ', $name);
    }
    public function updatePqmPreviousSemesters()
    {
        // dd('aaa');
        $prevoiousQyali = PreviousQualification::whereNotNull('pqm')->where('fkSemesterId', '!=', 13)->get();
        // dd($prevoiousQyali);
        foreach ($prevoiousQyali as $pqm) {
            Application::where('fkApplicantId', $pqm->fkApplicantId)
            ->where('fkSemesterId', $pqm->fkSemesterId)
            ->where('fkSemesterId', '!=', '13')
            ->update(
                [
                    'pqm' => $pqm->pqm
                ]
            );
        }
    }

    public function updatePqmForFall2020()
    {
        $application = Application::with(['program','faculty','program.programscheduler','applicant','applicant.previousQualification'])->find($id);
        $this->generateAndStorePQM($application);
    }

    public function generateAndStorePQM($application)
    {
        $pqm = $this->calculatePqm(
            $application->id,
            $application->faculty->title,
            $application->applicant->previousQualification->HSC_Title,
            $application->applicant->previousQualification->resultAwaiting,
            $application->program->title,
            $application->applicant->previousQualification->llb_obtained_marks,
            $application->applicant->previousQualification->llb_total_marks,
            $application->program->fkLeveliId,
            $application->program->programscheduler->interviewDateTime,
            $application->program->fkReqId,
            $application->applicant->previousQualification->SSC_Total,
            $application->applicant->previousQualification->SSC_Composite,
            $application->applicant->previousQualification->HSSC_Total,
            $application->applicant->previousQualification->HSSC_Composite,
            $application->applicant->previousQualification->HSSC_Total1,
            $application->applicant->previousQualification->HSSC_Total2,
            $application->applicant->previousQualification->HSSC1,
            $application->applicant->previousQualification->HSSC2,
            $application->applicant->previousQualification->BA_Bsc_Total,
            $application->applicant->previousQualification->BA_Bsc,
            $application->applicant->previousQualification->MSc_Total,
            $application->applicant->previousQualification->MSc,
            $application->applicant->previousQualification->BS_Total,
            $application->applicant->previousQualification->BS,
            $application->applicant->previousQualification->MS_Total,
            $application->applicant->previousQualification->MS
        );

        PreviousQualification::where('fkApplicantId', $application->fkApplicantId)
            ->where('fkSemesterId', $this->currentSemester)
            ->update([ 'pqm' => $pqm ]);
    }
}
