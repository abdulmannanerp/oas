<?php

namespace App\Http\Controllers;

use App\Model\Programme;
use App\Model\PreviousStats;
use App\Model\Semester;

class PreviousStatsController extends Controller{


    public function index(){
		$status = request()->session()->get('status');
		if ( request()->method() == 'POST' ) {

				$PreviousStats = PreviousStats::where('fkProgramId',request('program'))->where('fkSemesterId',request('semester'))->get();
				if(count($PreviousStats) > 0){
					return back()->with('status', 'Information Already Exists!');
				}
				PreviousStats::create(
					[
						'fkProgramId' => request('program'),
						'fkSemesterId' => request('semester'),
						'status' => 1,
						'applied_male' =>request('applied_male'),
						'applied_female' =>request('applied_female'),
						'fee_confirmed_male' =>request('fee_confirmed_male'),
						'fee_confirmed_female' =>request('fee_confirmed_female'),
						'selected_male' =>request('selected_male'),
						'selected_female' =>request('selected_female'),
						'joined_male' =>request('joined_male'),
						'joined_female'=>request('joined_female'),
						'applied_total' =>request('applied_total'),
						'fee_confirmed_total' =>request('fee_confirmed_total'),
						'selected_total' =>request('selected_total'),
						'joined_total' =>request('joined_total')
					]
				);
				return back()->with('status', 'Information updated successfully!');

	}
		$program = Programme::select('pkProgId', 'title')->where('status', 1)->get();
		$semester = Semester::select('pkSemesterId', 'title')->get();
		return view ('previousstats.add', compact('program', 'PreviousStats', 'semester',  'status'));   
		
	}
	  
}
