<?php

namespace App\Http\Controllers\Application;

use Validator;
use Carbon\Carbon;
use App\Traits\PQM;
use App\Jobs\SendEmail;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Traits\BankChallanDetail;
use App\Http\Controllers\Controller;
use Illuminate\Pagination\LengthAwarePaginator;
use Barryvdh\DomPDF\Facade as PDF;
use App\Model\{Applicant, ApplicantDetail, Application, Logger, Permission, Programme, RollNumber, UserLastLogin, oasEmail, PreviousQualification, ChallanDetail, ProgramFee, Semester, Result, UserLogQualification};

class ApplicationController extends Controller
{
    use PQM;
    use BankChallanDetail;
    protected $currentUserPermissions;
    protected $facultyPermissions;
    protected $departmentPermissions;
    protected $genderPermissions;
    protected $nationalityPermissions;
    protected $selectedSemester;

    public function __construct()
    {
        // $this->selectedSemester = $this->getCurrentSemester()->pkSemesterId;
    }

    public function index( $semester = '' )
    {
        $this->selectedSemester = $semester;
        if ( $semester == '' ) 
            $this->selectedSemester = Semester::where('status', '1')->first()->pkSemesterId;
        $this->assignPermissions();
        $paginate = (empty(request()->entries)) ? '10' : request()->entries;
        $filter = request('search_rdo');
        $searchable = request('search');
        switch ( $filter ) {
            case 'id':
                $applications = $this->getSearchByFormNumber( $searchable, $paginate );
                return $this->returnApplicationsView($filter, $searchable, $paginate, $applications, $this->selectedSemester);
                break;
            case 'name':
                $applications = $this->getSearchByName( $searchable, $paginate );
                return $this->returnApplicationsView($filter, $searchable, $paginate, $applications, $this->selectedSemester);
                break;
            case 'cnic':
                $applications = $this->getSearchByCnic( $searchable, $paginate );
                return $this->returnApplicationsView($filter, $searchable, $paginate, $applications, $this->selectedSemester);
                break;
            case 'email':
                $applications = $this->getSearchByEmail ( $searchable, $paginate );
                return $this->returnApplicationsView($filter, $searchable, $paginate, $applications, $this->selectedSemester);
                break;
            case 'rollno':
                $applications = $this->getSearchByRollNumber ( $searchable, $paginate );
                return $this->returnApplicationsView($filter, $searchable, $paginate, $applications, $this->selectedSemester);
                break;
            case 'challan_no':
                $applications = $this->getSearchByChallanNumber ( $searchable, $paginate );
                return $this->returnApplicationsView($filter, $searchable, $paginate, $applications, $this->selectedSemester);
                break;
            default:
                return $this->returnApplicationsView($filter, $searchable, $paginate, '', $this->selectedSemester);
                break;
        } //ending switch 
    }

    public function assignPermissions()
    {
        $this->currentUserPermissions = $this->getCurrentUserPermissions();
        $this->facultyPermissions = explode(',', $this->currentUserPermissions->fkFacultyId);
        $this->departmentPermissions = explode(',',$this->currentUserPermissions->fkDepartmentId);
        $this->genderPermissions = $this->currentUserPermissions->fkGenderId;
        $this->nationalityPermissions = explode(',',$this->currentUserPermissions->fkNationality);
    }

    public function checkIfHasFullAccess() {
        if 
        (  count ( $this->facultyPermissions ) == 9 && 
            $this->departmentPermissions[0] == '' && 
            $this->genderPermissions == 1 && 
            (count( $this->nationalityPermissions) == 3 || 
            $this->nationalityPermissions[0] == '' ||  $this->nationalityPermissions[0] == 'NULL') 
        )
            return true;
        return false;
    }

    public function checkIfHasLimitedAccess( $applications )
    {
        $filtered = $applications->filter(function ( $value, $key ) {
            return $this->filterApplication ( $value );
        });
        return $filtered;
    }

    public function filterApplication($application)
    {
        /* Apply gender & overseas permissions to incomplete applications */
        $gender = $application->applicant->fkGenderId;            
        $nationality = $application->applicant->fkNationality;
        if ( $application->fkCurrentStatus == 1 ) {
            if ( 
                ($this->genderPermissions == 1 || $gender == $this->genderPermissions) &&
                (empty( $this->nationalityPermissions[0] ) || in_array( $nationality, $this->nationalityPermissions ) || $this->nationalityPermissions[0] == 'NULL')
            )
                return true;
        }
        // $program = Programme::where('pkProgId', $application->fkProgramId)->select('fkFacId', 'fkDepId')->first();
        $facultyId = $application->program->fkFacId ?? '';
        $departmentId = $application->program->fkDepId ?? '';    
       
        if ( in_array($facultyId, $this->facultyPermissions) ) {
            if ( empty($this->departmentPermissions[0]) || in_array ( $departmentId, $this->departmentPermissions) ) {
                if ( $this->genderPermissions == 1 || $gender == $this->genderPermissions ) {
                    if ( empty( $this->nationalityPermissions[0] ) || in_array( $nationality, $this->nationalityPermissions ) || $this->nationalityPermissions[0] == 'NULL' )
                        return true;
                }
            }
        }
        return false;
    }

    public function checkAccessLevelAndGetApplication( $applications ) 
    {
        return ( $this->checkIfHasFullAccess() ) ? $applications : $this->checkIfHasLimitedAccess($applications);   
    }

    public function paginateResults($applications, $paginate)
    {
        $currentPage = LengthAwarePaginator::resolveCurrentPage();
        $currentPageResults = $applications->slice(($currentPage-1) * $paginate, $paginate)->all();
        $result = new LengthAwarePaginator($currentPageResults, count($applications), $paginate);
        $result->setPath(request()->url());
        return $result;
    }

    public function getSearchByFormNumber ( $formNumber, $paginate )
    {
        $applications = Application::with(['applicant', 'program', 'status'])->where('id', $formNumber)->where('fkSemesterId', $this->selectedSemester)->where(function ( $query ) {
                $query->where('a_status', '>=', 1)->orWhereNull('a_status');
            })->get();
        if ( $applications->count() ) {
            $applications = $this->checkAccessLevelAndGetApplication ( $applications );
            return $this->paginateResults( $applications, $paginate );
        }
        return '';
    }

    public function getSearchByName ( $name, $paginate )
    {
        $applications = Application::with(['applicant', 'program', 'status'])->where('fkSemesterId', $this->selectedSemester)->where(function ( $query ) {
                $query->where('a_status', '>=', 1)->orWhereNull('a_status');
            })->whereHas('applicant', function( $query) use ($name) {
            $query->where('name', 'like', '%'.$name.'%');
        })->get();
        if ( $applications->count() ) {
            $applications = $this->checkAccessLevelAndGetApplication ( $applications );
            return $this->paginateResults( $applications, $paginate );
        }
        return '';
    }

    public function getSearchByCnic ( $cnic,$paginate ) {
        $applications = Application::with(['applicant', 'program', 'status'])->where('fkSemesterId', $this->selectedSemester)->where(function ( $query ) {
                $query->where('a_status', '>=', 1)->orWhereNull('a_status');
            })->whereHas('applicant', function( $query) use ($cnic) {
            $query->where('cnic', $cnic );
        })->get();
        if ( $applications->count() ) {
            $applications = $this->checkAccessLevelAndGetApplication ( $applications );
            return $this->paginateResults( $applications, $paginate );
        }
        return '';
    }

    public function getSearchByEmail ( $email, $paginate ) 
    {
       $applications = Application::with(['applicant', 'program', 'status'])->where('fkSemesterId', $this->selectedSemester)->where(function ( $query ) {
                $query->where('a_status', '>=', 1)->orWhereNull('a_status');
            })->whereHas('applicant', function( $query) use ($email) {
            $query->where('email', $email);
        })->get();
        if ( $applications->count() ) {
           $applications = $this->checkAccessLevelAndGetApplication ( $applications );
            return $this->paginateResults( $applications, $paginate );
        }
        return '';
    }

    
    public function getSearchByRollNumber ( $rollno, $paginate ) 
    {
       $applications = Application::with(['applicant', 'program', 'status'])->where('fkSemesterId', $this->selectedSemester)->where(function ( $query ) {
                $query->where('a_status', '>=', 1)->orWhereNull('a_status');
            })->whereHas('getRollNumber', function( $query) use ($rollno) {
            $query->where('id', $rollno);
        })->get();
        if ( $applications->count() ) {
            $applications = $this->checkAccessLevelAndGetApplication ( $applications );
            return $this->paginateResults( $applications, $paginate );
        }
        return '';
    }

    public function getSearchByChallanNumber($Challanno, $paginate ){
            $applications = Application::with(['applicant', 'program', 'status'])->where('fkSemesterId', $this->selectedSemester)->where(function ( $query ) {
                $query->where('a_status', '>=', 1)->orWhereNull('a_status');
            })->whereHas('challan', function( $query) use ($Challanno) {
            $query->where('id', $Challanno);
        })->get();
        if ( $applications->count() ) {
            $applications = $this->checkAccessLevelAndGetApplication ( $applications );
            return $this->paginateResults( $applications, $paginate );
        }
        return '';
    }

    public function returnApplicationsView($filter, $searchable, $paginate, $applications, $selectedSemester)
    {
        $semesters = Semester::where('pkSemesterId', '>', '10')->get();
        $curr_sem = Semester::where('status', '1')->first();
        return view('applications.applications', compact('filter', 'searchable', 'paginate', 'applications', 'semesters', 'selectedSemester', 'curr_sem'));
    }
    public static function returnListNumber($programId, $rollNumber){
        // Result::with('list')->where('fkProgrammeId', $programId)
        $sql = "SELECT pkResultsId from tbl_oas_results rs inner join tbl_oas_result_list rl on rs.pkResultsId=rl.fkResultId  where rs.fkProgrammeId=".$programId." and rs.fkProgrammeId in(131, 132) and rl.rollno=".$rollNumber;
        $id = \DB::select($sql);
        if($id){
            return $id[0]->pkResultsId;
        }            
    }

    public function getSpecificSemesterApplications()
    {
        return redirect()->route('applicants', ['semester' => request('semesterSelector')]);
    }

    public function viewLog($id ='', $fkApplicantId = ''){
        $current_semes = Semester::where('status', '1')->first()->pkSemesterId;
        $previousQualificationLog = UserLogQualification::where('applicant_id', request('fkApplicantId'))->where('fkSemesterId', $current_semes)->where('description', 'Previous Qualification')->get();
        $documentsLog = UserLogQualification::where('application_id', request('id'))->where('fkSemesterId', $current_semes)->where('description', 'Document Verify')->get();
        // $previousQualificationLog = unserialize($previousQualificationLog->existing_value);
        // dd($previousQualificationLog, $documentsLog);
        return view ('applications.viewLog', compact('previousQualificationLog', 'documentsLog'));    
    }

    public function editApplication($id ='')
    {
        $status = request()->session()->get('status');
        if ( request()->method() == 'POST' ) {
            $this->validate(request(), [
                'email' => 'required|unique:tbl_users,email,'.request('applicant').',userId',
                'cnic' => 'required|unique:tbl_users,cnic,'.request('applicant').',userId'
            ]);

            if (  request('id') ) {
                /** update users program */
                $application = Application::where('id', request('id'))->first();
                $logger = Logger::where('user_id', request('applicant'))->where('description', 'Program Updated')->get();
                if ( $application->fkProgramId != request('program') && request('program') ) {
                    Logger::create(
                        [
                            'user_id' => request('applicant'),
                            'application_id' => request('id'),
                            'modified_by' => auth()->id(),
                            'existing_value' => $application->fkProgramId,
                            'updated_value' => request('program'),
                            'description' => 'Program Updated'
                        ]
                    );
                $newProgram = Programme::where('pkProgId', request('program'))->first();
                // Dev Mannan: Code for application program, department and faculty update
                Application::where('id', request('id'))->update(
                    [
                        'fkProgramId' => request('program'),
                        'fkDepId' => $newProgram->fkDepId,
                        'fkFacId' => $newProgram->fkFacId
                    ]
                    );
                    $this->updateFeeChallan(request('id'), request('program'), $application->fkSemesterId);
                // Dev Mannan: Code ends
                }

            }
            /** Update only user info */
            $applicant = Applicant::where('userId', request('applicant'))->first();
            Applicant::where('userId', request('applicant'))->update(
                [
                    'name' => request('name'),
                    'cnic' => request('cnic'),
                    'email' => request('email'),
                    'status' => request('status'),
                    'password' => (request('password')) ?  bcrypt(request('password')) : $applicant->password
                ]
            );
            return back()->with('status', 'Information updated successfully!');
        }

        if ( $id ) {
            $application = Application::with(['applicant'])->find($id);
            $programs = Programme::select('pkProgId', 'title')->get();
            $current_semes = Semester::where('status', '1')->first()->pkSemesterId;
            $logs = Logger::with(['user', 'existingProgram', 'updatedProgram'])->where('application_id', $id)->latest()->get();
            return view ('applications.edit', compact('application', 'programs', 'status', 'logs', 'current_semes'));    
        }
        return redirect()->route('applicants');
    }

    public function updateFeeChallan($id, $program, $semester)
    {
        ChallanDetail::where('fkApplicationtId', $id)->whereIn('challan_type',array(2,3))->update(['status' => 0]);
        $roll_no = RollNumber::where('fkApplicationId',$id)->first();
        $fee = ProgramFee::where('fkProgramId',$program)->first();
        if(!$roll_no){
            $roll_no='';
        }else{
            $roll_no = $roll_no->id;
        }
        $challanInfo = $this->insertInfo(
            $roll_no, 
            $id,
            $semester, 
            $program, 
            '2', 
            '',
            $fee->admission_fee,
            $fee->university_dues,
            '',
            '',
            '',
            (integer)$fee->admission_fee+(integer)$fee->university_dues
        );
        $challanInfo = $this->insertInfo(
            $roll_no, 
            $id,
            $semester, 
            $program, 
            '3', 
            '',
            '',
            '',
            $fee->library_security,
            $fee->book_bank_security,
            $fee->caution_money,
            (integer)$fee->library_security+(integer)$fee->book_bank_security+(integer)$fee->caution_money
        );
    }

    public function verify(Request $request, Application $application)
    {
        $application = Application::with(['program','program.programscheduler','applicant','applicant.previousQualification'])->where('id', $request->id)->first();
        if ($application) {
            $applied_faculty = Programme::where('pkProgId', $application->fkProgramId)->first();
            $tbl_user = Applicant::where('userId', $application->fkApplicantId)->first();
//     $applied_faculty->faculty->abbrev
            $data = array('name' => $tbl_user->name, 'program_name' => $application->program->title);
            $roll_number = RollNumber::where('fkApplicationId', $request->id)->first();
            // PQM Variables
            $level = $application->program->fkLeveliId;
            $interviewDateTime = $application->program->programscheduler->interviewDateTime;
            $preReq = $application->program->fkReqId;
            $sscTotal = $application->applicant->previousQualification->SSC_Total;
            $sscObtained = $application->applicant->previousQualification->SSC_Composite;

            $hsscTotal = $application->applicant->previousQualification->HSSC_Total;
            $hsscObtained = $application->applicant->previousQualification->HSSC_Composite;
            $hsscTotalPart1 = $application->applicant->previousQualification->HSSC_Total1;
            $hsscTotalPart2 = $application->applicant->previousQualification->HSSC_Total2;
            $hsscObtainedPart1 = $application->applicant->previousQualification->HSSC1;
            $hsscObtainedPart2 = $application->applicant->previousQualification->HSSC2;

            $baBscTotal = $application->applicant->previousQualification->BA_Bsc_Total;
            $baBscObtained = $application->applicant->previousQualification->BA_Bsc;
            
            $mscTotal = $application->applicant->previousQualification->MSc_Total;
            $mscObtained = $application->applicant->previousQualification->MSc;
            $bsTotal = $application->applicant->previousQualification->BS_Total;
            $bsObtained = $application->applicant->previousQualification->BS;

            $msTotal = $application->applicant->previousQualification->MS_Total;
            $msObtained = $application->applicant->previousQualification->MS;
            if ($request->selection == 'verify') {
                $application->doumentsVerified = date("Y-m-d H:i:s");
                $application->fkReviewedBy = auth()->id();
                $application->fkCurrentStatus = 5;


                if ($roll_number) {
                    $roll_number->status = 1;
                } else {
                    $roll_number = new RollNumber;
                    $roll_number->fkApplicationId = $request->id;
                    $roll_number->fkApplicantId = $application->fkApplicantId;
                    $roll_number->status = 1;
                    $roll_number->fkSemesterId = $this->selectedSemester;
                }
                $pqm = $this->calculatePqm(
                    $application->id, $level, $interviewDateTime, $preReq, $sscTotal, $sscObtained, $hsscTotal, $hsscObtained,$hsscTotalPart1, $hsscTotalPart2, $hsscObtainedPart1, $hsscObtainedPart2, $baBscTotal, $baBscObtained, $mscTotal, $mscObtained, $bsTotal, $bsObtained, $msTotal, $msObtained
                );
                PreviousQualification::where('fkApplicantId', $application->fkApplicantId)->update(
                    [
                        'pqm' => $pqm
                    ]
                );
                SendEmail::dispatch(
                    $applied_faculty->faculty->abbrev, 
                    $data, 
                    $tbl_user->email, 
                    'Documents Verified', 
                    'applications.verify'
                );
            }
            if ($request->selection == 'reject') {

                $comment = array('comment' => $request->comments);
                $data = $data + $comment;
                $application->comments = $request->comments;
                $application->fkReviewedBy = auth()->id();
                $application->fkCurrentStatus = 6;
                if ($roll_number) {
                    $roll_number->status = 0;
                }
                $pqm = $this->calculatePqm(
                    $application->id, $level, $interviewDateTime, $preReq, $sscTotal, $sscObtained, $hsscTotal, $hsscObtained,$hsscTotalPart1, $hsscTotalPart2, $hsscObtainedPart1, $hsscObtainedPart2, $baBscTotal, $baBscObtained, $mscTotal, $mscObtained, $bsTotal, $bsObtained, $msTotal, $msObtained
                );
                PreviousQualification::where('fkApplicantId', $application->fkApplicantId)->update(
                    [
                        'pqm' => $pqm
                    ]
                );
                SendEmail::dispatch(
                    $applied_faculty->faculty->abbrev, 
                    $data, 
                    $tbl_user->email, 
                    'Documents Rejected', 
                    'applications.reject'
                );
            }
            if (session()->get('gid') == 1 || session()->get('gid') == 6 || session()->get('gid') == 4 || session()->get('gid') == 5) {
                $ret = $request->selection . '-1';
            } else {
                $ret = $request->selection;
            }

            $application->save();
            $roll_number->save();
            return $ret;
        }
    }

    public function printApplication(Request $request)
    {
        $curr_sem = Semester::where('status', '1')->first();
        $adminSession = request()->session()->get('gid');
        $adminGroupIds = [1,2,3,4,5,6,7,8,9,11];
        if ( ! in_array($adminSession, $adminGroupIds)) {
            $authenticateUser = auth()->id();
            $authentcatedUserApplications = Application::where('fkApplicantId', $authenticateUser)->get()->pluck('id')->toArray();
            if ( !in_array($request->oas_app_id, $authentcatedUserApplications ) ) 
                return '401 Unauthorized Access';
        }
        if(isset($request->semesterid)){
            $current_semester = $request->semesterid;

        }else{
            $current_semester = $curr_sem->pkSemesterId;
        }
// dd($request->semesterid);
        $oas_applications = Application::where('id', $request->oas_app_id)->where('fkSemesterId', $current_semester)->first();
        $oas_applicatdetail = ApplicantDetail::where('fkApplicantId',$oas_applications->fkApplicantId)->first();
        // $this->thumbPrint('http://admission.iiu.edu.pk/'.$oas_applicatdetail->pic); 
        return view('applications.printApplication', compact('oas_applications', 'current_semester'));
    }

    public function printNoc(Request $request){
        $curr_sem = Semester::where('status', '1')->first();
        if(isset($request->semesterid)){
            $current_semester = $request->semesterid;
        }else{
            $current_semester = $curr_sem->pkSemesterId;
        }
        $oas_applications = Application::with(['applicant', 'applicant.address', 'applicant.applicantDetail', 'program'])->where('id', $request->oas_app_id)->where('fkSemesterId', $current_semester)->first();
        // return view('applications.printNoc', compact('oas_applications'));
        $pdf = PDF::loadView('applications.printNoc', compact('oas_applications'))->setPaper('a4', 'portrait');
        return $pdf->stream();
    }

    public function printChallan(Request $request){
        $oas_applications = Application::where('id', $request->oas_app_id)->first();
        $bankDetails = \DB::table('tbl_oas_banks')->where('accountFor', 1)->where('fkBankFor', $oas_applications->applicant->fkGenderId)->get();
        return view('applications.printChallan', compact('oas_applications', 'bankDetails'));
        
    }
    public function publicPrintRollNumber(Request $request){
        $applicant = Applicant::where('cnic', $request->cnic)->first();
        if($applicant){
            $oas_applications = Application::where('id', $request->form_no)->where('fkCurrentStatus', 5)->first();
            if($oas_applications){
                return view('rollnumber.rollnumber', compact('oas_applications'));
            }else{
                return back()->with('status', 'Roll Number slip is not available');
            }            
        }else{
            return back()->with('status', 'CNIC/Passport-No/B-Form is not valid');
        }
    }
    
    public function printRollNumber(Request $request)
    {
        $adminSession = request()->session()->get('gid');
        $adminGroupIds = [1,2,3,4,5,6,7,8,9];
        if ( ! in_array($adminSession, $adminGroupIds)) {
            $authenticateUser = auth()->id();
            $authentcatedUserApplications = Application::where('fkApplicantId', $authenticateUser)->get()->pluck('id')->toArray();
            if ( !in_array($request->oas_app_id, $authentcatedUserApplications ) ) 
                return '401 Unauthorized Access';
        }
        $oas_applications = Application::where('id', $request->oas_app_id)->first();
        $oas_applicatdetail = ApplicantDetail::where('fkApplicantId',$oas_applications->fkApplicantId)->first();
        // $this->thumbPrint('http://admission.iiu.edu.pk/'.$oas_applicatdetail->pic); 
        $active_semester = Semester::where('status', '1')->first()->pkSemesterId;
        return view('rollnumber.rollnumber', compact('oas_applications', 'active_semester'));
    }

    public function superadmin(Request $request, Application $application)
    {
        $application = Application::where('id', $request->id)->first();
        $logs = UserLastLogin::where('userId', $application->fkApplicantId)->get();
        // dump($logs);
        $fee_verify = 'NULL';
        $docverify = 'NULL';
        if ($application->docsVerifyAdmin) {
            $docverify = $application->docsVerifyAdmin->username;
        }
        if ($application->user) {
            $fee_verify = $application->user->username;
        }
        return response()->json(['application' => $application, 'fee_verified_by' => $fee_verify, 'documents_verified_by' => $docverify, 'logs'=>$logs]);
    }

    public function superadminid(Request $request){
        if($request->id){
            $users = \DB::table('tb_users')->where('id','=',$request->id)->get()->first();
            return response()->json(['username'=>$users->username]);
        }
    }
}
