<?php

namespace App\Http\Controllers\Application;

use App\Http\Controllers\Controller;
use App\Jobs\SendEmail;
use App\Model\ChallanDetail;
use App\Model\{Status, Application, oasEmail, Programme, Permission, FeeVerificationFailed};
use App\User;
use DB;
use Illuminate\Http\Request;
use League\Csv\{Reader, Writer, Statement};
use SplTempFileObject;
use App\Model\ChallanGrouping;
use Illuminate\Support\Facades\Log;


class VerificationController extends Controller
{
    protected $verified = [];
    protected $unverified = [];
    protected $verified_applications = '';
    protected $unverified_applications = '';
    protected $currentSemester;
    protected $export_applications_with_fee_verified;

    public function __construct()
    {
        $this->currentSemester = $this->getCurrentSemester()->pkSemesterId;
    }

    public function index()
    {
        $status = request()->session()->get('status');
        $applications = '';

        if (request()->method() == 'POST') {

            if (request('verify')) {

                $unSerialzedChallanIds = unserialize(request('verify'));
                DB::transaction(function () use ($unSerialzedChallanIds) {
                    $getApplicationsIdFromChallanTable = ChallanDetail::whereIn('id', $unSerialzedChallanIds)->get()->pluck('fkApplicationtId')->toArray();
                    $applications = Application::whereIn('id', $getApplicationsIdFromChallanTable)->where('fkSemesterId', $this->currentSemester)->update([
                        'fkFeeVerifiedBy' => auth()->guard('web')->id(),
                        'feeVerified' => date('Y-m-d H-i-s'),
                        'fkCurrentStatus' => 4
                    ]);
                    ChallanDetail::whereIn('id', $unSerialzedChallanIds)->update([
                        'fee_verified_by' => auth()->guard('web')->id(),
                        'fee_verified_date' => date('Y-m-d H-i-s'),
                        'status' => 4
                    ]);
                    $this->sendFeeVerificationEmail($getApplicationsIdFromChallanTable);
                });
                return back()->with('status', 'Records Verified Successfully');
            }
            if (request()->hasFile('csv')) {
                $applicationIds = $this->getRecoredsFromCsv();
                $applications = $this->checkPermissionsAndListRecords($applicationIds);
            } else {

                $applicationIds = request('application_ids'); // application_ids = Challan_numbers
                $applicationIds = $this->parseDataToArray($applicationIds);
                $applications = $this->checkPermissionsAndListRecords($applicationIds);
            }
        }
        return view('fee.verify', compact('status', 'applications'));
    }

    public function feeVerifyCron()
    {
        // Return those application ID's whose fee are submitted even against only 1 application for Level 1 (BS)
        $allApplications = Application::with(['applicant'])
            ->whereHas('applicant', function ($query) {
                $query->where('fkLevelId', 1);
            })
            ->where('fkSemesterId', $this->currentSemester)
            ->whereIn('fkCurrentStatus', [4, 5, 6])
            ->whereNotNull('fkFeeVerifiedBy')
            ->whereNotNull('feeVerified')
            ->distinct()
            ->get()->pluck('fkApplicantId')->toArray();
        // dd($allApplications);


        // Return all those ID's whose fee should be approved automatically becasue once fee is verified
        $value = Application::whereIn('fkApplicantId', $allApplications)
            ->where('fkSemesterId', $this->currentSemester)
            ->where('fkCurrentStatus', '>=', 2)
            ->where('fkCurrentStatus', '<', 4)
            ->get()->pluck('id')->toArray();
        // dd($value);
        // Lookup those ID's inside the challan detail table and fetch challan numbers
        $challanDetailNew = ChallanDetail::whereIn('fkApplicationtId', $value)
            ->pluck('id');

        // append those challan numbers to array so that it could be once verified
        foreach ($challanDetailNew as $v) {
            $unSerialzedChallanIds[] = $v;
        }
        // dd($unSerialzedChallanIds);

        if (!empty($unSerialzedChallanIds)) {
            DB::transaction(function () use ($unSerialzedChallanIds) {
                $getApplicationsIdFromChallanTable = ChallanDetail::whereIn('id', $unSerialzedChallanIds)->get()->pluck('fkApplicationtId')->toArray();
                $applications = Application::whereIn('id', $getApplicationsIdFromChallanTable)->where('fkSemesterId', $this->currentSemester)->update([
                    'fkFeeVerifiedBy' => 182,
                    'feeVerified' => date('Y-m-d H-i-s'),
                    'fkCurrentStatus' => 4
                ]);
                ChallanDetail::whereIn('id', $unSerialzedChallanIds)->update([
                    'fee_verified_by' => 182,
                    'fee_verified_date' => date('Y-m-d H-i-s'),
                    'status' => 4
                ]);
            });
            Log::info('Auto fee verified at : ' . now() . ' with ' . count($unSerialzedChallanIds) . ' records updated!');
        } else {
            Log::info('Auto fee verified at : ' . now() . ' with zero records updated!');
        }
    }

    public function feeVerifyFree()
    {

        // Return all those ID's whose fee should be approved automatically becasue once fee is verified
        $value = Application::where('fkSemesterId', $this->currentSemester)
            ->whereNull('fkFeeVerifiedBy')
            ->whereNull('feeVerified')
            ->where('new_application_status', 'new')
            ->where('fkCurrentStatus', 2)
            ->get()->pluck('id')->toArray();
        // dd($value);
        // Lookup those ID's inside the challan detail table and fetch challan numbers
        $challanDetailNew = ChallanDetail::whereIn('fkApplicationtId', $value)
            ->pluck('id');

        // append those challan numbers to array so that it could be once verified
        foreach ($challanDetailNew as $v) {
            $unSerialzedChallanIds[] = $v;
        }
        // dd($unSerialzedChallanIds);

        if (!empty($unSerialzedChallanIds)) {
            DB::transaction(function () use ($unSerialzedChallanIds) {
                $getApplicationsIdFromChallanTable = ChallanDetail::whereIn('id', $unSerialzedChallanIds)->get()->pluck('fkApplicationtId')->toArray();
                $applications = Application::whereIn('id', $getApplicationsIdFromChallanTable)->where('fkSemesterId', $this->currentSemester)->update([
                    'fkFeeVerifiedBy' => 182,
                    'feeVerified' => date('Y-m-d H-i-s'),
                    'fkCurrentStatus' => 4
                ]);
                ChallanDetail::whereIn('id', $unSerialzedChallanIds)->update([
                    'fee_verified_by' => 182,
                    'fee_verified_date' => date('Y-m-d H-i-s'),
                    'status' => 4
                ]);
            });
            Log::info('Free Auto fee verified at : ' . now() . ' with ' . count($unSerialzedChallanIds) . ' records updated!');
        } else {
            Log::info('Free Auto fee verified at : ' . now() . ' with zero records updated!');
        }
    }

    public function groupFeeVerify()
    {

        $allGroupChallans = ChallanGrouping::where('fkSemesterId', $this->currentSemester)->get();
        
        $ids = array();
        foreach ($allGroupChallans as $c) {

            $checkChallanPaidStatus = ChallanDetail::where('id', $c->challanId)->first();

            if ($checkChallanPaidStatus->fee_verified_by != NULL && $checkChallanPaidStatus->fee_verified_date != NULL) {

                if ($c->applicationId_1 != NULL) {
                    array_push($ids, $c->applicationId_1);
                }
                if ($c->applicationId_2 != NULL) {
                    array_push($ids, $c->applicationId_2);
                }
                if ($c->applicationId_3 != NULL) {
                    array_push($ids, $c->applicationId_3);
                }

                // Return all those ID's whose fee should be approved automatically becasue once fee is verified
                $value = Application::where('fkSemesterId', $this->currentSemester)
                    ->whereNull('fkFeeVerifiedBy')
                    ->whereNull('feeVerified')
                    ->where('fkCurrentStatus', 2)
                    ->whereIn('id', $ids)
                    ->get()->pluck('id')->toArray();

                // Lookup those ID's inside the challan detail table and fetch challan numbers
                // dump($value);
                if ($value) {
                    $challanDetailNew = ChallanDetail::whereIn('fkApplicationtId', $value)->pluck('id');

                    // append those challan numbers to array so that it could be once verified
                    foreach ($challanDetailNew as $v) {
                        $unSerialzedChallanIds[] = $v;
                    }

                    if (!empty($unSerialzedChallanIds)) {
                        DB::transaction(function () use ($unSerialzedChallanIds) {
                            $getApplicationsIdFromChallanTable = ChallanDetail::whereIn('id', $unSerialzedChallanIds)->get()->pluck('fkApplicationtId')->toArray();
                            $applications = Application::whereIn('id', $getApplicationsIdFromChallanTable)->where('fkSemesterId', $this->currentSemester)->update([
                                'fkFeeVerifiedBy' => 182,
                                'feeVerified' => date('Y-m-d H-i-s'),
                                'fkCurrentStatus' => 4
                            ]);
                            ChallanDetail::whereIn('id', $unSerialzedChallanIds)->update([
                                'fee_verified_by' => 182,
                                'fee_verified_date' => date('Y-m-d H-i-s'),
                                'status' => 4
                            ]);
                        });
                        Log::info('Group Auto fee verified at : ' . now() . ' with ' . count($unSerialzedChallanIds) . ' records updated!');
                    } else {
                        Log::info('Group Auto fee verified at : ' . now() . ' with zero records updated!');
                    }
                }
            }

            //    dd($value);
        }
        // dd($value);
        Log::info('Group Auto fee verified funcstion executed successfully!!!');
        
    }

    public function checkPermissionsAndListRecords($applicationIds)
    {
        $permissionsArray = $this->convertPermissiontoArray();
        // $applications = Application::with([
        //     'applicant' => function ( $query ) {
        //         $query->select('userId', 'name', 'fkGenderId' );
        //     },
        //     'program' => function ( $query ) {
        //         $query->select('pkProgId', 'title');
        //     },
        //     'user' => function ( $query ) {
        //         $query->select('*');
        //     }])->whereHas('applicant', function ( $query ) use ( $permissionsArray ) {
        //         $query->whereIn('fkGenderId', $permissionsArray);
        //     })->whereIn('id', $applicationIds )->where('fkSemesterId', $this->currentSemester)->get();
        $applications = ChallanDetail::with(['application', 'application.applicant', 'program', 'user'])
            ->whereIn('id', $applicationIds)
            ->where('fkSemesterId', $this->currentSemester)
            ->whereHas('application.applicant', function ($query) use ($permissionsArray) {
                $query->whereIn('fkGenderId', $permissionsArray);
            })->get();
        /* Not found challan numbers */
        $pluckAndConvertApplicationIdsToArray = $applications->pluck('id')->toArray();
        $failedApplications  = array_diff($applicationIds, $pluckAndConvertApplicationIdsToArray);
        $this->addApplicatoinNotFound($failedApplications);
        /* End not found challan numbers */
        return $applications;
    }

    public function sendFeeVerificationEmail($applicationIds)
    {
        $applications = Application::with(['applicant' => function ($query) {
            $query->select('userId', 'name', 'email');
        }, 'program' => function ($query) {
            $query->select('pkProgId', 'fkFacId', 'title');
        }, 'program.faculty' => function ($query) {
            $query->select('pkFacId', 'abbrev');
        }])->whereHas('applicant')->whereIn('id', $applicationIds)->where('fkSemesterId', $this->currentSemester)->select('id', 'fkApplicantId', 'fkProgramId')->get();
        foreach ($applications as $application) {
            if ($application->program) {
                $data = ['name' => $application->applicant->name, 'program' => $application->program->title];
                SendEmail::dispatch(
                    $application->program->faculty->abbrev,
                    $data,
                    $application->applicant->email,
                    'Fee Verified',
                    'mail.feeverified'
                );
            }
        }
    }

    public function getRecoredsFromCsv()
    {
        /* ApplicationIds = ChallanNumber */
        $applicationIds = [];
        $file = request()->file('csv');
        request('csv')->storeAs('public', 'fee-verification-file.csv');
        $csv = Reader::createFromPath(storage_path('app/public/') . 'fee-verification-file.csv', 'r');
        $records = $csv->setOffset(1)->fetchAll();
        if ($records) {
            foreach ($records as $record) {
                $applicationIds[] = $record[0];
            }
        }
        return $applicationIds;
    }

    public function parseDataToArray($data)
    {
        $data = trim($data);
        $delimeter = ' ';
        if (strpos($data, ',') !== false) :
            $delimeter = ',';
        endif;
        if (strpos($data, '-') !== false) :
            $delimeter = '-';
        endif;
        if (strpos($data, PHP_EOL) !== false) :
            $delimeter = PHP_EOL;
        endif;
        if ($delimeter == PHP_EOL) {
            return preg_split("/\\r\\n|\\r|\\n/", $data);
        }
        return  explode($delimeter, $data);
    }

    public function verified()
    {
        $startDate = request('start_date');
        $endDate = request('end_date');
        $export = request('export');
        $applications = $this->getApplicationsByStatus('feeVerified', $startDate, $endDate, '>=', '4', $export);
        // dump($applications);
        if (request()->method() == 'POST') {
            $this->exportRecordsToCsv($applications, 'fee-verified-');
        }
        return view('fee.verified', compact('applications', 'startDate', 'endDate'));
    }

    public function getApplicationsByStatus($column, $startDate, $endDate, $operator, $status, $export)
    {
        $permissionsArray = $this->convertPermissiontoArray();
        if ($startDate && $endDate) {
            /* Avoid appending multiple time to date $startDate = $startDate.' 00:00:00'; */
            if (count(explode(' ', $startDate)) <= 1) {
                $startDate = $startDate . ' 00:00:00';
                $endDate = $endDate . ' 23:59:59';
            }
            $applications =  Application::with(['applicant', 'challan', 'program', 'user', 'city'])->whereHas('applicant', function ($query) use ($permissionsArray) {
                $query->whereIn('fkGenderId', $permissionsArray);
            })->where('fkFeeVerifiedBy', '!=', 182)->where('fkSemesterId', $this->currentSemester)->whereBetween($column, [$startDate, $endDate])->where('fkCurrentStatus', $operator, $status)->orderBy($column, 'DESC');
            if ($export) {
                return $applications->get();
            }
            return $applications->paginate(100);
        } else {
            $applications = Application::with(['applicant', 'challan', 'program','faculty','department', 'user','applicant.programLevel', 'city'])->whereHas('applicant', function ($query) use ($permissionsArray) {
                $query->whereIn('fkGenderId', $permissionsArray);
            })->where('fkFeeVerifiedBy', '!=', 182)->where('fkSemesterId', $this->currentSemester)->where('fkCurrentStatus', $operator, $status)->orderBy($column, 'DESC');;
            if ($export) {
                return $applications->get();
            }
            return $applications->paginate(100);
        }
    }

    public function getApplicationsByStatusUnVerified($column, $startDate, $endDate, $operator, $status, $export)
    {
        $permissionsArray = $this->convertPermissiontoArray();
        if ($startDate && $endDate) {
            /* Avoid appending multiple time to date $startDate = $startDate.' 00:00:00'; */
            if (count(explode(' ', $startDate)) <= 1) {
                $startDate = $startDate . ' 00:00:00';
                $endDate = $endDate . ' 23:59:59';
            }
            $applications =  Application::with(['applicant', 'challan', 'program', 'user', 'city'])->whereHas('applicant', function ($query) use ($permissionsArray) {
                $query->whereIn('fkGenderId', $permissionsArray);
            })->where('fkSemesterId', $this->currentSemester)->whereBetween($column, [$startDate, $endDate])->where('fkCurrentStatus', $operator, $status)->orderBy($column, 'DESC');
            if ($export) {
                return $applications->get();
            }
            return $applications->paginate(100);
        } else {
            $applications = Application::with(['applicant', 'challan', 'program','faculty','department', 'user','applicant.programLevel', 'city'])->whereHas('applicant', function ($query) use ($permissionsArray) {
                $query->whereIn('fkGenderId', $permissionsArray);
            })->where('fkSemesterId', $this->currentSemester)->where('fkCurrentStatus', $operator, $status)->orderBy($column, 'DESC');;
            if ($export) {
                return $applications->get();
            }
            return $applications->paginate(100);
        }
    }

    public function convertPermissiontoArray()
    {
        $permissions = $this->getCurrentUserPermissions()->fkGenderId;
        $permissionsArray = $this->parseDataToArray($permissions);
        if ($permissions == 1) {
            $permissionsArray = ['2', '3'];
        }
        return $permissionsArray;
    }

    public function exportRecordsToCsv($applications, $filename)
    {
        $csv = Writer::createFromFileObject(new SplTempFileObject());
        if ($filename == 'fee-verified-') {
            $csv->insertOne(['Challan Number', 'Name','Faculty','Department', 'Program','Level', 'Verification Date', 'Verified By']);
            foreach ($applications as $application) {
                $csv->insertOne(
                    [
                        $application->challan->id ?? '',
                        $application->applicant->name ?? '',
                        $application->faculty->title ?? '',
                        $application->department->title ?? '',
                        $application->program->title ?? '',
                        $application->applicant->programLevel->programLevel ?? '',
                        date('d-M-Y h:i A',  strtotime($application->feeVerified)) ?? '',
                        $application->user->first_name. ' '. $application->user->last_name
                    ]
                );
            }
        } else {
            $csv->insertOne(['Challan Number', 'Name', 'Phone', 'Program', 'City', 'Branch Code', 'Date']);
            foreach ($applications as $application) {
                $date = date_create($application->feeSubmitted);
                $formattedDate = date_format($date, 'd-M-Y');
                $csv->insertOne(
                    [
                        $application->challan->id ?? '',
                        $application->applicant->name ?? '',
                        $application->applicant->detail->mobile ?? '',
                        $application->program->title ?? '',
                        $application->city->distName ?? '',
                        $application->branchCode ?? '',
                        $formattedDate
                    ]
                );
            }
        }

        $currentUser = User::find(auth()->guard('web')->id());
        $currentDate = date('d-m-Y h:i:s A');
        $message = "Report generated by $currentUser->first_name $currentUser->last_name on $currentDate via Online Admission System";
        $csv->insertOne([$message]);
        $fileName = $filename . $currentDate . '.csv';
        $csv->output($fileName);
        die();
    }

    public function unverified()
    {
        $startDate = request('start_date');
        $endDate = request('end_date');
        $export = request('export');
        $applications = $this->getApplicationsByStatusUnVerified('feeSubmitted', $startDate, $endDate, '=', '3', $export);
        // dd($applications);
        if (request()->method() == 'POST') {
            $this->exportRecordsToCsv($applications, 'fee-unverified-', $export);
        }
        return view('fee.unverified', compact('applications', 'startDate', 'endDate'));
    }

    public function addApplicatoinNotFound($applications)
    {
        $userId = auth()->guard('web')->id();
        foreach ($applications as $application) {
            FeeVerificationFailed::create([
                'user_id' => $userId,
                'form_number' => $application
            ]);
        }
    }

    public function failed()
    {
        if (request()->method() == 'POST') {
            $records = FeeVerificationFailed::where('user_id', auth()->guard('web')->id())->get();
            $this->exportFailedRecords($records);
        }
        $records = FeeVerificationFailed::where('user_id', auth()->id())->paginate(100);
        return view('fee.failed', compact('records'));
    }

    public function exportFailedRecords($records)
    {
        $csv = Writer::createFromFileObject(new SplTempFileObject());
        $csv->insertOne(['Challan Number', 'Created At']);
        foreach ($records as $record) {
            $csv->insertOne(
                [
                    $record->form_number ?? '',
                    $record->created_at->format('d-M-Y H:i:s') ?? ''
                ]
            );
        }
        $currentUser = User::find(auth()->guard('web')->id());
        $currentDate = \Carbon\Carbon::now()->format('d-M-Y H:i:s');
        $message = "Report generated by $currentUser->first_name $currentUser->last_name on $currentDate via Online Admission System";
        $csv->insertOne([$message]);
        $fileName = 'failed_records ' . $currentDate . '.csv';
        $csv->output($fileName);
        die();
    }







    // public function verifyAll() {
    //     $application_ids = request('application_ids');
    //     $application_ids_array = explode(',', $application_ids);

    //     $allApplicationsToSendEmail = Application::with('applicant')->whereIn('id', $application_ids_array)->get();

    //     foreach ( $allApplicationsToSendEmail as $app ) {

    //         $programId = $app->fkProgramId;
    //         if ( $programId != '' ) {
    //             $programme = Programme::where('pkProgId', $programId)->first();

    //             $abbrev = $programme->faculty->abbrev;
    //             $name = $app->applicant->name;
    //             $programTitle = $programme->title;
    //             $data = array('name' => $name, 'program' => $programTitle);
    //             $to = $app->applicant->email;
    //             $subject = 'Fee Verified';
    //             $view = 'mail.feeverified';
    //             SendEmail::dispatch(
    //                 $abbrev, 
    //                 $data, 
    //                 $to, 
    //                 'Fee Verified', 
    //                 'mail.feeverified'
    //             );
    //         }
    //     }

    //     $verifiedApplications = Application::whereIn('id', $application_ids_array)->where('fkCurrentStatus', '>=', '2')->whereNull('feeVerified')->get();
    //     $applications = '';
    //     DB::transaction(function () use ( $application_ids_array) {
    //         $applications = Application::whereIn('id', $application_ids_array)
    //             ->where('fkCurrentStatus', '>=', '2')
    //             ->whereNull('feeVerified')
    //             ->update(
    //                 [
    //                     'fkFeeVerifiedBy' => auth()->id(),
    //                     'feeVerified' => date('Y-m-d H-i-s'),
    //                     'fkCurrentStatus' => 4
    //                 ]
    //             );
    //     },3);

    //     if ( $applications  == 0 )
    //         return redirect()->back()->with('status', $applications .' record(s) verified');

    //     return redirect()->back()->with('verifiedApplications', $verifiedApplications)->with('status', 'Record(s) verified');
    // }

    // public function exportToCsv() {
    //     $applications_with_free_verification_failed = FeeVerificationFailed::all();

    //     $application_numbers = [];
    //     foreach ( $applications_with_free_verification_failed as $app) {
    //         $application_numbers[] = $app->form_number;
    //     }
    //     $cast_application_numbers_to_array = array_map(function($num){
    //         return (array)$num;
    //     }, $application_numbers);

    //     $csv = Writer::createFromFileObject(new SplTempFileObject());
    //     $csv->insertOne(['Application_Number', 'Attempt By']);
    //     foreach ( $applications_with_free_verification_failed as $application ) {
    //         $firstName = $application->user->first_name ?? '';
    //         $lastName = $application->user->last_name ?? '';
    //         $fullName = $firstName.' '.$lastName;
    //         $csv->insertOne(
    //             [
    //                 $application->form_number ?? '', 
    //                 $fullName
    //             ]
    //         );
    //     }
    //     $currentUser = User::find(auth()->id());
    //     $currentDate = date('d-m-Y h:i:s A');
    //     $message = "Report generated by $currentUser->first_name $currentUser->last_name on $currentDate via Online Admission System";
    //     $csv->insertOne([$message]);
    //     $currentDate = date ('d-m-Y h-i-s A');
    //     $fileName = 'users_with_records_not_found '.$currentDate.'.csv';
    //     $csv->output($fileName);
    // }

    // public function add_not_found_application_ids_to_database($applications_not_found)
    // {
    //     foreach ( $applications_not_found as $app ) {
    //         FeeVerificationFailed::firstOrCreate([
    //             'user_id' => auth()->id(),
    //             'form_number' => $app
    //         ]);
    //     }
    // }

    // public function getUserPermissions()
    // {
    //     $authId = auth()->id();
    //     return Permission::where('fkUserId', $authId)->first()->fkGenderId;
    // }

    // public function export_applications_with_fee_verified( $applications, $unverified = '' )
    // {
    //     $csv = Writer::createFromFileObject(new SplTempFileObject());

    //     if ( $unverified == 'unverified' ) {
    //         $csv->insertOne(['Application_Number', 'Name', 'Status', 'CNIC', 'Program Title', 'Bank Details', 'Branch Code', 'Date']);
    //         foreach ( $applications as $application ) {
    //             $date = date_create ( $application->feeSubmitted );
    //             $formattedDate = date_format ( $date, 'd-m-Y');
    //             $csv->insertOne(
    //                 [
    //                     $application->id ?? '', 
    //                     $application->applicant->name ?? '',
    //                     $application->status->status ?? '',
    //                     $application->applicant->cnic ?? '',
    //                     $application->program->title ?? '',
    //                     $application->bankDetails ?? '',
    //                     $application->branchCode ?? '',
    //                     $formattedDate ?? ''
    //                 ]
    //             );
    //         }
    //         $currentUser = User::find(auth()->id());
    //         $currentDate = date('d-m-Y h:i:s A');
    //         $message = "Report generated by $currentUser->first_name $currentUser->last_name on $currentDate via Online Admission System";
    //         $csv->insertOne([$message]);
    //         $currentDate = date ('d-m-Y h-i-s A');
    //         $fileName = 'applications_with_fee_unverified '.$currentDate.'.csv';
    //         $csv->output($fileName);
    //     } else {
    //         $csv->insertOne(['Application_Number', 'Name', 'CNIC', 'Program', 'Semester', 'Verified By', 'Date']);
    //         foreach ( $applications as $application ) {
    //             $csv->insertOne
    //             (
    //                 [
    //                     $application->id,
    //                     $application->applicant->name, 
    //                     $application->applicant->cnic ?? '',
    //                     $application->program->title ?? '',
    //                     $application->semester->title ?? '',
    //                     $application->user->first_name. ' '. $application->user->last_name,
    //                     $application->feeVerified
    //                 ]
    //             );
    //         }
    //         $currentUser = User::find(auth()->id());
    //         $currentDate = date('d-m-Y h:i:s A');
    //         $message = "Report generated by $currentUser->first_name $currentUser->last_name on $currentDate via Online Admission System";
    //         $csv->insertOne([$message]);
    //         $currentDate = date ('d-m-Y h-i-s A');
    //         $fileName = 'applications_with_fee_verified '.$currentDate.'.csv';
    //         $csv->output($fileName);
    //     }
    //     die;
    // }
}
