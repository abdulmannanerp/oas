<?php

namespace App\Http\Controllers\Applicants;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request; // used to get information

class ApplicantsController extends Controller {

    public function index() {
        return view('applications.applications');
    }

    public function search(Request $request) {
//        return $request->all();
        $this->validate($request,[
        'search' => 'required',
        'search_rdo' => 'required'
//        ‘something’ => ‘required|min:6’
        ]);
        echo (request()->get('search'));
        echo (request()->get('search_rdo'));
//        return view('applications.applications');
    }

}
