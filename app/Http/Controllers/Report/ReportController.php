<?php

namespace App\Http\Controllers\Report;

use App\User;
use Carbon\Carbon;
use League\Csv\Writer;
use SplTempFileObject;
use App\Model\ResultList;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;
use App\Model\{Application,Department,Faculty,Programme,Permission,Semester};

class ReportController extends Controller
{
    protected $currentUserPermissions;
    protected $currentUserGenderPermission;
    protected $bothMaleAndFemale = 1;
    protected $female = 2;
    protected $male = 3;
    protected $nationality = 1;
    protected $nationalityOperator = '>=';
    protected $allFaculties;
    protected $departmentPermissions;
    protected $selectedSemester;

    public function __construct()
    {
        // $this->selectedSemester = $this->getCurrentSemester()->pkSemesterId;
    }

    public function index()
    {
        $application = Application::where('fkSemesterId', $this->selectedSemester)->select('id')->first();
        $allApplications = Application::where('fkApplicantId', '!=', '0')->where('fkSemesterId', $this->selectedSemester )->count();
        $currentUserGenderPermission = $this->getCurrentUserPermissions()->fkGenderId;
        
        $allApplicatoinWithMaleApplicants = Application::select('id')->where('fkSemesterId', $this->selectedSemester)->whereHas('applicant', function ($query) {
            $query->select('userId')->where('fkGenderId', 3);
        })->count('id');
        $allApplicatoinWithFemaleApplicants = Application::select('id')->where('fkSemesterId',$this->selectedSemester)->whereHas('applicant', function ($query) {
            $query->select('userId')->where('fkGenderId', 2);
        })->count();

        return view('report.all', compact('application', 'allApplications', 'allApplicatoinWithMaleApplicants', 'allApplicatoinWithFemaleApplicants', 'currentUserGenderPermission'));
    }

    public function faculty( $semester = '' )
    {
        /**
         * Get user permission
        */     
        $this->currentUserPermissions =  $this->getCurrentUserPermissions();
        $this->currentUserGenderPermission = $this->currentUserPermissions->fkGenderId;
        $currentUserFacultyPermissions = explode ( ',', $this->currentUserPermissions->fkFacultyId);
        $this->departmentPermissions =  $this->currentUserPermissions->fkDepartmentId;    
        $allowedFaculties = Faculty::whereIn('pkFacId', $currentUserFacultyPermissions)->select('pkFacId', 'title', 'abbrev', 'email')->get();

        if ( $this->departmentPermissions ) :
            $this->departmentPermissions = explode ( ',',  $this->departmentPermissions);
        else :
            $this->departmentPermissions = 'not-found';
        endif;

        $semesters = $this->getAllSemesters();
        $selectedSemester = $this->selectedSemester = Semester::where('status', '1')->first()->pkSemesterId;
        if ( request()->isMethod('post') ) {
            $id = request('faculty');
            $faculty = request('faculty');
            $department = request('department');
            $program = request('program');
            $nationality =  request('overseas');
            $selectedSemester = $this->selectedSemester = request('semesterSelector');
            if ( !$selectedSemester ) {
                $selectedSemester = $this->selectedSemester = Semester::where('status', '1')->first()->pkSemesterId;
            }
            $this->allFaculties = $allowedFaculties;
            // $facultyTitle = Faculty::find($id)->title;
            
            if ( $nationality != '' ) :
                $this->nationality = request('overseas');
                $this->nationalityOperator = '=';
            endif;

            /**
             * Program Reoprt
             */
            if ( $faculty && $department && $program ) {
                $departments = Faculty::find ($faculty)->departments;
                if ( request()->session()->get('gid') == 9 )
                    $departments = Department::whereIn('pkDeptId', $this->departmentPermissions )->get();
                $selectedDepartment = $department;
                $programs = Department::find($department)->programme;
                $selectedProgram = $program;
                $statsToDisplay = 'program';
                $programTitle = Programme::find($program)->title;
                return $this->getProgramReport($program, $programTitle, $departments, $selectedDepartment, $programs, $selectedProgram, $semesters, $selectedSemester);
            }

            /**
             * Department Report
             */
            if ( $faculty && $department ) {
                $departments = Faculty::find ($faculty)->departments;
                if ( request()->session()->get('gid') == 9 )
                    $departments = Department::whereIn('pkDeptId', $this->departmentPermissions )->get();
                $selectedDepartment = $department;
                $programs = Department::find($department)->programme;
                $statsToDisplay = 'department';
                $departmentTitle = Department::find ($department)->title;
                $applications = Department::find( $department )->applications->pluck('id');
                return $this->getDepartmentReport($department, $applications, $departmentTitle, $departments, $selectedDepartment, $programs,$semesters, $selectedSemester);
            }

            /**
             * Faculty Report
             */
            if ( $faculty ) {
                $statsToDisplay = 'faculty';
                $facultyTitle = Faculty::find($faculty)->title;
                $applications = Faculty::find( $faculty )->applications->pluck('id');
                $departments = Faculty::find ($faculty)->departments;
                if ( request()->session()->get('gid') == 9 )
                    $departments = Department::whereIn('pkDeptId', $this->departmentPermissions )->get();
                $selectedDepartment = '';
                return $this->getFacultyReport($faculty, $facultyTitle, $applications, $departments, $selectedDepartment,$semesters, $selectedSemester, $selectedSemester);
            }
        }
        return view ('report.main',  
            [
                'faculties' => $allowedFaculties, 
                'departmentPermissions' => $this->departmentPermissions,
                'semesters' => $semesters,
                'selectedSemester' => $selectedSemester
            ]
        );
    }

    public function getFacultyReport($faculty, $facultyTitle, $applications, $departments, $selectedDepartment, $semesters, $selectedSemester) 
    {
        $facultyStats = [];

        $facultyStats['maleCount'] = $this->getFacultyMaleAndFemaleCount
                    (
                        $this->male, 
                        $applications,
                        $this->nationalityOperator,
                        $this->nationality
                    );

        $facultyStats['femaleCount'] = $this->getFacultyMaleAndFemaleCount
                    (
                        $this->female,
                        $applications,
                        $this->nationalityOperator,
                        $this->nationality
                    );

        $facultyStats['feePendingMale'] = $this->getFacultyStatusCount
                    (
                        $this->male,
                        $applications,
                        '=', '3'
                    );

        $facultyStats['feePendingFemale'] = $this->getFacultyStatusCount
                    (
                        $this->female,
                        $applications,
                        '=', '3'
                    );
        $facultyStats['feeVerifiedMale'] = $this->getFacultyStatusCount
                    (
                        $this->male,
                        $applications,
                        '>=', '4'
                    );

        $facultyStats['feeVerifiedFemale'] = $this->getFacultyStatusCount
                    (
                        $this->female,
                        $applications,
                        '>=', '4'
                    );

        $facultyStats['documentsVerifiedMale']  = $this->getFacultyStatusCount
                    (
                        $this->male,
                        $applications,
                        '=', '5'
                    );

        $facultyStats['documentsVerifiedFemale']= $this->getFacultyStatusCount
                    (
                        $this->female,
                        $applications,
                        '=', '5'
                    );

        $facultyStats['documentsRejectedMale']  = $this->getFacultyStatusCount
                    (
                        $this->male,
                        $applications,
                        '=', '6'
                    );

        $facultyStats['documentsRejectedFemale']= $this->getFacultyStatusCount
                    (
                        $this->female,
                        $applications,
                        '=', '6'
                    );
        
        return view('report.faculty-new', 
            [
                'id'                          => $faculty,
                'facultyStats'                => $facultyStats,
                'facultyTitle'                => $facultyTitle,
                'faculties'                   => $this->allFaculties,
                'currentUserGenderPermission' => $this->currentUserGenderPermission,
                'bothMaleAndFemale'           => $this->bothMaleAndFemale,
                'male'                        => $this->male,
                'female'                      => $this->female,
                'tempNationality'             => $this->nationality, // A helper variable, passed to reportController for faculty stats
                'departments'                 => $departments,
                'selectedDepartment'          => $selectedDepartment,
                'departmentPermissions'       => $this->departmentPermissions,
                'semesters'                   => $semesters,
                'selectedSemester'            => $selectedSemester
            ]
        );
    }

    public function getFacultyMaleAndFemaleCount($genderPermission, $applications, $nationalityOperator, $nationality)
    {
        $genderCount = 'N/A';
        if ( $this->currentUserGenderPermission == $this->bothMaleAndFemale || $this->currentUserGenderPermission == $genderPermission ): 
            $genderCount = Application::with(['applicant'])->whereIn('id', $applications)->where('fkSemesterId', $this->selectedSemester)->whereHas('applicant', function( $query ) use ( $genderPermission, $nationality, $nationalityOperator ) {
                $query->where('fkGenderId', $genderPermission)->where('fkNationality', $nationalityOperator,  $nationality);
            })->count();
        endif;
        return $genderCount;
    }

    public function getFacultyStatusCount($genderPermission, $applications, $operator, $status)
    {
        $statusCount = 'N/A';
        if ( $this->currentUserGenderPermission == $this->bothMaleAndFemale || $this->currentUserGenderPermission == $genderPermission ):
            $statusCount =  Application::whereIn('id', $applications)->where('fkSemesterId', $this->selectedSemester)->getStatusByGender($operator, $status, $genderPermission, $this->nationalityOperator, $this->nationality)->count();
        endif;
        return $statusCount;
    }

    public function getDepartmentReport($department, $applications, $departmentTitle, $departments, $selectedDepartment, $programs, $semesters, $selectedSemester)
    {
        $applicationIdsList = [];
        $departmentStats = [];
        foreach ( $applications as $key => $value ) :
            $applicationIdsList[] = $value;
        endforeach;
       $applicationIdsList = implode ( ',', $applicationIdsList);
       

        $departmentStats['maleCount'] = $this->getDepartmentMaleAndFemaleCount
                    (
                        $applications,
                        $this->male,
                        $this->nationalityOperator,
                        $this->nationality
                    );

        $departmentStats['femaleCount'] = $this->getDepartmentMaleAndFemaleCount
                    (
                        $applications,
                        $this->female,
                        $this->nationalityOperator,
                        $this->nationality
                    );

        $departmentStats['feePendingMale'] = $this->getDepartmentStatsCount
                    (
                        $this->male,
                        $applications,
                        '=', '3'
                    );
        $departmentStats['feePendingFemale'] = $this->getDepartmentStatsCount 
                    (
                        $this->female,
                        $applications,
                        '=', '3'
                    );
        $departmentStats['feeVerifiedMale'] = $this->getDepartmentStatsCount 
                    (
                        $this->male,
                        $applications,
                        '>=', '4'
                    );

        $departmentStats['feeVerifiedFemale'] = $this->getDepartmentStatsCount 
                    (
                        $this->female,
                        $applications,
                        '>=', '4'
                    );

        $departmentStats['documentsVerifiedMale'] = $this->getDepartmentStatsCount 
                    (
                        $this->male,
                        $applications,
                        '=', '5'
                    );

        $departmentStats['documentsVerifiedFemale'] = $this->getDepartmentStatsCount 
                    (
                        $this->female,
                        $applications,
                        '=', '5'
                    );

        $departmentStats['documentsRejectedMale'] = $this->getDepartmentStatsCount 
                    (
                        $this->male,
                        $applications,
                        '=', '6'
                    );

        $departmentStats['documentsRejectedFemale']= $this->getDepartmentStatsCount 
                    (
                        $this->female,
                        $applications,
                        '=', '6'
                    );

        return view('report.department', 
            [
                'id'                          => $department,
                'departmentStats'             => $departmentStats,
                'departments'                 => Department::all(),
                'faculties'                   => $this->allFaculties,
                'currentUserGenderPermission' => $this->currentUserGenderPermission,
                'bothMaleAndFemale'           => $this->bothMaleAndFemale,
                'male'                        => $this->male,
                'female'                      => $this->female,
                'nationality'                 => $this->nationality,
                'fileName'                    => $departmentTitle,
                'applicationIdsList'          => $applicationIdsList,
                'nationality'                 => $this->nationality,
                'nationalityOperator'         => $this->nationalityOperator,
                'departments'                 => $departments, 
                'selectedDepartment'          => $selectedDepartment,
                'programs'                    => $programs,
                'selectedProgram'             => '',
                'departmentPermissions'       => $this->departmentPermissions,
                'semesters'                   => $semesters,
                'selectedSemester'            => $selectedSemester
            ]
        );
    }

    public function getDepartmentMaleAndFemaleCount($applications, $gender, $nationalityOperator, $nationality)
    {
        return Application::with(['applicant'])->whereIn('id', $applications)->where('fkSemesterId', $this->selectedSemester)->whereHas('applicant', function( $query) use ($gender, $nationalityOperator, $nationality) {
            $query->where('fkGenderId', $gender)->where('fkNationality', $nationalityOperator, $nationality);
        })->count();
    }

    public function getDepartmentStatsCount($gender, $applications, $operator, $status )
    {
        return Application::whereIn('id', $applications)->where('fkSemesterId', $this->selectedSemester)->getStatusByGender($operator, $status, $gender, $this->nationalityOperator, $this->nationality)->count();
    }

    public function getProgramReport($programId, $programTitle, $departments, $selectedDepartment, $programs, $selectedProgram, $semesters, $selectedSemester) 
    {
        $programStats = [];
        $programStats['maleCount'] = $this->getProgramMaleAndFemaleCount
                    (
                        $programId,
                        $this->nationalityOperator,
                        $this->nationality, 
                        $this->male
                    );

        $programStats['femaleCount'] = $this->getProgramMaleAndFemaleCount
                    (
                        $programId,
                        $this->nationalityOperator,
                        $this->nationality,
                        $this->female
                    );

        $programStats['feePendingMale'] = $this->getProgramStatsCount
                    (
                        $programId,
                        $this->nationalityOperator,
                        $this->nationality,
                        '=', '3',$this->male
                    );

        $programStats['feePendingFemale'] = $this->getProgramStatsCount
                    (
                        $programId,
                        $this->nationalityOperator,
                        $this->nationality,
                        '=', '3', $this->female
                    );

        $programStats['feeVerifiedMale'] = $this->getProgramStatsCount
                    (
                        $programId,
                        $this->nationalityOperator,
                        $this->nationality,
                        '>=', '4', $this->male
                    );

        $programStats['feeVerifiedFemale'] = $this->getProgramStatsCount
                    (
                        $programId,
                        $this->nationalityOperator,
                        $this->nationality,
                        '>=', '4', $this->female
                    );

        $programStats['documentsVerifiedMale'] = $this->getProgramStatsCount
                    (
                        $programId,
                        $this->nationalityOperator,
                        $this->nationality,
                        '=', '5', $this->male
                    );

        $programStats['documentsVerifiedFemale'] = $this->getProgramStatsCount
                    (
                        $programId,
                        $this->nationalityOperator,
                        $this->nationality,
                        '=', '5', $this->female
                    );

        $programStats['documentsRejectedMale'] = $this->getProgramStatsCount
                    (
                        $programId,
                        $this->nationalityOperator,
                        $this->nationality,
                        '=', '6', $this->male
                    );

        $programStats['documentsRejectedFemale'] = $this->getProgramStatsCount
                    (
                        $programId,
                        $this->nationalityOperator,
                        $this->nationality,
                        '=', '6', $this->female
                    );

        return view('report.program', 
            [
                'programId'                  => $programId,
                'programStats'               => $programStats,
                'faculties'                  => $this->allFaculties,
                'currentUserGenderPermission'=> $this->currentUserGenderPermission,
                'bothMaleAndFemale'          => $this->bothMaleAndFemale,
                'male'                       => $this->male,
                'female'                     => $this->female,
                'nationality'                => $this->nationality,
                'fileName'                   => $programTitle,
                'nationality'                => $this->nationality,
                'nationalityOperator'        => $this->nationalityOperator,
                'departments'                => $departments, 
                'selectedDepartment'         => $selectedDepartment,
                'programs'                   =>  $programs, 
                'selectedProgram'            => $selectedProgram,
                'departmentPermissions'      => $this->departmentPermissions,
                'semesters'                  => $semesters,
                'selectedSemester'           => $selectedSemester
            ]
        );
    }

    public function getProgramMaleAndFemaleCount($programId, $nationalityOperator, $nationality, $gender)
    {
        return Application::with(['applicant'])->where('fkProgramId', $programId)->where('fkSemesterId', $this->selectedSemester)->whereHas('applicant', 
            function( $query) use ($nationalityOperator, $nationality, $gender) {
                $query->where('fkGenderId', $gender)->where('fkNationality', $nationalityOperator, $nationality);
            })->count();

    }

    public function getProgramStatsCount($programId, $nationalityOperator, $nationality, $operator, $status, $gender)
    {
        return Application::where('fkProgramId', $programId)->where('fkSemesterId', $this->selectedSemester)->getStatusByGender($operator, $status, $gender, $nationalityOperator, $nationality)->count();
    }

    public function generateFileName( $fileName ) {
        $currentDateTime = date('d-m-y h-i-s A');
        return $fileName.'-'.$currentDateTime.'.csv';
    } 

    /**
     * Create facluty report
     */
    public function createCsvReport()
    {
        $id = request('facultyId');
        $nationality = request('nationalityId');
        $helperWord = request('helperWord');
        $fileName = request('fileName');
        $selectedSemester = $this->selectedSemester = request('semester');
        
        $nationalityOperator = '=';
        if ( $nationality == '' || $nationality == 0 ) {
            $nationality = 1;
            $nationalityOperator = '>=';
        }
        $facultyName = $departments = Faculty::find ( $id )->title;

        $applications = Faculty::find( $id )->applications->pluck('id');
        $departments = Faculty::find ( $id )->departments;
        
        if ( $helperWord == 'all-male-applicants' ) {
            $applicationsWithMaleApplicants = Application::with(['applicant', 'program', 'pref1', 'pref2', 'pref3', 'program.programscheduler', 'applicant.previousQualification', 'applicant.detail', 'applicant.detail.district', 'applicant.address', 'getRollNumber'])->whereIn('id', $applications)->where('fkSemesterId', $this->selectedSemester)->whereHas('applicant', function( $query ) use ( $nationality, $nationalityOperator ) {
                $query->where('fkGenderId', 3)->where('fkNationality', $nationalityOperator,  $nationality);
            })->get();

            $fileName = $facultyName. '-'.$fileName;
            $fileName = $this->generateFileName ( $fileName );
            self::writeCsv( $applicationsWithMaleApplicants, $fileName );
        }

        if ( $helperWord == 'all-female-applicants' ) {
            $applicationsWithFemaleApplicants = Application::with(['applicant', 'program', 'pref1', 'pref2', 'pref3', 'program.programscheduler', 'applicant.previousQualification', 'applicant.detail', 'applicant.detail.district','applicant.address', 'getRollNumber'])->whereIn('id', $applications)->where('fkSemesterId', $this->selectedSemester)->whereHas('applicant', function( $query) use ( $nationality, $nationalityOperator ) {
                $query->where('fkGenderId', 2)->where('fkNationality', $nationalityOperator, $nationality);
            })->get();
            $fileName = $facultyName. '-'.$fileName;
            $fileName = $this->generateFileName ( $fileName );
            self::writeCsv( $applicationsWithFemaleApplicants, $fileName );
        }

        if ( $helperWord == 'fee-pending-male' ) {
            $feePendingMale = Application::with(['applicant', 'program','pref1', 'pref2', 'pref3', 'program.programscheduler', 'applicant.previousQualification', 'applicant.detail', 'applicant.detail.district','applicant.address', 'getRollNumber'])->whereIn('id', $applications)->where('fkSemesterId', $this->selectedSemester)->getStatusByGender('=', '3', '3', $nationalityOperator, $nationality);
            $fileName = $facultyName. '-'.$fileName;
            $fileName = $this->generateFileName ( $fileName );
            self::writeCsv( $feePendingMale, $fileName );
        }

        if ( $helperWord == 'fee-pending-female' ) {
            $feePendingFemale = Application::with(['applicant', 'program','pref1', 'pref2', 'pref3', 'program.programscheduler', 'applicant.previousQualification', 'applicant.detail', 'applicant.detail.district','applicant.address', 'getRollNumber'])->whereIn('id', $applications)->where('fkSemesterId', $this->selectedSemester)->getStatusByGender('=', '3', '2', $nationalityOperator, $nationality);
            $fileName = $facultyName. '-'.$fileName;
            $fileName = $this->generateFileName ( $fileName );
            self::writeCsv( $feePendingFemale, $fileName );
        }

        if ( $helperWord == 'fee-confirmed-male' ) {
            $feeConfirmedMale = Application::with(['applicant', 'program', 'pref1', 'pref2', 'pref3','program.programscheduler', 'applicant.previousQualification', 'applicant.detail', 'applicant.detail.district','applicant.address', 'getRollNumber'])->whereIn('id', $applications)->where('fkSemesterId', $this->selectedSemester)->getStatusByGender('>=', '4', '3',$nationalityOperator, $nationality);
            $fileName = $facultyName. '-'.$fileName;
            $fileName = $this->generateFileName ( $fileName );
            self::writeCsv( $feeConfirmedMale, $fileName );
        }

        if ( $helperWord == 'fee-confirmed-female' ) {
            $feeConfirmedFemale = Application::with(['applicant', 'program','pref1', 'pref2', 'pref3', 'program.programscheduler','applicant.previousQualification', 'applicant.detail', 'applicant.detail.district', 'applicant.address', 'getRollNumber'])->whereIn('id', $applications)->where('fkSemesterId', $this->selectedSemester)->getStatusByGender('>=', '4', '2', $nationalityOperator, $nationality);
            $fileName = $facultyName. '-'.$fileName;
            $fileName = $this->generateFileName ( $fileName );
            self::writeCsv( $feeConfirmedFemale, $fileName );
        }

        if ( $helperWord == 'docs-verified-male' ) {
            $docsVerifiedMale = Application::with(['applicant', 'program','pref1', 'pref2', 'pref3','program.programscheduler', 'applicant.previousQualification', 'applicant.detail', 'applicant.detail.district', 'applicant.address', 'getRollNumber'])->whereIn('id', $applications)->where('fkSemesterId', $this->selectedSemester)->getStatusByGender('=', '5', '3', $nationalityOperator, $nationality);
            $fileName = $facultyName. '-'.$fileName;
            $fileName = $this->generateFileName ( $fileName );
            self::writeCsv( $docsVerifiedMale, $fileName );
        }

        if ( $helperWord == 'docs-verified-female' ) {
            $docsVerifiedFemale = Application::with(['applicant', 'program','pref1', 'pref2', 'pref3','program.programscheduler', 'applicant.previousQualification', 'applicant.detail', 'applicant.detail.district', 'applicant.address', 'getRollNumber'])->whereIn('id', $applications)->where('fkSemesterId', $this->selectedSemester)->getStatusByGender('=', '5', '2', $nationalityOperator, $nationality);
            $fileName = $facultyName. '-'.$fileName;
            $fileName = $this->generateFileName ( $fileName );
            self::writeCsv( $docsVerifiedFemale, $fileName );
        }

        if ( $helperWord == 'rejected-male' ) {
            $rejectedMale = Application::with(['applicant', 'program','pref1', 'pref2', 'pref3','program.programscheduler', 'applicant.previousQualification', 'applicant.detail', 'applicant.detail.district', 'applicant.address', 'getRollNumber'])->whereIn('id', $applications)->where('fkSemesterId', $this->selectedSemester)->getStatusByGender('=', '6', '3', $nationalityOperator, $nationality);
            $fileName = $facultyName. '-'.$fileName;
            $fileName = $this->generateFileName ( $fileName );
            self::writeCsv( $rejectedMale, $fileName );
        }

        if ( $helperWord == 'rejected-female' ) {
            $rejectedFemale = Application::with(['applicant', 'program','pref1', 'pref2', 'pref3','program.programscheduler', 'applicant.previousQualification', 'applicant.detail', 'applicant.detail.district', 'applicant.address', 'getRollNumber'])->whereIn('id', $applications)->where('fkSemesterId', $this->selectedSemester)->getStatusByGender('=', '6', '2', $nationalityOperator, $nationality);
            $fileName = $facultyName. '-'.$fileName;
            $fileName = $this->generateFileName ( $fileName );
            self::writeCsv( $rejectedFemale, $fileName );
        }
    }

    /**
     * Create Report for programs
     * all-male, all-female
     */
    public function createCsvReportForPrograms()
    {
        $programId = request('programId');
        $nationalityOperator = request('operator');
        $nationality = request('nationality');
        $helper = request('helper');
        $fileName = request('fileName');
        $selectedSemester = $this->selectedSemester = request('semester');

        if ( $helper == 'all-male-applicants' ) {
            $applicationsWithMaleApplicants = Application::with(['applicant', 'program','pref1', 'pref2', 'pref3','program.programscheduler', 'applicant.previousQualification', 'applicant.detail', 'applicant.detail.district', 'applicant.address', 'getRollNumber'])->where('fkProgramId', $programId)->where('fkSemesterId', $this->selectedSemester)->whereHas('applicant', function( $query) use ($nationalityOperator, $nationality) {
                $query->where('fkGenderId', 3)->where('fkNationality', $nationalityOperator, $nationality);
            })->get();
            $fileName = $this->generateFileName( $fileName );
            self::writeCsv( $applicationsWithMaleApplicants, $fileName );
        }


        if ( $helper == 'all-female-applicants' ) {
                $applicationsWithFemaleApplicants = Application::with(['applicant', 'program','pref1', 'pref2', 'pref3','program.programscheduler', 'applicant.previousQualification', 'applicant.detail', 'applicant.detail.district', 'getRollNumber'])->where('fkProgramId', $programId)->where('fkSemesterId', $this->selectedSemester)->whereHas('applicant', function( $query) use ($nationalityOperator, $nationality) {
                $query->where('fkGenderId', 2)->where('fkNationality', $nationalityOperator, $nationality);
            })->get();
            
            $fileName = $this->generateFileName( $fileName );
            self::writeCsv( $applicationsWithFemaleApplicants, $fileName );
        }
    }


    /**
     * Create Report for departments
     * all-male, all-female
     */
    public function createCsvReportForDepartments()
    {
        $applicationIdsInDepartment = request('applicationsIds');
        $nationalityOperator = request('operator');
        $nationality = request('nationality');
        $helper = request('helper');
        $fileName = request('fileName');
        $selectedSemester = $this->selectedSemester = request('semester');

        $applicationIdsInDepartment = explode(',', $applicationIdsInDepartment );

        if ( $helper == 'all-male-applicants' ) {
            $applicationsWithMaleApplicants = Application::with(['applicant', 'program','pref1', 'pref2', 'pref3','program.programscheduler', 'applicant.previousQualification', 'applicant.detail', 'applicant.detail.district', 'applicant.address', 'getRollNumber'])->whereIn('id', $applicationIdsInDepartment)->where('fkSemesterId', $this->selectedSemester)->whereHas('applicant', function( $query) use ($nationalityOperator, $nationality) {
                $query->where('fkGenderId', 3)->where('fkNationality', $nationalityOperator, $nationality);
            })->get();
            $fileName = $this->generateFileName( $fileName );
            self::writeCsv( $applicationsWithMaleApplicants, $fileName );
        }


        if ( $helper == 'all-female-applicants' ) {
            $applicationsWithFemaleApplicants = Application::with(['applicant', 'program','pref1', 'pref2', 'pref3','program.programscheduler', 'applicant.previousQualification', 'applicant.detail', 'applicant.detail.district', 'getRollNumber'])->whereIn('id', $applicationIdsInDepartment)->where('fkSemesterId', $this->selectedSemester)->whereHas('applicant', function( $query) use ($nationalityOperator, $nationality) {
                $query->where('fkGenderId', 2)->where('fkNationality', $nationalityOperator, $nationality);
            })->get();
            $fileName = $this->generateFileName( $fileName );
            self::writeCsv( $applicationsWithFemaleApplicants, $fileName );
        }
    }

    /**
     * Create Report for program
     * Fee Pending, Fee Verified, Docuemnt Verified, Documents Rejected
     */
    public function createCsvReportForProgramsGeneral()
    {
        $programId = request('programId');
        $operator = request('operator');
        $status = request('status');
        $gender = request('gender');
        $nationalityOperator = request('nationalityOperator');
        $nationality = request('nationality');
        $fileName = request('fileName');
        $selectedSemester = $this->selectedSemester = request('semester');
        // dd($operator, $status, $gender, $nationalityOperator, $nationality);
        $result = Application::with(['applicant', 'program','pref1', 'pref2', 'pref3','program.programscheduler', 'applicant.previousQualification', 'applicant.detail', 'applicant.detail.district', 'applicant.address', 'getRollNumber'])->where('fkProgramId', $programId)->orderBy('pqm', 'DESC')->where('fkSemesterId', $this->selectedSemester)->getStatusByGender($operator, $status, $gender, $nationalityOperator, $nationality);
        $fileName = $this->generateFileName ( $fileName );
        self::writeCsv($result, $fileName);
    }

    /**
     * Create Report for department
     * Fee Pending, Fee Verified, Docuemnt Verified, Documents Rejected
     */
    public function createCsvReportForDepartmentsGeneral()
    {
        $applicationIds  = request('applicationIds');
        $operator = request('operator');
        $status = request('status');
        $gender = request('gender');
        $nationalityOperator = request('nationalityOperator');
        $nationality = request('nationality');
        $fileName = request('fileName');
        $selectedSemester = $this->selectedSemester = request('semester');


        $applicationIdsInDepartment = explode(',', $applicationIds );
        $result = Application::with(['applicant', 'program','pref1', 'pref2', 'pref3','program.programscheduler', 'applicant.previousQualification', 'applicant.detail', 'applicant.detail.district', 'applicant.address', 'getRollNumber'])->whereIn('id', $applicationIdsInDepartment)->where('fkSemesterId', $this->selectedSemester)->getStatusByGender($operator, $status, $gender, $nationalityOperator, $nationality);
        $fileName = $this->generateFileName ( $fileName );
        self::writeCsv($result, $fileName);
    }
    // Dev Mannan: Quota list generation
    public function QuotaCsv(){
        $fileName = 'Quota';
// Male = 3, Female = 2
        $res = ResultList::with('result')->whereHas('result', function($query){
            $query->where('fkSemesterId', 13);
        })->select('rollno')->get()->pluck('rollno')->toArray();
        // dd($res);
        $result = Application::with(
            [
                'applicant', 
                'program',
                'pref1', 
                'pref2', 
                'pref3',
                'program.programscheduler', 
                'applicant.previousQualification', 
                'applicant.detail', 
                'applicant.detail.district', 
                'applicant.address', 
                'getRollNumber'
                ]
            )->whereHas('applicant.detail', function($query) {
                $query->whereIn('fkDomicile', [1, 2, 7, 8, 9]);
            })->whereHas('getRollNumber', function($query) use ($res) {
                $query->whereNotIn('id', $res);
            })->where('fkFacId', 1)
            ->orderBy('pqm', 'DESC')
            ->where('fkSemesterId', 13)
            ->getStatusByGender('=', '5', '3', '>=');
        $fileName = $this->generateFileName ( $fileName );
        self::writeCsv($result, $fileName);
    }

    /**
     * Write CSV file.
     */
    public static function writeCsv( $applications, $fileName='export.csv' )
    {
        $csv = Writer::createFromFileObject(new SplTempFileObject());
        $csv->insertOne(
            [
                'S.No', 
                'Form No', 
                'Roll No',
                'Name',
                'Program Applied',
                'CNIC',
                'Nationality',
                'Father Name',
                'Present Address',
                'Permanent Address',
                'Program Name',
                'Preference 1',
                'Preference 2',
                'Preference 3',
                'Mobile No',
                'Email',
                'District',
                'Province',
                //Law Test Score
                'Entry Test Centers',
                'Total Marks',
                'Obtained Marks',
				'Willingness',
				//Law Test Score
                'Law Test Total Marks',
                'Law Test Obtained Marks',
                'Law Test Completion Date',
                //ssc
                'Degree Title',
                'Subjects',
                'Board',
                'Total Marks',
                'Obtained Marks',
                // hssc
                'Degree Title',
                'Subjects',
                'Board',
                'Total Marks Composite',
                'Obtained Marks Composite',
                'Total Marks Part 1',
                'Obtained Marks Part 1',
                'Total Marks Part 2',
                'Obtained Marks Part 2',
                //ba_bsc
                'Degree Title',
                'Subjects',
                'Specialization',
                'University',
                'Total Marks / CGPA',
                'Obtained Marks / GPA',
                //msc
                'Degree Title',
                'Subjects',
                'Specialization',
                'University',
                'Total Marks / CGPA',
                'Obtained Marks / GPA',
                //bs
                'Degree Title',
                'Subjects',
                'Specialization',
                'University',
                'Exam System',
                'Total Marks / CGPA',
                'Obtained Marks / GPA',
                //ms
                'Degree Title',
                'Subjects',
                'Specialization',
                'University',
                'Exam System',
                'Total CGPA',
                'Obtained GPA',
                'Result Awaiting',
                'Fee Verified',
                'Documents Verified',
                'Documents Verification Date',
                'Documents Rejected',
                'MS Usuludin Specialization',
                'PHD Usuludin Specialization',
                'PQM',
                //Fields for BS Civil Engineering
                'Total Marks Chemistry',
                'Obtained Marks Chemistry',
                'Total Marks Physics',
                'Obtained Marks Physics',
                'Total Marks Maths',
                'Obtained Marks Maths',
                //Fields for BS EE, CE & ME
                'SSC Total Marks Chemistry',
                'SSC Obtained Marks Chemistry',
                'SSC Total Marks Physics',
                'SSC Obtained Marks Physics',
                'SSC Total Marks Maths',
                'SSC Obtained Marks Maths',
                //Fields for Preferences
                'preference_1',
                'preference_2',
                'preference_3',
                'preference_4',
                'preference_5',
                // Fields For GAT/GRE Score
                'GAT/GRE Test Type',
                'GAT/GRE Total Marks',
                'GAT/GRE Obtained Marks',
                'GAT/GRE Completion Date',
                // New applications date and status field for admission launch again
                'New Application Date',
                'Application Status'
            ]
        );
        $serialNumber = 1;  
        foreach ( $applications as $application ) {
            $feeVerifiedStatus      = ( $application->fkCurrentStatus >= 4 )   ? 'Yes' : 'No';
            $documentVerifiedStatus = ( $application->fkCurrentStatus >= 5 ) ? 'Yes' : 'No';
            $documentRejectedStatus = ( $application->fkCurrentStatus == 6 ) ? 'Yes' : 'No';

            //PQM Vars
            // $level = $application->program->fkLeveliId;
            // $preReq = $application->program->fkReqId;
            $sscTotal = $application->applicant->previousQualification->SSC_Total;
            $sscObtained = $application->applicant->previousQualification->SSC_Composite;

            $hsscTotal = $application->applicant->previousQualification->HSSC_Total;
            $hsscObtained = $application->applicant->previousQualification->HSSC_Composite;
            $hsscTotalPart1 = $application->applicant->previousQualification->HSSC_Total1;
            $hsscTotalPart2 = $application->applicant->previousQualification->HSSC_Total2;
            $hsscObtainedPart1 = $application->applicant->previousQualification->HSSC1;
            $hsscObtainedPart2 = $application->applicant->previousQualification->HSSC2;

            $baBscTotal = $application->applicant->previousQualification->BA_Bsc_Total;
            $baBscObtained = $application->applicant->previousQualification->BA_Bsc;
            
            $mscTotal = $application->applicant->previousQualification->MSc_Total;
            $mscObtained = $application->applicant->previousQualification->MSc;
            $bsTotal = $application->applicant->previousQualification->BS_Total;
            $bsObtained = $application->applicant->previousQualification->BS;

            $msTotal = $application->applicant->previousQualification->MS_Total;
            $msObtained = $application->applicant->previousQualification->MS;

            // $interviewDateTime = $application->program->programscheduler->interviewDateTime;
            // $pqm = Self::calculatePqm(
            //     $application->id, $level, $interviewDateTime, $preReq, $sscTotal, $sscObtained, $hsscTotal, $hsscObtained,$hsscTotalPart1, $hsscTotalPart2, $hsscObtainedPart1, $hsscObtainedPart2, $baBscTotal, $baBscObtained, $mscTotal, $mscObtained, $bsTotal, $bsObtained, $msTotal, $msObtained
            // );
            $csv->insertOne(
                [
                    $serialNumber ?? '',
                    $application->id ?? '',
                    $application->getRollNumber->id ?? '',
                    $application->applicant->name ?? '',
                    $application->program->title ?? '',
                    $application->applicant->cnic ?? '',
                    $application->applicant->nationality->nationality ?? '',
                    $application->applicant->detail->fatherName ?? '',
                    $application->applicant->address->addressPo ?? '',
                    $application->applicant->address->addressPmt ?? '',
                    $application->program->title ?? '',
                    $application->pref1->title ?? '',
                    $application->pref2->title ?? '',
                    $application->pref3->title ?? '',
                    $application->applicant->detail->mobile ?? '',
                    $application->applicant->email ?? '',
                    $application->applicant->detail->district->distName ?? '', //to be added
                    $application->applicant->detail->province->provName ?? '', //to be added
                    
					// Engineering test score
                    $application->applicant->previousQualification->entry_test_center ?? '',
                    $application->applicant->previousQualification->entry_test_total_marks  ?? '',
                    $application->applicant->previousQualification->entry_test_obtained_marks  ?? '',
					$application->applicant->previousQualification->iiu_test_appear_willingness ?? '',
				   
				   
				    // law test score
                    $application->applicant->previousQualification->llb_total_marks ?? '',
                    $application->applicant->previousQualification->llb_obtained_marks ?? '',
                    $application->applicant->previousQualification->law_completion_date ?? '',

                    //ssc
                    $application->applicant->previousQualification->SSC_Title ?? '', // for O-Level
                    $application->applicant->previousQualification->SSC_Subject ?? '',
                    $application->applicant->previousQualification->SSC_From ?? '',
                    $sscTotal ?? '',
                    $sscObtained ?? '',

                    //hssc
                    $application->applicant->previousQualification->HSC_Title ?? '', // For A-Level
                    $application->applicant->previousQualification->HSC_Subject ?? '',
                    $application->applicant->previousQualification->HSC_From ?? '',
                    $hsscTotal ?? '',
                    $hsscObtained ?? '',
                    $hsscTotalPart1 ?? '',
                    $hsscObtainedPart1 ?? '',
                    $hsscTotalPart2 ?? '',
                    $hsscObtainedPart2 ?? '',

                    //ba_bsc
                    $application->applicant->previousQualification->BA_Bsc_Title ?? '',
                    $application->applicant->previousQualification->BA_Bsc_Subject ?? '',
                    $application->applicant->previousQualification->BA_Bsc_Specialization ?? '',
                    $application->applicant->previousQualification->BA_Bsc_From ?? '',
                    $baBscTotal ?? '',
                    $baBscObtained ?? '',
                    // $baBscTotal = $application->applicant->previousQualification->BA_Bsc_Total;
                    // $baBscObtained = $application->applicant->previousQualification->BA_Bsc;

                    //msc
                    $application->applicant->previousQualification->MSc_Title ?? '',
                    $application->applicant->previousQualification->MSc_Subject ?? '',
                    $application->applicant->previousQualification->MSc_Specialization ?? '',
                    $application->applicant->previousQualification->MSc_From ?? '',
                    $application->applicant->previousQualification->MSc_Total ?? '',
                    $application->applicant->previousQualification->MSc ?? '',

                    //bs
                    $application->applicant->previousQualification->BS_Title ?? '',
                    $application->applicant->previousQualification->BS_Subject ?? '',
                    $application->applicant->previousQualification->BS_Specialization ?? '',
                    $application->applicant->previousQualification->BS_From ?? '',
                    $application->applicant->previousQualification->BS_Exam_System ?? '',
                    $application->applicant->previousQualification->BS_Total ?? '',
                    $application->applicant->previousQualification->BS ?? '',

                    //ms
                    $application->applicant->previousQualification->MS_Title ?? '',
                    $application->applicant->previousQualification->MS_Subject ?? '',
                    $application->applicant->previousQualification->MS_Specialization ?? '',
                    $application->applicant->previousQualification->MS_From ?? '',
                    $application->applicant->previousQualification->MS_Exam_System ?? '',
                    $application->applicant->previousQualification->MS_Total ?? '',
                    $application->applicant->previousQualification->MS ?? '',

                    ($application->applicant->previousQualification->resultAwaiting == 1) ? 'Yes' : 'No',
                    $feeVerifiedStatus ?? '',
                    $documentVerifiedStatus ?? '',
                    $application->doumentsVerified ?? '',
                    $documentRejectedStatus ?? '',
                    $application->applicant->previousQualification->ms_usuludin_specialization ?? '',
                    $application->applicant->previousQualification->phd_usuludin_specialization ?? '',
                    // $application->applicant->previousQualification->pqm ?? ''
                    (($application->applicant->previousQualification->SSC_Title == 'O-Level' || $application->applicant->previousQualification->HSC_Title == 'A-Level') && ($application->applicant->fkLevelId != 4 && $application->applicant->fkLevelId != 5))? 'PQM Needs to be calculate manually in case of O-Level Or A-Level' : $application->pqm ?? '',
                    //Fields for BS Civil Engineering
                    $application->applicant->previousQualification->chemistry_total ?? '',
                    $application->applicant->previousQualification->chemistry_obtained ?? '',
                    $application->applicant->previousQualification->physics_total ?? '',
                    $application->applicant->previousQualification->physics_obtained ?? '',
                    $application->applicant->previousQualification->maths_total ?? '',
                    $application->applicant->previousQualification->maths_obtained ?? '',
                    //Fields for BS EE, CE & ME
                    $application->applicant->previousQualification->ssc_chemistry_total ?? '',
                    $application->applicant->previousQualification->ssc_chemistry_obtained ?? '',
                    $application->applicant->previousQualification->ssc_physics_total ?? '',
                    $application->applicant->previousQualification->ssc_physics_obtained ?? '',
                    $application->applicant->previousQualification->ssc_maths_total ?? '',
                    $application->applicant->previousQualification->ssc_maths_obtained ?? '',
                    //Fields for Preferences
                    $application->applicant->applicantDetail->preference_1 ?? '',
                    $application->applicant->applicantDetail->preference_2 ?? '',
                    $application->applicant->applicantDetail->preference_3 ?? '',
                    $application->applicant->applicantDetail->preference_4 ?? '',
                    $application->applicant->applicantDetail->preference_5 ?? '',
                    // Fields For GAT/GRE Score
                    $application->applicant->previousQualification->gat_gre_test_type ?? '',
                    $application->applicant->previousQualification->gat_gre_total_marks ?? '',
                    $application->applicant->previousQualification->gat_gre_obtained_marks ?? '',
                    $application->applicant->previousQualification->gat_gre_completion_date ?? '',
                    // New applications date and status field for admission launch again
                    $application->new_application_date ?? '',
                    $application->new_application_status ?? ''
                ]
            );
            $serialNumber++;
        }
        $currentUser = User::find(auth()->id());
        $currentDate = date('d-m-Y h:i:s A');
        $message = "Report generated by $currentUser->first_name $currentUser->last_name on $currentDate via Online Admission System";
        $csv->insertOne([$message]);
        $csv->output($fileName);
        die;
    }

    
    /**
     * Calculate Previous Qualifications Marks
     */
    /*
    public static function calculatePqm (
        $id,
        $level,
        $interviewDateTime,
        $preReq=0,
        $sscTotal = 0 ,
        $sscObtained = 0,
        $hsscTotal = 0,
        $hsscObtained = 0,
        $hsscTotalPart1 = 0 ,
        $hsscTotalPart2 = 0 ,
        $hsscObtainedPart1 = 0 ,
        $hsscObtainedPart2 = 0,
        $baBscTotal = 0,
        $baBscObtained = 0,
        $mscTotal = 0,
        $mscObtained = 0 ,
        $bsTotal = 0,
        $bsObtained = 0,
        $msTotal = 0,
        $msObtained = 0 )
    {

        if ( $level == 1 || $level == 2 ) {
           //BS & Diploma
           return Self::calculatePqmForUnderGraduate ( $sscObtained,$sscTotal,$hsscObtained,$hsscTotal,$hsscObtainedPart1,$hsscTotalPart1,$hsscObtainedPart2, $hsscTotalPart2 );
        } else if ( $level == 3 ) {
            // MA,  MSc
            if ( $interviewDateTime !== '0000-00-00 00:00:00' ) {
                return Self::calculatePqmForMaMsc( $baBscObtained, $baBscTotal);
            } else {
                return Self::calculatePqmForUnderGraduate ( $sscObtained, $sscTotal, $hsscObtained, $hsscTotal, $hsscObtainedPart1, $hsscTotalPart1, $hsscObtainedPart2, $hsscTotalPart2 );
            }
        } else if ( $level == 4 ) {
            // Masters
            return Self::calculatePqmForMasters($mscObtained, $mscTotal, $bsObtained, $bsTotal);
        }
        else if ( $level == 5 ) {
            // PostGraduate
            return Self::calculatePqmForPostGraduate($msObtained, $msTotal);
        }
    }
    
    public static function calculatePqmForUnderGraduate($sscObtained, $sscTotal, $hsscObtained, $hsscTotal, $hsscObtainedPart1, $hsscTotalPart1, $hsscObtainedPart2,$hsscTotalPart2 )
    {
        $ssc15Percent = 0;
        $hssc25Percent = 0;
        if ( $sscTotal != '' || $sscTotal != 0 ) {
            $ssc15Percent = ( (float)$sscObtained/(float)$sscTotal )*15;
        }
        if ( $hsscTotal != '' || $hsscTotal != 0 ) {
            $hssc25Percent = ( (float)$hsscObtained/(float)$hsscTotal )*25;
        } else if ( $hsscTotalPart1 != '' ) {
            $hssc25Percent = (float)$hsscObtainedPart1 / ( (float)$hsscTotalPart1 )*25; 
        } else if ( $hsscTotalPart1 != '' || $hsscTotalPart2 != '' ) {
            $hssc25Percent = ( ( (float)$hsscObtainedPart1+(float)$hsscObtainedPart2 ) / ( (float)$hsscTotalPart1+(float)$hsscTotalPart2) )*25; 
        }
        $pqm = $ssc15Percent + $hssc25Percent;
        return $pqm = number_format((float)$pqm, 2, '.', '');
    }

    public static function calculatePqmForMaMsc($baBscObtained, $baBscTotal) 
    {
        if ( $baBscTotal !='' || $baBscTotal != 0 ) {
            $pqm = ( (float)$baBscObtained/(float)$baBscTotal )*30;
            return $pqm = number_format((float)$pqm, 2, '.', '');  
        }
        return 0;
    }

    public static function calculatePqmForMasters($mscObtained, $mscTotal, $bsObtained, $bsTotal)
    {
        $msc30Percent = '';
        $bs30Percent = '';
        $previousDegree30Percent = 0;
        if ( $mscTotal && $bsTotal ) {
            $bs30Percent =  ((float)$bsObtained/(float)$bsTotal)*30;
            $msc30Percent =  ((float)$mscObtained/(float)$mscTotal)*30;
            if ( $msc30Percent >= $bs30Percent ) {
                $previousDegree30Percent = $msc30Percent;
            }
            else {
                $previousDegree30Percent = $bs30Percent; 
            }
        }
        else if ( $bsTotal ) {
            $previousDegree30Percent = $bs30Percent =  ((float)$bsObtained/(float)$bsTotal)*30;   
        }
        else if ( $mscTotal ) {
            $previousDegree30Percent = $msc30Percent =  ((float)$mscObtained/(float)$mscTotal)*30;
        }

        // $pqm = $previousDegree30Percent;
        return number_format((float)$previousDegree30Percent, 2, '.', '');
    }

    public static function calculatePqmForPostGraduate($msObtained, $msTotal)
    {
        $previousDegree30Percent = 0;
        if ( $msTotal )
            $previousDegree30Percent = ((float)$msObtained / (float)$msTotal)*30;
          
        // $pqm = $previousDegree30Percent;   
        return number_format((float)$previousDegree30Percent, 2, '.', '');
    }

    */

    /**
     * Bind the report name with current date & time
     */
    public function buildReportName ( $reportName ) {
        $date = date('m-d-Y h-i-s A');
        return $reportName.' '.$date.'.csv';
    }

    /**
     * Report generation message appended to the exported file
     */
    public function reportGenerationMessage()
    {
        $currentUser = User::find(auth()->id());
        $currentDate = date('d-m-Y h:i:s A');
        return $message = "Report generated by $currentUser->first_name $currentUser->last_name on $currentDate via Online Admission System";
    }

    public function topLevelReport( $semester = '' )
    {
        $selectedSemester = $this->selectedSemester = $semester;


        $this->currentUserPermissions =  $this->getCurrentUserPermissions();
        $currentUserFacultyPermissions = explode ( ',', $this->currentUserPermissions->fkFacultyId);
        $allowedFaculties = Faculty::whereIn('pkFacId', $currentUserFacultyPermissions)->select('pkFacId', 'title', 'abbrev', 'email')->first();
// dd($allowedFaculties->pkFacId, session('gid'));
        $allowedFacultyForFacultyUser = $allowedFaculties->pkFacId;


        
        if ( $semester == '' ) 
            // $selectedSemester = $this->selectedSemester = Semester::where('status', '1')->first()->pkSemesterId;
            $selectedSemester = Cache::get(auth()->id().'-active_semester');
        $faculties = Faculty::with(['departments', 'departments.programme'])->get();
        $semesters = $this->getAllSemesters();
        return view('report.top-level-report', compact('faculties', 'semesters', 'selectedSemester', 'allowedFacultyForFacultyUser'));
    }

    public function getAllSemesters()
    {
        return Semester::all();
    }

    public function getReportFromSpecificSemester( $reportName )
    {
        return redirect()->route($reportName, ['semester' => request('semesterSelector')]);
    }

}