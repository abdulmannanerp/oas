<?php

namespace App\Http\Controllers;

//use App\Model\{Account, Department, Faculty, Option, ProgramFee, Programme, Result, ResultList, RollNumber, Semester};
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use League\Csv\Reader;
use App\Model\Result;
use App\Model\ResultList;
use App\Model\RollNumber;
use App\Model\ApplicantDetail;
use App\Jobs\SendEmail;
use App\Model\oasEmail;
use Mail;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;

class GetsmsController extends Controller
{
	public function index()
	{
		// $results = Result::whereDate('created_at', DB::raw('CURDATE()'))->where('active', 1)->get();
		$results = Result::whereDate('created_at', Carbon::now()->toDateString())->where('active', 1)->get();
		foreach ($results as $r) {
			// dump($r->pkResultsId);
			$number = $this->GetNumber($r->pkResultsId); // List ID
			Log::info('SMS has been sent to list: ' . $r->pkResultsId);
		}
		// exit;
		// $pkResultsId = "15";
		// $results = Result::whereDate('created_at', DB::raw('CURDATE()'))->get();
		// dd($results);
		// exit;
		// $number = $this->GetNumber($pkResultsId); // List ID

	}
	public function GetNumber($pkResultsId)
	{
		$body = urlencode("Dear Applicant, Result has been published. For more details please visit admission.iiu.edu.pk%0aAdmission Office");

		$totalph = 0;
		$sendph = 0;
		$results = Result::where('pkResultsId', $pkResultsId)->get();
		if (count($results) > 0) {
			foreach ($results as $result) {
				$fkresultid = $result->pkResultsId;
				$rollnos = ResultList::where('fkResultId', $fkresultid)->get();
				foreach ($rollnos as $roll) {
					$rolln = $roll->rollno;
					$applicantids = RollNumber::where('id', $rolln)->get();
					foreach ($applicantids as $applicantid) {
						$userids = $applicantid->fkApplicantId;
						$phones = ApplicantDetail::where('fkApplicantId', $userids)->get();
						//$phones = array('923128681618','923128681618','923128681618','923128681618','923338999963');
						foreach ($phones as $ph) {
							$totalph++;
							$pht = $ph->mobile;
							//$pht = $ph;
							if (strlen($pht) == 12) {
								$sendph++;
								$mob = $pht;

								$url = "http://api.bizsms.pk/api-send-branded-sms.aspx?username=iiu@bizsms.pk&pass=wjQc%EueX9&text=" . $body . "&masking=IIUI&destinationnum=" . $mob . "&language=English";

								$curl = curl_init();

								// curl_setopt_array($curl, array(
								// 	CURLOPT_URL => $url,
								// 	CURLOPT_RETURNTRANSFER => true,
								// 	CURLOPT_ENCODING => "",
								// 	CURLOPT_MAXREDIRS => 10,
								// 	CURLOPT_TIMEOUT => 300,
								// 	CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
								// 	CURLOPT_CUSTOMREQUEST => "POST",
								// 	CURLOPT_HTTPHEADER => array(
								// 		"Cache-Control: no-cache",
								// 		"Postman-Token: 0f4b3a3f-c9e2-40fc-85a4-971282bbce35",
								// 		"Content-Length: 825"
								// 	),
								// ));

								curl_setopt_array($curl, array(
									CURLOPT_URL => $url,
									CURLOPT_RETURNTRANSFER => true,
									CURLOPT_ENCODING => '',
									CURLOPT_MAXREDIRS => 10,
									CURLOPT_TIMEOUT => 0,
									CURLOPT_FOLLOWLOCATION => true,
									CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
									CURLOPT_CUSTOMREQUEST => 'POST',
									CURLOPT_HTTPHEADER => array(
										'Cookie: ASP.NET_SessionId=je1oat1w1u4mwdnh5q1iwjj4',
										'Content-Length: 0'
									),
								));

								$response = curl_exec($curl);
								$err = curl_error($curl);

								curl_close($curl);
							}  /// Number Length check validation End

						}
					} //break;
				}
			}
			//$err = '';$response = '';
			echo 'total number = ' . $totalph . '<br>' . 'send sms = ' . $sendph;
			//////////////// email function

			if (isset($totalph)) {
			}

			//////////////
			if ($err) {
				// dd ( $err );
			} else {
				// dd ( $response );
				return redirect()->back()->with('status', 'Message sent with total no of phone' . $totalph . ' and send number of applicant =' . $sendph);
			}
		} else {
			$count = 0;
			//  dd ( 'Provided List no is not valid = '.$count );

		}
	}



	public function sendEmail($ApplicantDetail)
	{
		$data = [
			'name' => $ApplicantDetail->applicant->email
		];
		SendEmail::dispatch(
			$ApplicantDetail->applicant->application->faculty->abbrev,
			$data,
			$ApplicantDetail->applicant->email,
			'Result List Uploaded at IIU',
			'applications.resultUpload'
		);
		return;
	}
}
//$phone = array('923325234119','923125235653','923335507643','923338999963');