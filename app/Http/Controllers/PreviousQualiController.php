<?php

namespace App\Http\Controllers;

use App\Traits\PQM;
use App\Model\Application;
use App\Model\PreviousQualification;

class PreviousQualiController extends Controller{
	use PQM;

    public function index(){
		$application = Application::with(['program','program.programscheduler','applicant','applicant.previousQualification'])->whereIn('fkCurrentStatus', array('5','6'))->get();
		 // PQM Variables
		 foreach($application as $application){
		 $level = $application->program->fkLeveliId;
		//  echo $level; exit;
		 $interviewDateTime = $application->program->programscheduler->interviewDateTime;
		 $preReq = $application->program->fkReqId;
		 $sscTotal = $application->applicant->previousQualification->SSC_Total;
		 $sscObtained = $application->applicant->previousQualification->SSC_Composite;

		 $hsscTotal = $application->applicant->previousQualification->HSSC_Total;
		 $hsscObtained = $application->applicant->previousQualification->HSSC_Composite;
		 $hsscTotalPart1 = $application->applicant->previousQualification->HSSC_Total1;
		 $hsscTotalPart2 = $application->applicant->previousQualification->HSSC_Total2;
		 $hsscObtainedPart1 = $application->applicant->previousQualification->HSSC1;
		 $hsscObtainedPart2 = $application->applicant->previousQualification->HSSC2;

		 $baBscTotal = $application->applicant->previousQualification->BA_Bsc_Total;
		 $baBscObtained = $application->applicant->previousQualification->BA_Bsc;
		 
		 $mscTotal = $application->applicant->previousQualification->MSc_Total;
		 $mscObtained = $application->applicant->previousQualification->MSc;
		 $bsTotal = $application->applicant->previousQualification->BS_Total;
		 $bsObtained = $application->applicant->previousQualification->BS;

		 $msTotal = $application->applicant->previousQualification->MS_Total;
		 $msObtained = $application->applicant->previousQualification->MS;

		 $pqm = $this->calculatePqm(
			$application->id, $level, $interviewDateTime, $preReq, $sscTotal, $sscObtained, $hsscTotal, $hsscObtained,$hsscTotalPart1, 
			$hsscTotalPart2, $hsscObtainedPart1, $hsscObtainedPart2, $baBscTotal, $baBscObtained, $mscTotal, $mscObtained, 
			$bsTotal, $bsObtained, $msTotal, $msObtained
		);
		 PreviousQualification::where('fkApplicantId', $application->fkApplicantId)->update(
			[
				'pqm' => $pqm
			]
		);
	}
		// dd($application);
	}
	  
}
