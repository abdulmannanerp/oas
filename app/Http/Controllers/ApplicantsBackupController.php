<?php

namespace App\Http\Controllers;

use App\Model\Applicant;
use App\Model\ApplicantDetail;
use App\Model\Application;
use App\Backup;
use App\Model\Permission;
use App\Model\Programme;
use App\Model\RollNumber;
use App\Model\oasEmail;
use Carbon\Carbon;
use Illuminate\Http\Request;

class ApplicantsBackupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
	 
	 public function calculatePqm( $id, $level, $preReq=0, $sscTotal = 0 , $sscObtained = 0, $hsscTotal = 0, $hsscObtained = 0,$hsscTotalPart1 = 0 , $hsscTotalPart2 = 0 , $hsscObtainedPart1 = 0 , $hsscObtainedPart2 = 0, $baBscTotal = 0, $baBscObtained = 0, $mscTotal = 0, $mscObtained = 0 , $bsTotal = 0, $bsObtained = 0, $msTotal = 0, $msObtained = 0 )
    {
        $pqm = '';
        if ( $level == 1 ) {
            //undergraduate
            if ( $preReq == 2 ) {
                $pqm = 0;
                if ( $baBscTotal !='' || $baBscTotal != 0 ) {
                    $pqm = ( (float)$baBscObtained/(float)$baBscTotal )*40;
                    $pqm = number_format((float)$pqm, 2, '.', '');  
                }
                if ( $pqm == 0 )
                    $pqm = 20;
            } else {
                $ssc15Percent = 0;
                 $hssc25Percent = 0;
                if ( $sscTotal != '' || $sscTotal != 0 ) {
                    $ssc15Percent = ( (float)$sscObtained/(float)$sscTotal )*15;
                }
                if ( $hsscTotal !== '' ) {
                    if ( $hsscTotal != '' || $hsscTotal != 0 )
                        $hssc25Percent = ( (float)$hsscObtained/(float)$hsscTotal )*25;
                } else if ( $hsscTotalPart1 !== '' || $hsscTotalPart2 !== '' ) {
                    $hssc25Percent = ( ( (float)$hsscObtainedPart1+(float)$hsscObtainedPart2 ) / ( (float)$hsscTotalPart1+(float)$hsscTotalPart2) )*25; 
                }
                $pqm = $ssc15Percent + $hssc25Percent;
                $pqm = number_format((float)$pqm, 2, '.', '');
            }
        } else if ( $level == 2 ) {
            //graduate
            $msc30Percent = '';
            $bs30Percent = '';
            $previousDegree30Percent = '';
            if ( $mscTotal && $bsTotal ) {
                $bs30Percent =  ((float)$bsObtained/(float)$bsTotal)*30;
                $msc30Percent =  ((float)$mscObtained/(float)$mscTotal)*30;
                if ( $msc30Percent >= $bs30Percent ) {
                    $previousDegree30Percent = $msc30Percent;
                }
                else {
                    $previousDegree30Percent = $bs30Percent; 
                }
            }
            else if ( $bsTotal ) {
                $previousDegree30Percent = $bs30Percent =  ((float)$bsObtained/(float)$bsTotal)*30;   
            }
            else if ( $mscTotal ) {
                $previousDegree30Percent = $msc30Percent =  ((float)$mscObtained/(float)$mscTotal)*30;
            }
            else {
                $previousDegree30Percent = 0;
            }
            $pqm = $previousDegree30Percent;
            $pqm = number_format((float)$pqm, 2, '.', '');
        }
        else if ( $level == 3 ) {
            // post graduate
            if ( $msTotal )
                $previousDegree30Percent = ((float)$msObtained / (float)$msTotal)*30;
            else
                $previousDegree30Percent = 0;

            $pqm = $previousDegree30Percent;
            $pqm = number_format((float)$pqm, 2, '.', '');
        }
        return $pqm;
    }
	 
    public function index()
    {
		$totalrecord = Application::orderBy('id', 'desc')->first();
		
		$lastid = Backup::orderBy('id', 'desc')->first();
		
		$backuprecord = Backup::count();
		$applicationrecord = Application::count();
		
		if(isset($lastid->id) && $lastid->id >= $totalrecord->id)
		{
			dd('All Records Copy Succesfully.');
		}
		
		if(isset($backuprecord) && $backuprecord > 0)
		{
		 
		  $skip = $lastid->id;
		  $limit = 20000;
		}
		else
		{
			$limit = 20000;
			$skip = 0; 
		}
		$backuprecord = Backup::count();
	//dd('Currently Records Inserted: ',$backuprecord);exit();
		$applicants = Application::with(['applicant', 'program', 'program.department', 'program.faculty', 'applicant.previousQualification','applicant.gender', 'applicant.detail', 'applicant.detail.district','applicant.detail.province', 'getRollNumber'])->skip($skip)->take($limit)->get();
		 
		 
		//dump($application->id);
		
		foreach($applicants as $application)
		 {
			if($application->fkCurrentStatus > 1) 
				{
			if ( !$application->program ||  $application->applicant->previousQualification->SSC_Total == 0 ||  $application->applicant->previousQualification->SSC_Composite == 0 ) {
				$pqm = 0;
			} 
			
			else
			
			{
			       //PQM Vars
            $level = $application->program->fkLeveliId;
            $preReq = $application->program->fkReqId;
            $sscTotal = $application->applicant->previousQualification->SSC_Total;
            $sscObtained = $application->applicant->previousQualification->SSC_Composite;

            $hsscTotal = $application->applicant->previousQualification->HSSC_Total;
            $hsscObtained = $application->applicant->previousQualification->HSSC_Composite;
            $hsscTotalPart1 = $application->applicant->previousQualification->HSSC_Total1;
            $hsscTotalPart2 = $application->applicant->previousQualification->HSSC_Total2;
            $hsscObtainedPart1 = $application->applicant->previousQualification->HSSC1;
            $hsscObtainedPart2 = $application->applicant->previousQualification->HSSC2;

            $baBscTotal = $application->applicant->previousQualification->BA_Bsc_Total;
            $baBscObtained = $application->applicant->previousQualification->BA_Bsc;
            
            $mscTotal = $application->applicant->previousQualification->MSc_Total;
            $mscObtained = $application->applicant->previousQualification->MSc;
            $bsTotal = $application->applicant->previousQualification->BS_Total;
            $bsObtained = $application->applicant->previousQualification->BS;

            $msTotal = $application->applicant->previousQualification->MS_Total;
            $msObtained = $application->applicant->previousQualification->MS;
			
			
			
			$pqm = $this->calculatePqm(
                $application->id, $level, $preReq, $sscTotal, $sscObtained, $hsscTotal, $hsscObtained,$hsscTotalPart1, $hsscTotalPart2, $hsscObtainedPart1, $hsscObtainedPart2, $baBscTotal, $baBscObtained, $mscTotal, $mscObtained, $bsTotal, $bsObtained, $msTotal, $msObtained
            );
			
	}
			
		
					Backup::create([
					
                    'formno' => $application->id ?? '',
                    'rollno' => $application->getRollNumber->id ?? '',
                    'name' => $application->applicant->name ?? '',
                    'fathername' => $application->applicant->detail->fatherName ?? '',
                    'programe' => $application->program->title ?? '',
                    'department' => $application->program->department->title ?? '',
                    'faculty' => $application->program->faculty->title ?? '',
                    'mobile' => $application->applicant->detail->mobile ?? '',
                    'email' => $application->applicant->email ?? '',
                    'district' => $application->applicant->detail->district->distName ?? '', //to be added
                    'province' => $application->applicant->detail->province->provName ?? '',
					'cnic' => $application->applicant->cnic ?? '',
					'gender' => $application->applicant->gender->gender ?? '',
					
					//ssc
                    'degreetitlessc' => $application->applicant->previousQualification->SSC_Title ?? '', //to be added
                    'subjectsssc' => $application->applicant->previousQualification->SSC_Subject ?? '',
                    'boardssc' => $application->applicant->previousQualification->SSC_From ?? '',
                    'totalmarkssc' => $application->applicant->previousQualification->SSC_Total ?? '',
                    'SSC1' => $application->applicant->previousQualification->SSC1 ?? '',
					'SSC2' => $application->applicant->previousQualification->SSC2 ?? '',
					'SSC_Composite' => $application->applicant->previousQualification->SSC_Composite ?? '',
					 
                    //hssc1
                    'degreetitlehssc1' => $application->applicant->previousQualification->HSC_Title ?? '',
                    'subjecthssc1' => $application->applicant->previousQualification->HSC_Subject ?? '',
                    'boardhssc1' => $application->applicant->previousQualification->HSC_From ?? '',
                    'totalmarkhssc1' => $application->applicant->previousQualification->HSSC_Total1 ?? '',
                    'obtainedhssc1' => $application->applicant->previousQualification->HSSC1 ?? '',
                    
					//hss2
				    'degreetitlehssc2' => $application->applicant->previousQualification->HSC_Title ?? '',
                    'subjecthssc2' => $application->applicant->previousQualification->HSC_Subject ?? '',
                    'boardhssc2'  => $application->applicant->previousQualification->HSC_From ?? '',
                    'totalmarkhssc2' => $application->applicant->previousQualification->HSSC_Total2 ?? '',
					'obtainedhssc2' => $application->applicant->previousQualification->HSSC2 ?? '',
					
					//composite marks
					'HSSC_Composite' => $application->applicant->previousQualification->HSSC_Composite ?? '',
					'HSSC_Total' => $application->applicant->previousQualification->HSSC_Total ?? '',
					
                    //ba_bsc
                    'degreetitlebsc' => $application->applicant->previousQualification->BA_Bsc_Title ?? '',
                    'subjectbsc' => $application->applicant->previousQualification->BA_Bsc_Subject ?? '',
                    'Bsc_Specialization' => $application->applicant->previousQualification->BA_Bsc_Specialization ?? '',
                    'boardbsc' => $application->applicant->previousQualification->BA_Bsc_From ?? '',
                    'totalmarkbsc' => $application->applicant->previousQualification->BA_Bsc_Total ?? '',
                    'obtainedbsc' => $application->applicant->previousQualification->BA_Bsc ?? '',
					'bscexamsystem' => $application->applicant->previousQualification->BSc_Exam_System ?? '',
                    

                    //msc
                    'degreetitlemsc' => $application->applicant->previousQualification->MSc_Title ?? '',
                    'subjectmsc' => $application->applicant->previousQualification->MSc_Subject ?? '',
                    'mscspecialization' => $application->applicant->previousQualification->MSc_Specialization ?? '',
                    'boardmsc' => $application->applicant->previousQualification->MSc_From ?? '',
                    'totalmarkmsc' => $application->applicant->previousQualification->MSc_Total ?? '',
                    'obtainedmsc' => $application->applicant->previousQualification->MSc ?? '',
					'mscexamsystem' => $application->applicant->previousQualification->Msc_Exam_System ?? '',
					
                    //bs
                    'degreetitlebs' => $application->applicant->previousQualification->BS_Title ?? '',
                    'subjectbs' => $application->applicant->previousQualification->BS_Subject ?? '',
                    'bsspecialization' => $application->applicant->previousQualification->BS_Specialization ?? '',
                    'boardbs' => $application->applicant->previousQualification->BS_From ?? '',
                    'bsexamsystem' =>  $application->applicant->previousQualification->BS_Exam_System ?? '',
                    'totalmarkbs' => $application->applicant->previousQualification->BS_Total ?? '',
                    'obtainedbs' => $application->applicant->previousQualification->BS ?? '',

                    //ms
                    'degreetitlems' => $application->applicant->previousQualification->MS_Title ?? '',
                    'subjectms' => $application->applicant->previousQualification->MS_Subject ?? '',
                    'msspecialization' => $application->applicant->previousQualification->MS_Specialization ?? '',
                    'boardms' => $application->applicant->previousQualification->MS_From ?? '',
                    'msexamsystem' => $application->applicant->previousQualification->MS_Exam_System ?? '',
                    'totalmarkms' => $application->applicant->previousQualification->MS_Total ?? '',
                    'obtainedms' => $application->applicant->previousQualification->MS ?? '',
					
                    'resultwaithssc' => $application->applicant->previousQualification->resultAwaiting ?? '',
					 'FeeVerifiedBy' => $application->fkFeeVerifiedBy?? '',
					 
                    'feeverfied' => ($application->fkCurrentStatus == 4) ? 'Verified' : 'Not Verfied',
                    'documentverfied' => ($application->fkCurrentStatus == 5) ? 'Approved' : 'Rejected',
                    'pqm' => $pqm ?? '0'
					]);
					
					if(isset($lastid->id))
					{
						$lastid = Backup::orderBy('id', 'desc')->first();
						if($lastid->id >= $totalrecord->id)
						{
							dd('Record Inserted Succesfully');
						}
					}
	    	 }
					
		 }
		 if(isset($lastid->id))
		 {
			$precord = $lastid->id+20000;
			 
			 if($precord == $totalrecord->id)
			 {
				 dump('All Records Copied Succesfully. please do not refresh page');
				 dump('Currently Records Inserted: ',$precord);
				 dump('Total Number of Record which to be copy: ',$totalrecord->id);
			 }
			 else
			 {
				 dump('Refresh page to copy next 20000 Records');
				 dump('Currently Records Inserted: ',$precord);
				 dump('Total Number of Record which to be copy: ',$totalrecord->id);
			 }
		 }
		 else
		 {
			$lastid = 0;
			
			$precord = $lastid+20000;
			 
			 if($precord == $totalrecord->id)
			 {
				 dump('All Records Copied Succesfully. please do not refresh page');
				 dump('Currently Records Inserted: ',$precord);
				 dump('Total Number of Record which to be copy: ',$totalrecord->id);
			 }
			 else
			 {
				 dump('Refresh page to copy next 20000 Records');
				 dump('Currently Records Inserted: ',$precord);
				 dump('Total Number of Record which to be copy: ',$totalrecord->id);
			 }
		 }
		
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}