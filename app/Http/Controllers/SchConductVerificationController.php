<?php

namespace App\Http\Controllers;
use Carbon\Carbon;
use App\Traits\PQM;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Model\{Logger,ApplicantDetail, Permission, UserLastLogin, ScholarshipDetail, Listscholarship};


class SchConductVerificationController extends Controller
{
    protected $currentUserPermissions;
    protected $facultyPermissions;
    protected $departmentPermissions;
    protected $genderPermissions;
    protected $nationalityPermissions;
    protected $selectedSemester;

    public function index()
    {
        $this->assignPermissions();
        $SchView = ScholarshipDetail::get();

        if (count($SchView) > 0)
        {

        $scholarshipname = Listscholarship::find($SchView[0]->fkSchlrId);
        $ApplicantDetail = ApplicantDetail::find($SchView[0]->fkApplicantId);

         return view('scholarship.conductverification', compact('SchView', 'ApplicantDetail','scholarshipname'));

        }


        return view('scholarship.conductverification', compact('SchView'));

    }

    public function edit($id)
    {
        $SchView = ScholarshipDetail::find($id);
        $scholarshipname=Listscholarship::find($SchView['fkSchlrId']);
        $ApplicantDetail= ApplicantDetail::where('fkApplicantId',$SchView['fkApplicantId'])->first();
        return view('scholarship.editconductverification', compact('scholarshipname','ApplicantDetail','SchView'));
    }
    
    public function update($id)
    {
        $SchView=ScholarshipDetail::find($id);
        $SchView->conductverfication=request('con_ver_status');
        $SchView->condverremarks=request('con_ver_remarks');
        $SchView->update();
        return redirect('/admin/conductverification/');
    }

    public function assignPermissions()
    {
        $this->currentUserPermissions = $this->getCurrentUserPermissions();
        $this->facultyPermissions = explode(',', $this->currentUserPermissions->fkFacultyId);
        $this->departmentPermissions = explode(',',$this->currentUserPermissions->fkDepartmentId);
        $this->genderPermissions = $this->currentUserPermissions->fkGenderId;
        $this->nationalityPermissions = explode(',',$this->currentUserPermissions->fkNationality);
    }

     public function checkIfHasFullAccess() {
        if 
        (  count ( $this->facultyPermissions ) == 9 && 
            $this->departmentPermissions[0] == '' && 
            $this->genderPermissions == 1 && 
            (count( $this->nationalityPermissions) == 3 || 
            $this->nationalityPermissions[0] == '' ||  $this->nationalityPermissions[0] == 'NULL') 
        )
            return true;
        return false;
    }

    public function checkIfHasLimitedAccess( $applications )
    {
        $filtered = $applications->filter(function ( $value, $key ) {
            return $this->filterApplication ( $value );
        });
        return $filtered;
    }


    public function checkAccessLevelAndGetApplication( $applications ) 
    {
        return ( $this->checkIfHasFullAccess() ) ? $applications : $this->checkIfHasLimitedAccess($applications);   
    }

    public function paginateResults($applications, $paginate)
    {
        $currentPage = LengthAwarePaginator::resolveCurrentPage();
        $currentPageResults = $applications->slice(($currentPage-1) * $paginate, $paginate)->all();
        $result = new LengthAwarePaginator($currentPageResults, count($applications), $paginate);
        $result->setPath(request()->url());
        return $result;
    }


    public function returnApplicationsView($filter, $searchable, $paginate, $applications, $selectedSemester)
    {
        $semesters = Semester::where('pkSemesterId', '>', '10')->get();
        $curr_sem = Semester::where('status', '1')->first();
        return view('scholarship.addscholarship', compact('filter', 'searchable', 'paginate', 'applications', 'semesters', 'selectedSemester', 'curr_sem'));
    }
    public static function returnListNumber($programId, $rollNumber){
        // Result::with('list')->where('fkProgrammeId', $programId)
        $sql = "SELECT pkResultsId from tbl_oas_results rs inner join tbl_oas_result_list rl on rs.pkResultsId=rl.fkResultId  where rs.fkProgrammeId=".$programId." and rs.fkProgrammeId in(131, 132) and rl.rollno=".$rollNumber;
        $id = \DB::select($sql);
        if($id){
            return $id[0]->pkResultsId;
        }            
    }

    public function getSpecificSemesterApplications()
    {
        return redirect()->route('scholarships', ['semester' => request('semesterSelector')]);
    }

    public function viewLog($id ='', $fkApplicantId = ''){
        $current_semes = Semester::where('status', '1')->first()->pkSemesterId;
        $previousQualificationLog = UserLogQualification::where('applicant_id', request('fkApplicantId'))->where('fkSemesterId', $current_semes)->where('description', 'Previous Qualification')->get();
        $documentsLog = UserLogQualification::where('application_id', request('id'))->where('fkSemesterId', $current_semes)->where('description', 'Document Verify')->get();
        // $previousQualificationLog = unserialize($previousQualificationLog->existing_value);
        // dd($previousQualificationLog, $documentsLog);
        return view ('applications.viewLog', compact('previousQualificationLog', 'documentsLog'));    
    }

    public function superadmin(Request $request, Application $application)
    {
        $application = Application::where('id', $request->id)->first();
        $logs = UserLastLogin::where('userId', $application->fkApplicantId)->get();
        // dump($logs);
        $fee_verify = 'NULL';
        $docverify = 'NULL';
        if ($application->docsVerifyAdmin) {
            $docverify = $application->docsVerifyAdmin->username;
        }
        if ($application->user) {
            $fee_verify = $application->user->username;
        }
        return response()->json(['application' => $application, 'fee_verified_by' => $fee_verify, 'documents_verified_by' => $docverify, 'logs'=>$logs]);
    }

    public function superadminid(Request $request){
        if($request->id){
            $users = \DB::table('tb_users')->where('id','=',$request->id)->get()->first();
            return response()->json(['username'=>$users->username]);
        }
    }
}
