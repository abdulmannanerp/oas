<?php
namespace App\Http\Controllers;

use App\Model \{
    Account, Department, Faculty, Option, ProgramFee, Programme, Result, ResultList, RollNumber, Semester, Application, ApplicantDetail
};

use App\Jobs\GenerateArriersChallan;
use App\Jobs\PicReplace;
use App\Model\ChallanDetail;
use App\Traits\BankChallanDetail;
use Barryvdh\DomPDF\Facade as PDF;
use Config;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use League\Csv\Reader;
use League\Csv\Writer;
use Mail;
use SplTempFileObject;
use setasign\Fpdi\Fpdi;




class ResultController extends Controller
{
    use BankChallanDetail;
    public function index($id = '')
    {
        $result = '';
        $faculty = '';
        $department = '';
        $program = '';
        $selectedDepartments = '';
        $selectedProgrames = '';
        if ($id) {
            $result = Result::find($id);
            $selectedDepartments = $this->getSelectedDepartments($result->fkFacultyId);
            $selectedProgrames = $this->getSelectedProgramme($result->fkDeptId);
        }
        $faculties = Faculty::select('pkFacId', 'title')->get();
        $status = request()->session()->get('status') ?? '';
        $invalid = array();
        return view('results.index', compact('id', 'invalid','faculties', 'status', 'result', 'selectedDepartments', 'selectedProgrames'));
    }
    public function indexMerit($id = '')
    {
        $result = '';
        $faculty = '';
        $department = '';
        $program = '';
        $selectedDepartments = '';
        $selectedProgrames = '';
        if ($id) {
            $result = Result::find($id);
            $selectedDepartments = $this->getSelectedDepartments($result->fkFacultyId);
            $selectedProgrames = $this->getSelectedProgramme($result->fkDeptId);
        }
        $faculties = Faculty::select('pkFacId', 'title')->get();
        $status = request()->session()->get('status') ?? '';
        $invalid = array();
        return view('results.indexMerit', compact('id', 'invalid','faculties', 'status', 'result', 'selectedDepartments', 'selectedProgrames'));
    }

    public function store()
    {
        $checkIfResultAlreadyExists = Result::where([
            ['fkFacultyId', request('faculty')],
            ['fkDeptId', request('department')],
            ['fkProgrammeId', request('programme')],
            ['gender', request('selectGender')],
            ['listNumber', request('listNumber')],
            ['fkSemesterId', $this->getActiveSemester()],
            ['type', request('resultType')]
        ])->first();

        if ($checkIfResultAlreadyExists != '') {
            if (request('resultType') == 'Result')
                return redirect()->back()->withInput(request()->all())->withErrors(request('listNumber') . ' list already exists');
            return redirect()->back()->withInput(request()->all())->withErrors(['Result already exists']);
        }
        $path = request()->file('fileToUplaod')->store('results');

        $result = Result::create([
            'fkSemesterId' => $this->getActiveSemester(),
            'fkFacultyId' => request('faculty'),
            'fkDeptId' => request('department'),
            'fkProgrammeId' => request('programme'),
            'gender' => request('selectGender'),
            'listNumber' => request('listNumber'),
            'type' => request('resultType'),
            'status' => request('publishStatus'),
            'path' => $path,
            // 'startDate' => request('startDate'),
            'endDate' => request('endDate'),
            // 'semesterStartDate' => request('semesterStartDate'),
            // 'documentSubmissionLastDate' => request('documentSubmissionLastDate'),
            // 'feeSubmissionLastDate' => request('feeSubmissionLastDate'),
            'instructions' => request('instructions'),
            'active' => '1',
            'fkUserId' => request('userId'),
            'modifiedBy' => request('userId')
        ]);
        $csv = Reader::createFromPath(storage_path('app/') . $path, 'r');
        $records = $csv->setOffset(1)->fetchAll();
        if(request('selectGender') == 'Male'){
            $genderResult = 3;
        }
        if(request('selectGender') == 'Female'){
            $genderResult = 2;
        }        
        $invalid = '';
        foreach ($records as $record) {
            $recrd = preg_replace('/[^0-9]/', '', $record[0]);
            $roll_Number = RollNumber::where('id', $recrd)->first();
            if (empty($roll_Number) || ($roll_Number->application->fkCurrentStatus == 6 || $roll_Number->application->fkCurrentStatus == 7) || $genderResult != $roll_Number->applicat->fkGenderId) { 
                $invalid = $invalid. ' , '.$record[0];
                continue;
            }
            ResultList::create([
                'fkResultId' => $result->pkResultsId,
                'rollno' => str_replace( ' ', '', $record[0] ) ?? '',
                'name' => $record[1] ?? '',
                'fatherName' => $record[2] ?? ''
            ]);

            if ( $result->type == 'Result') {
                // Dev Mannan: Code for data insertion in tbl_oas_challan_detail
                $applicantInfo = RollNumber::with(['applicant', 'application', 'application.program', 'applicant.detail', 'application.semester'])->find($record[0]);
                if ( $applicantInfo ) {
                    if($applicantInfo->application->fkProgramId == 189){
                        $program_id = request('programme');
                    }else{
                        $program_id = $applicantInfo->application->fkProgramId;
                    }
                    $fee = ProgramFee::where('fkProgramId', $program_id)->first();
                    if(!$fee){
                        die('Last Inserted Applicant applied in a program where Fee Structure of specific program is missing. Please create fee structure and then try to upload the list');
                    }
                    $resultd = ChallanDetail::where('fkRollNumber', $record[0])->count();
                    if (!$resultd || (request('programme') == 131 || request('programme') == 132 || request('programme') == 157)) {
                        // dump('aaaaa');
                        $challanInfo = $this->insertInfo(
                            $record[0], 
                            $applicantInfo->fkApplicationId, 
                            $applicantInfo->fkSemesterId, 
                            $program_id, 
                            '2', 
                            '', 
                            $fee->admission_fee, 
                            $fee->university_dues, 
                            '', 
                            '', 
                            '', 
                            (integer)$fee->admission_fee + (integer)$fee->university_dues
                        );
                        $challanInfo = $this->insertInfo(
                            $record[0], 
                            $applicantInfo->fkApplicationId, 
                            $applicantInfo->fkSemesterId, 
                            $program_id, 
                            '3', 
                            '', 
                            '', 
                            '', 
                            $fee->library_security, 
                            $fee->book_bank_security, 
                            $fee->caution_money, 
                            (integer)$fee->library_security + (integer)$fee->book_bank_security + (integer)$fee->caution_money
                        );
                    }
                // Dev Mannan: Code ends
                }
            } //end result->type != interview
        } //endforeach
        $this->challanAblEmail();
        if($invalid != ''){
            return redirect()->back()->withInput(request()->all())->withErrors(['Result uploaded successfully and Following invalid Entries are not inserted [ '.$invalid.' ]']);
        }else{
            return back()->with('status', 'Uploaded Successfully');
        }
    }
    public function storeMerit()
    {
        $checkIfResultAlreadyExists = Result::where([
            ['fkFacultyId', request('faculty')],
            ['fkDeptId', request('department')],
            ['fkProgrammeId', request('programme')],
            ['gender', request('selectGender')],
            ['listNumber', request('listNumber')],
            ['fkSemesterId', $this->getActiveSemester()],
            ['type', request('resultType')]
        ])->first();

        if ($checkIfResultAlreadyExists != '') {
            if (request('resultType') == 'Result')
                return redirect()->back()->withInput(request()->all())->withErrors(request('listNumber') . ' list already exists');
            return redirect()->back()->withInput(request()->all())->withErrors(['Result already exists']);
        }
        $path = request()->file('fileToUplaod')->store('results');

        $result = Result::create([
            'fkSemesterId' => $this->getActiveSemester(),
            'fkFacultyId' => request('faculty'),
            'fkDeptId' => request('department'),
            'fkProgrammeId' => request('programme'),
            'gender' => request('selectGender'),
            'listNumber' => request('listNumber'),
            'type' => request('resultType'),
            'status' => NULL,
            'path' => NULL,
            // 'startDate' => request('startDate'),
            'endDate' => request('endDate'),
            // 'semesterStartDate' => request('semesterStartDate'),
            // 'documentSubmissionLastDate' => request('documentSubmissionLastDate'),
            // 'feeSubmissionLastDate' => request('feeSubmissionLastDate'),
            'instructions' => request('instructions'),
            'active' => '0',
            'fkUserId' => request('userId'),
            'modifiedBy' => request('userId')
        ]);
        $cutPoint = request('cutPoint');
        $ll = Application::with(
                [
                'applicant', 
                'program',
                'pref1', 
                'pref2', 
                'pref3',
                'program.programscheduler', 
                'applicant.previousQualification', 
                'applicant.detail', 
                'applicant.detail.district', 
                'applicant.address', 
                'getRollNumber'
                ])->where('fkProgramId', request('programme'))
                ->orderBy('pqm', 'DESC')
                ->where('fkSemesterId', $this->getActiveSemester())
                ->getStatusByGender('=', '5', '3', '>=', '1')
                ->paginate($cutPoint);
                dd($ll);
        $csv = Reader::createFromPath(storage_path('app/') . $path, 'r');
        $records = $csv->setOffset(1)->fetchAll();
        $invalid = '';
        foreach ($records as $record) {
            $recrd = preg_replace('/[^0-9]/', '', $record[0]);
            $roll_Number = RollNumber::where('id', $recrd)->first();
            if (empty($roll_Number)) { 
                $invalid = $invalid. ' , '.$record[0];
                continue;
            }
            ResultList::create([
                'fkResultId' => $result->pkResultsId,
                'rollno' => str_replace( ' ', '', $record[0] ) ?? '',
                'name' => $record[1] ?? '',
                'fatherName' => $record[2] ?? ''
            ]);

            if ( $result->type != 'Interview' ) {
                // Dev Mannan: Code for data insertion in tbl_oas_challan_detail
                $applicantInfo = RollNumber::with(['applicant', 'application', 'application.program', 'applicant.detail', 'application.semester'])->find($record[0]);
                if ( $applicantInfo ) {
                    $fee = ProgramFee::where('fkProgramId', $applicantInfo->application->fkProgramId)->first();
                    $resultd = ChallanDetail::where('fkRollNumber', $record[0])->count();
                    if (!$resultd) {
                        $challanInfo = $this->insertInfo(
                            $record[0], $applicantInfo->fkApplicationId, $applicantInfo->fkSemesterId, $applicantInfo->application->fkProgramId, '2', '', $fee->admission_fee, $fee->university_dues, '', '', '', (integer)$fee->admission_fee + (integer)$fee->university_dues
                        );
                        $challanInfo = $this->insertInfo(
                            $record[0], $applicantInfo->fkApplicationId, $applicantInfo->fkSemesterId, $applicantInfo->application->fkProgramId, '3', '', '', '', $fee->library_security, $fee->book_bank_security, $fee->caution_money, (integer)$fee->library_security + (integer)$fee->book_bank_security + (integer)$fee->caution_money
                        );
                    }
                // Dev Mannan: Code ends
                }
            } //end result->type != interview
        } //endforeach
        $this->challanAblEmail();
        if($invalid != ''){
            return redirect()->back()->withInput(request()->all())->withErrors(['Result uploaded successfully and Following invalid Entries are not inserted [ '.$invalid.' ]']);
        }else{
            return back()->with('status', 'Uploaded Successfully');
        }
    }
    
    public function challanAblEmail(){
        $row_count = 0;
        $csv = Writer::createFromFileObject(new SplTempFileObject());
        $csv->insertOne(['Issue_date', 'Challan_No', 'ClassName', 'Bank_Account', 'Amount','Narration','Profit_Center','Std_Id','SP_Gl', 'Std_Name', 'Ins_No', 'Year', 'Due_date', 'Valid_date', 'Status']);
        $results = Result::where('gender', 'Female')->where('fkSemesterId', $this->getActiveSemester())->where('email_sent', 0)->get();
        foreach($results as $rs){
            $resultList = ResultList::select('rollno')->where('fkResultId',$rs->pkResultsId)->get()->pluck('rollno')->toArray();
            $challanDetail = ChallanDetail::whereIn('fkRollNumber', $resultList)->where('email_sent', 0)->get();
            if($challanDetail && $challanDetail->count() > 0){
                foreach($challanDetail as $cd){
                    $row_count++;
                    $genderTitle = ($cd->application->applicant->fkGenderId == 2) ? 'Female' : 'Male';
                    $accountNumber = Account::where('fkFacId', $cd->program->faculty->pkFacId)->where('bank_type', $genderTitle)->first();
                    $challan_type = ($cd->challan_type == 2)? 'Dues': 'Security' ;
                    if($challan_type == 'Dues'){
                        $account = $accountNumber->dues_account;
                    }else{
                        $account = $accountNumber->security_account;
                    }
                    $program_name = str_replace(",", " ", $rs->program->title);
                    $csv->insertOne(
                        [
                            date_format(date_create($rs->created_at), 'Ymd') ?? '',
                            $cd->id ?? '',
                            $program_name ?? '',
                            $account ?? '',
                            $cd->total ?? '',
                            $rs->program->faculty->title ?? '',
                            '',
                            $cd->fkRollNumber ?? '',
                            '',
                            $cd->application->applicant->name ?? '',
                            '',
                            $cd->semester->title ?? '',
                            date_format(date_create($rs->endDate), 'Ymd') ?? '',
                            date_format(date_create($rs->endDate), 'Ymd') ?? '',
                            'New'
                        ]
                    );

                    ChallanDetail::where('id', $cd->id)
                    ->update([
                        'email_sent' => 1
                    ]);
                }
            }
            Result::where('pkResultsId', $rs->pkResultsId)
                ->update([
                    'email_sent' => 1
                ]);
        }
        if($row_count > 0){
            $currentDate = date('d-m-Y h:i:s A');
            // $message = "Report generated by Online Admission Software on $currentDate";
            // $csv->insertOne([$message]);
            $fileName = 'IIU-Admission-'.$currentDate.'.csv';
            file_put_contents(storage_path('app/abl/'.$fileName), $csv);
            // $csv->output($fileName);
            // exit;
            $email_host = 'smtp-relay.gmail.com';
            $email_port = '587';
            $email_encryption = 'tls';
            $email_username = 'fee.challans@iiu.edu.pk';
            // $email_password = 'jdBTw$v@sT1^';
           
			$email_password = 'kqagwvocxrcaxxeq'; //App Password

            Config::set('mail.driver', 'smtp');
            Config::set('mail.host', $email_host);
            Config::set('mail.port', $email_port);
            Config::set('mail.encryption', $email_encryption);
            Config::set('mail.username', $email_username);
            Config::set('mail.password', $email_password);

            // $to ='abdul.mannan@iiu.edu.pk';
            $to ='T24SysAdmins@abl.com';
            $subject = 'IIUI Admission Female Challan Data';
            Mail::send([], [], function ($message) use ($to, $subject, $email_username, $fileName) {
                $message->to($to)
                    ->cc(['nadia.hafeez@iiu.edu.pk', 'mariam.javed@iiu.edu.pk', 'fee.challans@iiu.edu.pk', 'zafar.kamran@abl.com'])
                    ->replyTo('fee.challans@iiu.edu.pk', 'IIU Admission Female Challan Data')
                    ->subject($subject);
                $message->from($email_username, 'IIU Admission Female Challan Data');
                $message->attach(storage_path('app/abl/'.$fileName));
            });
        }
        // die();
    }
    public function arrearChallanData(){
        $row_count = 0;
        $csv = Writer::createFromFileObject(new SplTempFileObject());
        $csv->insertOne(['Issue_date', 'Challan_No', 'ClassName', 'Bank_Account', 'Amount','Narration','Profit_Center','Std_Id','SP_Gl', 'Std_Name', 'Ins_No', 'Year', 'Due_date', 'Valid_date', 'Status']);
        $challanDetail = ChallanDetail::with(['application', 'application.applicant'])
        ->whereIn('challan_type', array(4,5))
        ->whereHas('application.applicant', function ( $query ) {
            $query->where('fkGenderId', 2);
        })->get();
        // dd($challanDetail);
            if($challanDetail && $challanDetail->count() > 0){
                foreach($challanDetail as $cd){
                    $row_count++;
                    $genderTitle = ($cd->application->applicant->fkGenderId == 2) ? 'Female' : 'Male';
                    $accountNumber = Account::where('fkFacId', $cd->program->faculty->pkFacId)->where('bank_type', $genderTitle)->first();
                    $challan_type = ($cd->challan_type == 4)? 'Dues': 'Security' ;
                    if($challan_type == 'Dues'){
                        $account = $accountNumber->dues_account;
                    }else{
                        $account = $accountNumber->security_account;
                    }
                    $program_name = str_replace(",", " ", $cd->program->title);
                    $csv->insertOne(
                        [
                            '20190902',
                            $cd->id ?? '',
                            $program_name ?? '',
                            $account ?? '',
                            $cd->total ?? '',
                            $cd->program->faculty->title ?? '',
                            '',
                            $cd->fkRollNumber ?? '',
                            '',
                            $cd->application->applicant->name ?? '',
                            '',
                            $cd->semester->title ?? '',
                            '20190915',
                            '20190915',
                            'New'
                        ]
                    );
                }
            }
            if($row_count > 0){
                $currentDate = date('d-m-Y h:i:s A');
                // $message = "Report generated by Online Admission Software on $currentDate";
                // $csv->insertOne([$message]);
                $fileName = 'IIU-Admission-'.$currentDate.'.csv';
                file_put_contents(storage_path('app/abl/'.$fileName), $csv);
                $csv->output($fileName);
                exit;
                // $email_host = 'smtp-relay.gmail.com';
                // $email_port = '587';
                // $email_encryption = 'tls';
                // $email_username = 'fee.challans@iiu.edu.pk';
                // $email_password = 'jdBTw$v@sT1^';
    
                // Config::set('mail.driver', 'smtp');
                // Config::set('mail.host', $email_host);
                // Config::set('mail.port', $email_port);
                // Config::set('mail.encryption', $email_encryption);
                // Config::set('mail.username', $email_username);
                // Config::set('mail.password', $email_password);
    
                // $to ='T24SysAdmins@abl.com';
                // $subject = 'IIU Admission Female Challan Data';
                // Mail::send([], [], function ($message) use ($to, $subject, $email_username, $fileName) {
                //     $message->to($to)
                //         ->cc(['nadia.hafeez@iiu.edu.pk', 'nishwa.iqbal@iiu.edu.pk', 'mariam.javed@iiu.edu.pk', 'nuzhat.zareen@iiu.edu.pk', 'fee.challans@iiu.edu.pk', 'Shazia.Razaq@abl.com'])
                //         ->replyTo('fee.challans@iiu.edu.pk', 'IIU Admission Female Challan Data')
                //         ->subject($subject);
                //     $message->from($email_username, 'IIU Admission Female Challan Data');
                //     $message->attach(storage_path('app/abl/'.$fileName));
                // });
            }
    }
    public function getActiveSemester()
    {
        return Semester::where('status', 1)->first()->pkSemesterId;
    }

    public function fckrecords()
    {
        $allrec = ResultList::all();
        foreach ($allrec as $f) {
            // echo $f->rollno;
            // exit;
            $applicantInfo = RollNumber::with(['applicant', 'application', 'application.program', 'applicant.detail', 'application.semester'])->find($f->rollno);
            if ($applicantInfo) {
                $fee = ProgramFee::where('fkProgramId', $applicantInfo->application->fkProgramId)->first();
                $resultd = ChallanDetail::where('fkRollNumber', $f->rollno)->count();
                if (!$resultd) {
                    $challanInfo = $this->insertInfo(
                        $f->rollno,
                        $applicantInfo->fkApplicationId,
                        $applicantInfo->fkSemesterId,
                        $applicantInfo->application->fkProgramId,
                        '2',
                        '',
                        $fee->admission_fee,
                        $fee->university_dues,
                        '',
                        '',
                        '',
                        (integer)$fee->admission_fee + (integer)$fee->university_dues,
                        $applicantInfo->fkApplicationId
                    );
                    $challanInfo = $this->insertInfo(
                        $f->rollno,
                        $applicantInfo->fkApplicationId,
                        $applicantInfo->fkSemesterId,
                        $applicantInfo->application->fkProgramId,
                        '3',
                        '',
                        '',
                        '',
                        $fee->library_security,
                        $fee->book_bank_security,
                        $fee->caution_money,
                        (integer)$fee->library_security + (integer)$fee->book_bank_security + (integer)$fee->caution_money,
                        $applicantInfo->fkApplicationId . '-R'
                    );


                }
            }
        }

    }

    public function update()
    {
        $invalid = '';
        $resultToPatch = request('result-to-patch');
        $result = Result::find($resultToPatch);
        $path = $result->path;
        $record_found = 0;
        if (request()->hasFile('fileToUplaod')) {
            $record_found = 1;
            $path = request()->file('fileToUplaod')->store('results');
            DB::transaction(function () use ($result, $path) {
                ResultList::where('fkResultId', $result->pkResultsId)->delete();
                $csv = Reader::createFromPath(storage_path('app/') . $path, 'r');
                $records = $csv->setOffset(1)->fetchAll();
                if(request('selectGender') == 'Male'){
                    $genderResult = 3;
                }
                if(request('selectGender') == 'Female'){
                    $genderResult = 2;
                }  
                $invalid = '';
                foreach ($records as $record) {
                    $recrd = preg_replace('/[^0-9]/', '', $record[0]);
                    $roll_Number = RollNumber::where('id', $recrd)->first();
                    if (empty($roll_Number) || ($roll_Number->application->fkCurrentStatus == 6 || $roll_Number->application->fkCurrentStatus == 7) || $genderResult != $roll_Number->applicat->fkGenderId) { 
                        $invalid = $invalid. ' , '.$record[0];
                        continue;
                    }
                    ResultList::create([
                        'fkResultId' => $result->pkResultsId,
                        'rollno' => str_replace( ' ', '', $record[0] ) ?? '',
                        'name' => $record[1] ?? '',
                        'fatherName' => $record[2] ?? ''
                    ]);
                    if ( $result->type != 'Interview' ) {
                        // Dev Mannan: Code for data insertion in tbl_oas_challan_detail
                        $applicantInfo = RollNumber::with(['applicant', 'application', 'application.program', 'applicant.detail', 'application.semester'])->find($record[0]);
                        if ( $applicantInfo ) {
                            if($applicantInfo->application->fkProgramId == 189){
                                $program_id = request('programme');
                            }else{
                                $program_id = $applicantInfo->application->fkProgramId;
                            }
                            $fee = ProgramFee::where('fkProgramId', $program_id)->first();
                            $resultd = ChallanDetail::where('fkRollNumber', $record[0])->count();
                            if (!$resultd || (request('programme') == 131 || request('programme') == 132 || request('programme') == 157)) {
                                //first challan -> dues
                                $challanInfo = $this->insertInfo(
                                    $record[0],
                                    $applicantInfo->fkApplicationId,
                                    $applicantInfo->fkSemesterId,
                                    $program_id,
                                    '2',
                                    '',
                                    $fee->admission_fee,
                                    $fee->university_dues,
                                    '',
                                    '',
                                    '',
                                    (integer)$fee->admission_fee + (integer)$fee->university_dues
                                );
                                //second challan -> security
                                $challanInfo = $this->insertInfo(
                                    $record[0],
                                    $applicantInfo->fkApplicationId,
                                    $applicantInfo->fkSemesterId,
                                    $program_id,
                                    '3',
                                    '',
                                    '',
                                    '',
                                    $fee->library_security,
                                    $fee->book_bank_security,
                                    $fee->caution_money,
                                    (integer)$fee->library_security + (integer)$fee->book_bank_security + (integer)$fee->caution_money
                                );
                            }
                        }
                        // Dev Mannan: Code ends
                    } 
                } //end foreach
            }, 3);
        }

        if ($result) {
            $result->fkFacultyId = request('faculty');
            $result->fkDeptId = request('department');
            $result->fkProgrammeId = request('programme');
            $result->gender = request('selectGender');
            $result->listNumber = request('listNumber');
            $result->type = request('resultType');
            $result->status = request('publishStatus');
            $result->path = $path;
            // $result->startDate = request('startDate');
            $result->endDate = request('endDate');
            // $result->semesterStartDate = request('semesterStartDate');
            // $result->documentSubmissionLastDate = request('documentSubmissionLastDate');
            // $result->feeSubmissionLastDate = request('feeSubmissionLastDate');
            $result->instructions = request('instructions');
            $result->fkUserId = request('userId');
            $result->modifiedBy = request('userId');
            $result->list_text = (empty(trim(request('list_text'))))? NULL : trim(request('list_text')) ;
            if($record_found == 1){
                $result->email_sent = 0;
            }
            $result->save();
            $this->challanAblEmail();
        }
        if($invalid != ''){
            return redirect()->back()->withInput(request()->all())->withErrors(['Result uploaded successfully and Following invalid Entries are not inserted [ '.$invalid.' ]']);
        }else{
            return back();
        }
    }

    public function manage()
    {
        $faculties = Result::select('fkFacultyId')->distinct('fkFacultyId')->get();
        $faculties = Faculty::whereIn('pkFacId', $faculties)->get();
        $semesters = Semester::where('pkSemesterId', '>', '10')->get();
        $results = '';
        if (request()->method() == 'POST') {
            $results = Result::with(['program'])->where([
                ['fkFacultyId', request('faculty')],
                ['fkDeptId', request('department')],
                ['fkSemesterId', request('semester')],
                ['fkProgrammeId', request('programme')]
            ])->orderBy('created_at', 'desc')->get();
        }
        return view('results.manage', compact('results', 'faculties', 'semesters'));
    }
    public function compareList($id, $gender, $semester){
        // dd($id, $gender, $semester);
        $Gen = $gender;
    if($gender == 'Male'){
        $gender = '3';    
    }
    if($gender == 'Female'){
        $gender = '2';    
    }
    $result = Application::with(
        [
            'applicant', 
            'program',
            'pref1', 
            'pref2', 
            'pref3', 
            'program.programscheduler', 
            'applicant.previousQualification', 
            'applicant.detail', 
            'applicant.detail.district', 
            'applicant.address', 
            'getRollNumber.rollNo'
        ])
        ->where('fkProgramId', $id)
        ->orderBy('pqm', 'DESC')
        ->where('fkSemesterId', $semester)
        ->getStatusByGender('=', '5', $gender, '>=', '1');
        // dd($id, $semester, $gender);
        $res =Result::where('fkProgrammeId', $id)->where('type', 'Result')->where('fkSemesterId', $semester)->where('gender', $Gen)->orderBy('pkResultsId', 'ASC')->get();
        $arrayVal = array();
        foreach($res as $rs){
            $rs->listNumber;
            $totalResults = ResultList::where('fkResultId', $rs->pkResultsId)->count();
            $arrayVal[$rs->listNumber] = $totalResults;
        }

        $total = 0; $text = '<div class="list-compare">';
        foreach ($arrayVal as $key => $value) {
            $text.= '<div class="inside-cmpr"><span class="cm-first">'.$value."</span> <span class='cm-second'>". $key. ": List </span></div>";
            $total += $value;
        }
        $text.= "<div class='inside-cmpr'><span class='cm-first'>". $total. " </span> <span class='cm-second'>Total</span></div>";
        $remaining = 1; $notSelected = 0;
        foreach ( $result as $list ){
            if(isset($list->getRollNumber->rollNo) && $list->getRollNumber->rollNo->result->type == 'Result'){
                if( $remaining < $total){
                    $remaining++;
                }
            }else{
              
                if( $remaining < $total){
                    // dump($list->getRollNumber->rollNo.'='. $remaining.'='.$total);
                    $notSelected++;
                }
            }
        }
        // dd($notSelected);
        if($notSelected > 0){
            $text.= "<div class='inside-cmpr'><span class='cm-first'>". $notSelected. " </span> <span class='cm-second'>Eligible But not selected</span></div>";
        }
        $text .= "</div>";
        $programTitle = Programme::where('pkProgId', $id)->first();
        return view('results.compare', compact('result', 'text', 'programTitle'));
    }

    public function changeActiveStatus()
    {
        $id = request('result_id');
        $currentStatus = request('current_status');
        $changeResultStatus = Result::find($id);
        $changeResultStatus->active = (string)!(bool)$currentStatus;
        $changeResultStatus->save();
        return (string)!(bool)$currentStatus;
    }

    public function changeStatePublic()
    {
        $id = request('result_id');
        $currentStatus = request('current_status');
        $changeResultStatus = Result::find($id);
        $changeResultStatus->public_access = (string)!(bool)$currentStatus;
        $changeResultStatus->save();
        return (string)!(bool)$currentStatus;
    }

    public function recentResults($id='')
    {
        if(!$id){
            $id =$this->getCurrentSemester()->pkSemesterId;
            $semester = $this->getCurrentSemester()->title;
        }else{
            $semester = Semester::where('pkSemesterId', $id)->first();
            $semester = $semester->title;
        }
        $newAnnouncements = Result::with(['program'])->where('active', '1')->where('fkSemesterId', $id)->orderBy('created_at', 'DESC')->get();
// dd($newAnnouncements);
        // if($id){
        //     $newAnnouncements = Result::with(['program'])->where('active', '1')->where('fkSemesterId', $id)->orderBy('created_at', 'desc')->get(); // ->whereRaw('created_at >= now() - INTERVAL 3 DAY')    
        //     $semester = Semester::where('pkSemesterId', $id)->first();
        // }else{
        //     $newAnnouncements = Result::with(['program'])->where('active', '1')->orderBy('created_at', 'desc')->get(); // ->whereRaw('created_at >= now() - INTERVAL 3 DAY')
        //     $semester = '';
        // }
        return view('results.recent', compact('newAnnouncements', 'semester'));
    }

    public function show($id='')
    {
        $results = '';
        $faculties = Result::select('fkFacultyId')->distinct('fkFacultyId')->get();
        $faculties = Faculty::whereIn('pkFacId', $faculties)->get();
        if(!$id){
            $id =$this->getCurrentSemester()->pkSemesterId;
            $title = $this->getCurrentSemester()->title;
        }
        // dd($id);
        $programs = Result::with(['program'])->select('fkProgrammeId')->where('active', 1)->where('fkSemesterId', $id)->orderBy('fkProgrammeId')->distinct()->get();
        // dd($programs);
        if (request()->isMethod('post')) {
            $results = Result::with(['program'])->where([
                ['fkProgrammeId', request('programme')],
                ['fkSemesterId', $id],
                ['active', '1']
            ])->orderBy('created_at', 'desc')->get();
        }

        return view('results.public', compact('faculties', 'results', 'programs','title'));
    }

    public function getResultByRollNumber()
    {
        $rollNumber = request('rollnumber');
        $results = ResultList::with(['result', 'result.program'])->whereHas('result', function ($query) {
            $query->active = 1;
        })->where('rollno', $rollNumber)->get();

        return view('results.search-by-rollno', compact('results'));
    }

    public function getResultDocs($resultId, $rollno, $doc, $cnic='')
    {
        /** Get dates, add helper to identify merit & result module */
        $result = ResultList::with(['result'])->where('rollno', $rollno)->whereHas('result', function ($query) use ($resultId) {
            $query->where('pkResultsId', $resultId);
        })->first();
        if ( $result ) {
            $getIssueDate = Result::find($resultId);
            //$issuedDate = $result->created_at->format('d-M-Y');
            $issuedDate = $getIssueDate->created_at->format('d-M-Y');
            // $semesterStartDate = $result->result->semesterStartDate;
            $semesterStartDate = Option::where('title', 'semester-start-date')->first()->value;
            $semesterStartDate = date_format(date_create($semesterStartDate), 'd-M-Y');
            // $documentSubmissionLastDate = $result->result->documentSubmissionLastDate;
            $documentSubmissionLastDate = Option::where('title', 'document-submission-last-date')->first()->value;
            $documentSubmissionLastDate = date_format(date_create($documentSubmissionLastDate), 'd-M-Y');
            // $feeSubmissionLastDate = $result->result->feeSubmissionLastDate;
            $feeSubmissionLastDate = $result->result->endDate;

            $download = false;
            if (request('download') == 'true') {
                $download = true;
            }

            $semester = $this->getCurrentSemester();
            if ($doc == 'performa') {


                $duesDetail = ChallanDetail::where('fkRollNumber', $rollno)->where('challan_type', 2)->first();
                $applicantDetail = ApplicantDetail::where('fkApplicantId', $duesDetail->application->applicant->userId)->first();
                if($applicantDetail->applicant->cnic != $cnic){
                    return redirect()->back()->withInput(request()->all())->withErrors(['CNIC donot match']);
                }
                
                $result_id = Result::where('pkResultsId',$resultId)->first();
                $faculty = Faculty::where('pkFacId', $result_id->fkFacultyId)->first();
                $program = Programme::where('pkProgId', $result_id->fkProgrammeId)->first();
                $duesDetail = ChallanDetail::where('fkRollNumber', $rollno)->where('fkProgramId', $result_id->fkProgrammeId)->where('status', 1)->where('challan_type', 2)->first();
                $securityDetail = ChallanDetail::where('fkRollNumber', $rollno)->where('fkProgramId', $result_id->fkProgrammeId)->where('status', 1)->where('challan_type', 3)->first();
                $applicantDetail = ApplicantDetail::where('fkApplicantId', $duesDetail->application->applicant->userId)->first();
                $pdf = PDF::loadView('applications.joiningPerforma', compact('applicantDetail', 'semester', 'faculty', 'program', 'duesDetail', 'securityDetail'))->setPaper('a4', 'portrait');
                return $pdf->stream();


                // return $this->getJoiningPerforma($rollno, $semester, $download);
            } else if ($doc == 'offer-letter') {
                $duesDetail = ChallanDetail::where('fkRollNumber', $rollno)->where('challan_type', 2)->first();
                $applicantDetail = ApplicantDetail::where('fkApplicantId', $duesDetail->application->applicant->userId)->first();
                if($applicantDetail->applicant->cnic != $cnic){
                    return redirect()->back()->withInput(request()->all())->withErrors(['CNIC donot match']);
                }
                return $this->getOfferLetter($resultId, $rollno, $semester, $download, $semesterStartDate, $documentSubmissionLastDate, $feeSubmissionLastDate);
            } else if ($doc == 'fee-challan') {
                $duesDetail = ChallanDetail::where('fkRollNumber', $rollno)->where('challan_type', 2)->first();
                $semester = Semester::where('pkSemesterId', $duesDetail->fkSemesterId)->first();

                $applicantDetail = ApplicantDetail::where('fkApplicantId', $duesDetail->application->applicant->userId)->first();
                if($applicantDetail->applicant->cnic != $cnic){
                    return redirect()->back()->withInput(request()->all())->withErrors(['CNIC donot match']);
                }
                return $this->getFeeChallan($resultId, $rollno, $semester, $download, $feeSubmissionLastDate, $issuedDate);
            }else if ($doc == 'fee-challan-second') {
                return $this->getFeeChallanSecond($resultId, $rollno, $semester, $download, $feeSubmissionLastDate, $issuedDate);
            }else if($doc == 'docs'){
                // dd ( 'get all other than cahllan');
                // offer letter
                $offerLett = $this->getOfferLetter($resultId, $rollno, $semester, $download, $semesterStartDate, $documentSubmissionLastDate, $feeSubmissionLastDate, 'all');
                $rand_offer = rand(pow(10, 6-1), pow(10, 6)-1);
                $offerLett->save(storage_path('app/public/images/RESULT-DOWNLOAD/'.$rand_offer.'.pdf'));
                // dd($rand_offer);

                // $feeChall = $this->getFeeChallan($resultId, $rollno, $semester, $download, $feeSubmissionLastDate, $issuedDate, 'all');
                // $rand_challan = rand(pow(10, 6-1), pow(10, 6)-1);
                // $feeChall->save(storage_path('app/public/images/RESULT-DOWNLOAD/'.$rand_challan.'.pdf'));
                // dd($rand_challan);

               // dd(request('userId'));
                // $semester = Semester::where('status', 1)->first();
                $result_id = Result::where('pkResultsId',$resultId)->first();
                $faculty = Faculty::where('pkFacId', $result_id->fkFacultyId)->first();
                $program = Programme::where('pkProgId', $result_id->fkProgrammeId)->first();
                $duesDetail = ChallanDetail::where('fkRollNumber', $rollno)->where('fkProgramId', $result_id->fkProgrammeId)->where('status', 1)->where('challan_type', 2)->first();
                $securityDetail = ChallanDetail::where('fkRollNumber', $rollno)->where('fkProgramId', $result_id->fkProgrammeId)->where('status', 1)->where('challan_type', 3)->first();
                // dd($duesDetail->application->applicant->userId);
                $semester = Semester::where('pkSemesterId', $duesDetail->fkSemesterId)->first();

                $applicantDetail = ApplicantDetail::where('fkApplicantId', $duesDetail->application->applicant->userId)->first();
                if($applicantDetail->applicant->cnic != $cnic){
                    return redirect()->back()->withInput(request()->all())->withErrors(['CNIC donot match']);
                }
                $joiningPer = PDF::loadView('applications.joiningPerforma', compact('applicantDetail', 'semester', 'faculty', 'program', 'duesDetail', 'securityDetail'))->setPaper('a4', 'portrait');
                $rand_joining = rand(pow(10, 6-1), pow(10, 6)-1);
                $joiningPer->save(storage_path('app/public/images/RESULT-DOWNLOAD/'.$rand_joining.'.pdf'));

                $pdf = new Fpdi();

                $files = [
                    storage_path('app/public/images/RESULT-DOWNLOAD/'.$rand_offer.'.pdf'),
                    // storage_path('app/public/images/RESULT-DOWNLOAD/'.$rand_challan.'.pdf'),
                    storage_path('app/public/images/RESULT-DOWNLOAD/'.$rand_joining.'.pdf')
                ];

                foreach ($files as $file) {
                    $pageCount = $pdf->setSourceFile($file);
                
                    for ($pageNo = 1; $pageNo <= $pageCount; $pageNo++) {
                        $pdf->AddPage();
                        $pageId = $pdf->importPage($pageNo, '/MediaBox');
                        //$pageId = $pdf->importPage($pageNo, Fpdi\PdfReader\PageBoundaries::ART_BOX);
                        $s = $pdf->useTemplate($pageId, 10, 10, 200);
                    }
                }
                $file = uniqid().'.pdf';
                unlink(storage_path('app/public/images/RESULT-DOWNLOAD/'.$rand_offer.'.pdf'));
                // unlink(storage_path('app/public/images/RESULT-DOWNLOAD/'.$rand_challan.'.pdf'));
                unlink(storage_path('app/public/images/RESULT-DOWNLOAD/'.$rand_joining.'.pdf'));
                $pdf->Output('I', 'simple.pdf');
            }
        }
        return 'No Record Found';
    }

    public function getJoiningPerforma($rollno, $semester, $download)
    {
        $result = ResultList::with(['result', 'result.program'])->where('rollno', $rollno)->get();
        return view('joining-performa');
    }

    public function getOfferLetter($resultId, $rollno, $semester, $download, $semesterStartDate, $documentSubmissionLastDate, $feeSubmissionLastDate, $doc='')
    {
        $options = Option::whereIn('title', ['semester-start-date', 'document-submission-last-date '])
            ->select('title', 'value')
            ->get()
            ->pluck('value')
            ->toArray();
        // dd ( $options );
        $result = ResultList::with(['result', 'result.program'])->where('rollno', $rollno)->where('fkResultId',$resultId)->first();
        if ($result) {
            // $startDate = $result->result->startDate;
            $startDate = $result->result->created_at;
            $endDate = $result->result->endDate;
            $applicant = RollNumber::with(['applicant', 'application', 'application.program', 'applicant.detail'])->find($rollno);
            if ($download) {
                $pdf = PDF::loadView('results.download-offer-letter', compact('result', 'resultId', 'rollno', 'semester', 'applicant', 'options', 'startDate', 'endDate', 'semesterStartDate', 'documentSubmissionLastDate', 'feeSubmissionLastDate'));
                return $pdf->download('Offer Letter.pdf');
            }
            if($doc == 'all'){
                $pdf = PDF::loadView('results.download-offer-letter', compact('result', 'resultId', 'rollno', 'semester', 'applicant', 'options', 'startDate', 'endDate', 'semesterStartDate', 'documentSubmissionLastDate', 'feeSubmissionLastDate'));
                return $pdf;
            }
            return view('results.offer-letter', compact('result', 'resultId', 'rollno', 'semester', 'applicant', 'options', 'startDate', 'endDate', 'semesterStartDate', 'documentSubmissionLastDate', 'feeSubmissionLastDate'));
        }
    }

    public function getFeeChallan($resultId, $rollno, $semester, $download, $feeSubmissionLastDate, $issuedDate, $doc = '')
    {
        // Dev Mannan: code for challan starts
        $result_obj = Result::where('pkResultsId',$resultId)->first();
        $challan_dues = ChallanDetail::where('fkRollNumber', $rollno)->where('fkProgramId', $result_obj->fkProgrammeId)->where('challan_type', 2)->where('status', '!=', 0)->first();
        $challan_security = ChallanDetail::where('fkRollNumber', $rollno)->where('fkProgramId', $result_obj->fkProgrammeId)->where('challan_type', 3)->where('status', '!=', 0)->first();
        // dd($challan_dues);
        // Dev Mannan: Challan Code ends
        $result = ResultList::with(['result', 'getRollno', 'result.program'])->where('rollno', $rollno)->where('fkResultId',$resultId)->first();
        $programId = $result->result->fkProgrammeId;
        if ($result && $programId) {
            $fee = ProgramFee::where('fkProgramId', $programId)->first();
            $applicant = RollNumber::with(['applicant', 'application', 'application.program', 'application.program.department', 'application.program.faculty', 'applicant.detail'])->find($rollno);
            $gender = $applicant->applicant->fkGenderId;
            $genderTitle = ($gender == 2) ? 'Female' : 'Male';
            $program = Programme::with(['faculty', 'department'])
                ->select('pkProgId', 'fkFacId', 'fkDepId', 'title')
                ->find($programId);
            //Get the applicant nationality. 
            $accountNumber = Account::where('fkFacId', $program->faculty->pkFacId)->where('bank_type', $genderTitle)->first();
            $copies = ['Bank Copy', 'Accounts Copy', 'Academics Copy', 'Student Copy'];
            if ($download) {
                $pdf = PDF::loadView('results.download-fee-challan', compact('result', 'resultId', 'rollno', 'result', 'fee', 'applicant', 'semester', 'program', 'accountNumber', 'copies', 'feeSubmissionLastDate', 'gender', 'issuedDate', 'challan_dues', 'challan_security'))->setPaper('a4', 'landscape');
                return $pdf->download('Fee Challan.pdf');
            }
            if($doc == 'all'){
                $pdf = PDF::loadView('results.download-fee-challan', compact('result', 'resultId', 'rollno', 'result', 'fee', 'applicant', 'semester', 'program', 'accountNumber', 'copies', 'feeSubmissionLastDate', 'gender', 'issuedDate', 'challan_dues', 'challan_security'))->setPaper('a4', 'landscape');
                return $pdf;
            }

            return view('results.fee-challan', compact('result', 'resultId', 'rollno', 'result', 'fee', 'applicant', 'semester', 'program', 'accountNumber', 'copies', 'feeSubmissionLastDate', 'gender', 'issuedDate', 'challan_dues', 'challan_security'));
        }
    }
    public function getFeeChallanSecond($resultId, $rollno, $semester, $download, $feeSubmissionLastDate, $issuedDate)
    {
        // Dev Mannan: code for challan starts
        $challan_dues = ChallanDetail::where('fkRollNumber', $rollno)->where('challan_type', 4)->where('status', '!=', 0)->first();
        $challan_security = ChallanDetail::where('fkRollNumber', $rollno)->where('challan_type', 5)->where('status', '!=', 0)->first();
        // dd($challan_dues);
        // Dev Mannan: Challan Code ends
        $result = ResultList::with(['result', 'getRollno', 'result.program'])->where('rollno', $rollno)->where('fkResultId',$resultId)->first();
        $programId = $result->result->fkProgrammeId;
        if ($result && $programId) {
            $fee = ProgramFee::where('fkProgramId', $programId)->first();
            $applicant = RollNumber::with(['applicant', 'application', 'application.program', 'application.program.department', 'application.program.faculty', 'applicant.detail'])->find($rollno);
            $gender = $applicant->applicant->fkGenderId;
            $genderTitle = ($gender == 2) ? 'Female' : 'Male';
            $program = Programme::with(['faculty', 'department'])
                ->select('pkProgId', 'fkFacId', 'fkDepId', 'title')
                ->find($programId);
            //Get the applicant nationality. 
            $accountNumber = Account::where('fkFacId', $program->faculty->pkFacId)->where('bank_type', $genderTitle)->first();
            $copies = ['Bank Copy', 'Accounts Copy', 'Academics Copy', 'Student Copy'];
            if ($download) {
                $pdf = PDF::loadView('results.download-fee-challan-second', compact('result', 'resultId', 'rollno', 'result', 'fee', 'applicant', 'semester', 'program', 'accountNumber', 'copies', 'feeSubmissionLastDate', 'gender', 'issuedDate', 'challan_dues', 'challan_security'))->setPaper('a4', 'landscape');
                return $pdf->download('Fee Challan.pdf');
            }
            return view('results.fee-challan-second', compact('result', 'resultId', 'rollno', 'result', 'fee', 'applicant', 'semester', 'program', 'accountNumber', 'copies', 'feeSubmissionLastDate', 'gender', 'issuedDate', 'challan_dues', 'challan_security'));
        }
    }

    public function getList($id)
    {
       
        $resultList = ResultList::with(['getRollno', 'getRollno.application'])->where('fkResultId', $id)->get();
        $resultDetail = ResultList::with(['result', 'result.program'])->where('fkResultId', $id)->first();
        $arr = array();
        $secondChallan = '';
        foreach($resultList as $rr){
            array_push($arr , $rr->getRollno->application->id);
        }
        $secondChallan = ChallanDetail::whereIn('challan_type', array(4, 5))->whereIn('fkApplicationtId', $arr)->get();
        // dd($secondChallan);
        return view('results.list', compact('resultList', 'resultDetail', 'secondChallan'));
    }

    public function calculateMean($programId)
    {
        $resultMaleIds = Result::where('fkProgrammeId', $programId)->where('gender', 'Male')->get()->pluck('pkResultsId');
        $resultFemaleIds = Result::where('fkProgrammeId', $programId)->where('gender', 'Female')->get()->pluck('pkResultsId');

        $listOfMaleRollNumbers = ResultList::whereIn('fkResultId', $resultMaleIds)->get()->pluck('rollno');
        $listOfFemaleRollNumbers = ResultList::whereIn('fkResultId', $resultFemaleIds)->get()->pluck('rollno');

        $listMaleOfRollNumbersWithoutAlphbets = array_filter($listOfMaleRollNumbers->toArray(), function ($rollno) {
            $rollNo = (int)$rollno;
            if ($rollNo) return $rollNo;
        });
        $listFemaleOfRollNumbersWithoutAlphbets = array_filter($listOfFemaleRollNumbers->toArray(), function ($rollno) {
            $rollNo = (int)$rollno;
            if ($rollNo) return $rollNo;
        });

        $avgOfMaleMatricMarks = $this->calculateAverageOfSSCMarks($listMaleOfRollNumbersWithoutAlphbets);
        $avgOfFemaleMatricMarks = $this->calculateAverageOfSSCMarks($listFemaleOfRollNumbersWithoutAlphbets);

        $avgOfMaleIntermediateMarks = $this->calculateAverageOfHSSCMarks($listMaleOfRollNumbersWithoutAlphbets);
        $avgOfFemaleIntermediateMarks = $this->calculateAverageOfHSSCMarks($listFemaleOfRollNumbersWithoutAlphbets);
        dd(
            'SSC - Male',
            array_sum($avgOfMaleMatricMarks) / count($avgOfMaleMatricMarks),
            'SSC - Female',
            array_sum($avgOfFemaleMatricMarks) / count($avgOfFemaleMatricMarks),
            'HSSC - Male',
            array_sum($avgOfMaleIntermediateMarks) / count($avgOfMaleIntermediateMarks),
            'HSSC - Female',
            array_sum($avgOfFemaleIntermediateMarks) / count($avgOfFemaleIntermediateMarks)
        );
    }

    public function calculateAverageOfSSCMarks($rollNumbers)
    {
        $matricAverage = [];
        $allApplicantspreviousQualification = RollNumber::with(['application.applicant.previousQualification'])->whereIn('id', $rollNumbers)->get();
        foreach ($allApplicantspreviousQualification as $previousQualification) {
            $obtained = $previousQualification->application->applicant->previousQualification->SSC_Composite;
            $total = $previousQualification->application->applicant->previousQualification->SSC_Total;
            $matricAverage[] = $obtained / $total * 100;
        }
        return array_filter($matricAverage, function ($avg) {
            return $avg = (float)$avg;
        });
    }

    public function calculateAverageOfHSSCMarks($rollNumbers)
    {
        $intermediateAverage = [];
        $allApplicantspreviousQualification = RollNumber::with(['application.applicant.previousQualification'])->whereIn('id', $rollNumbers)->get();
        foreach ($allApplicantspreviousQualification as $previousQualification) {
            if ($previousQualification->application->applicant->previousQualification->HSSC_Composite) {
                $obtained = $previousQualification->application->applicant->previousQualification->HSSC_Composite;
                $total = $previousQualification->application->applicant->previousQualification->HSSC_Total;
                $intermediateAverage[] = $obtained / $total * 100;
            } else {
                if ($previousQualification->application->applicant->previousQualification->HSSC1 != '') {
                    $obtainedPart1 = $previousQualification->application->applicant->previousQualification->HSSC1;
                    $obtainedPart2 = $previousQualification->application->applicant->previousQualification->HSSC2;
                    $totalPart1 = $previousQualification->application->applicant->previousQualification->HSSC_Total1;
                    $totalPart2 = $previousQualification->application->applicant->previousQualification->HSSC_Total2;
                    $intermediateAverage[] = (((int)$obtainedPart1 + (int)$obtainedPart2) / ((int)$totalPart1 + (int)$totalPart2)) * 100;
                }
            }
        }
        return array_filter($intermediateAverage, function ($avg) {
            return $avg = (float)$avg;
        });
    }

    public function getDepartments($facultyId = '')
    {
        $departmentIds = Result::select('fkDeptId')->where('fkFacultyId', request('id'))->distinct('fkDeptId')->get();
        return Department::whereIn('pkDeptId', $departmentIds)->where('status', 1)->get();
    }

    public function getProgramme($departmentId = '')
    {
        $departmentIds = Result::select('fkProgrammeId')->where('fkDeptId', request('id'))->distinct('fkProgrammeId')->get();
        // return Programme::whereIn('pkProgId', $departmentIds)->where('status', 1)->get(); // Dev Mannan: remove status for engineering programmes
        return Programme::whereIn('pkProgId', $departmentIds)->get();
    }

    public function getSelectedDepartments($facultyId = '')
    {
        return Department::where('fkFacId', $facultyId)->where('status', 1)->get();
    }


    public function getSelectedProgramme($departmentId = '')
    {
        return Programme::where('fkDepId', $departmentId)->where('status', 1)->get();
    }
    public function instructions()
    {
        return view('results.instructions');
    }

    public function affidavitSpecimen()
    {
        $pathToFile = public_path('docs/undertaking_IIUI.pdf');
        return response()->download($pathToFile);
        // return view('results.affidavit-specimen');
    }
    public static function secondChallan($rollNumber = ''){
        return ChallanDetail::where('fkRollNumber', $rollNumber)->whereIn('challan_type', array(4,5))->get();
    }
//     public function arriersCalculation(){
//         $csv = Writer::createFromFileObject(new SplTempFileObject());
//         $csv->insertOne(['phone', 'Email']);
//         $currentDate = date('d-m-Y h:i:s A');
//         $fileName = 'Arrier-Challan-Data-'.$currentDate.'.csv';

//         $programFee = ProgramFee::get();
//         foreach($programFee as $pf){
//             $challanDetail = ChallanDetail::whereIn('challan_type', array(3))->where('fkProgramId', $pf->fkProgramId)->get();
//             // dd($challanDetail);
//             if($challanDetail){
//                 foreach($challanDetail as $cd){
//                     if($cd->challan_type == 3 && $cd->library_security != 5000){
//                         // create arriers due challan
//                         $challanInfo = $this->insertInfo(
//                             $cd->fkRollNumber, $cd->fkApplicationtId, $cd->fkSemesterId, $cd->fkProgramId, '4', '', '', $pf->dues_arriers, '', '', '', $pf->dues_arriers
//                         );
//                         // create arriers security challan
//                         $challanInfo = $this->insertInfo(
//                             $cd->fkRollNumber, $cd->fkApplicationtId, $cd->fkSemesterId, $cd->fkProgramId, '5', '', '', '', $pf->security_arriers, '', '', $pf->security_arriers
//                         );

                        
                        

//                         $csv->insertOne(
//                             [
//                                 $cd->application->applicant->applicantDetail->mobile ?? '',
//                                 $cd->application->applicant->email ?? ''
//                             ]
//                         );


//                     }
//                 }
//             }
//         }
//         // $csv->output($fileName);
//         file_put_contents(storage_path('app/abl/'.$fileName), $csv);
// =======
   
    public function arriersCalculation(){
        GenerateArriersChallan::dispatch();
        return 'Task Completed';
    }

    public function picMove(){
        PicReplace::dispatch();
        return 'Task Completed';
    }

    public function checkFeeEmail($id=''){
        $row_count = 0;
       
        $csv = Writer::createFromFileObject(new SplTempFileObject());
        $csv->insertOne(['Issue_date', 'Challan_No', 'ClassName', 'Bank_Account', 'Amount','Narration','Profit_Center','Std_Id','SP_Gl', 'Std_Name', 'Ins_No', 'Year', 'Due_date', 'Valid_date', 'Status']);
        $results = Result::where('gender', 'Female')->where('pkResultsId', $id)->get();
		//$results = Result::where('gender', 'Female')->where('type', 'Result')->where('fkSemesterId', '19')->where('pkResultsId', '>=', '1850')->whereNotIn('fkFacultyId', [3,9,10])->get(); // send all modified challans
		
		
        //dd($results); exit;
        foreach($results as $rs){
            $resultList = ResultList::select('rollno')->where('fkResultId',$rs->pkResultsId)->get()->pluck('rollno')->toArray();
            $challanDetail = ChallanDetail::whereIn('fkRollNumber', $resultList)->get();
            if($challanDetail && $challanDetail->count() > 0){
                foreach($challanDetail as $cd){
                    $row_count++;
                    $genderTitle = ($cd->application->applicant->fkGenderId == 2) ? 'Female' : 'Male';
                    $accountNumber = Account::where('fkFacId', $cd->program->faculty->pkFacId)->where('bank_type', $genderTitle)->first();
                    $challan_type = ($cd->challan_type == 2)? 'Dues': 'Security' ;
                    if($challan_type == 'Dues'){
                        $account = $accountNumber->dues_account;
                    }else{
                        $account = $accountNumber->security_account;
                    }
                    $program_name = str_replace(",", " ", $rs->program->title);
                    $csv->insertOne(
                        [
                            date_format(date_create($rs->created_at), 'Ymd') ?? '',
                            $cd->id ?? '',
                            $program_name ?? '',
                            $account ?? '',
                            $cd->total ?? '',
                            $rs->program->faculty->title ?? '',
                            '',
                            $cd->fkRollNumber ?? '',
                            '',
                            $cd->application->applicant->name ?? '',
                            '',
                            $cd->semester->title ?? '',
                            date_format(date_create($rs->endDate), 'Ymd') ?? '',
                            date_format(date_create($rs->endDate), 'Ymd') ?? '',
                            'New'
                        ]
                    );

                    ChallanDetail::where('id', $cd->id)
                    ->update([
                        'email_sent' => 1
                    ]);
                }
            }
            Result::where('pkResultsId', $rs->pkResultsId)
                ->update([
                    'email_sent' => 1
                ]);
        }
        if($row_count > 0){
            $currentDate = date('d-m-Y h:i:s A');
            // $message = "Report generated by Online Admission Software on $currentDate";
            // $csv->insertOne([$message]);
            $fileName = 'IIU-Admission-'.$currentDate.'.csv';
            file_put_contents(storage_path('app/abl/'.$fileName), $csv);
		
			$email_host = 'smtp.gmail.com';
            $email_port = '587';
            $email_encryption = 'tls';
            $email_username = 'fee.challans@iiu.edu.pk';
			$email_password = 'kqagwvocxrcaxxeq'; //App Password

            Config::set('mail.driver', 'smtp');
            Config::set('mail.host', $email_host);
            Config::set('mail.port', $email_port);
            Config::set('mail.encryption', $email_encryption);
            Config::set('mail.username', $email_username);
            Config::set('mail.password', $email_password);

             //echo $id." - Call Ok"; exit;
             
             //$to ='junaid.azhar@iiu.edu.pk';
			 $to ='T24SysAdmins@abl.com';
             $subject = 'IIU Admission Female Challan Data';
		
            try{
					Mail::send([], [], function ($message) use ($to, $subject, $email_username, $fileName) {
						$message->to($to)
						    ->cc(['nadia.hafeez@iiu.edu.pk', 'mariam.javed@iiu.edu.pk', 'fee.challans@iiu.edu.pk', 'zafar.kamran@abl.com'])
							->replyTo('fee.challans@iiu.edu.pk', 'IIU Admission Female Challan Data')
							->subject($subject);
						$message->from($email_username, 'IIU Admission Female Challan Data');
						$message->attach(storage_path('app/abl/'.$fileName));
					});
            echo "Executed... <br>";
			}
			Catch(\Exception $e){
				echo $e->getMessage();
				exit();
			}
			
        }
        // die();
        echo 'email has been send to specific list';
    }
}
