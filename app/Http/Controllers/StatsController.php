<?php

namespace App\Http\Controllers;

use App\Model\Stats;
use App\Model\Logger;
use App\Model\Semester;
use App\Models\Program;
use App\Model\Applicant;
use App\Model\ResultList;
use App\Model\Application;
use App\Model\ProgramStats;
use App\Model\ChallanDetail;
use App\Model\PreviousStats;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class StatsController extends Controller
{
    public function index()
    {
    	$stats = Stats::all();
    	$semesters = Semester::whereIn('pkSemesterId', [6, 8, 9])->get();
    	return view('stats.index', compact('stats', 'semesters'));
    }

    public function getSingelSemesterMeta() 
    {
    	$semesterId = request('id');
    	return Stats::where('semester_id', $semesterId)->get();
    }
    public function programStats(){
        $semesters = Semester::whereIn('pkSemesterId', [9, 8, 10, 11, 12, 13, 14, 15, 16, 17])->orderByRaw('FIELD(pkSemesterId,9,8,10,11,12,13,14,15,16,17)')->get();
        $Stats = ''; 
        $semester_from = '';
        $semester_to = '';
        $Stats1 = '';
        $department = '';
        $department1 = '';
        $faculty = '';
        $faculty1 = '';
        // $pds = request('pds');
        if(request('form_submit') == 1){
                $department = "SELECT ps.fkDepId as id, td.title as title, sum(applied_male) as applied_male, sum(applied_female) as applied_female, sum(fee_confirmed_male) as fee_confirmed_male, sum(fee_confirmed_female) as fee_confirmed_female, sum(selected_male) as selected_male, sum(selected_female) as selected_female, sum(joined_male) as joined_male, sum(joined_female) as joined_female, sum(applied_total) as applied_total, sum(fee_confirmed_total) as fee_confirmed_total, sum(selected_total) as selected_total, sum(joined_total) as joined_total
                FROM 
                tbl_oas_previous_stats as ps
                inner join 
                tbl_oas_department as td
                on ps.fkDepId=td.pkDeptId
                where fkSemesterId = ".request('semester_from')."
                Group By ps.fkDepId";
                $department = \DB::select($department);
                $department1 = "SELECT ps.fkDepId as id, td.title as title, sum(applied_male) as applied_male, sum(applied_female) as applied_female, sum(fee_confirmed_male) as fee_confirmed_male, sum(fee_confirmed_female) as fee_confirmed_female, sum(selected_male) as selected_male, sum(selected_female) as selected_female, sum(joined_male) as joined_male, sum(joined_female) as joined_female, sum(applied_total) as applied_total, sum(fee_confirmed_total) as fee_confirmed_total, sum(selected_total) as selected_total, sum(joined_total) as joined_total
                FROM 
                tbl_oas_previous_stats as ps
                inner join 
                tbl_oas_department as td
                on ps.fkDepId=td.pkDeptId
                where fkSemesterId = ".request('semester_to')."
                Group By ps.fkDepId";
                $department1 = \DB::select($department1);
                $faculty = "SELECT ps.fkFacId as id, td.title as title, sum(applied_male) as applied_male, sum(applied_female) as applied_female, sum(fee_confirmed_male) as fee_confirmed_male, sum(fee_confirmed_female) as fee_confirmed_female, sum(selected_male) as selected_male, sum(selected_female) as selected_female, sum(joined_male) as joined_male, sum(joined_female) as joined_female, sum(applied_total) as applied_total, sum(fee_confirmed_total) as fee_confirmed_total, sum(selected_total) as selected_total, sum(joined_total) as joined_total
                FROM 
                tbl_oas_previous_stats as ps
                inner join 
                tbl_oas_faculty as td
                on ps.fkFacId=td.pkFacId
                where fkSemesterId = ".request('semester_from')."
                Group By ps.fkFacId";
                $faculty = \DB::select($faculty);

                $faculty1 = "SELECT ps.fkFacId as id, td.title as title, sum(applied_male) as applied_male, sum(applied_female) as applied_female, sum(fee_confirmed_male) as fee_confirmed_male, sum(fee_confirmed_female) as fee_confirmed_female, sum(selected_male) as selected_male, sum(selected_female) as selected_female, sum(joined_male) as joined_male, sum(joined_female) as joined_female, sum(applied_total) as applied_total, sum(fee_confirmed_total) as fee_confirmed_total, sum(selected_total) as selected_total, sum(joined_total) as joined_total
                FROM 
                tbl_oas_previous_stats as ps
                inner join 
                tbl_oas_faculty as td
                on ps.fkFacId=td.pkFacId
                where fkSemesterId = ".request('semester_to')."
                Group By ps.fkFacId";
                $faculty1 = \DB::select($faculty1);

                $Stats = ProgramStats::where('fkSemesterId', request('semester_from'))->orderBy('fkFacId', 'ASC')->orderBy('fkDepId', 'ASC')->get();
                $Stats1 = ProgramStats::where('fkSemesterId', request('semester_to'))->orderBy('fkFacId', 'ASC')->orderBy('fkDepId', 'ASC')->get()->toArray();
                
            $semester_from = request('semester_from');
            $semester_to = request('semester_to');
            
            // dd($ProgramStats1);

        }
        return view('stats.program', compact('semesters', 'Stats', 'Stats1', 'semester_from', 'semester_to', 'department', 'department1', 'faculty', 'faculty1'));
    }
    public function refreshPreviousStats(){
        // Dev Mannan: code for current semester starts
        $semester = Semester::where('status', 1)->first();
        // dd($semester->pkSemesterId);
        PreviousStats::where('fkSemesterId', $semester->pkSemesterId)->delete();
        $programs = Program::get();
        $applications = Application::first();
        foreach($programs as $program){
            PreviousStats::create(
                [
                    'fkProgramId' => $program->pkProgId,
                    'fkFacId' => $program->fkFacId, 
                    'fkDepId' => $program->fkDepId,
                    'fkSemesterId' => $semester->pkSemesterId,
                    'status' => 1,
                    'applied_male' => $applications->GetOverAllStatusByGenderByProgram('>=', '1', '3', $program->pkProgId )->count(),
                    'applied_female' => $applications->GetOverAllStatusByGenderByProgram('>=', '1', '2', $program->pkProgId )->count(),
                    'fee_confirmed_male' => $applications->GetOverAllStatusByGenderByProgram('>=', '4', '3', $program->pkProgId )->count(),
                    'fee_confirmed_female' => $applications->GetOverAllStatusByGenderByProgram('>=', '4', '2', $program->pkProgId )->count(),
                    'selected_male' => $this->selectedStudents($program->pkProgId, 'Male'),
                    'selected_female' => $this->selectedStudents($program->pkProgId, 'Female'),
                    'updated_at' => now()
                ]
            );
        }
        return redirect()->route('program-stats');
    // Dev Mannan: code for current semester ends
    }
    public function selectedStudents($programId = '', $gender = ''){
        return $results = ResultList::with(['result'])->whereHas('result', function ($query) use ($programId, $gender) {
            $query->where('gender', $gender)->where('type', 'Result')->where('fkProgrammeId', $programId);
        })->count();
    }
    public function test(){
        dd($this->joinedStudents(33, 2));
    }
    public function joinedStudents($programId = '', $gender = ''){
        return $challanDetail = ChallanDetail::with(['application', 'application.applicant'])
        ->whereIn('challan_type', array(2, 3))
        ->where('status', 4)
        ->whereNotNull('fee_verified_date')
        ->where('fkProgramId', $programId)
        ->whereHas('application.applicant', function ( $query ) use ($gender) {
            $query->where('fkGenderId', $gender);
        })->groupBy('fkRollNumber')->count();
    }
    public function lateCandidates(){
        $applicants = Applicant::select(DB::raw('count(userId) as id, date(createdDtm) as date'))->whereDate('createdDtm', '>=', '2022-06-01')->groupBy(DB::raw('date(createdDtm)'))->get();
        
        // $logs = Logger::select(DB::raw('count(id) as id, date(created_at) as date'))->whereDate('created_at', '>=', '2020-06-04')->groupBy(DB::raw('date(created_at)'))->get();
        return view('stats.lateCandidates', compact('applicants'));
    }
}
