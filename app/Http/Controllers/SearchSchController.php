<?php

namespace App\Http\Controllers;

use Validator;
use Carbon\Carbon;
use App\Traits\PQM;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Model\{Logger, ApplicantDetail, Permission, UserLastLogin, ScholarshipDetail, Listscholarship, ScholarshipInformation, Applicant, ScholarshipFamilyDetail,Nationality,ApplicantAddress,City,Country,District,Province,Application,Faculty,Scholarship};

class SearchSchController extends Controller
{
    protected $currentUserPermissions;
    protected $facultyPermissions;
    protected $departmentPermissions;
    protected $genderPermissions;
    protected $nationalityPermissions;
    protected $selectedSemester;

    public function __construct()
    {
        // $this->selectedSemester = $this->getCurrentSemester()->pkSemesterId;
    }
    public function getFacultyTitle($id){

        $f= Faculty::where('pkFacId','=',$id)->first();
        if(!$f){
            return 'N/A '.$id;
        }
        return $f->title;
    }
    public function getScholorshipTitle($id){

        $f= Scholarship::where('id','=',$id)->first();
        if(!$f){
            return 'N/A '.$id;
        }
        return $f->name;
    }

    public function index(Request $request)
    {
        $this->assignPermissions();
        $listscholarship = Listscholarship::get();
        $sid=$request->sid;
        if(!$sid){
            $sid=0;
        }

        return view('scholarship.searchsch', compact('listscholarship'));
    }

    public function fetch(Request $req){
$sid=$req->sid;
        if(!$sid){
            $sid=0;
        }
        $col_order = ['name'];
        $total_data  = Scholarship::count();
        $limit = $req->input('length');
        $start = $req->input('start');
        $order = $col_order[$req->input('order.0.column')];
        $dir = $req->input('order.0.dir');
        if($sid!=0){
        $scholorshipName=$this->getScholorshipTitle($sid);
        }
        if(empty($req->input('search.value'))){
            $post = Applicant::whereIn('userId',ScholarshipDetail::where('fkSchlrId',$sid)->pluck('fkApplicantId')->toArray())->with(['ApplicantDetail', 'ScholarshipInformation', 'ScholarshipFamilyDetail', 'Application','ScholarshipDetail'])->offset($start)->limit($limit)
                ->orderBy($order,$dir)
                ->get();

            $total_filtered = Applicant::whereIn('userId',ScholarshipDetail::where('fkSchlrId',$sid)->pluck('fkApplicantId')->toArray())->with(['ApplicantDetail', 'ScholarshipInformation', 'ScholarshipFamilyDetail', 'Application','ScholarshipDetail'])->count();
        } else{
            $search = $req->input('search.value');
          

$post = Applicant::whereIn('userId',ScholarshipDetail::where('fkSchlrId',$sid)->pluck('fkApplicantId')->toArray())->with(['ApplicantDetail', 'ScholarshipInformation', 'ScholarshipFamilyDetail', 'Application','ScholarshipDetail'])->where('name','like',"%{$search}%")
                ->orWhere('name','like',"%{$search}%")->offset($start)
                ->limit($limit)
                ->orderBy($order,$dir)
                ->get();

            $total_filtered = Applicant::whereIn('userId',ScholarshipDetail::where('fkSchlrId',$sid)->pluck('fkApplicantId')->toArray())->with(['ApplicantDetail', 'ScholarshipInformation', 'ScholarshipFamilyDetail', 'Application','ScholarshipDetail'])->where('name','like',"%{$search}%")
                ->orWhere('name','like',"%{$search}%")->count();
        }

        $data = array();
        if($post){
            foreach ($post as $row){
                $nest=array();
                $nest[] = $scholorshipName;
                $nest[] = $row->gender->gender;
                $nest[] = $row->name;
                $nest[] = ($row->cnic)??'Nill';
                $nest[] = ($row->ApplicantDetail->fatherName)??'Nill';
                $nest[] = ($row->ApplicantDetail->father_cnic)??'Nill';
                $nest[] = ($row->ScholarshipInformation->reg)??'Nill';
                $nest[] = $this->getFacultyTitle($row->Application[0]->fkFacId);
                $data[] = $nest;
            }
        }

        $json = array(
            'draw'              =>  intval($req->input('draw')),
            'recordsTotal'      =>  intval($total_data),
            'recordsFiltered'   =>  intval($total_filtered),
            'data'              =>  $data
        );

        echo json_encode($json);
    }//end

    public function exportcsv(Request $request)
    {
       $sid=$request->sid;
        if(!$sid){
            $sid=0;
        }

        if($sid!=0){
        $scholorshipName=$this->getScholorshipTitle($sid);
        }

        $fileName = strtolower(str_replace(' ', '-', $scholorshipName)).'-'.date('d-m-Y').'-'.time().'.csv';
        $tasks =  Applicant::whereIn('userId',ScholarshipDetail::where('fkSchlrId',$sid)->pluck('fkApplicantId')->toArray())->with(['ApplicantDetail', 'ScholarshipInformation', 'ScholarshipFamilyDetail', 'Application','ScholarshipDetail'])
                ->get();

        $columns = array('Scholarship Name', 
            'Gender', 
            'Name', 
            'CNIC', 
            'Father Name', 
            'Father CNIC', 
            'Reg #', 
            'Faculty',
            'Father Death Date', 
            'Province',
            'District',
            'Deptt',
            'Program',
            'CGPA',
            'Email id of Student',
            'Contact No (Student)',
            'Contact No (Guardian)',
            'Last Degree / Certificate Passed',
            'Name of  Last Institution Studying',
            'Marks obatained in FA/Fsc',
            'Passed In  Year',
            'Per Month Fee of Last Institution\ college',
            'Father Status (Deceased OR Alive, Seperated)',
            'Other Supporting Person',
            'Father/Guardians Profession Status (Retired /Jobless/seperated)',
            'Total No of Family Members',
            'Total No. of Dependent Family Members',
            'Family Member Studying',
            'Earning Hands',
            'Father / Mother Guardian Income',
            'Mother Income',
            'Income from land',
            'Income from other sources',
            'Total Monthly Income',
            'Eelctricity',
            'Gas',
            'Telephone',
            'Water',
            'Family Expense on education/Semester',
            'Expense on Food',
            'Expense on Medical',
            'Total Monthly Expense',
            'Accomodation Ownership',
            'Accommodation/Rent Expense',
            'Accommodation & Location',
            'Accomodation Structure',
            'No of Vehicles',
            'Vehicle Type (Car OR Motorcycle OR NA)   Model of the Vehicle',
            'Vehicle Engine Capacity / CC',
            'Size of agricultural Land (in acres)',
            'Size of house Land (in Marlas)',
            'Live Stock/ No. of Cattles',
            'Monthly Pocket Money',
            'Reasons of applying',
            'How were admission charges',
            'Total outstanding/Unpaid dues (fee section Verfication)',
            'Financial Assistance earlier received (if any)/will be filled by Fee section also',
            'Recommended by the Committee (R/NR/W)',
            'Total Marks',
            'Recommended Financial Impact (Rs.)',

        );
        $callback = function() use ($columns, $tasks, $scholorshipName){ 
    $file = fopen('php://output', 'w');
                fputcsv($file, $columns,',');
                foreach ($tasks as $task) {
                    $row=array();
                    $row['Scholarship Name']  = $scholorshipName;
                    $row['Gender']    = $task->gender->gender;
                    $row['Name']    = $task->name;
                    $row['CNIC']  = ($task->cnic)??'Nill';
                    $row['Father Name']  = ($task->ApplicantDetail->fatherName)??'Nill';
                    $row['Father CNIC']  = ($task->ApplicantDetail->father_cnic)??'Nill';
                    $row['Reg #']  = ($task->ScholarshipInformation->reg)??'Nill';
                    $row['Faculty']  = $this->getFacultyTitle($task->Application[0]->fkFacId);
                    $row['Father Death Date']  = ($task->ScholarshipFamilyDetail->death_date)??'Nill';
                    $row['Province']  = ($task->applicantDetail->province->provName)??'Nill';
                    $row['District']  = ($task->address->cityPo)??'Nill';
                    $row['Deptt']  = ($task->ScholarshipFamilyDetail->death_date)??'Nill';
                    $row['Program']  = ($task->ScholarshipFamilyDetail->death_date)??'Nill';
                    $row['CGPA']  = ($task->ScholarshipInformation->cgpa)??'Nill';
                    $row['Email id of Student']  = ($task->email)??'Nill';
                    $row['Contact No (Student)']  = ($task->ApplicantDetail->mobile)??'Nill';
                    $row['Contact No (Guardian)']  = ($task->ApplicantDetail->fatherMobile)??'Nill';
                    $row['Last Degree / Certificate Passed']  = ($task->ScholarshipInformation->year_of_passing)??'Nill';
                    $row['Name of  Last Institution Studying']  = ($task->ScholarshipInformation->last_inst_attend)??'Nill';
                    $row['Marks obatained in FA/Fsc']  = ($task->ScholarshipInformation->marks_obtain)??'Nill';
                    $row['Passed In  Year']  = ($task->ScholarshipInformation->year_of_passing)??'Nill';
                    $row['Per Month Fee of Last Institution\ college']  = ($task->ScholarshipInformation->inst_fee)??'Nill';
                    $row['Father Status (Deceased OR Alive, Seperated)']  = ($task->ScholarshipFamilyDetail->death_date)??'Nill';
                    $row['Other Supporting Person']  = ($task->ScholarshipFamilyDetail->supporting_person)??'Nill';
                    $row['Father/Guardians Profession Status (Retired /Jobless/seperated)']  = ($task->ApplicantDetail->fatherOccupation)??'Nill';
                    $row['Total No of Family Members']  = ($task->ScholarshipFamilyDetail->death_date)??'Nill';
                    $row['Total No. of Dependent Family Members']  = ($task->ApplicantDetail->dependants)??'Nill';
                    $row['Family Member Studying']  = ($task->ScholarshipFamilyDetail->family_mem_studing)??'Nill';
                    $row['Earning Hands']  = ($task->ScholarshipFamilyDetail->family_mem_earning)??'Nill';
                    $row['Father / Mother Guardian Income']  = ($task->ApplicantDetail->monthlyIncome)??'Nill';
                    $row['Mother Income']  = ($task->ScholarshipFamilyDetail->mother_income)??'Nill';
                    $row['Income from land ']  = ($task->ScholarshipFamilyDetail->land_income)??'Nill';
                    $row['Income from other sources']  = ($task->ScholarshipFamilyDetail->misc_income)??'Nill';
                    $row['Total Monthly Income']  = ($task->ScholarshipFamilyDetail->monthly_income)??'Nill';
                    $row['Eelctricity']  = ($task->ScholarshipFamilyDetail->elect_bill)??'Nill';
                    $row['Gas']  = ($task->ScholarshipFamilyDetail->gas_bill)??'Nill';
                    $row['Telephone']  = ($task->ScholarshipFamilyDetail->tel_bill)??'Nill';
                    $row['Water']  = ($task->ScholarshipFamilyDetail->water_bill)??'Nill';
                    $row['Family Expense on education/Semester']  = ($task->ScholarshipFamilyDetail->edu_expense)??'Nill';
                    $row['Expense on Food']  = ($task->ScholarshipFamilyDetail->food_expense)??'Nill';
                    $row['Expense on Medical']  = ($task->ScholarshipFamilyDetail->medical_expense)??'Nill';
                    $row['Total Monthly Expense']  = ($task->ScholarshipFamilyDetail->monthly_expenditure)??'Nill';
                    $row['Accomodation Ownership']  = ($task->ScholarshipFamilyAssets->home_status)??'Nill';
                    $row['Accommodation/Rent Expense']  = ($task->ScholarshipFamilyAssets->home_value)??'Nill';
                    $row['Accommodation & Location']  = ($task->ScholarshipFamilyAssets->accommodation_land)??'Nill';
                    $row['Accomodation Structure']  = ($task->ScholarshipFamilyAssets->accommodation_type)??'Nill';
                    $row['No of Vehicles']  = ($task->ScholarshipFamilyAssets->vehicals)??'Nill';
                    $row['Vehicle Type (Car OR Motorcycle OR NA)   Model of the Vehicle']  = ($task->ScholarshipFamilyDetail->death_date)??'Nill';
                    $row['Vehicle Engine Capacity / CC']  = ($task->ScholarshipFamilyAssets->engine_cap)??'Nill';
                    $row['Size of agricultural Land (in acres)']  = ($task->ScholarshipFamilyAssets->land_size)??'Nill';
                    $row['Size of house Land (in Marlas)']  = ($task->ScholarshipFamilyAssets->acco_size)??'Nill';
                    $row['Live Stock/ No. of Cattles']  = ($task->ScholarshipFamilyAssets->no_of_cattles)??'Nill';
                    $row['Monthly Pocket Money']  = ($task->ScholarshipFamilyDetail->pocket_money)??'Nill';
                    $row['Reasons of applying']  = ($task->ScholarshipFamilyAssets->reason_for_apply)??'Nill';
                    $row['How were admission charges']  = ($task->ScholarshipFamilyAssets->source_of_financing)??'Nill';
                    $row['Total outstanding/Unpaid dues (fee section Verfication)']  = ($task->ScholarshipDetail->feeremarks)??'Nill';
                    $row['Financial Assistance earlier received (if any)/will be filled by Fee section also']  = ($task->ScholarshipDetail->condverremarks)??'Nill';
                    $row['Recommended by the Committee (R/NR/W)']  = ($task->ScholarshipFamilyDetail->death_date)??'Nill';
                    $row['Total Marks']  = ($task->ScholarshipInformation->total_marks)??'Nill';
                    $row['Recommended Financial Impact (Rs.)']  = ($task->ScholarshipFamilyDetail->death_date)??'Nill';

                    fputcsv($file,$row,',' );
                }fclose($file);
            };
             $headers = array(
            'Cache-Control'       => 'must-revalidate, post-check=0, pre-check=0'
                ,   'Content-type'        => 'text/csv'
                ,   'Content-Disposition' => 'attachment; filename='.$fileName
                ,   'Expires'             => '0'
                ,   'Pragma'              => 'public'
            );
            return response()->stream($callback, 200, $headers);
    }
        
    
    
    public function assignPermissions()
    {
        $this->currentUserPermissions = $this->getCurrentUserPermissions();
        $this->facultyPermissions = explode(',', $this->currentUserPermissions->fkFacultyId);
        $this->departmentPermissions = explode(',',$this->currentUserPermissions->fkDepartmentId);
        $this->genderPermissions = $this->currentUserPermissions->fkGenderId;
        $this->nationalityPermissions = explode(',',$this->currentUserPermissions->fkNationality);
    }

    public function checkIfHasFullAccess() {
        if 
        (  count ( $this->facultyPermissions ) == 9 && 
            $this->departmentPermissions[0] == '' && 
            $this->genderPermissions == 1 && 
            (count( $this->nationalityPermissions) == 3 || 
            $this->nationalityPermissions[0] == '' ||  $this->nationalityPermissions[0] == 'NULL') 
        )
            return true;
        return false;
    }

    public function checkIfHasLimitedAccess( $applications )
    {
        $filtered = $applications->filter(function ( $value, $key ) {
            return $this->filterApplication ( $value );
        });
        return $filtered;
    }

    public function filterApplication($application)
    {
        /* Apply gender & overseas permissions to incomplete applications */
        $gender = $application->applicant->fkGenderId;            
        $nationality = $application->applicant->fkNationality;
        if ( $application->fkCurrentStatus == 1 ) {
            if ( 
                ($this->genderPermissions == 1 || $gender == $this->genderPermissions) &&
                (empty( $this->nationalityPermissions[0] ) || in_array( $nationality, $this->nationalityPermissions ) || $this->nationalityPermissions[0] == 'NULL')
            )
                return true;
        }
        // $program = Programme::where('pkProgId', $application->fkProgramId)->select('fkFacId', 'fkDepId')->first();
        $facultyId = $application->program->fkFacId ?? '';
        $departmentId = $application->program->fkDepId ?? '';    
       
        if ( in_array($facultyId, $this->facultyPermissions) ) {
            if ( empty($this->departmentPermissions[0]) || in_array ( $departmentId, $this->departmentPermissions) ) {
                if ( $this->genderPermissions == 1 || $gender == $this->genderPermissions ) {
                    if ( empty( $this->nationalityPermissions[0] ) || in_array( $nationality, $this->nationalityPermissions ) || $this->nationalityPermissions[0] == 'NULL' )
                        return true;
                }
            }
        }
        return false;
    }



    public function checkAccessLevelAndGetApplication( $applications ) 
    {
        return ( $this->checkIfHasFullAccess() ) ? $applications : $this->checkIfHasLimitedAccess($applications);   
    }

    public function paginateResults($applications, $paginate)
    {
        $currentPage = LengthAwarePaginator::resolveCurrentPage();
        $currentPageResults = $applications->slice(($currentPage-1) * $paginate, $paginate)->all();
        $result = new LengthAwarePaginator($currentPageResults, count($applications), $paginate);
        $result->setPath(request()->url());
        return $result;
    }


    public function superadmin(Request $request, Application $application)
    {
        $application = Application::where('id', $request->id)->first();
        $logs = UserLastLogin::where('userId', $application->fkApplicantId)->get();
        // dump($logs);
        $fee_verify = 'NULL';
        $docverify = 'NULL';
        if ($application->docsVerifyAdmin) {
            $docverify = $application->docsVerifyAdmin->username;
        }
        if ($application->user) {
            $fee_verify = $application->user->username;
        }
        return response()->json(['application' => $application, 'fee_verified_by' => $fee_verify, 'documents_verified_by' => $docverify, 'logs'=>$logs]);
    }

    public function superadminid(Request $request){
        if($request->id){
            $users = \DB::table('tb_users')->where('id','=',$request->id)->get()->first();
            return response()->json(['username'=>$users->username]);
        }
    }
}

