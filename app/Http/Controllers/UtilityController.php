<?php

namespace App\Http\Controllers;

use App\Model\Applicant;
use App\Model\FailedJobs;
use App\Jobs\FetchRecords;
use App\Model\Application;
use Illuminate\Http\Request;
use App\Mail\FetRequestDocuments;
use App\Mail\EmailNotificationToFll;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Artisan;

class UtilityController extends Controller
{

    /**
     * Get the list of applicants who was submitted the application
     * but not submitted there fee
     * @return [array] [Phone numbers for SMS campaign]
     */
    public function getPhoneOfStatus2()
    {
        FetchRecords::dispatch();
        return 'Job Processed';
    }

    public function addDepartmentAndFacultyWithProgramInApplicationsTable()
    {
        $applications = Application::with(['program:pkProgId,title,fkDepId,fkFacId', 'program.department:pkDeptId,fkFacId', 'program.faculty:pkFacId'])
            ->select('id', 'fkProgramId')
            ->whereNotNull('fkProgramId')
            ->where('fkProgramId', '!=', 0)
            ->get();
        
        foreach ($applications as $application) {
            $application->fkFacId = $application->program->faculty->pkFacId;
            $application->fkDepId = $application->program->department->pkDeptId;
            $application->save();
        }

        return 'Update Complete.';
    }

    public function reQueueFailedJobs()
    {
        $jobs = FailedJobs::all();
        foreach ($jobs as $job) {
            Artisan::call('queue:retry', ['id' => $job->id]);
            $job->delete();
        }
        return 'done';
    }

    /**
     * Fall-2020 requirement to send email to FET students 
     *
     * @return void
     */
    public function sendEmailToFet()
    {
        $applications = Application::with(['applicant'])
            ->where('fkFacId', 3)
            ->whereIn('fkCurrentStatus', [4,5])
            ->get();
        foreach ( $applications as $application ) {
            Mail::to($application->applicant->email)
                ->queue(new FetRequestDocuments());
        }
        return 'Done';
    }

    public function emailNotificationToFll()
    {
        $applications = Application::with(['applicant'])
            ->where('fkFacId', 5)
            ->whereIn('fkCurrentStatus', [4,5])
            ->get();
        foreach ( $applications as $application ) {
            Mail::to($application->applicant->email)
                ->queue(new EmailNotificationToFll());
        }
        return 'Done';
    }
}
