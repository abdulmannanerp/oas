<?php

namespace App\Http\Controllers\Frontend;
use App\Model\Applicant;


use App\Model\ApplicantDetail;
use App\Model\ApplicationStatus;
use App\Http\Controllers\Controller;
use App\Model\PreviousQualification;

class AptitudeTestController extends Controller{
    protected $currentSemester;
    public function __construct()
    {
        $this->currentSemester = $this->getCurrentSemester()->pkSemesterId;
    }


    public function index ($hash = ''){
        $adminId = $this->decodeAdminData($hash);
        $status = request()->session()->get('status');
        if ( request()->method() == 'POST' ) {
            $applicationStatus = ApplicationStatus::where('fkApplicantId', auth()->id())->where('fkSemesterId', $this->currentSemester)->first();
            if($applicationStatus->step_save == 1 && $adminId == ''){
                return redirect()->route('familydetail');
            }
            if(request('type_1') || request('year_1') || request('obtained_1') || request('total_1') || request('type_2') || request('year_2') || request('obtained_2') || request('total_2')|| request('type_3') || request('year_3') || request('obtained_3') || request('total_3')|| request('type_4') || request('year_4') || request('obtained_4') || request('total_4')){
                PreviousQualification::where('fkApplicantId', request('userId'))->where('fkSemesterId', $this->currentSemester)->update(
                    [
                        'APT_Test_1' => request('type_1'),
                        'APT_Test_1_Year' => request('year_1'),
                        'APT_Test_1_Marks' => request('obtained_1'),
                        'APT_Test_1_Total_Marks' => request('total_1'),

                        'APT_Test_2' => request('type_2'),
                        'APT_Test_2_Year' => request('year_2'),
                        'APT_Test_2_Marks' => request('obtained_2'),
                        'APT_Test_2_Total_Marks' => request('total_2'),

                        'APT_Test_3' => request('type_3'),
                        'APT_Test_3_Year' => request('year_3'),
                        'APT_Test_3_Marks' => request('obtained_3'),
                        'APT_Test_3_Total_Marks' => request('total_3'),

                        'APT_Test_4' => request('type_4'),
                        'APT_Test_4_Year' => request('year_4'),
                        'APT_Test_4_Marks' => request('obtained_4'),
                        'APT_Test_4_Total_Marks' => request('total_4')
                    ]
                );
                ApplicationStatus::where('fkApplicantId', request('userId'))->where('fkSemesterId', $this->currentSemester)->update(
                    [
                        'step_aptitude'=> 1                    
                    ]
                );
                if($adminId != ''){
                    PreviousQualification::where('fkApplicantId', request('userId'))->where('fkSemesterId', $this->currentSemester)->update(
                        [
                            'modifiedBy' => $adminId
                        ]
                    );
                }
            }
            $this->logMaintenance($hash, request('userId'), 'Aptitude Test');
            return redirect()->route('familydetail', $hash);
            
            // return back()->with('status', 'Information updated successfully!');
        }else{
            $previousQualification = PreviousQualification::where('fkApplicantId', auth()->id())->where('fkSemesterId', $this->currentSemester)->first();
            // $applicationStatus = ApplicationStatus::where('fkApplicantId', auth()->id())->first();
            $userdetail = Applicant::where('userId',auth()->id())->first();
            $applicantDetail = ApplicantDetail::where('fkApplicantId', auth()->id())->first();
            return view('frontend.aptitudeTest', compact('status','hash','applicantDetail','userdetail', 'previousQualification'));
        }
    }
}
