<?php

namespace App\Http\Controllers\Frontend;
use App\Model\oasEmail;


use App\Model\Applicant;
use App\Model\ApplicantDetail;
use App\Model\ApplicationStatus;
use App\Http\Controllers\Controller;
use Carbon\Carbon;

class UpdatePasswordController extends Controller{
    public function index ($hash = ''){
        $adminId = $this->decodeAdminData($hash);
        $status = request()->session()->get('status');
        if ( request()->method() == 'POST' ) {
            $this->validate(request(), [
                'password' => 'required|min:6|confirmed'
            ]);
            $userdetail = Applicant::where('userId',request('userId'))->first();
            Applicant::where('userId', request('userId'))->update(
                [
                    'password' => bcrypt(request('password'))
                ]
            );
            if($userdetail->fkGenderId == 3){
                $abbr = 'AdmissionMale';

            }elseif($userdetail->fkGenderId == 2){
                $abbr = 'AdmissionFemale';
            }
            $data = array('name' => $userdetail->name, 'email' => $userdetail->email, 'password' => request('password'));
            oasEmail::email($abbr, $data, $userdetail->email, 'Password Change Notification', 'frontend.UpdatePasswordEmail');
            if($adminId != ''){
                Applicant::where('userId', request('userId'))->update(
                    [
                        'updatedBy' => $adminId,
                        'updatedDtm' => Carbon::now()                   
                    ]
                );
            }
            return back()->with('status', 'Password updated successfully!');
        }else{
            $applicationStatus = ApplicationStatus::where('fkApplicantId', auth()->id())->first();
            $userdetail = Applicant::where('userId',auth()->id())->first();
            $applicantDetail = ApplicantDetail::where('fkApplicantId', auth()->id())->first();
            return view('frontend.updatePassword', compact('status','hash','applicantDetail','applicationStatus','userdetail'));
        }
    }
}
