<?php

namespace App\Http\Controllers\Frontend;
use App\Model\oasEmail;


use App\Model\Applicant;
use App\Model\Application;
use App\Model\ApplicantDetail;
use App\Model\ApplicationStatus;
use App\Http\Controllers\Controller;
use App\Model\ChallanGrouping;
use App\Model\ChallanDetail;

class SaveDetailController extends Controller{
    protected $currentSemester;
    public function __construct()
    {
        $this->currentSemester = $this->getCurrentSemester()->pkSemesterId;
    }
    public function index ($hash = ''){
        // $this->programGrouping(request('userId'));
        // exit;
        
        $adminId = $this->decodeAdminData($hash);
        $custom_error = request()->session()->get('custom_error');
        if ( request()->method() == 'POST' ) {
        $applicationStatus = ApplicationStatus::where('fkApplicantId', auth()->id())->where('fkSemesterId', $this->currentSemester)->first();
        if($applicationStatus->step_save  == 1){
            return redirect()->route('savedetail');
        }
        $this->validate(request(), [
            'save' => 'required'
        ]);
        $applicationForms = ApplicationStatus::where('fkApplicantId', request('userId'))->where('fkSemesterId', $this->currentSemester)->first();
        if($applicationForms->step_personal != 1 || $applicationForms->step_education != 1 || $applicationForms->step_programs != 1 || $applicationForms->step_other != 1 ){
        // if($applicationForms->step_personal != 1 || $applicationForms->step_education != 1 || $applicationForms->step_programs != 1 || $applicationForms->step_family != 1 || $applicationForms->step_other != 1){
            return back()->with('custom_error', 'Application Submittion failed. Please Compelete all mandatory Forms');
        }
        $userdetail = Applicant::where('userId', request('userId'))->first();
        if($userdetail->fkLevelId == 1 && $applicationForms->step_preference != 1){
            return back()->with('custom_error', 'In case of Undergraduate, you must fill preferences');
        }        

        Application::where('fkApplicantId', request('userId'))->where('fkSemesterId', $this->currentSemester)->where('a_status', 1)->update(
            [
                'fkCurrentStatus' => 2
            ]
        );
        ApplicationStatus::where('fkApplicantId', request('userId'))->where('fkSemesterId', $this->currentSemester)->update(
            [
                'step_save'=> 1                    
            ]
        );
        $userdetail = Applicant::where('userId', request('userId'))->first();
        // dd($userdetail->fkGenderId);
        if($userdetail->fkGenderId == 3){
            $abbr = 'AdmissionMale';

        }elseif($userdetail->fkGenderId == 2){
            $abbr = 'AdmissionFemale';
        }
        $data = array('name' => $userdetail->name);
        $data = [
            'name' => $userdetail->name,
            'fkNationality' => $userdetail->fkNationality
        ];

        // Dev Mannan: Code for challan grouping generation starts
        if($userdetail->fkLevelId == 1){
            $this->programGrouping(request('userId'));
        }
        // Dev Mannan: Code for challan grouping generation ends

        oasEmail::email($abbr, $data, $userdetail->email, 'IIU Admission Office', 'frontend.SubmitApplicationEmail');
        $this->logMaintenance($hash, request('userId'), 'Save');
        return redirect()->route('application', $hash);
        }else{
            // $applicationStatus = ApplicationStatus::where('fkApplicantId', auth()->id())->first();
            $userdetail = Applicant::where('userId',auth()->id())->first();
            $applicantDetail = ApplicantDetail::where('fkApplicantId', auth()->id())->first();
            return view('frontend.saveDetail', compact('custom_error','hash','applicantDetail','userdetail'));
        }
    }

    public function programGrouping($userId = '')
    {

        $applied_applications = Application::where('fkApplicantId', $userId)->where('fkSemesterId', $this->currentSemester)->whereNotNull('fkProgramId')->where('a_status', 1)->get();
        $dataArray = [];
        // dump($applied_applications);
        $facultyIds = array();
        foreach ($applied_applications as $a) {

            $dataArray[] = ['pk' => $a->id, 'programId' => $a->fkProgramId, 'facultyId' => $a->fkFacId, 'fkApplicantId' => $a->fkApplicantId];
            $facultyIds[] = $a->fkFacId;
        }

        // pick unique faculty Id's from array
        $uniqueFids = array_unique($facultyIds);

        // putting all the programs to their faculty groups
        $separateFacultyPrograms = [];
        foreach ($uniqueFids as $u) {
            $a = array();
            $b = array();

            $fkapplicatId = '';
            $looping = 1;
            // Making 3 grouping on basis of faculty ID
            foreach ($dataArray as $h) {

                if ($h['facultyId'] == $u) {
                    array_push($a, $h['programId']);
                    array_push($b, $h['pk']);
                    $fkapplicatId = $h['fkApplicantId'];
                    if ($looping == 3 || $looping == 6 || $looping == 9 || $looping == 12 || $looping == 15 || $looping == 18 || $looping == 21) {
                        $separateFacultyPrograms[] = ['applicantId' => $fkapplicatId, 'facultyId' => $u, 'applicationId' => $b, 'allProgramIds' => $a];
                        $a = array();
                        $b = array();
                    }

                    $looping++;
                } else {
                    $looping = 1;
                }
            }
            if (!count($a) == 0) {
                $separateFacultyPrograms[] = ['applicantId' => $fkapplicatId, 'facultyId' => $u, 'applicationId' => $b, 'allProgramIds' => $a];
            }
        }
        // dump($dataArray);

        // dump($separateFacultyPrograms);


        foreach ($separateFacultyPrograms as $s) {
            // dump($s);

            $data = [];
            $data['fkApplicantId'] = $s['applicantId'];
            $data['fkFacId'] = $s['facultyId'];
            $data['fkSemesterId'] = $this->currentSemester;

            $i = 1;
            foreach ($s['applicationId'] as $a) {

                // dump($a);
                $data['applicationId_' . $i] = $a;
                $i++;
                // array_push($data, array('applicationId_'.$i => $a));
            }
            $j = 1;
            foreach ($s['allProgramIds'] as $a) {
                $data['programId_' . $j] = $a;
                $j++;
            }

            // 
            $record = ChallanGrouping::create($data);
            $challanDetail = ChallanDetail::where('fkApplicationtId', $record->applicationId_1)->where('fkSemesterId', $this->currentSemester)->where('fkProgramId', $record->programId_1)->where('challan_type', 1)->first();
            ChallanGrouping::where('id', $record->id)->update(
                [
                    'challanId' => $challanDetail->id
                ]
            );


            // dump($s['allProgramIds']);
            // exit;
            // $data_1 = ChallanGrouping::where('fkApplicantId', $data['fkApplicantId'])->whereIn('programId_1', $s['allProgramIds'])->first();
            // $data_2 = ChallanGrouping::where('fkApplicantId', $data['fkApplicantId'])->whereIn('programId_2', $s['allProgramIds'])->first();
            // $data_3 =  ChallanGrouping::where('fkApplicantId', $data['fkApplicantId'])->whereIn('programId_3', $s['allProgramIds'])->first();
            // if (!$data_1 && !$data_2 && !$data_3) {

            // }
        }
        // dump($data);

        // ChallanGrouping::
    }
}
