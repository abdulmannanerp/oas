<?php

namespace App\Http\Controllers\Frontend;

use Imagick;
use Carbon\Carbon;
use App\Model\Degree;
use App\Model\Semester;
use App\Model\Applicant;
use App\Model\RollNumber;
use App\Model\ResultList;
use App\Model\Institute;
use App\Model\ApplicantDetail;
use App\Model\ApplicationStatus;
use App\Http\Controllers\Controller;
use App\Model\PreviousQualification;
use App\Model\Application;
use Auth;

class PreviousQualificationController extends Controller
{
    protected $currentSemester;
    public function __construct()
    {
        $this->currentSemester = $this->getCurrentSemester()->pkSemesterId;
    }

    public function index($hash = '')
    {
        $adminId = $this->decodeAdminData($hash);
        $custom_error = request()->session()->get('custom_error');
        $semester = Semester::where('status', 1)->first();
        if (request()->method() == 'POST') {
            // Code for Law admission score starts
            // $preQuali = PreviousQualification::where('fkApplicantId', request('userId'))->where('fkSemesterId', $this->currentSemester)->first();
            // $updated_data = PreviousQualification::where('fkApplicantId', request('userId'))->where('fkSemesterId', $this->currentSemester)->update(
            //     [
            //         'llb_total_marks' => trim(request('llb_total_marks')),
            //         'llb_obtained_marks' => trim(request('llb_obtained_marks')),
            //         'law_completion_date' => (trim(request('law_completion_date')) == '')? NULL : Carbon::parse(request('law_completion_date')),
            //         'lat_upcoming' => request('lat_upcoming')
            //     ]
            // );
            // if($updated_data == 1){
            //     $updated_data = PreviousQualification::where('fkApplicantId', request('userId'))->where('fkSemesterId', $this->currentSemester)->first();               
            // } 
            // $this->logMaintenance($hash, request('userId'), 'Previous Qualification', $preQuali, $updated_data);
            // return back()->with('message','Marks Saved Successfully !');
            // die();
            // Code for Law admission score ends
            $applicationStatus = ApplicationStatus::where('fkApplicantId', auth()->id())->where('fkSemesterId', $this->currentSemester)->first();
            $preQuali = PreviousQualification::where('fkApplicantId', request('userId'))->where('fkSemesterId', $this->currentSemester)->first();
            if ($applicationStatus->step_save == 1 && $adminId == '') {
                return redirect()->route('selectprogram');
            }
            $this->validate(request(), [
                'SSC_Total' => 'required|numeric',
                'SSC_Composite' => 'required|numeric',
                'SSC_Year' => 'required',
                'SSC_degree_type' => 'required'
            ]);
            // Code for uploading matric transcript
            if (!$preQuali->matric_last_result) {
                $this->validate(request(), [
                    'matric_last_result' => 'required|image|mimes:jpeg,png,jpg,gif,bmp,tiff|max:4096'
                ]);
            }

            if (request()->hasFile('matric_last_result')) {
                $image = request()->file('matric_last_result');
                $image_ext = strtolower($image->getClientOriginalExtension());
                if ($image_ext == 'jpeg' || $image_ext == 'png' || $image_ext == 'jpg' || $image_ext == 'gif' || $image_ext == 'bmp' || $image_ext == 'tiff') {
                } else {
                    return back()->with('custom_error', 'Matric Result uploaded File format should have [jpeg/png/jpg/gif/bmp/tiff] only with Max Size (4MB)')->withInput();
                }
                $filename    = request('userId') . '.' . $image->getClientOriginalExtension();
                $im = new Imagick($image->getRealPath());
                $rand = rand(pow(10, 6 - 1), pow(10, 6) - 1);
                // file_put_contents(storage_path('app/public/images/'.$semester->title.'/phd/'.$rand.'_'.$filename), $image);
                $image->move(
                    storage_path('app/public/images/' . $semester->title . '/matric/'),
                    $rand . '_' . $filename
                );
                // $im->writeImage(storage_path('app/public/images/'.$semester->title.'/phd/'.$rand.'_'.$filename));
                PreviousQualification::where('fkApplicantId', request('userId'))->where('fkSemesterId', $this->currentSemester)->update(
                    [
                        'matric_last_result' => 'images/' . $semester->title . '/matric/' . $rand . '_' . $filename
                    ]
                );
            }

            // Code for BS program only Uploads starts

            if (request()->hasFile('entry_test_sheet')) {
                $image = request()->file('entry_test_sheet');
                $image_ext = strtolower($image->getClientOriginalExtension());
                if ($image_ext == 'jpeg' || $image_ext == 'png' || $image_ext == 'jpg' || $image_ext == 'gif' || $image_ext == 'bmp' || $image_ext == 'tiff') {
                } else {
                    return back()->with('custom_error', 'Result ard/sheet Transcript uploaded File format should have [jpeg/png/jpg/gif/bmp/tiff] only with Max Size (4MB)')->withInput();
                }
                $filename    = request('userId') . '.' . $image->getClientOriginalExtension();
                $im = new Imagick($image->getRealPath());
                $rand = rand(pow(10, 6 - 1), pow(10, 6) - 1);
                // file_put_contents(storage_path('app/public/images/'.$semester->title.'/phd/'.$rand.'_'.$filename), $image);
                $image->move(
                    storage_path('app/public/images/' . $semester->title . '/result_card/'),
                    $rand . '_' . $filename
                );
                // $im->writeImage(storage_path('app/public/images/'.$semester->title.'/phd/'.$rand.'_'.$filename));
                PreviousQualification::where('fkApplicantId', request('userId'))->where('fkSemesterId', $this->currentSemester)->update(
                    [
                        'entry_test_sheet' => 'images/' . $semester->title . '/result_card/' . $rand . '_' . $filename
                    ]
                );
            }

            // Code for BS program only Uploads ends

            // Code for uploading HSSC Transcript
            if (request('resultAwaiting') == 0 && !$preQuali->hssc_last_result) {
                $this->validate(request(), [
                    'hssc_last_result' => 'required|image|mimes:jpeg,png,jpg,gif,bmp,tiff|max:4096'
                ]);
            }

            if (request()->hasFile('hssc_last_result')) {
                $image = request()->file('hssc_last_result');
                $image_ext = strtolower($image->getClientOriginalExtension());
                if ($image_ext == 'jpeg' || $image_ext == 'png' || $image_ext == 'jpg' || $image_ext == 'gif' || $image_ext == 'bmp' || $image_ext == 'tiff') {
                } else {
                    return back()->with('custom_error', 'HSSC Result uploaded File format should have [jpeg/png/jpg/gif/bmp/tiff] only with Max Size (4MB)')->withInput();
                }
                $filename    = request('userId') . '.' . $image->getClientOriginalExtension();
                $im = new Imagick($image->getRealPath());
                $rand = rand(pow(10, 6 - 1), pow(10, 6) - 1);
                // file_put_contents(storage_path('app/public/images/'.$semester->title.'/phd/'.$rand.'_'.$filename), $image);
                $image->move(
                    storage_path('app/public/images/' . $semester->title . '/hssc/'),
                    $rand . '_' . $filename
                );
                // $im->writeImage(storage_path('app/public/images/'.$semester->title.'/phd/'.$rand.'_'.$filename));
                PreviousQualification::where('fkApplicantId', request('userId'))->where('fkSemesterId', $this->currentSemester)->update(
                    [
                        'hssc_last_result' => 'images/' . $semester->title . '/hssc/' . $rand . '_' . $filename
                    ]
                );
            }
            // HSSC validation starts
            if (request('combined_hssc') == 1) {
                $this->validate(request(), [
                    'HSSC_Total' => 'required|numeric',
                    'HSSC_Composite' => 'required|numeric',
                    'HSC_Year' => 'required',
                    'HSC_degree_type' => 'required'
                ]);
            }
            if (request('resultAwaiting') == 1) {
                $this->validate(request(), [
                    'HSC_Year' => 'required',
                    'HSC_degree_type' => 'required'
                ]);
                // if(request('HSC_Title_d') != 'A-Level'){
                //     $this->validate(request(), [
                //         'HSSC_Total1' => 'required|numeric',
                //         'HSSC1' => 'required|numeric'
                //     ]);
                // }
            }
            if (request('resultAwaiting') != 1 && request('combined_hssc') != 1) {
                $this->validate(request(), [
                    'HSSC_Total2' => 'required|numeric',
                    'HSSC2' => 'required|numeric',
                    'HSC_Year' => 'required',
                    'HSC_degree_type' => 'required'
                ]);
                if (request('HSC_Title_d') != 'A-Level' && request('resultAwaiting') != '1') {
                    $this->validate(request(), [
                        'HSSC_Total1' => 'required|numeric',
                        'HSSC1' => 'required|numeric'
                    ]);
                }
            }


            // HSSC validation ends
            // in case of other selection text box must be filled
            if ((trim(request('SSC_Title_t')) == '' && request('SSC_Title_d') == 'Other')  ||
                (trim(request('SSC_From_t')) == '' && request('SSC_From_d') == 'Other Boards') ||
                (trim(request('SSC_Subject_t')) == '' && request('SSC_Subject_d') == 'Other') ||

                (trim(request('HSC_Title_t')) == '' && request('HSC_Title_d') == 'Other') ||
                (trim(request('HSC_From_t')) == '' && request('HSC_From_d') == 'Other Boards') ||
                (trim(request('HSC_Subject_t')) == '' && request('HSC_Subject_d') == 'Other') ||

                (trim(request('BA_Bsc_Title_t')) == '' && request('BA_Bsc_Title_d') == 'Other') ||
                (trim(request('BA_Bsc_From_t')) == '' && request('BA_Bsc_From_d') == 'Other Universities') ||

                (trim(request('MSc_Title_t')) == '' && request('MSc_Title_d') == 'Other') ||
                (trim(request('MSc_From_t')) == '' && request('MSc_From_d') == 'Other Universities') ||

                (trim(request('BS_Title_t')) == '' && request('BS_Title_d') == 'Other') ||
                (trim(request('BS_From_t')) == '' && request('BS_From_d') == 'Other Universities') ||

                (trim(request('MS_Title_t')) == '' && request('MS_Title_d') == 'Other') ||
                (trim(request('MS_From_t')) == '' && request('MS_From_d') == 'Other Universities')
            ) {
                return back()->with('custom_error', 'In case of Other you need to specify your degree/board/group')->withInput();
            }
            // Master (MA/MSc/LLB (3 years) MBA (3.5/2 years)) validation
            // Diploma (IB&F, ID in MH&HRL) 1 year validation
            if (request('userLevel') == 2 || request('userLevel') == 3) {
                if (request('resultwaitbsc') == '1') {
                    // Dev Mannan: Following line commented and add new for covid 19 
                    // if((trim(request('BA_Bsc_Title_t')) == '' && request('BA_Bsc_Title_d') == '') || (trim(request('BA_Bsc_From_t')) == '' && request('BA_Bsc_From_d') == '') ||  (trim(request('BA_Bsc_Subject')) == '' || trim(request('BA_Bsc_Specialization')) == '')){
                    if ((trim(request('BA_Bsc_Title_t')) == '' && request('BA_Bsc_Title_d') == '') || (trim(request('BA_Bsc_From_t')) == '' && request('BA_Bsc_From_d') == '') ||  (trim(request('BA_Bsc_Subject')) == '' || trim(request('BA_Bsc_Specialization')) == '' || trim(request('BA_Bsc_Total')) == '' || trim(request('BA_Bsc')) == '')) {
                        return back()->with('custom_error', 'Undergraduate(14-Year) Section is compulsory')->withInput();
                    }
                } else {
                    if ((trim(request('BA_Bsc_Title_t')) == '' && request('BA_Bsc_Title_d') == '') || (trim(request('BA_Bsc_From_t')) == '' && request('BA_Bsc_From_d') == '') ||  (trim(request('BA_Bsc_Subject')) == '' || trim(request('BA_Bsc_Specialization')) == '' || trim(request('BA_Bsc_Total')) == '' || trim(request('BA_Bsc')) == '')) {
                        return back()->with('custom_error', 'Undergraduate(14-Year) Section is compulsory')->withInput();
                    }
                }
            }
            // MS / LLM 2 years MBA 1.5 years validation
            $_14 = '';
            $_ma_msc = '';
            $_16 = '';
            if (request('userLevel') == 4 || request('userLevel') == 5) {
                if ((trim(request('BA_Bsc_Title_t')) == '' && request('BA_Bsc_Title_d') == '') || (trim(request('BA_Bsc_From_t')) == '' && request('BA_Bsc_From_d') == '') || (trim(request('BA_Bsc_Subject')) == '' || trim(request('BA_Bsc_Specialization')) == '' || trim(request('BA_Bsc_Total')) == '' || trim(request('BA_Bsc')) == '')) {
                    $_14 = 'empty';
                }
                if ((trim(request('MSc_Title_t')) == '' && request('MSc_Title_d') == '') || (trim(request('MSc_From_t')) == '' && request('MSc_From_d') == '') || (trim(request('MSc_Subject')) == '' || trim(request('MSc_Specialization')) == '' || trim(request('MSc_Total')) == '' || trim(request('MSc')) == '')) {
                    $_ma_msc = 'empty';
                }
                if ((trim(request('BS_Title_t')) == '' && request('BS_Title_d') == '') || (trim(request('BS_From_t')) == '' && request('BS_From_d') == '') || (trim(request('BS_Subject')) == '' || trim(request('BS_Specialization')) == '' || trim(request('BS_Total')) == '' || trim(request('BS')) == '')) {
                    $_16 = 'empty';
                }
                if ($_14 == 'empty' || $_ma_msc == 'empty') {
                    if ($_16 == 'empty') {
                        return back()->with('custom_error', 'You need to fill Sections Undergraduate(16-Year) OR Undergraduate(14-Year) Optional + Undergraduate(MA/MSC) Optional')->withInput();
                    }
                }
                if ($_16 == 'empty') {
                    if ($_14 == 'empty' || $_ma_msc == 'empty') {
                        return back()->with('custom_error', 'You need to fill Sections Undergraduate(16-Year) OR Undergraduate(14-Year) Optional + Undergraduate(MA/MSC) Optional')->withInput();
                    }
                }

                if (!$preQuali->bs_last_result) {
                    $this->validate(request(), [
                        'bs_last_result' => 'required|image|mimes:jpeg,png,jpg,gif,bmp,tiff|max:4096'
                    ]);
                }
                if (request()->hasFile('bs_last_result')) {
                    $image = request()->file('bs_last_result');
                    $image_ext = strtolower($image->getClientOriginalExtension());
                    if ($image_ext == 'jpeg' || $image_ext == 'png' || $image_ext == 'jpg' || $image_ext == 'gif' || $image_ext == 'bmp' || $image_ext == 'tiff') {
                    } else {
                        return back()->with('custom_error', 'BS Result uploaded File format should have [jpeg/png/jpg/gif/bmp/tiff] only with Max Size (4MB)')->withInput();
                    }
                    $filename    = request('userId') . '.' . $image->getClientOriginalExtension();
                    $im = new Imagick($image->getRealPath());
                    $rand = rand(pow(10, 6 - 1), pow(10, 6) - 1);
                    // file_put_contents(storage_path('app/public/images/'.$semester->title.'/phd/'.$rand.'_'.$filename), $image);
                    $image->move(
                        storage_path('app/public/images/' . $semester->title . '/ms/'),
                        $rand . '_' . $filename
                    );
                    // $im->writeImage(storage_path('app/public/images/'.$semester->title.'/phd/'.$rand.'_'.$filename));
                    PreviousQualification::where('fkApplicantId', request('userId'))->where('fkSemesterId', $this->currentSemester)->update(
                        [
                            'bs_last_result' => 'images/' . $semester->title . '/ms/' . $rand . '_' . $filename
                        ]
                    );
                }
            }
            $_18 = '';
            if (request('userLevel') == 5) {
                if ((trim(request('MS_Title_t')) == '' && request('MS_Title_d') == '') || (trim(request('MS_From_t')) == '' && request('MS_From_d') == '') || (trim(request('MS_Subject')) == '' || trim(request('MS_Specialization')) == '' || trim(request('MS_Total')) == '' || request('MS_degree_type') == '' || request('MS_Year') == '' ||  trim(request('MS')) == '')) {
                    $_18 = 'empty';
                }
                if ($_18 == 'empty') {
                    return back()->with('custom_error', 'You need to fill Sections Graduate(18-Year) Section')->withInput();
                }

                // dd(request()->file('ms_last_result')->getMimeType(),request()->file('ms_last_result')->getClientOriginalExtension() );
                if (!$preQuali->ms_last_result || !$preQuali->phd_research_proposal) {
                    $this->validate(request(), [
                        'ms_last_result' => 'required|image|mimes:jpeg,png,jpg,gif,bmp,tiff|max:4096',
                        'phd_research_proposal' => 'required|file|mimes:pdf,doc,docx|max:4096'
                    ]);
                }
                if (request()->hasFile('ms_last_result')) {
                    $image = request()->file('ms_last_result');
                    $image_ext = strtolower($image->getClientOriginalExtension());
                    if ($image_ext == 'jpeg' || $image_ext == 'png' || $image_ext == 'jpg' || $image_ext == 'gif' || $image_ext == 'bmp' || $image_ext == 'tiff') {
                    } else {
                        return back()->with('custom_error', 'MS Result uploaded File format should have [jpeg/png/jpg/gif/bmp/tiff] only with Max Size (4MB)')->withInput();
                    }
                    $filename    = request('userId') . '.' . $image->getClientOriginalExtension();
                    $im = new Imagick($image->getRealPath());
                    $rand = rand(pow(10, 6 - 1), pow(10, 6) - 1);
                    // file_put_contents(storage_path('app/public/images/'.$semester->title.'/phd/'.$rand.'_'.$filename), $image);
                    $image->move(
                        storage_path('app/public/images/' . $semester->title . '/phd/'),
                        $rand . '_' . $filename
                    );
                    // $im->writeImage(storage_path('app/public/images/'.$semester->title.'/phd/'.$rand.'_'.$filename));
                    PreviousQualification::where('fkApplicantId', request('userId'))->where('fkSemesterId', $this->currentSemester)->update(
                        [
                            'ms_last_result' => 'images/' . $semester->title . '/phd/' . $rand . '_' . $filename
                        ]
                    );
                }
                if (request()->hasFile('phd_research_proposal')) {
                    $image = request()->file('phd_research_proposal');
                    $image_ext = strtolower($image->getClientOriginalExtension());
                    if ($image_ext != 'pdf') {
                        return back()->with('custom_error', 'PHD proposal uploaded File format should have [pdf] only with Max Size (4MB)')->withInput();
                    }
                    $filename    = request('userId') . '.' . $image->getClientOriginalExtension();
                    $im = new Imagick($image->getRealPath());
                    $rand = rand(pow(10, 6 - 1), pow(10, 6) - 1);
                    $image->move(
                        storage_path('app/public/images/' . $semester->title . '/phd/'),
                        $rand . '_' . $filename
                    );
                    // $im->writeImage(storage_path('app/public/images/'.$semester->title.'/phd/'.$rand.'_'.$filename));
                    PreviousQualification::where('fkApplicantId', request('userId'))->where('fkSemesterId', $this->currentSemester)->update(
                        [
                            'phd_research_proposal' => 'images/' . $semester->title . '/phd/' . $rand . '_' . $filename
                        ]
                    );
                }
            }
            $ssc_title = (trim(request('SSC_Title_t')) != '') ? trim(request('SSC_Title_t')) : request('SSC_Title_d');
            $ssc_from = (trim(request('SSC_From_t')) != '') ? trim(request('SSC_From_t')) : request('SSC_From_d');
            $ssc_subject = (trim(request('SSC_Subject_t')) != '') ? trim(request('SSC_Subject_t')) : request('SSC_Subject_d');

            $hssc_title = (trim(request('HSC_Title_t')) != '') ? trim(request('HSC_Title_t')) : request('HSC_Title_d');
            $hssc_from = (trim(request('HSC_From_t')) != '') ? trim(request('HSC_From_t')) : request('HSC_From_d');
            $hssc_subject = (trim(request('HSC_Subject_t')) != '') ? trim(request('HSC_Subject_t')) : request('HSC_Subject_d');
            $resultAwaiting = (request('resultAwaiting') == '1') ? 1 : 0;

            $ba_bsc_title = (trim(request('BA_Bsc_Title_t')) != '') ? trim(request('BA_Bsc_Title_t')) : request('BA_Bsc_Title_d');
            $ba_bsc_from = (trim(request('BA_Bsc_From_t')) != '') ? trim(request('BA_Bsc_From_t')) : request('BA_Bsc_From_d');
            $resultwaitbsc = (request('resultwaitbsc') == '1') ? 1 : 0;

            $msc_title = (trim(request('MSc_Title_t')) != '') ? trim(request('MSc_Title_t')) : request('MSc_Title_d');
            $msc_from = (trim(request('MSc_From_t')) != '') ? trim(request('MSc_From_t')) : request('MSc_From_d');

            $bs_title = (trim(request('BS_Title_t')) != '') ? trim(request('BS_Title_t')) : request('BS_Title_d');
            $bs_from = (trim(request('BS_From_t')) != '') ? trim(request('BS_From_t')) : request('BS_From_d');

            $ms_title = (trim(request('MS_Title_t')) != '') ? trim(request('MS_Title_t')) : request('MS_Title_d');
            $ms_from = (trim(request('MS_From_t')) != '') ? trim(request('MS_From_t')) : request('MS_From_d');

            if ($ssc_title == 'O-Level') {
                $this->validate(request(), [
                    'o_level_obtained' => 'required'
                ]);
            }
            // Dev Mannan: Special check in case candidate got admission and got entry in result list table then previous qualification could not be updated
            if (Auth::guard('web')->check() && ($adminId == 1 || $adminId == 20)) {
                // dd('admin');
            } else {
                $rollNumber = RollNumber::where('fkApplicantId', request('userId'))->where('fkSemesterId', $this->currentSemester)->get();
                if ($rollNumber) {
                    foreach ($rollNumber as $rn) {
                        $resList = ResultList::where('rollno', $rn->id)->first();
                        if ($resList) {
                            return back()->with('custom_error', 'Previous Qualification cannot be updated because result list has been published')->withInput();
                        }
                    }
                }
            }
            $updated_data = PreviousQualification::where('fkApplicantId', request('userId'))->where('fkSemesterId', $this->currentSemester)->update(
                [

                    'SSC_Title' => $ssc_title,
                    'SSC_From' => $ssc_from,
                    'SSC_Subject' => $ssc_subject,
                    'SSC_Year' => request('SSC_Year'),
                    'SSC_Total' => request('SSC_Total'),
                    'SSC_Composite' => request('SSC_Composite'),
                    'SSC_degree_type' => request('SSC_degree_type'),

                    'HSC_Title' => $hssc_title,
                    'HSC_From' => $hssc_from,
                    'HSC_Subject' => $hssc_subject,
                    'HSC_Year' => request('HSC_Year'),
                    'HSSC_Total' => request('HSSC_Total'),
                    'HSSC_Composite' => request('HSSC_Composite'),
                    'HSSC_Total1' => request('HSSC_Total1'),
                    'HSSC1' => request('HSSC1'),
                    'HSSC_Total2' => request('HSSC_Total2'),
                    'HSSC2' => request('HSSC2'),
                    'resultAwaiting' => $resultAwaiting,
                    'HSC_degree_type' => request('HSC_degree_type'),

                    // Code for BS Programs only starts
                    'entry_test_center' => trim(request('entry_test_center')),
                    'entry_test_total_marks' => trim(request('entry_test_total_marks')),
                    'entry_test_obtained_marks' => trim(request('entry_test_obtained_marks')),
                    'iiu_test_appear_willingness' => request('iiu_test_appear_willingness'),
                    // Code for BS Programs only starts

                    'llb_total_marks' => trim(request('llb_total_marks')),
                    'llb_obtained_marks' => trim(request('llb_obtained_marks')),
                    'law_completion_date' => (trim(request('law_completion_date')) == '') ? NULL : Carbon::parse(request('law_completion_date')),
                    'lat_upcoming' => request('lat_upcoming'),

                    'gat_gre_test_type' => request('gat_gre_test_type'),
                    'gat_gre_total_marks' => trim(request('gat_gre_total_marks')),
                    'gat_gre_obtained_marks' => trim(request('gat_gre_obtained_marks')),
                    'gat_gre_completion_date' => (trim(request('gat_gre_completion_date')) == '') ? NULL : Carbon::parse(request('gat_gre_completion_date')),


                    'BA_Bsc_Title' => $ba_bsc_title,
                    'BA_Bsc_From' => $ba_bsc_from,
                    'BA_Bsc_Subject' => trim(request('BA_Bsc_Subject')),
                    'BA_Bsc_Year' => request('BA_Bsc_Year'),
                    'BA_Bsc_Specialization' => trim(request('BA_Bsc_Specialization')),
                    'BA_Bsc_Total' => request('BA_Bsc_Total'),
                    'BA_Bsc' => request('BA_Bsc'),
                    'resultwaitbsc' => $resultwaitbsc,
                    'BA_Bsc_degree_type' => request('BA_Bsc_degree_type'),

                    'MSc_Title' => $msc_title,
                    'MSc_From' => $msc_from,
                    'MSc_Subject' => trim(request('MSc_Subject')),
                    'MSc_Year' => request('MSc_Year'),
                    'MSc_Specialization' => trim(request('MSc_Specialization')),
                    'MSc_Total' => request('MSc_Total'),
                    'MSc' => request('MSc'),
                    'MSc_degree_type' => request('MSc_degree_type'),

                    'BS_Title' => $bs_title,
                    'BS_From' => $bs_from,
                    'BS_Subject' => trim(request('BS_Subject')),
                    'BS_Year' => request('BS_Year'),
                    'BS_Specialization' => trim(request('BS_Specialization')),
                    'BS_Total' => request('BS_Total'),
                    'BS' => request('BS'),
                    'BS_Exam_System' => request('BS_Exam_System'),
                    'BS_degree_type' => request('BS_degree_type'),

                    'MS_Title' => $ms_title,
                    'MS_From' => $ms_from,
                    'MS_Subject' => trim(request('MS_Subject')),
                    'MS_Year' => request('MS_Year'),
                    'MS_Specialization' => trim(request('MS_Specialization')),
                    'MS_Total' => request('MS_Total'),
                    'MS' => request('MS'),
                    'MS_Exam_System' => request('MS_Exam_System'),
                    'MS_degree_type' => request('MS_degree_type'),

                    'o_level_obtained' => (trim(request('o_level_obtained')) == '') ? NULL : trim(request('o_level_obtained'))

                ]
            );
            ApplicationStatus::where('fkApplicantId', request('userId'))->where('fkSemesterId', $this->currentSemester)->update(
                [
                    'step_education' => 1
                ]
            );
            if ($adminId != '') {
                PreviousQualification::where('fkApplicantId', request('userId'))->where('fkSemesterId', $this->currentSemester)->update(
                    [
                        'modifiedBy' => $adminId
                    ]
                );
            }
            // Dev Mannan: PQM update/create for all applications
            $applications = Application::with(['program', 'faculty', 'program.programscheduler', 'applicant', 'applicant.previousQualification'])->where('fkApplicantId', request('userId'))->where('fkSemesterId', $this->currentSemester)->whereNotNull('fkProgramId')->get();
            if (!$applications->isEmpty()) {
                foreach ($applications as $application) {
                    $this->generateAndStorePQM($application);
                }
            }
            if ($updated_data == 1) {
                $updated_data = PreviousQualification::where('fkApplicantId', request('userId'))->where('fkSemesterId', $this->currentSemester)->first();
            }
            $this->logMaintenance($hash, request('userId'), 'Previous Qualification', $preQuali, $updated_data);
            return redirect()->route('selectprogram', $hash);

            // return back()->with('status', 'Information updated successfully!');
        } else {
            $previousQualification = PreviousQualification::where('fkApplicantId', auth()->id())->where('fkSemesterId', $this->currentSemester)->first();
            // Dev Mannan: Check if current qualification doesnot exist then show qualification from previous semester
            if (is_null($previousQualification->SSC_Total) && is_null($previousQualification->SSC_Composite)) {
                $application_count = PreviousQualification::where('fkApplicantId', auth()->id())->count();
                if ($application_count > 1) {
                    $previousQualification = PreviousQualification::where('fkApplicantId', auth()->id())->where('fkSemesterId', $this->currentSemester - 1)->first();
                    if (is_null($previousQualification)) {
                        $previousQualification = PreviousQualification::where('fkApplicantId', auth()->id())->where('fkSemesterId', $this->currentSemester - 2)->first();
                        if (is_null($previousQualification)) {
                            $previousQualification = PreviousQualification::where('fkApplicantId', auth()->id())->where('fkSemesterId', $this->currentSemester - 3)->first();
                            if (is_null($previousQualification)) {
                                $previousQualification = PreviousQualification::where('fkApplicantId', auth()->id())->where('fkSemesterId', $this->currentSemester - 4)->first();
                                if (is_null($previousQualification)) {
                                    $previousQualification = PreviousQualification::where('fkApplicantId', auth()->id())->where('fkSemesterId', $this->currentSemester - 5)->first();
                                    if (is_null($previousQualification)) {
                                        $previousQualification = PreviousQualification::where('fkApplicantId', auth()->id())->where('fkSemesterId', $this->currentSemester - 6)->first();
                                        if (is_null($previousQualification)) {
                                            $previousQualification = PreviousQualification::where('fkApplicantId', auth()->id())->where('fkSemesterId', $this->currentSemester - 7)->first();
                                            if (is_null($previousQualification)) {
                                                $previousQualification = PreviousQualification::where('fkApplicantId', auth()->id())->where('fkSemesterId', $this->currentSemester - 8)->first();
                                                if (is_null($previousQualification)) {
                                                    $previousQualification = PreviousQualification::where('fkApplicantId', auth()->id())->where('fkSemesterId', $this->currentSemester - 9)->first();
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            // $applicationStatus = ApplicationStatus::where('fkApplicantId', auth()->id())->first();
            $userdetail = Applicant::where('userId', auth()->id())->first();

            $sscDegree = Degree::where('degreelevel', 1)->orderBy('id')->groupBy('title')->get();
            $sscInstitute = Institute::where('type', 1)->orderBy('id')->groupBy('instName')->get();
            $sscGroup = Degree::where('degreelevel', 1)->orderBy('id')->groupBy('subjects')->get();

            $hsscDegree = Degree::where('degreelevel', 2)->orderBy('id')->groupBy('title')->get();
            $hsscGroup = Degree::where('degreelevel', 2)->orderBy('id')->groupBy('subjects')->get();

            $bscDegree = Degree::where('degreelevel', 3)->orderBy('id')->groupBy('title')->get();
            $bscInstitute = Institute::where('type', 2)->orderBy('id')->groupBy('instName')->get();

            $mscDegree = Degree::where('degreelevel', 4)->orderBy('id')->groupBy('title')->get();

            $bsDegree = Degree::where('degreelevel', 5)->orderBy('id')->groupBy('title')->get();

            $msDegree = Degree::where('degreelevel', 6)->orderBy('id')->groupBy('title')->get();

            $applicantDetail = ApplicantDetail::where('fkApplicantId', auth()->id())->first();

            return view('frontend.previousQualification', compact('custom_error', 'hash', 'applicantDetail', 'userdetail', 'previousQualification', 'sscDegree', 'sscInstitute', 'sscInstitute', 'sscGroup', 'hsscDegree', 'hsscGroup', 'bscDegree', 'bscInstitute', 'mscDegree', 'bsDegree', 'msDegree'));
        }
    }
}
