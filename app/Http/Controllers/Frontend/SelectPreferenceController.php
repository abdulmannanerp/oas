<?php

namespace App\Http\Controllers\Frontend;
use App\Model\Semester;
use Carbon\Carbon;
use App\Model\Applicant;
use App\Model\Programme;
use App\Model\Application;
use App\Model\ChallanDetail;
use App\Model\ApplicantDetail;
use App\Model\ApplicationStatus;
use App\Model\ProgrammeScheduler;
use App\Http\Controllers\Controller;
use App\Model\PreviousQualification;
use App\Model\ProgramFee;

class SelectPreferenceController extends Controller{

    protected $currentSemester;
    public function __construct()
    {
        $this->currentSemester = $this->getCurrentSemester()->pkSemesterId;
    }
    public function index ($hash = ''){
        $adminId = $this->decodeAdminData($hash);
        $status = request()->session()->get('status');
        $custom_error = request()->session()->get('custom_error');

        $applied_applications = Application::where('fkApplicantId', auth()->id())->where('fkSemesterId', $this->currentSemester)->whereNotNull('fkProgramId')->where('a_status', 1)->get();

        if ( request()->method() == 'POST' ) {

            for ($i = 1; $i <= $applied_applications->count(); $i++){
                ApplicantDetail::where('fkApplicantId', request('userId'))->update(
                    [
                        "preference_$i" => request("preference_$i")
                    ]
                );
            }            
            ApplicationStatus::where('fkApplicantId', request('userId'))->where('fkSemesterId', $this->currentSemester)->update(
                [
                    'step_preference'=> 1                    
                ]
            );
            $this->logMaintenance($hash, request('userId'), 'Select Preference');
            // return redirect()->route('savedetail', $hash);
            return redirect()->route('otherdetail', $hash);
        }else{

            $userdetail = Applicant::where('userId',auth()->id())->first();
            $applicantDetail = ApplicantDetail::where('fkApplicantId', auth()->id())->first();
            return view('frontend.selectPreference', compact('custom_error','status','hash','applicantDetail','userdetail', 'applied_applications'));
        }
    }
}