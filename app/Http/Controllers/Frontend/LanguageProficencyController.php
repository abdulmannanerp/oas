<?php

namespace App\Http\Controllers\Frontend;
use App\Http\Controllers\Controller;


use App\Model\Applicant;
use App\Model\ApplicantDetail;
use App\Model\ApplicationStatus;

class LanguageProficencyController extends Controller{

    protected $currentSemester;
    public function __construct()
    {
        $this->currentSemester = $this->getCurrentSemester()->pkSemesterId;
    }

    public function index ($hash = ''){
        $adminId = $this->decodeAdminData($hash);
        $status = request()->session()->get('status');
        if ( request()->method() == 'POST' ) {
            $applicationStatus = ApplicationStatus::where('fkApplicantId', auth()->id())->where('fkSemesterId', $this->currentSemester)->first();
            if($applicationStatus->step_save == 1 && $adminId == ''){
                return redirect()->route('aptitudetest');
            }
             if(request('arabic_written') || request('arabic_spoken') || request('english_written') || request('english_spoken')){
                ApplicantDetail::where('fkApplicantId', request('userId'))->update(
                    [
                        'arabicWritten' => request('arabic_written'),
                        'arabicSpoken' => request('arabic_spoken'),

                        'englishWritten' => request('english_written'),
                        'englishSpoken' => request('english_spoken')
                    ]
                );
                ApplicationStatus::where('fkApplicantId', request('userId'))->where('fkSemesterId', $this->currentSemester)->update(
                    [
                        'step_language'=> 1                    
                    ]
                );
                if($adminId != ''){
                    ApplicantDetail::where('fkApplicantId', request('userId'))->update(
                        [
                            'modifiedBy' => $adminId
                        ]
                    );
                }
            }
            $this->logMaintenance($hash, request('userId'), 'Language Proficiency');
            return redirect()->route('aptitudetest', $hash);
            // return back()->with('status', 'Information updated successfully!');
        }else{
            $applicantDetail = ApplicantDetail::where('fkApplicantId', auth()->id())->first();
            // $applicationStatus = ApplicationStatus::where('fkApplicantId', auth()->id())->first();
            $userdetail = Applicant::where('userId',auth()->id())->first(); 
            return view('frontend.languageProficency', compact('status','hash','userdetail', 'applicantDetail'));
        }
    }
}
