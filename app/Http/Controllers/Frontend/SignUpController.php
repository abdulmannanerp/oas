<?php

namespace App\Http\Controllers\Frontend;
use Carbon\Carbon;

use App\Model\Option;
use App\Model\oasEmail;
use App\Model\Semester;
use App\Model\Applicant;
use App\Model\Application;
use App\Model\Nationality;
use App\Model\ProgramLevel;
use App\Model\ApplicantDetail;
use App\Model\ApplicantAddress;
use App\Model\ApplicationStatus;
use App\Model\ProgrammeScheduler;
use App\Http\Controllers\Controller;
use App\Model\PreviousQualification;
use Illuminate\Support\Facades\Auth;

class SignUpController extends Controller {

    public function index ()
    {
        $status = request()->session()->get('status');
        $status_ok = request()->session()->get('status_ok');
		if ( request()->method() == 'POST' ) {
            $this->validate(request(), [
                'email' => 'required|unique:tbl_users,email',
                'cnic' => 'required|unique:tbl_users,cnic',
                'name' => 'required',
                'password' => 'required|min:6|confirmed',
                'gender' => 'required',
                'nationality' => 'required',
                'level' => 'required'
            ],
            [
                'email.unique' => 'Email already registered please login to continue',
                'cnic.unique' => 'CNIC already registered please login to continue'
            ]
        );
            if ( strpos (request('cnic'), '-') !== false ) {
                request()->request->set('cnic', $this->trimDashesFromCnic(request('cnic')));
            }
            $existingCnic = Applicant::where('cnic', request('cnic'))->first();
            if($existingCnic){
                return back()->with('status', 'CNIC Already Exists')->withInput();
            }
            if(request('gender') == 'others'){
                $selectedGender = 3;
                $others = request('gender');
                
            }else{
                $selectedGender = request('gender');
                $others = NULL;
            }
            
            $semester = Semester::where('status', 1)->first();
            $applicant_data = Applicant::create(
                [
                    'password' => bcrypt(request('password')),
                    'name' => request('name'),
                    'email' => request('email'),
                    'fkGenderId' =>$selectedGender,
                    'gender_other' => $others,
                    'cnic' =>request('cnic'),
                    'fkNationality' =>request('nationality'),
                    'fkLevelId' =>request('level')
                ]
            );
            Application::create(
                [
                    'fkApplicantId' => $applicant_data->userId,
                    'fkCurrentStatus' => 1,
                    'fkSemesterId' => $semester->pkSemesterId
                ]
            );
            ApplicantAddress::create(
                [
                    'fkApplicantId' => $applicant_data->userId,
                ]
            );
            ApplicantDetail::create(
                [
                    'fkApplicantId' => $applicant_data->userId
                    // 'fkSemesterId' => $semester->pkSemesterId
                ]
            );
            ApplicationStatus::create(
                [
                    'fkApplicantId' => $applicant_data->userId,
                    'fkSemesterId' => $semester->pkSemesterId
                ]
            );
            PreviousQualification::create(
                [
                    'fkApplicantId' => $applicant_data->userId,
                    'fkSemesterId' => $semester->pkSemesterId
                ]
            );
            if($selectedGender == 3){
                $abbr = 'AdmissionMale';

            }elseif($selectedGender == 2){
                $abbr = 'AdmissionFemale';
            }
            $data = array('name' => request('name'), 'email' => request('email'), 'password' => request('password'));
            oasEmail::email($abbr, $data, request('email'), 'IIU Admission Office', 'frontend.signUpEmail');
            
            if (Auth::guard('applicant')->attempt(['email' => request('email'), 'password' => request('password')])) {
                // Authentication passed...
                return redirect()->intended('/frontend/personaldetail');
            }
            // redirect to personal info page with auth id
            // return back()->with('status', 'Information updated successfully!');
        }else{
            $openClose = Option::where('title','admission-start')->first();
            $nationalilty = Nationality::where('nationalitystatus', 1)->get();
            $level = ProgramLevel::where('status', 1)->get();
            $semester = Semester::where('status', 1)->first();
            $Closingdate = Carbon::parse($semester->Closingdate)->format('d-M-Y');
            $extendedPrograms = ProgrammeScheduler::with(['program'])->where('lastDate','>=', Carbon::now()->toDateTimeString())->whereHas('program', function($query){
                $query->OrderBy('title');
            })->get();
            return view('frontend.signUp',compact('nationalilty', 'level', 'status', 'status_ok','Closingdate', 'openClose', 'extendedPrograms'));
        }
    }
    public function trimDashesFromCnic($cnic){
        return str_replace('-', '', $cnic);
    }
}
