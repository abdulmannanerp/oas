<?php

namespace App\Http\Controllers\Frontend;
use App\Model\City;

use App\Model\Semester;

use App\Model\Applicant;
use App\Model\ApplicantDetail;
use App\Model\ApplicantAddress;
use App\Model\ApplicationStatus;
use App\Http\Controllers\Controller;

class AddressDetailController extends Controller{
    protected $currentSemester;
    public function __construct()
    {
        $this->currentSemester = $this->getCurrentSemester()->pkSemesterId;
    }
    public function index ($hash = ''){
        $adminId = $this->decodeAdminData($hash);
        $status = request()->session()->get('status');
        $semester = Semester::where('status', 1)->first();
        $userdetail = Applicant::where('userId',auth()->id())->first();
        if ( request()->method() == 'POST' ) {
            $applicationStatus = ApplicationStatus::where('fkApplicantId', auth()->id())->where('fkSemesterId', $this->currentSemester)->first();
            if($applicationStatus->step_save == 1 && $adminId == ''){
                return redirect()->route('previousqualification');
            }
            $this->validate(request(), [
                'permanent_city' => 'required',
                'permanent_carrier' => 'numeric|required',
                'permanent_address' => 'required',
                'permanent_area' => 'required',
                'mailing_city' => 'required',
                'mailing_carrier' => 'numeric|required',
                'mailing_address' => 'required',
                'mailing_area' => 'required',
                // 'father_city' => 'required',
                // 'father_carrier' => 'numeric|required',
                // 'father_address' => 'required',
                // 'father_area' => 'required',
            ]);
            if($userdetail->fkNationality == 1){
                $this->validate(request(), [
                    'permanent_phone' => 'numeric|required',
                    'mailing_phone' => 'numeric|required'
                    // 'father_phone' => 'numeric|required'
                ]);
            }
            $permanent_phone = preg_replace('/[^0-9]/', '', request('permanent_phone'));
            $permanent_phone = '92'.request('permanent_carrier').$permanent_phone;
            $mailing_phone = preg_replace('/[^0-9]/', '', request('mailing_phone'));
            $mailing_phone = '92'.request('mailing_carrier').$mailing_phone;
            // $father_phone = preg_replace('/[^0-9]/', '', request('father_phone'));
            // $father_phone = '92'.request('father_carrier').$father_phone;

            if($userdetail->fkNationality == 2 || $userdetail->fkNationality == 3){
                $permanent_phone = preg_replace('/[^0-9]/', '', request('permanent_carrier'));
                $mailing_phone = preg_replace('/[^0-9]/', '', request('mailing_carrier'));
                // $father_phone = preg_replace('/[^0-9]/', '', request('father_carrier'));
            }
            ApplicantAddress::where('fkApplicantId', request('userId'))->update(
                [
                    'cityPmt' => request('permanent_city'),
                    'phonePmt' => $permanent_phone,
                    'addressPmt' => request('permanent_address'),
                    'areaPmt' => request('permanent_area'),

                    'cityPo' => request('mailing_city'),
                    'phonePo' => $mailing_phone,
                    'addressPo' => request('mailing_address'),
                    'areaPo' => request('mailing_area'),

                    // 'cityFather' => request('father_city'),
                    // 'phoneFather' => $father_phone,
                    // 'addressFather' => request('father_address'),
                    // 'areaFather' => request('father_area'),
                ]
            );
            ApplicationStatus::where('fkApplicantId', request('userId'))->where('fkSemesterId', $this->currentSemester)->update(
                [
                    'step_address'=> 1                    
                ]
            );
            if($adminId != ''){
                ApplicantAddress::where('fkApplicantId', request('userId'))->update(
                    [
                        'modifiedBy'=> $adminId
                    ]
                );
            }
            $this->logMaintenance($hash, request('userId'), 'Address');
            return redirect()->route('previousqualification', $hash);
            // return back()->with('status', 'Information updated successfully!');
        }else{
            $applicantAddress = ApplicantAddress::where('fkApplicantId', auth()->id())->first(); 
            $city = City::all();
            // $applicationStatus = ApplicationStatus::where('fkApplicantId', auth()->id())->first(); 
            $userdetail = Applicant::where('userId',auth()->id())->first(); 
            $applicantDetail = ApplicantDetail::where('fkApplicantId', auth()->id())->first();
            return view('frontend.addressDetail', compact('status','hash','applicantDetail','city','userdetail', 'applicantAddress'));
        }
    }
}
