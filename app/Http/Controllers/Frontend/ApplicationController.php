<?php

namespace App\Http\Controllers\Frontend;

use Imagick;


use App\Model\City;
use App\Model\Result;
use App\Model\Faculty;
use App\Model\oasEmail;
use App\Model\Semester;
use App\Model\Applicant;
use App\Model\ResultList;
use App\Model\Application;
use App\Model\ChallanDetail;
use Illuminate\Http\Request;
use App\Model\ApplicantDetail;
use App\Model\ApplicationStatus;
use Barryvdh\DomPDF\Facade as PDF;
use App\Http\Controllers\Controller;
use App\Model\Programme;
use App\Model\RollNumber;
use Intervention\Image\Facades\Image;
use App\Model\ChallanGrouping;

class ApplicationController extends Controller
{
    protected $currentSemester;

    public function __construct()
    {
        $this->currentSemester = $this->getCurrentSemester()->pkSemesterId;
    }

    public function index($hash = '')
    {
        $adminId = $this->decodeAdminData($hash);
        $status = request()->session()->get('status');
        $semester = Semester::where('status', 1)->first();
        if (request()->method() == 'POST') {
            $application = Application::where('id', request('applicationId'))->where('fkSemesterId', $this->currentSemester)->first();
            // if($application->feeSubmitted != NULL && $application->bankDetails != NULL && $application->branchCode != NULL && $application->challan_pic != NULL){
            //     return back()->with('status', 'Information Already updated!');
            // }
            // dd(request()->all());
            $this->validate(request(), [
                'feeSubmitted' => 'required',
                'bankDetails' => 'required',
                'branchCode' => 'required'
            ]);
            if (!$application->challan_pic) {
                $this->validate(request(), [
                    'challan_pic' => 'required|image|mimes:jpeg,png,jpg,gif,bmp,tiff|max:4096',
                ]);
            }
            if (request()->hasFile('challan_pic')) {
                $image = request()->file('challan_pic');
                $filename    = request('applicationId') . '.' . $image->getClientOriginalExtension();
                // $image_resize = Image::make($image->getRealPath());              
                // $image_resize->save(storage_path('app/public/images/'.$filename));
                $im = new Imagick($image->getRealPath());
                $size = $im->getImageLength();
                if ($size > 1000000) {
                    $im->adaptiveResizeImage('1200', '1200');
                }
                // $im->writeImage(storage_path('app/public/images/c_'.$filename));
                $rand = rand(pow(10, 6 - 1), pow(10, 6) - 1);
                $im->writeImage(storage_path('app/public/images/' . $semester->title . '/challan/' . $rand . '_' . $filename));
                Application::where('id', request('applicationId'))->where('fkSemesterId', $this->currentSemester)->update(
                    [
                        'challan_pic' => 'images/' . $semester->title . '/challan/' . $rand . '_' . $filename
                    ]
                );
            }
            $fkCurrentStatus = ($application->feeVerified || $application->fkFeeVerifiedBy) ? $application->fkCurrentStatus : 3;
            Application::where('id', request('applicationId'))->where('fkSemesterId', $this->currentSemester)->update(
                [
                    'feeSubmitted' => date('Y-m-d', strtotime(request('feeSubmitted'))),
                    'bankDetails' => request('bankDetails'),
                    'branchCode' => request('branchCode'),
                    'fkCurrentStatus' => $fkCurrentStatus
                ]
            );
            return back()->with('status', 'Information updated successfully!');
        } else {
            $city = City::all();
            // $applicationStatus = ApplicationStatus::where('fkApplicantId', auth()->id())->first();
            $userdetail = Applicant::where('userId', auth()->id())->first();
            $application = Application::where('fkApplicantId', auth()->id())->where('fkSemesterId', $this->currentSemester)->whereNotNull('fkProgramId')->where('a_status', 1)->orderBy('fkFacId', 'ASC')->get();

            $applicantDetail = ApplicantDetail::where('fkApplicantId', auth()->id())->first();
            $appli_array = $application->pluck('id')->toArray();
            $resultlist = ResultList::with(['getRollno.application', 'result'])
                ->whereHas('getRollno.application', function ($query) use ($appli_array) {
                    $query->whereIn('id', $appli_array);
                })
                ->whereHas('result', function ($query) {
                    $query->where('active', 1)->where('type', 'Result');
                })->get();
            $engResListData = '';
            foreach ($application as $app) {
                if ($app->fkProgramId == 189 && $app->fkCurrentStatus >= 5) {
                    $getRoll = RollNumber::where('fkApplicationId', $app->id)->first();
                    $engResListData = ResultList::with(['result'])
                        ->whereHas('result', function ($query) {
                            $query->whereIn('fkProgrammeId', array(157, 132, 131))->where('active', 1);
                        })
                        ->where('rollno', $getRoll->id)
                        ->get();
                }
            }
            $challanGrouping = '';

            if ($userdetail->fkLevelId == 1) {
                $challanGrouping = ChallanGrouping::where('fkApplicantId', auth()->id())->orderBy('fkFacId', 'ASC')->get();
                // $a = [];
                $applicationList = array();
                foreach ($challanGrouping as $c) {
                    if ($c->applicationId_1 != NULL) {
                        // $a[] = ['applicationId_1' => $c->applicationId_1, 'challanId' => $c->challanId, 'fkFacId' => $c->fkFacId];
                        array_push($applicationList, $c->applicationId_1);
                    }
                    if ($c->applicationId_2 != NULL) {
                        // $a[] = ['applicationId_2' => $c->applicationId_2, 'challanId' => $c->challanId, 'fkFacId' => $c->fkFacId];
                        array_push($applicationList, $c->applicationId_2);
                    }
                    if ($c->applicationId_3 != NULL) {
                        // $a[] = ['applicationId_3' => $c->applicationId_3, 'challanId' => $c->challanId, 'fkFacId' => $c->fkFacId];
                        array_push($applicationList,$c->applicationId_3);
                    }
                }
                // dd($applicationList);

                $application = Application::whereIn('id', $applicationList)->where('fkSemesterId', $this->currentSemester)->get();
            }



            return view('frontend.application', compact('engResListData', 'status', 'hash', 'applicantDetail', 'userdetail', 'city', 'application', 'resultlist'));
        }
    }

    public function statusUpdate($hash = '')
    {


        $this->validate(request(), [
            'consent_taken' => 'required'
        ]);

        $consent_taken = request('consent_taken');
        $userId = request('userId');

        // Applicant::where('userId', $userId)->update(
        //     [
        //         'accepted_rejected' => $consent_taken                 
        //     ]
        // );

        Application::where('id', request('applicationId'))->where('fkSemesterId', $this->currentSemester)->update(
            [
                'accepted_rejected' => $consent_taken
            ]
        );

        return back()->with('status', 'Information updated successfully!');
    }


    public function getFeeChallan($applicationid, $semesterid, $programid, $userId = '')
    {
        $userdetail = Applicant::where('userId', auth()->id())->first();
        if ($userId)
            $userdetail = Applicant::where('userId', $userId)->first();
        $challanDetail = ChallanDetail::where('fkApplicationtId', $applicationid)->where('fkSemesterId', $semesterid)->where('fkProgramId', $programid)->where('challan_type', 1)->first();
        $bankDetails = \DB::table('tbl_oas_banks')->where('accountFor', 1)->where('fkBankFor', $userdetail->fkGenderId)->first();
        $copies = ['Bank Copy', 'Accounts Copy', 'Admission Office Copy', 'Student Copy'];
        $challanType = '$';
        // return view('frontend.feeChallan', compact('challanDetail', 'bankDetails', 'copies'));
        $pdf = PDF::loadView('frontend.feeChallan', compact('challanDetail', 'bankDetails', 'copies', 'userdetail', 'challanType'))->setPaper('a4', 'landscape');
        return $pdf->stream();
    }

    public function getFeeChallanPkr($applicationid, $semesterid, $programid, $userId = '')
    {
        $userdetail = Applicant::where('userId', auth()->id())->first();
        if ($userId)
            $userdetail = Applicant::where('userId', $userId)->first();
        $challanDetail = ChallanDetail::where('fkApplicationtId', $applicationid)->where('fkSemesterId', $semesterid)->where('fkProgramId', $programid)->where('challan_type', 1)->first();
        $bankDetails = \DB::table('tbl_oas_banks')->where('accountFor', 1)->where('fkBankFor', $userdetail->fkGenderId)->first();
        $copies = ['Bank Copy', 'Accounts Copy', 'Admission Office Copy', 'Student Copy'];
        $challanType = 'PKR';
        // return view('frontend.feeChallan', compact('challanDetail', 'bankDetails', 'copies'));
        $pdf = PDF::loadView('frontend.feeChallan', compact('challanDetail', 'bankDetails', 'copies', 'userdetail', 'challanType'))->setPaper('a4', 'landscape');
        return $pdf->stream();
    }
    public function printApplication(Request $request)
    {
        $oas_applications = Application::where('id', $request->oas_app_id) > where('fkSemesterId', $this->currentSemester)->first();
        $oas_applicatdetail = ApplicantDetail::where('fkApplicantId', $oas_applications->fkApplicantId)->first();
        // $this->thumbPrint('http://admission.iiu.edu.pk/'.$oas_applicatdetail->pic); 
        return view('applications.printApplication', compact('oas_applications'));
    }
    public function printJoiningPerforma(Request $request)
    {
        $applicantDetail = ApplicantDetail::where('fkApplicantId', auth()->id())->first();
        $semester = Semester::where('status', 1)->first();
        $result_id = Result::where('pkResultsId', $request->oas_app_id)->first();
        $faculty = Faculty::where('pkFacId', $result_id->fkFacultyId)->first();
        $program = Programme::where('pkProgId', $result_id->fkProgrammeId)->first();
        $duesDetail = ChallanDetail::where('fkRollNumber', $request->rollno)->where('challan_type', 2)->first();
        $securityDetail = ChallanDetail::where('fkRollNumber', $request->rollno)->where('challan_type', 3)->first();
        $pdf = PDF::loadView('applications.joiningPerforma', compact('applicantDetail', 'semester', 'faculty', 'program', 'duesDetail', 'securityDetail'))->setPaper('a4', 'portrait');
        return $pdf->stream();
        // return view('applications.joiningPerforma', compact('applicantDetail', 'semester', 'faculty', 'program', 'duesDetail', 'securityDetail'));
    }
    public function resetPassword(Request $request)
    {
        $userdetail = Applicant::where('email', $request->email)->first();

        if ($userdetail) {
            $pass = $this->getRandomString();
            Applicant::where('email', $userdetail->email)->update(
                [
                    'password' => bcrypt($pass)
                ]
            );
            if ($userdetail->fkGenderId == 3) {
                $abbr = 'AdmissionMale';
            } elseif ($userdetail->fkGenderId == 2) {
                $abbr = 'AdmissionFemale';
            }
            $data = array('name' => $userdetail->name, 'email' => $userdetail->email, 'password' => $pass);
            oasEmail::email($abbr, $data, request('email'), 'Password Change Notification', 'frontend.UpdatePasswordEmail');
            return back()->with('status_ok', 'Password Change Notification, Please check your email');
        } else {
            return back()->with('status', 'Email does not exist');
        }
    }
    public function getRandomString($length = 10)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $string = '';
        for ($i = 0; $i < $length; $i++) {
            $string .= $characters[mt_rand(0, strlen($characters) - 1)];
        }
        return $string;
    }
}
