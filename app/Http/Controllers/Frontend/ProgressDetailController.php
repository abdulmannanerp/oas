<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Nationality;
use App\Model\Listscholarship;
use App\Model\ApplicantDetail;
use App\Model\Applicant;
use App\Model\ApplicantAddress;
use App\Model\City;
use App\Model\ScholarshipInformation;
use App\Model\ScholarshipFamilyDetail;
use App\Model\ScholarshipFamilyAssets;
use App\Model\ScholarshipDetail;
use Barryvdh\DomPDF\Facade as PDF;

class ProgressDetailController extends Controller
{
   public function index($hash = '')
   {
      $adminId = $this->decodeAdminData($hash);

        $userdetail = Applicant::where('userId',auth()->id())->first();
        $applicantDetail = ApplicantDetail::where('fkApplicantId', auth()->id())->first();
        $SchProgress = ScholarshipDetail::where('fkApplicantId',auth()->id())->orderBy('id', 'ASC')->get();

        //dd($SchProgress);
        
        if(count($SchProgress) == 0)
        {
            $scholarshipname = 'not found';
        } else  {
            $scholarshipname = Listscholarship::find($SchProgress[0]->fkSchlrId);
        }

        $status = request()->session()->get('status');
         return view('frontend.progressDetail', compact('scholarshipname','SchProgress', 'userdetail','applicantDetail', 'hash', 'status'));
   }
}
