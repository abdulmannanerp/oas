<?php

namespace App\Http\Controllers\Frontend;

use App\Model\Semester;
use Carbon\Carbon;
use App\Model\Applicant;
use App\Model\Programme;
use App\Model\Application;
use App\Model\ChallanDetail;
use App\Model\ApplicantDetail;
use App\Model\ApplicationStatus;
use App\Model\ProgrammeScheduler;
use App\Http\Controllers\Controller;
use App\Model\PreviousQualification;
use App\Model\ProgramFee;
use App\Model\ChallanGrouping;

class SelectProgramController extends Controller
{

    protected $currentSemester;
    public function __construct()
    {
        $this->currentSemester = $this->getCurrentSemester()->pkSemesterId;
    }

    public function index($hash = '')
    {
        $adminId = $this->decodeAdminData($hash);
        $status = request()->session()->get('status');
        $custom_error = request()->session()->get('custom_error');
        if (request()->method() == 'POST') {
            $this->validate(request(), [
                'program' => 'required'

            ]);
            // Dev Mannan: Code for application count for Level 1 candidates
            $applied_applications = Application::where('fkApplicantId', auth()->id())->where('fkSemesterId', $this->currentSemester)->whereNotNull('fkProgramId')->where('a_status', 1)->get();
            $userdetail_for_preference = Applicant::where('userId', auth()->id())->first();
            $total_programs_applied = 0;
            $total_engineering_count = 0;
            if ($applied_applications) {
                foreach ($applied_applications as $ap) {
                    if ($ap->fkProgramId == 189 && $userdetail_for_preference->fkGenderId == 3) {
                        $total_engineering_count += 1;
                    }
                }
                if (in_array(189, request('program')) && $userdetail_for_preference->fkGenderId == 3) {
                    $total_engineering_count += 1;
                }

                $total_programs_applied = $total_engineering_count + $applied_applications->count()  + count(request('program'));
                // $total_programs_applied = $applied_applications->count() + count(request('program'));
            } else {
                if (in_array(189, request('program')) && $userdetail_for_preference->fkGenderId == 3) {
                    $total_programs_applied = count(request('program')) + 1;
                } else {
                    $total_programs_applied = count(request('program'));
                }
                // $total_programs_applied = count(request('program'));
            }
            $userdetail_for_program_limitation = Applicant::where('userId', auth()->id())->first();
            // Dev Mannan: Line commented for max programs from 5
            // if($userdetail_for_program_limitation->fkLevelId == 1 && $total_programs_applied > 5){
            //     return back()->with('custom_error', 'For BS Level you cannot apply more than 5 programs');
            // }
            // Dev Mannan: Code for application count for Level 1 candidates ends

            // Dev Mannan: Code for special check through which one candidate(of every level) can only select atmost 1 program starts

            // $specialApplication = Application::where('fkApplicantId', auth()->id())->where('fkSemesterId', $this->currentSemester)->where('new_application_status', 'new')->first();

            // if(count(request('program')) == 1 && $specialApplication === null ){

            // }else{
            //     return back()->with('custom_error', 'You can only select one program');
            // }

            // Dev Mannan: Code for special check through which one candidate(of every level) can only select atmost 1 program Ends



            foreach (request('program') as $programs) {
                $preference1 = NULL;
                $preference2 = NULL;
                $preference3 = NULL;
                $userdetail_for_preference = Applicant::where('userId', auth()->id())->first();
                if ($programs == 189 && $userdetail_for_preference->fkGenderId == 3) {
                    if (request('first_radio') == '' || request('second_radio') == '' || request('third_radio') == '') {
                        return back()->with('custom_error', 'In case of BS Engineering (EE, CE & ME), Preferences must be selected');
                    } else {
                        if (request('first_radio') == 'first') {
                            $preference1 = 132;
                        } elseif (request('first_radio') == 'second') {
                            $preference2 = 132;
                        } elseif(request('first_radio') == 'third'){
                            $preference3 = 132;
                        }

                        if (request('second_radio') == 'first') {
                            $preference1 = 131;
                        } elseif (request('second_radio') == 'second') {
                            $preference2 = 131;
                        }elseif (request('second_radio') == 'third') {
                            $preference3 = 131;
                        }

                        if (request('third_radio') == 'first') {
                            $preference1 = 157;
                        } elseif (request('third_radio') == 'second') {
                            $preference2 = 157;
                        }elseif (request('third_radio') == 'third') {
                            $preference3 = 157;
                        }

                        // dd('1', $preference1,'2', $preference2,'3', $preference3);
                        // $preference1 = request('first_prefer');
                        // $preference2 = request('second_prefer'); 
                        // $preference3 = request('third_prefer');                       
                    }
                }
                if ($programs == 189 && $userdetail_for_preference->fkGenderId == 2) {
                    $preference1 = 131;
                }

                if ($programs == 174) {
                    if (request('mba_experience') < 3) {
                        return back()->with('custom_error', 'MBA-EXECUTIVE (Weekend Program) Requires minumum 3 years work experience');
                    } else {
                        $mba_experience = request('mba_experience');
                    }
                }
                // $chemistry_total = NULL;
                // $chemistry_obtained = NULL;
                // $physics_total = NULL;
                // $physics_obtained = NULL;
                // $maths_total = NULL;
                // $maths_obtained = NULL;
                // if ($programs == 157) {
                //     // dd(request('chemistry_total'), request('chemistry_obtained'), request('physics_total'), request('physics_obtained'), request('maths_total'), request('maths_obtained'));
                //     if (request('chemistry_total') == '' || request('chemistry_obtained') == '' || request('physics_total') == '' || request('physics_obtained') == '' || request('maths_total') == '' || request('maths_obtained') == '') {
                //         return back()->with('custom_error', 'In case of BS Civil Engineering, Given Fields must be filled with chemistry, pysics and mathematics marks');
                //     } else {
                //         $chemistry_total = request('chemistry_total');
                //         $chemistry_obtained = request('chemistry_obtained');
                //         $physics_total = request('physics_total');
                //         $physics_obtained = request('physics_obtained');
                //         $maths_total = request('maths_total');
                //         $maths_obtained = request('maths_obtained');
                //     }
                // }
                // $ssc_chemistry_total = NULL;
                // $ssc_chemistry_obtained = NULL;
                // $ssc_physics_total = NULL;
                // $ssc_physics_obtained = NULL;
                // $ssc_maths_total = NULL;
                // $ssc_maths_obtained = NULL;
                // if ($programs == 189) {
                //     if (request('ssc_chemistry_total') == '' || request('ssc_chemistry_obtained') == '' || request('ssc_physics_total') == '' || request('ssc_physics_obtained') == '' || request('ssc_maths_total') == '' || request('ssc_maths_obtained') == '') {
                //         return back()->with('custom_error', 'In case of BS Engineering (EE, CE & ME), Given Fields must be filled with chemistry, pysics and mathematics marks');
                //     } else {
                //         $ssc_chemistry_total = request('ssc_chemistry_total');
                //         $ssc_chemistry_obtained = request('ssc_chemistry_obtained');
                //         $ssc_physics_total = request('ssc_physics_total');
                //         $ssc_physics_obtained = request('ssc_physics_obtained');
                //         $ssc_maths_total = request('ssc_maths_total');
                //         $ssc_maths_obtained = request('ssc_maths_obtained');
                //     }
                // }
                if ($programs == 66) {
                    if (request('ms_usuludin_specialization') == '') {
                        return back()->with('custom_error', 'In case of MS Usuluddin Please select Specialization');
                    } else {
                        $ms_usuludin_specialization = request('ms_usuludin_specialization');
                    }
                }
                if ($programs == 65) {
                    if (request('phd_usuludin_specialization') == '') {
                        return back()->with('custom_error', 'In case of PHD Usuluddin Please select Specialization');
                    } else {
                        $phd_usuludin_specialization = request('phd_usuludin_specialization');
                    }
                }
                $nullAppli = Application::where('fkProgramId', NULL)->where('fkApplicantId', request('userId'))->where('fkSemesterId', $this->currentSemester)->first();
                $program = Programme::where('pkProgId', $programs)->first();
                // condition when first application ever entered
                if ($nullAppli) {
                    Application::where('id', $nullAppli->id)->update(
                        [
                            'fkProgramId' => $programs,
                            'fkFacId' => $program->fkFacId,
                            'fkDepId' => $program->fkDepId,
                            'preferrence1' => $preference1,
                            'preferrence2' => $preference2,
                            'preferrence3' => $preference3,
                            'a_status' => 1,

                            'new_application_date' => date("Y-m-d H:i:s"),
                            'new_application_status' => 'new'
                        ]
                    );
                    if ($programs == 174) {
                        PreviousQualification::where('fkApplicantId', request('userId'))->where('fkSemesterId', $this->currentSemester)->update(
                            [
                                'mba_experience' => $mba_experience
                            ]
                        );
                    }
                    // if ($programs == 157) {
                    //     PreviousQualification::where('fkApplicantId', request('userId'))->where('fkSemesterId', $this->currentSemester)->update(
                    //         [
                    //             'chemistry_total' => $chemistry_total,
                    //             'chemistry_obtained' => $chemistry_obtained,
                    //             'physics_total' => $physics_total,
                    //             'physics_obtained' => $physics_obtained,
                    //             'maths_total' => $maths_total,
                    //             'maths_obtained' => $maths_obtained
                    //         ]
                    //     );
                    // }
                    // if ($programs == 189) {
                    //     PreviousQualification::where('fkApplicantId', request('userId'))->where('fkSemesterId', $this->currentSemester)->update(
                    //         [
                    //             'ssc_chemistry_total' => $ssc_chemistry_total,
                    //             'ssc_chemistry_obtained' => $ssc_chemistry_obtained,
                    //             'ssc_physics_total' => $ssc_physics_total,
                    //             'ssc_physics_obtained' => $ssc_physics_obtained,
                    //             'ssc_maths_total' => $ssc_maths_total,
                    //             'ssc_maths_obtained' => $ssc_maths_obtained
                    //         ]
                    //     );
                    // }
                    if ($programs == 66) {
                        PreviousQualification::where('fkApplicantId', request('userId'))->where('fkSemesterId', $this->currentSemester)->update(
                            [
                                'ms_usuludin_specialization' => $ms_usuludin_specialization
                            ]
                        );
                    }
                    if ($programs == 65) {
                        PreviousQualification::where('fkApplicantId', request('userId'))->where('fkSemesterId', $this->currentSemester)->update(
                            [
                                'phd_usuludin_specialization' => $phd_usuludin_specialization
                            ]
                        );
                    }
                    $challanDetail = ChallanDetail::where('fkApplicationtId', $nullAppli->id)->where('fkSemesterId', $this->currentSemester)->where('fkProgramId', $programs)->where('challan_type', 1)->first();
                    if (!$challanDetail) {
                        ChallanDetail::create([
                            'fkApplicationtId' => $nullAppli->id,
                            'fkSemesterId' => $this->currentSemester,
                            'fkProgramId' => $programs,
                            'challan_type' => 1,
                            'admission_processing_fee' => $program->fee,
                            'overseas_admission_processing_fee' => 12000,
                            'total' => $program->fee
                        ]);
                    }
                } else {
                    $nullAppli = Application::where('fkProgramId', $programs)->where('fkApplicantId', request('userId'))->where('fkSemesterId', $this->currentSemester)->first();
                    $applicationIdForGrouping = '';
                    $challanIdForGrouping = '';
                    if (!$nullAppli) {
                        // check if applied in program after or before save
                        $appliSaved = ApplicationStatus::where('fkApplicantId', request('userId'))->where('fkSemesterId', $this->currentSemester)->first();
                        if ($appliSaved->step_save == 1) {
                            $fkCurrentStatus = 2;
                        } else {
                            $fkCurrentStatus = 1;
                        }
                        $newApplication = Application::create(
                            [
                                'fkApplicantId' => request('userId'),
                                'fkProgramId' => $programs,
                                'fkFacId' => $program->fkFacId,
                                'fkDepId' => $program->fkDepId,
                                'fkCurrentStatus' => $fkCurrentStatus,
                                'fkSemesterId' => $this->currentSemester,
                                'preferrence1' => $preference1,
                                'preferrence2' => $preference2,
                                'preferrence3' => $preference3,
                                'a_status' => 1,

                                'new_application_date' => date("Y-m-d H:i:s"),
                                'new_application_status' => 'new'
                            ]
                        );
                        if ($programs == 174) {
                            PreviousQualification::where('fkApplicantId', request('userId'))->where('fkSemesterId', $this->currentSemester)->update(
                                [
                                    'mba_experience' => $mba_experience
                                ]
                            );
                        }
                        // if ($programs == 157) {
                        //     PreviousQualification::where('fkApplicantId', request('userId'))->where('fkSemesterId', $this->currentSemester)->update(
                        //         [
                        //             'chemistry_total' => $chemistry_total,
                        //             'chemistry_obtained' => $chemistry_obtained,
                        //             'physics_total' => $physics_total,
                        //             'physics_obtained' => $physics_obtained,
                        //             'maths_total' => $maths_total,
                        //             'maths_obtained' => $maths_obtained
                        //         ]
                        //     );
                        // }
                        // if ($programs == 189) {
                        //     PreviousQualification::where('fkApplicantId', request('userId'))->where('fkSemesterId', $this->currentSemester)->update(
                        //         [
                        //             'ssc_chemistry_total' => $ssc_chemistry_total,
                        //             'ssc_chemistry_obtained' => $ssc_chemistry_obtained,
                        //             'ssc_physics_total' => $ssc_physics_total,
                        //             'ssc_physics_obtained' => $ssc_physics_obtained,
                        //             'ssc_maths_total' => $ssc_maths_total,
                        //             'ssc_maths_obtained' => $ssc_maths_obtained
                        //         ]
                        //     );
                        // }
                        if ($programs == 66) {
                            PreviousQualification::where('fkApplicantId', request('userId'))->where('fkSemesterId', $this->currentSemester)->update(
                                [
                                    'ms_usuludin_specialization' => $ms_usuludin_specialization
                                ]
                            );
                        }
                        if ($programs == 65) {
                            PreviousQualification::where('fkApplicantId', request('userId'))->where('fkSemesterId', $this->currentSemester)->update(
                                [
                                    'phd_usuludin_specialization' => $phd_usuludin_specialization
                                ]
                            );
                        }
                        $challanDetail = ChallanDetail::where('fkApplicationtId', $newApplication->id)->where('fkSemesterId', $this->currentSemester)->where('fkProgramId', $programs)->where('challan_type', 1)->first();
                        $chId = '';
                        if (!$challanDetail) {
                            $chId = ChallanDetail::create([
                                'fkApplicationtId' => $newApplication->id,
                                'fkSemesterId' => $this->currentSemester,
                                'fkProgramId' => $programs,
                                'challan_type' => 1,
                                'admission_processing_fee' => $program->fee,
                                'overseas_admission_processing_fee' => 12000,
                                'total' => $program->fee
                            ]);
                        }
                        $applicationIdForGrouping = $newApplication->id;
                        $challanIdForGrouping = $chId->id;
                    } else {
                        Application::where('id', $nullAppli->id)->update(
                            [
                                'a_status' => 1,
                                'new_application_date' => date("Y-m-d H:i:s"),
                                'new_application_status' => 'new'
                            ]
                        );

                        $challanDetailForGrouping = ChallanDetail::where('fkApplicationtId', $nullAppli->id)->where('fkSemesterId', $this->currentSemester)->where('fkProgramId', $programs)->where('challan_type', 1)->first();
                        $applicationIdForGrouping = $nullAppli->id;
                        $challanIdForGrouping = $challanDetailForGrouping->id;
                    }

                    // Dev Mannan: Code for program save in gropuping method for fkLevel 1 only starts
                    $this->groupingSingleApplication($userdetail_for_program_limitation->fkLevelId, $programs, $program->fkFacId, $applicationIdForGrouping, $challanIdForGrouping);
                    // Dev Mannan: Code for program save in gropuping method for fkLevel 1 only ends

                }
            }
            ApplicationStatus::where('fkApplicantId', request('userId'))->where('fkSemesterId', $this->currentSemester)->update(
                [
                    'step_programs' => 1
                ]
            );
            // Dev Mannan: PQM update/create for all applications
            $applications = Application::with(['program', 'faculty', 'program.programscheduler', 'applicant', 'applicant.previousQualification'])->where('fkApplicantId', request('userId'))->where('fkSemesterId', $this->currentSemester)->whereNotNull('fkProgramId')->get();
            if (!$applications->isEmpty()) {
                foreach ($applications as $application) {
                    $this->generateAndStorePQM($application);
                }
            }
            $this->logMaintenance($hash, request('userId'), 'Program');
            if ($userdetail_for_program_limitation->fkLevelId == 1) {
                // exit;
                return redirect()->route('selectpreference', $hash);
            } else {
                // return redirect()->route('savedetail', $hash);
                return redirect()->route('otherdetail', $hash);
            }
            // return back()->with('status', 'Information updated successfully!');
        } else {
            $previousQualification = PreviousQualification::where('fkApplicantId', auth()->id())->where('fkSemesterId', $this->currentSemester)->first();
            $programmeScheduler = ProgrammeScheduler::join('tbl_oas_programme', 'tbl_oas_programme_schedule.fkProgramId', '=', 'tbl_oas_programme.pkProgId')
                ->select('tbl_oas_programme_schedule.fkGenderId', 'tbl_oas_programme_schedule.fkProgramId')
                ->orderby('tbl_oas_programme.fkLeveliId', 'ASC')
                ->orderby('tbl_oas_programme.fkFacId', 'ASC')
                ->orderby('tbl_oas_programme.fkDepId', 'ASC')
                ->with('program')
                ->where('tbl_oas_programme_schedule.status', 1)
                ->where('tbl_oas_programme.status', 1)
                ->where(function ($query) use ($hash) {
                    if ($hash == '') {
                        $query->where('tbl_oas_programme_schedule.lastDate', '>=', Carbon::now()->toDateTimeString());
                    } else {
                        $adminId = $this->decodeAdminData($hash);
                        $users = \DB::table('tb_users')->where('id', '=', $adminId)->get()->first();
                        if (is_null($users)) {
                            echo '401 Unauthorized Access';
                            exit;
                        }
                    }
                })->get();
            //  dd($programmeScheduler);
            $applicationStatus = ApplicationStatus::where('fkApplicantId', auth()->id())->where('fkSemesterId', $this->currentSemester)->first();
            if ($applicationStatus->step_education != 1) {
                return redirect('frontend/previousqualification')->with('custom_error', 'You need to fill Previous Qualification');
            }
            // Dev Mannan: Code for re-launching admission 
            $newApp = Application::where('fkApplicantId', auth()->id())->where('fkSemesterId', $this->currentSemester)->where('new_application_status', 'new')->first();
            if (empty($newApp)) {
                $newapplicant  = 'can_apply';
                // echo 'No record';
            } else {
                $newapplicant  = 'cannot_apply';
                // echo 'Already applied';
            }
            // dd($newApp);

            $application = Application::where('fkApplicantId', auth()->id())->where('fkSemesterId', $this->currentSemester)->whereNotNull('fkProgramId')->where('a_status', 1)->get(); // ->where('a_status', 1)
            $userdetail = Applicant::where('userId', auth()->id())->first();
            $applicantDetail = ApplicantDetail::where('fkApplicantId', auth()->id())->first();
            return view('frontend.selectProgram', compact('custom_error', 'status', 'hash', 'applicantDetail', 'userdetail', 'programmeScheduler', 'previousQualification', 'application', 'newapplicant'));
        }
    }

    public function groupingSingleApplication($fkLevelId = '', $singleProgram = '', $facId = '', $applicationId = '', $challanId = '')
    {

        // $programs=35;
        $appliSaved = ApplicationStatus::where('fkApplicantId', request('userId'))->where('fkSemesterId', $this->currentSemester)->first();
        if ($appliSaved->step_save == 1 && $fkLevelId == 1) {
            $alreadyExists = \DB::select("SELECT id from `tbl_oas_challan_grouping`  WHERE fkApplicantId = " . request('userId') . " and (programId_1 = $singleProgram  or programId_2 = $singleProgram or programId_3 = $singleProgram)");
            if (!$alreadyExists) {
                // echo ' <br />does not exists';
                $getLastAppliedSameFaculty = ChallanGrouping::where('fkApplicantId', request('userId'))->where('fkFacId', $facId)->orderby('id', 'desc')->first();
                // No application found against this faculty. So we need to insert just one at the begining
                if ($getLastAppliedSameFaculty) {
                    
                    // echo ' <br />Record already found against this faculty' . $facId;
                    // foreach ($getAllAppliedSameFaculty as $g) {
                        // case 1: where programId_2 is null then insert at programId_2
                        if ($getLastAppliedSameFaculty->programId_2 == NULL) {
                            // echo '<br />Entered on Program 2';
                            ChallanGrouping::where('id', $getLastAppliedSameFaculty->id)->update(
                                [
                                    'programId_2' => $singleProgram,
                                    'applicationId_2' => $applicationId,
                                ]
                            );
                            // case 3: where programId_3 is null then insert at programId_3
                        } elseif ($getLastAppliedSameFaculty->programId_3 == NULL) {
                            // echo '<br /> Entered on Program 3';
                            ChallanGrouping::where('id', $getLastAppliedSameFaculty->id)->update(
                                [
                                    'programId_3' => $singleProgram,
                                    'applicationId_3' => $applicationId,
                                ]
                            );
                            // if both 2 and 3 is null then create a new one with first value
                        } else {
                            // echo '<br />New row has been created';
                            ChallanGrouping::create(
                                [
                                    'fkApplicantId' => request('userId'),
                                    'fkFacId' => $facId,
                                    'challanId' => $challanId,
                                    'programId_1' => $singleProgram,
                                    'applicationId_1' => $applicationId,
                                    'fkSemesterId' => $this->currentSemester,
                                ]
                            );
                        }
                    // }
                } else {
                    // Here we need to insert new record with first value
                    // echo ' <br />No Record found against this faculty' . $facId. ' So new row has been created';
                    ChallanGrouping::create(
                        [
                            'fkApplicantId' => request('userId'),
                            'fkFacId' => $facId,
                            'challanId' => $challanId,
                            'programId_1' => $singleProgram,
                            'applicationId_1' => $applicationId,
                            'fkSemesterId' => $this->currentSemester,
                        ]
                    );
                }
            }
            // print_r($getAllAppliedSameFaculty);
            // exit;
        }
    }


    public function deleteApplication()
    {
        $id = (request('id')) ? request('id') : '';
        if ($id) {
            Application::where('id', $id)->update(
                [
                    'a_status' => 2
                ]
            );
            $appli = Application::where('id', $id)->first();
            $noAppli = Application::where('fkApplicantId', $appli->fkApplicantId)->where('fkSemesterId', $this->currentSemester)->where('a_status', 1)->get();
            if ($noAppli->isEmpty()) {
                ApplicationStatus::where('fkApplicantId', $appli->fkApplicantId)->where('fkSemesterId', $this->currentSemester)->update(
                    [
                        'step_programs' => 0
                    ]
                );
            }
            return redirect()->route('selectprogram');
        }
    }

    public function reApply()
    {
        $id = (request('id')) ? request('id') : '';

        if ($id) {
            Application::where('id', $id)->update(
                [
                    'new_application_date' => date("Y-m-d H:i:s"),
                    'new_application_status' => 'new'
                ]
            );

            return back()->with('custom_error', 'You have successfully Re-Applied in the program');
        }
    }

    public function getProgramFee()
    {
        $id = (request('id')) ? request('id') : '';
        if ($id) {
            return ProgramFee::where('fkProgramId', $id)->get();
        }
    }
}
