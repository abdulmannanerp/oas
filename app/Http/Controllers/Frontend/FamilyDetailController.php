<?php

namespace App\Http\Controllers\Frontend;
use App\Http\Controllers\Controller;


use App\Model\Applicant;
use App\Model\ApplicantDetail;
use App\Model\ApplicationStatus;

class FamilyDetailController extends Controller{
    protected $currentSemester;
    public function __construct()
    {
        $this->currentSemester = $this->getCurrentSemester()->pkSemesterId;
    }

    public function index ($hash = ''){
        $adminId = $this->decodeAdminData($hash);
        $status = request()->session()->get('status');
        $userdetail = Applicant::where('userId',auth()->id())->first();
        if ( request()->method() == 'POST' ) {
            $applicationStatus = ApplicationStatus::where('fkApplicantId', auth()->id())->where('fkSemesterId', $this->currentSemester)->first();
            if($applicationStatus->step_save == 1 && $adminId == ''){
                return redirect()->route('otherdetail');
            }
            $this->validate(request(), [
                'father_status' => 'required',
                'dependent' => 'numeric|required',
                'monthly_income' => 'numeric|required',
                'guardian_spouse' => 'required',
                'father_occupation' => 'required',
                'father_phone' => 'numeric|required',
                'carrier' => 'numeric|required',
                'father_cnic' => 'numeric|required'
            ]);
            if($userdetail->fkNationality == 1){
                $this->validate(request(), [
                    'father_mobile' => 'numeric|required'                
                ]);
            }
            // if(request('father_status') || request('dependent') || request('monthly_income') || trim(request('guardian_spouse')) || trim(request('father_occupation')) || request('father_phone') || request('father_mobile') || trim(request('account_title')) || request('account_number') || trim(request('bank_name')) || trim(request('branch_name'))){
                $phone = preg_replace('/[^0-9]/', '', request('father_mobile'));
                $phone = '92'.request('carrier').$phone;
                
                if($userdetail->fkNationality == 2 || $userdetail->fkNationality == 3){
                    $phone = preg_replace('/[^0-9]/', '', request('carrier'));
                }
                ApplicantDetail::where('fkApplicantId', request('userId'))->update(
                    [
                        'fatherStatus' => request('father_status'),
                        'dependants' => request('dependent'),
                        'monthlyIncome' => request('monthly_income'),
                        'Guradian_Spouse' => trim(request('guardian_spouse')),
                        'fatherOccupation' => trim(request('father_occupation')),
                        'fatherPhone' => request('father_phone'),
                        'fatherMobile' => $phone,
                        'father_cnic' => request('father_cnic'),

                        'titleBankAccount' => trim(request('account_title')),
                        'accountNo' => request('account_number'),
                        'bankName' => trim(request('bank_name')),
                        'bankBranch' => trim(request('branch_name'))
                    ]
                );
                ApplicationStatus::where('fkApplicantId', request('userId'))->where('fkSemesterId', $this->currentSemester)->update(
                    [
                        'step_family'=> 1                    
                    ]
                );
                if($adminId != ''){
                    ApplicantDetail::where('fkApplicantId', request('userId'))->update(
                        [
                            'modifiedBy' => $adminId
                        ]
                    );
                }
            // }
            $this->logMaintenance($hash, request('userId'), 'family Details');
            return redirect()->route('otherdetail', $hash);
            // return back()->with('status', 'Information updated successfully!');
        }else{
            $applicantDetail = ApplicantDetail::where('fkApplicantId', auth()->id())->first();
            // $applicationStatus = ApplicationStatus::where('fkApplicantId', auth()->id())->first();
            $userdetail = Applicant::where('userId',auth()->id())->first();
            return view('frontend.familyDetail', compact('status','hash','userdetail', 'applicantDetail'));
        }
    }
}
