<?php

namespace App\Http\Controllers\Frontend;
use App\Http\Controllers\Controller;

use Carbon\Carbon;
use App\Model\Applicant;
use App\Model\ApplicantDetail;
use App\Model\ApplicationStatus;
use App\Model\ApplicantAddress;
use App\Model\City;

class OtherDetailController extends Controller{
    protected $currentSemester;
    public function __construct()
    {
        $this->currentSemester = $this->getCurrentSemester()->pkSemesterId;
    }
    public function index ($hash = ''){
        $adminId = $this->decodeAdminData($hash);
        $status = request()->session()->get('status');
        if ( request()->method() == 'POST' ) {
            $userdetail = Applicant::where('userId',auth()->id())->first(); 
            $applicationStatus = ApplicationStatus::where('fkApplicantId', auth()->id())->where('fkSemesterId', $this->currentSemester)->first();
            // if($applicationStatus->step_save == 1 && $adminId == ''){
            //     return redirect()->route('savedetail');
            // }
            $this->validate(request(), [
                'blood_group' => 'required',
                // 'disability' => 'required',
                'marital_status' => 'required',
                // 'religion' => 'required',
                // 'find_us' => 'required',

                'permanent_city' => 'required',
                'permanent_address' => 'required',

                'mailing_city' => 'required',
                'mailing_address' => 'required',

                'father_status' => 'required',
                'dependent' => 'numeric|required',
                'monthly_income' => 'numeric|required',
                'guardian_spouse' => 'required',
                'father_occupation' => 'required',
                'father_phone' => 'numeric|required',
                'carrier' => 'numeric|required',
                'father_cnic' => 'numeric|required'

            ]);

            if($userdetail->fkNationality == 1){
                $this->validate(request(), [

                    'father_mobile' => 'numeric|required',

                    'permanent_carrier' => 'numeric|required',
                    'mailing_carrier' => 'numeric|required',
                    'permanent_phone' => 'numeric|required',
                    'mailing_phone' => 'numeric|required'
                ]);
            }

            $phone = preg_replace('/[^0-9]/', '', request('father_mobile'));
            $phone = '92'.request('carrier').$phone;
            
            $permanent_phone = preg_replace('/[^0-9]/', '', request('permanent_phone'));
            $permanent_phone = '92'.request('permanent_carrier').$permanent_phone;
            $mailing_phone = preg_replace('/[^0-9]/', '', request('mailing_phone'));
            $mailing_phone = '92'.request('mailing_carrier').$mailing_phone;

            if($userdetail->fkNationality == 2 || $userdetail->fkNationality == 3){
                $permanent_phone = preg_replace('/[^0-9]/', '', request('overseas_permanent_carrier'));
                $mailing_phone = preg_replace('/[^0-9]/', '', request('overseas_mailing_carrier'));

                $phone = preg_replace('/[^0-9]/', '', request('carrier'));
            }

            // dd($permanent_phone, $mailing_phone);
            ApplicantAddress::where('fkApplicantId', request('userId'))->update(
                [
                    'cityPmt' => request('permanent_city'),
                    'phonePmt' => $permanent_phone,
                    'addressPmt' => request('permanent_address'),
                    'areaPmt' => request('permanent_area'),

                    'cityPo' => request('mailing_city'),
                    'phonePo' => $mailing_phone,
                    'addressPo' => request('mailing_address'),
                    'areaPo' => request('mailing_area'),
                ]
            );
            // if(request('disability') != 'None'){
            //     $this->validate(request(), [
            //         'issuing_authority' => 'required',
            //         'nature_of_disability' => 'required',
            //         'cause_of_disability' => 'required',
            //         'wheel_chair' => 'required',
            //         'accomodation_required' => 'required',
            //         'disabled_accessibility' => 'required',
            //         'special_arrangements' => 'required',
            //         'hssc_arrangements' => 'required'
                    
            //     ]);
            // }
            ApplicantDetail::where('fkApplicantId', request('userId'))->update(
                [
                    'blood' => request('blood_group'),
                    'married' => request('marital_status'),
                    'xStudent' => trim(request('reg_no')),

                    // 'hobbie1' => trim(request('hobby_1')),
                    // 'hobbie2' => trim(request('hobby_2')),
                    // 'hobbie3' => trim(request('hobby_3')),

                    // 'activity1' => trim(request('activity_1')),
                    // 'prize1' => trim(request('prize_1')),
                    // 'awardBy1' => trim(request('awarded_by_1')),

                    // 'activity2' => trim(request('activity_2')),
                    // 'prize2' => trim(request('prize_2')),
                    // 'awardBy2' => trim(request('awarded_by_2')),
                    
                    // 'activity3' => trim(request('activity_3')),
                    // 'prize3' => trim(request('prize_3')),
                    // 'awardBy3' => trim(request('awarded_by_3')),

                    // 'religion' => request('religion'),
                    'facebook_url' => trim(request('facebook_url')),
                    'twitter_url' => trim(request('twitter_url')),

                    // 'find_us' =>request('find_us'),

                    // 'disable' => request('disability'),
                    // 'issuing_authority' => trim(request('issuing_authority')),
                    // 'nature_of_disability' => trim(request('nature_of_disability')),
                    // 'cause_of_disability' => trim(request('cause_of_disability')),
                    // 'wheel_chair' => trim(request('wheel_chair')),

                    // 'accomodation_required' =>  trim(request('accomodation_required')),
                    // 'disabled_accessibility' =>  trim(request('disabled_accessibility')),
                    // 'special_arrangements' =>  trim(request('special_arrangements')),
                    // 'hssc_arrangements' =>  trim(request('hssc_arrangements')),


                    // 'place_of_birth' => request('place_of_birth') ?? NULL,
                    // 'passport_number' => request('passport_number') ?? NULL,
                    // 'date_issue' => Carbon::parse(request('date_issue')) ?? NULL,
                    // 'place_issue' => request('place_issue') ?? NULL,
                    // 'hec_letter_no' => request('hec_letter_no') ?? NULL,
                    // 'hec_letter_date' => Carbon::parse(request('hec_letter_date')) ?? NULL, 
                    // 'pakistani_visa' => request('pakistani_visa') ?? NULL,
                    // 'type_of_visa' => request('type_of_visa') ?? NULL,
                    // 'visa_issue_date' => Carbon::parse(request('visa_issue_date')) ?? NULL,
                    // 'visa_expiry_date' => Carbon::parse(request('visa_expiry_date')) ?? NULL

                    'fatherStatus' => request('father_status'),
                    'dependants' => request('dependent'),
                    'monthlyIncome' => request('monthly_income'),
                    'Guradian_Spouse' => trim(request('guardian_spouse')),
                    'fatherOccupation' => trim(request('father_occupation')),
                    'fatherPhone' => request('father_phone'),
                    'fatherMobile' => $phone,
                    'father_cnic' => request('father_cnic'),

                    'titleBankAccount' => trim(request('account_title')),
                    'accountNo' => request('account_number'),
                    'bankName' => trim(request('bank_name')),
                    'bankBranch' => trim(request('branch_name'))
                ]
            );
            ApplicationStatus::where('fkApplicantId', request('userId'))->where('fkSemesterId', $this->currentSemester)->update(
                [
                    'step_other'=> 1                    
                ]
            );
            if($adminId != ''){
                ApplicantDetail::where('fkApplicantId', request('userId'))->update(
                    [
                        'modifiedBy' => $adminId
                    ]
                );
            }
            $this->logMaintenance($hash, request('userId'), 'Other Info');
            return redirect()->route('savedetail', $hash);
            // return back()->with('status', 'Information updated successfully!');
        }else{
            $applicantDetail = ApplicantDetail::where('fkApplicantId', auth()->id())->first();
            // $applicationStatus = ApplicationStatus::where('fkApplicantId', auth()->id())->first();
            $applicantAddress = ApplicantAddress::where('fkApplicantId', auth()->id())->first();
            $city = City::all();
            $userdetail = Applicant::where('userId',auth()->id())->first();
            return view('frontend.otherDetail', compact('status','hash','userdetail', 'applicantDetail', 'applicantAddress', 'city'));
        }
    }
}
