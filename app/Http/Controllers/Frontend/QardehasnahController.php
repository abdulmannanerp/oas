<?php

namespace App\Http\Controllers\Frontend;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Nationality;
use App\Model\Listscholarship;
use App\Model\ApplicantDetail;
use App\Model\Applicant;
use App\Model\Application;
use App\Model\ApplicantAddress;
use App\Model\City;
use App\Model\Country;
use App\Model\District;
use App\Model\Province;
use App\Model\Semester;
use App\Model\ScholarshipInformation;
use App\Model\ScholarshipFamilyDetail;
use App\Model\ScholarshipFamilyAssets;
use App\Model\ScholarshipDetail;
use Barryvdh\DomPDF\Facade as PDF;

class QardehasnahController extends Controller
{
   public function index($hash = '')
   {
     
      $adminId = $this->decodeAdminData($hash);

      if ( request()->method() == 'POST' )
      {
         $this->validate(request(), [

            //'reg_number' => 'required',
            //'cgpa' => 'required',
            'semester_complete' => 'required',
            'semester_left' => 'required',
            'last_inst_attend' => 'required',
            'last_inst_fee' => 'required',
            'family_mem_studing' => 'required',
            'family_mem_earning' => 'required',
            'land_income' => 'required',
            'misc_income' => 'required',
            'monthly_income' => 'required',
            'gas_bill' => 'required',
            'electricity_bill' => 'required',
            'telephone_bill' => 'required',
            'water_bill' => 'required',
            'edu_expenditure' => 'required',
            'family_accommodation' => 'required',
            'food_expense' => 'required',
            'medical_expense' => 'required',
            'pocketmoney' => 'required',
            'monthly_expensiture' => 'required',
            'vehicles' => 'required',
            'land_size' => 'required',
            'accommodation_type' => 'required',
            'property_value' => 'required',
            'vehicle_type' => 'required',
            'land_value' => 'required',
            'home_value' => 'required',
            'other_property' => 'required',
            'vehicle_engine_capacity' => 'required',
            'accommodation_land' => 'required',
            'bank_balance' => 'required',
            'stock' => 'required',
            'misc' => 'required',
            'total_assets' => 'required',
            'remarks' => 'required',
            'request' => 'required',
            'total_marks' => 'required',
            'marks_obtain' => 'required',
            'sch_semester' => 'required',

         ]);


         $ScholarshipInformation = new ScholarshipInformation();


         if($this->userApplyForTheScholoship(auth()->id(), request('listscholarship'), request('sch_semester')))
         {



            //Personal Detail For Scholarship Store in tbl_sch_personal_detail//

           $ScholarshipInformation = new ScholarshipInformation();
           $ScholarshipInformation->fkApplicantId=auth()->id();
           $ScholarshipInformation->fkSchlrId= request('listscholarship');
           //dd(request('listscholarship'));
           $ScholarshipInformation->sch_semester=request('sch_semester');
           $ScholarshipInformation->reg=request('reg_number');
           $ScholarshipInformation->cgpa=request('cgpa');
           $ScholarshipInformation->sem_complete=request('semester_complete');
           $ScholarshipInformation->sem_left=request('semester_left');
           $ScholarshipInformation->last_inst_attend=request('last_inst_attend');
           $ScholarshipInformation->inst_fee=request('last_inst_fee');
           $ScholarshipInformation->year_of_passing=request('year_of_passing');
           $ScholarshipInformation->total_marks=request('total_marks');
           $ScholarshipInformation->marks_obtain=request('marks_obtain');
           $ScholarshipInformation->save();

           //Family Income Details store in tbl_sch_family_detail//

           $ScholarshipFamilyDetail = new ScholarshipFamilyDetail();
           $ScholarshipFamilyDetail->fkApplicantId=auth()->id();
           $ScholarshipFamilyDetail->fkSchlrId= request('listscholarship');
           $ScholarshipFamilyDetail->family_mem_studing=request('family_mem_studing');
           $ScholarshipFamilyDetail->family_mem_earning=request('family_mem_earning');
           $ScholarshipFamilyDetail->mother_income=request('mother_income');
           $ScholarshipFamilyDetail->land_income=request('land_income');
           $ScholarshipFamilyDetail->misc_income=request('misc_income');
           $ScholarshipFamilyDetail->monthly_income=request('monthly_income');
           $ScholarshipFamilyDetail->supporting_person=request('supporting_person');
           $ScholarshipFamilyDetail->death_date=request('death_date');
           $ScholarshipFamilyDetail->gas_bill=request('gas_bill');
           $ScholarshipFamilyDetail->elect_bill=request('electricity_bill');
           $ScholarshipFamilyDetail->tel_bill=request('telephone_bill');
           $ScholarshipFamilyDetail->water_bill=request('water_bill');
           $ScholarshipFamilyDetail->edu_expense=request('edu_expenditure');
           $ScholarshipFamilyDetail->acc_expense=request('family_accommodation');
           $ScholarshipFamilyDetail->food_expense=request('food_expense');
           $ScholarshipFamilyDetail->medical_expense=request('medical_expense');
           $ScholarshipFamilyDetail->pocket_money=request('pocketmoney');
           $ScholarshipFamilyDetail->monthly_expenditure=request('monthly_expensiture');
           $ScholarshipFamilyDetail->save();


           //Family Assets Details store in tbl_sch_applicant_assets//

           $ScholarshipFamilyAssets = new ScholarshipFamilyAssets();
           $ScholarshipFamilyAssets->fkApplicantId=auth()->id();
           $ScholarshipFamilyAssets->fkSchlrId= request('listscholarship');
           $ScholarshipFamilyAssets->vehicle_category=serialize(request('vehiclecategory'));
           $ScholarshipFamilyAssets->land_type=serialize(request('landtype'));
           $ScholarshipFamilyAssets->vehicals=request('vehicles');
           $ScholarshipFamilyAssets->vehicle_type=request('vehicle_type');
           $ScholarshipFamilyAssets->engine_cap=request('vehicle_engine_capacity');
           $ScholarshipFamilyAssets->land_size=request('land_size');
           $ScholarshipFamilyAssets->land_value=request('land_value');
           $ScholarshipFamilyAssets->acco_loc=request('accommodation_land');
           $ScholarshipFamilyAssets->acco_size=request('accommodation_type');
           $ScholarshipFamilyAssets->home_value=request('home_value');
           $ScholarshipFamilyAssets->home_status=request('home_status');
           $ScholarshipFamilyAssets->other_property=request('other_property');
           $ScholarshipFamilyAssets->property_value=request('property_value');
           $ScholarshipFamilyAssets->bank_bal=request('bank_balance');
           $ScholarshipFamilyAssets->stock=request('stock');
           $ScholarshipFamilyAssets->misc=request('misc');
           $ScholarshipFamilyAssets->total_asset=request('total_assets');
           $ScholarshipFamilyAssets->cattle_status=request('cattle_status');
           $ScholarshipFamilyAssets->no_of_cattles=request('no_of_cattles');
           $ScholarshipFamilyAssets->source_of_financing=request('source_of_financing');
           $ScholarshipFamilyAssets->remarks=request('remarks');
           $ScholarshipFamilyAssets->reason_for_apply=request('request');
           $ScholarshipFamilyAssets->save();

           //Store Complete Scholarship Details in tbl_sch_details //
            
           $ScholarshipDetail = new ScholarshipDetail();
           $ScholarshipDetail->fkApplicantId=auth()->id();
           $ScholarshipDetail->fkSchlrId= request('listscholarship');
           $ScholarshipDetail->feeverification= '0';
           $ScholarshipDetail->conductverfication= '0';
           $ScholarshipDetail->status='0';
           $ScholarshipDetail->save();
         }else{
            return redirect('frontend/qard-e-hasnah')->with('message', 'You have already apply for this schollership');
         }
      }

         $province = Province::all();
         //$district = District::where('pkDistId', $applicantDetail->fkDistrict)->first();
         $nationalilty = Nationality::where('nationalitystatus', 1)->get();
         $listscholarship = Listscholarship::get();
         $status = request()->session()->get('status');
         $userdetail = Applicant::where('userId',auth()->id())->first();
         $city = City::all();
         $Scholarshipdetail = ScholarshipDetail::where('fkApplicantId', auth()->id())->first();
         $scholarshipinformation = ScholarshipInformation::where('fkApplicantId', auth()->id())->first();
         $scholarshipFamilyDetail = ScholarshipFamilyDetail::where('fkApplicantId', auth()->id())->first();
         $scholarshipFamilyAssets = ScholarshipFamilyAssets::where('fkApplicantId', auth()->id())->first();
         $applicantAddress = ApplicantAddress::where('fkApplicantId', auth()->id())->first();
         $applicantDetail = ApplicantDetail::where('fkApplicantId', auth()->id())->first();
         return view('frontend.qard-e-hasnah', compact('userdetail','applicantDetail', 'hash', 'status', 'nationalilty','listscholarship', 'applicantAddress', 'city', 'scholarshipinformation', 'scholarshipFamilyDetail', 'scholarshipFamilyAssets', 'Scholarshipdetail', 'province'));
   }


   // public function edit()
   // {
      
   // }


   public function userApplyForTheScholoship($appId, $schId,$semester){
     // $scholarshipFamilyDetail = ScholarshipFamilyDetail::where('fkApplicantId', $appId)->where('fkSchlrId', $schId)->count();
         
     // $scholarshipFamilyAssets = ScholarshipFamilyAssets::where('fkApplicantId', $appId)->where('fkSchlrId',$schId)->count();
      $isexist=ScholarshipInformation::where('fkApplicantId', $appId)->where('fkSchlrId',$schId)->where('sch_semester',$semester)->count();
     
      if($isexist>0){
         return false;
      }
      return true;

     // return ($scholarshipFamilyDetail > 0 || $scholarshipFamilyAssets > 0) ? false : true;
   }


   public function printScholarship($applicantID,  $scholarshipId )
    {
        
       $current_uri = request()->segments();

        if ( isset($applicantID) && isset($scholarshipId)) {
            $authenticateUser = auth()->id();
            $applicant = Applicant::where('userId',$applicantID)->first();

         $nationalilty = Nationality::where('nationalitystatus', 1)->get();
         $listscholarship = Listscholarship::get();
         $status = request()->session()->get('status');
         $city = City::all();
         $Scholarshipdetail = ScholarshipDetail::where('fkApplicantId', $applicantID)->where('fkSchlrId',$scholarshipId)->first();
         $scholarshipname = Listscholarship::find($scholarshipId);
         $scholarshipinformation = ScholarshipInformation::where('fkApplicantId', $applicantID)->where('fkSchlrId',$scholarshipId)->first();
         
         $scholarshipFamilyDetail = ScholarshipFamilyDetail::where('fkApplicantId', $applicantID)->where('fkSchlrId',$scholarshipId)->first();
         
         $scholarshipFamilyAssets = ScholarshipFamilyAssets::where('fkApplicantId', $applicantID)->where('fkSchlrId',$scholarshipId)->first();
         $applicantAddress = ApplicantAddress::where('fkApplicantId', $applicantID)->first();
         $applicantDetail = ApplicantDetail::where('fkApplicantId', $applicantID)->first();
         

      return view('frontend.printScholarship', compact('nationalilty', 'applicant', 'applicantDetail', 'applicantAddress', 'scholarshipFamilyAssets', 'scholarshipFamilyDetail', 'scholarshipinformation', 'Scholarshipdetail', 'city', 'listscholarship','scholarshipname' ));




            if ( !in_array($request->oas_app_id, $authentcatedUserApplications ) ) 
                return '401 Unauthorized Access';
        }
       
        $oas_applications = Application::where('id', $request->oas_app_id)->first();
        $oas_applicatdetail = ApplicantDetail::where('fkApplicantId',$oas_applications->fkApplicantId)->first();
        // $this->thumbPrint('http://admission.iiu.edu.pk/'.$oas_applicatdetail->pic); 
        return view('frontend.printScholarship', compact('oas_applications'));
    }
}
