<?php

namespace App\Http\Controllers\Frontend;
use Imagick;

use Carbon\Carbon;
use App\Model\Country;
use App\Model\District;
use App\Model\Province;
use App\Model\Semester;
use App\Model\Applicant;
use App\Model\Application;
use App\Model\Nationality;
use App\Model\ProgramLevel;
use App\Model\ApplicantDetail;
use App\Model\ApplicantAddress;
use App\Model\ApplicationStatus;
use App\Model\ProgrammeScheduler;
use App\Http\Controllers\Controller;
use Intervention\Image\Facades\Image;
use App\Model\City;


class PersonalDetailController extends Controller{
    protected $currentSemester;

    public function __construct()
    {
        $this->currentSemester = $this->getCurrentSemester()->pkSemesterId;
    }

    public function index ($hash = ''){
        // dd(phpinfo());
        $adminId = $this->decodeAdminData($hash);
        $status = request()->session()->get('status');
        $semester = Semester::where('status', 1)->first();
        if(!file_exists(storage_path('app/public/images/'.$semester->title))) {
            mkdir(storage_path('app/public/images/'.$semester->title), 0777, true);
            mkdir(storage_path('app/public/images/'.$semester->title.'/profile'), 0777, true);
            mkdir(storage_path('app/public/images/'.$semester->title.'/challan'), 0777, true);
        }
        if ( request()->method() == 'POST' ) {
            $applicationStatus = ApplicationStatus::where('fkApplicantId', auth()->id())->where('fkSemesterId', $this->currentSemester)->first();
            if($applicationStatus->step_save == 1 && $adminId == ''){
                // Code for picture update of duplicate records starts
                // $applicantDetail = ApplicantDetail::where('fkApplicantId', auth()->id())->first();
                // if(!$applicantDetail->pic){
                //     $this->validate(request(), [
                //         'picture' => 'required|image|mimes:jpeg,png,jpg,gif,bmp,tiff|max:4096',
                //     ]); 
                // }
                // if (request()->hasFile('picture')) {
                //     $image = request()->file('picture');
                //     $filename    = request('userId').'.'.$image->getClientOriginalExtension();
                //     $im = new Imagick ($image->getRealPath());
                //     $im->adaptiveResizeImage('140','140');
                //     $im->writeImage(storage_path('app/public/images/'.$semester->title.'/profile/'.rand(pow(10, 6-1), pow(10, 6)-1).'_'.$filename));
                //     ApplicantDetail::where('fkApplicantId', request('userId'))->update(
                //         [
                //             'pic' => 'images/'.$semester->title.'/profile/'.rand(pow(10, 6-1), pow(10, 6)-1).'_'.$filename
                //         ]
                //     );
                // }
                // Code for picture update of duplicate records ends
                return redirect()->route('addressdetail');
            }
            $applicantDetail = ApplicantDetail::where('fkApplicantId', auth()->id())->first();
            if(!$applicantDetail->pic){
                $this->validate(request(), [
                    'picture' => 'required|image|mimes:jpeg,png,jpg,gif,bmp,tiff|max:4096',
                ]); 
            }
            $this->validate(request(), [
                'full_name' => 'required',
                'father_name' => 'required',
                'gender' => 'required',
                'nationality' => 'required',
                'level' => 'required',
                'date_of_birth' => 'required',
                'country' => 'required',
                
                // 'permanent_city' => 'required',
                // 'permanent_address' => 'required',
                // 'permanent_area' => 'required',
                // 'mailing_city' => 'required',
                // 'mailing_address' => 'required',
                // 'mailing_area' => 'required',

                'disability' => 'required',
                'religion' => 'required',
                'find_us' => 'required',
                'transport_facility' => 'required',
                'hostel_facility' => 'required'

            ]);

            if(request('disability') != 'None'){
                $this->validate(request(), [
                    'issuing_authority' => 'required',
                    'nature_of_disability' => 'required',
                    'cause_of_disability' => 'required',
                    'wheel_chair' => 'required',
                    'accomodation_required' => 'required',
                    'disabled_accessibility' => 'required',
                    'special_arrangements' => 'required',
                    'hssc_arrangements' => 'required'
                    
                ]);
            }
             

            if(request('nationality') == 1){
                $this->validate(request(), [
                    'domicile' => 'required',
                    'district' => 'required',
                    'phone' => 'numeric|required',
                    'carrier' => 'numeric|required',
                    
                    // 'permanent_carrier' => 'numeric|required',
                    // 'mailing_carrier' => 'numeric|required',
                    // 'permanent_phone' => 'numeric|required',
                    // 'mailing_phone' => 'numeric|required'
                ]);
                $phone = preg_replace('/[^0-9]/', '', request('phone'));
                $phone = '92'.request('carrier').$phone;
            }

            // $permanent_phone = preg_replace('/[^0-9]/', '', request('permanent_phone'));
            // $permanent_phone = '92'.request('permanent_carrier').$permanent_phone;
            // $mailing_phone = preg_replace('/[^0-9]/', '', request('mailing_phone'));
            // $mailing_phone = '92'.request('mailing_carrier').$mailing_phone;

            if(request('nationality') == 2){
                $this->validate(request(), [
                    'nicop' => 'required'
                ]);
            }
            if(request('nationality') == 2 || request('nationality') == 3){
                $this->validate(request(), [
                    'overseas_phone' => 'numeric|required'
                ]);
                $phone = request('overseas_phone');

                // $permanent_phone = preg_replace('/[^0-9]/', '', request('overseas_permanent_carrier'));
                // $mailing_phone = preg_replace('/[^0-9]/', '', request('overseas_mailing_carrier'));
            }

            // dd(Carbon::parse(request('date_of_birth')));
            // dd(date('Y-m-d', strtotime(request('date_of_birth'))));
            if (request()->hasFile('picture')) {
                $image = request()->file('picture');
                $filename    = request('userId').'.'.$image->getClientOriginalExtension();
                // $image_resize = Image::make($image->getRealPath());              
                // $image_resize->resize(140, 140);
                // $image_resize->save(storage_path('app/public/images/'.$filename));

                $im = new Imagick ($image->getRealPath());
                $im->adaptiveResizeImage('140','140');
                $rand = rand(pow(10, 6-1), pow(10, 6)-1);
                $im->writeImage(storage_path('app/public/images/'.$semester->title.'/profile/'.$rand.'_'.$filename));
                ApplicantDetail::where('fkApplicantId', request('userId'))->update(
                    [
                        'pic' => 'images/'.$semester->title.'/profile/'.$rand.'_'.$filename
                    ]
                );
            }
            if(request('gender') == 'others'){
                $selectedGender = 3;
                $others = request('gender');
                
            }else{
                $selectedGender = request('gender');
                $others = NULL;
            }
            Applicant::where('userId', request('userId'))->update(
                [
                    'name' => request('full_name'),
                    'fkGenderId' => $selectedGender,
                    'gender_other' => $others,
                    'fkNationality' => request('nationality'),
                    'fkLevelId' => request('level')                    
                ]
            );
            ApplicantDetail::where('fkApplicantId', request('userId'))->update(
                [
                    'fatherName' => request('father_name'),
                    'DOB' => Carbon::parse(request('date_of_birth')),
                    'fkDomicile' => request('domicile'),
                    'fkDistrict' => request('district'),
                    'afghan_refugee' => (request('afghan_refugee') == '1')? 1 : 0,
                    'mobile' => $phone,

                    'disable' => request('disability'),
                    'issuing_authority' => trim(request('issuing_authority')),
                    'nature_of_disability' => trim(request('nature_of_disability')),
                    'cause_of_disability' => trim(request('cause_of_disability')),
                    'wheel_chair' => trim(request('wheel_chair')),

                    'accomodation_required' =>  trim(request('accomodation_required')),
                    'disabled_accessibility' =>  trim(request('disabled_accessibility')),
                    'special_arrangements' =>  trim(request('special_arrangements')),
                    'hssc_arrangements' =>  trim(request('hssc_arrangements')),

                    'place_of_birth' => request('place_of_birth') ?? NULL,
                    'passport_number' => request('passport_number') ?? NULL,
                    'date_issue' => Carbon::parse(request('date_issue')) ?? NULL,
                    'place_issue' => request('place_issue') ?? NULL,
                    'hec_letter_no' => request('hec_letter_no') ?? NULL,
                    'hec_letter_date' => Carbon::parse(request('hec_letter_date')) ?? NULL, 
                    'pakistani_visa' => request('pakistani_visa') ?? NULL,
                    'type_of_visa' => request('type_of_visa') ?? NULL,
                    'visa_issue_date' => Carbon::parse(request('visa_issue_date')) ?? NULL,
                    'visa_expiry_date' => Carbon::parse(request('visa_expiry_date')) ?? NULL,

                    'religion' => request('religion'),
                    'find_us' =>request('find_us'),
                    'transport_facility' =>request('transport_facility'),
                    'hostel_facility' =>request('hostel_facility')
                ]
            );
            if(request('nationality') == 2){
                ApplicantDetail::where('fkApplicantId', request('userId'))->update(
                    [
                        'nicop' => trim(request('nicop'))
                    ]
                );  
            }
            ApplicantAddress::where('fkApplicantId', request('userId'))->update(
                [
                    'fkCountryIdPmt'=> request('country'),

                    // 'cityPmt' => request('permanent_city'),
                    // 'phonePmt' => $permanent_phone,
                    // 'addressPmt' => request('permanent_address'),
                    // 'areaPmt' => request('permanent_area'),

                    // 'cityPo' => request('mailing_city'),
                    // 'phonePo' => $mailing_phone,
                    // 'addressPo' => request('mailing_address'),
                    // 'areaPo' => request('mailing_area'),
                ]
            );
            ApplicationStatus::where('fkApplicantId', request('userId'))->where('fkSemesterId', $this->currentSemester)->update(
                [
                    'step_personal'=> 1 ,
                    'step_address'=> 1                    
                ]
            );
            if($adminId != ''){
                Applicant::where('userId', request('userId'))->update(
                    [
                        'updatedBy' => $adminId,
                        'updatedDtm' => Carbon::now()                   
                    ]
                );
                ApplicantDetail::where('fkApplicantId', request('userId'))->update(
                    [
                        'modifiedBy' => $adminId
                    ]
                );
                ApplicantAddress::where('fkApplicantId', request('userId'))->update(
                    [
                        'modifiedBy'=> $adminId
                    ]
                );
            }
            $appliedPrograms = Application::where('fkApplicantId', request('userId'))->where('fkSemesterId', $this->currentSemester)->where('a_status','!=',2)->where('fkProgramId','!=',NULL)->get();
            if($appliedPrograms){
                foreach($appliedPrograms as $ap){
                    $pType = ProgrammeScheduler::where('fkProgramId',$ap->fkProgramId)->first();
                    if($pType->fkGenderId == 1){
                        continue;
                    }elseif($pType->fkGenderId != $selectedGender){
                        // dump($pType->fkGenderId);
                        Application::where('id', $ap->id)->update(
                            [
                                'a_status' => 0
                            ]
                        );
                    }elseif($pType->fkGenderId == $selectedGender){
                        // dump($pType->fkGenderId);
                        Application::where('id', $ap->id)->update(
                            [
                                'a_status' => 1
                            ]
                        );
                    }
                }
            }
            $this->logMaintenance($hash, request('userId'), 'Personal Info');
            return redirect()->route('previousqualification', $hash);
            // return back()->with('status', 'Information updated successfully!');
        }else{

            $nationalilty = Nationality::where('nationalitystatus', 1)->get();
            $level = ProgramLevel::where('status', 1)->get(); 
            $province = Province::all();
            $country = Country::all();
            $userdetail = Applicant::where('userId',auth()->id())->first();  
            $applicantDetail = ApplicantDetail::where('fkApplicantId', auth()->id())->first(); 
            $applicantAddress = ApplicantAddress::where('fkApplicantId', auth()->id())->first();
            $city = City::all();
            $district = District::where('pkDistId', $applicantDetail->fkDistrict)->first();
            // $applicationStatus = ApplicationStatus::where('fkApplicantId', auth()->id())->first();
            return view('frontend.personalDetail', compact('nationalilty', 'hash','level', 'province','country', 'userdetail', 'status', 'applicantDetail', 'applicantAddress', 'district', 'city'));
        }
    }
    public function getDistrict(){
        $id = (request('id')) ? request('id') : '';
        if ($id) {
            return District::where('fkProvId', $id)->get();
        }

    }
}
