<?php

namespace App\Http\Controllers;

use Exception;
use GuzzleHttp\Client;
use App\Model\Applicant;
use Illuminate\Http\Request;
use App\Model\AljamiaNewStudent;
use App\Jobs\MigrateToAljamiaJob;
use App\Model\AljamiaNewAcadRecord;
use App\Jobs\SplitMigrateToAljamiaJob;
use App\Jobs\MigrateSingleRecordToAljamia;
use App\Jobs\SendCnicAndGenderInformationToAljamia;

class MigrateToAljamia extends Controller
{
    public function index()
    {
        // Applicant::with(['detail', 'nationality', 'detail.province', 'detail.district', 'detail.semester', 'address', 'gender', 'programLevel', 'address', 'previousQualification'])
        //     ->whereHas('application')
        //     ->whereHas('application', function ($query) {
        //         $query->where('fkCurrentStatus', '>=', 2)->where('fkSemesterId', 13);
        //     })->chunk(500, function ($applicants) {
        //         MigrateToAljamiaJob::dispatch($applicants)->delay(now()->addSeconds(30));
        //     });
        // // MigrateToAljamiaJob::dispatch($test);
        // return 'added to queue';
    }

    public function splitMigrateToAljamia()
    {
        $applicants = Applicant::with([
                'detail',
                'nationality',
                'detail.province',
                'detail.district',
                'detail',
                'address',
                'address.country',
                'gender',
                'programLevel',
                'address',
                'previousQualification'
            ])
            ->whereHas('application', function ($query) {
                $query->where('fkCurrentStatus', '>=', 2)->where('fkSemesterId', 19)->where('new_application_status', 'new')->where('created_at', '>=', '2023-08-29');
            })
            ->get();
			
				
			 	//$query->where('fkCurrentStatus', '>=', 2)->where('fkSemesterId', 19)->where('new_application_status', 'new')->where('fkFacId', 10);
				//$query->where('fkCurrentStatus', '>=', 2)->where('fkSemesterId', 19)->where('new_application_status', 'new')->where('created_at', '>=', '2023-08-29');
			
            // foreach ( $applicants as $applicant ) {
            //     try {
            //         $info = $this->parseApplicantInfo($applicant);
            //         MigrateSingleRecordToAljamia::dispatch($info);
            //     } catch (Exception $e) {
            //         info('---Exception Start---');
            //         info($applicant->userId);
            //         info($e) ;
            //         info('---Exception End---');
            //     }
            // }
        foreach ($applicants as $applicant) {
            $content = json_decode(
                json_encode($this->parseApplicantInfo($applicant))
            );
            $gender = ($content->gender == 'Male') ? 'M' : 'F';
            $studentExists = AljamiaNewStudent::where('REGNO', $content->cnic)->first();
            if ( $studentExists ) {
                continue;
            }
            AljamiaNewStudent::create([
                 'REGNO' => $content->cnic,
                 'STUDNAME' => $content->name,
                 'STUDFATHERNAME' => $content->fatherName,
                 'NIDPASSNO' => $content->cnic,
                 'STUDPRESADD' => $content->addressPresent,
                 'STUDPERMADD' => $content->addressPermanent,
                 'STUDEMAIL' => $content->email,
                 'STUDPHONE' => $content->mobile,
                 'BATCHCODE' => '',
                 'STUDLOGIN' => '',
                 'STUDPASS' => '',
                 'NATIONALITY' => $content->nationality,
                 'DATEOFBIRTH' => date_format(date_create($content->dob),'d-m-Y'),
                 'CGPA' => '',
                 'CREDHRATTEMPT' => '',
                 'S_NO' => '',
                 'STUDPOINTS' => '',
                 'STATUS' => '',
                 'STUDPRESADDONE' => $content->addressPresent,
                 'STUDPRESADDTWO' => $content->addressPresent,
                 'STUDPRESADDCITY' => $content->cityPresent,
                 'STUDPRESADDZIP' => '',
                 'STUDPRESADDCOUNTRY' => $content->country,
                 'STUDPERMADDONE' => $content->addressPermanent,
                 'STUDPERMADDTWO' => $content->addressPermanent,
                 'STUDPERMADDCITY' => $content->cityPermanent,
                 'STUDPERMADDZIP' => '',
                 'STUDPERMADDCOUNTRY' => $content->country,
                 'SEX' => $gender,
                 'SECRETQUESTION' => '',
                 'SECRETANSWER' => '',
                 'ACADPROGCODE' => '',
                 'DEPCODE' => '',
                 'FACCODE' => '',
                 'OVERALLPERCENTAGE' => '',
                 'PROGCMPLTDATE' => '',
                 'HIFZ' => '',
                 'DEGREENO' => '',
                 'THESISTITLE' => '',
                 'READMIT' => '',
                 'CONV' => '',
                 'HIFZDATE' => '',
                 'DISTRICT' => $content->district,
                 'DOMICILE' => $content->domicile,
                 'STUDSTATUS' => '',
                 'STUDSTATUSDATE' => '',
                 'SPECIALIZATION' => '',
                 'REGNO_ALLOTED' => '',
                 'DISABILITY' => $content->disability,
				 'REGNO_ALLOTED_DATE' => ''
				 
				  
             ]);

             $education = $this->populateEducationArray($content);
             foreach ($education as $key => $value) {
                $degree = $value['degree'];
                $completionyear = $value['completionyear'];
                $univboard = $value['univboard'];
                $majorsubjects = $value['majorsubjects'];
                $marksobt = $value['marksobt'];
                $marksTotal = $value['total'];
                AljamiaNewAcadRecord::updateOrCreate([
                    'REGNO' => $content->cnic,
                    'DEGREE' => $degree,
                    'YEAROFEXAM' => $completionyear,
                    'UNIVBOARD' => $univboard,
                    'MAJORSUBJECTS' => $majorsubjects,
                    'MARKSOBT' => $marksobt,
                    'TOTAL' => $marksTotal,
                    'DIVISION' => '',
                    'PERCENTAGE' => '',
                    'BATCHCODE' => '',
                    'ACADPROGCODE' => '',
                    'DEPCODE' => '',
                    'FACCODE' => '',
                    'NIDPASSNO' => $content->cnic,
                    'THESISSUPERVISER' => '',
                    'APPROVAL_OF_SYNOP_DATE' => '',
                    'GAT_TEST' => '',
                    'COMPREHENSIVE_EXAM' => '',
                    'LAST_RESEARCH_TITLE' => ''
                ]);
            }
        }
        return 'record added to queue';
    }

    public function populateEducationArray($applicant)
    {
        $education = [];
        $education['ssc']['degree'] = 'SSC';
        $education['ssc']['univboard'] = $applicant->sscFrom;
        $education['ssc']['majorsubjects'] = $applicant->sscSubjects;
        $education['ssc']['marksobt'] = $applicant->sscObtainedMarks;
        $education['ssc']['total'] = $applicant->sscTotalMarks;
        $education['ssc']['completionyear'] = $applicant->sscDegreeYear;

        $hsscTotalMarks = '';
        $hsscObtainedMarsks = '';
        if ($applicant->hsscComposite) {
            $hsscTotalMarks = $applicant->hsscTotal;
            $hsscObtainedMarsks = $applicant->hsscComposite;
        } else {
            $hsscTotalMarks = (integer)$applicant->hssc1Total + (integer)$applicant->hssc2Total;
            $hsscObtainedMarsks = (integer)$applicant->hssc1 + (integer)$applicant->hssc2;
        }
        $education['hssc']['degree'] = 'HSSC';
        $education['hssc']['univboard'] = $applicant->hsscFrom;
        $education['hssc']['majorsubjects'] = $applicant->hsscSubjects;
        $education['hssc']['marksobt'] = $hsscObtainedMarsks;
        $education['hssc']['total'] = $hsscTotalMarks;
        $education['hssc']['completionyear'] = $applicant->hsscDegreeYear;

        if ($applicant->baBscMarksObtained) {
            $education['baBsc']['degree'] = 'BA BSC';
            $education['baBsc']['univboard'] = $applicant->baBscFrom;
            $education['baBsc']['majorsubjects'] = $applicant->baBscSubjects;
            $education['baBsc']['marksobt'] = $applicant->baBscMarksObtained;
            $education['baBsc']['total'] = $applicant->baBscTotalMarks;
            $education['baBsc']['completionyear'] = $applicant->baBscDegreeCompletionYear;
        }

        if ($applicant->mscMarksObtained) {
            $education['baBsc']['degree'] = 'MSC';
            $education['baBsc']['univboard'] = $applicant->mscFrom;
            $education['baBsc']['majorsubjects'] = $applicant->mscSubjects;
            $education['baBsc']['marksobt'] = $applicant->mscMarksObtained;
            $education['baBsc']['total'] = $applicant->mscTotalMarks;
            $education['baBsc']['completionyear'] = $applicant->mscDegreeCompletionYear;
        }

        if ($applicant->bsMarksObtained) {
            $education['bs']['degree'] = 'BS';
            $education['bs']['univboard'] = $applicant->bsFrom;
            $education['bs']['majorsubjects'] = $applicant->bsSubjects;
            $education['bs']['marksobt'] = $applicant->bsMarksObtained;
            $education['bs']['total'] = $applicant->bsTotalMarks;
            $education['bs']['completionyear'] = $applicant->bsDegreeCompletionYear;
        }

        if ($applicant->msMarksObtained) {
            $education['bs']['degree'] = 'MS';
            $education['bs']['univboard'] = $applicant->msFrom;
            $education['bs']['majorsubjects'] = $applicant->msSubjects;
            $education['bs']['marksobt'] = $applicant->msMarksObtained;
            $education['bs']['total'] = $applicant->msTotalMarks;
            $education['bs']['completionyear'] = $applicant->msDegreeCompletionYear;
        }

        return $education;
    }

    public function updateGender()
    {
        $applicants = Applicant::with([
            'gender'
        ])
        ->whereHas('application', function ($query) {
            $query->where('fkCurrentStatus', '>=', 2)->where('fkSemesterId', 13)->where('fkFacId', 2);
        })
        ->get();
        foreach ($applicants as $applicant) {
            $info = [];
            $info['gender'] = $this->cleanString($applicant->gender->gender) ?? '';
            $info['cnic']  = $this->cleanString($applicant->cnic) ?? '';
            SendCnicAndGenderInformationToAljamia::dispatch($info);
        }
        
        return 'done';
    }

    public function parseApplicantInfo($applicant)
    {
        $info = [];
        /* Basic Info */
        $info['userId']          = $this->cleanString($applicant->userId) ?? '';
        $info['name']            = $this->cleanString($applicant->name) ?? '';
        $info['email']           = $this->cleanString($applicant->email) ?? '';
        $info['gender']          = $this->cleanString($applicant->gender->gender) ?? '';
        $info['cnic']            = $this->cleanString($applicant->cnic) ?? '';
        $info['level']           = $this->cleanString($applicant->programLevel->programLevel) ?? '';
        $info['nationality']     = $this->cleanString($applicant->nationality->nationality) ?? '';
        $info['userCreationDate']= $this->cleanString($applicant->createdDtm) ?? '';
        $info['semester']        = $this->getCurrentSemester()->title ?? '';

        /** Detail */
        $info['dob']                       = $this->cleanString($applicant->detail->DOB) ?? '';
        $info['mobile']                    = $this->cleanString($applicant->detail->mobile) ?? '';
        $info['englishSpoken']             = $this->cleanString($applicant->detail->englishSpoken) ?? '';
        $info['englishWritten']            = $this->cleanString($applicant->detail->englishWritten) ?? '';
        $info['arabicSpoken']              = $this->cleanString($applicant->detail->arabicSpoken) ?? '';
        $info['arabicWritten']             = $this->cleanString($applicant->detail->arabicWritten) ?? '';
        $info['language3']                 = $this->cleanString($applicant->detail->language3) ?? '';
        $info['language3Spoken']           = $this->cleanString($applicant->detail->language3Spoken) ?? '';
        $info['language3Written']          = $this->cleanString($applicant->detail->language3Written) ?? '';
        $info['language4']                 = $this->cleanString($applicant->detail->language4) ?? '';
        $info['language4Spoken']           = $this->cleanString($applicant->detail->language4Spoken) ?? '';
        $info['language4Written']          = $this->cleanString($applicant->detail->language4Written) ?? '';
        $info['domicile']                  = (isset($applicant->detail->province->provName)) ?
                                                $this->cleanString($applicant->detail->province->provName) : '';
        $info['district']                  = (isset($applicant->detail->district->distName)) ?
                                                $this->cleanString($applicant->detail->district->distName) : '';
        $info['hobbie1']                   = $this->cleanString($applicant->detail->hobbie1) ?? '';
        $info['hobbie2']                   = $this->cleanString($applicant->detail->hobbie2) ?? '';
        $info['hobbie3']                   = $this->cleanString($applicant->detail->hobbie3) ?? '';
        $info['activity1']                 = $this->cleanString($applicant->detail->activity1) ?? '';
        $info['prize1']                    = $this->cleanString($applicant->detail->prize1) ?? '';
        $info['awardBy1']                  = $this->cleanString($applicant->detail->awardBy1) ?? '';
        $info['activity2']                 = $this->cleanString($applicant->detail->activity2) ?? '';
        $info['prize2']                    = $this->cleanString($applicant->detail->prize2) ?? '';
        $info['awardBy2']                  = $this->cleanString($applicant->detail->awardBy2) ?? '';
        $info['activity3']                 = $this->cleanString($applicant->detail->activity3) ?? '';
        $info['prize3']                    = $this->cleanString($applicant->detail->prize3) ?? '';
        $info['awardBy3']                  = $this->cleanString($applicant->detail->awardBy3) ?? '';
        $info['previousRegistrationNumber']= $this->cleanString($applicant->detail->xStudent) ?? '';
        $info['disability']                = $this->cleanString($applicant->detail->disable) ?? '';
        $info['bloodGroup']                = $this->cleanString($applicant->detail->blood) ?? '';
        $info['maritalStatus']             = $this->cleanString($applicant->detail->married) ?? '';
        $info['religion']                  = $this->cleanString($applicant->detail->religion) ?? '';
        $info['facebook']                  = $this->cleanString($applicant->detail->facebook_url) ?? '';
        $info['twitter']                   = $this->cleanString($applicant->detail->twitter_url) ?? '';

        $info['fatherName']      = $this->cleanString($applicant->detail->fatherName) ?? '';
        $info['fatherStatus']    = ($applicant->detail->fatherStatus == 1) ? 'Alive' : 'Deceased';
        $info['fatherDependants']= $this->cleanString($applicant->detail->dependants) ?? '';
        $info['fatherIncome']    = $this->cleanString($applicant->detail->monthlyIncome) ?? '';
        $info['fatherOccupation']= $this->cleanString($applicant->detail->fatherOccupation) ?? '';
        $info['fatherPhone']     = $this->cleanString($applicant->detail->fatherPhone) ?? '';
        $info['fatherMobile']    = $this->cleanString($applicant->detail->fatherMobile) ?? '';
        $info['fatherCnic']      = $this->cleanString($applicant->detail->father_cnic) ?? '';
        $info['guardianName']    = $this->cleanString($applicant->detail->Guradian_Spouse) ?? '';

        /** Bank Account Information */
        $info['bankAccountTitle']= $this->cleanString($applicant->detail->titleBankAccount) ?? '';
        $info['accountNo']       = $this->cleanString($applicant->detail->accountNo) ?? '';
        $info['bankName']        = $this->cleanString($applicant->detail->bankName) ?? '';
        $info['bankBranch']      = $this->cleanString($applicant->detail->bankBranch) ?? '';

        /** Address */
        $info['addressPresent']  = $this->cleanString($applicant->address->addressPo) ?? '';
        $info['cityPresent']     = $this->cleanString($applicant->address->cityPo) ?? '';
        $info['phonePresent']    = $this->cleanString($applicant->address->phonePo) ?? '';
        $info['areaPresent']     = $this->cleanString($applicant->address->areaPo) ?? '';
        $info['addressPermanent']= $this->cleanString($applicant->address->addressPmt) ?? '';
        $info['cityPermanent']   = $this->cleanString($applicant->address->cityPmt) ?? '';
        $info['phonePermanent']  = $this->cleanString($applicant->address->phonePmt) ?? '';
        $info['areaPermanent']   = $this->cleanString($applicant->address->areaPmt) ?? '';
        $info['fatherAddress']   = $this->cleanString($applicant->address->addressFather) ?? '';
        $info['cityFather']      = $this->cleanString($applicant->address->cityFather) ?? '';
        $info['phoneFather']     = $this->cleanString($applicant->address->phoneFather) ?? '';
        $info['areaFather']      = $this->cleanString($applicant->address->areaFather) ?? '';
        $info['country']     = $this->cleanString($applicant->address->country->countryName) ?? '';

        /** Previous Qualification */
        $info['sscTitle']        = $this->cleanString($applicant->previousQualification->SSC_Title) ?? '';
        $info['sscFrom']         = $this->cleanString($applicant->previousQualification->SSC_From) ?? '';
        $info['sscSubjects']     = $this->cleanString($applicant->previousQualification->SSC_Subject) ?? '';
        $info['sscObtainedMarks']= $this->cleanString($applicant->previousQualification->SSC_Composite) ?? '';
        $info['sscTotalMarks']   = $this->cleanString($applicant->previousQualification->SSC_Total) ?? '';
        $info['sscDegreeType']   = $this->cleanString($applicant->previousQualification->SSC_degree_type) ?? '';
        $info['sscDegreeYear']   = $this->cleanString($applicant->previousQualification->SSC_Year) ?? '';

        $info['hsscTitle']     = $this->cleanString($applicant->previousQualification->HSC_Title) ?? '';
        $info['hsscFrom']      = $this->cleanString($applicant->previousQualification->HSC_From) ?? '';
        $info['hsscSubjects']  = $this->cleanString($applicant->previousQualification->HSC_Subject) ?? '';
        $info['hssc1']         = $this->cleanString($applicant->previousQualification->HSSC1) ?? '';
        $info['hssc1Total']    = $this->cleanString($applicant->previousQualification->HSSC_Total1) ?? '';
        $info['hssc2']         = $this->cleanString($applicant->previousQualification->HSSC2) ?? '';
        $info['hssc2Total']    = $this->cleanString($applicant->previousQualification->HSSC_Total2) ?? '';
        $info['hsscComposite'] = $this->cleanString($applicant->previousQualification->HSSC_Composite) ?? '';
        $info['hsscTotal']     = $this->cleanString($applicant->previousQualification->HSSC_Total) ?? '';
        $info['hsscDegreeType']= $this->cleanString($applicant->previousQualification->HSC_degree_type) ?? '';
        $info['hsscDegreeYear']= $this->cleanString($applicant->previousQualification->HSC_Year) ?? '';
        
        $info['baBscTitle']               = $this->cleanString($applicant->previousQualification->BA_Bsc_Title) ?? '';
        $info['baBscFrom']                = $this->cleanString($applicant->previousQualification->BA_Bsc_From) ?? '';
        $info['baBscSubjects']            = $this->cleanString($applicant->previousQualification->BA_Bsc_Subject) ?? '';
        $info['baBscSpecialization']      = $this->cleanString($applicant->previousQualification->BA_Bsc_Specialization) ?? '';
        $info['baBscMarksObtained']       = $this->cleanString($applicant->previousQualification->BA_Bsc) ?? '';
        $info['baBscTotalMarks']          = $this->cleanString($applicant->previousQualification->BA_Bsc_Total) ?? '';
        $info['baBscDegreeCompletionYear']= $this->cleanString($applicant->previousQualification->BA_Bsc_Year) ?? '';
        $info['baBscDegreeType']          = $this->cleanString($applicant->previousQualification->BA_Bsc_degree_type) ?? '';

        $info['mscTitle']               = $this->cleanString($applicant->previousQualification->MSc_Title) ?? '';
        $info['mscFrom']                = $this->cleanString($applicant->previousQualification->MSc_From) ?? '';
        $info['mscSubjects']            = $this->cleanString($applicant->previousQualification->MSc_Subject) ?? '';
        $info['mscSpecialization']      = $this->cleanString($applicant->previousQualification->MSc_Specialization) ?? '';
        $info['mscMarksObtained']       = $this->cleanString($applicant->previousQualification->MSc) ?? '';
        $info['mscTotalMarks']          = $this->cleanString($applicant->previousQualification->MSc_Total) ?? '';
        $info['mscDegreeCompletionYear']= $this->cleanString($applicant->previousQualification->MSc_Year) ?? '';
        $info['mscDegreeType']          = $this->cleanString($applicant->previousQualification->MSc_degree_type) ?? '';

        $info['bsTitle']               = $this->cleanString($applicant->previousQualification->BS_Title) ?? '';
        $info['bsFrom']                = $this->cleanString($applicant->previousQualification->BS_From) ?? '';
        $info['bsSubjects']            = $this->cleanString($applicant->previousQualification->BS_Subject) ?? '';
        $info['bsSpecialization']      = $this->cleanString($applicant->previousQualification->BS_Specialization) ?? '';
        $info['bsExamSystem']          = $this->cleanString($applicant->previousQualification->BS_Exam_System) ?? '';
        $info['bsMarksObtained']       = $this->cleanString($applicant->previousQualification->BS) ?? '';
        $info['bsTotalMarks']          = $this->cleanString($applicant->previousQualification->BS_Total) ?? '';
        $info['bsDegreeCompletionYear']= $this->cleanString($applicant->previousQualification->BS_Year) ?? '';
        $info['bsDegreeType']          = $this->cleanString($applicant->previousQualification->BS_degree_type) ?? '';

        $info['msTitle']               = $this->cleanString($applicant->previousQualification->MS_Title) ?? '';
        $info['msFrom']                = $this->cleanString($applicant->previousQualification->MS_From) ?? '';
        $info['msSubjects']            = $this->cleanString($applicant->previousQualification->MS_Subject) ?? '';
        $info['msSpecialization']      = $this->cleanString($applicant->previousQualification->MS_Specialization) ?? '';
        $info['msExamSystem']          = $this->cleanString($applicant->previousQualification->MS_Exam_System) ?? '';
        $info['msMarksObtained']       = $this->cleanString($applicant->previousQualification->MS) ?? '';
        $info['msTotalMarks']          = $this->cleanString($applicant->previousQualification->MS_Total) ?? '';
        $info['msDegreeCompletionYear']= $this->cleanString($applicant->previousQualification->MS_Year) ?? '';
        $info['msDegreeType']          = $this->cleanString($applicant->previousQualification->MS_degree_type) ?? '';

        $info['resultAwaitingHSSC'] = $this->cleanString($applicant->previousQualification->resultAwaiting) ?? '';
        $info['resultAwaitingbaBsc']= $this->cleanString($applicant->previousQualification->resultwaitbsc) ?? '';
        $info['pqm']                = $this->cleanString($applicant->previousQualification->pqm) ?? '';

        $info['lawTestObtainedMarks'] = $this->cleanString($applicant->previousQualification->llb_obtained_marks) ?? '';
        $info['lawTestTotalMarks']    = $this->cleanString($applicant->previousQualification->llb_total_marks) ?? '';
        $info['lawTestCompletionDate']= $this->cleanString($applicant->previousQualification->llb_completion_date) ?? '';

        $info['mbaExecutiveExperience']= $this->cleanString($applicant->previousQualification->mba_experience) ?? '';
        return $info;
    }

    /**
     * Remove ",", "'" and "&" symbol from string
     * Oracle wants this to be cleaned
     */
    public function cleanString($string)
    {
        $removeCommaFromString           =  str_replace(",", "", $string);
        $removeApostropheFromString      = str_replace("'", "", $removeCommaFromString);
        return $removeAmpersandFromString= str_replace("&", "and", $removeApostropheFromString);
    }
}
