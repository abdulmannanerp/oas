<?php
namespace App\Http\Controllers;

use Google_Client;
use App\Aljamia\Course;
use App\Aljamia\Teacher;
use App\Jobs\ProcessCourses;
use Google_Service_Classroom;
use Google_Service_Exception;
use App\Mail\TeacherCourseCreated;
use Google_Service_Classroom_Course;
use Illuminate\Support\Facades\Mail;
use Google_Service_Classroom_Student;
use Google_Service_Classroom_Teacher;
use Google_Service_Classroom_CourseWork;
use Google_Service_Classroom_CourseAlias;

class googleClassroom extends Controller
{
    public function __construct() {
        $google_client = $this->getClient();
        $this->service = new Google_Service_Classroom($google_client);
     }

    public function index(){
        // require_once(__DIR__.'/../../../vendor/autoload.php');
        $google_client = $this->getClient();

        $service = new Google_Service_Classroom($google_client);

        // Dev Mannan: View Courses
    // Print the first 10 courses the user has access to.
        $optParams = array(
        'teacherId' => 'lms@iiu.edu.pk'
        );
        $results = $service->courses->listCourses($optParams);
        dd(count($results->getCourses()));
        if (count($results->getCourses()) == 0) {
            print "No courses found.\n";
        } else {
            print "Courses:\n";
            foreach ($results->getCourses() as $course) {
                echo '<br />';
                printf("%s (%s)\n", $course->getName(), $course->getId());
            }
        }

// Dev Mannan: Get coursework related to course
    // $courseId = '75938774825';
    // $results = $service->courses_courseWork->listCoursesCourseWork($courseId);
    // echo '<pre>';
    // print_r($results);
    // echo '</pre>';

        echo '<div align="center"><a href="'.$google_client->createAuthUrl().'">Login Button</a></div>';

    }
    public function addCourse($name='', $section='', $courseState='', $alias='', $courseLocalId='', $teacherEmail, $descriptionHeading='', $description='',$room='' ){
        // Add course(s) along with its alias
        $course = new Google_Service_Classroom_Course(array(
            'name' => $name,
            'section' => 'Section: '.$section,
            'descriptionHeading' => $descriptionHeading,
            'description' => $alias,
            'room' => $room,
            'ownerId' => 'me',
            'courseState' => $courseState
        ));
        $course = $this->service->courses->create($course);
        $courseId = $course->id;
        // $alias = 'd:'.$alias;
        // $courseAlias = new Google_Service_Classroom_CourseAlias(array(
        //     'alias' => $alias
        //     ));
        // $results = $this->service->courses_aliases->create($courseId, $courseAlias);
        Course::find($courseLocalId)->update([
            'g_class_created' => 1,
            'g_class_active' => 1,
            'g_course_url' => $course->alternateLink,
            'g_enrollment_code' => $course->enrollmentCode,
            'g_class_id' => $course->id,
            'g_class_created_at' => $course->creationTime
        ]);
        if ($teacherEmail ) {
            $this->addTeacher($course->id, $teacherEmail);
        }
        
        return 'Done';
        // return back()->with('status', 'Class created successfully');
    //     $result = array(
    //         'id' => $course->id,
    //         'creationTime' => $course->creationTime,
    //         'enrollmentCode' => $course->enrollmentCode,
    //         'alternateLink' => $course->alternateLink
    //     );

    //     return $result;
    }
    public function updateCourse($id='', $name='', $section='', $descriptionHeading='', $description='',$room='', $courseState='', $alias=''){
        if($id==''){
            dd('Invalild ID');
        }
        $course = $this->service->courses->get($id);
        if($name != ''){
            $course->name = $name;
        }
        if($section != ''){
            $course->section = $section;
        }
        if($descriptionHeading != ''){
            $course->descriptionHeading = $descriptionHeading;
        }
        if($description != ''){
            $course->description = $description;
        }
        if($room != ''){
            $course->room = $room;
        }
        if($courseState != ''){
            $course->courseState = $courseState;
        }
        $course = $this->service->courses->update($id, $course);
        $result = array(
            'id' => $course->id,
            'creationTime' => $course->creationTime,
            'enrollmentCode' => $course->enrollmentCode,
            'alternateLink' => $course->alternateLink,
            'name' => $course->name,
            'section' => $course->section,
            'descriptionHeading' => $course->descriptionHeading,
            'description' => $course->description,
            'room' => $course->room,
            'courseState' => $course->courseState
        );

        return $result;
    }

    public function updateCourseStatus($id = '', $courseState='', $courseId){
        if($id == ''){
            dd('Course ID is Null');
        }
        $course = $this->service->courses->get($id);
        if($courseState != ''){
            $course->courseState = $courseState;
            $course = $this->service->courses->update($id, $course);
            // $result = array(
            //     'id' => $course->id,
            //     'creationTime' => $course->creationTime,
            //     'enrollmentCode' => $course->enrollmentCode,
            //     'alternateLink' => $course->alternateLink,
            //     'name' => $course->name,
            //     'section' => $course->section,
            //     'descriptionHeading' => $course->descriptionHeading,
            //     'description' => $course->description,
            //     'room' => $course->room,
            //     'courseState' => $course->courseState
            // );
            Course::find($courseId)->update([
                'g_class_active' => 0
            ]);
            return back()->with('status', 'Course updated successfully');
            // return $result;
        }

    }
    public function deleteCourse($id='', $courseId=''){
        // if($id == ''){
        //     dd('Course ID is Null');
        // }
        // $results = $this->service->courses->delete($id);
        // if($results){
        //     Course::find($courseId)->update([
        //         'g_class_deleted' => 1,
        //         'g_class_deleted_at' => \Carbon\Carbon::now()
        //     ]);
        //     return back()->with('status', 'Class Deleted');
        //     // return 'deleted';
        // }

        
        $optParams = array(
            'teacherId' => 'lms@iiu.edu.pk'
            );
        $results = $this->service->courses->listCourses($optParams);
        foreach ($results->getCourses() as $course) {
            $this->service->courses->delete($course->getId());
        }
    }

    public function addTeacher($id='', $teacherEmail=''){
        // And Assign teacher to each course
        $data = '';
        if($teacherEmail == '' || $id == ''){
            dd('Class ID or Teacher Email cannot be empty');
        }
        $teacher = new Google_Service_Classroom_Teacher(array(
            'userId' => $teacherEmail
        ));
        try {
            $teacher = $this->service->courses_teachers->create($id, $teacher);
            $course = $this->service->courses->get($id);
            $teacherFolder = $course->teacherFolder->alternateLink;
            $data = array(
                'id' => $id,
                'alternateLink' => $teacherFolder
            );
            Course::where('g_class_id', $id)->update([
                'g_drive_url' => $teacherFolder
            ]);

        } catch (Google_Service_Exception $e) {
            if ($e->getCode() == 409) {
                printf("User '%s' is already a member of this course.\n", $teacherEmail);
            } else {
                throw $e;
            }
        }
        return $data;
        
    }
    public function deleteTeacher($id='', $teacherEmail=''){
        
        $data = '';
        if($teacherEmail == '' || $id == ''){
            dd('Class ID or Teacher Email cannot be empty');
        }
        try {
            $teacher = $this->service->courses_teachers->delete($id, $teacherEmail);
            if($teacher){
                $data = 'teacher_removed';
            }
        } catch (Google_Service_Exception $e) {
            if ($e->getCode() == 409) {
                printf("User '%s' is already a member of this course.\n", $teacherEmail);
              } else {
                throw $e;
            }
        }
        return $data;
    }
    public function testQuery(){
        // $dumper = $this->addCourse('Test Course', 'Test Secton', 'Test Description Heading', 'Test Description', '555', 'Active', 'alaisssss');
        // $dumper = $this->updateCourse('79490121567','', '', '', '', '', 'PROVISIONED', '');
        // $dumper = $this->deleteCourse('79490121567');
        // $dumper = $this->addTeacher('79580734690', 'admission.notifications@iiu.edu.pk');
        // $dumper = $this->deleteTeacher('79580734690', 'admissionalert.fsl@iiu.edu.pk');
        // dd($dumper);
        $this->index();
    }

    // public function testJob(){
    //     $sql = Course::with(['teacher'])->whereIn('id',[186,187])->get();
    //     foreach ( $sql as $course ) {
    //         ProcessCourses::dispatch($course);
    //     }
    //     return 'Done';
    // }
    // public function batchAdding(){
    //     $this->service->getClient()->setUseBatch(true);
    //     $batch = $this->service->createBatch();
    //     // $sql = \DB::select("SELECT * FROM raw group by Coursecode limit 10");
    //     $sql = Course::with(['teacher'])->take(10)->get();
    //     dump($sql);
    //     // dd('aaaa');
    //     foreach($sql as $db) {
    //         // $alias = $this->formatAlias($db->Acadprogname).'_'.$this->formatAlias($db->Coursecode).'_section_'.$this->formatAlias($db->Gender).'_'.$this->formatAlias($db->Semcode).'_'.$this->formatAlias($db->Batchname).'_'.$this->formatAlias($db->Facname).'_'.$this->formatAlias($db->Depname).'_'.$this->formatAlias($db->Acadprogname);
    //         // echo $db->Facname;
    //         $course = new Google_Service_Classroom_Course(array(
    //             'name' => $db->title.'_'.$db->id,
    //             'section' => $db->section,
    //             'ownerId' => 'me',
    //             'courseState' => 'ACTIVE'
    //         ));
    //         $course = $this->service->courses->create($course);
    //         $batch->add($course, $db->id);
    //     }
    //     $results = $batch->execute();
    //     dump($results);
    //     foreach($results as $responseId => $course) {
    //         $courseIdsql = substr($responseId, strpos($responseId,'-') + 1);
    //         if ($course instanceof Google_Service_Exception) {
    //             $e = $course;
    //             printf("Error adding courseId from db '%s' to the course: %s\n", $courseIdsql,
    //                 $e->getMessage());
    //         } else {
    //             Course::find($courseIdsql)->update([
    //                 'g_class_created' => 1,
    //                 'g_class_active' => 1,
    //                 'g_course_url' => $course->alternateLink,
    //                 'g_enrollment_code' => $course->enrollmentCode,
    //                 'g_class_id' => $course->id,
    //                 'g_class_created_at' => $course->creationTime
    //             ]);
    //         }
    //     }
    //     $this->service->getClient()->setUseBatch(false);
    // }


    // public function formatAlias($data){
    //     $data = trim($data);
    //     $data = str_replace(' ', '-', $data);
    //     $data = strtolower($data);
    //     return $data;
    // }

    public function getClient(){
        $scopes = [
            Google_Service_Classroom::CLASSROOM_COURSES,
                Google_Service_Classroom::CLASSROOM_ROSTERS,
                Google_Service_Classroom::CLASSROOM_COURSEWORK_STUDENTS
        ];
            //Make object of Google API Client for call Google API
            $google_client = new Google_Client();
            $google_client->setScopes($scopes);
            $google_client->setAuthConfig(__DIR__.'/../../../credentials.json');
            $google_client->setAccessType('offline');
            $google_client->setPrompt('select_account consent');
            // if(isset($_GET["code"])){
            $tokenPath = __DIR__.'/../../../token.json';
            if (file_exists($tokenPath)) {
                $accessToken = json_decode(file_get_contents($tokenPath), true);
                $google_client->setAccessToken($accessToken);
            }
            // If there is no previous token or it's expired.
            if ($google_client->isAccessTokenExpired()) {
                // Refresh the token if possible, else fetch a new one.
                if ($google_client->getRefreshToken()) {
                    $google_client->fetchAccessTokenWithRefreshToken($google_client->getRefreshToken());
                } else {
                    // Request authorization from the user.
                    $authUrl = $google_client->createAuthUrl();
                    $authCode = $_GET["code"];
                    // Exchange authorization code for an access token.
                    $accessToken = $google_client->fetchAccessTokenWithAuthCode($authCode);
                    $google_client->setAccessToken($accessToken);
                    // Check to see if there was an error.
                    if (array_key_exists('error', $accessToken)) {
                        throw new Exception(join(', ', $accessToken));
                    }
                }
                // Save the token to a file.
                if (!file_exists(dirname($tokenPath))) {
                    mkdir(dirname($tokenPath), 0700, true);
                }
                file_put_contents($tokenPath, json_encode($google_client->getAccessToken()));
            }
        // }
            return $google_client;
        

    }
	
    public function startQueue()
    {
        $courses = Course::with(['teacher', 'department', 'faculty'])
            ->where('faccode', 'FMS1')
            ->where('g_class_created', 0)
            ->where('teacher_id', '!=',  13)
            // ->take(10)
            ->get();
            dd('here');
        foreach ($courses as $course) {
            ProcessCourses::dispatch($course);
        }
        return 'Done';
    }

    public function sendEamilToTeacher()
    {
        $teachers = Teacher::with(['courses'])->whereHas('courses', function ($query) {
            $query->where('g_class_created', 1);
        })->where('course_email_sent', '0')->take(2)->get();
        foreach ($teachers as $teacher) {
            $when = now()->addSeconds(30);
            // $teacher->email
            // ->cc('teacher@iiu.edu.pk')
            Mail::to('khurramshahzad@iiu.edu.pk')->send(new TeacherCourseCreated($teacher));
            $teacher->course_email_sent = 1;
            $teacher->save();
        }
        return 'email processed';
    }

    public function emailViewed($id, $aljamiaId)
    {
        $teacher = Teacher::where('id', $id)->where('teacher_id_aljamia', $aljamiaId)->first();
        if ($teacher) {
            if ($teacher->course_email_viewed == 1) {
                return 'Already viewed';
            }
            $teacher->course_email_viewed = 1;
            $teacher->course_email_viewed_at = \Carbon\Carbon::now();
            $teacher->save();
        }
        return 'Done';
    }

    public function courseViewed($courseId = null)
    {
        if ($courseId) {
            $course = Course::find($courseId);
            if ($course) {
                if (! $course->course_link_viewed == 1) {
                    $course->course_link_viewed = 1;
                    $course->course_link_viewed_at = \Carbon\Carbon::now();
                    $course->save();
                }
                return redirect()->away($course->g_course_url);
            }
        }
        return redirect()->away('https://classroom.google.com');
    }
}
