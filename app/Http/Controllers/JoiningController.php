<?php

namespace App\Http\Controllers;

use App\Jobs\MigrateSingleRecordToAljamia;
use App\Model\Applicant;
use App\Model\Application;
use App\Model\Joining;
use Illuminate\Http\Request;

class JoiningController extends Controller
{
    /**
     * Return joining view
     */
    public function index()
    {
        $applications = '';
        $status = request()->session()->get('status');
        if (request()->method() == 'POST') {
            $applications = $this->searchApplicationsByFormNumber(request('form-numbers'));
        }
        return view('joining.index', compact('status', 'applications'));
    }

    /**
     * Saves the joining to database table
     */
    public function confirmJoining()
    {
        $applications = unserialize(request('application-ids'));
        foreach ($applications as $application) {
            $joining = Joining::create([
                'fkApplicationId'=> $application,
                'fkSemesterId'   => $this->getCurrentSemester()->pkSemesterId,
                'fkAdminId'      => auth()->guard('web')->id(),
                'date_of_joining'=> \Carbon\Carbon::now(),
            ]);
            $this->pushSingleJoiningToAljamia($joining);
        }
        return redirect()->route('joining')->with('status', 'Joining added successfully');
    }

    /**
     * Get applicant info for pushing to aljamia
     */
    public function pushSingleJoiningToAljamia($joining)
    {
        $applicant = Applicant::with([
            'detail',
            'nationality',
            'detail.province',
            'detail.district',
            'address',
            'gender',
            'programLevel',
            'address',
            'previousQualification'
        ])->whereHas('application', function ($query) use ($joining) {
            $query->where('id', $joining->fkApplicationId);
        })->first();
        $info = $this->parseApplicantInfo($applicant);
        MigrateSingleRecordToAljamia::dispatch($info)->onQueue('joining');
        $joining->pushed_to_aljamia = 1;
        $joining->save();
    }

    /**
     * Search appplicants by the form number provided
     */
    public function searchApplicationsByFormNumber($formNumbers)
    {
        $permissions = $this->getPermissions();
        $formNumbers = $this->splitFormNumberByComma($formNumbers);
        return Application::with(['applicant', 'program'])
            ->whereHas('applicant', function ($query) use ($permissions) {
                $query->whereIn('fkGenderId', $permissions);
            })->whereIn('id', $formNumbers)
            ->where('fkSemesterId', $this->getCurrentSemester()->pkSemesterId)
            ->get();
    }

    /**
     * Return array by splitting application ids by seperator i.e; comma
     */
    public function splitFormNumberByComma($formNumbers)
    {
        $formNumbers = explode(',', $formNumbers);
        $formNumbers = array_map(function ($form) {
            return trim($form);
        }, $formNumbers);
        return array_filter($formNumbers, function ($form) {
            if ($form) {
                return $form;
            }
        });
    }

    /**
     * Get current logged in user's permissions
     */
    public function getPermissions()
    {
        $permissions = $this->getCurrentUserPermissions()->fkGenderId;
        if ($permissions == 1) {
            return [1,2,3];
        }
        return explode(',', $permissions);
    }

    /**
     * Prepare the information & pass that to queue, This method is
     * extracted from queue because previousQualification was not being
     * eagerloaded in queue
     * probably the issue is in applicant model previousQualification relation
     */
    public function parseApplicantInfo($applicant)
    {
        $info = [];
        /* Basic Info */
        $info['userId']          = $this->cleanString($applicant->userId) ?? '';
        $info['name']            = $this->cleanString($applicant->name) ?? '';
        $info['email']           = $this->cleanString($applicant->email) ?? '';
        $info['gender']          = $this->cleanString($applicant->gender->gender) ?? '';
        $info['cnic']            = $this->cleanString($applicant->cnic) ?? '';
        $info['level']           = $this->cleanString($applicant->programLevel->programLevel) ?? '';
        $info['nationality']     = $this->cleanString($applicant->nationality->nationality) ?? '';
        $info['userCreationDate']= $this->cleanString($applicant->createdDtm) ?? '';
        $info['semester']        = $this->getCurrentSemester()->title ?? '';

        /** Detail */
        $info['dob']                       = $this->cleanString($applicant->detail->DOB) ?? '';
        $info['mobile']                    = $this->cleanString($applicant->detail->mobile) ?? '';
        $info['englishSpoken']             = $this->cleanString($applicant->detail->englishSpoken) ?? '';
        $info['englishWritten']            = $this->cleanString($applicant->detail->englishWritten) ?? '';
        $info['arabicSpoken']              = $this->cleanString($applicant->detail->arabicSpoken) ?? '';
        $info['arabicWritten']             = $this->cleanString($applicant->detail->arabicWritten) ?? '';
        $info['language3']                 = $this->cleanString($applicant->detail->language3) ?? '';
        $info['language3Spoken']           = $this->cleanString($applicant->detail->language3Spoken) ?? '';
        $info['language3Written']          = $this->cleanString($applicant->detail->language3Written) ?? '';
        $info['language4']                 = $this->cleanString($applicant->detail->language4) ?? '';
        $info['language4Spoken']           = $this->cleanString($applicant->detail->language4Spoken) ?? '';
        $info['language4Written']          = $this->cleanString($applicant->detail->language4Written) ?? '';
        $info['domicile']                  = $this->cleanString($applicant->detail->province->provName) ?? '';
        $info['district']                  = $this->cleanString($applicant->detail->district->distName) ?? '';
        $info['hobbie1']                   = $this->cleanString($applicant->detail->hobbie1) ?? '';
        $info['hobbie2']                   = $this->cleanString($applicant->detail->hobbie2) ?? '';
        $info['hobbie3']                   = $this->cleanString($applicant->detail->hobbie3) ?? '';
        $info['activity1']                 = $this->cleanString($applicant->detail->activity1) ?? '';
        $info['prize1']                    = $this->cleanString($applicant->detail->prize1) ?? '';
        $info['awardBy1']                  = $this->cleanString($applicant->detail->awardBy1) ?? '';
        $info['activity2']                 = $this->cleanString($applicant->detail->activity2) ?? '';
        $info['prize2']                    = $this->cleanString($applicant->detail->prize2) ?? '';
        $info['awardBy2']                  = $this->cleanString($applicant->detail->awardBy2) ?? '';
        $info['activity3']                 = $this->cleanString($applicant->detail->activity3) ?? '';
        $info['prize3']                    = $this->cleanString($applicant->detail->prize3) ?? '';
        $info['awardBy3']                  = $this->cleanString($applicant->detail->awardBy3) ?? '';
        $info['previousRegistrationNumber']= $this->cleanString($applicant->detail->xStudent) ?? '';
        $info['disability']                = $this->cleanString($applicant->detail->disable) ?? '';
        $info['bloodGroup']                = $this->cleanString($applicant->detail->blood) ?? '';
        $info['maritalStatus']             = $this->cleanString($applicant->detail->married) ?? '';
        $info['religion']                  = $this->cleanString($applicant->detail->religion) ?? '';
        $info['facebook']                  = $this->cleanString($applicant->detail->facebook_url) ?? '';
        $info['twitter']                   = $this->cleanString($applicant->detail->twitter_url) ?? '';

        $info['fatherName']      = $this->cleanString($applicant->detail->fatherName) ?? '';
        $info['fatherStatus']    = ($applicant->detail->fatherStatus == 1) ? 'Alive' : 'Deceased';
        $info['fatherDependants']= $this->cleanString($applicant->detail->dependants) ?? '';
        $info['fatherIncome']    = $this->cleanString($applicant->detail->monthlyIncome) ?? '';
        $info['fatherOccupation']= $this->cleanString($applicant->detail->fatherOccupation) ?? '';
        $info['fatherPhone']     = $this->cleanString($applicant->detail->fatherPhone) ?? '';
        $info['fatherMobile']    = $this->cleanString($applicant->detail->fatherMobile) ?? '';
        $info['fatherCnic']      = $this->cleanString($applicant->detail->father_cnic) ?? '';
        $info['guardianName']    = $this->cleanString($applicant->detail->Guradian_Spouse) ?? '';

        /** Bank Account Information */
        $info['bankAccountTitle']= $this->cleanString($applicant->detail->titleBankAccount) ?? '';
        $info['accountNo']       = $this->cleanString($applicant->detail->accountNo) ?? '';
        $info['bankName']        = $this->cleanString($applicant->detail->bankName) ?? '';
        $info['bankBranch']      = $this->cleanString($applicant->detail->bankBranch) ?? '';

        /** Address */
        $info['addressPresent']  = $this->cleanString($applicant->address->addressPo) ?? '';
        $info['cityPresent']     = $this->cleanString($applicant->address->cityPo) ?? '';
        $info['phonePresent']    = $this->cleanString($applicant->address->phonePo) ?? '';
        $info['areaPresent']     = $this->cleanString($applicant->address->areaPo) ?? '';
        $info['addressPermanent']= $this->cleanString($applicant->address->addressPmt) ?? '';
        $info['cityPermanent']   = $this->cleanString($applicant->address->cityPmt) ?? '';
        $info['phonePermanent']  = $this->cleanString($applicant->address->phonePmt) ?? '';
        $info['areaPermanent']   = $this->cleanString($applicant->address->areaPmt) ?? '';
        $info['fatherAddress']   = $this->cleanString($applicant->address->addressFather) ?? '';
        $info['cityFather']      = $this->cleanString($applicant->address->cityFather) ?? '';
        $info['phoneFather']     = $this->cleanString($applicant->address->phoneFather) ?? '';
        $info['areaFather']      = $this->cleanString($applicant->address->areaFather) ?? '';

        /** Previous Qualification */
        $info['sscTitle']        = $this->cleanString($applicant->previousQualification->SSC_Title) ?? '';
        $info['sscFrom']         = $this->cleanString($applicant->previousQualification->SSC_From) ?? '';
        $info['sscSubjects']     = $this->cleanString($applicant->previousQualification->SSC_Subject) ?? '';
        $info['sscObtainedMarks']= $this->cleanString($applicant->previousQualification->SSC_Composite) ?? '';
        $info['sscTotalMarks']   = $this->cleanString($applicant->previousQualification->SSC_Total) ?? '';
        $info['sscDegreeType']   = $this->cleanString($applicant->previousQualification->SSC_degree_type) ?? '';

        $info['hsscTitle']     = $this->cleanString($applicant->previousQualification->HSC_Title) ?? '';
        $info['hsscFrom']      = $this->cleanString($applicant->previousQualification->HSC_From) ?? '';
        $info['hsscSubjects']  = $this->cleanString($applicant->previousQualification->HSC_Subject) ?? '';
        $info['hssc1']         = $this->cleanString($applicant->previousQualification->HSSC1) ?? '';
        $info['hssc1Total']    = $this->cleanString($applicant->previousQualification->HSSC_Total1) ?? '';
        $info['hssc2']         = $this->cleanString($applicant->previousQualification->HSSC2) ?? '';
        $info['hssc2Total']    = $this->cleanString($applicant->previousQualification->HSSC_Total2) ?? '';
        $info['hsscComposite'] = $this->cleanString($applicant->previousQualification->HSSC_Composite) ?? '';
        $info['hsscTotal']     = $this->cleanString($applicant->previousQualification->HSSC_Total) ?? '';
        $info['hsscDegreeType']= $this->cleanString($applicant->previousQualification->HSC_degree_type) ?? '';
        
        $info['baBscTitle']               = $this->cleanString($applicant->previousQualification->BA_Bsc_Title) ?? '';
        $info['baBscFrom']                = $this->cleanString($applicant->previousQualification->BA_Bsc_From) ?? '';
        $info['baBscSubjects']            = $this->cleanString($applicant->previousQualification->BA_Bsc_Subject) ?? '';
        $info['baBscSpecialization']      = $this->cleanString($applicant->previousQualification->BA_Bsc_Specialization) ?? '';
        $info['baBscMarksObtained']       = $this->cleanString($applicant->previousQualification->BA_Bsc) ?? '';
        $info['baBscTotalMarks']          = $this->cleanString($applicant->previousQualification->BA_Bsc_Total) ?? '';
        $info['baBscDegreeCompletionYear']= $this->cleanString($applicant->previousQualification->BA_Bsc_Year) ?? '';
        $info['baBscDegreeType']          = $this->cleanString($applicant->previousQualification->BA_Bsc_degree_type) ?? '';

        $info['mscTitle']               = $this->cleanString($applicant->previousQualification->MSc_Title) ?? '';
        $info['mscFrom']                = $this->cleanString($applicant->previousQualification->MSc_From) ?? '';
        $info['mscSubjects']            = $this->cleanString($applicant->previousQualification->MSc_Subject) ?? '';
        $info['mscSpecialization']      = $this->cleanString($applicant->previousQualification->MSc_Specialization) ?? '';
        $info['mscMarksObtained']       = $this->cleanString($applicant->previousQualification->MSc) ?? '';
        $info['mscTotalMarks']          = $this->cleanString($applicant->previousQualification->MSc_Total) ?? '';
        $info['mscDegreeCompletionYear']= $this->cleanString($applicant->previousQualification->MSc_Year) ?? '';
        $info['mscDegreeType']          = $this->cleanString($applicant->previousQualification->MSc_degree_type) ?? '';

        $info['bsTitle']               = $this->cleanString($applicant->previousQualification->BS_Title) ?? '';
        $info['bsFrom']                = $this->cleanString($applicant->previousQualification->BS_From) ?? '';
        $info['bsSubjects']            = $this->cleanString($applicant->previousQualification->BS_Subject) ?? '';
        $info['bsSpecialization']      = $this->cleanString($applicant->previousQualification->BS_Specialization) ?? '';
        $info['bsExamSystem']          = $this->cleanString($applicant->previousQualification->BS_Exam_System) ?? '';
        $info['bsMarksObtained']       = $this->cleanString($applicant->previousQualification->BS) ?? '';
        $info['bsTotalMarks']          = $this->cleanString($applicant->previousQualification->BS_Total) ?? '';
        $info['bsDegreeCompletionYear']= $this->cleanString($applicant->previousQualification->BS_Year) ?? '';
        $info['bsDegreeType']          = $this->cleanString($applicant->previousQualification->BS_degree_type) ?? '';

        $info['msTitle']               = $this->cleanString($applicant->previousQualification->MS_Title) ?? '';
        $info['msFrom']                = $this->cleanString($applicant->previousQualification->MS_From) ?? '';
        $info['msSubjects']            = $this->cleanString($applicant->previousQualification->MS_Subject) ?? '';
        $info['msSpecialization']      = $this->cleanString($applicant->previousQualification->MS_Specialization) ?? '';
        $info['msExamSystem']          = $this->cleanString($applicant->previousQualification->MS_Exam_System) ?? '';
        $info['msMarksObtained']       = $this->cleanString($applicant->previousQualification->MS) ?? '';
        $info['msTotalMarks']          = $this->cleanString($applicant->previousQualification->MS_Total) ?? '';
        $info['msDegreeCompletionYear']= $this->cleanString($applicant->previousQualification->MS_Year) ?? '';
        $info['msDegreeType']          = $this->cleanString($applicant->previousQualification->MS_degree_type) ?? '';

        $info['resultAwaitingHSSC'] = $this->cleanString($applicant->previousQualification->resultAwaiting) ?? '';
        $info['resultAwaitingbaBsc']= $this->cleanString($applicant->previousQualification->resultwaitbsc) ?? '';
        $info['pqm']                = $this->cleanString($applicant->previousQualification->pqm) ?? '';

        $info['lawTestObtainedMarks'] = $this->cleanString($applicant->previousQualification->llb_obtained_marks) ?? '';
        $info['lawTestTotalMarks']    = $this->cleanString($applicant->previousQualification->llb_total_marks) ?? '';
        $info['lawTestCompletionDate']= $this->cleanString($applicant->previousQualification->llb_completion_date) ?? '';

        $info['mbaExecutiveExperience']= $this->cleanString($applicant->previousQualification->mba_experience) ?? '';
        return $info;
    }

    /**
     * Remove ",", "'" and "&" symbol from string
     * Oracle wants this to be cleaned
     */
    public function cleanString($string)
    {
        $removeCommaFromString           =  str_replace(",", "", $string);
        $removeApostropheFromString      = str_replace("'", "", $removeCommaFromString);
        return $removeAmpersandFromString= str_replace("&", "and", $removeApostropheFromString);
    }
}
