<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

/**
 * Class : Application (AppController)
 * Application Class to control all user related operations.
 * @author : M.Talha khan
 * @version : 1.1
 * @since : 12 April 2018
 */
class Application extends BaseController
{
    /**
     * This is default constructor of the class
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('user_model');
		$this->load->model('application_model');
        $this->isLoggedIn();   
    }
    
    /**
     * This function used to load the first screen of the user
     */
    public function index()
    {
        $this->global['pageTitle'] = 'IIUI Admission : Dashboard';
		
        $this->loadViews("personalDetail", $this->global, NULL , NULL);
    }
    
    /**
     * This function is used to load the user list
     */
    function userListing()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {        
            $searchText = $this->security->xss_clean($this->input->post('searchText'));
            $data['searchText'] = $searchText;
            
            $this->load->library('pagination');
            
            $count = $this->user_model->userListingCount($searchText);

			$returns = $this->paginationCompress ( "userListing/", $count, 10 );
            
            $data['userRecords'] = $this->user_model->userListing($searchText, $returns["page"], $returns["segment"]);
            
            $this->global['pageTitle'] = 'Admission IIUI : User Listing';
            
            $this->loadViews("users", $this->global, $data, NULL);
        }
    }

    /**
     * This function is used to load the Personal Details form
     */
    function personalDetail()
    {
        $adminId = $this->session->userdata("AdminUserId");
        $data['adminId'] = $adminId;
		$userId = $this->session->userdata("userId");
		
		$this->load->model('application_model');
		$data['userInfo'] = $this->application_model->getUserInfo($userId);
		$data['picInfo'] = $this->application_model->getPicInfo($userId);
		$data['applyingfor'] = $this->application_model->getapplyingfor();
		$data['gender'] = $this->application_model->getgenderInfo();
		$locId = $this->application_model->getDistricDomicById($userId);
		if(!empty($locId[0]->fkDistrict))
		{
			$districtId = $locId[0]->fkDistrict; $domcileId = $locId[0]->fkDomicile;
			$data['provinceId'] = $this->application_model->getdomicileById($domcileId);
			$data['districtId'] = $this->application_model->getdistrictById($districtId);
		}
		$data['province'] = $this->application_model->getprovinceInfo();
		$countryid = $this->application_model->getCountryInfo($userId);
		$countryeId = $countryid[0]->fkCountryIdPmt;
		$data['countryId'] = $this->application_model->getcountryById($countryeId);
		$data['countries'] = $this->application_model->getAllcountryName();
		$data['nationality'] = $this->application_model->getnationalityInfo();
		$data['appstatus'] = $this->application_model->CurrentStatus($userId);
		
		
		$this->global['pageTitle'] = 'Admission IIUI : Personal Detail';

		$this->loadViews("personaldetail", $this->global, $data, NULL);
        
    }

	/**
     * This function is used to load the Address Details form
     */
    function addressDetail()
    {
		$adminId = $this->session->userdata("AdminUserId");
        $data['adminId'] = $adminId;
        $userId = $this->session->userdata("userId");
		
		$this->load->model('application_model');
		$data['userInfo'] = $this->application_model->getAddressUserInfo($userId);
		$data['district'] = $this->application_model->getAllDistrict();
		$data['locId'] = $this->application_model->getAllAddressById($userId);
		$data['appstatus'] = $this->application_model->CurrentStatus($userId);
		
		$this->global['pageTitle'] = 'Admission IIUI : Address Detail';

		$this->loadViews("addressdetail", $this->global, $data, NULL);
        
    }
	
	/**
     * This function is used to load the Educational Details form
     */
    function educationalDetail()
    {
		
        $adminId = $this->session->userdata("AdminUserId");
        $data['adminId'] = $adminId;
		$userId = $this->session->userdata("userId");
		
		$this->load->model('application_model');

		$data['levelInfo'] = $this->application_model->getlevelgenderId($userId);
		$data['userInfo'] = $this->application_model->geEducationtUserInfo($userId);
	
		$data['ssc'] = $this->application_model->getsscInfo();
		$data['sscboard'] = $this->application_model->getsscBoardInfo();
		$data['sscgroup'] = $this->application_model->getsscgroupInfo();
		$data['hssc'] = $this->application_model->gethsscInfo();
		$data['hsscboard'] = $this->application_model->gethsscBoardInfo();
		$data['hsscgroup'] = $this->application_model->gethsscgroupInfo();
		$data['bsc'] = $this->application_model->getbscInfo();
		$data['bscboard'] = $this->application_model->getbscBoardInfo();
		$data['bscgroup'] = $this->application_model->getbscgroupInfo();
		$data['msc'] = $this->application_model->getmscInfo();
		$data['mscboard'] = $this->application_model->getmscBoardInfo();
		$data['mscgroup'] = $this->application_model->getmscgroupInfo();
		$data['bs'] = $this->application_model->getbsInfo();
		$data['bsboard'] = $this->application_model->getbsBoardInfo();
		$data['bsgroup'] = $this->application_model->getbsgroupInfo();
		$data['ms'] = $this->application_model->getmsInfo();
		$data['msboard'] = $this->application_model->getmsBoardInfo();
		$data['msgroup'] = $this->application_model->getmsgroupInfo();
		$data['countries'] = $this->application_model->getAllcountryName();
		$data['nationality'] = $this->application_model->getnationalityInfo();
		$data['appstatus'] = $this->application_model->CurrentStatus($userId); 
		
		$this->global['pageTitle'] = 'Admission IIUI : Qualification Detail';

		$this->loadViews("educationaldetail", $this->global, $data, NULL);
        
    }
	
	function programechoice()
    {
		
        $userId = $this->session->userdata("userId");
		
		$data['adminId'] = $this->session->userdata("AdminUserId");
		
		$this->load->model('application_model');
		
		$userInfo = $this->application_model->getUserInfoById($userId);
		
		$Ulevelid = $userInfo[0]->fkLevelId; $Ugenderid = $userInfo[0]->fkGenderId; $todate = date('Y-m-d');
		
		$activesemester = $this->application_model->getactiveSemId();
		
		$semesterId = $activesemester[0]->pkSemesterId;
		
		$data['prequalification'] = $this->application_model->getallpreviousQualification($userId, $semesterId); 
		
		$hscsubject = $this->application_model->gethscsubject($userId, $semesterId);
		
		$subject = $hscsubject[0]->HSC_Subject;
		
		if($Ugenderid == 2)
		$genderid = 3;
		elseif($Ugenderid == 3)
		$genderid = 2;
		
		$data['facInfo'] = $this->application_model->getallfaculty($Ulevelid, $genderid, $todate, $userId);
		
		$data['departmentInfo'] = $this->application_model->getalldepartment();
		
		$data['progInfo'] = $this->application_model->getallprog($Ulevelid, $genderid, $todate, $userId, $subject);
		
		$data['selectprog'] = $this->application_model->getallselectprog($userId);
		
		$data['appstatus'] = $this->application_model->CurrentStatus($userId);
		 
		$data['userInfo'] = $this->application_model->getProgrameUserInfo($userId);
		
		$data['applyingfor'] = $this->application_model->getapplyingfor();
		
		$data['preqsite'] = $this->application_model->preqsite();
  			 
		$this->global['pageTitle'] = 'Admission IIUI : Programe Choice';

		$this->loadViews("programechoice", $this->global, $data, NULL);
	}
	
	 
	
	/**
     * This function is used to load the Language Proficency form
     */
    function languageproficency()
    {
        $adminId = $this->session->userdata("AdminUserId");
        $data['adminId'] = $adminId;
		$userId = $this->session->userdata("userId");
		
		$this->load->model('application_model');
		$data['language'] = $this->application_model->getAllLanguage($userId);
		$data['appstatus'] = $this->application_model->CurrentStatus($userId);
		
		$this->global['pageTitle'] = 'Admission IIUI : Language Detail';

		$this->loadViews("languageproficency", $this->global, $data, NULL);
        
    }
	
	/**
     * This function is used to load the Language Proficency form
     */
    function aptitudetest()
    {
        $adminId = $this->session->userdata("AdminUserId");
        $data['adminId'] = $adminId;
		
		$userId = $this->session->userdata("userId");
		
		$this->load->model('application_model');
		$data['apttest'] = $this->application_model->getAllAptitudeTest($userId);
		$data['appstatus'] = $this->application_model->CurrentStatus($userId);
		
		$this->global['pageTitle'] = 'Admission IIUI : Aptitude Test Detail';

		$this->loadViews("aptitudetest", $this->global, $data, NULL);
        
    }
	
	/**
     * This function is used to load the Personal Details form
     */
    function familydetails()
    {
        $adminId = $this->session->userdata("AdminUserId");
        $data['adminId'] = $adminId;
		$userId = $this->session->userdata("userId");
		
		$this->load->model('application_model');
		$data['userInfo'] = $this->application_model->getFamilyInfoById($userId);
		$data['appstatus'] = $this->application_model->CurrentStatus($userId);
				
		$this->global['pageTitle'] = 'Admission IIUI : Family Details';

		$this->loadViews("familydetail", $this->global, $data, NULL);
        
    }
	
	/**
     * This function is used to load the Personal Details form
     */
    function otherdetails()
    {
        $adminId = $this->session->userdata("AdminUserId");
        $data['adminId'] = $adminId;
		$userId = $this->session->userdata("userId");
		
		$this->load->model('application_model');
		$data['userInfo'] = $this->application_model->getFamilyInfoById($userId);
		$data['appstatus'] = $this->application_model->CurrentStatus($userId);
		
				
		$this->global['pageTitle'] = 'Admission IIUI : Other Details';

		$this->loadViews("otherdetail", $this->global, $data, NULL);
        
    }
	
	/**
     * This function is used to load the Personal Details form
     */
    function savedetails()
    {
        $userId = $this->session->userdata("userId");
		
		$this->load->model('application_model');
		$data['userInfo'] = $this->application_model->getFamilyInfoById($userId);
		$data['appstatus'] = $this->application_model->CurrentStatus($userId);
		$data['stepstatus'] = $this->application_model->stepstatus($userId);		
		$this->global['pageTitle'] = 'Admission IIUI : Save Details';

		$this->loadViews("savedetails", $this->global, $data, NULL);
        
    }

/**
     * This function is used to load the Personal Details form
*/
	
	function appsubmit()
    {
		
		$userId = $this->session->userdata("userId");
		
		$this->load->model('application_model');
		
		$data['formInfo'] = $this->application_model->getFormInfoById($userId);
		
		$data['rollInfo'] = $this->application_model->getrollnoall($userId);
		
		$data['allcities'] = $this->application_model->GetAllCities();
		
		$data['appstatus'] = $this->application_model->CurrentStatus($userId);
				
		$this->global['pageTitle'] = 'Admission IIUI : Save Details';

		$this->loadViews("appsubmit", $this->global, $data, NULL);
        
    }


    function printform($progid = NULL)
    {
		if($progid == null)
            {
                redirect('appsubmit');
            }		
		$userId = $this->session->userdata("userId");
		
		$this->load->model('application_model');
		$data['userInfo'] = $this->application_model->getUserPrintInfo($userId);
		$data['challanInfo'] = $this->application_model->getUserChallanInfo($userId,$progid);
		$data['facInfo'] = $this->application_model->getFacultyInfo($progid);
		$countryid = $this->application_model->getUserPrintInfo($userId);
		$country = $countryid[0]->fkCountryIdPmt;
		if(!empty($country))
		{
		   $data['country'] = $this->application_model->getUserCountryName($country);
		}
		$loc = $this->application_model->getDistricDomicById($userId);
		$districtId = $loc[0]->fkDistrict; $domcileId = $loc[0]->fkDomicile; 
		$data['district'] = $this->application_model->getdistrictById($districtId);
		$data['domicile'] = $this->application_model->getdomicileById($domcileId);
				
		//$this->global['pageTitle'] = 'Admission IIUI : Save Details';

		$this->load->view("printform", $data);
        
    }
	
	function removeprograme($progid = NULL)
    {
		if($progid == null)
            {
                redirect('programechoice');
            }		
		
        $userId = $this->session->userdata("userId");
		
		$this->load->model('application_model');
		
		$programe = $this->application_model->countproggrame($userId, $progid);
		
		$totalprog = $programe[0]->ProgramId;
		
		if($totalprog > 1)
		
		{
		
			$delete = $this->application_model->removeprograme($userId, $progid);
			 
			if($delete)
				{
					$this->session->set_flashdata('success', 'Programe Choice removed Successfully');
					redirect('programechoice');
				}
		}
		elseif($totalprog == 1)
		    {
				$programeId = $progid;
				
				$Cprog = $this->application_model->InsertProgrameChoiceExisted($userId, $programeId);
				
				$cpid = $Cprog[0]->fkProgramId;
				
				$programechoice = array('fkProgramId'=>NULL);
				
				$programme = $this->application_model->UpdateRemoveProg($userId, $cpid, $programechoice);
				
				if($programme)
				
				$this->session->set_flashdata('success', 'Programe Choice removed Successfully');
				
				redirect('programechoice');
			}
    }
	
	/**
     * This function is used to load the Personal Details form
     */
    function downloadform($progid = NULL)
    {
		if($progid == null)
            {
                redirect('appsubmit');
            }
		$this->load->library('pdf');
		  		
		$userId = $this->session->userdata("userId");
		
				
		$this->load->model('application_model');
		$data['userInfo'] = $this->application_model->getUserPrintInfo($userId);
		$data['challanInfo'] = $this->application_model->getUserChallanInfo($userId,$progid);
		$data['facInfo'] = $this->application_model->getFacultyInfo($progid);
		$countryid = $this->application_model->getUserPrintInfo($userId);
		$country = $countryid[0]->fkCountryIdPmt;
		if(!empty($country))
		{
		   $data['country'] = $this->application_model->getUserCountryName($country);
		}
		$loc = $this->application_model->getDistricDomicById($userId);
		$districtId = $loc[0]->fkDistrict; $domcileId = $loc[0]->fkDomicile; 
		$data['district'] = $this->application_model->getdistrictById($districtId);
		$data['domicile'] = $this->application_model->getdomicileById($domcileId);
				
		$this->pdf->load_view('printformpdf', $data);
		  
	    $this->pdf->render();
	    $data['Attachment'] = FALSE;
	    $this->pdf->stream("Printformpdf.pdf", $data);
	
	
    }
	
	function feechallan($progid = NULL)
    {
		 if($progid == null)
            {
                redirect('appsubmit');
            }		
		$userId = $this->session->userdata("userId");
		
		$this->load->model('application_model');
		$data['userInfo'] = $this->application_model->getUserPrintInfo($userId);
		$data['facInfo'] = $this->application_model->getFacultyInfo($progid);
		$data['challanInfo'] = $this->application_model->getUserChallanInfo($userId,$progid);
		$countryid = $this->application_model->getUserPrintInfo($userId);
		$info = $this->application_model->getUserPrintInfo($userId);
		$semid = $info[0]->fkSemesterId; $levelid = $info[0]->fkLevelId;
		$data['semInfo'] = $this->application_model->getSemInfo($semid);
		$data['levelInfo'] = $this->application_model->getLevInfo($levelid);
		$data['progdate'] = $this->application_model->getUserProgramelastdate($progid);
		$dept = $this->application_model->GetPInfo($progid);
		$gender = $countryid[0]->fkGenderId; $deptid = $dept[0]->fkDepId;
		$data['bankInfo'] = $this->application_model->getUserBankInfo($gender, $deptid);
        $country = $countryid[0]->fkCountryIdPmt;
		if(!empty($country))
		{
		   $data['country'] = $this->application_model->getUserCountryName($country);
		}
		$loc = $this->application_model->getDistricDomicById($userId);
		$districtId = $loc[0]->fkDistrict; $domcileId = $loc[0]->fkDomicile; 
		$data['district'] = $this->application_model->getdistrictById($districtId);
		$data['domicile'] = $this->application_model->getdomicileById($domcileId);
				
		//$this->global['pageTitle'] = 'Admission IIUI : Save Details';

		$this->load->view("feechallan", $data);
        
    }
	
	function downloadchallanform($progid = NULL)
    {
		 if($progid == null)
            {
                redirect('appsubmit');
            }	
			$this->load->library('pdf');	
		$userId = $this->session->userdata("userId");
		
		$this->load->model('application_model');
		$data['userInfo'] = $this->application_model->getUserPrintInfo($userId);
		$data['facInfo'] = $this->application_model->getFacultyInfo($progid);
		$data['challanInfo'] = $this->application_model->getUserChallanInfo($userId,$progid);
		$countryid = $this->application_model->getUserPrintInfo($userId);
		$info = $this->application_model->getUserPrintInfo($userId);
		$semid = $info[0]->fkSemesterId; $levelid = $info[0]->fkLevelId;
		$data['semInfo'] = $this->application_model->getSemInfo($semid);
		$data['levelInfo'] = $this->application_model->getLevInfo($levelid);
		$data['progdate'] = $this->application_model->getUserProgramelastdate($progid);
		$dept = $this->application_model->GetPInfo($progid);
		$gender = $countryid[0]->fkGenderId; $deptid = $dept[0]->fkDepId;
		$data['bankInfo'] = $this->application_model->getUserBankInfo($gender, $deptid);
        $country = $countryid[0]->fkCountryIdPmt;
		if(!empty($country))
		{
		   $data['country'] = $this->application_model->getUserCountryName($country);
		}
		$loc = $this->application_model->getDistricDomicById($userId);
		$districtId = $loc[0]->fkDistrict; $domcileId = $loc[0]->fkDomicile; 
		$data['district'] = $this->application_model->getdistrictById($districtId);
		$data['domicile'] = $this->application_model->getdomicileById($domcileId);
				
		$this->pdf->load_view('feechallanpdf', $data);  
	    $this->pdf->render();
	    $data['Attachment'] = FALSE;
	    $this->pdf->stream("feechallanpdf.pdf", $data);
        
    }
	
	function printslip($progid = NULL, $rollno = NULL)
    {
		if($progid == null)
            {
                redirect('appsubmit');
            }		
		$userId = $this->session->userdata("userId");
		
		$this->load->model('application_model');
		$data['userInfo'] = $this->application_model->getUserPrintInfo($userId);
		$data['challanInfo'] = $this->application_model->getUserChallanInfo($userId,$progid);
		$data['facInfo'] = $this->application_model->getFacultyInfo($progid);
		$countryid = $this->application_model->getUserPrintInfo($userId);
		$country = $countryid[0]->fkCountryIdPmt;
		if(!empty($country))
		{
		   $data['country'] = $this->application_model->getUserCountryName($country);
		}
		$loc = $this->application_model->getDistricDomicById($userId);
		$districtId = $loc[0]->fkDistrict; $domcileId = $loc[0]->fkDomicile; 
		$data['district'] = $this->application_model->getdistrictById($districtId);
		$data['domicile'] = $this->application_model->getdomicileById($domcileId);
		$data['testdate'] = $this->application_model->getSlipTestdate($userId,$progid);
		$data['rollno'] = $rollno;
		//$data['rollnoInfo'] = $this->application_model->getRollno($userId);		
		//$this->global['pageTitle'] = 'Admission IIUI : Save Details';

		$this->load->view("printslip", $data);
        
    }
	
	function downloadrslip($progid = NULL, $rollno)
    {
		if($progid == null)
            {
                redirect('appsubmit');
            }
			$this->load->library('pdf');		
		$userId = $this->session->userdata("userId");
		
		$this->load->model('application_model');
		$data['userInfo'] = $this->application_model->getUserPrintInfo($userId);
		$data['challanInfo'] = $this->application_model->getUserChallanInfo($userId,$progid);
		$data['facInfo'] = $this->application_model->getFacultyInfo($progid);
		$countryid = $this->application_model->getUserPrintInfo($userId);
		$country = $countryid[0]->fkCountryIdPmt;
		if(!empty($country))
		{
		   $data['country'] = $this->application_model->getUserCountryName($country);
		}
		$loc = $this->application_model->getDistricDomicById($userId);
		$districtId = $loc[0]->fkDistrict; $domcileId = $loc[0]->fkDomicile; 
		$data['district'] = $this->application_model->getdistrictById($districtId);
		$data['domicile'] = $this->application_model->getdomicileById($domcileId);
		$data['testdate'] = $this->application_model->getSlipTestdate($userId,$progid);
		$data['rollno'] = $rollno;
				
		$this->pdf->load_view('downloadslippdf', $data);  
	    $this->pdf->render();
	    $data['Attachment'] = FALSE;
	    $this->pdf->stream("downloadslippdf.pdf", $data);
        
    }
	
	/**
     * This function is used to load the Personal Details form
     */
    function updateappsubmit()
    {
		$userId = $this->session->userdata("userId");				
		$feedeposit = $this->input->post('feedeposit');
		$bankdetails = $this->input->post('bankdetails');
		$branchcode = $this->input->post('branchcode');
		$progid = $this->input->post('progid');
		$totalprog = count($this->input->post('progid'));
		
		$i = 0; $feedate = array(); $bankinfo = array(); $bcode = array();
		 
		for($app=0; $app<=$totalprog; $app++)
		{
			if(!empty($feedeposit[$app]) && !empty($bankdetails[$app]) && !empty($branchcode[$app]))
			{
			  $feedate = $feedeposit[$app];
			  $bankinfo = $bankdetails[$app];
			  $bcode = $branchcode[$app];
			  $programeid = $progid[$app];
			  $feeInfo = array('bankDetails'=>$bankinfo, 'branchCode'=>$bcode, 'feeSubmitted'=>$feedate);
			  $this->load->model('application_model');
			  $result = $this->application_model->InsertFeeInfo($userId, $programeid , $feeInfo);
			  $applicationstatus = $this->application_model->GetApplicationStatus($userId, $programeid);
			  if($applicationstatus[0]->fkCurrentStatus <= 3)
			  {
			    $ApplicantInfo = array('fkCurrentStatus'=>3);
			    $result = $this->application_model->UpdateAppStatus($userId,  $programeid ,$ApplicantInfo);
			  }
			}		
		}
		
		if ($result == FALSE)
			{
				for($app=0; $app<=$totalprog; $app++)
					{
						if(empty($feedeposit[$app]) || empty($bankdetails[$app]) || empty($branchcode[$app]))
						{
							$this->session->set_flashdata('error', 'Enter Fee details Compeletely!');
							redirect('appsubmit');
						}
					
					}
			}
		
if ($result == TRUE)
	{
		
					/************ PHP MAIL FUNCTION Starts   ***********************/
					
				  require 'PHPMailer/src/Exception.php';
                  require 'PHPMailer/src/PHPMailer.php';
                  require 'PHPMailer/src/SMTP.php';
                  require "PHPMailer/src/OAuth.php";
                  require "PHPMailer/src/POP3.php";
                
				 $uId = $this->application_model->getUserInfoById($userId);
				  $email = $uId[0]->email;  $name = $uId[0]->name;
				  $feesubmit = $this->application_model->checkfeesubmit($userId);  
			      $progsId = $this->application_model->getallselectprog($userId);
			        $val = 0;
					//print_r($progid).'<br>';print_r($progid);exit();
					foreach($progsId as $pid)
					 
					//error_reporting(E_ALL);
                	  
			{  //echo $progid[$val];
			//echo $feesubmit[$val]->fkProgramId;exit();
			   if(!empty($feesubmit[$val]->feeSubmitted) && !empty($feesubmit[$val]->bankDetails) && !empty($feesubmit[$val]->branchCode) )
				{//echo $progid[$val];exit();
				  $pids = $feesubmit[$val]->fkProgramId; $ptitle = $pid->title;
				  $facId = $this->application_model->getfacInfo($pids);
				  $fromemail = $facId[0]->email;

                  //$mail = new PHPMailer;
                $mail = new PHPMailer\PHPMailer\PHPMailer();

				$mail->isSMTP();//isSMTP();                                    // Set mailer to use SMTP
				$mail->Host = 'smtp-relay.gmail.com';                    // Specify main and backup SMTP servers
				$mail->SMTPAuth = true;                            // Enable SMTP authentication
				$mail->Username = $fromemail;             // SMTP username
				$mail->Password = 'jdBTw$v@sT1^'; // SMTP password
				$mail->SMTPSecure = 'tls';                         // Enable TLS encryption, `ssl` also accepted
				$mail->Port = 587;                                 // TCP port to connect to
			      
				
			  $bodyContent = '<h3>Dear '.$name.'</h3><br>';
			  $bodyContent .= '<p style="font-size:14px">Thank you for submitting the fee deposit details for programe <b>'.$feesubmit[$val]->title.'.</b> Your application is now in review process and you will soon receive Roll No Slip in your Online Admission Account. If somehow the roll no slip is not received online even after the fee is deposited, then you can still appear in the test/interview along with original copy of deposit receipt.</p>
				<br/>
				<h3>Best Regards</h3>
				<h4">Admission Office</h4>
				<p>International Islamic University, Islamabad (IIUI)</p>
				<p>http://admission.iiu.edu.pk/</p>
				<br/><br/>
				<div style="font-size:smaller;background-color: #3c763d;color: #fff;">This communication contains information that is for the exclusive use of the intended recipient(s). If you are not the intended recipient, disclosure, copying, distribution or other use of, or taking of any action in reliance upon, this communication or the information in it is prohibited and may be unlawful. If you have received this communication in error please notify the sender by return email, delete it from your system and destroy any copies.</div>
				';
		       
				$mail->setFrom($fromemail, 'IIU Adminssion Office');
				$mail->addReplyTo('admissions@iiu.edu.pk', 'IIU Adminssion Office');
				$mail->addAddress($email);   // Add a recipient
				$mail->addCC('admission.notifications@iiu.edu.pk');
				//$mail->addBCC('bcc@example.com');
				   $mail->isHTML(true);  // Set email format to HTML
						
						$mail->Subject = 'IIU Admission Office';
						$mail->Body    = $bodyContent;
				
						if(!$mail->send()) 
						{
							
							$msg = 'Mailer Error: ' . $mail->ErrorInfo;
							$msg = 'Message could not be sent.';
						}
						 else {
								 $msg = 'Message has been sent';
						      }	
			
			
							 
					/************ PHP MAIL FUNCTION ENDS   ***********************/
			  $progIds = $pids;
		  	  $feeemail = array('feeemail'=>1);
			  $result = $this->application_model->updatefeeEmail($userId,$progIds, $feeemail);
		  
		  }
		  /**    IF END AND ELSE STATRS   empty($feedate[$val]) &&   **/
		
		  //elseif(empty($feesubmit[$val]->bankDetails) && $feesubmit[$val]->branchCode = 0000-00-00)
		   
					 {/*
				  echo $pids = $pid->fkProgramId; $ptitle = $pid->title;
				  $facId = $this->application_model->getfacInfo($pids);
				  $fromemail = $facId[0]->email;

                  //$mail = new PHPMailer;
                $mail = new PHPMailer\PHPMailer\PHPMailer();

				$mail->isSMTP();//isSMTP();                                    // Set mailer to use SMTP
				$mail->Host = 'smtp-relay.gmail.com';                    // Specify main and backup SMTP servers
				$mail->SMTPAuth = true;                            // Enable SMTP authentication
				$mail->Username = $fromemail;             // SMTP username
				$mail->Password = 'jdBTw$v@sT1^'; // SMTP password
				$mail->SMTPSecure = 'tls';                         // Enable TLS encryption, `ssl` also accepted
				$mail->Port = 587;                                 // TCP port to connect to
			      
				
			  $bodyContent = '<h3>Dear '.$name.'</h3><br>';
			  $bodyContent .= '<p style="font-size:14px">Thank you for submitting the fee deposit details for programe <b>'.$ptitle.'.</b> Your application is now in review process and you will soon receive Roll No Slip in your Online Admission Account. If somehow the roll no slip is not received online even after the fee is deposited, then you can still appear in the test/interview along with original copy of deposit receipt.</p>
				<br/>
				<h3>Best Regards</h3>
				<h4">Admission Office</h4>
				<p>International Islamic University, Islamabad (IIUI)</p>
				<p>http://admission.iiu.edu.pk/</p>
				<br/><br/>
				<div style="font-size:smaller;background-color: #3c763d;color: #fff;">This communication contains information that is for the exclusive use of the intended recipient(s). If you are not the intended recipient, disclosure, copying, distribution or other use of, or taking of any action in reliance upon, this communication or the information in it is prohibited and may be unlawful. If you have received this communication in error please notify the sender by return email, delete it from your system and destroy any copies.</div>
				';
		       
				$mail->setFrom($fromemail, 'IIU Adminssion Office');
				$mail->addReplyTo('admissions@iiu.edu.pk', 'IIU Adminssion Office');
				$mail->addAddress($email);   // Add a recipient
				$mail->addCC('admission.notifications@iiu.edu.pk');
				//$mail->addBCC('bcc@example.com');
				   $mail->isHTML(true);  // Set email format to HTML
						
						$mail->Subject = 'IIU Admission Office';
						$mail->Body    = $bodyContent;
				
						if(!$mail->send()) 
						{
							
							$msg = 'Mailer Error: ' . $mail->ErrorInfo;
							$msg = 'Message could not be sent.';
						}
						 else {
								 $msg = 'Message has been sent';
						      }	
			
			
							 
					/************ PHP MAIL FUNCTION ENDS   ***********************/
			 // $progIds = $pids;
		  	 // $feeemail = array('feeemail'=>1);
			  //$result = $this->application_model->updatefeeEmail($userId,$progIds, $feeemail);
		  
		  
						 
					}
		  
		  /** ELSE END HERE **/
		        
		$val++; 
		  
	 }// echo $progid[$val];
		  
}
			 
		$this->session->set_flashdata('success', 'Application Fee details Submitted Successfully!');
		redirect('appsubmit');	 
        
    }

	
	/**
     * This function is used to check whether email already exist or not
     */
    function GetdistrictByDomicileId()
    {
        $domicileId = $this->input->post('domicileId');
			
		$result = $this->application_model->getdistrictInfo($domicileId);
		 
	    echo json_encode($result);  
    }

    /**
     * This function is used to check whether email already exist or not
     */
    function checkEmailExists()
    {
        $userId = $this->input->post("userId");
        $email = $this->input->post("email");

        if(empty($userId)){
            $result = $this->user_model->checkEmailExists($email);
        } else {
            $result = $this->user_model->checkEmailExists($email, $userId);
        }

        if(empty($result)){ echo("true"); }
        else { echo("false"); }
    }
    
    /**
     * This function is used to add/Edit Applicant to the system
     */
    function UpdatePersonalInfo()
    {
        /*if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else*/
        {
            $this->load->library('form_validation');
            
            $this->form_validation->set_rules('applyingfor','Applying for','trim|required|numeric');
            $this->form_validation->set_rules('name','Full Name','min_length[2]|trim|required|callback_alpha_dash_space');
			$this->form_validation->set_rules('fname','Father Name','min_length[2]|trim|required|callback_alpha_dash_space');
            $this->form_validation->set_rules('email','Email','trim|required|valid_email|max_length[128]');
			$this->form_validation->set_rules('dob','Date of Birth','trim|required');
			$this->form_validation->set_rules('gender','Gender','trim|required|numeric');
			$this->form_validation->set_rules('nationality','Nationality','trim|required|numeric');
			$this->form_validation->set_rules('cnic','CNIC/B-Form','required|max_length[13]|min_length[4]');
            $this->form_validation->set_rules('mobile','Mobile Number','required|max_length[14]|numeric');
            
            if($this->form_validation->run() == FALSE)
            {
                $this->personalDetail();
            }
            else
            {
				$this->load->library('upload');
                 $this->load->library('image_lib');   
				if (!empty($_FILES['pic1']['name']) && isset($_FILES['pic1']['name']))
				{
					$userId = $this->vendorId;
					$path = $_FILES['pic1']['name'];
					$ext = pathinfo($path, PATHINFO_EXTENSION);
				$newfile_name = preg_replace("/[^a-zA-Z0-9_-]/", "", $_FILES['pic1']['name']);
				//$newfile_name = $newfile_name;
				//$newfile_name = $userId.'.'.$ext;
				$newfile_name1 = substr($newfile_name, 0, -3);
				$newfile_name = $newfile_name1.$ext;
				// Specify configuration for File 1
					$config['upload_path'] = 'uploads/'.$userId;
					$config['allowed_types'] = 'gif|jpg|png|jpeg|tif|bmp';
					$config['is_image']      = 1;
					$config['max_size'] = '504800';
					$config['max_width']  = '20000';
					$config['max_height']  = '20000';
					$config['file_name'] = $newfile_name;
					$config['overwrite'] = TRUE;
					
					
					$this->load->library('image_lib', $config);
					
					$this->image_lib->resize();

					$userId = str_replace( ':', '', $userId);
if (!is_dir('uploads/'.$userId)) {
    mkdir('./uploads/' . $userId, 0777, TRUE);

}
					
					// Initialize config for File 1
					$this->upload->initialize($config);
					
					// Upload file 1
					if ($this->upload->do_upload('pic1'))
					{
						$data = $this->upload->overwrite = true;
						$data = $this->upload->data();
					}
					else
					{
						$error =  'Image file size must be less than 2MB';
						$this->upload->display_errors();  //and dimension less than 2000x2000;
						
						$this->session->set_flashdata('error', $error);
						
						redirect('personaldetail');
					}
					
					$picpath = $config['upload_path'].'/';  
					$picname = $newfile_name.'_thumb'; 
					$picture = $picpath.$newfile_name.'.'.$ext;
					$dbpicture = $picpath.$picname.'.'.$ext;
					// resize image code
					
					$config['image_library'] = 'gd2';
						$config['source_image'] = $picture;
						$config['create_thumb'] = TRUE;
						$config['maintain_ratio'] = TRUE;
						$config['width']         = 400;
						$config['height']       = 200;
						
						$this->load->library('image_lib', $config);
						
						$this->image_lib->initialize($config);
						if(!$this->image_lib->resize())
						 {
							$error =  $this->upload->display_errors();
							$this->session->set_flashdata('error', $error);
							redirect('personaldetail');
						 }
						
						unlink( FCPATH . $picture);
					
				}
				else 
					{
						$picture = $this->input->post('pics');
						$dbpicture = $this->input->post('pics');
					}
				
				
                $applyingfor = $this->input->post('applyingfor');
				$name = ucwords(strtolower($this->security->xss_clean($this->input->post('name'))));
				$fname = ucwords(strtolower($this->security->xss_clean($this->input->post('fname'))));
                $email = $this->security->xss_clean($this->input->post('email'));
                $dob = $this->input->post('dob');
                $gender = $this->input->post('gender');
				$nationality = $this->input->post('nationality');
				$cnic = $this->input->post('cnic');
				$domicile = $this->input->post('domicile');
				$district = $this->input->post('district');
				$country = $this->input->post('country');
                $mobile = $this->security->xss_clean($this->input->post('mobile'));
				$mobile = ltrim($mobile, 0);
                $userId = $this->vendorId;
				if($district == '')
				$district = NULL;
				
				if($domicile == '')
				$domicile = NULL;
				
				if($country == '')
				$country = NULL;
				
				 $Infoall = $this->application_model->getlevelgenderId($userId);
				   
				 $leveid = $Infoall[0]->fkLevelId;  $genid = $Infoall[0]->fkGenderId;
				
				//if($applyingfor != $leveid || $gender != $genid)
				if($gender != $genid)
	{		   
				   $resultwait = array('resultAwaiting'=>0);
				   $this->application_model->updateresultwait($userId, $resultwait);
				   
				   $programe = $this->application_model->countprogramme($userId);
				   
				   $totalprog = $programe[0]->ProgramId; $pid = $programe[0]->id;
		
		if($totalprog > 1)
		
		{
			   $delete = $this->application_model->removeprogrameByUserId($userId, $pid);
			   
			   $programechoice = array('fkProgramId'=>NULL);
				
			   $programme = $this->application_model->UpdateRemoveProgBId($userId, $pid, $programechoice);
			   
			   $stepprograme = array('step_programs'=>0);
				
			   $this->application_model->UpdateStepProg($userId, $stepprograme);
			
		}
		
		elseif($totalprog == 1)
		    {
				$stepprograme = array('step_programs'=>0);
				
			    $this->application_model->UpdateStepProg($userId, $stepprograme);
				
				$programechoice = array('fkProgramId'=>NULL);
				
				$programme = $this->application_model->UpdateRemoveProgBId($userId, $pid, $programechoice);
				   
			}
   }

				
                $userInfo = array('fkLevelId'=>$applyingfor, 'name'=>$name, 'fkGenderId'=>$gender, 'fkNationality'=>$nationality,
								  'updatedBy'=>$this->vendorId);
                
                $this->load->model('application_model');
                $updateUserInfo = $this->application_model->UpdateUserInfo($userId, $userInfo);
                
                if($updateUserInfo > 0)
                {
                   $ApplicantInfo = array('fatherName'=>$fname, 'DOB'=>$dob, 'fkDomicile'=>$domicile, 'fkDistrict'=>$district, 'pic'=>$dbpicture,
								  'mobile'=> $mobile, 'modifiedBy'=>$this->vendorId);
				   $updateUserInfo = $this->application_model->UpdateApplicantInfo($userId, $ApplicantInfo);
				   
				   $UpdateCountryInfo = array('fkCountryIdPmt'=>$country);
				   $updateUserInfo = $this->application_model->UpdateCountryInfo($userId, $UpdateCountryInfo);
				   
		 /** Code for adding picture extension Old method used *****/
				  
				  /*$userId = $this->vendorId;
				   $picupload = $this->application_model->checkpicUpload($userId);
				   $pic = $picupload[0]->pic;
				  
				   
				   if (strpos($pic, ".") !== true) 
				      {
						$lastchar = substr($pic, -4);
						if($lastchar == 'jpeg' || $lastchar == 'JPEG')
						{
							$updatepic = $pic.'.jpeg';
						}
						else
						{
							$updatepics = substr($pic, -3);
							$updatepic = $pic.'.'.$updatepics;
						}
						$UpdatePics = array('pic'=>$updatepic);
					   $updatePersInfo = $this->application_model->Updatepics($UpdatePics, $userId);
					  }*/
		 /** Code for adding picture extension Old method used *****/
		 
		 		   
					   $UpdatePerInfo = array('step_personal'=>1);
					   $updatePersInfo = $this->application_model->UpdateStepPersonalInfo($UpdatePerInfo, $userId);
					   
					   $this->session->set_flashdata('success', 'Personal Detail Updated successfully');
				   
                }
                else
                {
                    $this->session->set_flashdata('error', 'Application Info Updation failed');
                }
                
                redirect('addressDetail');
            }
        }
    }

/**
     * This function is used to add/Edit Applicant Address Info to the system
     */
    function UpdateAddressInfo()
    {
        /*if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else*/
        {
            $this->load->library('form_validation');
            
            $this->form_validation->set_rules('addresspmt','Permanent Address','trim|required|max_length[255]');
			$this->form_validation->set_rules('citypmt','Permanent City','trim|required');
            $this->form_validation->set_rules('phpmt','Permanent Phone','trim|required|numeric|max_length[14]');
			$this->form_validation->set_rules('maddress','Mailing Address','trim|required|max_length[255]');
			$this->form_validation->set_rules('mcity','Mailing City','trim|required');
            $this->form_validation->set_rules('mphone','Mailing Phone','trim|required|numeric|max_length[14]');
			$this->form_validation->set_rules('faddress','Father Address','trim|required|max_length[255]');
			$this->form_validation->set_rules('fcity','Father City','trim|required');
            $this->form_validation->set_rules('fphone','Father Phone','trim|required|numeric|max_length[14]');
            
            if($this->form_validation->run() == FALSE)
            {
                $this->addressDetail();
            }
            else
            {
				$addresspmt = ucwords(strtolower($this->security->xss_clean($this->input->post('addresspmt'))));
				$citypmt = $this->input->post('citypmt');
				$phpmt = $this->input->post('phpmt');
				$maddress = ucwords(strtolower($this->security->xss_clean($this->input->post('maddress'))));
                $mcity = $this->input->post('mcity');
                $mphone = $this->input->post('mphone');
				$faddress = ucwords(strtolower($this->security->xss_clean($this->input->post('faddress'))));
				$fcity = $this->input->post('fcity');
                $fphone = $this->security->xss_clean($this->input->post('fphone'));
                $userId = $this->vendorId;
				
                $addressInfo = array('addressPmt'=>$addresspmt, 'cityPmt'=>$citypmt, 'phonePmt'=>$phpmt, 'addressPo'=>$maddress, 'cityPo'=>$mcity,
								  'phonePo'=>$mphone, 'addressFather'=>$faddress, 'cityFather'=>$fcity, 'phoneFather'=>$fphone,'modifiedBy'=>$this->vendorId, 'updated_at'=>date('Y-m-d H:i:s'));
                
                $this->load->model('application_model');
                $updateAddressInfo = $this->application_model->UpdateAddressInfo($userId, $addressInfo);
				
				$UpdateaddressInfo = array('step_address'=>1);
				$updateAddressInfo = $this->application_model->UpdateStepAddressInfo($userId, $UpdateaddressInfo);
                
                if($updateAddressInfo > 0)
                {
       			    $this->session->set_flashdata('success', 'Applicant Address Detail Updated successfully');
                }
                else
                {
                    $this->session->set_flashdata('error', 'Applicant Address Info Updation failed');
                }
                
                redirect('educationalDetail');
            }
        }
    }

/**
     * This function is used to add/Edit Applicant Education Info to the system
     */
    function UpdateEducationInfo()
    {
        /*if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else*/
        {
            $this->load->library('form_validation');
            
            //$this->form_validation->set_rules('ssctitle','Select Certificate/Degree','trim|required');
			//$this->form_validation->set_rules('sscfrom','Select Board/University','trim|required');
            //$this->form_validation->set_rules('scsubject','Select Group','trim|required');
			$this->form_validation->set_rules('ssctotal','SSC Total Marks','trim|required');
			$this->form_validation->set_rules('sscobtainmark','SSC Obtained Marks','trim|required');
            //$this->form_validation->set_rules('hsctitle','Select Certificate/Degree','trim|required');
			//$this->form_validation->set_rules('hscfrom','Select Board/University','trim|required');
			//$this->form_validation->set_rules('hscsubject','Select Group','trim|required');
            
            if($this->form_validation->run() == FALSE)
            {
                $this->educationalDetail();
            }
            else
            {
				$ssctitle1 = $this->input->post('ssctitle');
				$otherssctitle = $this->input->post('otherssctitle');
				if($otherssctitle != '')
				{
				  $ssctitle = $otherssctitle;
				}
				else
				{
					$ssctitle = $ssctitle1;
				}
				$sscfrom1 = $this->input->post('sscfrom');
				$othersscfrom = $this->input->post('othersscfrom');
				if($othersscfrom != '')
				{
				  $sscfrom = $othersscfrom;
				}
				else
				{
					$sscfrom = $sscfrom1;
				}
				$scsubject1 = $this->input->post('scsubject');
				$othersscsubject = $this->input->post('otherscsubject');
				if($othersscsubject != '')
				{
				  $scsubject = $othersscsubject;
				}
				else
				{
					$scsubject = $scsubject1;
				}
				$ssctotal = $this->security->xss_clean($this->input->post('ssctotal'));
				$sscobtainmark = $this->security->xss_clean($this->input->post('sscobtainmark'));
                
				if($ssctotal > $sscobtainmark && $sscobtainmark == '')
				{
					$this->session->set_flashdata('error', 'SSC total Marks must be greater than Obtain Marks');
					redirect('educationalDetail');
				}
				
			   
                $hsctitle1 = $this->input->post('hsctitle');
				$otherhssctitle = $this->input->post('otherhssctitle');
				if($otherhssctitle != '')
				{
				  $hsctitle = $otherhssctitle;
				}
				else
				{
					$hsctitle = $hsctitle1;
				}
				$hscfrom1 = $this->input->post('hscfrom');
				$otherhsscfrom = $this->input->post('otherhsscfrom');
				if($otherhsscfrom != '')
				{
				  $hscfrom = $otherhsscfrom;
				}
				else
				{
					$hscfrom = $hscfrom1;
				}
				$hscsubject1 = $this->input->post('hscsubject');
				$otherhsscgroup = $this->input->post('otherhsscgroup');
				if($otherhsscgroup != '')
				{
				  $hscsubject = $otherhsscgroup;
				}
				else
				{
					$hscsubject = $hscsubject1;
				}
				$HSSCtot1 = $this->security->xss_clean($this->input->post('HSSCtot1'));
				$hsscobtain1 = $this->security->xss_clean($this->input->post('hsscobtain1'));
				$hssctot2 = $this->security->xss_clean($this->input->post('hssctot2'));
				$hsscobtain2 = $this->security->xss_clean($this->input->post('hsscobtain2'));
				$hssccombinetotal = $this->security->xss_clean($this->input->post('hssccombinetotal'));
				$hssccombineobtain = $this->security->xss_clean($this->input->post('hssccombineobtain'));
				
				if($HSSCtot1 > $hsscobtain1 && $hsscobtain1 == '')
				{
					$this->session->set_flashdata('error', 'HSSC1 total Marks must be greater than Obtain Marks');
					redirect('educationalDetail');
				}
				if($hssctot2 > $hsscobtain2 && $hsscobtain2 == '')
				{
					$this->session->set_flashdata('error', 'HSSC2 total Marks must be greater than Obtain Marks');
					redirect('educationalDetail');
				}
				
				if($hssccombinetotal > $hssccombineobtain && $hssccombineobtain == '')
				{
					$this->session->set_flashdata('error', 'HSSC Combine total Marks must be greater than Obtain Marks');
					redirect('educationalDetail');
				}
				
				$bsctitle1 = $this->input->post('bsctitle');
				$othertitlebsc = $this->input->post('othertitlebsc');
				if($othertitlebsc != '')
				{
				  $bsctitle = $othertitlebsc;
				}
				else
				{
					$bsctitle = $bsctitle1;
				}
				$bscfrom1 = $this->input->post('bscfrom');
				$otherfrombsc = $this->input->post('otherfrombsc');
				if($otherfrombsc != '')
				{
				  $bscfrom = $otherfrombsc;
				}
				else
				{
					$bscfrom = $bscfrom1;
				}
				$bscsubject1 = $this->input->post('bscsubject');
				$othergroupbsc = $this->input->post('othergroupbsc');
				if($othergroupbsc != '')
				{
				  $bscsubject = $othergroupbsc;
				}
				$BA_Bsc_Specialization = $this->input->post('BA_Bsc_Specialization');
				if($BA_Bsc_Specialization != '')
				{
				  $BA_Bsc_Specialization = $BA_Bsc_Specialization;
				}
				else
				{
					$bscsubject = $bscsubject1;
				}
				$bsctotalmark = $this->security->xss_clean($this->input->post('bsctotalmark'));
				$bscobtainmark = $this->security->xss_clean($this->input->post('bscobtainmark'));
				
				if($bsctotalmark > $bscobtainmark && $bscobtainmark == '')
				{
					$this->session->set_flashdata('error', 'BA/BSC total Marks must be greater than Obtain Marks');
					redirect('educationalDetail');
				}
				
				$msctitle1 = $this->input->post('msctitle');
				$othertitlemsc = $this->input->post('othertitlemsc');
				
				if($othertitlemsc != '')
				{
				  $msctitle = $othertitlemsc;
				}
				else
				{
					$msctitle = $msctitle1;
				}
				$mscfrom1 = $this->input->post('mscfrom');
				$otherfrommsc = $this->input->post('otherfrommsc');
				if($otherfrommsc != '')
				{
				  $mscfrom = $otherfrommsc;
				}
				else
				{
					$mscfrom = $mscfrom1;
				}
				$mscsubject1 = $this->input->post('mscsubject');
				$othersubjectmsc = $this->input->post('othersubjectmsc');
				if($othersubjectmsc != '')
				{
				  $mscsubject = $othersubjectmsc;
				}
				else
				{
					$mscsubject = $mscsubject1;
				}
				$MSc_Specialization = $this->input->post('MSc_Specialization');
				if($MSc_Specialization != '')
				{
				  $MSc_Specialization = $MSc_Specialization;
				}
				$msctotalmark = $this->security->xss_clean($this->input->post('msctotalmark'));
				$mscobtainmark = $this->security->xss_clean($this->input->post('mscobtainmark'));
				
				 if($msctotalmark > $mscobtainmark && $mscobtainmark == '')
				{
					$this->session->set_flashdata('error', 'MA/MSC total Marks must be greater than Obtain Marks');
					redirect('educationalDetail');
				}
				
				$bstitle1 = $this->input->post('bstitle');
				$othertitletbs = $this->input->post('othertitletbs');
				if($othertitletbs != '')
				{
				  $bstitle = $othertitletbs;
				  $bsradio = $this->input->post('bsradio');
				}
				else
				{
					$bstitle = $bstitle1;
					$bsradio = 'NULL';
				}
				$bsfrom1 = $this->input->post('bsfrom');
				$otherfrombs = $this->input->post('otherfrombs');
				if($otherfrombs != '')
				{
				  $bsfrom = $otherfrombs;
				}
				else
				{
					$bsfrom = $bsfrom1;
				}
				$bssubject1 = $this->input->post('bssubject');
				$othersubjectbs = $this->input->post('othersubjectbs');
				if($othersubjectbs != '')
				{
				  $bssubject = $othersubjectbs;
				}
				else
				{
					$bssubject = $bssubject1;
				}
				$BS_Specialization = $this->input->post('BS_Specialization');
				if($BS_Specialization != '')
				{
				  $BS_Specialization = $BS_Specialization;
				}
				$bstotalmark = $this->security->xss_clean($this->input->post('bstotalmark'));
				$bsobtainmark = $this->security->xss_clean($this->input->post('bsobtainmark'));
				
				if($bstotalmark > $bsobtainmark && $bsobtainmark == '')
				{
					$this->session->set_flashdata('error', 'BS total Marks must be greater than Obtain Marks');
					redirect('educationalDetail');
				}
				
				$mstitle1 = $this->input->post('mstitle');
				$othertitlems = $this->input->post('othertitlems');
				if($othertitlems != '')
				{
				  $mstitle = $othertitlems;
				   $msradio = $this->input->post('msradio');
				}
				else
				{
					$mstitle = $mstitle1;
					 $msradio = 'NULL';
				}
				
				$msfrom1 = $this->input->post('msfrom');
				$otherfromms = $this->input->post('otherfromms');
				if($otherfromms != '')
				{
				  $msfrom = $otherfromms;
				}
				else
				{
					$msfrom = $msfrom1;
				}
				$mssubject1 = $this->input->post('mssubject');
				$othersubjectms = $this->input->post('othersubjectms');
				if($othersubjectms != '')
				{
				  $mssubject = $othersubjectms;
				}
				else
				{
					$mssubject = $mssubject1;
				}
				$MS_Specialization = $this->input->post('MS_Specialization');
				if($MS_Specialization != '')
				{
				  $MS_Specialization = $MS_Specialization;
				}
				$mstotal = $this->security->xss_clean($this->input->post('mstotal'));
				$msobtain = $this->security->xss_clean($this->input->post('msobtain'));
				
				if($mstotal > $msobtain && $msobtain == '')
				{
					$this->session->set_flashdata('error', 'MS total Marks must be greater than Obtain Marks');
					redirect('educationalDetail');
				}
				
				
                $userId = $this->vendorId;
				
				$edumark = $this->application_model->GetEduMarks($userId);
				
				$markssctotal = $edumark[0]->SSC_Total; $marksscobtain = $edumark[0]->SSC_Composite; $markhssctotal = $edumark[0]->HSSC_Total; 
				$markhsscomp = $edumark[0]->HSSC_Composite; $markhssctotal1 = $edumark[0]->HSSC_Total1; 
				$markhsscobtain1 = $edumark[0]->HSSC1; $markhssctotal2 = $edumark[0]->HSSC_Total2; $markhsscobtain2 = $edumark[0]->HSSC2;
				$markbsctotal = $edumark[0]->BA_Bsc_Total; $markbscobtain = $edumark[0]->BA_Bsc; $markmsctotal = $edumark[0]->MSc_Total; 
				$markmscobtain = $edumark[0]->MSc; $markbstotal = $edumark[0]->BS_Total; $markbs = $edumark[0]->BS;
				$markmstotal = $edumark[0]->MS_Total; $markms = $edumark[0]->MS;
				
				if($markssctotal != $ssctotal || $marksscobtain != $sscobtainmark || $markhssctotal != $hssccombinetotal || $markhsscomp != $hssccombineobtain || $markhssctotal1 != $HSSCtot1 || $markhsscobtain1 != $hsscobtain1 || $markhssctotal2 != $hssctot2 || $markhsscobtain2 != $hsscobtain2 || $markbsctotal != $bsctotalmark || $markbscobtain != $bscobtainmark || $markmsctotal != $msctotalmark || $markmscobtain != $mscobtainmark || $markbstotal != $bstotalmark || $markbs != $bsobtainmark || $markmstotal != $mstotal || $markms != $msobtain)
				
				{
					
							   $resultwait = array('resultAwaiting'=>0);
							   $this->application_model->updateresultwait($userId, $resultwait);
							   $stepstatus = $this->application_model->stepstatus($userId);
							   $programe = $this->application_model->countprogramme($userId);
							   $apstatus = $stepstatus[0]->step_save;
							   $totalprog = $programe[0]->ProgramId; $pid = $programe[0]->id;
					
					if($totalprog > 1 && $apstatus == 0)
					
					{
						   $delete = $this->application_model->removeprogrameByUserId($userId, $pid);
						   
						   $programechoice = array('fkProgramId'=>NULL);
							
						   $programme = $this->application_model->UpdateRemoveProgBId($userId, $pid, $programechoice);
						   
						   $stepprograme = array('step_programs'=>0);
							
						   $this->application_model->UpdateStepProg($userId, $stepprograme);
						
					}
					
					elseif($totalprog == 1 && $apstatus == 0)
						{
							$stepprograme = array('step_programs'=>0);
							
							$this->application_model->UpdateStepProg($userId, $stepprograme);
							
							$programechoice = array('fkProgramId'=>NULL);
							
							$programme = $this->application_model->UpdateRemoveProgBId($userId, $pid, $programechoice);
							   
						}
					
					
				}
				$resultwait = $this->input->post('resultwait');
				if($resultwait == NULL)
				{
				 $resultwait = 0;
				}
				if(empty($hssctot2) && empty($hsscobtain2) && empty($hssccombinetotal) && empty($hssccombineobtain))
				{
				 $resultwait = 1;
				}
				if(!empty($bstotalmark) && !empty($bsobtainmark))
				{
				 $resultwait = 0;
				}
				if(!empty($mstotal) && !empty($msobtain))
				{
				 $resultwait = 0;
				}
                $educationInfo = array('SSC_Title'=>$ssctitle, 'SSC_From'=>$sscfrom, 'SSC_Subject'=>$scsubject, 'SSC_Total'=>$ssctotal, 
				                     'SSC_Composite'=>$sscobtainmark, 'HSC_Title'=>$hsctitle, 'HSC_From'=>$hscfrom, 'HSC_Subject'=>$hscsubject,
				                     'HSSC_Total1'=>$HSSCtot1, 'HSSC1'=>$hsscobtain1, 'HSSC_Total2'=>$hssctot2, 'HSSC2'=>$hsscobtain2, 
									 'HSSC_Total'=>$hssccombinetotal, 'HSSC_Composite'=>$hssccombineobtain, 'BA_Bsc_Title'=>$bsctitle, 
									 'BA_Bsc_From'=>$bscfrom, 'BA_Bsc_Subject'=>$bscsubject, 'BA_Bsc_Specialization'=>$BA_Bsc_Specialization,
									 'BA_Bsc_Total'=>$bsctotalmark, 'BA_Bsc'=>$bscobtainmark, 'MSc_Title'=>$msctitle, 'MSc_From'=>$mscfrom,
									 'MSc_Subject'=>$mscsubject,'MSc_Specialization'=>$MSc_Specialization,
									 'MSc_Total'=>$msctotalmark, 'MSc'=>$mscobtainmark, 'BS_Title'=>$bstitle, 'BS_Exam_System'=>$bsradio, 'BS_From'=>$bsfrom,
									 'BS_Subject'=>$bssubject, 'BS_Specialization'=>$BS_Specialization,'BS_Subject'=>$bssubject, 'BS_Total'=>$bstotalmark,
									 'BS'=>$bsobtainmark,'MS_Title'=>$mstitle,'MS_Exam_System'=>$msradio,'MS_From'=>$msfrom, 'MS_Subject'=>$mssubject,
									 'MS_Specialization'=>$MS_Specialization, 'MS_Total'=>$mstotal, 'MS'=>$msobtain,'modifiedBy'=>$userId,
									 'resultAwaiting'=>$resultwait, 'update_at'=>date('Y-m-d H:i:s'));
                
                $this->load->model('application_model');
                $updateEducationInfo = $this->application_model->UpdateEducationInfo($userId, $educationInfo);
				
				$UpdateEduInfo = array('step_education'=>1);
			    $updateEduInfo = $this->application_model->UpdateStepEduInfo($UpdateEduInfo, $userId);
                
                if($updateEducationInfo > 0)
                {
       			    $this->session->set_flashdata('success', 'Applicant Education Detail Updated successfully');
                }
                else
                {
                    $this->session->set_flashdata('error', 'Applicant Education Info Updation failed');
                }
                
                redirect('programechoice');
            }
        }
    }
	
	function Updateprogramelevel()
    {   
	   $userId = $this->vendorId;
	   $proglevel = $this->input->post('applyingfor');
	   $levelInfo = array('fkLevelId'=>$proglevel);
	   $updateplevelInfo = $this->application_model->UpdatePlevelInfo($levelInfo, $userId);
	   
		if($updateplevelInfo == TRUE)
                {
       			    $this->session->set_flashdata('success', 'Applicant programe Level Updated successfully. Enter your last degree marks in Educational Details');
                }
                else
                {
                    $this->session->set_flashdata('error', 'Applicant programe Level Updation failed');
                }
		
		redirect('educationalDetail');
	}
	
	/**
     * This function is used to add/Edit Applicant Address Info to the system
     */
    function UpdateprogramechoiceInfo()
    {
        /*if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else*/
        {
            /*$this->load->library('form_validation');
            
            $this->form_validation->set_rules('addresspmt','Permanent Address','trim|required|max_length[255]');
			$this->form_validation->set_rules('citypmt','Permanent City','trim|required');
            $this->form_validation->set_rules('phpmt','Permanent Phone','trim|required|numeric|max_length[14]');
			$this->form_validation->set_rules('maddress','Mailing Address','trim|required|max_length[255]');
			$this->form_validation->set_rules('mcity','Mailing City','trim|required');
            $this->form_validation->set_rules('mphone','Mailing Phone','trim|required|numeric|max_length[14]');
			$this->form_validation->set_rules('faddress','Father Address','trim|required|max_length[255]');
			$this->form_validation->set_rules('fcity','Father City','trim|required');
            $this->form_validation->set_rules('fphone','Father Phone','trim|required|numeric|max_length[14]');
            
            if($this->form_validation->run() == FALSE)
            {
                $this->familydetails();
            }
            else*/
            {
				
				$resultwait = $this->input->post('resultwait');
				
				$choice = $this->input->post('choice');
				
				if(!empty($choice) && $choice[0] != '' && $choice[1] != '' && $choice[2] != '')
				{
					if($choice[0] == $choice[1] || $choice[0] == $choice[2] || $choice[1] == $choice[2] || $choice[2] == $choice[1])
					{
						$this->session->set_flashdata('error', 'BS Engineering programe must contain unique prefrence');
					    redirect('programechoice');
					}
				     $choice1 = $choice[0];  $choice2 = $choice[1]; $choice3 = $choice[2]; 
				}
				else
				{
				   $choice1 = NULL;  $choice2 = NULL; $choice3 = NULL;	
				}
				 if(empty($resultwait))
				 {
					 $this->session->set_flashdata('error', 'Select atleast One Programe Choice');
					 redirect('programechoice');
				 }
				 
				 foreach ($resultwait as $key => $programeId)
				 {
					 //echo "Index {$key}'s programeId is {$programeId}.";
					 
					 $userId = $this->vendorId;
					 
					 $programeExist = $this->application_model->ProgrameExist($userId);
					 
					 if($programeExist[0]->fkProgramId == NULL)
					    {
							$programechoice = array('fkProgramId'=>$programeId, 'preferrence1'=>$choice1, 'preferrence2'=>$choice2, 'preferrence3'=>$choice3);
				   
				   			$result = $this->application_model->UpdateProgrameChoice($userId,$programechoice);
						}
					elseif($programeExist[0]->fkProgramId != NULL)
					{
						$existed = $this->application_model->InsertProgrameChoiceExisted($userId, $programeId);
						
						$existstauts = $this->application_model->ProgrameExistedStatus($userId);
						
						$appstatus = $existstauts[0]->fkCurrentStatus; $semId = $existstauts[0]->fkSemesterId; 
						
						 if($existed == NULL)
						 {
						
						$programechoice = array('fkApplicantId'=>$userId, 'fkProgramId'=>$programeId, 'fkSemesterId'=>$semId, 'fkCurrentStatus'=>2, 'preferrence1'=>$choice1, 'preferrence2'=>$choice2, 'preferrence3'=>$choice3);
			   
						$result = $this->application_model->InsertProgrameChoice($programechoice);
						 }
					}
					
					
				 }
				 
				 $Updatechoice = array('step_programs'=>1);
			     $updateChoiceInfo = $this->application_model->UpdateStepChoiceInfo($Updatechoice, $userId);
				 
				 
				 if($result == TRUE)
						{
							$this->session->set_flashdata('success', 'Programe Choice Updated successfully');
							
						}
						else
						{
							$this->session->set_flashdata('error', 'You had already Applied for this Programe');
						}
						if($appstatus == 2)
						{
							redirect('appsubmit');
						}
						else
						redirect('languageproficency');
	
            }
        }
    }
	
	/**
     * This function is used to add/Edit Applicant Address Info to the system
     */
    function updatefamilydetail()
    {
        /*if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else*/
        {
            /*$this->load->library('form_validation');
            
            $this->form_validation->set_rules('addresspmt','Permanent Address','trim|required|max_length[255]');
			$this->form_validation->set_rules('citypmt','Permanent City','trim|required');
            $this->form_validation->set_rules('phpmt','Permanent Phone','trim|required|numeric|max_length[14]');
			$this->form_validation->set_rules('maddress','Mailing Address','trim|required|max_length[255]');
			$this->form_validation->set_rules('mcity','Mailing City','trim|required');
            $this->form_validation->set_rules('mphone','Mailing Phone','trim|required|numeric|max_length[14]');
			$this->form_validation->set_rules('faddress','Father Address','trim|required|max_length[255]');
			$this->form_validation->set_rules('fcity','Father City','trim|required');
            $this->form_validation->set_rules('fphone','Father Phone','trim|required|numeric|max_length[14]');
            
            if($this->form_validation->run() == FALSE)
            {
                $this->familydetails();
            }
            else*/
            {
				$fstatus = $this->security->xss_clean($this->input->post('fstatus'));
				$dependants = $this->security->xss_clean($this->input->post('dependants'));
				$monthlyIncome = $this->security->xss_clean($this->input->post('monthlyIncome'));
				$Guradian_Spouse = $this->security->xss_clean($this->input->post('Guradian_Spouse'));
				$fatherOccupation = $this->security->xss_clean($this->input->post('fatherOccupation'));
				$fatherPhone = $this->security->xss_clean($this->input->post('fatherPhone'));
				$fatherMobile = $this->security->xss_clean($this->input->post('fatherMobile'));
				$titleBankAccount = $this->security->xss_clean($this->input->post('titleBankAccount'));
				$accountNo = $this->security->xss_clean($this->input->post('accountNo'));
				$bankname = $this->security->xss_clean($this->input->post('bankname'));
				$bankBranch = $this->security->xss_clean($this->input->post('bankBranch'));
				
                $userId = $this->vendorId;
				
                $familyInfo = array('fatherStatus'=>$fstatus, 'dependants'=>$dependants, 'monthlyIncome'=>$monthlyIncome, 'Guradian_Spouse'=>$Guradian_Spouse, 'fatherOccupation'=>$fatherOccupation,'fatherPhone'=>$fatherPhone, 'fatherMobile'=>$fatherMobile, 'titleBankAccount'=>$titleBankAccount, 'accountNo'=>$accountNo,'bankname'=>$bankname,'bankBranch'=>$bankBranch,'modifiedBy'=>$this->vendorId, 'updated_at'=>date('Y-m-d H:i:s'));
                
                $this->load->model('application_model');
                $updateAddressInfo = $this->application_model->UpdateFamilyInfo($userId, $familyInfo);
                
				$Updatefamily = array('step_family'=>1);
	    		$UpdatefamilyInfo = $this->application_model->UpdatefamilystepInfo($Updatefamily, $userId);
				
                if($updateAddressInfo > 0)
                {
       			    $this->session->set_flashdata('success', 'Applicant Family Detail Updated successfully');
                }
                else
                {
                    $this->session->set_flashdata('error', 'Applicant Family Info Updation failed');
                }
                
                redirect('otherdetails');
            }
        }
    }
	
	/**
     * This function is used to add/Edit Applicant Address Info to the system
     */
    function updateotherdetail()
    {
        /*if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else*/
        {
            /*$this->load->library('form_validation');
            
            $this->form_validation->set_rules('addresspmt','Permanent Address','trim|required|max_length[255]');
			$this->form_validation->set_rules('citypmt','Permanent City','trim|required');
            $this->form_validation->set_rules('phpmt','Permanent Phone','trim|required|numeric|max_length[14]');
			$this->form_validation->set_rules('maddress','Mailing Address','trim|required|max_length[255]');
			$this->form_validation->set_rules('mcity','Mailing City','trim|required');
            $this->form_validation->set_rules('mphone','Mailing Phone','trim|required|numeric|max_length[14]');
			$this->form_validation->set_rules('faddress','Father Address','trim|required|max_length[255]');
			$this->form_validation->set_rules('fcity','Father City','trim|required');
            $this->form_validation->set_rules('fphone','Father Phone','trim|required|numeric|max_length[14]');
            
            if($this->form_validation->run() == FALSE)
            {
                $this->familydetails();
            }
            else*/
            {
				$xStudent = $this->security->xss_clean($this->input->post('xStudent'));
				$hobbie1 = $this->security->xss_clean($this->input->post('hobbie1'));
				$hobbie2 = $this->security->xss_clean($this->input->post('hobbie2'));
				$hobbie3 = $this->security->xss_clean($this->input->post('hobbie3'));
				$activity1 = $this->security->xss_clean($this->input->post('activity1'));
				$prize1 = $this->security->xss_clean($this->input->post('prize1'));
				$awardBy1 = $this->security->xss_clean($this->input->post('awardBy1'));
				$activity2 = $this->security->xss_clean($this->input->post('activity2'));
				$prize2 = $this->security->xss_clean($this->input->post('prize2'));
				$awardBy2 = $this->security->xss_clean($this->input->post('awardBy2'));
				$activity3 = $this->security->xss_clean($this->input->post('activity3'));
				$prize3 = $this->security->xss_clean($this->input->post('prize3'));
				$awardBy3 = $this->security->xss_clean($this->input->post('awardBy3'));
				$blood = $this->security->xss_clean($this->input->post('blood'));
				$disables = $this->security->xss_clean($this->input->post('disables'));
				$married = $this->security->xss_clean($this->input->post('married'));
				if ($married == 'Select Status')
				{
					echo $married = NULL;
				}
				if ($blood == 'Select Blood')
				{
					echo $blood = NULL;
				}
                $userId = $this->vendorId;
				
                $otherInfo = array('xStudent'=>$xStudent, 'hobbie1'=>$hobbie1, 'hobbie2'=>$hobbie2, 'hobbie3'=>$hobbie3, 'activity1'=>$activity1,'prize1'=>$prize1, 'awardBy1'=>$awardBy1, 'activity2'=>$activity2, 'prize2'=>$prize2,'awardBy2'=>$awardBy2,'activity3'=>$activity3,'prize3'=>$prize3,'awardBy3'=>$awardBy3,'blood'=>$blood,'disable'=>$disables,'married'=>$married,'modifiedBy'=>$this->vendorId, 'updated_at'=>date('Y-m-d H:i:s'));
                $Otherinfos = array('step_other'=>1);
	   		    $UpdateOtherInfo = $this->application_model->UpdateOtherstepInfo($Otherinfos, $userId);
                $this->load->model('application_model');
                $updateOtherInfo = $this->application_model->UpdateOtherInfo($userId, $otherInfo);
                
                if($updateOtherInfo > 0)
                {
       			    $this->session->set_flashdata('success', 'Applicant Other Detail Updated successfully');
                }
                else
                {
                    $this->session->set_flashdata('error', 'Applicant Other Info Updation failed');
                }
                
                redirect('savedetails');
            }
        }
    }

/**
     * This function is used to add/Edit Applicant SaveDtail Info to the system
     */
    function updatsavedetail()
    {
        /*if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else*/
        {
            /*$this->load->library('form_validation');
            
            $this->form_validation->set_rules('addresspmt','Permanent Address','trim|required|max_length[255]');
			$this->form_validation->set_rules('citypmt','Permanent City','trim|required');
            $this->form_validation->set_rules('phpmt','Permanent Phone','trim|required|numeric|max_length[14]');
			$this->form_validation->set_rules('maddress','Mailing Address','trim|required|max_length[255]');
			$this->form_validation->set_rules('mcity','Mailing City','trim|required');
            $this->form_validation->set_rules('mphone','Mailing Phone','trim|required|numeric|max_length[14]');
			$this->form_validation->set_rules('faddress','Father Address','trim|required|max_length[255]');
			$this->form_validation->set_rules('fcity','Father City','trim|required');
            $this->form_validation->set_rules('fphone','Father Phone','trim|required|numeric|max_length[14]');
            
            if($this->form_validation->run() == FALSE)
            {
                $this->familydetails();
            }
            else*/
            {
				$submitvalue = $this->security->xss_clean($this->input->post('submitvalue'));

				$userId = $this->vendorId;
				$stepexist = $this->application_model->CheckAllStepExist($userId);
				$checkpicUpload = $this->application_model->checkpicUpload($userId);
				//echo $checkpicUpload[0]->pic;
                if(empty($checkpicUpload[0]->pic))
				{
					$this->session->set_flashdata('error', 'Applicantion Submittion failed. Please Upload Picture in correct format');
					redirect('personaldetail');
				}
				
                if($stepexist[0]->step_personal != 1 || $stepexist[0]->step_address != 1 || $stepexist[0]->step_programs != 1 || $stepexist[0]->step_family != 1 || $stepexist[0]->step_education != 1)
                {
       			    $this->session->set_flashdata('error', 'Applicantion Submittion failed. Please Compelete Form');
					redirect('savedetails');
                }
                else
                {
               
				
				$submitvalue = $this->security->xss_clean($this->input->post('submitvalue'));
				
				if($submitvalue == 1)
				
				{				
                $userId = $this->vendorId;
				
				$Updatesave = array('step_save'=>1);
			    $updateSaveInfo = $this->application_model->UpdateStepSaveInfo($Updatesave, $userId);
				
                $otherInfo = array('fkCurrentStatus'=>2, 'fkModifiedBy'=>$this->vendorId, 'updated_at'=>date('Y-m-d H:i:s'));
                
                $this->load->model('application_model');
				
                $updateOtherInfo = $this->application_model->UpdateSubmitInfo($userId, $otherInfo);
				
                if($updateOtherInfo > 0)
                {
					
					
       			    $this->session->set_flashdata('success', 'Application Save successfully');
                }
                else
                {
                    $this->session->set_flashdata('error', 'Applicantion not saved');
                }
				
				}
				else
                {
                    $this->session->set_flashdata('error', 'Applicantion Saving failed');
                }
                redirect('appsubmit');
              }
		   }
        }
    }

    
    /**
     * This function is used load user edit information
     * @param number $userId : Optional : This is user id
     */
    function editOld($userId = NULL)
    {
        if($this->isAdmin() == TRUE || $userId == 1)
        {
            $this->loadThis();
        }
        else
        {
            if($userId == null)
            {
                redirect('userListing');
            }
            
            $data['roles'] = $this->user_model->getUserRoles();
            $data['userInfo'] = $this->user_model->getUserInfo($userId);
            
            $this->global['pageTitle'] = 'Admission IIUI : Edit User';
            
            $this->loadViews("editOld", $this->global, $data, NULL);
        }
    }
	
	/**
     * This function is used to add/Edit Applicant Language Info to the system
     */
    function updatelanguagedetail()
    {
        /*if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else*/
        {
            /*$this->load->library('form_validation');
            
            $this->form_validation->set_rules('addresspmt','Permanent Address','trim|required|max_length[255]');
			$this->form_validation->set_rules('citypmt','Permanent City','trim|required');
            $this->form_validation->set_rules('phpmt','Permanent Phone','trim|required|numeric|max_length[14]');
			$this->form_validation->set_rules('maddress','Mailing Address','trim|required|max_length[255]');
			$this->form_validation->set_rules('mcity','Mailing City','trim|required');
            $this->form_validation->set_rules('mphone','Mailing Phone','trim|required|numeric|max_length[14]');
			$this->form_validation->set_rules('faddress','Father Address','trim|required|max_length[255]');
			$this->form_validation->set_rules('fcity','Father City','trim|required');
            $this->form_validation->set_rules('fphone','Father Phone','trim|required|numeric|max_length[14]');
          
            if($this->form_validation->run() == FALSE)
            {
                $this->languageproficency();
            }
            else*/
            {
				$arabicWritten = $this->security->xss_clean($this->input->post('arabicWritten'));
				$arabicSpoken = $this->security->xss_clean($this->input->post('arabicSpoken'));
				$englishWritten = $this->security->xss_clean($this->input->post('englishWritten'));
				$englishSpoken = $this->security->xss_clean($this->input->post('englishSpoken'));
				$language3 = $this->security->xss_clean($this->input->post('language3'));
				$language3Written = $this->security->xss_clean($this->input->post('language3Written'));
				$language3Spoken = $this->security->xss_clean($this->input->post('language3Spoken'));
				$language4 = $this->security->xss_clean($this->input->post('language4'));
				$language4Written = $this->security->xss_clean($this->input->post('language4Written'));
				$language4Spoken = $this->security->xss_clean($this->input->post('language4Spoken'));
	
                $userId = $this->vendorId;
				
                $langInfo = array('arabicWritten'=>$arabicWritten, 'arabicSpoken'=>$arabicSpoken, 'englishWritten'=>$englishWritten, 'englishSpoken'=>$englishSpoken, 'language3'=>$language3,'language3Written'=>$language3Written, 'language3Spoken'=>$language3Spoken, 'language4'=>$language4, 'language4Written'=>$language4Written,'language4Spoken'=>$language4Spoken,'modifiedBy'=>$this->vendorId, 'updated_at'=>date('Y-m-d H:i:s'));
                
                $this->load->model('application_model');
                $updateAddressInfo = $this->application_model->UpdateLangInfo($userId, $langInfo);
				
				$Updatelanguage = array('step_language'=>1);
			    $updateLanguageInfo = $this->application_model->UpdateLnaguageInfo($Updatelanguage, $userId);
                
                if($updateAddressInfo > 0)
                {
       			    $this->session->set_flashdata('success', 'Applicant Language Detail Updated successfully');
                }
                else
                {
                    $this->session->set_flashdata('error', 'Applicant Language Info Updation failed');
                }
                
                redirect('aptitudetest');
            }
        }
    }


/**
     * This function is used to add/Edit Aptitude Test Info to the system
     */
    function updateaptitudetest()
    {
        /*if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else*/
        {
            /*$this->load->library('form_validation');
            
            $this->form_validation->set_rules('addresspmt','Permanent Address','trim|required|max_length[255]');
			$this->form_validation->set_rules('citypmt','Permanent City','trim|required');
            $this->form_validation->set_rules('phpmt','Permanent Phone','trim|required|numeric|max_length[14]');
			$this->form_validation->set_rules('maddress','Mailing Address','trim|required|max_length[255]');
			$this->form_validation->set_rules('mcity','Mailing City','trim|required');
            $this->form_validation->set_rules('mphone','Mailing Phone','trim|required|numeric|max_length[14]');
			$this->form_validation->set_rules('faddress','Father Address','trim|required|max_length[255]');
			$this->form_validation->set_rules('fcity','Father City','trim|required');
            $this->form_validation->set_rules('fphone','Father Phone','trim|required|numeric|max_length[14]');
          
            if($this->form_validation->run() == FALSE)
            {
                $this->languageproficency();
            }
            else*/
            {
				$APT_Test_1 = $this->security->xss_clean($this->input->post('APT_Test_1'));
				$APT_Test_1_Year = $this->security->xss_clean($this->input->post('APT_Test_1_Year'));
				$APT_Test_1_Marks = $this->security->xss_clean($this->input->post('APT_Test_1_Marks'));
				$APT_Test_1_Total_Marks = $this->security->xss_clean($this->input->post('APT_Test_1_Total_Marks'));
				$APT_Test_2 = $this->security->xss_clean($this->input->post('APT_Test_2'));
				$APT_Test_2_Year = $this->security->xss_clean($this->input->post('APT_Test_2_Year'));
				$APT_Test_2_Marks = $this->security->xss_clean($this->input->post('APT_Test_2_Marks'));
				$APT_Test_2_Total_Marks = $this->security->xss_clean($this->input->post('APT_Test_2_Total_Marks'));
				$APT_Test_3 = $this->security->xss_clean($this->input->post('APT_Test_3'));
				$APT_Test_3_Year = $this->security->xss_clean($this->input->post('APT_Test_3_Year'));
				$APT_Test_3_Marks = $this->security->xss_clean($this->input->post('APT_Test_3_Marks'));
				$APT_Test_3_Total_Marks = $this->security->xss_clean($this->input->post('APT_Test_3_Total_Marks'));
				$APT_Test_4 = $this->security->xss_clean($this->input->post('APT_Test_4'));
				$APT_Test_4_Year = $this->security->xss_clean($this->input->post('APT_Test_4_Year'));
				$APT_Test_4_Marks = $this->security->xss_clean($this->input->post('APT_Test_4_Marks'));
				$APT_Test_4_Total_Marks = $this->security->xss_clean($this->input->post('APT_Test_4_Total_Marks'));
	
                $userId = $this->vendorId;
				
                $apptitTestInfo = array('APT_Test_1'=>$APT_Test_1, 'APT_Test_1_Year'=>$APT_Test_1_Year, 'APT_Test_1_Marks'=>$APT_Test_1_Marks, 'APT_Test_1_Total_Marks'=>$APT_Test_1_Total_Marks,'APT_Test_2'=>$APT_Test_2, 'APT_Test_2_Year'=>$APT_Test_2_Year, 'APT_Test_2_Marks'=>$APT_Test_2_Marks, 'APT_Test_2_Total_Marks'=>$APT_Test_2_Total_Marks,'APT_Test_3'=>$APT_Test_3, 'APT_Test_3_Year'=>$APT_Test_3_Year, 'APT_Test_3_Marks'=>$APT_Test_3_Marks, 'APT_Test_3_Total_Marks'=>$APT_Test_3_Total_Marks,'APT_Test_4'=>$APT_Test_4, 'APT_Test_4_Year'=>$APT_Test_4_Year, 'APT_Test_4_Marks'=>$APT_Test_4_Marks, 'APT_Test_4_Total_Marks'=>$APT_Test_4_Total_Marks,'modifiedBy'=>$this->vendorId, 'update_at'=>date('Y-m-d H:i:s'));
                
                $this->load->model('application_model');
                $updateAddressInfo = $this->application_model->UpdateAptitudeTestInfo($userId, $apptitTestInfo);
				
				$Updateapptitude = array('step_aptitude'=>1);
			    $UpdateapptitudeInfo = $this->application_model->UpdateApptitudeInfo($Updateapptitude, $userId);
                
                if($updateAddressInfo > 0)
                {
       			    $this->session->set_flashdata('success', 'Applicant Aptitude Test Detail Updated successfully');
                }
                else
                {
                    $this->session->set_flashdata('error', 'Applicant Aptitude Test Updation failed');
                }
                
                redirect('familydetails');
            }
        }
    }
    
    
    /**
     * This function is used to edit the user information
     */
    function editUser()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->library('form_validation');
            
            $userId = $this->input->post('userId');
            
            $this->form_validation->set_rules('fname','Full Name','trim|required|max_length[128]');
            $this->form_validation->set_rules('email','Email','trim|required|valid_email|max_length[128]');
            $this->form_validation->set_rules('password','Password','matches[cpassword]|max_length[20]');
            $this->form_validation->set_rules('cpassword','Confirm Password','matches[password]|max_length[20]');
            $this->form_validation->set_rules('role','Role','trim|required|numeric');
            $this->form_validation->set_rules('mobile','Mobile Number','required|min_length[10]');
            
            if($this->form_validation->run() == FALSE)
            {
                $this->editOld($userId);
            }
            else
            {
                $name = ucwords(strtolower($this->security->xss_clean($this->input->post('fname'))));
                $email = $this->security->xss_clean($this->input->post('email'));
                $password = $this->input->post('password');
                $roleId = $this->input->post('role');
                $mobile = $this->security->xss_clean($this->input->post('mobile'));
                
                $userInfo = array();
                
                if(empty($password))
                {
                    $userInfo = array('email'=>$email, 'roleId'=>$roleId, 'name'=>$name,
                                    'mobile'=>$mobile, 'updatedBy'=>$this->vendorId, 'updatedDtm'=>date('Y-m-d H:i:s'));
                }
                else
                {
                    $userInfo = array('email'=>$email, 'password'=>getHashedPassword($password), 'roleId'=>$roleId,
                        'name'=>ucwords($name), 'mobile'=>$mobile, 'updatedBy'=>$this->vendorId, 
                        'updatedDtm'=>date('Y-m-d H:i:s'));
                }
                
                $result = $this->user_model->editUser($userInfo, $userId);
                
                if($result == true)
                {
                    $this->session->set_flashdata('success', 'User updated successfully');
                }
                else
                {
                    $this->session->set_flashdata('error', 'User updation failed');
                }
                
                redirect('userListing');
            }
        }
    }


    /**
     * This function is used to delete the user using userId
     * @return boolean $result : TRUE / FALSE
     */
    function deleteUser()
    {
        if($this->isAdmin() == TRUE)
        {
            echo(json_encode(array('status'=>'access')));
        }
        else
        {
            $userId = $this->input->post('userId');
            $userInfo = array('isDeleted'=>1,'updatedBy'=>$this->vendorId, 'updatedDtm'=>date('Y-m-d H:i:s'));
            
            $result = $this->user_model->deleteUser($userId, $userInfo);
            
            if ($result > 0) { echo(json_encode(array('status'=>TRUE))); }
            else { echo(json_encode(array('status'=>FALSE))); }
        }
    }
    
    /**
     * This function is used to load the change password screen
     */
    function loadChangePass()
    {
        $this->global['pageTitle'] = 'Admission IIUI : Change Password';
        
        $this->loadViews("changePassword", $this->global, NULL, NULL);
    }
    
    
    /**
     * This function is used to change the password of the user
     */
    function changePassword()
    {
        $this->load->library('form_validation');
        
        $this->form_validation->set_rules('oldPassword','Old password','required|max_length[20]');
        $this->form_validation->set_rules('newPassword','New password','required|max_length[20]');
        $this->form_validation->set_rules('cNewPassword','Confirm new password','required|matches[newPassword]|max_length[20]');
        
        if($this->form_validation->run() == FALSE)
        {
            $this->loadChangePass();
        }
        else
        {
            $oldPassword = $this->input->post('oldPassword');
            $newPassword = $this->input->post('newPassword');
            
            $resultPas = $this->user_model->matchOldPassword($this->vendorId, $oldPassword);
            
            if(empty($resultPas))
            {
                $this->session->set_flashdata('nomatch', 'Your old password not correct');
                redirect('loadChangePass');
            }
            else
            {
                $usersData = array('password'=>getHashedPassword($newPassword), 'updatedBy'=>$this->vendorId,
                                'updatedDtm'=>date('Y-m-d H:i:s'));
                
                $result = $this->user_model->changePassword($this->vendorId, $usersData);
                
                if($result > 0) { $this->session->set_flashdata('success', 'Password updation successful'); }
                else { $this->session->set_flashdata('error', 'Password updation failed'); }
                
                redirect('loadChangePass');
            }
        }
    }

    /**
     * Page not found : error 404
     */
    function pageNotFound()
    {
        $this->global['pageTitle'] = 'Admission IIUI : 404 - Page Not Found';
        
        $this->loadViews("404", $this->global, NULL, NULL);
    }

    /**
     * This function used to show login history
     * @param number $userId : This is user id
     */
    function loginHistoy($userId = NULL)
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $userId = ($userId == NULL ? $this->session->userdata("userId") : $userId);

            $searchText = $this->input->post('searchText');
            $fromDate = $this->input->post('fromDate');
            $toDate = $this->input->post('toDate');

            $data["userInfo"] = $this->user_model->getUserInfoById($userId);

            $data['searchText'] = $searchText;
            $data['fromDate'] = $fromDate;
            $data['toDate'] = $toDate;
            
            $this->load->library('pagination');
            
            $count = $this->user_model->loginHistoryCount($userId, $searchText, $fromDate, $toDate);

            $returns = $this->paginationCompress ( "login-history/".$userId."/", $count, 5, 3);

            $data['userRecords'] = $this->user_model->loginHistory($userId, $searchText, $fromDate, $toDate, $returns["page"], $returns["segment"]);
            
            $this->global['pageTitle'] = 'Admission IIUI : User Login History';
            
            $this->loadViews("loginHistory", $this->global, $data, NULL);
        }        
    }
	function alpha_dash_space($fullname){
		if (! preg_match('/^[a-zA-Z\s]+$/', $fullname)) {
			$this->form_validation->set_message('alpha_dash_space', 'The %s field may only contain alpha characters & White spaces');
			return FALSE;
		} else {
			return TRUE;
		}
	}
}

?>