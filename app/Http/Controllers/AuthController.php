<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function index()
    {
    	if ( request()->method() == 'POST' ) {
    		$this->authenticateUser( request('email') );
    		return redirect()->route('dashboard.index');
    	}
    	$users = User::all();
    	return view('auth.users', compact('users'));
    }

    public function authenticateUser( $email )
    {
    	$user = User::where('email', $email)->first();
    	if ( $user ) {
    		Auth::login($user);
    	}
    }
}
