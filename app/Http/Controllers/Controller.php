<?php namespace App\Http\Controllers;

use Mail;
use Carbon\Carbon;
use App\Model\UserLastLogin;
use App\Model\UserLogQualification;
use Illuminate\Http\Request;
use Validator, Input, Redirect;
use App\Model\{Semester,Permission};
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;
use App\Traits\PQM;


abstract class Controller extends BaseController {

	use DispatchesJobs, ValidatesRequests, PQM;

	protected $user_id ;
	protected $users ;

	public function __construct()
	{
		$this->middleware('ipblocked');
		$driver             = config('database.default');
        $database           = config('database.connections');
       
        $this->db           = $database[$driver]['database'];
        $this->dbuser       = $database[$driver]['username'];
        $this->dbpass       = $database[$driver]['password'];
        $this->dbhost       = $database[$driver]['host']; 

        // Load Sximo Config
        $sximo 	= config('sximo');
        $this->config = $sximo ;
		$this->data['sximoconfig'] = $sximo ;  		
	} 
	
	public function generateAndStorePQM($application)
    {
        $pqm = $this->calculatePqm(
            $application->id,
			// lines start for Covid PQM
            $application->faculty->title,
            $application->applicant->previousQualification->HSC_Title,
            $application->applicant->previousQualification->resultAwaiting,
            $application->program->title,
            $application->applicant->previousQualification->llb_obtained_marks,
            $application->applicant->previousQualification->llb_total_marks,
			// lines ends for Covid PQM
            $application->program->fkLeveliId,
            $application->program->programscheduler->interviewDateTime,
            $application->program->fkReqId,
            $application->applicant->previousQualification->SSC_Total,
            $application->applicant->previousQualification->SSC_Composite,
			// $application->applicant->previousQualification->resultAwaiting, // commented for covid PQM
            $application->applicant->previousQualification->HSSC_Total,
            $application->applicant->previousQualification->HSSC_Composite,
            $application->applicant->previousQualification->HSSC_Total1,
            $application->applicant->previousQualification->HSSC_Total2,
            $application->applicant->previousQualification->HSSC1,
            $application->applicant->previousQualification->HSSC2,
            $application->applicant->previousQualification->BA_Bsc_Total,
            $application->applicant->previousQualification->BA_Bsc,
            $application->applicant->previousQualification->MSc_Total,
            $application->applicant->previousQualification->MSc,
            $application->applicant->previousQualification->BS_Total,
            $application->applicant->previousQualification->BS,
            $application->applicant->previousQualification->MS_Total,
            $application->applicant->previousQualification->MS
        );
        $application->pqm = $pqm;
        $application->save();
        // PreviousQualification::where('fkApplicantId', $application->fkApplicantId)
        //     ->where('fkSemesterId', $this->currentSemester)
        //     ->update([ 'pqm' => $pqm ]);
	}
	
	public function getCurrentUserPermissions() 
	{
		return Cache::get(auth()->guard('web')->id().'-permissions', function () {
			return Permission::where('fkUserId', auth()->guard('web')->id())->first();
		});
	}

	public static function getCurrentSemester()
	{
		return Cache::get('activeSemester', function() {
			return Semester::where('status', 1)->first();
		});
	}

	public function decodeAdminData( $hash ) {
		return  explode('/' ,base64_decode($hash))[0];
	}

	function getComboselect( Request $request)
	{

		if($request->ajax() == true && \Auth::check() == true)
		{
			$param = explode(':',$request->input('filter'));
			$parent = (!is_null($request->input('parent')) ? $request->input('parent') : null);
			$limit = (!is_null($request->input('limit')) ? $request->input('limit') : null);
			$rows = $this->model->getComboselect($param,$limit,$parent);
			$items = array();
		
			$fields = explode("|",$param[2]);
			
			foreach($rows as $row) 
			{
				$value = "";
				foreach($fields as $item=>$val)
				{
					if($val != "") $value .= $row->{$val}." ";
				}
				$items[] = array($row->{$param['1']} , $value); 	
	
			}
			
			return json_encode($items); 	
		} else {
			return json_encode(array('OMG'=>" Ops .. Cant access the page !"));
		}	
	}

	public function getCombotable( Request $request)
	{
		if(Request::ajax() == true && Auth::check() == true)
		{				
			$rows = $this->model->getTableList($this->db);
			$items = array();
			foreach($rows as $row) $items[] = array($row , $row); 	
			return json_encode($items); 	
		} else {
			return json_encode(array('OMG'=>"  Ops .. Cant access the page !"));
		}				
	}		
	
	public function getCombotablefield( Request $request)
	{
		if($request->input('table') =='') return json_encode(array());	
		if(Request::ajax() == true && Auth::check() == true)
		{	

				
			$items = array();
			$table = $request->input('table');
			if($table !='')
			{
				$rows = $this->model->getTableField($request->input('table'));			
				foreach($rows as $row) 
					$items[] = array($row , $row); 				 	
			} 
			return json_encode($items);	
		} else {
			return json_encode(array('OMG'=>"  Ops .. Cant access the page !"));
		}					
	}

	function postMultisearch( Request $request)
	{
		$post = $_POST;
		$items ='';
		foreach($post as $item=>$val):
			if($_POST[$item] !='' and $item !='_token' and $item !='md' && $item !='id'):
				$items .= $item.':'.trim($val).'|';
			endif;	
		
		endforeach;
		
		return Redirect::to($this->module.'?search='.substr($items,0,strlen($items)-1).'&md='.$request->inpuy('md'));
	}

	function buildSearch( $map = false)
	{

			$keywords = ''; $fields = '';	$param ='';
			$allowsearch = $this->info['config']['forms'];
			foreach($allowsearch as $as) $arr[$as['field']] = $as ;		
			$mapping = '';
			if($_GET['search'] !='')
			{
				$type = explode("|",$_GET['search'] );
				if(count($type) >= 1)
				{
					foreach($type as $t)
					{
						$keys = explode(":",$t);
						if(in_array($keys[0],array_keys($arr))):
							if($arr[$keys[0]]['type'] == 'select' || $arr[$keys[0]]['type'] == 'radio' )
							{
								$param .= " AND ".$arr[$keys[0]]['alias'].".".$keys[0]." ".self::searchOperation($keys[1])." '".$keys[2]."' ";	
								$mapping .= $keys[0].' '.self::searchOperation($keys[1]).' '.$keys[2]. '<br />';

							} else {
								$operate = self::searchOperation($keys[1]);
								if($operate == 'like')
								{
									$param .= " AND ".$arr[$keys[0]]['alias'].".".$keys[0]." LIKE '%".$keys[2]."%%' ";	
									$mapping .= $keys[0].' LIKE '.$keys[2]. '<br />';	
								} else if( $operate =='is_null') {
									$param .= " AND ".$arr[$keys[0]]['alias'].".".$keys[0]." IS NULL ";
									$mapping .= $keys[0].' IS NULL <br />';

								} else if( $operate =='not_null') {
									$param .= " AND ".$arr[$keys[0]]['alias'].".".$keys[0]." IS NOT NULL ";
									$mapping .= $keys[0].' IS NOT NULL <br />';

								} else if( $operate =='between') {
									$param .= " AND (".$arr[$keys[0]]['alias'].".".$keys[0]." BETWEEN '".$keys[2]."' AND '".$keys[3]."' ) ";								
									$mapping .= $keys[0].' BETWEEN '.$keys[2]. ' - '. $keys[3] .'<br />';
								} else {
									$param .= " AND ".$arr[$keys[0]]['alias'].".".$keys[0]." ".self::searchOperation($keys[1])." '".$keys[2]."' ";
									$mapping .= $keys[0].' '.self::searchOperation($keys[1]).' '.$keys[2]. '<br />';	
								}												
							}						
						endif;	
					}
				} 
			}

		if($map == true)
		{
			return $param = array(
					'param'	=> $param,
					'maps'	=> '
					<div class="infobox infobox-info fade in" style="font-size:10px;">
					  <button data-dismiss="alert" class="close" type="button"> x </button>  
					 <b class="text-danger"> Search Result </b> :  <br /> '.$mapping.'
					</div>
					'
				);			

		} else {
			return $param;
		}		
		
	
	}


	function onSearch( $params )
	{
		// Used for extracting URL GET search 
		$psearch = explode('|',$params);
		$currentSearch = array();
		foreach($psearch as $ps)
		{
			$tosearch = explode(':',$ps);
			if(count($tosearch) >=2)
			$currentSearch[$tosearch[0]] = $tosearch[2]; 
		}
		return $currentSearch;		
	}
	
	function searchOperation( $operate)
	{
		$val = '';
		switch ($operate) {
			case 'equal':
				$val = '=' ;
				break;
			case 'bigger_equal':
				$val = '>=' ;
				break;
			case 'smaller_equal':
				$val = '<=' ;
				break;				
			case 'smaller':
				$val = '<' ;
				break;
			case 'bigger':
				$val = '>' ;
				break;
			case 'not_null':
				$val = 'not_null' ;
				break;								

			case 'is_null':
				$val = 'is_null' ;
				break;	

			case 'like':
				$val = 'like' ;
				break;	

			case 'between':
				$val = 'between' ;
				break;					

			default:
				$val = '=' ;
				break;
		}
		return $val;
	}		

	function inputLogs(Request $request, $note = NULL)
	{
		$data = array(
			'module'	=> $request->segment(1),
			'task'		=> $request->segment(2),
			'user_id'	=> Session::get('uid'),
			'ipaddress'	=> $request->getClientIp(),
			'note'		=> $note
		);
		\DB::table( 'tb_logs')->insert($data);		;
	
	}

	function validateForm( $forms = array() )
	{
		if(count($forms) <= 0)
			$forms = $this->info['config']['forms'];
		
		$rules = array();
		foreach($forms as $form)
		{
			if($form['required']== '' || $form['required'] !='0' )
			{
				$rules[$form['field']] = $form['required'];
			} 
			
				
			if( $form['type'] =='file')
			{
				if($form['required'] =='required')
				{
					$validation = 'required';
					if($form['option']['upload_type'] =='image')
					{
						$validation .= '|mimes:jpg,jpeg,png,gif,bmp';

					} else {
						$validation .= '|mimes:zip,csv,xls,doc,docx,xlsx,pdf,rtf';						
					}

					if($form['option']['image_multiple'] != '1')
					{
						// IF SINGLE UPLOAD FILE OR IMAGE 	
						$rules[$form['field']] = $validation;

					}  else {
						// IF MULTIPLE UPLOAD FILE OR IMAGE 	
						$FilesArray = [];
						if(isset($_FILES[$form['field']])) {
							if(count($_FILES[$form['field']]) >=1 )
							{
								$nbr = count($_FILES[$form['field']]) - 1;
								foreach(range(0, $nbr) as $index) {
								   // $imagesArray['images.' . $index] = 'required|image';
								    $rules[$form['field'].'.'.$index] = $validation;
								}
							}
						}	
					}


					
				
				} else {
					
					if($form['option']['upload_type'] =='image')
					{
						$validation = 'mimes:jpg,jpeg,png,gif,bmp';
					} 
					else {
						$validation = 'mimes:zip,csv,xls,doc,docx,xlsx';						
					}

					if($form['option']['image_multiple'] != '1')
					{
						// IF SINGLE UPLOAD FILE OR IMAGE 	
						$rules[$form['field']] = $validation;

					}  else {
						// IF MULTIPLE UPLOAD FILE OR IMAGE 	
						$FilesArray = [];
						if(isset($_FILES[$form['field']])) {
							if(count($_FILES[$form['field']]) >=1 )
							{
								$nbr = count($_FILES[$form['field']]) - 1;
								foreach(range(0, $nbr) as $index) {
								   // $imagesArray['images.' . $index] = 'required|image';
								    $rules[$form['field'].'.'.$index] = $validation;
								}
							}
						}	

					}
				} 
			}							
		}	
		return $rules ;


	}	

	function validateListError( $rules )
    {
        $errMsg = __('core.note_error') ;
        $errMsg .= '<hr /> <ul>';
        foreach($rules as $key=>$val)
        {
            $errMsg .= '<li>'.$key.' : '.$val[0].'</li>';
        }
        $errMsg .= '</li>'; 
        return $errMsg;
    }

	function validatePost(  $request )
	{	
		$str = $this->info['config']['forms'];
		$data = array();
		foreach($str as $f){
			$field = $f['field'];
			// Update for V5.1.5 issue on Autofilled createOn and updatedOn fields
			if($field == 'createdOn') $data['createdOn'] = date('Y-m-d H:i:s');
            if($field == 'updatedOn') $data['updatedOn'] = date('Y-m-d H:i:s');			
			if($f['view'] ==1) 
			{
				

				if($f['type'] =='textarea_editor' || $f['type'] =='textarea')
				{
					 $data[$field] = $request->input( $field );
				} else {

					if(!is_null( $request->input( $field ) )) $data[$field] = $request->input( $field ) ;

					// if post is file or image		


					if($f['type'] =='file')
					{	
						
						if(!is_dir(public_path().$f['option']['path_to_upload']))
							mkdir( public_path().$f['option']['path_to_upload'],0777 );   

						$files ='';	
						if($f['option']['upload_type'] =='file')
						{

							if(isset($f['option']['image_multiple']) && $f['option']['image_multiple'] ==1)
							{								
								if(isset($_POST['curr'.$field]))
								{
									$curr =  '';
									for($i=0; $i<count($_POST['curr'.$field]);$i++)
									{
										$files .= $_POST['curr'.$field][$i].',';
									}
								}	

								if(!is_null($request->file($field)))
								{

									$destinationPath = '.'. $f['option']['path_to_upload']; 	
									foreach($_FILES[$field]['tmp_name'] as $key => $tmp_name ){
									 	$file_name = $_FILES[$field]['name'][$key];
										$file_tmp =$_FILES[$field]['tmp_name'][$key];
										if($file_name !='')
										{
											move_uploaded_file($file_tmp,$destinationPath.'/'.$file_name);
											$files .= $file_name.',';

										}
										
									}
									
									if($files !='')	$files = substr($files,0,strlen($files)-1);	
									$data[$field] = $files;
								} else {
									unset($data[$field]);	
								}	
																					

							} else {
							
								if(!is_null($request->file($field)))
								{								

									$file = $request->file($field); 
								 	$destinationPath = '.'. $f['option']['path_to_upload']; 
									$filename = $file->getClientOriginalName();
									$extension =$file->getClientOriginalExtension(); //if you need extension of the file
									$rand = rand(1000,100000000);
									$newfilename = strtotime(date('Y-m-d H:i:s')).'-'.$rand.'.'.$extension;
									$uploadSuccess = $file->move($destinationPath, $newfilename);
									if( $uploadSuccess ) {
									   $data[$field] = $newfilename;
									}
								}	 
							}	

						} else {

							if(!is_dir(public_path().$f['option']['path_to_upload']))
								mkdir( public_path().$f['option']['path_to_upload'],0777 );   

							if(isset($f['option']['image_multiple']) && $f['option']['image_multiple'] ==1)
							{
								$files = '';
								if(isset($_POST['curr'.$field]))
								{
									$curr =  '';
									for($i=0; $i<count($_POST['curr'.$field]);$i++)
									{
										$files .= $_POST['curr'.$field][$i].',';
									}
								}

								$destinationPath = '.'. $f['option']['path_to_upload']; 
								if(count($request->file($f['field'])) >=1 )
								{
									
									$destinationPath = '.'. $f['option']['path_to_upload']; 
									foreach($_FILES[$field]['tmp_name'] as $key => $tmp_name ){
									 	$file_name = $_FILES[$field]['name'][$key];
										$file_tmp =$_FILES[$field]['tmp_name'][$key];
										if($file_name !='')
										{
											//move_uploaded_file($file_tmp,$destinationPath.'/'.$file_name);
											//echo  $file_name.'<br />';
											$file = $request->file($field)[$key];
											$filename = $file->getClientOriginalName();
											$extension =$file->getClientOriginalExtension(); //if you need extension of the file
											$rand = rand(1000,100000000);
											$newfilename = strtotime(date('Y-m-d H:i:s')).'-'.$rand.'.'.$extension;
											$files .= $newfilename.',';

											$uploadSuccess = $file->move($destinationPath, $newfilename);


											 if($f['option']['resize_width'] != '0' && $f['option']['resize_width'] !='')
											 {
											 	if( $f['option']['resize_height'] ==0 )
												{
													$f['option']['resize_height']	= $f['option']['resize_width'];
												}
											 	$orgFile = $destinationPath.'/'.$newfilename;
												 \SiteHelpers::cropImage($f['option']['resize_width'] , $f['option']['resize_height'] , $orgFile ,  $extension,	 $orgFile)	;						 
											 }
										}										
									}

																	
								} 
								if($files !='')	$files = substr($files,0,strlen($files)-1);	
									$data[$field] = $files;	

								

							} else {

								if(!is_null($request->file($field)))
								{
									$file = $request->file($field); 
								 	$destinationPath = '.'. $f['option']['path_to_upload']; 
									$filename = $file->getClientOriginalName();
									$extension =$file->getClientOriginalExtension(); //if you need extension of the file
									$rand = rand(1000,100000000);
									$newfilename = strtotime(date('Y-m-d H:i:s')).'-'.$rand.'.'.$extension;


									$uploadSuccess = $file->move($destinationPath, $newfilename);


									 if($f['option']['resize_width'] != '0' && $f['option']['resize_width'] !='')
									 {
									 	if( $f['option']['resize_height'] ==0 )
										{
											$f['option']['resize_height']	= $f['option']['resize_width'];
										}
									 	$orgFile = $destinationPath.'/'.$newfilename;
										 \SiteHelpers::cropImage($f['option']['resize_width'] , $f['option']['resize_height'] , $orgFile ,  $extension,	 $orgFile)	;						 
									 }
									 
									if( $uploadSuccess ) {
									   $data[$field] = $newfilename;
									}
								}	 
							}		

						}						
					}	

					// Handle Checkbox input 
					if($f['type'] =='checkbox')
					{
						if(!is_null( $request->{$field} ))
						{
							$data[$field] = implode(",", $request->input( $field ));
						} else {
							$data[$field] = '0';	
						}
					}
					// if post is date						
					if($f['type'] =='date')
					{
						$data[$field] = date("Y-m-d",strtotime($request->input($field)));
					}
					
					// if post is seelct multiple						
					if($f['type'] =='select')
					{
						//echo '<pre>'; print_r( $_POST[$field] ); echo '</pre>'; 
						if( isset($f['option']['select_multiple']) &&  $f['option']['select_multiple'] ==1 )
						{
							if(isset($_POST[$field]))
							{
								$multival = implode(",",$request->input( $field )); 
								$data[$field] = $multival;
							}
						} else {
							$data[$field] = $request->input( $field );
						}	
					}									
					
				}	 						

			}	
		}
		$this->access = $this->model->validAccess($this->info['id'] , session('gid'));
		$global	= (isset($this->access['is_global']) ? $this->access['is_global'] : 0 );
		
		if($global == 0 )
			$data['entry_by'] = \Session::get('uid');
		/* Added for Compatibility laravel 5.2 */
		$values = array();
		foreach($data as $key=>$val)
		{
			if($val !='') $values[$key] = $val;
		}			
		return $values;

	}


	function postFilter( Request $request)
	{
		$module = $this->module;
		$sort 	= (!is_null($request->input('sort')) ? $request->input('sort') : '');
		$order 	= (!is_null($request->input('order')) ? $request->input('order') : '');
		$rows 	= (!is_null($request->input('rows')) ? $request->input('rows') : '');
		$md 	= (!is_null($request->input('md')) ? $request->input('md') : '');
		
		$filter = '?';
		if($sort!='') $filter .= '&sort='.$sort; 
		if($order!='') $filter .= '&order='.$order; 
		if($rows!='') $filter .= '&rows='.$rows; 
		if($md !='') $filter .= '&md='.$md;
		 
		 

		return Redirect::to($this->data['pageModule'] . $filter);
	
	}	

	function injectPaginate()
	{

		$sort 	= (isset($_GET['sort']) 	? $_GET['sort'] : '');
		$order 	= (isset($_GET['order']) 	? $_GET['order'] : '');
		$rows 	= (isset($_GET['rows']) 	? $_GET['rows'] : '');
		$search = (isset($_GET['search']) ? $_GET['search'] : '');
		$s 		= (isset($_GET['s']) ? $_GET['s'] : '');

		$appends = array();
		if($sort!='') 	$appends['sort'] = $sort; 
		if($order!='') 	$appends['order'] = $order; 
		if($rows!='') 	$appends['rows'] = $rows; 
		if($search!='') $appends['search'] = $search; 
		if($s!='') $appends['s'] = $s; 
		
		return $appends;
			
	}	

	function returnUrl()
	{
		$pages 	= (isset($_GET['page']) ? $_GET['page'] : '');
		$sort 	= (isset($_GET['sort']) ? $_GET['sort'] : '');
		$order 	= (isset($_GET['order']) ? $_GET['order'] : '');
		$rows 	= (isset($_GET['rows']) ? $_GET['rows'] : '');
		$search 	= (isset($_GET['search']) ? $_GET['search'] : '');
		
		$appends = array();
		if($pages!='') 	$appends['page'] = $pages; 
		if($sort!='') 	$appends['sort'] = $sort; 
		if($order!='') 	$appends['order'] = $order; 
		if($rows!='') 	$appends['rows'] = $rows; 
		if($search!='') $appends['search'] = $search; 
		
		$url = "";
		foreach($appends as $key=>$val)
		{
			$url .= "&$key=$val";
		}
		return $url;
			
	}	

	public function getRemovecurrentfiles( Request $request)
	{
		$id 	= $request->input('id');
		$field 	= $request->input('field');
		$file 	= $request->input('file');
		if(file_exists('./'.$file) && $file !='')
		{
			if(unlink('.'.$file))
			{
				\DB::table($this->info['table'])->where($this->info['key'],$id)->update(array($field=>''));
			}
			return response()->json(array('status'=>'success'));
		} else {
			return response()->json(array('status'=>'error'));
		}
	}	

	public function getRemovefiles( $request)
	{
		$files = '.'.$request->input('file');
		if(file_exists($files) && $files !='')
		{
			unlink( $files);
		}
		return response()->json(array('status'=>'success'));
	}


	public function getSearch( $mode = 'native' )
	{
		if(isset($_GET['type']))
			$mode = $_GET['type'];

		$this->data['tableForm'] 	= $this->info['config']['forms'];	
		$this->data['tableGrid'] 	= $this->info['config']['grid'];
		$this->data['searchMode'] 	= $mode ;
		$this->data['pageUrl']		= url($this->module);
		return view('sximo.module.utility.search',$this->data);
	
	}

	function getImport( Request $request)
	{
		$task = $request->input('template');
		if($task !='')
		{
			$fields =  $this->info['config']['grid'];
			$output = fopen('php://output', 'w');
			$head= array();
			foreach($fields as $f )
			{
				$head[] = $f['field'];
			}

			fputcsv($output, $head);
			header('Content-Type: text/csv; charset=utf-8');
			header('Content-Disposition: attachment; filename='.$this->module.'.csv');	

		} else {
			$this->data['url'] = url($this->module) ;
			$this->data['module'] = $this->module ;
			return view('sximo.module.utility.import', $this->data);
		}	

	}

	function postImport( $request)
	{

		if(!is_null($request->file('fileimport')))
		{
			$file = 	$request->file('fileimport');
			$filename = $file->getClientOriginalName();
			$uploadSuccess = $file->move('./uploads/' , $filename );
			if( $uploadSuccess ) {
				$csv = array_map('str_getcsv', file('./uploads/'.$filename));
				$table = $this->info['config']['grid'];
				$fields = array();
				foreach($table as $f )
				{
					$fields[] = $f['field'];
				}
				//print_r($fields);
				foreach($csv as $row) {
					$data = array();
					foreach($fields as $key=>$val)
					{
						if($key != 0 )
							$data[$val] = (isset($row[$key]) ? $row[$key] : '' ) ;	
					}
					//print_r($data);
					//echo $row[0];
					$this->model->insertRow($data ,$row[0]);	
											
				}
				
				return response()->json(array('status'	=> 'success','message'=>'Csv Imported Successful !'));			  
			} else {
				return response()->json(array('status'	=> 'error','message'=>'Upload Failed!'));	
			}
		} else {			
			return response()->json(array('status'	=> 'error','message'=>'Please select file to Upload!'));
		}	

	}

	public function getExpotion()
	{
		$this->data['pageUrl'] = url( $this->data['pageModule']);
		return view('sximo.module.utility.export', $this->data);
	}
	public function getExport( Request $request, $t = 'excel')
	{
		if(!is_null($request->input('filter')))
			return $this->getExpotion();

		$t = $request->input('t');
		$this->access = $this->model->validAccess($this->info['id'] , session('gid'));

		if(isset($this->access['is_'.$t]) ){
			if($this->access['is_'.$t] == 0 )
				return redirect('dashboard')->with('message', __('core.note_restric'))->with('status','error');
		} else {
			return redirect('dashboard')->with('message', __('core.note_restric'))->with('status','error');
		}

		$info 		= $this->model->makeInfo( $this->module);
		$filter = '';	
		if(!is_null($request->input('search')))
		{
			$search = 	$this->buildSearch('maps');
			$filter = $search['param'];
			$this->data['search_map'] = $search['maps'];
		} 

		$params 	= array(
					'params'	=> $filter ,
					'fstart'	=> $request->input('fstart'),
					'flimit'	=> $request->input('flimit')	
		);		

		$results 	= $this->model->getRows( $params );
		$fields		= $info['config']['grid'];
		$rows		= $results['rows'];
		$content 	= array(
						'fields' => $fields,
						'rows' => $rows,
						'title' => $this->data['pageTitle'],
					);
		
		if($t == 'word')
		{			
			 return view('sximo.module.utility.word',$content);
			 
		} else if($t == 'pdf') {		
		 
		 	$html = view('sximo.module.utility.pdf', $content)->render();
	    	return \PDF::load($html)->show();
			
		} else if($t == 'csv') {		
		 
			return view('sximo.module.utility.csv',$content);
			
		} else if ($t == 'print') {
		
			//return view('sximo.module.utility.print',$content);
			$data['html'] = view('sximo.module.utility.print', $content)->render();	
			return view('layouts.blank',$data);			 
			 				 
		} else  {			
		
			 return view('sximo.module.utility.excel',$content);
		}	
	}		

	function getLookup( $request)
	{
		$id = $request->input('param');
		$args = explode("-",$id);
		if(count($args)>=2) 
		{

			$model = '\\App\\Models\\'.ucwords($args['3']);
			$model = new $model();
			$info = $model->makeInfo( $args['3'] );
			$data['pageTitle'] = $info['title'];
			$data['pageNote'] = $info['note'];			
			$params = array(
				'params'	=> " And ".$args['4'].".".$args['5']." ='". $args['6'] ."'",
				//'global'	=> (isset($this->access['is_global']) ? $this->access['is_global'] : 0 )
			);
			$results = $model->getRows( $params );	
			$data['access']		=$model->validAccess($info['id']);
			$data['rowData']		= $results['rows'];
			$data['tableGrid'] 	= $info['config']['grid'];
			$data['tableForm'] 	= $info['config']['forms'];	
			$data['colspan']		= \SiteHelpers::viewColSpan($info['config']['grid']);
			$data['nested_subgrid']	= (isset($info['config']['subgrid']) ? $info['config']['subgrid'] : array());
			//print_r($data['nested_subgrid']);exit;
			$data['id'] 		= $args[6];
			$data['key']		= $info['key'];
			//$data['ids']		= 'md'-$info['id'];
			return view('sximo.module.utility.masterdetail',$data);

		} else {
			return 'Invalid Argument';
		}
	}


	function detailview( $model , $detail , $id )
	{
		
		$info = $model->makeInfo( $detail['module'] );
		$params = array(
			'params'	=> " And `".$detail['key']."` ='". $id ."'",
			'global'	=> (isset($this->access['is_global']) ? $this->access['is_global'] : 0 )
		);
		$results = $model->getRows( $params );	
		$data['rowData']		= $results['rows'];
		$data['tableGrid'] 		= $info['config']['grid'];
		$data['tableForm'] 		= $info['config']['forms'];	
		
		return $data;

		
			
	}


function detailviewsave( $model ,$request , $detail , $id )

	{
		//\DB::table($detail['table'])->where($detail['key'],$request[$detail['key']])->delete();

		$info = $model->makeInfo( $detail['module'] );
		$relation_key = $info['key'];
		$access = $model->validAccess($info['id'] , session('gid'));

		if($access['is_add'] == '1' && $access['is_edit'] == '1' )
		{
			$str = $info['config']['forms'];
			$global	= (isset($access['is_global']) ? $access['is_global'] : 0 );
			$total = count($request['counter']);
			$mkeys = array();
			$getAllCurrentData = \DB::table($detail['table'])->where($detail['master_key'] , $id  )->get();

			$pkeys = array();
			for($i=0; $i<$total;$i++) 
					$pkeys[] = $request['bulk_'.$relation_key][$i];

			foreach ($getAllCurrentData as $keys) {	
				if(!in_array($keys->{$relation_key} , $pkeys))		
				{	
					// Remove If items is not resubmited
					\DB::table($detail['table'])->where($relation_key,$keys->{$relation_key})->delete();
				}
			}

			for($i=0; $i<$total;$i++)
			{
				$data =array();
				foreach($str as $f){
					$field = $f['field'];
					if($f['view'] ==1)
					{
						if(isset($request['bulk_'.$field][$i]) && $request['bulk_'.$field][$i] !='')

						{
							$data[$f['field']] = $request['bulk_'.$field][$i];
						}
					}			
				}

				$rules = self::validateForm($str);
				$validator = Validator::make($data, $rules);
				if($validator->passes()) {	
					$data[$detail['key']] =  $id ;
					if($global == 0 )
						$data['entry_by'] = \Session::get('uid');
					
					// Check if data currentry exist
					$check = \DB::table($detail['table'])->where($relation_key , $request['bulk_'.$relation_key][$i]  )->get();
					if(count($check) >=1)
					{
						\DB::table($detail['table'])->where($relation_key ,  $request['bulk_'.$relation_key][$i] )->update($data);
					} else {
						unset($data[$relation_key]);
						\DB::table($detail['table'])->insert($data);	
					}
				}			

			}	
		}	

		

	}

	public function liveSearch( $request )
	{
		$query = '';
		$forms = $this->info['config']['forms'];
		$keyword = trim($request->input('s'));
		if(!is_null($request->input('search')))
			$keyword = trim($request->input('search')['value']);
		
		$i = 0;
		foreach($forms as $form)
		{
			if($form['search'] == '1'){
				if($i == 0){
					$query .= " AND ( ".$form['alias'].".".$form['field']." LIKE '".$keyword."%' ) ";
				}
				else {
					$query .= " OR ( ".$form['alias'].".".$form['field']." LIKE '".$keyword."%' ) ";	
				}
				$i++;
			}

		}
		return $query ;
	}

	public function grab( $request , $args = array())
	{

		$this->access = $this->access($this->info['id']);
		$sort = (!is_null($request->input('sort')) ? $request->input('sort') : $this->info['setting']['orderby']); 
		$order = (!is_null($request->input('order')) ? $request->input('order') :  $this->info['setting']['ordertype']);
		// End Filter sort and order for query 
		// Filter Search for query		
		$filter ='';	
		$filter .= (isset($args['params']) ? $args['params'] : '');
		if(!is_null($request->input('search')))
		{
			$search = 	$this->buildSearch('maps');
			$filter = $search['param'];
			$this->data['search_map'] = $search['maps'];
		} 
		if(!is_null($request->input('s')))
		{
			$filter .= $this->liveSearch( $request);
		}

		
		$page = $request->input('page', 1);
		$params = array(
			'page'		=> $page ,
			'limit'		=> (!is_null($request->input('rows')) ? filter_var($request->input('rows'),FILTER_VALIDATE_INT) : static::$per_page ) ,
			'sort'		=> $sort ,
			'order'		=> $order,
			'params'	=> $filter,
			'global'	=> (isset($this->access['is_global']) ? $this->access['is_global'] : 0 )
		);
		// Get Query 
		$results = $this->model->getRows( $params , session('uid') );				
		
		// Build pagination setting
		$pagination = new Paginator($results['rows'], $results['total'], $params['limit']);	
		if($this->info['type'] =='ajax'){
			$pagination->setPath( $this->module.'/data' );
		} else {
			$pagination->setPath( $this->module );	
		}
		$this->data['param']		= $params;
		$this->data['rowData']		= $results['rows'];
		// Build Pagination 
		$this->data['pagination']	= $pagination;
		// Build pager number and append current param GET
		$this->data['pager'] 		= $this->injectPaginate();	
		// Row grid Number 
		$this->data['i']			= ($page * $params['limit'])- $params['limit']; 
		// Grid Configuration 
		$this->data['tableGrid'] 	= $this->info['config']['grid'];
		$this->data['tableForm'] 	= $this->info['config']['forms'];
		$this->data['colspan'] 		= \SiteHelpers::viewColSpan($this->info['config']['grid']);		
		// Group users permission
		$this->data['access']		= $this->access;
		// Detail from master if any
		$this->data['fields'] =  \SiteHelpers::fieldLang($this->info['config']['grid']);
		// Master detail link if any 
		$this->data['subgrid']	= (isset($this->info['config']['subgrid']) ? $this->info['config']['subgrid'] : array());
		$this->data['setting'] 		= $this->info['setting']; 

		$this->data['insort']	= $sort ;
		$this->data['inorder']	= $order ;
				
		return  $this->data;
	}
	public function hook( $request , $id = null  ) 
	{
		$this->access = $this->access($this->info['id']);
		$row = $this->model->getRow($id);
		if($row)
		{
			$this->data['row'] =  $row;
			$this->data['fields'] 		=  \SiteHelpers::fieldLang($this->info['config']['grid']);
			$this->data['id'] = $id;
			$this->data['access']		= $this->access;
			$this->data['subgrid']	= (isset($this->info['config']['subgrid']) ? $this->info['config']['subgrid'] : array()); 
			$this->data['fields'] =  \SiteHelpers::fieldLang($this->info['config']['grid']);
			$this->data['prevnext'] = $this->model->prevNext($id);
			$this->data['setting'] 		= $this->info['setting']; 
			
		}  
		return $this->data;
	}

	function copy( $request)
	{
	    foreach(\DB::select("SHOW COLUMNS FROM ".  $this->info['table'] ) as $column)
        {
			if( $column->Field != $this->info['key'])
				$columns[] = $column->Field;
        }
		
		if(count($request->input('ids')) >=1)
		{
			$toCopy = implode(",",$request->input('ids'));
			$sql = "INSERT INTO ".  $this->info['table'] ." (".implode(",", $columns).") ";
			$sql .= " SELECT ".implode(",", $columns)." FROM ". $this->info['table']." WHERE ".$this->info['key']." IN (".$toCopy.")";
			\DB::select($sql);
			return ['message'=>__('core.note_success'),'status'=>'success'];
		} else {
			return ['message'=>__('Please select row to copy'),'status'=>'error'];
		}	
		
	}

	function access( $id )
	{
		$row = \DB::table('tb_groups_access')->where('module_id', $id)->where('group_id', session('gid') )->get();
		
		if(count($row) >= 1)
		{
			$row = $row[0];
			if($row->access_data !='')
			{
				$data = json_decode($row->access_data,true);
			} else {
				$data = array();
			}	
			return $data;		
			
		} else {
			return false;
		}			
	
	}	
	function logMaintenance($hash, $userId, $formName, $existing_data='', $updated_data=''){
		$user_agent = $_SERVER["HTTP_USER_AGENT"];
	    $os_platform    =   "Unknown OS Platform";
    	$os_array       =   array(
                            '/windows nt 10/i'     =>  'Windows 10',
                            '/windows nt 6.3/i'     =>  'Windows 8.1',
                            '/windows nt 6.2/i'     =>  'Windows 8',
                            '/windows nt 6.1/i'     =>  'Windows 7',
                            '/windows nt 6.0/i'     =>  'Windows Vista',
                            '/windows nt 5.2/i'     =>  'Windows Server 2003/XP x64',
                            '/windows nt 5.1/i'     =>  'Windows XP',
                            '/windows xp/i'         =>  'Windows XP',
                            '/windows nt 5.0/i'     =>  'Windows 2000',
                            '/windows me/i'         =>  'Windows ME',
                            '/win98/i'              =>  'Windows 98',
                            '/win95/i'              =>  'Windows 95',
                            '/win16/i'              =>  'Windows 3.11',
                            '/macintosh|mac os x/i' =>  'Mac OS X',
                            '/mac_powerpc/i'        =>  'Mac OS 9',
                            '/linux/i'              =>  'Linux',
                            '/ubuntu/i'             =>  'Ubuntu',
                            '/iphone/i'             =>  'iPhone',
                            '/ipod/i'               =>  'iPod',
                            '/ipad/i'               =>  'iPad',
                            '/android/i'            =>  'Android',
                            '/blackberry/i'         =>  'BlackBerry',
                            '/webos/i'              =>  'Mobile'
                        );
		foreach ($os_array as $regex => $value) { 
			if (preg_match($regex, $user_agent)) {
				$os_platform    =   $value;
			}
		}  
		if($hash){
			$hash = explode('/' ,base64_decode($hash))[0];
		}
		// dd($hash);
		UserLastLogin::create(
			[
				'agentString' => request()->server('HTTP_USER_AGENT'),
				'platform' => $os_platform,
				'machineIp' => request()->ip(),
				'userId' => $userId,
				'fkApplicationID' => $userId,
				'createdDtm' => Carbon::now(),
				'userAgent' => $formName,
				'sessionData' => '{"role":"4","roleText":"Student","name":"","AdminUserId":"'.$hash.'"}'


			]
		);
		if($existing_data != '' && $updated_data !=''){
			// dd('existing', $existing_data, 'new', $updated_data);
			$data1 = array(
				'SSC_Title'=> $existing_data->SSC_Title,
				'SSC_From'=> $existing_data->SSC_From,
				'SSC_Subject'=> $existing_data->SSC_Subject,
				'SSC_Year'=> $existing_data->SSC_Year,
				'SSC_Total'=> $existing_data->SSC_Total,
				'SSC_Composite'=> $existing_data->SSC_Composite,
				'SSC_degree_type'=> $existing_data->SSC_degree_type,

				'HSC_Title'=> $existing_data->HSC_Title,
				'HSC_From'=> $existing_data->HSC_From,
				'HSC_Subject'=> $existing_data->HSC_Subject,
				'HSC_Year'=> $existing_data->HSC_Year,
				'HSSC_Total'=> $existing_data->HSSC_Total,
				'HSSC_Composite'=> $existing_data->HSSC_Composite,
				'HSSC_Total1'=> $existing_data->HSSC_Total1,
				'HSSC1'=> $existing_data->HSSC1,
				'HSSC_Total2'=> $existing_data->HSSC_Total2,
				'HSSC2'=> $existing_data->HSSC2,
				'resultAwaiting'=> $existing_data->resultAwaiting,
				'HSC_degree_type'=> $existing_data->HSC_degree_type,

				'llb_total_marks'=> $existing_data->llb_total_marks,
				'llb_obtained_marks'=> $existing_data->llb_obtained_marks,
				'law_completion_date'=> $existing_data->law_completion_date,
				'lat_upcoming'=> $existing_data->lat_upcoming,

				'BA_Bsc_Title'=> $existing_data->BA_Bsc_Title,
				'BA_Bsc_From'=> $existing_data->BA_Bsc_From,
				'BA_Bsc_Subject'=> $existing_data->BA_Bsc_Subject,
				'BA_Bsc_Year'=> $existing_data->BA_Bsc_Year,
				'BA_Bsc_Specialization'=> $existing_data->BA_Bsc_Specialization,
				'BA_Bsc_Total'=> $existing_data->BA_Bsc_Total,
				'BA_Bsc'=> $existing_data->BA_Bsc,
				'resultwaitbsc'=> $existing_data->resultwaitbsc,
				'BA_Bsc_degree_type'=> $existing_data->BA_Bsc_degree_type,
				
				'MSc_Title'=> $existing_data->MSc_Title,
				'MSc_From'=> $existing_data->MSc_From,
				'MSc_Subject'=> $existing_data->MSc_Subject,
				'MSc_Year'=> $existing_data->MSc_Year,
				'MSc_Specialization'=> $existing_data->MSc_Specialization,
				'MSc_Total'=> $existing_data->MSc_Total,
				'MSc'=> $existing_data->MSc,
				'MSc_degree_type'=> $existing_data->MSc_degree_type,

				'BS_Title'=> $existing_data->BS_Title,
				'BS_From'=> $existing_data->BS_From,
				'BS_Subject'=> $existing_data->BS_Subject,
				'BS_Year'=> $existing_data->BS_Year,
				'BS_Specialization'=> $existing_data->BS_Specialization,
				'BS_Total'=> $existing_data->BS_Total,
				'BS'=> $existing_data->BS,
				'BS_Exam_System'=> $existing_data->BS_Exam_System,
				'BS_degree_type'=> $existing_data->BS_degree_type,

				'MS_Title'=> $existing_data->MS_Title,
				'MS_From'=> $existing_data->MS_From,
				'MS_Subject'=> $existing_data->MS_Subject,
				'MS_Year'=> $existing_data->MS_Year,
				'MS_Specialization'=> $existing_data->MS_Specialization,
				'MS_Total'=> $existing_data->MS_Total,
				'MS'=> $existing_data->MS,
				'MS_Exam_System'=> $existing_data->MS_Exam_System,
				'MS_degree_type'=> $existing_data->MS_degree_type,

				'bs_last_result'=> $existing_data->bs_last_result,
				'ms_last_result'=> $existing_data->ms_last_result,
				'phd_research_proposal'=> $existing_data->phd_research_proposal

			);

			$data2 = array(
				'SSC_Title'=> $updated_data->SSC_Title,
				'SSC_From'=> $updated_data->SSC_From,
				'SSC_Subject'=> $updated_data->SSC_Subject,
				'SSC_Year'=> $updated_data->SSC_Year,
				'SSC_Total'=> $updated_data->SSC_Total,
				'SSC_Composite'=> $updated_data->SSC_Composite,
				'SSC_degree_type'=> $updated_data->SSC_degree_type,

				'HSC_Title'=> $updated_data->HSC_Title,
				'HSC_From'=> $updated_data->HSC_From,
				'HSC_Subject'=> $updated_data->HSC_Subject,
				'HSC_Year'=> $updated_data->HSC_Year,
				'HSSC_Total'=> $updated_data->HSSC_Total,
				'HSSC_Composite'=> $updated_data->HSSC_Composite,
				'HSSC_Total1'=> $updated_data->HSSC_Total1,
				'HSSC1'=> $updated_data->HSSC1,
				'HSSC_Total2'=> $updated_data->HSSC_Total2,
				'HSSC2'=> $updated_data->HSSC2,
				'resultAwaiting'=> $updated_data->resultAwaiting,
				'HSC_degree_type'=> $updated_data->HSC_degree_type,

				'llb_total_marks'=> $updated_data->llb_total_marks,
				'llb_obtained_marks'=> $updated_data->llb_obtained_marks,
				'law_completion_date'=> $updated_data->law_completion_date,
				'lat_upcoming'=> $updated_data->lat_upcoming,

				'BA_Bsc_Title'=> $updated_data->BA_Bsc_Title,
				'BA_Bsc_From'=> $updated_data->BA_Bsc_From,
				'BA_Bsc_Subject'=> $updated_data->BA_Bsc_Subject,
				'BA_Bsc_Year'=> $updated_data->BA_Bsc_Year,
				'BA_Bsc_Specialization'=> $updated_data->BA_Bsc_Specialization,
				'BA_Bsc_Total'=> $updated_data->BA_Bsc_Total,
				'BA_Bsc'=> $updated_data->BA_Bsc,
				'resultwaitbsc'=> $updated_data->resultwaitbsc,
				'BA_Bsc_degree_type'=> $updated_data->BA_Bsc_degree_type,
				
				'MSc_Title'=> $updated_data->MSc_Title,
				'MSc_From'=> $updated_data->MSc_From,
				'MSc_Subject'=> $updated_data->MSc_Subject,
				'MSc_Year'=> $updated_data->MSc_Year,
				'MSc_Specialization'=> $updated_data->MSc_Specialization,
				'MSc_Total'=> $updated_data->MSc_Total,
				'MSc'=> $updated_data->MSc,
				'MSc_degree_type'=> $updated_data->MSc_degree_type,

				'BS_Title'=> $updated_data->BS_Title,
				'BS_From'=> $updated_data->BS_From,
				'BS_Subject'=> $updated_data->BS_Subject,
				'BS_Year'=> $updated_data->BS_Year,
				'BS_Specialization'=> $updated_data->BS_Specialization,
				'BS_Total'=> $updated_data->BS_Total,
				'BS'=> $updated_data->BS,
				'BS_Exam_System'=> $updated_data->BS_Exam_System,
				'BS_degree_type'=> $updated_data->BS_degree_type,

				'MS_Title'=> $updated_data->MS_Title,
				'MS_From'=> $updated_data->MS_From,
				'MS_Subject'=> $updated_data->MS_Subject,
				'MS_Year'=> $updated_data->MS_Year,
				'MS_Specialization'=> $updated_data->MS_Specialization,
				'MS_Total'=> $updated_data->MS_Total,
				'MS'=> $updated_data->MS,
				'MS_Exam_System'=> $updated_data->MS_Exam_System,
				'MS_degree_type'=> $updated_data->MS_degree_type,

				'bs_last_result'=> $updated_data->bs_last_result,
				'ms_last_result'=> $updated_data->ms_last_result,
				'phd_research_proposal'=> $updated_data->phd_research_proposal

			);
			UserLogQualification::create(
				[
					'applicant_id' => $updated_data->fkApplicantId,
					'fkSemesterId' => $updated_data->fkSemesterId,
					'modified_by' => $hash,
					'description' => 'Previous Qualification',
					'existing_value' => serialize($data1),
					'updated_value' => serialize($data2)
				]
			);
		}

	}
	function logMaintenanceDocumentVerifyReject($existing_data='', $updated_data=''){
		$data1 = array(
			'fkCurrentStatus' => $existing_data->fkCurrentStatus,
			'doumentsVerified' => $existing_data->doumentsVerified,
			'comments' => $existing_data->comments,
			'fkReviewedBy' => $existing_data->fkReviewedBy
		);
		$data2 = array(
			'fkCurrentStatus' => $updated_data->fkCurrentStatus,
			'doumentsVerified' => $updated_data->doumentsVerified,
			'comments' => $updated_data->comments,
			'fkReviewedBy' => $updated_data->fkReviewedBy
		);

		UserLogQualification::create(
			[
				'applicant_id' => $updated_data->fkApplicantId,
				'application_id' => $updated_data->id,
				'fkSemesterId' => $updated_data->fkSemesterId,
				'modified_by' => auth()->guard('web')->id(),
				'description' => 'Document Verify',
				'existing_value' => serialize($data1),
				'updated_value' => serialize($data2)
			]
		);
	}	

}

