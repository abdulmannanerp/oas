<?php

namespace App\Http\Controllers\ApplicantAuth;

use App\Model\Semester;
use App\Model\Applicant;
use App\Model\Application;
use Illuminate\Http\Request;
use App\Model\ApplicationStatus;
use App\Http\Controllers\Controller;
use App\Model\PreviousQualification;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class SchLoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;
    protected $guard = 'applicant';

    protected function guard()
    {
        return Auth::guard('applicant');
    }

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }

    public function schapplicantLogin(Request $request)
    {
        $this->validate($request, [
            'email'   => 'required',
            'password' => 'required|min:6'
        ]);
        $find1 = strpos($request->email, '@');
        if($find1 !== false){
           $attampt = 'email';
        }else{
            $attampt = 'cnic';
        }
        if (Auth::guard('applicant')->attempt([$attampt => $request->email, 'password' => $request->password])) {
            $applicant = Applicant::where($attampt, $request->email)->first();
            if($applicant->status == 0){
                return redirect('/')->withInput($request->only('email'))->with('status','Account Suspended. Please contact Admission Office');
            }
            // Dev Mannan: In case of login insert record for new semester
            $semester = Semester::where('status', 1)->first();
            $applicationStatus = ApplicationStatus::where('fkApplicantId', $applicant->userId)->where('fkSemesterId', $semester->pkSemesterId)->first();
            if($applicationStatus == null){
                Application::create(
                    [
                        'fkApplicantId' => $applicant->userId,
                        'fkCurrentStatus' => 1,
                        'fkSemesterId' => $semester->pkSemesterId
                    ]
                );
                ApplicationStatus::create(
                    [
                        'fkApplicantId' => $applicant->userId,
                        'fkSemesterId' => $semester->pkSemesterId
                    ]
                );
                PreviousQualification::create(
                    [
                        'fkApplicantId' => $applicant->userId,
                        'fkSemesterId' => $semester->pkSemesterId
                    ]
                );
            }
            return redirect()->intended('/frontend/progressDetail');
        }
        return redirect('/scholarshipdetail')->withInput($request->only('email'))->with('status','Login Credentials are invalid');
    }

    public function applicantLogout()
    {
        Auth::guard('applicant')->logout();
        // request->session()->invalidate();
        \Session::flush();
        return redirect('/');
    }

    public function adminAuth($hash)
    {
        
        $user = Applicant::find(explode('/' ,base64_decode($hash))[1]);
        Auth::guard('applicant')->login($user);
        return redirect()->intended('/frontend/progressDetail/'.$hash);
    }
}
