<?php

namespace App\Http\Controllers;

use App\Model\ProgramFeeOld;

class FeeViewOldController extends Controller
{
    public function index()
    {
		$FeeView = ProgramFeeOld::where('status',1)->orderBy('fkFacId', 'ASC')->orderBy('fkDepId','ASC')->get();
		$sql1 = "SELECT title from tbl_oas_semester where status=1";
    	return view('results.feestructureold', compact('FeeView'));
	}
	public function programes(){
		$semester = Semester::where('status', 1)->first();
		$programlisting = Programme::where('status', 1)->whereHas('department', function ($query) {
            $query->where('status', 1);
        })->orderBy('fkFacId', 'ASC')->orderBy('fkDepId', 'ASC')->orderBy('title', 'DESC')->get();
		return view('results.programs', compact('programlisting', 'semester'));
	}
		  
}
