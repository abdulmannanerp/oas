<?php

namespace App\Http\Controllers;
use App\Model\Listscholarship;

use Illuminate\Http\Request;

class ScholarshipViewController extends Controller
{
    public function index()
    {
        $status = request()->session()->get('status');
        $status_ok = request()->session()->get('status_ok');
        $SchView = Listscholarship::where('status',1)->orderBy('id', 'ASC')->get();
        return view('results.scholarshipdetail', compact('SchView','status','status_ok'));
    }
}
