<?php

namespace App\Http\Controllers;

use App\Model\Account;
use App\Model\Faculty;

class AccountController extends Controller
{
    public function index()
    {
		$permissions = $this->getCurrentUserPermissions();
		if($permissions->fkUserId == 1 || $permissions->fkUserId == 38 || $permissions->fkUserId == 55){
		}else{
			return 'You donot have permission to access this page';
			exit;
		}
		if($permissions->fkUserId == 38){
			$var1= 'Male';
			$var2= 'Overseas-Male';
		}
		if($permissions->fkUserId == 55){
			$var1= 'Female';
			$var2= 'Overseas-Female';
		}
		if($permissions->fkUserId == 1){
			$result = Account::orderBy('fkFacId')->get();
		}else{
			$result = Account::whereIn('bank_type', [$var1, $var2])->orderBy('fkFacId')->get();
		}
		// dd($result);
    	return view('account.account', compact('result'));
	}
	public function editAccount($id =''){
			$status = request()->session()->get('status');
			if ( request()->method() == 'POST' ) {
				$this->validate(request(),[
					'admission_account' => 'required',
					'security_account' => 'required',
					'dues_account' => 'required',
				]);
					Account::where('id', request('id'))->update(
						[
							'admission_account' => request('admission_account'),
							'security_account' => request('security_account'),
							'dues_account' => request('dues_account'),
							'status' => request('status'),
							'updated_by' => auth()->id()
						]
					);
				return back()->with('status', 'Information updated successfully!');
			}
			if ( $id ) {
				$account = Account::find($id);
				$faculty = Faculty::select('pkFacId', 'title')->get();
				return view ('account.edit', compact('account', 'faculty', 'status'));    
			}
			return redirect()->route('faculty-accounts');
	}

	  
}
