<?php

namespace App\Http\Controllers;

use Validator;
use Carbon\Carbon;
use App\Traits\PQM;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Model\{Logger, ApplicantDetail, Permission, UserLastLogin, ScholarshipDetail, Listscholarship, ScholarshipInformation, Applicant, ScholarshipFamilyDetail,Nationality,ApplicantAddress,City,Country,District,Province,Application,Faculty,Scholarship};

class SearchScholarshipsController extends Controller
{
    protected $currentUserPermissions;
    protected $facultyPermissions;
    protected $departmentPermissions;
    protected $genderPermissions;
    protected $nationalityPermissions;
    protected $selectedSemester;

    public function __construct()
    {
        // $this->selectedSemester = $this->getCurrentSemester()->pkSemesterId;
    }
    public static function getFacultyTitle($id){

        $f= Faculty::where('pkFacId','=',$id)->first();
        if(!$f){
            return 'N/A '.$id;
        }
        return $f->title;
    }
    public static function getScholorshipTitle($id){

        $f= Scholarship::where('id','=',$id)->first();
        if(!$f){
            return 'N/A '.$id;
        }
        return $f->name;
    }

    public function index(Request $request)
    {
        $this->assignPermissions();
        $listscholarship = Listscholarship::get();
        $sid=$request->sid;
        if(!$sid){
            $sid=0;
        }
        $Applicant = Applicant::whereIn('userId',ScholarshipDetail::where('fkSchlrId',$sid)->pluck('fkApplicantId')->toArray())->with(['ApplicantDetail', 'ScholarshipInformation', 'ScholarshipFamilyDetail', 'Application','ScholarshipDetail'])->get();
      
        return view('scholarship.scholarships', compact('Applicant','listscholarship'));
    }

    
    public function assignPermissions()
    {
        $this->currentUserPermissions = $this->getCurrentUserPermissions();
        $this->facultyPermissions = explode(',', $this->currentUserPermissions->fkFacultyId);
        $this->departmentPermissions = explode(',',$this->currentUserPermissions->fkDepartmentId);
        $this->genderPermissions = $this->currentUserPermissions->fkGenderId;
        $this->nationalityPermissions = explode(',',$this->currentUserPermissions->fkNationality);
    }

    public function checkIfHasFullAccess() {
        if 
        (  count ( $this->facultyPermissions ) == 9 && 
            $this->departmentPermissions[0] == '' && 
            $this->genderPermissions == 1 && 
            (count( $this->nationalityPermissions) == 3 || 
            $this->nationalityPermissions[0] == '' ||  $this->nationalityPermissions[0] == 'NULL') 
        )
            return true;
        return false;
    }

    public function checkIfHasLimitedAccess( $applications )
    {
        $filtered = $applications->filter(function ( $value, $key ) {
            return $this->filterApplication ( $value );
        });
        return $filtered;
    }

    public function filterApplication($application)
    {
        /* Apply gender & overseas permissions to incomplete applications */
        $gender = $application->applicant->fkGenderId;            
        $nationality = $application->applicant->fkNationality;
        if ( $application->fkCurrentStatus == 1 ) {
            if ( 
                ($this->genderPermissions == 1 || $gender == $this->genderPermissions) &&
                (empty( $this->nationalityPermissions[0] ) || in_array( $nationality, $this->nationalityPermissions ) || $this->nationalityPermissions[0] == 'NULL')
            )
                return true;
        }
        // $program = Programme::where('pkProgId', $application->fkProgramId)->select('fkFacId', 'fkDepId')->first();
        $facultyId = $application->program->fkFacId ?? '';
        $departmentId = $application->program->fkDepId ?? '';    
       
        if ( in_array($facultyId, $this->facultyPermissions) ) {
            if ( empty($this->departmentPermissions[0]) || in_array ( $departmentId, $this->departmentPermissions) ) {
                if ( $this->genderPermissions == 1 || $gender == $this->genderPermissions ) {
                    if ( empty( $this->nationalityPermissions[0] ) || in_array( $nationality, $this->nationalityPermissions ) || $this->nationalityPermissions[0] == 'NULL' )
                        return true;
                }
            }
        }
        return false;
    }

    public function checkAccessLevelAndGetApplication( $applications ) 
    {
        return ( $this->checkIfHasFullAccess() ) ? $applications : $this->checkIfHasLimitedAccess($applications);   
    }

    public function paginateResults($applications, $paginate)
    {
        $currentPage = LengthAwarePaginator::resolveCurrentPage();
        $currentPageResults = $applications->slice(($currentPage-1) * $paginate, $paginate)->all();
        $result = new LengthAwarePaginator($currentPageResults, count($applications), $paginate);
        $result->setPath(request()->url());
        return $result;
    }


    public function superadmin(Request $request, Application $application)
    {
        $application = Application::where('id', $request->id)->first();
        $logs = UserLastLogin::where('userId', $application->fkApplicantId)->get();
        // dump($logs);
        $fee_verify = 'NULL';
        $docverify = 'NULL';
        if ($application->docsVerifyAdmin) {
            $docverify = $application->docsVerifyAdmin->username;
        }
        if ($application->user) {
            $fee_verify = $application->user->username;
        }
        return response()->json(['application' => $application, 'fee_verified_by' => $fee_verify, 'documents_verified_by' => $docverify, 'logs'=>$logs]);
    }

    public function superadminid(Request $request){
        if($request->id){
            $users = \DB::table('tb_users')->where('id','=',$request->id)->get()->first();
            return response()->json(['username'=>$users->username]);
        }
    }
}

