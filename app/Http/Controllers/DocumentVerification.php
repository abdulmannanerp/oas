<?php

namespace App\Http\Controllers;

use App\User;
use Carbon\Carbon;
use App\Jobs\SendSMS;
use SplTempFileObject;
use League\Csv\Writer;
use App\Jobs\SendEmail;
use App\Traits\Utility;
use App\Traits\PQM;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;
use App\Model\Applicant;
use App\Model\Programme;
use App\Model\Department;
use App\Model\Faculty;
use App\Model\Permission;
use App\Model\RollNumber;
use App\Model\Application;
use App\Model\oasEmail;
use App\Model\PreviousQualification;

class DocumentVerification extends Controller
{
    use Utility;
    use PQM;

    protected $currentSemester;

    public function __construct()
    {
        $this->currentSemester = $this->getCurrentSemester()->pkSemesterId;
    }

    public function docs()
    {
        $faculties = $this->getAllFaculties();
        $applications = $selectedFaculty = $departments = $selectedDepartment = $programs = $selectedProgram = $selectedStatus = '';
        if (request('search')) {
            $selectedFaculty = request('faculty');
            $departments  = Department::where('fkFacId', $selectedFaculty)->where('status', 1)->get();
            $selectedDepartment = request('department');
            $programs = Programme::with(['programscheduler'])
                ->where('fkDepId', $selectedDepartment)
                ->whereHas('programscheduler', function ($query) {
                    $query->where('status', 1);
                })
                ->get();
            $selectedProgram = request('program');
            $selectedStatus = request('status');
            $applications = $this->getSearchResults(request('faculty'), request('department'), request('program'), request('status'), request('export'));
            if (request('export')) {
                $this->exportResults($applications);
            }
        }
        return view('docs.documentVerification', compact('faculties', 'applications', 'selectedFaculty', 'departments', 'selectedDepartment', 'programs', 'selectedProgram', 'selectedStatus'));
    }

    public function getSearchResults($faculty, $department=null, $program=null, $status, $export=null)
    {
        $operator = ($status == 'verified') ? '>=' : '<';
        $status = 5;

        if ($faculty && $department && $program) {
            return $this->getApplicationsByColumn('fkProgramId', $program, $operator, $status, $export);
        }

        if ($faculty && $department) {
            return $this->getApplicationsByColumn('fkDepId', $department, $operator, $status, $export);
        }

        if ($faculty) {
            return $this->getApplicationsByColumn('fkFacId', $faculty, $operator, $status, $export);
        }
    }

    public function getApplicationsByColumn($columnName, $columnValue, $operator, $status, $export)
    {
        $permissions = $this->splitPermissionsToArray($this->getCurrentUserPermissions()->fkGenderId);

        $getNationalityPermissions = $this->getCurrentUserPermissions()->fkNationality;
        if (strpos($getNationalityPermissions, ',') !== false) {
            $getNationalityPermissions = explode(',', $getNationalityPermissions);
        } else {
            $getNationalityPermissions = array($getNationalityPermissions);
        }
        $eagerLoadColumns = ['applicant'];
        if ($export) {
            $eagerLoadColumns = ['applicant', 'program', 'program.programscheduler', 'applicant.previousQualification', 'applicant.detail', 'applicant.detail.district', 'getRollNumber'];
        }
        return Application::with($eagerLoadColumns)
            ->whereHas('applicant', function ($query) use ($permissions, $getNationalityPermissions) {
                $query->whereIn('fkGenderId', $permissions)->whereIn('fkNationality', $getNationalityPermissions);
            })
            ->where($columnName, $columnValue)
            ->where('fkCurrentStatus', $operator, $status)
            ->where('fkSemesterId', $this->currentSemester)
            ->where('a_status', '>=', 1)
            ->orderBy('fkCurrentStatus', 'desc')
            ->tap(function ($query) use ($export) {
                return $export ? $query->get() : $query->paginate(30);
            });
    }

    public function splitPermissionsToArray($permissions)
    {
        if (strpos($permissions, ',')) {
            return explode(',', $permissions);
        } elseif ($permissions == 1) {
            return [2,3];
        } else {
            return (array)$permissions;
        }
    }

    public function ajaxDocsVerify()
    {
        $id = request('id');
        $comments = request('comments');
        $type = request('type');
        if ($id) {
            $application = Application::with(['program','faculty','program.programscheduler','applicant','applicant.previousQualification'])->find($id);
            $this->generateAndStorePQM($application);
            $this->generateRollNumber($application, $type);
            $this->updateDocumentVerificationStatus($application, $type, $comments);
            $this->sendSMSOnDocumentVerification($application, $type, $comments);
            $this->sendEmail($application, $type, $comments);
            $response = new \stdClass;
            $response->id = $id;
            $response->message = ($type == 'Approve') ? 'Approved' : 'Rejected';
            return json_encode($response);
        }
    }

    public function sendEmail($application, $type, $comments)
    {
        $data = [
            'name' => $application->applicant->name,
            'program_name' => $application->program->title,
            'comment' => $comments
        ];
        if ($type == 'Approve') {
            SendEmail::dispatch(
                $application->faculty->abbrev,
                $data,
                $application->applicant->email,
                'Documents Verified',
                'applications.verify'
            );
            return;
        }

        SendEmail::dispatch(
            $application->faculty->abbrev,
            $data,
            $application->applicant->email,
            'Documents Rejected',
            'applications.reject'
        );
    }

    public function sendSMSOnDocumentVerification($application, $type, $comments)
    {
        $phone = $application->applicant->detail->mobile;
        // $message = urlencode('Your Roll No Slip can be found through an online admission account at http://admission.iiu.edu.pk. Please note that there is no admission test for undergraduate level (BS/LLB) except Faculty of Computing and Faculty of Engineering &amp; Technology.');
        $message = urlencode('Your application has been successfully submitted. please keep visiting IIU website https://www.iiu.edu.pk/ for merit list after closing date of admission.');

        if ($type == 'Reject') {
            $programTitle = $application->program->title;
            $message = urlencode('You application for '.$programTitle.' has been rejected due to following reason. '.$comments.'In case of any query, Please contact admission section.');
        }
        try {
            SendSMS::dispatch($message, $phone);
        } catch (Exception $e) {
            Log::info('Unable to send document verification message to ' .$application->id .' '. $e);
        }
    }

    public function updateDocumentVerificationStatus($application, $type, $comments)
    {
        $applicationStatus = 5;
        $documentReviewedBy = auth()->guard('web')->id();
        if ($type == 'Reject') {
            $applicationStatus = 6;
        }
        
        $application->fkCurrentStatus = $applicationStatus;
        $application->doumentsVerified = date("Y-m-d H:i:s");
        $application->comments = $comments;
        $application->fkReviewedBy = auth()->guard('web')->id();
        // Dev Mannan: Code for error
        $previous_data = Application::where('id', $application->id)->first();
        $application->save();
        // Dev Mannan: Code for error
        $updated_data = Application::where('id', $application->id)->first();
        $this->logMaintenanceDocumentVerifyReject($previous_data, $updated_data);
    }

    public function generateRollNumber($application, $type)
    {
        $rollNumber = RollNumber::where('fkApplicationId', $application->id)->first();
        $rollNumberStatus = ($type == 'Approve') ? $rollNumberStatus = 1 : $rollNumberStatus = 0;
        if ($rollNumber) {
            $rollNumber->status = $rollNumberStatus;
        }

        if (!$rollNumber) {
            $rollNumber = new RollNumber;
            $rollNumber->fkApplicationId = $application->id;
            $rollNumber->fkApplicantId = $application->applicant->userId;
            $rollNumber->status = $rollNumberStatus;
            $rollNumber->fkSemesterId = $this->currentSemester;
        }

        $rollNumber->save();
    }

    // public function generateAndStorePQM($application)
    // {
    //     $pqm = $this->calculatePqm(
    //         $application->id,
    //         // $application->faculty->title,
    //         // $application->applicant->previousQualification->HSC_Title,
    //         // $application->applicant->previousQualification->resultAwaiting,
    //         // $application->program->title,
    //         // $application->applicant->previousQualification->llb_obtained_marks,
    //         // $application->applicant->previousQualification->llb_total_marks,
    //         $application->program->fkLeveliId,
    //         $application->program->programscheduler->interviewDateTime,
    //         $application->program->fkReqId,
    //         $application->applicant->previousQualification->SSC_Total,
    //         $application->applicant->previousQualification->SSC_Composite,
    //         $application->applicant->previousQualification->HSSC_Total,
    //         $application->applicant->previousQualification->HSSC_Composite,
    //         $application->applicant->previousQualification->HSSC_Total1,
    //         $application->applicant->previousQualification->HSSC_Total2,
    //         $application->applicant->previousQualification->HSSC1,
    //         $application->applicant->previousQualification->HSSC2,
    //         $application->applicant->previousQualification->BA_Bsc_Total,
    //         $application->applicant->previousQualification->BA_Bsc,
    //         $application->applicant->previousQualification->MSc_Total,
    //         $application->applicant->previousQualification->MSc,
    //         $application->applicant->previousQualification->BS_Total,
    //         $application->applicant->previousQualification->BS,
    //         $application->applicant->previousQualification->MS_Total,
    //         $application->applicant->previousQualification->MS
    //     );
    //     $application->pqm = $pqm;
    //     $application->save();
    //     // PreviousQualification::where('fkApplicantId', $application->fkApplicantId)
    //     //     ->where('fkSemesterId', $this->currentSemester)
    //     //     ->update([ 'pqm' => $pqm ]);
    // }

    public function exportResults($applications)
    {
        $csv = Writer::createFromFileObject(new SplTempFileObject());
        $csv->insertOne(
            [
                'S.No',
                'Form No',
                'Roll No',
                'Name',
                'Father Name',
                'Program Name',
                'Mobile No',
                'Email',
                'District',
                //ssc
                'Degree Title',
                'Subjects',
                'Board',
                'Total Marks',
                'Obtained Marks',
                // hssc
                'Degree Title',
                'Subjects',
                'Board',
                'Total Marks Composite',
                'Obtained Marks Composite',
                'Total Marks Part 1',
                'Obtained Marks Part 1',
                'Total Marks Part 2',
                'Obtained Marks Part 2',
                //ba_bsc
                'Degree Title',
                'Subjects',
                'Specialization',
                'University',
                'Total Marks / CGPA',
                'Obtained Marks / GPA',
                //msc
                'Degree Title',
                'Subjects',
                'Specialization',
                'University',
                'Total Marks / CGPA',
                'Obtained Marks / GPA',
                //bs
                'Degree Title',
                'Subjects',
                'Specialization',
                'University',
                'Exam System',
                'Total Marks / CGPA',
                'Obtained Marks / GPA',
                //ms
                'Degree Title',
                'Subjects',
                'Specialization',
                'University',
                'Exam System',
                'Total CGPA',
                'Obtained GPA',
                'Result Awaiting',
                'Fee Verified',
                'Documents Verified',
                'Documents Rejected',
                'PQM'
            ]
        );
        $serialNumber = 1;
        foreach ($applications as $application) {
            $feeVerifiedStatus      = ($application->fkCurrentStatus >= 4)   ? 'Yes' : 'No';
            $documentVerifiedStatus = ($application->fkCurrentStatus >= 5) ? 'Yes' : 'No';
            $documentRejectedStatus = ($application->fkCurrentStatus == 6) ? 'Yes' : 'No';

            $csv->insertOne(
                [
                    $serialNumber ?? '',
                    $application->id ?? '',
                    $application->getRollNumber->id ?? '',
                    $application->applicant->name ?? '',
                    $application->applicant->detail->fatherName ?? '',
                    $application->program->title ?? '',
                    $application->applicant->detail->mobile ?? '',
                    $application->applicant->email ?? '',
                    $application->applicant->detail->district->distName ?? '', //to be added

                    //ssc
                    $application->applicant->previousQualification->SSC_Title ?? '', //to be added
                    $application->applicant->previousQualification->SSC_Subject ?? '',
                    $application->applicant->previousQualification->SSC_From ?? '',
                    $sscTotal ?? '',
                    $sscObtained ?? '',

                    //hssc
                    $application->applicant->previousQualification->HSC_Title ?? '',
                    $application->applicant->previousQualification->HSC_Subject ?? '',
                    $application->applicant->previousQualification->HSC_From ?? '',
                    $hsscTotal ?? '',
                    $hsscObtained ?? '',
                    $hsscTotalPart1 ?? '',
                    $hsscObtainedPart1 ?? '',
                    $hsscTotalPart2 ?? '',
                    $hsscObtainedPart2 ?? '',

                    //ba_bsc
                    $application->applicant->previousQualification->BA_Bsc_Title ?? '',
                    $application->applicant->previousQualification->BA_Bsc_Subject ?? '',
                    $application->applicant->previousQualification->BA_Bsc_Specialization ?? '',
                    $application->applicant->previousQualification->BA_Bsc_From ?? '',
                    $baBscTotal ?? '',
                    $baBscObtained ?? '',
                    // $baBscTotal = $application->applicant->previousQualification->BA_Bsc_Total;
                    // $baBscObtained = $application->applicant->previousQualification->BA_Bsc;

                    //msc
                    $application->applicant->previousQualification->MSc_Title ?? '',
                    $application->applicant->previousQualification->MSc_Subject ?? '',
                    $application->applicant->previousQualification->MSc_Specialization ?? '',
                    $application->applicant->previousQualification->MSc_From ?? '',
                    $application->applicant->previousQualification->MSc_Total ?? '',
                    $application->applicant->previousQualification->MSc ?? '',

                    //bs
                    $application->applicant->previousQualification->BS_Title ?? '',
                    $application->applicant->previousQualification->BS_Subject ?? '',
                    $application->applicant->previousQualification->BS_Specialization ?? '',
                    $application->applicant->previousQualification->BS_From ?? '',
                    $application->applicant->previousQualification->BS_Exam_System ?? '',
                    $application->applicant->previousQualification->BS_Total ?? '',
                    $application->applicant->previousQualification->BS ?? '',

                    //ms
                    $application->applicant->previousQualification->MS_Title ?? '',
                    $application->applicant->previousQualification->MS_Subject ?? '',
                    $application->applicant->previousQualification->MS_Specialization ?? '',
                    $application->applicant->previousQualification->MS_From ?? '',
                    $application->applicant->previousQualification->MS_Exam_System ?? '',
                    $application->applicant->previousQualification->MS_Total ?? '',
                    $application->applicant->previousQualification->MS ?? '',

                    ($application->applicant->previousQualification->resultAwaiting == 1) ? 'Yes' : 'No',
                    $feeVerifiedStatus ?? '',
                    $documentVerifiedStatus ?? '',
                    $documentRejectedStatus ?? '',
                    $application->applicant->previousQualification->pqm ?? '',
                ]
            );
            $serialNumber++;
        }
        $currentUser = User::find(auth()->guard('web')->id());
        $currentDate = date('d-m-Y h:i:s A');
        $message = "Report generated by $currentUser->first_name $currentUser->last_name on $currentDate via Online Admission System";
        $csv->insertOne([$message]);
        $csv->output('document-verification.csv');
        die();
    }

    public function generateStats()
    {
        $department = request('department');
        $program = request('program');
        $departmentStats = new \stdClass;
        $programStats = new \stdClass;
        $departmentStats->name = Department::select('title')->where('pkDeptId', $department)->first()->title;
        $departmentStats->male = $this->getStats('fkDepId', $department, 3, '>=', '1');
        $departmentStats->female = $this->getStats('fkDepId', $department, 2, '>=', '1');
        $departmentStats->feePendingMale = $this->getStats('fkDepId', $department, 3, '=', '3');
        $departmentStats->feePendingFemale = $this->getStats('fkDepId', $department, 2, '=', '3');
        $departmentStats->feeVerifiedMale = $this->getStats('fkDepId', $department, 3, '>=', '4');
        $departmentStats->feeVerifiedFemale = $this->getStats('fkDepId', $department, 2, '>=', '4');
        $departmentStats->documentsVerifiedMale = $this->getStats('fkDepId', $department, 3, '=', '5');
        $departmentStats->documentsVerifiedFemale = $this->getStats('fkDepId', $department, 2, '=', '5');
        $departmentStats->documentsRejectedMale = $this->getStats('fkDepId', $department, 3, '=', '6');
        $departmentStats->documentsRejectedFemale = $this->getStats('fkDepId', $department, 2, '=', '6');

        if ($program) {
            $programStats->name = Programme::select('title')->where('pkProgId', $program)->first()->title;
            $programStats->male = $this->getStats('fkProgramId', $program, 3, '>=', '1');
            $programStats->female = $this->getStats('fkProgramId', $program, 2, '>=', '1');
            $programStats->feePendingMale = $this->getStats('fkProgramId', $program, 3, '=', '3');
            $programStats->feePendingFemale = $this->getStats('fkProgramId', $program, 2, '=', '3');
            $programStats->feeVerifiedMale = $this->getStats('fkProgramId', $program, 3, '>=', '4');
            $programStats->feeVerifiedFemale = $this->getStats('fkProgramId', $program, 2, '>=', '4');
            $programStats->documentsVerifiedMale = $this->getStats('fkProgramId', $program, 3, '=', '5');
            $programStats->documentsVerifiedFemale = $this->getStats('fkProgramId', $program, 2, '=', '5');
            $programStats->documentsRejectedMale = $this->getStats('fkProgramId', $program, 3, '=', '6');
            $programStats->documentsRejectedFemale = $this->getStats('fkProgramId', $program, 2, '=', '6');
        }

        $stats = new \stdClass;
        $stats->department = $departmentStats;
        $stats->program = $programStats;
        return json_encode($stats);
    }

    public function getStats($columnName, $columnValue, $gender, $statusOperator, $status)
    {
        //add semester
        return Application::with(['applicant'])
            ->where($columnName, $columnValue)
            ->whereHas('applicant', function ($query) use ($gender) {
                $query->where('fkGenderId', $gender);
            })->where('fkCurrentStatus', $statusOperator, $status)->where('fkSemesterId', $this->currentSemester)->count();
    }

    

    public function index()
    {
        $selectedFilters = [
            'facultyId' => request('faculty'),
            'facultyTitle' => Faculty::find(request('faculty'))->title ?? '',
            'departmentId' => request('department') ?? '',
            'departmentTitle' => Department::find(request('department'))->title ?? '',
            'programmeId' => request('programme') ?? '',
            'programmeTitle' => Programme::find(request('programme'))->title ?? ''
        ];

        $selectedDepartments = '';
        $selectedPorgrams = '';
        if ($selectedFilters['facultyId'] != '') {
            $facultyId = $selectedFilters['facultyId'];
            $selectedDepartments = $this->getDepartments($facultyId);
        }
        if ($selectedFilters['departmentId'] != '') {
            $departmentId = $selectedFilters['departmentId'];
            $selectedPorgrams = $this->getProgramme($departmentId);
        }

        $verificationStatus = request('verification-status');
        $status = 5;
        ($verificationStatus == 'verified') ? $operator = '>=' : $operator = '<';
        if (request()->route()->getName() == 'export-docs-verification-data') {
            ($verificationStatus == 'verified') ? $operator = '=' : $operator = '<';
        }
        $userHasPermissions = $this->getPermissionsOfCurrentUser();
        $allowedGenderAccess = $userHasPermissions->fkGenderId;
        // Dev Mannan: Code for overseas permission Starts
        if (empty($userHasPermissions->fkNationality) || $userHasPermissions->fkNationality == 'NULL' || $userHasPermissions->fkNationality == null) {
            $overceasAccess = 'NULL';
        } else {
            $overceasAccess = explode(',', $userHasPermissions->fkNationality);
        }
        // Dev Mannan: Code for overseas permission Ends

        if ($userHasPermissions->fkFacultyId == '') {
            $faculties = Faculty::all();
        } else {
            $allowdFacultiesToCurrentUser = explode(',', $userHasPermissions->fkFacultyId);
            $faculties = Faculty::whereIn('pkFacId', $allowdFacultiesToCurrentUser)->get();
        }

        if (request('programme') != '') {
            $programId = request('programme');
            $departmentId = request('department');
            $department = Department::find($departmentId);
            if ($allowedGenderAccess != '1') {
                if ($overceasAccess != 'NULL') {
                    $gender = Applicant::where('fkGenderId', $allowedGenderAccess)->whereIn('fkNationality', $overceasAccess)->get();
                    if ($gender) {
                        $allowed_gender = [];
                        foreach ($gender as $gn) {
                            array_push($allowed_gender, $gn->userId);
                        }
                    }
                    // Gender permission with overseas access => done
                    $applications = Application::with(['applicant', 'program', 'program.programscheduler', 'applicant.previousQualification', 'applicant.detail', 'applicant.detail.district', 'getRollNumber'])->whereHas('applicant', function ($query) use ($allowedGenderAccess) {
                        $query->where('fkGenderId', $allowedGenderAccess);
                    })->where('fkProgramId', $programId)->whereIn('fkApplicantId', $allowed_gender)->where('fkCurrentStatus', '>', 1)->where('fkCurrentStatus', $operator, $status)->orderBy('feeVerified', 'desc')->get();
                } else {
                    //Gender permission with no overseas access => done
                    $applications = Application::with(['applicant', 'program', 'program.programscheduler', 'applicant.previousQualification', 'applicant.detail', 'applicant.detail.district', 'getRollNumber'])->whereHas('applicant', function ($query) use ($allowedGenderAccess) {
                        $query->where('fkGenderId', $allowedGenderAccess);
                    })->where('fkProgramId', $programId)->where('fkCurrentStatus', '>', 1)->where('fkCurrentStatus', $operator, $status)->orderBy('feeVerified', 'desc')->get();
                }
            } else {
                //Has access to both male and female => done
                $applications = Application::with(['applicant', 'program', 'program.programscheduler', 'applicant.previousQualification', 'applicant.detail', 'applicant.detail.district', 'getRollNumber'])->where('fkProgramId', $programId)->where('fkCurrentStatus', '>', 1)->where('fkCurrentStatus', $operator, $status)->orderBy('feeVerified', 'desc')->get();
            }
            if (request()->route()->getName() == 'export-docs-verification-data') {
                return $this->exportRecordsToCSV($applications, $selectedFilters);
            }
            // Dev Mannan: Stats code starts
            $male = 3;
            $female = 2;
            $bothMaleAndFemale = 1;
            $nationality = 1;
            $nationalityOperator = '>=';
            $applicationIdsInDepartment = $department->applications->pluck('id');
            if ($allowedGenderAccess == $bothMaleAndFemale || $allowedGenderAccess == $male) :
                $departmentApplicationsWithMaleApplicants = Application::with(['applicant'])->whereIn('id', $applicationIdsInDepartment)->whereHas('applicant', function ($query) use ($nationalityOperator, $nationality) {
                    $query->where('fkGenderId', 3)->where('fkNationality', $nationalityOperator, $nationality);
                })->get()->count();
            $applicant['departmentfeePendingMale'] = Application::whereIn('id', $applicationIdsInDepartment)->getStatusByGender('<', '4', '3', $nationalityOperator, $nationality)->where('fkCurrentStatus', '=', 3)->count();
            $applicant['departmentFeeConfirmedMale'] = Application::whereIn('id', $applicationIdsInDepartment)->getStatusByGender('>=', '4', '3', $nationalityOperator, $nationality)->count();
            $applicant['departmentDocumentsVerifiedMale'] = Application::whereIn('id', $applicationIdsInDepartment)->getStatusByGender('=', '5', '3', $nationalityOperator, $nationality)->count();
            $applicant['departmentRejectedMale'] = Application::whereIn('id', $applicationIdsInDepartment)->getStatusByGender('=', '6', '3', $nationalityOperator, $nationality)->count();

            $programApplicationsWithMaleApplicants = Application::with(['applicant'])->where('fkProgramId', $programId)->whereHas('applicant', function ($query) use ($nationalityOperator, $nationality) {
                $query->where('fkGenderId', 3)->where('fkNationality', $nationalityOperator, $nationality);
            })->get()->count();
            $applicant['feePendingMale'] = Application::where('fkProgramId', $programId)->getStatusByGender('<', '4', '3', $nationalityOperator, $nationality)->where('fkCurrentStatus', '=', 3)->count();
            $applicant['feeConfirmedMale'] = Application::where('fkProgramId', $programId)->getStatusByGender('>=', '4', '3', $nationalityOperator, $nationality)->count();
            $applicant['documentsVerifiedMale'] = Application::where('fkProgramId', $programId)->getStatusByGender('>=', '5', '3', $nationalityOperator, $nationality)->count();
            $applicant['rejectedMale'] = Application::where('fkProgramId', $programId)->getStatusByGender('=', '6', '3', $nationalityOperator, $nationality)->count(); else :
                $departmentApplicationsWithMaleApplicants = 'N/A';
            $applicant['departmentfeePendingMale'] = 'N/A';
            $applicant['departmentFeeConfirmedMale'] = 'N/A';
            $applicant['departmentDocumentsVerifiedMale'] = 'N/A';
            $applicant['departmentRejectedMale'] = 'N/A';

            $programApplicationsWithMaleApplicants = 'N/A';
            $applicant['feePendingMale'] = 'N/A';
            $applicant['feeConfirmedMale'] = 'N/A';
            $applicant['documentsVerifiedMale'] = 'N/A';
            $applicant['rejectedMale'] = 'N/A';
            endif;

            if ($allowedGenderAccess == $bothMaleAndFemale || $allowedGenderAccess == $female) :
                $departmentApplicationsWithFemaleApplicants = Application::with(['applicant'])->whereIn('id', $applicationIdsInDepartment)->whereHas('applicant', function ($query) use ($nationalityOperator, $nationality) {
                    $query->where('fkGenderId', 2)->where('fkNationality', $nationalityOperator, $nationality);
                })->get()->count();
            $applicant['departmentfeePendingFemale'] = Application::whereIn('id', $applicationIdsInDepartment)->getStatusByGender('<', '4', '2', $nationalityOperator, $nationality)->where('fkCurrentStatus', '=', 3)->count();
            $applicant['departmentFeeConfirmedFemale'] = Application::whereIn('id', $applicationIdsInDepartment)->getStatusByGender('>=', '4', '2', $nationalityOperator, $nationality)->count();
            $applicant['departmentDocumentsVerifiedFemale'] = Application::whereIn('id', $applicationIdsInDepartment)->getStatusByGender('=', '5', '2', $nationalityOperator, $nationality)->count();
            $applicant['departmentRejectedFemale'] = Application::whereIn('id', $applicationIdsInDepartment)->getStatusByGender('=', '6', '2', $nationalityOperator, $nationality)->count();

            $programApplicationsWithFemaleApplicants = Application::with(['applicant'])->where('fkProgramId', $programId)->whereHas('applicant', function ($query) use ($nationalityOperator, $nationality) {
                $query->where('fkGenderId', 2)->where('fkNationality', $nationalityOperator, $nationality);
            })->get()->count();
            $applicant['feePendingFemale'] = Application::where('fkProgramId', $programId)->getStatusByGender('<', '4', '2', $nationalityOperator, $nationality)->where('fkCurrentStatus', '=', 3)->count();
            $applicant['feeConfirmedFemale'] = Application::where('fkProgramId', $programId)->getStatusByGender('>=', '4', '2', $nationalityOperator, $nationality)->count();
            $applicant['documentsVerifiedFemale'] = Application::where('fkProgramId', $programId)->getStatusByGender('>=', '5', '2', $nationalityOperator, $nationality)->count();
            $applicant['rejectedFemale'] = Application::where('fkProgramId', $programId)->getStatusByGender('=', '6', '2', $nationalityOperator, $nationality)->count(); else :
                $departmentApplicationsWithFemaleApplicants = 'N/A';
            $applicant['departmentfeePendingFemale'] = 'N/A';
            $applicant['departmentFeeConfirmedFemale'] = 'N/A';
            $applicant['departmentDocumentsVerifiedFemale'] = 'N/A';
            $applicant['departmentRejectedFemale'] = 'N/A';


            $programApplicationsWithFemaleApplicants = 'N/A';
            $applicant['feePendingFemale'] = 'N/A';
            $applicant['feeConfirmedFemale'] = 'N/A';
            $applicant['documentsVerifiedFemale'] = 'N/A';
            $applicant['rejectedFemale'] = 'N/A';
            endif;
            
            // Dev Mannan: Stats Code ends
            $page = request('page_num');
            $applications = $this->paginateCollection($applications, $perPage = 30, $page);
            return view('docs.verify', compact('faculties', 'department', 'applications', 'selectedFilters', 'selectedDepartments', 'selectedPorgrams', 'departmentApplicationsWithMaleApplicants', 'departmentApplicationsWithFemaleApplicants', 'programApplicationsWithMaleApplicants', 'programApplicationsWithFemaleApplicants', 'applicant'));
        } elseif (request('department')) {
            $departmentId = request('department');
            $department = Department::find($departmentId);
            if ($allowedGenderAccess != '1') {
                if ($overceasAccess != 'NULL') {
                    $gender = Applicant::where('fkGenderId', $allowedGenderAccess)->whereIn('fkNationality', $overceasAccess)->get();
                    if ($gender) {
                        $allowed_gender = [];
                        foreach ($gender as $gn) {
                            array_push($allowed_gender, $gn->userId);
                        }
                    }
                    //Gender permission with overseas access => done
                    $applications = $department->getApplicationWithStatus($operator, $status)->get();
                    $applications = $applications->load(['applicant' => function ($query) use ($allowedGenderAccess) {
                        $query->where('fkGenderId', $allowedGenderAccess);
                    }])->where('fkCurrentStatus', '>', 1)->whereIn('fkApplicantId', $allowed_gender);
                    $applications = $applications->load(['applicant', 'program','program.programscheduler', 'applicant.previousQualification', 'applicant.detail', 'applicant.detail.district', 'getRollNumber']);
                } else {
                    //Gender permission with no overseas access => done
                    $applications = $department->getApplicationWithStatus($operator, $status)->get();
                    $applications = $applications->load(['applicant'=> function ($query) use ($allowedGenderAccess) {
                        $query->where('fkGenderId', $allowedGenderAccess);
                    }])->where('fkCurrentStatus', '>', 1);
                    $applications = $applications->load(['applicant', 'program','program.programscheduler', 'applicant.previousQualification', 'applicant.detail', 'applicant.detail.district', 'getRollNumber']);
                }
            } else {
                //Has access to both male and female => done
                $applications = $department->getApplicationWithStatus($operator, $status)->get();
                $applications = $applications->load(['applicant', 'program','program.programscheduler', 'applicant.previousQualification', 'applicant.detail', 'applicant.detail.district', 'getRollNumber'])->where('fkCurrentStatus', '>', 1);
            }
            if (request()->route()->getName() == 'export-docs-verification-data') {
                return $this->exportRecordsToCSV($applications, $selectedFilters);
            }
            // Dev Mannan: Stats code starts
            $male = 3;
            $female = 2;
            $bothMaleAndFemale = 1;
            $nationality = 1;
            $nationalityOperator = '>=';
            $applicationIdsInDepartment = $department->applications->pluck('id');
            if ($allowedGenderAccess == $bothMaleAndFemale || $allowedGenderAccess == $male) :
                $departmentApplicationsWithMaleApplicants = Application::with(['applicant'])->whereIn('id', $applicationIdsInDepartment)->whereHas('applicant', function ($query) use ($nationalityOperator, $nationality) {
                    $query->where('fkGenderId', 3)->where('fkNationality', $nationalityOperator, $nationality);
                })->get()->count();
            $applicant['departmentfeePendingMale'] = Application::whereIn('id', $applicationIdsInDepartment)->getStatusByGender('<', '4', '3', $nationalityOperator, $nationality)->where('fkCurrentStatus', '=', 3)->count();
            $applicant['departmentFeeConfirmedMale'] = Application::whereIn('id', $applicationIdsInDepartment)->getStatusByGender('>=', '4', '3', $nationalityOperator, $nationality)->count();
            $applicant['departmentDocumentsVerifiedMale'] = Application::whereIn('id', $applicationIdsInDepartment)->getStatusByGender('=', '5', '3', $nationalityOperator, $nationality)->count();
            $applicant['departmentRejectedMale'] = Application::whereIn('id', $applicationIdsInDepartment)->getStatusByGender('=', '6', '3', $nationalityOperator, $nationality)->count(); else :
                $departmentApplicationsWithMaleApplicants = 'N/A';
            $applicant['departmentfeePendingMale'] = 'N/A';
            $applicant['departmentFeeConfirmedMale'] = 'N/A';
            $applicant['departmentDocumentsVerifiedMale'] = 'N/A';
            $applicant['departmentRejectedMale'] = 'N/A';
            endif;

            if ($allowedGenderAccess == $bothMaleAndFemale || $allowedGenderAccess == $female) :
                $departmentApplicationsWithFemaleApplicants = Application::with(['applicant'])->whereIn('id', $applicationIdsInDepartment)->whereHas('applicant', function ($query) use ($nationalityOperator, $nationality) {
                    $query->where('fkGenderId', 2)->where('fkNationality', $nationalityOperator, $nationality);
                })->get()->count();
            $applicant['departmentfeePendingFemale'] = Application::whereIn('id', $applicationIdsInDepartment)->getStatusByGender('<', '4', '2', $nationalityOperator, $nationality)->where('fkCurrentStatus', '=', 3)->count();
            $applicant['departmentFeeConfirmedFemale'] = Application::whereIn('id', $applicationIdsInDepartment)->getStatusByGender('>=', '4', '2', $nationalityOperator, $nationality)->count();
            $applicant['departmentDocumentsVerifiedFemale'] = Application::whereIn('id', $applicationIdsInDepartment)->getStatusByGender('=', '5', '2', $nationalityOperator, $nationality)->count();
            $applicant['departmentRejectedFemale'] = Application::whereIn('id', $applicationIdsInDepartment)->getStatusByGender('=', '6', '2', $nationalityOperator, $nationality)->count(); else :
                $departmentApplicationsWithFemaleApplicants = 'N/A';
            $applicant['departmentfeePendingFemale'] = 'N/A';
            $applicant['departmentFeeConfirmedFemale'] = 'N/A';
            $applicant['departmentDocumentsVerifiedFemale'] = 'N/A';
            $applicant['departmentRejectedFemale'] = 'N/A';
            endif;
            // echo 'Total Department male= ' . $departmentApplicationsWithMaleApplicants . '<br />';
            // echo 'Total Department female= ' . $departmentApplicationsWithFemaleApplicants . '<br />';
            // echo 'Department Fee pending male= ' . $applicant['departmentfeePendingMale'] . '<br />';
            // echo 'Department Fee pending female= ' . $applicant['departmentfeePendingFemale'] . '<br />';
            // echo 'Department Fee confirmed male= ' . $applicant['departmentFeeConfirmedMale'] . '<br />';
            // echo 'Department Fee confirmed female= ' . $applicant['departmentFeeConfirmedFemale'] . '<br />';
            // echo 'Department Documents Verified male= ' . $applicant['departmentDocumentsVerifiedMale'] . '<br />';
            // echo 'Department Documents Verified female= ' . $applicant['departmentDocumentsVerifiedFemale'] . '<br />';
            // echo 'Department Rejected male= ' . $applicant['departmentRejectedMale'] . '<br />';
            // echo 'Department Rejected female= ' . $applicant['departmentRejectedFemale'] . '<br />';
            // exit;
            // Dev Mannan: Stats Code ends
            $page = request('page_num');
            // dump($page);
            // dump($applications);
            $applications = $this->paginateCollection($applications, $perPage = 30, $page);
            // $applications = $applications->forPage($page,30);
            return view('docs.verify', compact('faculties', 'department', 'applications', 'selectedFilters', 'selectedDepartments', 'selectedPorgrams', 'departmentApplicationsWithMaleApplicants', 'departmentApplicationsWithFemaleApplicants', 'applicant'));
        }

        return view('docs.verify', compact('faculties', 'selectedDepartments', 'selectedPorgrams'));
    }

    public static function paginateCollection($items, $perPage = 30, $page, $options = ['path' => 'docs'])
    {
        $page = $page ? : (\Illuminate\Pagination\Paginator::resolveCurrentPage() ? : 1);
        $items = $items instanceof \Illuminate\Support\Collection ? $items : \Illuminate\Support\Collection::make($items);
        return new \Illuminate\Pagination\LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);
    }

    public function getDepartments($facultyId = '')
    {
        $id = (request('id')) ? request('id') : $facultyId;
        $userHasPermissions = $this->getPermissionsOfCurrentUser();
        $allowedDepartmentsToCurrentUser = $userHasPermissions->fkDepartmentId;

        if ($id) {
            if ($allowedDepartmentsToCurrentUser != '') {
                return Department::where('fkFacId', $id)->whereIn('pkDeptId', explode(',', $allowedDepartmentsToCurrentUser))->where('status', 1)->get();
            } else {
                return Department::where('fkFacId', $id)->where('status', 1)->get();
            }
        }
    }

    public function getProgramme($departmentId = '')
    {
        $id = (request('id')) ? request('id') : $departmentId;
        if ($id) {
            return Programme::with(['programscheduler'])
                ->where('fkDepId', $id)
                ->whereHas('programscheduler', function ($query) {
                    $query->where('status', 1)->orWhere('status', 0);
                })
                ->get();
        }
    }

    public function updateDocumentStatus()
    {
        $request = [];
        parse_str(request('data'), $request);
        $status = $request['status'];
        $id = $request['appId'];
        $comment = $request['comment'];

        $application = Application::find($id);
        $applicant = Application::find($id)->applicant;
        $to = $applicant->email;
        $name = $applicant->name;
        $programId = $request['program'];
        $programme = Programme::where('pkProgId', $programId)->first();
        $abbrev = $programme->faculty->abbrev;
        $data = array('name' => $name);
        if ($status == 5) {
            // $job = (new SendEmail($abbrev, $data, $to, 'Documents Verified', 'applications.verify'))
            //     ->delay(Carbon::now()->addSeconds(10));
            // dispatch_now($job);
            SendEmail::dispatch(
                $abbrev,
                $data,
                $to,
                'Documents Verified',
                'applications.verify'
            );
            $rollNumber = RollNumber::where('fkApplicationId', $id)->first();
            if (!$rollNumber) {
                RollNumber::create([
                    'fkApplicationId' => $id,
                    'fkSemesterId' => $application->fkSemesterId,
                    'status' => 1
                ]);
            } else {
                $rollNumber->status = 1;
                $rollNumber->save();
            }
        }

        if ($status == 6) {
            // $job = (new SendEmail($abbrev, $data, $to, 'Documents Rejected', 'applications.reject'))
            //     ->delay(Carbon::now()->addSeconds(10));
            // dispatch_now($job);
            SendEmail::dispatch(
                $abbrev,
                $data,
                $to,
                'Documents Rejected',
                'applications.reject'
            );
            $rollNumber = RollNumber::where('fkApplicationId', $id)->first();
            if (!$rollNumber) {
                RollNumber::create([
                    'fkApplicationId' => $id,
                    'fkSemesterId' => $application->fkSemesterId,
                    'status' => 0
                ]);
            } else {
                $rollNumber->status = 0;
                $rollNumber->save();
            }
        }


        $application = Application::find($id);
        $application->fkCurrentStatus = $status;
        $application->fkModifiedBy = auth()->id();
        $application->doumentsVerified = Carbon::now();
        $application->comments = $comment;
        $application->save();

        return $status;
    }

    public function getPermissionsOfCurrentUser()
    {
        return Permission::where('fkUserId', auth()->guard('web')->id())->first();
    }

    public function exportRecordsToCSV($applications, $selectedFilters = '')
    {
        $csv = Writer::createFromFileObject(new SplTempFileObject());
        $csv->insertOne(
            [
                'S.No',
                'Form No',
                'Roll No',
                'Name',
                'CNIC',
                'Father Name',
                'Program Name',
                'Mobile No',
                'Email',
                'District',
                //ssc
                'Degree Title',
                'Subjects',
                'Board',
                'Total Marks',
                'Obtained Marks',
                // hssc
                'Degree Title',
                'Subjects',
                'Board',
                'Total Marks Composite',
                'Obtained Marks Composite',
                'Total Marks Part 1',
                'Obtained Marks Part 1',
                'Total Marks Part 2',
                'Obtained Marks Part 2',
                //ba_bsc
                'Degree Title',
                'Subjects',
                'Specialization',
                'University',
                'Total Marks / CGPA',
                'Obtained Marks / GPA',
                //msc
                'Degree Title',
                'Subjects',
                'Specialization',
                'University',
                'Total Marks / CGPA',
                'Obtained Marks / GPA',
                //bs
                'Degree Title',
                'Subjects',
                'Specialization',
                'University',
                'Exam System',
                'Total Marks / CGPA',
                'Obtained Marks / GPA',
                //ms
                'Degree Title',
                'Subjects',
                'Specialization',
                'University',
                'Exam System',
                'Total CGPA',
                'Obtained GPA',
                'Result Awaiting',
                'Fee Verified',
                'Documents Verified',
                'Documents Rejected',
                'PQM'
            ]
        );
        $serialNumber = 1;
        foreach ($applications as $application) {
            $feeVerifiedStatus      = ($application->fkCurrentStatus >= 4)   ? 'Yes' : 'No';
            $documentVerifiedStatus = ($application->fkCurrentStatus >= 5) ? 'Yes' : 'No';
            $documentRejectedStatus = ($application->fkCurrentStatus == 6) ? 'Yes' : 'No';

            //PQM Vars
            $level = $application->program->fkLeveliId;
            $preReq = $application->program->fkReqId;
            $sscTotal = $application->applicant->previousQualification->SSC_Total;
            $sscObtained = $application->applicant->previousQualification->SSC_Composite;

            $hsscTotal = $application->applicant->previousQualification->HSSC_Total;
            $hsscObtained = $application->applicant->previousQualification->HSSC_Composite;
            $hsscTotalPart1 = $application->applicant->previousQualification->HSSC_Total1;
            $hsscTotalPart2 = $application->applicant->previousQualification->HSSC_Total2;
            $hsscObtainedPart1 = $application->applicant->previousQualification->HSSC1;
            $hsscObtainedPart2 = $application->applicant->previousQualification->HSSC2;

            $baBscTotal = $application->applicant->previousQualification->BA_Bsc_Total;
            $baBscObtained = $application->applicant->previousQualification->BA_Bsc;
            
            $mscTotal = $application->applicant->previousQualification->MSc_Total;
            $mscObtained = $application->applicant->previousQualification->MSc;
            $bsTotal = $application->applicant->previousQualification->BS_Total;
            $bsObtained = $application->applicant->previousQualification->BS;

            $msTotal = $application->applicant->previousQualification->MS_Total;
            $msObtained = $application->applicant->previousQualification->MS;

            $interviewDateTime = $application->program->programscheduler->interviewDateTime;
            $pqm = Self::calculatePqm(
                $application->id,
                $level,
                $interviewDateTime,
                $preReq,
                $sscTotal,
                $sscObtained,
                $hsscTotal,
                $hsscObtained,
                $hsscTotalPart1,
                $hsscTotalPart2,
                $hsscObtainedPart1,
                $hsscObtainedPart2,
                $baBscTotal,
                $baBscObtained,
                $mscTotal,
                $mscObtained,
                $bsTotal,
                $bsObtained,
                $msTotal,
                $msObtained
            );
            $csv->insertOne(
                [
                    $serialNumber ?? '',
                    $application->id ?? '',
                    $application->getRollNumber->id ?? '',
                    $application->applicant->name ?? '',
                    $application->applicant->cnic ?? '',
                    $application->applicant->detail->fatherName ?? '',
                    $application->program->title ?? '',
                    $application->applicant->detail->mobile ?? '',
                    $application->applicant->email ?? '',
                    $application->applicant->detail->district->distName ?? '', //to be added

                    //ssc
                    $application->applicant->previousQualification->SSC_Title ?? '', //to be added
                    $application->applicant->previousQualification->SSC_Subject ?? '',
                    $application->applicant->previousQualification->SSC_From ?? '',
                    $sscTotal ?? '',
                    $sscObtained ?? '',

                    //hssc
                    $application->applicant->previousQualification->HSC_Title ?? '',
                    $application->applicant->previousQualification->HSC_Subject ?? '',
                    $application->applicant->previousQualification->HSC_From ?? '',
                    $hsscTotal ?? '',
                    $hsscObtained ?? '',
                    $hsscTotalPart1 ?? '',
                    $hsscObtainedPart1 ?? '',
                    $hsscTotalPart2 ?? '',
                    $hsscObtainedPart2 ?? '',

                    //ba_bsc
                    $application->applicant->previousQualification->BA_Bsc_Title ?? '',
                    $application->applicant->previousQualification->BA_Bsc_Subject ?? '',
                    $application->applicant->previousQualification->BA_Bsc_Specialization ?? '',
                    $application->applicant->previousQualification->BA_Bsc_From ?? '',
                    $baBscTotal ?? '',
                    $baBscObtained ?? '',
                    // $baBscTotal = $application->applicant->previousQualification->BA_Bsc_Total;
                    // $baBscObtained = $application->applicant->previousQualification->BA_Bsc;

                    //msc
                    $application->applicant->previousQualification->MSc_Title ?? '',
                    $application->applicant->previousQualification->MSc_Subject ?? '',
                    $application->applicant->previousQualification->MSc_Specialization ?? '',
                    $application->applicant->previousQualification->MSc_From ?? '',
                    $application->applicant->previousQualification->MSc_Total ?? '',
                    $application->applicant->previousQualification->MSc ?? '',

                    //bs
                    $application->applicant->previousQualification->BS_Title ?? '',
                    $application->applicant->previousQualification->BS_Subject ?? '',
                    $application->applicant->previousQualification->BS_Specialization ?? '',
                    $application->applicant->previousQualification->BS_From ?? '',
                    $application->applicant->previousQualification->BS_Exam_System ?? '',
                    $application->applicant->previousQualification->BS_Total ?? '',
                    $application->applicant->previousQualification->BS ?? '',

                    //ms
                    $application->applicant->previousQualification->MS_Title ?? '',
                    $application->applicant->previousQualification->MS_Subject ?? '',
                    $application->applicant->previousQualification->MS_Specialization ?? '',
                    $application->applicant->previousQualification->MS_From ?? '',
                    $application->applicant->previousQualification->MS_Exam_System ?? '',
                    $application->applicant->previousQualification->MS_Total ?? '',
                    $application->applicant->previousQualification->MS ?? '',

                    ($application->applicant->previousQualification->resultAwaiting == 1) ? 'Yes' : 'No',
                    $feeVerifiedStatus ?? '',
                    $documentVerifiedStatus ?? '',
                    $documentRejectedStatus ?? '',
                    $pqm ?? 'NULL'
                ]
            );
            $serialNumber++;
        }

        $currentUser = User::find(auth()->id());
        $currentDate = date('d-m-Y h:i:s A');
        $message = "Report generated by $currentUser->first_name $currentUser->last_name on $currentDate via Online Admission System";
        $csv->insertOne([$message]);

        $facultyName = $selectedFilters['facultyTitle'] ?? '';
        $departmentName = $selectedFilters['departmentTitle'] ?? '';
        $programName = $selectedFilters['programmeTitle'] ?? '';

        $currentDate = date('d-m-Y h-i-s A');
        $fileName = 'document-verification- ' . $departmentName . ' -- ' . $programName . ' -- ' . $currentDate . '.csv';
        $csv->output($fileName);
        die;
    }

    /* Start PQM */
    // public static function calculatePqm(
    //     $id,
    //     $level,
    //     $interviewDateTime,
    //     $preReq=0,
    //     $sscTotal = 0,
    //     $sscObtained = 0,
    //     $hsscTotal = 0,
    //     $hsscObtained = 0,
    //     $hsscTotalPart1 = 0,
    //     $hsscTotalPart2 = 0,
    //     $hsscObtainedPart1 = 0,
    //     $hsscObtainedPart2 = 0,
    //     $baBscTotal = 0,
    //     $baBscObtained = 0,
    //     $mscTotal = 0,
    //     $mscObtained = 0,
    //     $bsTotal = 0,
    //     $bsObtained = 0,
    //     $msTotal = 0,
    //     $msObtained = 0
    // ) {
    //     if ($level == 1 || $level == 2) {
    //         /* BS & Diploma */
    //         return Self::calculatePqmForUnderGraduate($sscObtained, $sscTotal, $hsscObtained, $hsscTotal, $hsscObtainedPart1, $hsscTotalPart1, $hsscObtainedPart2, $hsscTotalPart2);
    //     } elseif ($level == 3) {
    //         /* MA,  MSc */
    //         if ($interviewDateTime !== '0000-00-00 00:00:00') {
    //             return Self::calculatePqmForMaMsc($baBscObtained, $baBscTotal);
    //         } else {
    //             return Self::calculatePqmForUnderGraduate($sscObtained, $sscTotal, $hsscObtained, $hsscTotal, $hsscObtainedPart1, $hsscTotalPart1, $hsscObtainedPart2, $hsscTotalPart2);
    //         }
    //     } elseif ($level == 4) {
    //         /* Masters */
    //         return Self::calculatePqmForMasters($mscObtained, $mscTotal, $bsObtained, $bsTotal);
    //     } elseif ($level == 5) {
    //         /* PostGraduate */
    //         return Self::calculatePqmForPostGraduate($msObtained, $msTotal);
    //     }
    // }
    
    // public static function calculatePqmForUnderGraduate($sscObtained, $sscTotal, $hsscObtained, $hsscTotal, $hsscObtainedPart1, $hsscTotalPart1, $hsscObtainedPart2, $hsscTotalPart2)
    // {
    //     $ssc15Percent = 0;
    //     $hssc25Percent = 0;
    //     if ($sscTotal != '' || $sscTotal != 0) {
    //         $ssc15Percent = ((float)$sscObtained/(float)$sscTotal)*15;
    //     }
    //     if ($hsscTotal !== '' || $hsscTotal != 0) {
    //         $hssc25Percent = ((float)$hsscObtained/(float)$hsscTotal)*25;
    //     } elseif ($hsscTotalPart1 !== '' && $hsscTotalPart2 !== '') {
    //         $hssc25Percent = (((float)$hsscObtainedPart1+(float)$hsscObtainedPart2) / ((float)$hsscTotalPart1+(float)$hsscTotalPart2))*25;
    //     } elseif ($hsscTotalPart1 !== '') {
    //         $hssc25Percent = (float)$hsscObtainedPart1 / ((float)$hsscTotalPart1)*25;
    //     }
    //     $pqm = $ssc15Percent + $hssc25Percent;
    //     return $pqm = number_format((float)$pqm, 2, '.', '');
    // }

    // public static function calculatePqmForMaMsc($baBscObtained, $baBscTotal)
    // {
    //     if ($baBscTotal !='' || $baBscTotal != 0) {
    //         $pqm = ((float)$baBscObtained/(float)$baBscTotal)*30;
    //         return $pqm = number_format((float)$pqm, 2, '.', '');
    //     }
    // }

    // public static function calculatePqmForMasters($mscObtained, $mscTotal, $bsObtained, $bsTotal)
    // {
    //     $msc30Percent = '';
    //     $bs30Percent = '';
    //     $previousDegree30Percent = 0;
    //     if ($mscTotal && $bsTotal) {
    //         $bs30Percent =  ((float)$bsObtained/(float)$bsTotal)*30;
    //         $msc30Percent =  ((float)$mscObtained/(float)$mscTotal)*30;
    //         if ($msc30Percent >= $bs30Percent) {
    //             $previousDegree30Percent = $msc30Percent;
    //         } else {
    //             $previousDegree30Percent = $bs30Percent;
    //         }
    //     } elseif ($bsTotal) {
    //         $previousDegree30Percent = $bs30Percent =  ((float)$bsObtained/(float)$bsTotal)*30;
    //     } elseif ($mscTotal) {
    //         $previousDegree30Percent = $msc30Percent =  ((float)$mscObtained/(float)$mscTotal)*30;
    //     }

    //     // $pqm = $previousDegree30Percent;
    //     return number_format((float)$previousDegree30Percent, 2, '.', '');
    // }

    // public static function calculatePqmForPostGraduate($msObtained, $msTotal)
    // {
    //     $previousDegree30Percent = 0;
    //     if ($msTotal) {
    //         $previousDegree30Percent = ((float)$msObtained / (float)$msTotal)*30;
    //     }
          
    //     // $pqm = $previousDegree30Percent;
    //     return number_format((float)$previousDegree30Percent, 2, '.', '');
    // }
    
    public function bulkUpdatePqmForFetBsProgram()
    {
        $applications = Application::with([
            'program',
            'faculty',
            'program.programscheduler',
            'applicant',
            'applicant.previousQualification'
        ])
        ->where('fkFacId', 3)
        ->where('fkCurrentStatus', 5)
        ->where('fkSemesterId', 13)
        ->get();
        foreach ( $applications as $application ) {
            $this->generateAndStorePQM($application);
        }
        return 'done';
    }

    public function updatePqmForSpring2021()
    {
        $applications = Application::with(['program','faculty','program.programscheduler','applicant','applicant.previousQualification'])
            ->where('fkCurrentStatus', 5)
            ->where('fkSemesterId', 17)
            ->get();
            foreach ( $applications as $application ) {
                $this->generateAndStorePQM($application);
            }
    }

    public function bulkUpdatePqmForProgram($programId)
    {
        $applications = Application::with([
            'program',
            'faculty',
            'program.programscheduler',
            'applicant',
            'applicant.previousQualification'
        ])
        ->where('fkProgramId', $programId)
        ->where('fkCurrentStatus', 5)
        ->where('fkSemesterId', 13)
        ->get();
        // dd ( $applications );
        foreach ( $applications as $application ) {
            $this->generateAndStorePQM($application);
        }
        return 'done';
    }
}
