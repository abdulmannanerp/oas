<?php

namespace App\Http\Controllers;

use Exception;
use GuzzleHttp\Client;
use App\Model\Applicant;
use App\Model\RollNumber;

use App\Model\ChallanDetail;
use App\Model\StudentBasicInformation;
use App\Model\ChallanParentTable;
use App\Model\ChallanChildTable;
use App\Model\Semester;
use App\Model\Account;

use Illuminate\Http\Request;
use App\Model\AljamiaNewStudent;
use App\Jobs\MigrateToAljamiaJob;
use App\Model\AljamiaNewAcadRecord;
use App\Jobs\SplitMigrateToAljamiaJob;
use App\Jobs\MigrateSingleRecordToAljamia;
use App\Jobs\SendCnicAndGenderInformationToAljamia;

class MigrateToTables extends Controller
{
    public function index($faculty, $semester)
    {
        // dd($faculty);
        $challanDetail = ChallanDetail::with([
            'program',
            'program.department',
            'program.faculty',
            'semester',
            'application.applicant',
            'application.applicant.gender',
            'application.applicant.applicantDetail',
            'rollNumber'
        ])
        ->whereHas('application', function ($query) use ($faculty) {
            $query->where('fkFacId', $faculty);
        })
        ->where('fkSemesterId', $semester)->whereIn('challan_type', array(2, 3))
        // ->groupBy('fkApplicationtId')
        ->get();

        $semester = Semester::where('pkSemesterId', $semester)->first();
        // dd($challanDetail);
        foreach ($challanDetail as $cd) {
            $content = json_decode(
                json_encode($this->parseStudentInfo($cd))
            );
            $studentExists = StudentBasicInformation::where('CNIC', $content->CNIC)->first();
            if ( !$studentExists ) {
                StudentBasicInformation::create([
                    'ROLLNO' => $content->ROLLNO,
                    'STUDNAME' => $content->STUDNAME,
                    'FATHERNAME' => $content->FATHERNAME,
                    'CNIC' => $content->CNIC,
                    'EMAIL' => $content->EMAIL,
                    'GENDER' => $content->GENDER,
                    'FAC_DESCRIP' => $content->FAC_DESCRIP,
                    'DEP_DESCRIP' => $content->DEP_DESCRIP,
                    'ACADPROG_DESCRIP' => $content->ACADPROG_DESCRIP,
                    'SEMESTER' => $semester->title
                ]);
            }
            $challanExits = ChallanParentTable::where('CHALLANNO', $content->CHALLANNO)->first();
            if ( !$challanExits ) {
                $accountNumber = $this->getAccount($cd->program->faculty->pkFacId, $content->GENDER);
                ChallanParentTable::create([
                    'CHALLANNO' => $content->CHALLANNO,
                    'ROLLNO' => $content->ROLLNO,
                    'NAME' => $content->STUDNAME,
                    'FATHERNAME' => $content->FATHERNAME,
                    'CNIC' => $content->CNIC,
                    'GENDER' => $content->GENDER,
                    'CHALLANDATE' => $content->CHALLANDATE,
                    'BANKNAME'=> $accountNumber->bank_name,
                    'SEMESTER' => $semester->title,
                    'BANKACCOUNTNO' => ($cd->challan_type == 2) ? $accountNumber->dues_account : $accountNumber->security_account,

                ]);
            }

            $challanExits = ChallanChildTable::where('CHALLANNO', $content->CHALLANNO)->first();
            if ( !$challanExits ) {
                ChallanChildTable::create([
                    'CHALLANNO' => $content->CHALLANNO,
                    'ROLLNO' => $content->ROLLNO,
                    'NAME' => $content->STUDNAME,
                    'FATHERNAME' => $content->FATHERNAME,
                    'CNIC' => $content->CNIC,
                    'GENDER' => $content->GENDER,
                    'SEMESTER' => $semester->title,
                    'CHALLANDATE' => $content->CHALLANDATE,
                    'ADMISSIONFEE' => ($cd->challan_type == 2) ? $content->ADMISSIONFEE : 0,
                    'UNIVERSITYDUES' => ($cd->challan_type == 2) ? $content->UNIVERSITYDUES : 0 ,
                    'LIBRARYSECURITY' => ($cd->challan_type == 2) ? 0 : $content->LIBRARYSECURITY,
                    'BOOKBANKSECURITY' => ($cd->challan_type == 2) ? 0 : $content->BOOKBANKSECURITY,
                    'CAUTIONMONEY' => ($cd->challan_type == 2) ? 0 : $content->CAUTIONMONEY 

                ]);
            }
        }
    }

    

    public function getAccount($facultyId, $gender){
        return Account::where('fkFacId', $facultyId)->where('bank_type', $gender)->first();
    }


    public function parseStudentInfo($ch){
        $info = [];

        $info['ROLLNO'] = $this->cleanString($ch->fkRollNumber) ?? '';
        $info['STUDNAME'] = $this->cleanString($ch->application->applicant->name) ?? '';
        $info['FATHERNAME'] = $this->cleanString($ch->application->applicant->applicantDetail->fatherName) ?? '';
        $info['CNIC'] = $this->cleanString($ch->application->applicant->cnic) ?? '';
        $info['EMAIL'] = $this->cleanString($ch->application->applicant->email) ?? '';
        $info['GENDER'] = $this->cleanString($ch->application->applicant->gender->gender) ?? '';
        $info['FAC_DESCRIP'] = $this->cleanString($ch->program->faculty->title) ?? '';
        $info['DEP_DESCRIP'] = $this->cleanString($ch->program->department->title) ?? '';
        $info['ACADPROG_DESCRIP'] = $this->cleanString($ch->program->title) ?? '';
        $info['CHALLANNO'] = $this->cleanString($ch->id) ?? '';
        $info['CHALLANDATE'] = $this->cleanString($ch->created_at) ?? '';
        $info['ADMISSIONFEE'] = $this->cleanString($ch->admission_fee) ?? '';
        $info['UNIVERSITYDUES'] = $this->cleanString($ch->university_dues) ?? '';
        $info['LIBRARYSECURITY'] = $this->cleanString($ch->library_security) ?? '';
        $info['BOOKBANKSECURITY'] = $this->cleanString($ch->book_bank_security) ?? '';
        $info['CAUTIONMONEY'] = $this->cleanString($ch->caution_money) ?? '';
        return $info;
    }


    /**
     * Remove ",", "'" and "&" symbol from string
     * Oracle wants this to be cleaned
     */
    public function cleanString($string)
    {
        $removeCommaFromString           =  str_replace(",", "", $string);
        $removeApostropheFromString      = str_replace("'", "", $removeCommaFromString);
        return $removeAmpersandFromString= str_replace("&", "and", $removeApostropheFromString);
    }
}
