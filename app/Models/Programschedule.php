<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class programschedule extends Sximo  {
	
	protected $table = 'tbl_oas_programme_schedule';
	protected $primaryKey = 'pkScheduleId';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "  SELECT tbl_oas_programme_schedule.* FROM tbl_oas_programme_schedule  ";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE tbl_oas_programme_schedule.pkScheduleId IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	

}
