<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class department extends Sximo  {
	
	protected $table = 'tbl_oas_department';
	protected $primaryKey = 'pkDeptId';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "  SELECT tbl_oas_department.* FROM tbl_oas_department  ";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE tbl_oas_department.pkDeptId IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	

}
