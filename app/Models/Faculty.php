<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class faculty extends Sximo  {
	
	protected $table = 'tbl_oas_faculty';
	protected $primaryKey = 'pkFacId';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "  SELECT tbl_oas_faculty.* FROM tbl_oas_faculty  ";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE tbl_oas_faculty.pkFacId IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	

}
