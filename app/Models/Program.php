<?php 
namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class program extends Sximo  {
	
	protected $table = 'tbl_oas_programme';
	protected $primaryKey = 'pkProgId';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "  SELECT tbl_oas_programme.* FROM tbl_oas_programme  ";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE tbl_oas_programme.pkProgId IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	

}
