<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class semester extends Sximo  {
	
	protected $table = 'tbl_oas_semester';
	protected $primaryKey = 'pkSemesterId';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "  SELECT tbl_oas_semester.* FROM tbl_oas_semester  ";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE tbl_oas_semester.pkSemesterId IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	

}
