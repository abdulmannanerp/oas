<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ProgramFeeOld extends Model
{
    protected $guarded = [];
    protected $table = 'tbl_oas_program_fee_structure_old';
    protected $dates = [
        'created_at',
        'updated_at'
    ];

    public function program() 
    {
        return $this->belongsTo('App\Model\Programme', 'fkProgramId');
    }
    public function semester() 
    {
        return $this->belongsTo('App\Model\Semester', 'fkSemesterId');
    }
}
