<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class PreviousQualification extends Model {

    protected $table = 'tbl_oas_previous_qualification';
    public $timestamps = false;
    protected $guarded = [];
//    protected $primaryKey = 'pkLevelId';

   
}
