<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Country extends Model {

    protected $table = 'tbl_oas_country';
    protected $primaryKey = 'pkCountryId';
}
