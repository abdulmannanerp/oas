<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Status extends Model {

    protected $table = 'tbl_oas_app_status';
    protected $primaryKey = 'pkAppStatusId';

}
