<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Faculty extends Model
{
    protected $primaryKey = 'pkFacId';
    protected $table = 'tbl_oas_faculty';


    public function departments() {
        return $this->hasMany('App\Model\Department', 'fkFacId');
    }

    public function programmes() {
        return $this->hasMany('App\Model\Programme', 'fkFacId');
    }

    public function applications() {
        return $this->hasManyThrough('App\Model\Application', 'App\Model\Programme', 'fkFacId', 'fkProgramId', 'pkFacId');
        return $this->hasMany('App\Model\Application', 'fkFacId');
    }
}
