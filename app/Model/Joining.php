<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Joining extends Model
{
    protected $guarded = [];
    protected $table = 'tbl_joining';
}
