<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class MeritList extends Model
{
    protected $table = 'tbl_oas_merit_list';
    protected $guarded = [];

    public function merit()
    {
    	return $this->belongsTo('App\Model\Merit', 'merit_id');
    }

    public function getRollNo()
    {
    	return $this->belongsTo('App\Model\RollNumber', 'rollno');
    }
}
