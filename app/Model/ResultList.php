<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ResultList extends Model
{
	protected $guarded = [];
    protected $table = 'tbl_oas_result_list';

    public function result()
    {
    	return $this->belongsTo('App\Model\Result', 'fkResultId');
    }

    public function getRollno()
    {
        return $this->belongsTo('App\Model\RollNumber', 'rollno');
    }

    protected $dates = [
        'created_at',
        'updated_at'
    ];
}
