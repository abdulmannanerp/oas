<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class FeeVerificationFailed extends Model
{
    protected $table = 'tbl_oas_fee_verification_failed';
    protected $guarded = [];

    public function user() {
        return $this->belongsTo('App\User', 'user_id');
    }
}
