<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Convocation extends Model
{

	protected $guarded = [];
    public function program()
    {
    	return $this->belongsTo('App\Model\Programme', 'degree');
    }

    public function user()
    {
    	return $this->belongsTo('App\User', 'fkFeeVerifiedBy');
    }
}
