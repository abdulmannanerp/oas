<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ScholarshipInformation extends Model
{
    protected $table = 'tbl_sch_personal_detail';
    protected $fillable = ['fkApplicantId'];
     protected $fillables = ['fkSchlrId'];
    
    
    public function applicant() 
    {
        return $this->belongsTo('App\Model\Applicant', 'fkApplicantId');
    }

     public function listscholarship()
    {
        return $this->belongsTo('App\Model\Listscholarship', 'fkSchlrId');
    }
}
