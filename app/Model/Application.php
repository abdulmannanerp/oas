<?php

namespace App\Model;

use App\Model\Semester;
use Illuminate\Database\Eloquent\Model;

class Application extends Model
{
    protected $table = 'tbl_oas_applications';
    protected $guarded =[];

    public function applicant() 
    {
        return $this->belongsTo('App\Model\Applicant', 'fkApplicantId');
    }

    public function user() 
    {
        return $this->belongsTo('App\User', 'fkFeeVerifiedBy');
    }

    public function docsVerifyAdmin() 
    {
        return $this->belongsTo('App\User', 'fkReviewedBy');
    }

    public function status() 
    {
        return $this->belongsTo('App\Model\Status', 'fkCurrentStatus');
    }

    public function program() 
    {
        return $this->belongsTo('App\Model\Programme', 'fkProgramId');
    }

    public function pref1() 
    {
        return $this->belongsTo('App\Model\Programme', 'preferrence1');
    }

    public function pref2() 
    {
        return $this->belongsTo('App\Model\Programme', 'preferrence2');
    }

    public function pref3() 
    {
        return $this->belongsTo('App\Model\Programme', 'preferrence3');
    }

    public function department()
    {
        return $this->belongsTo('App\Model\Department', 'fkDepId');
    }

    public function faculty()
    {
        return $this->belongsTo('App\Model\Faculty', 'fkFacId');
    }

    public function semester()
    {
        return $this->belongsTo('App\Model\Semester', 'fkSemesterId');
    }

    public function getRollNumber() {
        return $this->hasOne('App\Model\RollNumber', 'fkApplicationId');
    }

    public function results() 
    {
        return $this->belongsTo('App\Model\ResultList', 'rollno');
    }

    public function city() 
    {
        return $this->belongsTo('App\Model\City', 'bankDetails');
    }

    public function challan()
    {
        return $this->hasOne('App\Model\ChallanDetail', 'fkApplicationtId');
    }
    public function challanMany()
    {
        return $this->hasMany('App\Model\ChallanDetail', 'fkApplicationtId');
    }

    //Overall Stats
    public function scopeGetStats($query, $operator, $number) 
    {
        return $query->where('fkCurrentStatus', $operator,  $number)->where('fkSemesterId', $this->getCurrentSemester());;
    }

    public function scopeGetStatusByGender($query, $operator, $number, $gender, $nationalityOperator='', $nationality = '') 
    {
        return $query->where('fkCurrentStatus', $operator, $number)->with('applicant')->whereHas('applicant', function ( $query )  use ($gender, $nationalityOperator,  $nationality) {
            $query->where('fkGenderId', $gender)->where('fkNationality', $nationalityOperator, $nationality);
        })->where('fkSemesterId', $this->getCurrentSemester())->get();
    }

    public function scopeGetOverAllStatusByGender($query, $operator, $status, $gender ) {
        return $query->whereHas('applicant', function ( $query ) use ( $operator, $status, $gender ) {
            $query->select('userId')->where('fkGenderId', $gender);
        })->where('fkCurrentStatus', $operator, $status )->where('fkSemesterId', $this->getCurrentSemester());
    }

    public function scopeGetOverAllStatusByGenderOrigional($query, $operator, $status, $gender ) {
        return $query->whereHas('applicant', function ( $query ) use ( $operator, $status, $gender ) {
            $query->select('userId')->where('fkGenderId', $gender);
        })->where('fkCurrentStatus', $operator, $status )->where('fkFeeVerifiedBy', '!=' , 182)->where('fkSemesterId', $this->getCurrentSemester());
    }

    public function scopeGetOverAllStatusByGenderByProgram($query, $operator, $status, $gender, $programId ) {
        return $query->whereHas('applicant', function ( $query ) use ( $operator, $status, $gender ) {
            $query->select('userId')->where('fkGenderId', $gender);
        })->where('fkCurrentStatus', $operator, $status )->where('fkProgramId', $programId)->where('fkSemesterId', $this->getCurrentSemester());
    }


    public function getCurrentSemester()
    {
        return Semester::where('status', 1)->first()->pkSemesterId;
    }
}
