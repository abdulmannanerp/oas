<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    protected $table = 'tbl_oas_applicant_address';

    public function country() {
    	return $this->belongsTo('App\Model\Country', 'fkCountryIdPmt');
    }
}
