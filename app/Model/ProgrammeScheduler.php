<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ProgrammeScheduler extends Model
{
    protected $table = 'tbl_oas_programme_schedule';
    protected $primaryKey = 'pkScheduleId';

    public function program(){
        return $this->belongsTo('App\Model\Programme', 'fkProgramId');
    }
}
