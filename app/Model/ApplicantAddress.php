<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ApplicantAddress extends Model {

    protected $table = 'tbl_oas_applicant_address';
    protected $guarded =[];
//    protected $primaryKey = 'pkLevelId';


     public function country() {
        return $this->belongsTo('App\Model\Country', 'fkCountryIdPmt');
    }
   
}
