<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Semester extends Model
{
    protected $table = 'tbl_oas_semester';
    protected $primaryKey = 'pkSemesterid';
}
