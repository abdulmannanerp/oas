<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Scholarship extends Model
{

    protected $table = 'tbl_scholarship';
}
