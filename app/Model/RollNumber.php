<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class RollNumber extends Model
{
    protected $table = 'tbl_oas_roll_number';
    protected $guarded = [];
    public $timestamps = false;

    public function application()
    {
    	return $this->belongsTo('App\Model\Application', 'fkApplicationId');
    }

    public function applicat()
    {
    	return $this->belongsTo('App\Model\Applicant', 'fkApplicantId');
    }
    /* Same as above, method spell different to avoid breaking code */
    public function applicant()
    {
        return $this->belongsTo('App\Model\Applicant', 'fkApplicantId');
    }

    public function previousQualification()
    {
        return $this->belongsTo('App\Model\PreviousQualification', 'fkApplicantId');
    }
    public function rollNo()
    {
        return $this->hasOne('App\Model\ResultList', 'rollno');
    }
    public function engineering() {
        return $this->hasOne('App\Model\RollNumberEng', 'fkRollNumberId');
    }

}
