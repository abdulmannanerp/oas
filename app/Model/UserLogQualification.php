<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class UserLogQualification extends Model {

    protected $table = 'tbl_logs_qualification';
    protected $guarded = [];
    public $timestamps = false;

}
