<?php

namespace App\Model;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Http\Controllers\Controller;
// use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;

class Applicant extends Authenticatable 
{
    use Notifiable;
    protected $guard = 'applicant';
    protected $table = 'tbl_users';
    protected $primaryKey = 'userId';
    protected $guarded =[];
    public $timestamps = false;

    // protected $currentSemester;
    // public function __construct()
    // {
    //     $this->currentSemester = Controller::getCurrentSemester()->pkSemesterId;
    // }

    protected $hidden = [
        'password','remember_token'
    ];

    public function detail() {
        return $this->hasOne('App\Model\ApplicantDetail', 'fkApplicantId');
    }

    public function applicantDetail() {
        return $this->hasOne('App\Model\ApplicantDetail', 'fkApplicantId');
    }
    
    public function ScholarshipInformation() {
        return $this->hasOne('App\Model\ScholarshipInformation', 'fkApplicantId');
    }


     public function ScholarshipFamilyDetail() {
        return $this->hasOne('App\Model\ScholarshipFamilyDetail', 'fkApplicantId');
    }


     public function ScholarshipFamilyAssets() {
        return $this->hasOne('App\Model\ScholarshipFamilyAssets', 'fkApplicantId');
    }

    public function ScholarshipDetail(){
        return $this->hasOne('App\Model\ScholarshipDetail', 'fkApplicantId');
    }


    public function application() {
        return $this->hasMany('App\Model\Application', 'fkApplicantId');
    }

    public function gender() {
        return $this->belongsTo('App\Model\Gender', 'fkGenderId');
    }
    
    
    public function programLevel() {
        return $this->belongsTo('App\Model\ProgramLevel', 'fkLevelId');
    }
    
    public function nationality() {
        return $this->belongsTo('App\Model\Nationality', 'fkNationality');
    }

    public function address() {
        return $this->hasOne('App\Model\Address', 'fkApplicantId');
    }

    

     public function previousQualification( $semester = '' ) {
        //  dd($semester);
        // $currentSemester = Controller::getCurrentSemester()->pkSemesterId;
        if($semester == ''){
            $semester = Cache::get(auth()->id().'-active_semester');
            if(is_null($semester)){
               $semester = Controller::getCurrentSemester()->pkSemesterId;
            }
        }
        return $this->hasOne('App\Model\PreviousQualification', 'fkApplicantId')->where('fkSemesterId', $semester);
    }
    
     public function applicantAddress() {
        return $this->hasOne('App\Model\ApplicantAddress', 'fkApplicantId');
    }
}
