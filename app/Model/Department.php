<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    protected $primaryKey = 'pkDeptId';
    protected $table = 'tbl_oas_department';

    public function faculty()
    {
        return $this->belongsTo('App\Model\Faculty', 'fkFacId');
    }

    public function programme()
    {
        return $this->hasMany('App\Model\Programme', 'fkDepId');
    }

    public function applications()
    {
        return $this->hasManyThrough('App\Model\Application', 'App\Model\Programme', 'fkDepId', 'fkProgramId', 'pkDeptId');
        return $this->hasMany('App\Model\Application', 'fkDeptId');
    }


    public function getApplicationWithStatus( $operator,$status )
    {
        return $this->applications()->where('fkCurrentStatus', $operator, $status );
    } 
}
