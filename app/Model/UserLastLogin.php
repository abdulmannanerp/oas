<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class UserLastLogin extends Model {

    protected $table = 'tbl_last_login';
    protected $guarded = [];
    public $timestamps = false;

}
