<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class PreviousStats extends Model
{
    protected $table = 'tbl_oas_previous_stats';
    protected $guarded = [];

    public function semester()
    {
    	return $this->belongsTo('App\Model\Semester', 'fkSemesterId');
    }
    public function program()
    {
    	return $this->belongsTo('App\Model\Programme', 'fkProgramId');
    }
}
