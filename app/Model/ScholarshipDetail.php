<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ScholarshipDetail extends Model
{
    protected $table = 'tbl_sch_details';
    protected $fillable = ['fkApplicantId'];
    protected $fillables = ['fkSchlrId'];


     public function listscholarship()
    {
        return $this->belongsTo('App\Model\Listscholarship', 'fkSchlrId');
    }

     public function applicant() 
    {
        return $this->belongsTo('App\Model\Applicant', 'fkApplicantId');
    }

  public function scholarshipinformation() 
    {
        //return $this->belongsTo('App\Model\ScholarshipInformation', 'fkApplicantId');
        return $this->hasOne(ScholarshipInformation::class, 'fkApplicantId', 'local_key');
    }

    
}


