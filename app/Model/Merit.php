<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Merit extends Model
{
    protected $table = 'tbl_oas_merit';
    protected $guarded = [];

    public function program()
    {
    	return $this->belongsTo('App\Model\Programme', 'fkProgId');
    }

    public function meritList()
    {
    	return $this->hasMany('App\Model\MeritList', 'merit_id');
    }
}