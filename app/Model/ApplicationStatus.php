<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ApplicationStatus extends Model {

    protected $table = 'tbl_oas_app_step_status';
    protected $guarded =[];

    public function applicant() 
    {
        return $this->belongsTo('App\Model\Applicant', 'fkApplicantId');
    }

   
}
