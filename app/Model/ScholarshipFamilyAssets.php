<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ScholarshipFamilyAssets extends Model
{
    protected $table = 'tbl_sch_applicant_assets';
    protected $fillable = ['fkApplicantId'];
    protected $fillables = ['fkSchlrId'];


     public function applicant() 
    {
        return $this->belongsTo('App\Model\Applicant', 'fkApplicantId');
    }

     public function listscholarship()
    {
        return $this->belongsTo('App\Model\Listscholarship', 'fkSchlrId');
    }


    public function setVehCategoryAttribute($value)
    {
        $this->attributes['vehicle_category'] = json_encode($value);
    }

    public function getVehCategoryAttribute($value)
    {
        return $this->attributes['vehicle_category'] = json_decode($value);
    }


    public function setLandCategoryAttribute($value)
    {
        $this->attributes['land_type'] = json_encode($value);
    }

    public function getLandCategoryAttribute($value)
    {
        return $this->attributes['land_type'] = json_decode($value);
    }
}
