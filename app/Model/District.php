<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class District extends Model
{
    protected $table = 'tbl_oas_district';
    protected $primaryKey = 'pkDistId';

    public function province()
    {
    	return $this->belongsTo('App\Model\Province', 'fkProvId');
    }
}
