<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ScholarshipFamilyDetail extends Model
{
    protected $table = 'tbl_sch_family_detail';
    protected $fillable = ['fkApplicantId'];
    protected $fillables = ['fkSchlrId'];


    public function applicant() 
    {
        return $this->belongsTo('App\Model\Applicant', 'fkApplicantId');
    }

     public function listscholarship()
    {
        return $this->belongsTo('App\Model\Listscholarship', 'fkSchlrId');
    }
}
