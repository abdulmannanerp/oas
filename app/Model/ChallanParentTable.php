<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ChallanParentTable extends Model
{
    protected $table = 'challan_parent_table';
    protected $guarded = [];
    public $timestamps = false;
}
