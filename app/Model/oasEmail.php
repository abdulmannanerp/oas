<?php

namespace App\Model;

use Mail;
use Swift_Mailer;
use Illuminate\Support\Facades\Log;
use Illuminate\Database\Eloquent\Model;
use Swift_SmtpTransport as SmtpTransport;
use Config;

class oasEmail extends Model
{

    public static function email($faculty_abbr, $data, $to, $subject, $view, $cc_param='')
    {

        if ($faculty_abbr == 'FA') {
            $email_host = 'smtp-relay.gmail.com';
            $email_port = '587';
            $email_encryption = 'tls';
            $email_username = 'admissionalert.fa@iiu.edu.pk';
            // $email_password = 'jdBTw$v@sT1^';
            $email_password = 'qhqwxzyrnjlcnkpx';
        } else if ($faculty_abbr == 'FBAS') {
            $email_host = 'smtp-relay.gmail.com';
            $email_port = '587';
            $email_encryption = 'tls';
            $email_username = 'admissionalert.fbas@iiu.edu.pk';
            $email_password = 'ubqifshfawcwclbz';
        } else if ($faculty_abbr == 'FET') {
            $email_host = 'smtp-relay.gmail.com';
            $email_port = '587';
            $email_encryption = 'tls';
            $email_username = 'admissionalert.fet@iiu.edu.pk';
            $email_password = 'qpgxgsydceaelukb';
        } else if ($faculty_abbr == 'FIS') {
            $email_host = 'smtp-relay.gmail.com';
            $email_port = '587';
            $email_encryption = 'tls';
            $email_username = 'admissionalert.fis@iiu.edu.pk';
            $email_password = 'hntzloowqwggdabe';
        } else if ($faculty_abbr == 'FLL') {
            $email_host = 'smtp-relay.gmail.com';
            $email_port = '587';
            $email_encryption = 'tls';
            $email_username = 'admissionalert.fll@iiu.edu.pk';
            $email_password = 'uldiagupqssdttdv';
        } else if ($faculty_abbr == 'FMS') {
            $email_host = 'smtp-relay.gmail.com';
            $email_port = '587';
            $email_encryption = 'tls';
            $email_username = 'admissionalert.fms@iiu.edu.pk';
            $email_password = 'rjczxecxhdoqzxsw';
        } else if ($faculty_abbr == 'FSS') {
            $email_host = 'smtp-relay.gmail.com';
            $email_port = '587';
            $email_encryption = 'tls';
            // $email_username = 'admissionalert.fss@iiu.edu.pk';
            // $email_password = 'jdBTw$v@sT1^';
            $email_username = 'admissionalert.fa@iiu.edu.pk';
            $email_password = 'qhqwxzyrnjlcnkpx';
        } else if ($faculty_abbr == 'IIIE') {
            $email_host = 'smtp-relay.gmail.com';
            $email_port = '587';
            $email_encryption = 'tls';
            $email_username = 'admissionalert.iiie@iiu.edu.pk';
            $email_password = 'tqgaprlmmesmggup';
        } else if ($faculty_abbr == 'FSL') {
            $email_host = 'smtp-relay.gmail.com';
            $email_port = '587';
            $email_encryption = 'tls';
            $email_username = 'admissionalert.fsl@iiu.edu.pk';
            $email_password = 'wdbqplisolbxvtvv';

       } else if ($faculty_abbr == 'FE') {
            $email_host = 'smtp-relay.gmail.com';
            $email_port = '587';
            $email_encryption = 'tls';
            $email_username = 'admissionalert.fsl@iiu.edu.pk';
            $email_password = 'wdbqplisolbxvtvv';

        }  else if ($faculty_abbr == 'FC') {
            $email_host = 'smtp-relay.gmail.com';
            $email_port = '587';
            $email_encryption = 'tls';
            $email_username = 'admissionalert.fbas@iiu.edu.pk';
            $email_password = 'ubqifshfawcwclbz';
            
        } else if ($faculty_abbr == 'AdmissionMale') {
            $email_host = 'smtp-relay.gmail.com';
            $email_port = '587';
            $email_encryption = 'tls';
            $email_username = 'admissionalert1@iiu.edu.pk';
            // $email_password = 'admissionalert100';
            $email_password = 'zdakhrobwlmdjenm';

        } else if ($faculty_abbr == 'AdmissionFemale') {
            $email_host = 'smtp-relay.gmail.com';
            $email_port = '587';
            $email_encryption = 'tls';
            $email_username = 'admissionalert2@iiu.edu.pk';
            // $email_password = 'admissionalert20';
            $email_password = 'sbtpzpetuctwmftb';

        }
        if($cc_param){
            $cc_email = ['admission.notifications@iiu.edu.pk', 'admissions@iiu.edu.pk'];
        }else{
            $cc_email = ['admission.notifications@iiu.edu.pk'];
        }
        Config::set('mail.driver', 'smtp');
        Config::set('mail.host', $email_host);
        Config::set('mail.port', $email_port);
        Config::set('mail.encryption', $email_encryption);
        Config::set('mail.username', $email_username);
        Config::set('mail.password', $email_password);

            // $email_username = 'admissionalert1@iiu.edu.pk';
        Mail::send($view, $data, function ($message) use ($to, $subject, $email_username, $cc_email) {
            $message->to($to)
                ->cc($cc_email)
                ->replyTo('admissions@iiu.edu.pk', 'Admission IIUI')
                ->subject($subject);
            $message->from($email_username, 'Admission IIUI');
        });
    }

}
