<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ProgramStats extends Model
{
    protected $table = 'tbl_oas_previous_stats';

    public function program()
    {
    	return $this->belongsTo('App\Model\Programme', 'fkProgramId');
    }
    public function semester()
    {
        return $this->belongsTo('App\Model\Semester', 'fkSemesterId');
    }
    public function department() {
        return $this->belongsTo('App\Model\Department', 'fkDepId');
    }
    public function faculty() {
        return $this->belongsTo('App\Model\Faculty', 'fkFacId');
    }
}
