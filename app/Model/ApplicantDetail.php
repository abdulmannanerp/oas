<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ApplicantDetail extends Model {

    protected $table = 'tbl_oas_applicant_detail';
    protected $guarded =[];

    public function applicant() 
    {
        return $this->belongsTo('App\Model\Applicant', 'fkApplicantId');
    }

    public function province() 
    {
        return $this->belongsTo('App\Model\Province', 'fkDomicile');
    }

    public function district()
    {
        return $this->belongsTo('App\Model\District', 'fkDistrict');
    }

    public function semester()
    {
        return $this->belongsTo('App\Model\Semester', 'fkSemesterId');
    }

    
}
