<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Level extends Model
{
    protected $primaryKey = 'pkLevelId';
    protected $table = 'tbl_oas_prog_level';
}
