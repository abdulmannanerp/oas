<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Logger extends Model
{
    protected $table = 'tbl_oas_logs';
    protected $guarded = [];

    public function user()
    {
    	return $this->belongsTo('App\User', 'modified_by')->select(['id', 'first_name', 'last_name']);
    }

    public function existingProgram()
    {
    	return $this->belongsTo('App\Model\Programme', 'existing_value')->select(['pkProgId', 'title']);
    }

    public function updatedProgram()
    {
    	return $this->belongsTo('App\Model\Programme', 'updated_value')->select(['pkProgId', 'title']);
    }


}
