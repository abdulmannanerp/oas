<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class MeritManage extends Model
{
    protected $table = 'tbl_oas_merit_manage';
    protected $guarded = [];

    public function merit()
    {
        return $this->belongsTo('App\Model\Merit', 'merit_id');
    }

    public function faculty()
    {
    	return $this->belongsTo('App\Model\Faculty', 'fkFacId');
    }

    public function department()
    {
    	return $this->belongsTo('App\Model\Department', 'fkDeptId');
    }

    public function program()
    {
    	return $this->belongsTo('App\Model\Programme', 'fkProgId');
    }

    public function user()
    {
    	return $this->belongsTo('App\User', 'user_id');
    }
}
