<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class RollNumberEng extends Model
{
    protected $table = 'tbl_oas_rollnumber_eng';
    protected $guarded = [];
    // public $timestamps = false;
}
