<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Result extends Model
{
	protected $guarded = [];
    protected $table = 'tbl_oas_results';
    protected $primaryKey = 'pkResultsId';

    public function program()
    {
    	return $this->belongsTo('App\Model\Programme', 'fkProgrammeId');
    }

    public function list() 
    {
    	return $this->hasMany('App\Model\ResultList', 'fkResultId');
    }
}
