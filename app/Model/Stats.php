<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Stats extends Model
{
    protected $table = 'tbl_oas_stats';

    public function semester()
    {
    	return $this->belongsTo('App\Model\Semester', 'semester_id');
    }
}
