<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Programme extends Model
{
    protected $primaryKey = 'pkProgId';
    protected $table = 'tbl_oas_programme';

    public function department() {
        return $this->belongsTo('App\Model\Department', 'fkDepId');
    }

    public function faculty() {
        return $this->belongsTo('App\Model\Faculty', 'fkFacId');
    }
    public function programscheduler(){
        return $this->hasOne('App\Model\ProgrammeScheduler', 'fkProgramId');
    }

    public function applications()
    {
        return $this->hasMany('App\Model\Application', 'fkProgramId');
    }

    public function level ()
    {
        return $this->belongsTo('App\Model\Level', 'fkLeveliId');
    }
}
