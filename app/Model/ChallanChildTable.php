<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ChallanChildTable extends Model
{
    protected $table = 'challan_child_table';
    protected $guarded = [];
    public $timestamps = false;
}
