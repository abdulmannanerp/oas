<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class AljamiaNewAcadRecord extends Model
{
    protected $table = 'newacadrecord';
    protected $guarded = [];
    public $timestamps = false;
}
