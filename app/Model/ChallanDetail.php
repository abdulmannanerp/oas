<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ChallanDetail extends Model
{
    protected $guarded = [];
    protected $table = 'tbl_oas_challan_detail';
    protected $dates = [
        'created_at',
        'updated_at'
    ];

    public function program() 
    {
        return $this->belongsTo('App\Model\Programme', 'fkProgramId');
    }
    public function semester() 
    {
        return $this->belongsTo('App\Model\Semester', 'fkSemesterId');
    }
    public function applicant() 
    {
        return $this->belongsTo('App\Model\Application', 'fkApplicationtId');
    }
    /* Duplicate of above */
    public function application() 
    {
        return $this->belongsTo('App\Model\Application', 'fkApplicationtId');
    }
    public function rollNumber() 
    {
        return $this->belongsTo('App\Model\RollNumber', 'fkRollNumber');
    }
    public function user() 
    {
        return $this->belongsTo('App\User', 'fee_verified_by');
    }
}
