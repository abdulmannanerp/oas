<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Account extends Model
{
    protected $table = 'tbl_oas_faculty_accounts';

    public function faculty()
    {
        return $this->belongsTo('App\Model\Faculty', 'fkFacId');
    }

}
