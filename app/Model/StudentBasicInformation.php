<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class StudentBasicInformation extends Model
{
    protected $table = 'student_basic_information';
    protected $guarded = [];
    public $timestamps = false;
}
