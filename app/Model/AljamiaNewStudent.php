<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class AljamiaNewStudent extends Model
{
    protected $table = 'newstudent';
    protected $guarded = [];
    public $timestamps = false;
}
